library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

entity MMU is
generic ( n: integer := 32;
	  m: integer := 26;
	  TLB_ENTRIES: integer := 64;
	  OFFSET: integer := 12);
Port(clock, reset:in std_logic;
    address_in:in std_logic_vector(31 downto 0);
    address_out:out std_logic_vector(m-1 downto 0); -- 26 bit physical address
    address_valid:out STD_LOGIC := '0';
    ram_address:out std_logic_vector(m-1 downto 0); -- Address to RAM for reading
    r_enable_from_MMU:out STD_LOGIC := '0';       --Read enable to RAM
    ram_data:in std_logic_vector(n-1 downto 0); --Data from RAM
    ram_data_valid:in STD_LOGIC := '0';
    r:in std_logic;         --TLB read request from processor
--    TLB_frame_number:in std_logic_vector(m-OFFSET-1 downto 0); -- Frame number update from processor
    TLB_update_enable:in std_logic); 
end MMU;

architecture Behavioral of MMU is
component TLB is
generic ( n: integer := 32;
	  m: integer := 26;
	  TLB_ENTRIES: integer := 64;
	  OFFSET: integer := 12);
port (clock, reset: in std_logic;
    pn:in std_logic_vector(31-OFFSET downto 0);
--    fn_in:in std_logic_vector(m-OFFSET-1 downto 0);
    fn_out:out std_logic_vector(m-OFFSET-1 downto 0);
    r:in std_logic;
    w:in std_logic;
--    TLB_update_done:out std_logic;
    hit:out std_logic);
end component;

signal tlb_pageno: std_logic_vector(31-OFFSET downto 0) := (others => '0');
signal data_write_address_offset: std_logic_vector(OFFSET-1 downto 0);
--signal tlb_framenoin: std_logic_vector(m-OFFSET-1 downto 0);
signal tlb_framenoout: std_logic_vector(m-OFFSET-1 downto 0);
signal tlb_r: std_logic:= '0';
signal tlb_w: std_logic:= '0';
--signal tlb_update_complete : std_logic; 
signal tlb_hit, update_address_out:std_logic:='0';

TYPE STATE_TYPE IS (s0, WAIT_HIT, s1, s2, s3, s4);
SIGNAL state   : STATE_TYPE := s0;

begin
TLB1:TLB generic map (n, m, TLB_ENTRIES, OFFSET) port map (clock, reset, tlb_pageno, tlb_framenoout, tlb_r, tlb_w, tlb_hit);

process(clock)
begin
if (rising_edge(clock)) then
	IF (reset = '1') THEN
        	state <= s0;
        	tlb_r <= '0';
        	tlb_w <= '0';
 	ELSE		
        	CASE state IS
            		WHEN s0=>
				tlb_w<='0';
			--	tlb_update_complete <= '0';
				address_valid <= '0';
				tlb_pageno <= (others => '0');
				data_write_address_offset <= (others => '0');
	--			tlb_framenoin <= (others => '0');
				if (r = '1') then
                			tlb_pageno <= address_in(31 downto OFFSET); --20 bit page number
                			tlb_r<='1';    
                			state <= WAIT_HIT;
					data_write_address_offset <= address_in(OFFSET-1 downto 0);
				end if;

				if TLB_update_enable = '1' then
					tlb_pageno <= address_in(31 downto OFFSET);
					tlb_w <= '1';

		--			tlb_pageno_tmp <= address_in(31 downto OFFSET);
		--			tlb_framenoin <= TLB_frame_number;
		--			tlb_update_started <= '1';
				end if;

		--		if tlb_update_complete = '0' and tlb_update_started = '1' then
		--			tlb_pageno <= tlb_pageno_tmp;
		--			tlb_w <= '1';
		--		end if;

		--		if tlb_update_complete = '1' then
		--			tlb_update_started <= '0';
		--			tlb_pageno_tmp <= (others => '0');
		--		end if;
			WHEN WAIT_HIT=>
				state <= s1;
            		WHEN s1=>
  				tlb_r<='0';
                		IF tlb_hit = '1' THEN
					update_address_out <= '1';
                    			
                		ELSIF tlb_hit = '0' and update_address_out = '0' then
                    			ram_address <= address_in(31 downto 32-m);
					r_enable_from_MMU <= '1';
                			state <= s2;
                		END IF;

				if update_address_out = '1' then
					address_out <= tlb_framenoout & data_write_address_offset;   
					address_valid <= '1'; 
					update_address_out <= '0';
                			state <= s0;
				end if;
            		WHEN s2=>               		
				if(ram_data_valid = '1') then
                	--		tlb_framenoin <= ram_data(m-OFFSET-1 downto 0);
                			address_out <= ram_data(m-OFFSET-1 downto 0) & address_in(OFFSET-1 downto 0);
					address_valid <= '1';
                			tlb_w <= '1';
					r_enable_from_MMU <= '0';
                			state <= s0;
				end if;
            		when others =>
                		state <= s0;
        	END CASE;
 	END IF;
end if;
END PROCESS;
end Behavioral;


