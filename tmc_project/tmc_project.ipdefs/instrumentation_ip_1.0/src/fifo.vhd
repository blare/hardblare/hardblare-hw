----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
------------------------------------------------------

----------------------------------------------------------------------------
entity fifo is
	Generic(
			ADDR_W	: integer	:= 6;					-- address width in bits
			DATA_W 	: integer	:= 8; 				-- data width in bits
			BUFF_L	: integer 	:=63					-- buffer length must be less than address space as in  BUFF_L <or= 2^(ADDR_W)-1
			);
	Port ( 
			clk 					: in std_logic;
			n_reset 				: in std_logic;
			rd_en 				    : in std_logic; 		-- read enable 
			wr_en					: in std_logic; 		-- write enable 
			data_in 				: in std_logic_vector(DATA_W- 1 downto 0); 
			data_out				: out std_logic_vector(DATA_W- 1 downto 0); 
			empty 				    : out std_logic; 
			full					: out std_logic; 
			err					     : out std_logic
);
end fifo;
----------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------------------------
architecture arch of fifo is

	type reg_file_type is array (0 to ((2**ADDR_W) - 1)) of std_logic_vector(DATA_W - 1 downto 0);
	
	-----memory, pointers, and flip flops-------
	signal mem_array					: reg_file_type ;
	signal rd_ptr, wr_ptr 			: std_logic_vector(ADDR_W-1 downto 0); 		-- current pointers
	signal rd_ptr_nxt					: std_logic_vector(ADDR_W-1 downto 0); 		-- next pointer
	signal wr_ptr_nxt 				: std_logic_vector(ADDR_W-1 downto 0); 		-- next pointer
	signal full_ff, empty_ff		: std_logic;																		-- full and empty flag flip flops
	signal full_ff_nxt 				: std_logic;																		-- full and empty flag flip flops for next state
	signal empty_ff_nxt 				: std_logic;
	---------------------------------------------------

begin

	---------- Process to update read, write, full, and empty on clock edges
	reg_update :	
	process(clk) 
	begin
		if rising_edge(clk) then
			if (n_reset = '0')  then
				rd_ptr <= (others => '0');
				wr_ptr <= (others => '0');
				full_ff <= '0';
				empty_ff <= '1';
			else
				rd_ptr <=	rd_ptr_nxt;	
				wr_ptr <= wr_ptr_nxt;
				full_ff <= full_ff_nxt;
				empty_ff <= empty_ff_nxt;			
			end if;	-- end of n_reset if
		end if;	-- end of rising_edge(clk) if
	end process;

	
	----------- Process to control read and write pointers and empty/full flip flops
	Ptr_Cont :	
	process(wr_en, rd_en, wr_ptr, rd_ptr, empty_ff, full_ff)                     

	begin
		wr_ptr_nxt <= wr_ptr;											-- no change to pointers
		rd_ptr_nxt <= rd_ptr;
		full_ff_nxt <= full_ff;
		empty_ff_nxt <= empty_ff;

		---------- check if fifo is full during a write attempt, after a write increment counter
		----------------------------------------------------
		if(wr_en = '1' and rd_en = '0') then
			if(full_ff = '0') then
				if(conv_integer(wr_ptr) < BUFF_L-1 ) then      		
					wr_ptr_nxt <= wr_ptr  + '1';
					empty_ff_nxt <= '0';
				else	
					wr_ptr_nxt <= (others => '0');				
					empty_ff_nxt <= '0';  
				end if; 
				-- check if fifo is full
				if (conv_integer(wr_ptr + '1') = conv_integer(rd_ptr) or (conv_integer(wr_ptr) = (BUFF_L-1) and conv_integer(rd_ptr) = 0)) then      
					full_ff_nxt <= '1';
				end if ;
			end if;
		end if;	
		---------- check to see if fifo is empty during a read attempt, after a read decrement counter
		---------------------------------------------------------------
		if(wr_en = '0' and rd_en = '1') then	
			if(empty_ff = '0') then
				if(conv_integer(rd_ptr) < BUFF_L-1 ) then   			
					rd_ptr_nxt <= rd_ptr + '1';
					full_ff_nxt <= '0';
				else	
					rd_ptr_nxt <= (others => '0');  
					full_ff_nxt <= '0';		
				end if;
				-- check if fifo is empty
				if (conv_integer(rd_ptr  + '1') = conv_integer(wr_ptr) or (conv_integer(rd_ptr) = (BUFF_L-1) and conv_integer(wr_ptr) = 0 )) then     
					empty_ff_nxt <= '1';
				end if ;
			end if;
		end if;
		-----------------------------------------------------------------
		if(wr_en = '1' and rd_en = '1') then
			if(conv_integer(wr_ptr) < BUFF_L-1 ) then  		
				wr_ptr_nxt <= wr_ptr  + '1';	
			else											
				wr_ptr_nxt <=  (others => '0');
			end if;
			if(conv_integer(rd_ptr) < BUFF_L-1 ) then      		
				rd_ptr_nxt <= rd_ptr + '1';		
			else
				rd_ptr_nxt <= (others => '0');
			end if;
		end if;
	end process;

	-------- Process to control memory array writing and reading		
	mem_cont :	
	process(clk)   		
	begin
		if ( rising_edge(clk)) then
			if( n_reset = '0') then
				mem_array <= (others => (others => '0'));  			-- reset memory array
				err <= '0';
			else
				-- if write enable and not full then latch in data and increment wright pointer
				if( wr_en = '1') and (full_ff = '0') then
					mem_array (conv_integer(wr_ptr)) <=  data_in ;
					err <= '0';
				elsif(wr_en = '1') and (full_ff = '1') then					-- check if full and trying to write
					err <= '1';
				end if ;
				-- if read enable and fifo not empty then latch data out and increment read pointer
				if( rd_en = '1') and (empty_ff = '0') then
					data_out <= mem_array (conv_integer(rd_ptr));
					err <= '0';
				elsif(rd_en = '1') and (empty_ff = '1') then			-- check if empty and trying to read 
					err <= '1';
				end if ;
			end if;	-- end of n_reset if
		end if;	-- end of rising_edge(clk) if
	end process;
	
	-------- connect ff to output ports
	full <= full_ff;
	empty <= empty_ff;

end arch;
---------------------------------------------------------------------------------------