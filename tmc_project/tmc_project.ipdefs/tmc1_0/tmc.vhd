---------------------------------------------------------------------
-- TITLE: TMC top module
-- AUTHOR: Arnab
-- DATE CREATED: 16/10/2018
-- FILENAME: tmc.vhd
-- PROJECT: DIFT Coprocessor
-- DESCRIPTION:
--    This entity includes AXI master to connect to DRAM memory controller in PS.
---------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
--use ieee.math_real.all;
use ieee.numeric_std.all;
use work.dependencies_pack.all;

entity tmc is
  Generic
  (
    data_width : integer range 32 to 64 := 32;
    addr_width : integer := 32
    );

  Port
  (
    clock              : in  std_logic;
    reset_in           : in  std_logic;

  -- axi master interface
  axim_done   : in std_logic;
  axim_error_in: in std_logic;

  axim_address : out std_logic_vector(31 downto 0);
  axim_data_out : out std_logic_vector(31 downto 0);
  axim_data_in : in std_logic_vector(31 downto 0);
  axim_read_en : out std_logic;
  axim_write_en : out std_logic;

  axim_start  : out std_logic;

  enable_type_out: out std_logic_vector(1 downto 0);
--  data_read_out  : out  std_logic_vector(31 downto 0);
--  address_ram_store_out : out std_logic_vector(31 downto 0);
--  mdata_out_store_out: out std_logic_vector(31 downto 0);
--  state_out: out std_logic;

--  write_enable_out : out std_logic;
--  write_data_out : out std_logic_vector(31 downto 0);
--  address_from_MMU_out : out std_logic_vector(25 downto 0);
data_from_TMC_out : out std_logic_vector(31 downto 0);
virtual_mem_address_out : out std_logic_vector(31 downto 0);
--  virtual_mem_select_p_out : out std_logic_vector(1 downto 0);
--  c_alu_out : out std_logic_vector(31 downto 0);
t_opcode      : out std_logic_vector(31 downto 0);
pause_out    : out std_logic;
--  a_bus_p_out : out std_logic_vector(31 downto 0);
b_bus_p_out : out std_logic_vector(31 downto 0);
--  c_alu_p_out : out std_logic_vector(31 downto 0);
--  c_alu_p_p_out : out std_logic_vector(31 downto 0);
--  a_source_out : out std_logic_vector(1 downto 0);
--  enable_reg_file_s_out : out std_logic;
reg_source_1_gp_out : out std_logic_vector(31 downto 0);

reset_out1 : out std_logic;

   i_mem_clk : out std_logic;
   i_mem_reset : out std_logic;
   i_mem_sel : out  std_logic_vector(3 downto 0);
   --   mem_write    : in  std_logic;
   i_mem_en      : out std_logic;
   i_mem_address  : out  std_logic_vector(31 downto 0);
   i_mem_data_w  : out std_logic_vector(31 downto 0);
   i_mem_data_r  : in std_logic_vector(31 downto 0);
   
  -- config_mem
  config_mem_clk          : out std_logic;
  config_mem_reset        : out std_logic;
  config_mem_address      : out std_logic_vector(31 downto 0);
  config_mem_data_w       : out std_logic_vector(31 downto 0) := (others => '0');
  config_mem_data_r       : in std_logic_vector(31 downto 0);
  config_mem_sel          : out std_logic_vector(3 downto 0) := "1111";
  config_mem_en           : out std_logic;
  
        -- instrumentation bram interface
        instrumentation_empty   : in std_logic;
        instrumentation_data_r  : in  std_logic_vector(31 downto 0);
        instrumentation_enable  : out std_logic
        
        -- kblare_ps2pl bram interface
--        kblare_ps2pl_clk       : out std_logic;
--        kblare_ps2pl_reset     : out std_logic;
--        kblare_ps2pl_address   : out std_logic_vector(31 downto 0);
--        kblare_ps2pl_data_w    : out std_logic_vector(31 downto 0);
--        kblare_ps2pl_data_r : in std_logic_vector(31 downto 0);
--        kblare_ps2pl_sel       : out std_logic_vector(3 downto 0);
--        kblare_ps2pl_en        : out std_logic;
        
--        kblare_pl2ps_clk       : out std_logic;
--        kblare_pl2ps_reset     : out std_logic;
--        kblare_pl2ps_address   : out std_logic_vector(31 downto 0);
--        kblare_pl2ps_data_w    : out std_logic_vector(31 downto 0);
--        kblare_pl2ps_data_r    : in std_logic_vector(31 downto 0);
--        kblare_pl2ps_sel       : out std_logic_vector(3 downto 0);
--        kblare_pl2ps_en        : out std_logic
        
        
        );
end; --entity tmc

architecture logic of tmc is
  
  component dependencies_pu is
  port(clk           : in std_logic;
    reset_in      : in std_logic;
    pc_enable     : in std_logic;
    stall         : in std_logic;
    intr_in       : in std_logic;
    --initial_config_sec_policy_registers : in sec_policy_registers_array;

    i_mem_clk     : out std_logic;
    i_mem_reset   : out std_logic;
    i_mem_en      : out std_logic;
    i_mem_address : out std_logic_vector(31 downto 0);
    i_mem_data_w  : out std_logic_vector(31 downto 0);
    i_mem_data_r  : in std_logic_vector(31 downto 0);
    i_mem_sel     : out std_logic_vector(3 downto 0);
    
    mem_clk       : out std_logic;
    mem_reset     : out std_logic;
    mem_address   : out std_logic_vector(31 downto 0);
    mem_data_w    : out std_logic_vector(31 downto 0);
    mem_data_r    : in std_logic_vector(31 downto 0);
    mem_sel       : out std_logic_vector(3 downto 0);
    mem_en        : out std_logic;

    -- config_mem
    config_mem_clk          : out std_logic;
    config_mem_reset        : out std_logic;
    config_mem_address      : out std_logic_vector(31 downto 0);
    config_mem_data_w       : out std_logic_vector(31 downto 0) := (others => '0');
    config_mem_data_r       : in std_logic_vector(31 downto 0);
    config_mem_sel          : out std_logic_vector(3 downto 0) := "1111";
    config_mem_en           : out std_logic;
    
    -- instrumentation bram interface
    instrumentation_empty   : in std_logic;
    instrumentation_data_r  : in  std_logic_vector(31 downto 0);
    instrumentation_enable  : out std_logic;

    -- kblare_ps2pl bram interface
    kblare_ps2pl_clk       : out std_logic;
    kblare_ps2pl_reset     : out std_logic;
    kblare_ps2pl_address   : out std_logic_vector(31 downto 0);
    kblare_ps2pl_data_w    : out std_logic_vector(31 downto 0);
    kblare_ps2pl_data_r : in std_logic_vector(31 downto 0);
    kblare_ps2pl_sel       : out std_logic_vector(3 downto 0);
    kblare_ps2pl_en        : out std_logic;

    kblare_pl2ps_clk       : out std_logic;
    kblare_pl2ps_reset     : out std_logic;
    kblare_pl2ps_address   : out std_logic_vector(31 downto 0);
    kblare_pl2ps_data_w    : out std_logic_vector(31 downto 0);
    kblare_pl2ps_data_r    : in std_logic_vector(31 downto 0);
    kblare_pl2ps_sel       : out std_logic_vector(3 downto 0);
    kblare_pl2ps_en        : out std_logic;

    -- axi master interface
    axim_done   : in std_logic;
    axim_error_in: in std_logic;

    axim_address : out std_logic_vector(31 downto 0);
    axim_data_out : out std_logic_vector(31 downto 0);
    axim_data_in : in std_logic_vector(31 downto 0);
    axim_read_en : out std_logic;
    axim_write_en : out std_logic;

    axim_start  : out std_logic;
    enable_type_out: out std_logic_vector(1 downto 0);
--  data_read_out  : out  std_logic_vector(31 downto 0);
--  address_ram_store_out : out std_logic_vector(31 downto 0);
--  mdata_out_store_out: out std_logic_vector(31 downto 0);
--  state_out: out std_logic;

--  write_enable_out : out std_logic;
--  write_data_out : out std_logic_vector(31 downto 0);
--  address_from_MMU_out : out std_logic_vector(25 downto 0);
data_from_TMC_out : out std_logic_vector(31 downto 0);
virtual_mem_address_out : out std_logic_vector(31 downto 0);
--  virtual_mem_select_p_out : out std_logic_vector(1 downto 0);
--  c_alu_out : out std_logic_vector(31 downto 0);
pause_out    : out std_logic;
--  a_bus_p_out : out std_logic_vector(31 downto 0);
b_bus_p_out : out std_logic_vector(31 downto 0);
--  c_alu_p_out : out std_logic_vector(31 downto 0);
--  c_alu_p_p_out : out std_logic_vector(31 downto 0);
--  a_source_out : out std_logic_vector(1 downto 0);
--  enable_reg_file_s_out : out std_logic;
reg_source_1_gp_out : out std_logic_vector(31 downto 0);


    -- local BRAM interface
     -- tag_clk       : out std_logic;
     -- tag_rst       : out std_logic;
     -- tag_en        : out std_logic;
     -- tag_write     : out std_logic;
     -- tag_w_en      : out std_logic_vector(3 downto 0);
     -- tag_address_o : out std_logic_vector(31 downto 0);
     -- tag_data_out  : out std_logic_vector(31 downto 0);
     -- tag_rd_data   : in  std_logic_vector(31 downto 0);

     interrupt     : out std_logic;
     t_pc          : out std_logic_vector(31 downto 0);
     t_opcode      : out std_logic_vector(31 downto 0);
     t_r_dest      : out std_logic_vector(31 downto 0)
     );
end component; --component dependencies_pu

--  component ram
--    generic(load_file_name :    string); 
--    port(clk, reset        : in std_logic;
--      mem_byte_sel : in  std_logic_vector(3 downto 0);
--      mem_write    : in  std_logic;
--      mem_address  : in  std_logic_vector;
--      mem_data_w   : in  std_logic_vector(31 downto 0);
--      mem_data_r   : out std_logic_vector(31 downto 0));
--  end component;

component datamemory is 
port (
  clk          : in  std_logic;
  mem_byte_sel : in  std_logic_vector(3 downto 0);
  en           : in  std_logic;
  we_i         : in  std_logic; 
  addr_i       : in  std_logic_vector(data_mem_address_width-1 downto 0);
  din_i        : in  std_logic_vector(data_mem_data_width-1 downto 0);
  dout_o       : out std_logic_vector(data_mem_data_width-1 downto 0)
  );
end component;


--  signal clk            : std_logic := '1';
--  signal reset          : std_logic := '1'; --, '0' after 100 ns;
signal pc_enable      : std_logic := '0';
signal stall          : std_logic := '1'; 
signal interrupt      : std_logic := '0';
signal mem_clk        : std_logic := '0';
signal mem_reset      : std_logic;
signal mem_sel        : std_logic_vector(3 downto 0);
signal mem_write      : std_logic;
signal mem_address    : std_logic_vector(31 downto 0);
signal mem_data_w     : std_logic_vector(31 downto 0);
signal mem_data_r     : std_logic_vector(31 downto 0);
signal mem_pause      : std_logic;
signal mem_en         : std_logic;
--signal i_mem_clk      : std_logic := '0';
--signal i_mem_reset    : std_logic := '0';
signal i_mem_byte_sel : std_logic_vector(3 downto 0);
signal i_mem_write    : std_logic;
--  signal i_mem_address  : std_logic_vector(31 downto 0);
--  signal i_mem_data_w   : std_logic_vector(31 downto 0);
--  signal i_mem_data_r   : std_logic_vector(31 downto 0);
signal i_mem_pause    : std_logic;
--  signal i_mem_en       : std_logic := '1';
signal t_pc           : std_logic_vector(31 downto 0);
--  signal t_opcode       : std_logic_vector(31 downto 0);
signal t_r_dest       : std_logic_vector(31 downto 0);
signal mem_byte_sel   : std_logic_vector(3 downto 0);
signal c_kblare       : std_logic_vector(31 downto 0);
signal rand_num       : integer := 0;
signal generate_interrupt        : std_logic;
signal init_config_sec_policy_registers : sec_policy_registers_array;
  -- instrumentation bram interface
--  signal instrumentation_empty   : std_logic;
--  signal instrumentation_data_r  : std_logic_vector(31 downto 0);
--  signal instrumentation_enable  : std_logic;

  ---- kblare_ps2pl bram interface
  --signal kblare_ps2pl_empty      : std_logic;
  --signal kblare_ps2pl_data_r     : std_logic_vector(31 downto 0);
  signal kblare_ps2pl_enable     : std_logic;

  ---- kblare_pl2ps bram interface
  --signal kblare_pl2ps_full       : std_logic;
  --signal kblare_pl2ps_data_w     : std_logic_vector(31 downto 0) := (others => '0');
  signal kblare_pl2ps_enable     : std_logic;

--  signal config_mem_clk        : std_logic := '0';
--  signal config_mem_address : std_logic_vector(31 downto 0);
--  signal config_mem_data_w  : std_logic_vector(31 downto 0);
--  signal config_mem_data_r  : std_logic_vector(31 downto 0);
--  signal config_mem_sel     : std_logic_vector(3 downto 0);
--  signal config_mem_write   : std_logic;
--  signal config_mem_en      : std_logic;
--  signal config_mem_reset   : std_logic;

--  signal kblare_ps2pl_clk       : std_logic := '0';
--  signal kblare_ps2pl_reset     : std_logic;
  signal kblare_ps2pl_address_s : std_logic_vector(31 downto 0) := (others => '0');
--  signal kblare_ps2pl_data_r    : std_logic_vector(31 downto 0);
--  signal kblare_ps2pl_data_w    : std_logic_vector(31 downto 0);
--  signal kblare_ps2pl_sel       : std_logic_vector(3 downto 0);
--  signal kblare_ps2pl_en        : std_logic;
  signal kblare_ps2pl_enable_reg: std_logic;

--  signal kblare_pl2ps_clk       : std_logic := '0';
--  signal kblare_pl2ps_reset     : std_logic;
  signal kblare_pl2ps_address_s : std_logic_vector(31 downto 0);
--  signal kblare_pl2ps_data_w    : std_logic_vector(31 downto 0);
--  signal kblare_pl2ps_data_r    : std_logic_vector(31 downto 0);
--  signal kblare_pl2ps_sel       : std_logic_vector(3 downto 0);
--  signal kblare_pl2ps_en        : std_logic;
  signal kblare_pl2ps_enable_reg: std_logic;


-- axi master interface
--  signal axim_o_go               :  std_logic;
--  signal axim_o_RNW              :  std_logic;
--  signal axim_o_address          :  std_logic_vector(31 downto 0);
--  signal axim_o_increment_burst  :  std_logic;
--  signal axim_o_clear_data_fifos :  std_logic;
--  signal axim_o_burst_length     :  std_logic_vector(7 downto 0);
--  signal axim_o_burst_size       :  std_logic_vector(6 downto 0);
--  signal axim_o_read_fifo_en     :  std_logic;
--  signal axim_o_write_fifo_en    :  std_logic;
--  signal axim_o_write_data       :  std_logic_vector(31 downto 0);
--  signal axim_i_error            : std_logic;
--  signal axim_i_busy             : std_logic;
--  signal axim_i_done             : std_logic;
--  signal axim_i_read_data        : std_logic_vector(31 downto 0);
--  signal axim_i_write_fifo_empty : std_logic;
--  signal axim_i_write_fifo_full  : std_logic;
--  signal axim_i_read_fifo_empty  : std_logic;
--  signal axim_i_read_fifo_full   : std_logic;

signal reset_out: std_logic;



begin --architecture
  
  stall       <= '0';
  mem_pause   <= '0';
  i_mem_pause <= '0';
  pc_enable   <= not reset_in;
  
  reset_out <= reset_in;
  reset_out1 <= reset_in;
  
  u1 : dependencies_pu PORT MAP (
    clk      => clock,
    reset_in => reset_out,
    pc_enable=> pc_enable,
    stall    => stall, 
    intr_in  => interrupt,
    --initial_config_sec_policy_registers => init_config_sec_policy_registers,
    i_mem_clk     => i_mem_clk,
    i_mem_reset   => i_mem_reset,
    i_mem_en      => i_mem_en,
    i_mem_address => i_mem_address,
    i_mem_data_w  => i_mem_data_w,
    i_mem_data_r  => i_mem_data_r,
    i_mem_sel     => i_mem_sel,

    mem_clk     => mem_clk,
    mem_reset   => mem_reset,
    mem_address => mem_address,
    mem_data_w  => mem_data_w,
    mem_data_r  => mem_data_r,
    mem_sel     => mem_byte_sel,
    mem_en      => mem_en,

    config_mem_clk     => config_mem_clk,
    config_mem_reset   => config_mem_reset,
    config_mem_address => config_mem_address,
    config_mem_data_w  => config_mem_data_w,
    config_mem_data_r  => config_mem_data_r,
    config_mem_sel     => config_mem_sel,
    config_mem_en      => config_mem_en,

    instrumentation_empty  => instrumentation_empty ,
    instrumentation_data_r => instrumentation_data_r,
    instrumentation_enable => instrumentation_enable,
    
    kblare_ps2pl_clk        => open       ,
    kblare_ps2pl_reset      => open,
    kblare_ps2pl_address    => open,
    kblare_ps2pl_data_r     => (others => '0'),
    kblare_ps2pl_data_w     => open,
    kblare_ps2pl_sel        => open,
    kblare_ps2pl_en         => kblare_ps2pl_enable_reg,

    kblare_pl2ps_clk        => open,
    kblare_pl2ps_reset      => open,
    kblare_pl2ps_address    => open,
    kblare_pl2ps_data_w     => open,
    kblare_pl2ps_data_r     => (others=>'0'),
    kblare_pl2ps_sel        => open,
    kblare_pl2ps_en         => kblare_pl2ps_enable_reg,

    -- axi master interface
    axim_done => axim_done,
    axim_error_in =>  axim_error_in,

    axim_address => axim_address,
    axim_data_out => axim_data_out,
    axim_data_in => axim_data_in,
    axim_read_en => axim_read_en,
    axim_write_en => axim_write_en,

    axim_start => axim_start,

    enable_type_out => enable_type_out,
--  data_read_out => data_read_out,
--  address_ram_store_out => address_ram_store_out,
--  mdata_out_store_out => mdata_out_store_out,
--  state_out => state_out,

--  write_enable_out => write_enable_out,
--  write_data_out => write_data_out,
--  address_from_MMU_out => address_from_MMU_out,
data_from_TMC_out => data_from_TMC_out,
virtual_mem_address_out => virtual_mem_address_out,
--  virtual_mem_select_p_out => virtual_mem_select_p_out,
--  c_alu_out => c_alu_out,
pause_out => pause_out,
--  a_bus_p_out => a_bus_p_out,
b_bus_p_out => b_bus_p_out,
--  c_alu_p_out => c_alu_p_out,
--  c_alu_p_p_out => c_alu_p_p_out,
--  a_source_out => a_source_out,
--  enable_reg_file_s_out => enable_reg_file_s_out,
reg_source_1_gp_out => reg_source_1_gp_out,


  --  tag_clk       => tag_clk      ,
  --  tag_rst       => tag_rst      ,
  --  tag_en        => tag_en       ,
  --  tag_write     => tag_write    ,
  --  tag_w_en      => tag_w_en     ,
  --  tag_address_o => tag_address_o,
  --  tag_data_out  => tag_data_out ,
  --  tag_rd_data   => tag_rd_data  ,
  
  interrupt => generate_interrupt,
  t_pc     => t_pc,
  t_opcode => t_opcode,
  t_r_dest => t_r_dest);
  
--    u2 : ram generic map ("/lab/home/labsticc/biswas/tmc/code_dependencies.txt")
--    PORT MAP (
--      clk          => m_axi_aclk,
--      reset        => m_axi_aresetn,
--      mem_byte_sel => i_mem_byte_sel,
--      mem_write    => '0',
--      mem_address  => i_mem_address(inst_mem_address_width-1 downto 0),
--      mem_data_w   => i_mem_data_w,
--      mem_data_r   => i_mem_data_r);

u3 : datamemory port map (
  clk          => clock,
  mem_byte_sel => mem_byte_sel,
  en           => '1',
  we_i         => mem_write,
  addr_i       => mem_address(data_mem_address_width-1 downto 0),
  din_i        => mem_data_w,
  dout_o       => mem_data_r);

--    u4_tag_memory : datamemory port map (
--      clk          => tag_clk,
--      mem_byte_sel => tag_w_en,
--      en           => tag_en,
--      we_i         => tag_write,
--      addr_i       => tag_address_o(data_mem_address_width-1 downto 0),
--      din_i        => tag_data_out,
--      dout_o       => tag_rd_data);



process(clock)
begin
  if rising_edge(clock) then
    if kblare_ps2pl_enable = '1' then 
      kblare_ps2pl_enable_reg <= '1';
      else
      kblare_ps2pl_enable_reg <= '0';
    end if;
  end if;
end process;

process(clock)
begin
  if rising_edge(clock) then
    if kblare_pl2ps_enable = '1' then 
      kblare_pl2ps_enable_reg <= '1';
      else
      kblare_pl2ps_enable_reg <= '0';
    end if;
  end if;
end process;

--  clk_out <= clk;
--  pc      <= t_pc;

end; --architecture logic

