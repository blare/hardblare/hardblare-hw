----------------------------------------------------------------------------------
-- Company: CS
-- PhD Student: MAW
-- 
-- Create Date: 17.06.2016 18:07:20
-- Design Name: 
-- Module Name: address_compute - archi_comportementale
-- Project Name: 
-- Target Devices: 
-- Tool Versions: v20xxx
-- Description: Waypoint packet of PFT protocol
-- 
-- Dependencies: None
-- 
-- Revision: Not tracking
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.All;

entity address_compute is
  port(
  clk, reset, enable_b_i, enable_w : in std_logic;
  pc_in : in std_logic_vector(31 downto 0);
  wapoint_address_cs : in std_logic_vector(4 downto 0);
  waypoint_address : in std_logic_vector(31 downto 0);
  pc_o : out std_logic_vector(31 downto 0)
  );
end entity;

architecture archi_comportementale of address_compute is
  
  signal pc_s, pc_s_reg : std_logic_vector(31 downto 0);    
  signal waypoint_address_reg : std_logic_vector(31 downto 0);

  signal waypoint_address_cs_reg : std_logic_vector(4 downto 0);
  signal temp_signal : std_logic_vector(6 downto 0);   

  begin

    process(clk)
    begin        
        if (clk'event and clk = '1') then
            if reset = '1' then 
                waypoint_address_cs_reg <= (others => '0');
                waypoint_address_reg <= (others => '0');
            else 
                waypoint_address_cs_reg <= wapoint_address_cs;
                waypoint_address_reg <= waypoint_address;
            end if;
        end if;
    end process;
    
    
        process(clk)
        begin
            if (clk'event and clk = '1') then
                if reset = '1' then 
                    pc_s_reg <= (others => '0');
                else             
                    pc_s_reg <= pc_s;
                end if;
            end if;
        end process;


    temp_signal <= enable_b_i & waypoint_address_cs_reg & enable_w;
      
    process(temp_signal, waypoint_address_reg, pc_in)
    begin
    case temp_signal is
        when "0000011" =>
            pc_s <= waypoint_address_reg;
        when "0000101" =>
            pc_s <= pc_in(31 downto 29) & waypoint_address_reg(28 downto 0);
        when "0001001" =>
            pc_s <= pc_in(31 downto 22) & waypoint_address_reg(21 downto 0);
        when "0010001" =>
            pc_s <= pc_in(31 downto 15) & waypoint_address_reg(14 downto 0); 
        when "0100001" =>
            pc_s <= pc_in(31 downto 8) & waypoint_address_reg(7 downto 0);
        when "1000000" =>
            pc_s <= pc_in;
        when others => 
            pc_s <= pc_s_reg;
    end case; 
    end process;       
    
    pc_o <= pc_s;

end architecture;