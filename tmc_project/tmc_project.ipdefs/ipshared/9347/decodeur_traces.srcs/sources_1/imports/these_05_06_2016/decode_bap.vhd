library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.All;

entity decode_bap is
  port(
  clk, reset, start_b, enable, enable_i : in std_logic;
  data_in : in std_logic_vector(7 downto 0);
  instruction_address : in std_logic_vector(31 downto 0);
  stop_b, w_en, atom_output_e : out std_logic;
  pc : out std_logic_vector(31 downto 0)
  );
end entity;

architecture archi_comportementale of decode_bap is
  type state_type is (wait_state, bap_10, bap_11, bap_20, bap_21, bap_30, bap_31, bap_40, bap_41, bap_50, bap_51, bap_exception_10, bap_exception_11, bap_exception_2, wait_enable);
  signal state_reg,state_next, temp_state   : state_type := wait_state;
  signal data_reg_s : std_logic_vector(7 downto 0);    
  signal instruction_address_reg, pc_s, pc_s_reg : std_logic_vector(31 downto 0);
  signal output_data_0 : std_logic_vector(5 downto 0) := (others => '0');
  signal output_data_1 : std_logic_vector(6 downto 0) := (others => '0');
  signal output_data_2 : std_logic_vector(6 downto 0) := (others => '0');
  signal output_data_3 : std_logic_vector(6 downto 0) := (others => '0');
  signal output_data_4 : std_logic_vector(2 downto 0) := (others => '0');
  signal output_data_0_s, output_data_0_s_reg : std_logic;
  signal output_data_1_s, output_data_1_s_reg : std_logic;
  signal output_data_2_s, output_data_2_s_reg : std_logic;
  signal output_data_3_s, output_data_3_s_reg : std_logic;
  signal output_data_4_s, output_data_4_s_reg : std_logic;
  signal enable_i_reg : std_logic;
  signal temp_signal : std_logic_vector(5 downto 0);

  signal stop_b_s, w_en_s, modified_address_en : std_logic;

    -- detect multiple branch packets 
    procedure successive_branch_packets(signal a,b : in std_logic; signal state : out state_type; signal c : out std_logic) is
    begin
        if a = '1' then
            if b = '1' then
                state <= bap_11;
                c <= '1';
            elsif b = '0' then
                state <= bap_10;
                c <= '1'; 
            else
                state <= wait_state;
                c <= '0';
            end if;
        else
            state <= wait_state;            
        end if;
    end procedure;
   

  begin
    -----------------------------------------
    -----------------------------------------
    ----- N E X T  S T A T E  L O G I C -----
    -----------------------------------------
    -----------------------------------------
    process(clk) --, reset, enable
    begin
        if (clk'event and clk='1') then -- condition better than rising_edge(clk)
            if reset = '1' then
                state_reg <= wait_state;          
            elsif (enable = '1') then
                state_reg <= state_next;
            else
                state_reg <= wait_enable;
            end if;
        end if;
    end process;
    
--    data_reg_s <= data_in;

    process(clk)
    begin        
        if (clk'event and clk = '1') then
            if reset = '1' then 
                data_reg_s <= (others => '0');
            else 
                data_reg_s <= data_in;
            end if;
        end if;
    end process;
    
    process(clk)
    begin
        if (clk'event and clk = '1') then 
            if reset = '1' then 
                temp_state <= wait_state; 
            elsif enable = '0' then 
                temp_state <= state_next; 
            end if; 
        end if; 
    end process;
    
    process(clk)
    begin
        if (clk'event and clk = '1') then
            if reset = '1' then 
                data_reg_s <= (others => '0');
                enable_i_reg <= '0';
                instruction_address_reg <= (others => '0');
                pc_s_reg <= (others => '0');
                output_data_0_s_reg <= '0';
                output_data_1_s_reg <= '0';
                output_data_2_s_reg <= '0';
                output_data_3_s_reg <= '0';
                output_data_4_s_reg <= '0';
            else             
                data_reg_s <= data_in;
                enable_i_reg <= enable_i;
                instruction_address_reg <= instruction_address;
                pc_s_reg <= pc_s;
                output_data_0_s_reg <= output_data_0_s;
                output_data_1_s_reg <= output_data_1_s;
                output_data_2_s_reg <= output_data_2_s;
                output_data_3_s_reg <= output_data_3_s;
                output_data_4_s_reg <= output_data_4_s;
            end if;
        end if;
    end process; 
    

  process(state_reg, data_reg_s, start_b, temp_state)
    begin
    output_data_0_s <= '0';
    output_data_1_s <= '0';
    output_data_2_s <= '0';
    output_data_3_s <= '0';
    output_data_4_s <= '0';

    stop_b_s <= '0';
    atom_output_e <= '0';
    modified_address_en <= '0'; 
    case state_reg is
    when wait_state =>
        modified_address_en <= '0'; 
        successive_branch_packets(start_b, data_reg_s(7), state_next, output_data_0_s);

    when wait_enable =>
        output_data_0_s <= '0';
        output_data_1_s <= '0';
        output_data_2_s <= '0';
        output_data_3_s <= '0';
        output_data_4_s <= '0';
        modified_address_en <= '0'; 
        stop_b_s <= '0';
        atom_output_e <= '0';
        state_next <= temp_state;
   
    when bap_10 | bap_20 | bap_30 | bap_40 | bap_50 | bap_exception_10 | bap_exception_2 =>        
        atom_output_e <= '1';        
        stop_b_s <= '1';
        modified_address_en <= '1';
        successive_branch_packets(start_b, data_reg_s(7), state_next, output_data_0_s);
    
--    when bap_20 =>
--        atom_output_e <= '1';  
--        --output_data_2_s <= '1';      
--        stop_b_s <= '1';
--        modified_address_en <= '1';        
--        if start_b = '1' then
--            if data_reg_s(7) = '1' then
--                state_next <= bap_11;
--                output_data_0_s <= '1';
--            elsif (data_reg_s(7) = '0') then
--                state_next <= bap_10;
--                output_data_0_s <= '1'; 
--            else
--                state_next <= wait_state;
--                output_data_0_s <= '0';
--            end if;
--        else
--            state_next <= wait_state;            
--        end if;    
        
--    when bap_30 =>
--        atom_output_e <= '1';  
--        --output_data_3_s <= '1';      
--        stop_b_s <= '1';
--        modified_address_en <= '1';        
--        if start_b = '1' then
--            if data_reg_s(7) = '1' then
--                state_next <= bap_11;
--                output_data_0_s <= '1';
--            elsif (data_reg_s(7) = '0') then
--                state_next <= bap_10;
--                output_data_0_s <= '1'; 
--            else
--                state_next <= wait_state;
--                output_data_0_s <= '0';
--            end if;
--        else
--            state_next <= wait_state;            
--        end if;  

--    when bap_40 =>
--        atom_output_e <= '1';  
--        --output_data_4_s <= '1';      
--        stop_b_s <= '1';
--        modified_address_en <= '1';        
--        if start_b = '1' then
--            if data_reg_s(7) = '1' then
--                state_next <= bap_11;
--                output_data_0_s <= '1';
--            elsif (data_reg_s(7) = '0') then
--                state_next <= bap_10;
--                output_data_0_s <= '1'; 
--            else
--                state_next <= wait_state;
--                output_data_0_s <= '0';
--            end if;
--        else
--            state_next <= wait_state;            
--        end if;

--    when bap_50 | bap_exception_10 | bap_exception_2 =>
--        atom_output_e <= '1';                
--        stop_b_s <= '1';
--        modified_address_en <= '1';        
--        if start_b = '1' then
--            if data_reg_s(7) = '1' then
--                state_next <= bap_11;
--                output_data_0_s <= '1';
--            elsif (data_reg_s(7) = '0') then
--                state_next <= bap_10;
--                output_data_0_s <= '1'; 
--            else
--                state_next <= wait_state;
--                output_data_0_s <= '0';
--            end if;
--        else
--            state_next <= wait_state;            
--        end if;  
    

    when bap_11 =>
    stop_b_s <= '0';
    output_data_1_s <= '1';
    modified_address_en <= '0';

    if data_reg_s(7) = '1' then
    state_next <= bap_21;
    else
    state_next <= bap_20; 
    end if;

    when bap_21 =>
    stop_b_s <= '0';
    output_data_2_s <= '1';
    modified_address_en <= '0';
    if data_reg_s(7) = '1' then
    state_next <= bap_31;
    else
    state_next <= bap_30; 
    end if;

    when bap_31 =>
    stop_b_s <= '0';
    modified_address_en <= '0';
    output_data_3_s <= '1';
    if data_reg_s(7) = '1' then
    state_next <= bap_41;
    else
    state_next <= bap_40;
    end if;

    when bap_41 =>
    stop_b_s <= '0';
    output_data_4_s <= '1';
    modified_address_en <= '0';    
    if data_reg_s(6) = '1' then
        state_next <= bap_51;
    else
        state_next <= bap_50;     
    end if;

    when bap_51 =>
    stop_b_s <= '0';
    modified_address_en <= '0';
    if data_reg_s(7) = '1' then
        state_next <= bap_exception_11;
    else
        state_next <= bap_exception_10;     
    end if;

    when bap_exception_11 =>
    stop_b_s <= '1';
    state_next <= bap_exception_2;
    modified_address_en <= '0'; 

    end case;
  end process;
  
    temp_signal <= enable_i_reg & output_data_4_s_reg & output_data_3_s_reg & output_data_2_s_reg & output_data_1_s_reg & output_data_0_s_reg;
           
    process(clk)
    begin      
      if rising_edge(clk) then
        if reset = '1' then 
          output_data_0 <= (others => '0');
        elsif (output_data_0_s = '1') then
          output_data_0 <= data_reg_s(6 downto 1);
        end if;
      end if;
    end process;
  
    process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then 
                output_data_1 <= (others => '0'); 
            elsif (output_data_1_s = '1') then
                output_data_1 <= data_reg_s(6 downto 0);
            end if;
        end if;
    end process;
  
    process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then 
                output_data_2 <= (others => '0'); 
            elsif (output_data_2_s = '1') then
                output_data_2 <= data_reg_s(6 downto 0);
            end if;
        end if;
    end process;
  
    process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then 
                output_data_3 <= (others => '0');
             elsif (output_data_3_s = '1') then
                output_data_3 <= data_reg_s(6 downto 0);
            end if;
        end if;
    end process;
    
    process(clk)
    begin        
        if rising_edge(clk) then
            if reset = '1' then 
                    output_data_4 <= (others => '0'); 
            elsif (output_data_4_s = '1') then
                output_data_4 <= data_reg_s(2 downto 0);
            end if;
        end if;
    end process;
      
    process(temp_signal, output_data_0, output_data_1, output_data_2, output_data_3, output_data_4, pc_s_reg, instruction_address_reg)
    begin
    case temp_signal is 
        when "100000" =>
            pc_s <= instruction_address_reg;
        when "010000" => 
            pc_s <= output_data_4 & output_data_3 & output_data_2 & output_data_1 & output_data_0 & "00";
        when "001000" => 
            pc_s <= pc_s_reg(31 downto 29) & output_data_3 & output_data_2 & output_data_1 & output_data_0 & "00";
        when "000100" => 
            pc_s <= pc_s_reg(31 downto 22) & output_data_2 & output_data_1 & output_data_0 & "00";
        when "000010" => 
            pc_s <= pc_s_reg(31 downto 15) & output_data_1 & output_data_0 & "00";
        when "000001" => 
            pc_s <= pc_s_reg(31 downto 8) & output_data_0 & "00";
        when others => 
            pc_s <= pc_s_reg;
    end case; 
    end process;       
    
    process(clk)
    begin        
        if (rising_edge(clk)) then
            w_en <= '0';
            if reset = '1' then 
                pc <= (others => '0');
                w_en <= '0'; 
            elsif (enable_i_reg = '1' or modified_address_en = '1') then 
               pc <= pc_s;
               w_en <= '1';           
            end if;
        end if;
    end process;   
     stop_b <= stop_b_s;             
     --w_en <= w_en_s;
     --w_en <= '1' when (enable_i_reg = '1' or modified_address_en = '1') else '0'; 

end architecture;