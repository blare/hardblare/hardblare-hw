
################################################################
# This is a generated script based on design: Dispatcher_HardBlare
#
# Though there are limitations about the generated script,
# the main purpose of this utility is to make learning
# IP Integrator Tcl commands easier.
################################################################

namespace eval _tcl {
proc get_script_folder {} {
   set script_path [file normalize [info script]]
   set script_folder [file dirname $script_path]
   return $script_folder
}
}
variable script_folder
set script_folder [_tcl::get_script_folder]

################################################################
# Check if script is running in correct Vivado version.
################################################################
set scripts_vivado_version 2018.2
set current_vivado_version [version -short]

if { [string first $scripts_vivado_version $current_vivado_version] == -1 } {
   puts ""
   catch {common::send_msg_id "BD_TCL-109" "ERROR" "This script was generated using Vivado <$scripts_vivado_version> and is being run in <$current_vivado_version> of Vivado. Please run the script in Vivado <$scripts_vivado_version> then open the design in Vivado <$current_vivado_version>. Upgrade the design by running \"Tools => Report => Report IP Status...\", then run write_bd_tcl to create an updated script."}

   return 1
}

################################################################
# START
################################################################

# To test this script, run the following commands from Vivado Tcl console:
# source Dispatcher_HardBlare_script.tcl


# The design that will be created by this Tcl script contains the following 
# module references:
# Dispatcher_Plasma_MIPS_HardBlare

# Please add the sources of those modules before sourcing this Tcl script.

# If there is no project opened, this script will create a
# project, but make sure you do not have an existing project
# <./myproj/project_1.xpr> in the current working folder.

set list_projs [get_projects -quiet]
if { $list_projs eq "" } {
   create_project project_1 myproj -part xc7z020clg484-1
   set_property BOARD_PART em.avnet.com:zed:part0:1.4 [current_project]
}


# CHANGE DESIGN NAME HERE
variable design_name
set design_name Dispatcher_HardBlare

# If you do not already have an existing IP Integrator design open,
# you can create a design using the following command:
#    create_bd_design $design_name

# Creating design if needed
set errMsg ""
set nRet 0

set cur_design [current_bd_design -quiet]
set list_cells [get_bd_cells -quiet]

if { ${design_name} eq "" } {
   # USE CASES:
   #    1) Design_name not set

   set errMsg "Please set the variable <design_name> to a non-empty value."
   set nRet 1

} elseif { ${cur_design} ne "" && ${list_cells} eq "" } {
   # USE CASES:
   #    2): Current design opened AND is empty AND names same.
   #    3): Current design opened AND is empty AND names diff; design_name NOT in project.
   #    4): Current design opened AND is empty AND names diff; design_name exists in project.

   if { $cur_design ne $design_name } {
      common::send_msg_id "BD_TCL-001" "INFO" "Changing value of <design_name> from <$design_name> to <$cur_design> since current design is empty."
      set design_name [get_property NAME $cur_design]
   }
   common::send_msg_id "BD_TCL-002" "INFO" "Constructing design in IPI design <$cur_design>..."

} elseif { ${cur_design} ne "" && $list_cells ne "" && $cur_design eq $design_name } {
   # USE CASES:
   #    5) Current design opened AND has components AND same names.

   set errMsg "Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 1
} elseif { [get_files -quiet ${design_name}.bd] ne "" } {
   # USE CASES: 
   #    6) Current opened design, has components, but diff names, design_name exists in project.
   #    7) No opened design, design_name exists in project.

   set errMsg "Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 2

} else {
   # USE CASES:
   #    8) No opened design, design_name not in project.
   #    9) Current opened design, has components, but diff names, design_name not in project.

   common::send_msg_id "BD_TCL-003" "INFO" "Currently there is no design <$design_name> in project, so creating one..."

   create_bd_design $design_name

   common::send_msg_id "BD_TCL-004" "INFO" "Making design <$design_name> as current_bd_design."
   current_bd_design $design_name

}

common::send_msg_id "BD_TCL-005" "INFO" "Currently the variable <design_name> is equal to \"$design_name\"."

if { $nRet != 0 } {
   catch {common::send_msg_id "BD_TCL-114" "ERROR" $errMsg}
   return $nRet
}

##################################################################
# DESIGN PROCs
##################################################################



# Procedure to create entire design; Provide argument to make
# procedure reusable. If parentCell is "", will use root.
proc create_root_design { parentCell } {

  variable script_folder
  variable design_name

  if { $parentCell eq "" } {
     set parentCell [get_bd_cells /]
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj


  # Create interface ports

  # Create ports
  set bram_bbt_annotations_address [ create_bd_port -dir O -from 31 -to 0 bram_bbt_annotations_address ]
  set bram_bbt_annotations_bytes_selection [ create_bd_port -dir O -from 3 -to 0 bram_bbt_annotations_bytes_selection ]
  set bram_bbt_annotations_clk [ create_bd_port -dir O -type clk bram_bbt_annotations_clk ]
  set bram_bbt_annotations_data_read [ create_bd_port -dir I -from 31 -to 0 bram_bbt_annotations_data_read ]
  set bram_bbt_annotations_data_write [ create_bd_port -dir O -from 31 -to 0 bram_bbt_annotations_data_write ]
  set bram_bbt_annotations_en [ create_bd_port -dir O bram_bbt_annotations_en ]
  set bram_bbt_annotations_reset [ create_bd_port -dir O -type rst bram_bbt_annotations_reset ]
  set bram_firmware_code_address [ create_bd_port -dir O -from 31 -to 0 bram_firmware_code_address ]
  set bram_firmware_code_bytes_selection [ create_bd_port -dir O -from 3 -to 0 bram_firmware_code_bytes_selection ]
  set bram_firmware_code_clk [ create_bd_port -dir O -type clk bram_firmware_code_clk ]
  set bram_firmware_code_data_read [ create_bd_port -dir I -from 31 -to 0 bram_firmware_code_data_read ]
  set bram_firmware_code_data_write [ create_bd_port -dir O -from 31 -to 0 bram_firmware_code_data_write ]
  set bram_firmware_code_en [ create_bd_port -dir O bram_firmware_code_en ]
  set bram_firmware_code_reset [ create_bd_port -dir O -type rst bram_firmware_code_reset ]
  set bram_kernel_to_monitor_address [ create_bd_port -dir O -from 31 -to 0 bram_kernel_to_monitor_address ]
  set bram_kernel_to_monitor_bytes_selection [ create_bd_port -dir O -from 3 -to 0 bram_kernel_to_monitor_bytes_selection ]
  set bram_kernel_to_monitor_clk [ create_bd_port -dir O -type clk bram_kernel_to_monitor_clk ]
  set bram_kernel_to_monitor_data_read [ create_bd_port -dir I -from 31 -to 0 bram_kernel_to_monitor_data_read ]
  set bram_kernel_to_monitor_data_write [ create_bd_port -dir O -from 31 -to 0 bram_kernel_to_monitor_data_write ]
  set bram_kernel_to_monitor_en [ create_bd_port -dir O bram_kernel_to_monitor_en ]
  set bram_kernel_to_monitor_reset [ create_bd_port -dir O -type rst bram_kernel_to_monitor_reset ]
  set bram_monitor_to_kernel_address [ create_bd_port -dir O -from 31 -to 0 bram_monitor_to_kernel_address ]
  set bram_monitor_to_kernel_bytes_selection [ create_bd_port -dir O -from 3 -to 0 bram_monitor_to_kernel_bytes_selection ]
  set bram_monitor_to_kernel_clk [ create_bd_port -dir O -type clk bram_monitor_to_kernel_clk ]
  set bram_monitor_to_kernel_data_read [ create_bd_port -dir I -from 31 -to 0 bram_monitor_to_kernel_data_read ]
  set bram_monitor_to_kernel_data_write [ create_bd_port -dir O -from 31 -to 0 bram_monitor_to_kernel_data_write ]
  set bram_monitor_to_kernel_en [ create_bd_port -dir O bram_monitor_to_kernel_en ]
  set bram_monitor_to_kernel_reset [ create_bd_port -dir O -type rst bram_monitor_to_kernel_reset ]
  set bram_ptm_traces_address [ create_bd_port -dir O -from 31 -to 0 bram_ptm_traces_address ]
  set bram_ptm_traces_bytes_selection [ create_bd_port -dir O -from 3 -to 0 bram_ptm_traces_bytes_selection ]
  set bram_ptm_traces_clk [ create_bd_port -dir O -type clk bram_ptm_traces_clk ]
  set bram_ptm_traces_data_read [ create_bd_port -dir I -from 31 -to 0 bram_ptm_traces_data_read ]
  set bram_ptm_traces_data_write [ create_bd_port -dir O -from 31 -to 0 bram_ptm_traces_data_write ]
  set bram_ptm_traces_en [ create_bd_port -dir O bram_ptm_traces_en ]
  set bram_ptm_traces_reset [ create_bd_port -dir O -type rst bram_ptm_traces_reset ]
  set bram_tmc_address [ create_bd_port -dir O -from 31 -to 0 bram_tmc_address ]
  set bram_tmc_bytes_selection [ create_bd_port -dir O -from 3 -to 0 bram_tmc_bytes_selection ]
  set bram_tmc_clk [ create_bd_port -dir O -type clk bram_tmc_clk ]
  set bram_tmc_data_read [ create_bd_port -dir I -from 31 -to 0 bram_tmc_data_read ]
  set bram_tmc_data_write [ create_bd_port -dir O -from 31 -to 0 bram_tmc_data_write ]
  set bram_tmc_en [ create_bd_port -dir O bram_tmc_en ]
  set bram_tmc_reset [ create_bd_port -dir O -type rst bram_tmc_reset ]
  set clk [ create_bd_port -dir I clk ]
  set dispatcher_pause [ create_bd_port -dir I dispatcher_pause ]
  set ptm_is_enabled [ create_bd_port -dir I ptm_is_enabled ]
  set reset [ create_bd_port -dir I reset ]

  # Create instance: Dispatcher_Plasma_MI_0, and set properties
  set block_name Dispatcher_Plasma_MIPS_HardBlare
  set block_cell_name Dispatcher_Plasma_MI_0
  if { [catch {set Dispatcher_Plasma_MI_0 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_msg_id "BD_TCL-105" "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $Dispatcher_Plasma_MI_0 eq "" } {
     catch {common::send_msg_id "BD_TCL-106" "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create port connections
  connect_bd_net -net Dispatcher_Plasma_MI_0_bram_bbt_annotations_address [get_bd_ports bram_bbt_annotations_address] [get_bd_pins Dispatcher_Plasma_MI_0/bram_bbt_annotations_address]
  connect_bd_net -net Dispatcher_Plasma_MI_0_bram_bbt_annotations_bytes_selection [get_bd_ports bram_bbt_annotations_bytes_selection] [get_bd_pins Dispatcher_Plasma_MI_0/bram_bbt_annotations_bytes_selection]
  connect_bd_net -net Dispatcher_Plasma_MI_0_bram_bbt_annotations_clk [get_bd_ports bram_bbt_annotations_clk] [get_bd_pins Dispatcher_Plasma_MI_0/bram_bbt_annotations_clk]
  connect_bd_net -net Dispatcher_Plasma_MI_0_bram_bbt_annotations_data_write [get_bd_ports bram_bbt_annotations_data_write] [get_bd_pins Dispatcher_Plasma_MI_0/bram_bbt_annotations_data_write]
  connect_bd_net -net Dispatcher_Plasma_MI_0_bram_bbt_annotations_en [get_bd_ports bram_bbt_annotations_en] [get_bd_pins Dispatcher_Plasma_MI_0/bram_bbt_annotations_en]
  connect_bd_net -net Dispatcher_Plasma_MI_0_bram_bbt_annotations_reset [get_bd_ports bram_bbt_annotations_reset] [get_bd_pins Dispatcher_Plasma_MI_0/bram_bbt_annotations_reset]
  connect_bd_net -net Dispatcher_Plasma_MI_0_bram_firmware_code_address [get_bd_ports bram_firmware_code_address] [get_bd_pins Dispatcher_Plasma_MI_0/bram_firmware_code_address]
  connect_bd_net -net Dispatcher_Plasma_MI_0_bram_firmware_code_bytes_selection [get_bd_ports bram_firmware_code_bytes_selection] [get_bd_pins Dispatcher_Plasma_MI_0/bram_firmware_code_bytes_selection]
  connect_bd_net -net Dispatcher_Plasma_MI_0_bram_firmware_code_clk [get_bd_ports bram_firmware_code_clk] [get_bd_pins Dispatcher_Plasma_MI_0/bram_firmware_code_clk]
  connect_bd_net -net Dispatcher_Plasma_MI_0_bram_firmware_code_data_write [get_bd_ports bram_firmware_code_data_write] [get_bd_pins Dispatcher_Plasma_MI_0/bram_firmware_code_data_write]
  connect_bd_net -net Dispatcher_Plasma_MI_0_bram_firmware_code_en [get_bd_ports bram_firmware_code_en] [get_bd_pins Dispatcher_Plasma_MI_0/bram_firmware_code_en]
  connect_bd_net -net Dispatcher_Plasma_MI_0_bram_firmware_code_reset [get_bd_ports bram_firmware_code_reset] [get_bd_pins Dispatcher_Plasma_MI_0/bram_firmware_code_reset]
  connect_bd_net -net Dispatcher_Plasma_MI_0_bram_kernel_to_monitor_address [get_bd_ports bram_kernel_to_monitor_address] [get_bd_pins Dispatcher_Plasma_MI_0/bram_kernel_to_monitor_address]
  connect_bd_net -net Dispatcher_Plasma_MI_0_bram_kernel_to_monitor_bytes_selection [get_bd_ports bram_kernel_to_monitor_bytes_selection] [get_bd_pins Dispatcher_Plasma_MI_0/bram_kernel_to_monitor_bytes_selection]
  connect_bd_net -net Dispatcher_Plasma_MI_0_bram_kernel_to_monitor_clk [get_bd_ports bram_kernel_to_monitor_clk] [get_bd_pins Dispatcher_Plasma_MI_0/bram_kernel_to_monitor_clk]
  connect_bd_net -net Dispatcher_Plasma_MI_0_bram_kernel_to_monitor_data_write [get_bd_ports bram_kernel_to_monitor_data_write] [get_bd_pins Dispatcher_Plasma_MI_0/bram_kernel_to_monitor_data_write]
  connect_bd_net -net Dispatcher_Plasma_MI_0_bram_kernel_to_monitor_en [get_bd_ports bram_kernel_to_monitor_en] [get_bd_pins Dispatcher_Plasma_MI_0/bram_kernel_to_monitor_en]
  connect_bd_net -net Dispatcher_Plasma_MI_0_bram_kernel_to_monitor_reset [get_bd_ports bram_kernel_to_monitor_reset] [get_bd_pins Dispatcher_Plasma_MI_0/bram_kernel_to_monitor_reset]
  connect_bd_net -net Dispatcher_Plasma_MI_0_bram_monitor_to_kernel_address [get_bd_ports bram_monitor_to_kernel_address] [get_bd_pins Dispatcher_Plasma_MI_0/bram_monitor_to_kernel_address]
  connect_bd_net -net Dispatcher_Plasma_MI_0_bram_monitor_to_kernel_bytes_selection [get_bd_ports bram_monitor_to_kernel_bytes_selection] [get_bd_pins Dispatcher_Plasma_MI_0/bram_monitor_to_kernel_bytes_selection]
  connect_bd_net -net Dispatcher_Plasma_MI_0_bram_monitor_to_kernel_clk [get_bd_ports bram_monitor_to_kernel_clk] [get_bd_pins Dispatcher_Plasma_MI_0/bram_monitor_to_kernel_clk]
  connect_bd_net -net Dispatcher_Plasma_MI_0_bram_monitor_to_kernel_data_write [get_bd_ports bram_monitor_to_kernel_data_write] [get_bd_pins Dispatcher_Plasma_MI_0/bram_monitor_to_kernel_data_write]
  connect_bd_net -net Dispatcher_Plasma_MI_0_bram_monitor_to_kernel_en [get_bd_ports bram_monitor_to_kernel_en] [get_bd_pins Dispatcher_Plasma_MI_0/bram_monitor_to_kernel_en]
  connect_bd_net -net Dispatcher_Plasma_MI_0_bram_monitor_to_kernel_reset [get_bd_ports bram_monitor_to_kernel_reset] [get_bd_pins Dispatcher_Plasma_MI_0/bram_monitor_to_kernel_reset]
  connect_bd_net -net Dispatcher_Plasma_MI_0_bram_ptm_traces_address [get_bd_ports bram_ptm_traces_address] [get_bd_pins Dispatcher_Plasma_MI_0/bram_ptm_traces_address]
  connect_bd_net -net Dispatcher_Plasma_MI_0_bram_ptm_traces_bytes_selection [get_bd_ports bram_ptm_traces_bytes_selection] [get_bd_pins Dispatcher_Plasma_MI_0/bram_ptm_traces_bytes_selection]
  connect_bd_net -net Dispatcher_Plasma_MI_0_bram_ptm_traces_clk [get_bd_ports bram_ptm_traces_clk] [get_bd_pins Dispatcher_Plasma_MI_0/bram_ptm_traces_clk]
  connect_bd_net -net Dispatcher_Plasma_MI_0_bram_ptm_traces_data_write [get_bd_ports bram_ptm_traces_data_write] [get_bd_pins Dispatcher_Plasma_MI_0/bram_ptm_traces_data_write]
  connect_bd_net -net Dispatcher_Plasma_MI_0_bram_ptm_traces_en [get_bd_ports bram_ptm_traces_en] [get_bd_pins Dispatcher_Plasma_MI_0/bram_ptm_traces_en]
  connect_bd_net -net Dispatcher_Plasma_MI_0_bram_ptm_traces_reset [get_bd_ports bram_ptm_traces_reset] [get_bd_pins Dispatcher_Plasma_MI_0/bram_ptm_traces_reset]
  connect_bd_net -net Dispatcher_Plasma_MI_0_bram_tmc_address [get_bd_ports bram_tmc_address] [get_bd_pins Dispatcher_Plasma_MI_0/bram_tmc_address]
  connect_bd_net -net Dispatcher_Plasma_MI_0_bram_tmc_bytes_selection [get_bd_ports bram_tmc_bytes_selection] [get_bd_pins Dispatcher_Plasma_MI_0/bram_tmc_bytes_selection]
  connect_bd_net -net Dispatcher_Plasma_MI_0_bram_tmc_clk [get_bd_ports bram_tmc_clk] [get_bd_pins Dispatcher_Plasma_MI_0/bram_tmc_clk]
  connect_bd_net -net Dispatcher_Plasma_MI_0_bram_tmc_data_write [get_bd_ports bram_tmc_data_write] [get_bd_pins Dispatcher_Plasma_MI_0/bram_tmc_data_write]
  connect_bd_net -net Dispatcher_Plasma_MI_0_bram_tmc_en [get_bd_ports bram_tmc_en] [get_bd_pins Dispatcher_Plasma_MI_0/bram_tmc_en]
  connect_bd_net -net Dispatcher_Plasma_MI_0_bram_tmc_reset [get_bd_ports bram_tmc_reset] [get_bd_pins Dispatcher_Plasma_MI_0/bram_tmc_reset]
  connect_bd_net -net bram_bbt_annotations_data_read_0_1 [get_bd_ports bram_bbt_annotations_data_read] [get_bd_pins Dispatcher_Plasma_MI_0/bram_bbt_annotations_data_read]
  connect_bd_net -net bram_firmware_code_data_read_0_1 [get_bd_ports bram_firmware_code_data_read] [get_bd_pins Dispatcher_Plasma_MI_0/bram_firmware_code_data_read]
  connect_bd_net -net bram_kernel_to_monitor_data_read_0_1 [get_bd_ports bram_kernel_to_monitor_data_read] [get_bd_pins Dispatcher_Plasma_MI_0/bram_kernel_to_monitor_data_read]
  connect_bd_net -net bram_monitor_to_kernel_data_read_0_1 [get_bd_ports bram_monitor_to_kernel_data_read] [get_bd_pins Dispatcher_Plasma_MI_0/bram_monitor_to_kernel_data_read]
  connect_bd_net -net bram_ptm_traces_data_read_0_1 [get_bd_ports bram_ptm_traces_data_read] [get_bd_pins Dispatcher_Plasma_MI_0/bram_ptm_traces_data_read]
  connect_bd_net -net bram_tmc_data_read_0_1 [get_bd_ports bram_tmc_data_read] [get_bd_pins Dispatcher_Plasma_MI_0/bram_tmc_data_read]
  connect_bd_net -net clk_1 [get_bd_ports clk] [get_bd_pins Dispatcher_Plasma_MI_0/clk]
  connect_bd_net -net dispatcher_pause_0_1 [get_bd_ports dispatcher_pause] [get_bd_pins Dispatcher_Plasma_MI_0/dispatcher_pause]
  connect_bd_net -net ptm_is_enabled_0_1 [get_bd_ports ptm_is_enabled] [get_bd_pins Dispatcher_Plasma_MI_0/ptm_is_enabled]
  connect_bd_net -net reset_1 [get_bd_ports reset] [get_bd_pins Dispatcher_Plasma_MI_0/reset]

  # Create address segments


  # Restore current instance
  current_bd_instance $oldCurInst

  save_bd_design
}
# End of create_root_design()


##################################################################
# MAIN FLOW
##################################################################

create_root_design ""


