----------------------------------------------------------------------------------
-- Company: CS
-- PhD Student: MAW
-- 
-- Create Date: 12.02.2016 16:41:23
-- Design Name: 
-- Module Name: decode_waypoint - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: v20xxx
-- Description: Waypoint packet of PFT protocol
-- 
-- Dependencies: None
-- 
-- Revision: Not tracking
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.All;

entity decode_waypoint is
    port(
    clk, reset, start_w, enable  : in std_logic;
    data_in : in std_logic_vector(7 downto 0);
    pc_in : in std_logic_vector(31 downto 0);
    waypoint_address : out std_logic_vector(31 downto 0);
    wapoint_address_cs : out std_logic_vector(4 downto 0);
    stop_w, out_en : out std_logic
    );
end entity;

architecture archi_comportementale of decode_waypoint is
    -- 13 states
    type state_type is (wait_state, waypoint, waypoint_00, waypoint_01, waypoint_10, waypoint_11, waypoint_20, waypoint_21, waypoint_30, waypoint_31, waypoint_4, waypoint_4_ib, waypoint_ib);
    signal state_reg,state_next   : state_type;
    signal data_reg_s       : std_logic_vector(7 downto 0);

    signal output_data_0, output_data_0_reg : std_logic_vector(5 downto 0);
    signal output_data_1, output_data_1_reg : std_logic_vector(6 downto 0);
    signal output_data_2, output_data_2_reg : std_logic_vector(6 downto 0);
    signal output_data_3, output_data_3_reg : std_logic_vector(6 downto 0);
    signal output_data_4, output_data_4_reg : std_logic_vector(2 downto 0);

    signal out_en_s, stop_s : std_logic;
    -- control signals
    signal en_0, en_1, en_2, en_3, en_4 : std_logic;
    signal en_0_s, en_1_s, en_2_s, en_3_s : std_logic;
    signal en_0_reg, en_1_reg, en_2_reg, en_3_reg, en_4_reg : std_logic;
    --signal en_s, en_s_s : std_logic;
    begin

    process(clk)
    begin
        if (clk'event and clk='1') then  -- condition better than rising_edge(clk) because of the fact that it also detects 'Z' to '1' transitions
            if reset = '1' then
                state_reg <= wait_state;
                data_reg_s <= (others=>'0');
            else
                data_reg_s <= data_in;
                en_0_reg <= en_0;
                en_1_reg <= en_1;
                en_2_reg <= en_2;
                en_3_reg <= en_3;
                en_4_reg <= en_4;
                state_reg <= state_next;
                out_en <= out_en_s;
            end if;
        end if;
    end process;
    

    -----------------------------------------
    -----------------------------------------
    ----- N E X T  S T A T E  L O G I C -----
    -----------------------------------------
    -----------------------------------------
    process(state_reg, data_reg_s, start_w, en_0_reg, en_1_reg, en_2_reg, en_3_reg, en_4_reg)
    begin
        en_0 <= '0';
        en_1 <= '0';
        en_2 <= '0';
        en_3 <= '0';
        en_4 <= '0';
        en_0_s <= '0';
        en_1_s <= '0';
        en_2_s <= '0';
        en_3_s <= '0';
        out_en_s <= '0';
        stop_s <= '0';
        case state_reg is
            when waypoint =>
            -- The waypoint address indicates the last instruction that was executed.
            -- It does not indicate whether that instruction passed its condition code test.
            if (enable  = '1') then 
                if data_reg_s(7) = '1' then
                    state_next <= waypoint_01;
                    en_0_s <= '1';
                else
                    state_next <= waypoint_00;
                    en_0 <= '1';
                end if;
            else
                state_next <= waypoint;
            end if;

            when waypoint_01 =>
            if (enable  = '1') then 
                if (data_reg_s(7) = '1') then
                   state_next <= waypoint_11;
                   en_1_s <= '1';
                else
                    state_next <= waypoint_10;
                    en_1 <= '1';
                end if;
            else
                state_next <= waypoint_01;
            end if;

            when waypoint_11 =>
            if (enable  = '1') then 
                if (data_reg_s(7) = '1') then
                    state_next <= waypoint_21;
                    en_2_s <= '1';
                else
                    state_next <= waypoint_20;
                    en_2 <= '1';
                end if;
            else
                state_next <= waypoint_11;
            end if;

            when waypoint_21 =>
            if (enable  = '1') then 
                if (data_reg_s(7) = '1') then
                    state_next <= waypoint_31;
                    en_3_s <= '1';
                else
                    state_next <= waypoint_30;
                    en_3 <= '1';
                end if;
            else
                state_next <= waypoint_21;
            end if;

            when waypoint_31 =>
            if (enable  = '1') then 
                out_en_s <= '0';
                stop_s <= '0';
                if (data_reg_s(6) = '1') then
                    state_next <= waypoint_4_ib;
                    en_4 <= '1';
                else
                    state_next <= waypoint_4;
                    en_4 <= '1';
                end if;
            else
                state_next <= waypoint_31;
            end if;

            when waypoint_4_ib =>
            if (enable  = '1') then 
                state_next <= waypoint_ib;
            else
                state_next <= waypoint_4_ib;
            end if;

            when waypoint_00 | waypoint_10 | waypoint_20 | waypoint_30 | waypoint_4 =>
            if (enable  = '1') then 
                out_en_s <= '1';
                stop_s <= '1';
                if (data_reg_s = X"72") then
                    state_next <= waypoint;
                else
                    state_next <= wait_state;
                end if;
            else
                state_next <= waypoint_00;
            end if;

            when waypoint_ib =>
            if (enable  = '1') then 
                out_en_s <= '1';
                stop_s <= '1';
                if (data_reg_s = X"72") then
                    state_next <= waypoint;
                else
                    state_next <= wait_state;
                end if;
            else
                state_next <= waypoint_ib;
            end if;


            when wait_state =>
            en_0 <= '0';
            en_1 <= '0';
            en_2 <= '0';
            en_3 <= '0';
            en_4 <= '0';
            out_en_s <= '0';
            stop_s <= '0';
            if (enable  = '1') then 
                if start_w = '1' then
                    state_next <= waypoint;
                else
                    state_next <= wait_state;
                end if;
            else
                state_next <= wait_state;
            end if;
        end case;
    end process;


    process(clk, reset)
    begin
        if reset = '1' then
            output_data_0_reg <= (others => '0');
            output_data_1_reg <= (others => '0');
            output_data_2_reg <= (others => '0');
            output_data_3_reg <= (others => '0');
            output_data_4_reg <= (others => '0');
        elsif (clk'event and clk = '1') then
            output_data_0_reg <= output_data_0;
            output_data_1_reg <= output_data_1;
            output_data_2_reg <= output_data_2;
            output_data_3_reg <= output_data_3;
            output_data_4_reg <= output_data_4;
        end if;
    end process;

    wapoint_address_cs <= en_0_reg & en_1_reg & en_2_reg & en_3_reg & en_4_reg;

    process(clk)
    begin        
        if rising_edge(clk) then
            if reset = '1' then 
                output_data_0 <= (others => '0'); 
            elsif (en_0 = '1') then
                output_data_0 <= pc_in(7) & data_reg_s(5 downto 1);
            elsif (en_0_s = '1') then 
                output_data_0 <= data_reg_s(6 downto 1);
            end if;
        end if;
    end process;

    process(clk)
    begin        
        if rising_edge(clk) then
            if reset = '1' then 
                output_data_1 <= (others => '0'); 
            elsif (en_1 = '1') then
                output_data_1 <= pc_in(14) & data_reg_s(5 downto 0);
            elsif (en_1_s = '1') then 
                output_data_1 <= data_reg_s(6 downto 0);
            end if;
        end if;
    end process;

    process(clk)
    begin        
        if rising_edge(clk) then
            if reset = '1' then 
                output_data_2 <= (others => '0'); 
            elsif (en_2 = '1') then
                output_data_2 <= pc_in(21) & data_reg_s(5 downto 0);
            elsif (en_2_s = '1') then 
                output_data_2 <= data_reg_s(6 downto 0);
            end if;
        end if;
    end process;

    process(clk)
    begin        
        if rising_edge(clk) then
            if reset = '1' then 
                output_data_3 <= (others => '0'); 
            elsif (en_3 = '1') then
                output_data_3 <= pc_in(28) & data_reg_s(5 downto 0);
            elsif (en_3_s = '1') then 
                output_data_3 <= data_reg_s(6 downto 0);
            end if;
        end if;
    end process;

    process(clk)
    begin        
        if rising_edge(clk) then
            if reset = '1' then 
                output_data_2 <= (others => '0'); 
            elsif (en_4 = '1') then
                output_data_4 <= data_reg_s(2 downto 0);
            end if;
        end if;
    end process;

    waypoint_address <= output_data_4 & output_data_3 & output_data_2 & output_data_1 & output_data_0 & "00";
    stop_w <= stop_s;



end architecture;
