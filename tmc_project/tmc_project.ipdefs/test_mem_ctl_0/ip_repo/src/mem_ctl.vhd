-- simple memory controller to write to BRAM
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--type ARRAY_TYPE is array (integer range <>) of std_logic;

entity mem_ctl is

port(
  clk : in std_logic;
  data_v : in std_logic;
  data_in : in std_logic_vector(31 downto 0);
  
  rst,en : out std_logic;
  w_en : out std_logic_vector(3 downto 0);
  addr, data_out : out std_logic_vector(31 downto 0)
);
end entity;

architecture rtl of mem_ctl is
signal temp_data : std_logic_vector(31 downto 0) := (others=>'0');
signal temp_addr : std_logic_vector(31 downto 0) := X"00000000";
begin
  en <= data_v;
  rst <= '0';
  process(clk)
  begin
    if (rising_edge(clk)) then
      if (data_v = '1') then
        temp_addr <= std_logic_vector(unsigned(temp_addr) + to_unsigned(4,32) );
        temp_data <= data_in;
        w_en <= "1111"; -- 4 bytes
      else
        temp_addr <= temp_addr;
        temp_data <= data_in;
        w_en <= "0000"; -- 4 bytes
      end if;
    end if;
  end process;
  addr <= temp_addr;
  data_out <= temp_data;
end architecture;
