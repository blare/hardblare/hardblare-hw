library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity update_address is
port (
clk : in std_logic;
enable_i, enable_m : in std_logic;
instruction_address : in std_logic_vector(31 downto 0);
modified_address : in std_logic_vector(31 downto 0);
pc : out std_logic_vector(31 downto 0);
w_en : out std_logic
);
end entity;

architecture archi_simple of update_address is

begin
    process(clk, enable_i, enable_m, instruction_address, modified_address)
    begin
        if (clk'event and clk = '1') then
            if (enable_i = '1') then
                pc <= instruction_address;
                w_en <= '1';
            elsif (enable_m_0 = '1') then
                pc <= modified_address;
                w_en <= '1';
            else
                w_en <= '0';
            end if;
        end if;
    end process;
end architecture;
