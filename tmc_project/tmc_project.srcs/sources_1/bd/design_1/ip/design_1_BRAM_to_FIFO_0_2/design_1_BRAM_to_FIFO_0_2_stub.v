// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
// Date        : Sun Jun  7 12:02:51 2020
// Host        : HardBlare-Workstation running 64-bit Ubuntu 16.04.6 LTS
// Command     : write_verilog -force -mode synth_stub -rename_top design_1_BRAM_to_FIFO_0_2 -prefix
//               design_1_BRAM_to_FIFO_0_2_ design_1_BRAM_to_FIFO_0_2_stub.v
// Design      : design_1_BRAM_to_FIFO_0_2
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "BRAM_to_FIFO,Vivado 2018.2" *)
module design_1_BRAM_to_FIFO_0_2(from_bram_address, from_bram_clk, 
  from_bram_data_write, from_bram_enable, from_bram_reset, from_bram_bytes_selection, 
  to_bram_data_read, to_fifo_data_write, to_fifo_write_enable, to_fifo_read_enable, 
  to_fifo_clk, to_fifo_reset, from_fifo_data_read)
/* synthesis syn_black_box black_box_pad_pin="from_bram_address[31:0],from_bram_clk,from_bram_data_write[31:0],from_bram_enable,from_bram_reset,from_bram_bytes_selection[3:0],to_bram_data_read[31:0],to_fifo_data_write[31:0],to_fifo_write_enable,to_fifo_read_enable,to_fifo_clk,to_fifo_reset,from_fifo_data_read[31:0]" */;
  input [31:0]from_bram_address;
  input from_bram_clk;
  input [31:0]from_bram_data_write;
  input from_bram_enable;
  input from_bram_reset;
  input [3:0]from_bram_bytes_selection;
  output [31:0]to_bram_data_read;
  output [31:0]to_fifo_data_write;
  output to_fifo_write_enable;
  output to_fifo_read_enable;
  output to_fifo_clk;
  output to_fifo_reset;
  input [31:0]from_fifo_data_read;
endmodule
