

// TRACES VALUE
#define TRACE_VALUE_NULL     0x00000000
#define TRACE_VALUE_SYSCALL  0xffff0008

// SYSCALL PROTOCOL HANDLING
#define SYSCALL_PROTOCOL_SYSCALL_EXIT           0x00000000
#define SYSCALL_PROTOCOL_REQUEST_SEND_TAG       0x00000001
#define SYSCALL_PROTOCOL_REQUEST_GET_TAG        0x00000002
#define SYSCALL_PROTOCOL_REQUEST_ALLOC_MEMORY   0x00000003
#define SYSCALL_PROTOCOL_REQUEST_DEALLOC_MEMORY 0x00000004

// MASK
#define SIX_BITS      0x3F
#define FIVE_BITS     0x1F
#define FOUR_BITS     0xF
#define TWELVE_BITS   0xFFF
#define SIXTEEN_BITS  0xFFFF

// OPCODE
#define SVC_ANNOT                    0x10
#define INSTR_VALUE_INIT             0x0
#define INIT_SYSTEM_TAG_REG_POLICY   0x17
#define INIT_USER_TAG_REG_POLICY     0x18
#define LW                           0x1
#define SW                           0x2
#define LCR                          0x3
#define TLB                          0x4
#define Tag_rri                      0x7
#define Tag_reg_imm                  0x20
#define Tag_reg_reg                  0x21
#define Tag_mem_reg                  0x22
#define Lui                          0x2B
#define TRR                          0x23
#define Tag_arith_log                0x26
#define Tag_check_reg                0x2E
#define Tag_mem_tr                   0x24
#define Tag_tr_mem                   0x25
#define Tag_mem_tr_2                 0x2C
#define Tag_tr_mem_2                 0x2D
#define Tag_instrumentation_tr       0x27
#define Tag_tr_instrumentation       0x28
#define Tag_Kblare_tr                0x29
#define Tag_tr_Kblare                0x2A
#define Tag_check_mem                0x2F
#define CHECK_SYSTEM_TAG_MEM_POLICY  0x14

// ARM_OPCODE_TYPE
#define SECURITY_POLICY_ARM_OPCODE_FUNC_NUMBER              0x19
#define SECURITY_POLICY_ARM_OPCODE_NULL                     0x0
#define SECURITY_POLICY_ARM_OPCODE_MOV                      0x1
#define SECURITY_POLICY_ARM_OPCODE_Arith_Logic              0x2
#define SECURITY_POLICY_ARM_OPCODE_COMP                     0x3
#define SECURITY_POLICY_ARM_OPCODE_Arith_logic_and_COMP     0x4
#define SECURITY_POLICY_ARM_OPCODE_FP_MOV                   0x5
#define SECURITY_POLICY_ARM_OPCODE_FP_Arith_Logic           0x6
#define SECURITY_POLICY_ARM_OPCODE_FP_CMP                   0x7
#define SECURITY_POLICY_ARM_OPCODE_CUSTOM1                  0x8
#define SECURITY_POLICY_ARM_OPCODE_CUSTOM2                  0x9
#define SECURITY_POLICY_ARM_OPCODE_CUSTOM3                  0xa
#define SECURITY_POLICY_ARM_OPCODE_CUSTOM4                  0xb
#define SECURITY_POLICY_ARM_OPCODE_Not_implemented1         0xc
#define SECURITY_POLICY_ARM_OPCODE_Not_implemented2         0xd
#define SECURITY_POLICY_ARM_OPCODE_Not_implemented3         0xe
#define SECURITY_POLICY_ARM_OPCODE_Not_implemented4         0xf
// When merging tags of different memory addresses is needed (e.g. @[0x2341] size 42)
#define SECURITY_POLICY_MERGING_TAG_RESULT_TO_LSM_HOOK      0x10
#define SECURITY_POLICY_PUSH_PC                             0x11
#define SECURITY_POLICY_POP_PC                              0x12
#define SECURITY_POLICY_PUSH_LR                             0x13
#define SECURITY_POLICY_POP_LR                              0x14
#define SECURITY_POLICY_PUSH_STACK_FRAME                    0x15
#define SECURITY_POLICY_POP_STACK_FRAME                     0x16
#define SECURITY_POLICY_MALLOC_HEAP_FRAME                   0x17
#define SECURITY_POLICY_FREE_HEAP_FRAME                     0x18

// Default TAG
#define SECURITY_POLICY_DEFAULT_TAG_NUMBER                  0x7
#define SECURITY_POLICY_DEFAULT_TAG_NEW_FRAME_STACK         0x00
#define SECURITY_POLICY_DEFAULT_TAG_DELETE_FRAME_STACK      0x01
#define SECURITY_POLICY_DEFAULT_TAG_NEW_FRAME_HEAP          0x02
#define SECURITY_POLICY_DEFAULT_TAG_DELETE_FRAME_HEAP       0x03
#define SECURITY_POLICY_DEFAULT_TAG_PC                      0x04
#define SECURITY_POLICY_DEFAULT_TAG_LR                      0x05
#define SECURITY_POLICY_DEFAULT_TAG_UNKNOWN                 0x06

// SECURITY POLICY ARM OPCODE OPERATOR
#define SECURITY_POLICY_ARM_OPCODE_FUNC_OPERATOR_NUMBER      10
#define SECURITY_POLICY_ARM_OPCODE_FUNC_OPERATOR_IDX_OR      0
#define SECURITY_POLICY_ARM_OPCODE_FUNC_OPERATOR_IDX_NOR     1
#define SECURITY_POLICY_ARM_OPCODE_FUNC_OPERATOR_IDX_AND     2
#define SECURITY_POLICY_ARM_OPCODE_FUNC_OPERATOR_IDX_NAND    3
#define SECURITY_POLICY_ARM_OPCODE_FUNC_OPERATOR_IDX_XOR     4
#define SECURITY_POLICY_ARM_OPCODE_FUNC_OPERATOR_IDX_XNOR    5
#define SECURITY_POLICY_ARM_OPCODE_FUNC_OPERATOR_IDX_SUP     6
#define SECURITY_POLICY_ARM_OPCODE_FUNC_OPERATOR_IDX_INF     7
#define SECURITY_POLICY_ARM_OPCODE_FUNC_OPERATOR_IDX_SUM     8
#define SECURITY_POLICY_ARM_OPCODE_FUNC_OPERATOR_IDX_SUB     9


uint32_t first_byte_policy(uint32_t a, uint32_t b);
uint32_t or(uint32_t a, uint32_t b);
uint32_t nor(uint32_t a, uint32_t b);
uint32_t and(uint32_t a, uint32_t b);
uint32_t nand(uint32_t a, uint32_t b);
uint32_t xor(uint32_t a, uint32_t b);
uint32_t xnor(uint32_t a, uint32_t b);
uint32_t sup(uint32_t a, uint32_t b);
uint32_t inf(uint32_t a, uint32_t b);
uint32_t sum(uint32_t a, uint32_t b);
uint32_t sub(uint32_t a, uint32_t b);

uint32_t (*ARM_OPCODE_FUNC_OPERATOR[SECURITY_POLICY_ARM_OPCODE_FUNC_OPERATOR_NUMBER])(uint32_t, uint32_t);
uint32_t (*ARM_OPCODE_FUNC[SECURITY_POLICY_ARM_OPCODE_FUNC_NUMBER])(uint32_t, uint32_t);
uint32_t SECURITY_POLICY_DEFAULT_TAG[SECURITY_POLICY_DEFAULT_TAG_NUMBER];

#define NEG_IMM16                0x8000
#define NEG_IMM12                0x800

#define NEG_IMM16_MASK           0x7FFF
#define NEG_IMM12_MASK           0x7FF


// ERROR STATUS FOR THE DISPATCHER
#define TAG_CHECK_MEM_FAILED             0x1
//#define TAG_CHECK_MEM_FAILED           0x2
//#define TAG_CHECK_MEM_FAILED           0x1
//#define TAG_CHECK_MEM_FAILED           0x1
//#define TAG_CHECK_MEM_FAILED           0x1
//#define TAG_CHECK_MEM_FAILED           0x1


uint32_t wait_read_instrumentation_value(void);
void decode_init_format(const uint32_t trace_value, uint32_t annot);
void decode_tr_format(const uint32_t trace_value,uint32_t annot);
void decode_ti_format(const uint32_t trace_value,uint32_t annot);
uint32_t decode_annotation(const uint32_t trace_value, const uint32_t annotation);
void handling_syscall(void);
