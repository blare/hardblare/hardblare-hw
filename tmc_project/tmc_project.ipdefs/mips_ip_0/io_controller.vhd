library ieee;
use ieee.std_logic_1164.all;
--use work.mips_pack.all;
--use ieee.numeric_std.all;

entity io_controller is 
port(
	mem_address_in 		: in  std_logic_vector(31 downto 0);
	data_mem_cs			: out std_logic;
	annotations_mem_cs 	: out std_logic;
	annotations_bb_cs 	: out std_logic;
	trace_mem_cs		: out std_logic;
	kernel2monitor_cs   : out std_logic;	
    monitor2kernel_cs   : out std_logic
	
);
end entity;

architecture behavioral of io_controller is 
-- alias physical_mem : std_logic_vector(2 downto 0) is mem_address_in(31 downto 29);
alias physical_mem : std_logic_vector(2 downto 0) is mem_address_in(18 downto 16);
begin

	cs_process: process(mem_address_in)	
	begin
		case physical_mem is 
		when "001" => 
			data_mem_cs			<= '0';
			annotations_mem_cs 	<= '1';
			annotations_bb_cs 	<= '0';
			trace_mem_cs		<= '0';
			kernel2monitor_cs      <= '0';    
            monitor2kernel_cs      <= '0';
		when "010" => 
			data_mem_cs			<= '0';
			annotations_mem_cs 	<= '0';
			annotations_bb_cs 	<= '1';
			trace_mem_cs		<= '0';
			kernel2monitor_cs      <= '0';    
            monitor2kernel_cs      <= '0';
		when "011" => 
			data_mem_cs			<= '0';
			annotations_mem_cs 	<= '0';
			annotations_bb_cs 	<= '0';
			trace_mem_cs		<= '1';
			kernel2monitor_cs      <= '0';    
            monitor2kernel_cs      <= '0';
		when "100" => 
            data_mem_cs            <= '0';
            annotations_mem_cs     <= '0';
            annotations_bb_cs      <= '0';
            trace_mem_cs           <= '0';
            kernel2monitor_cs      <= '1';	
            monitor2kernel_cs      <= '0'; 
		when "101" => 
            data_mem_cs            <= '0';
            annotations_mem_cs     <= '0';
            annotations_bb_cs      <= '0';
            trace_mem_cs           <= '0';
            kernel2monitor_cs      <= '0';    
            monitor2kernel_cs      <= '1';
		when others => 
			data_mem_cs			<= '1';
			annotations_mem_cs 	<= '0';
			annotations_bb_cs 	<= '0';
			trace_mem_cs		<= '0';
			kernel2monitor_cs      <= '0';    
            monitor2kernel_cs      <= '0';
		end case;
	end process;


end architecture;
