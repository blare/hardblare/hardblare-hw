---------------------------------------------------------------------
-- TITLE: Test Bench
-- AUTHOR: MAW
-- DATE CREATED: 25/10/2017
-- FILENAME: tbench.vhd
-- PROJECT: DIFT Coprocessor
-- DESCRIPTION:
--    This entity provides a test bench for testing the MIPS CPU core.
---------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.math_real.all;
use ieee.numeric_std.all;

entity testbench is
  
end testbench; --entity testbench

architecture logic of testbench is
  
component reset_gen is
    Port
        (
	clock           : in std_logic;
        reset_in      : in std_logic;

	i_mem_sel : out  std_logic_vector(3 downto 0);
     	i_mem_en      : out std_logic;
        i_mem_address  : out  std_logic_vector(31 downto 0);
        i_mem_data_w  : out std_logic_vector(31 downto 0);
        i_mem_data_r  : in std_logic_vector(31 downto 0);
	
	reset_out : out std_logic);
end component;
    
  signal clock            : std_logic := '1';
  signal reset_in          : std_logic := '0'; --, '0' after 100 ns;
  signal i_mem_sel: std_logic_vector(3 downto 0);
  signal i_mem_en, reset_out : std_logic;
  signal i_mem_address : std_logic_vector(31 downto 0);
  signal i_mem_data_w : std_logic_vector(31 downto 0);
  signal i_mem_data_r : std_logic_vector(31 downto 0);


begin --architecture

  clock         <= not clock after 10 ns;
  reset_in       <= '1' after 90 ns;
--  i_mem_data_r <= x"FFFFFFFF";
  
reset1:reset_gen port map (clock, reset_in, i_mem_sel, i_mem_en, i_mem_address, i_mem_data_w, i_mem_data_r, reset_out);
  
    process(clock)
    begin
      if rising_edge(clock) then
        if i_mem_en = '1' then 
          	i_mem_data_r <= x"FFFFFFFF";
        else
          	i_mem_data_r <= (others => '0');
        end if;
      end if;
    end process;

end; --architecture logic

