---------------------------------------------------------------------
-- TITLE: TMC top module
-- AUTHOR: Arnab
-- DATE CREATED: 16/10/2018
-- FILENAME: tmc.vhd
-- PROJECT: DIFT Coprocessor
-- DESCRIPTION:
--    This entity includes AXI master to connect to DRAM memory controller in PS.
---------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
--use ieee.math_real.all;
use ieee.numeric_std.all;

entity reset_gen is
    Port
        (
	clock           : in std_logic;
        reset_in      : in std_logic;

	i_mem_sel : out  std_logic_vector(3 downto 0);
     	i_mem_en      : out std_logic;
        i_mem_address  : out  std_logic_vector(31 downto 0);
        i_mem_data_w  : out std_logic_vector(31 downto 0);
        i_mem_data_r  : in std_logic_vector(31 downto 0);

	reset_out1: out std_logic;	
	reset_out : out std_logic);
end reset_gen; 

architecture logic of reset_gen is

--  signal reset_sent    : std_logic := '0';
  signal reset_duration : integer range 0 to 10;

begin --architecture  

	i_mem_address <= (others => '0');
	i_mem_en <= '1';
	i_mem_sel <= "0000";
      
    process(clock)
    begin
      if rising_edge(clock) then
	if reset_in = '0' then
		reset_out <= '0';
		reset_out1 <= '1';
		reset_duration <= 0;
	else
		reset_out1 <= '0';
		if i_mem_data_r = "11111111111111111111111111111111" and reset_duration < 10 then
			reset_out <= '1';
		--	reset_sent <= '1';
			reset_duration <= reset_duration + 1;
		else
			reset_out <= '0';
		end if;
	end if;
      end if;
    end process;

end; --architecture logic

