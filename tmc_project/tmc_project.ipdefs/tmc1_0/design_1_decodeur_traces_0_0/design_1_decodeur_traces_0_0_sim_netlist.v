// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
// Date        : Wed Jan 22 14:07:03 2020
// Host        : HardBlare-Workstation running 64-bit Ubuntu 16.04.6 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_1_decodeur_traces_0_0 -prefix
//               design_1_decodeur_traces_0_0_ design_1_decodeur_traces_1_0_sim_netlist.v
// Design      : design_1_decodeur_traces_1_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module design_1_decodeur_traces_0_0_datapath
   (w_en,
    \output_data_3_reg[0] ,
    \temp_state_reg[3] ,
    E,
    \state_reg_reg[0] ,
    \temp_state_reg[1] ,
    \temp_state_reg[1]_0 ,
    \temp_state_reg[0] ,
    \state_reg_reg[0]_0 ,
    stop_bap,
    \state_reg_reg[0]_1 ,
    \state_reg_reg[0]_2 ,
    \temp_state_reg[0]_0 ,
    \state_reg_reg[2] ,
    next_state_logic__48,
    SR,
    w_ctxt_en,
    \temp_state_reg[0]_1 ,
    context_id,
    pc,
    reset,
    clk,
    Q,
    start_b,
    enable_reg_reg,
    state_next,
    \state_reg_reg[0]_3 ,
    D,
    enable_reg_reg_0);
  output w_en;
  output [0:0]\output_data_3_reg[0] ;
  output [1:0]\temp_state_reg[3] ;
  output [0:0]E;
  output \state_reg_reg[0] ;
  output \temp_state_reg[1] ;
  output \temp_state_reg[1]_0 ;
  output \temp_state_reg[0] ;
  output \state_reg_reg[0]_0 ;
  output stop_bap;
  output \state_reg_reg[0]_1 ;
  output \state_reg_reg[0]_2 ;
  output \temp_state_reg[0]_0 ;
  output \state_reg_reg[2] ;
  output [0:0]next_state_logic__48;
  output [0:0]SR;
  output [0:0]w_ctxt_en;
  output \temp_state_reg[0]_1 ;
  output [31:0]context_id;
  output [31:0]pc;
  input reset;
  input clk;
  input [7:0]Q;
  input start_b;
  input enable_reg_reg;
  input [0:0]state_next;
  input \state_reg_reg[0]_3 ;
  input [0:0]D;
  input enable_reg_reg_0;

  wire [0:0]D;
  wire [0:0]E;
  wire [7:0]Q;
  wire [0:0]SR;
  wire [7:1]a;
  wire clk;
  wire [31:0]context_id;
  wire en_s_instruction;
  wire enable_reg_reg;
  wire enable_reg_reg_0;
  wire [0:0]next_state_logic__48;
  wire [7:0]output_data_0;
  wire [7:0]output_data_1;
  wire [7:0]output_data_2;
  wire [7:0]output_data_3;
  wire [0:0]\output_data_3_reg[0] ;
  wire [31:0]pc;
  wire reset;
  wire start_b;
  wire [0:0]state_next;
  wire \state_reg_reg[0] ;
  wire \state_reg_reg[0]_0 ;
  wire \state_reg_reg[0]_1 ;
  wire \state_reg_reg[0]_2 ;
  wire \state_reg_reg[0]_3 ;
  wire \state_reg_reg[2] ;
  wire stop_bap;
  wire \temp_state_reg[0] ;
  wire \temp_state_reg[0]_0 ;
  wire \temp_state_reg[0]_1 ;
  wire \temp_state_reg[1] ;
  wire \temp_state_reg[1]_0 ;
  wire [1:0]\temp_state_reg[3] ;
  wire [0:0]w_ctxt_en;
  wire w_en;

  design_1_decodeur_traces_0_0_decode_bap u_decode_bap
       (.D({output_data_3,output_data_2,output_data_1,output_data_0}),
        .E(en_s_instruction),
        .a(a),
        .clk(clk),
        .\data_reg_s_reg[0] (\output_data_3_reg[0] ),
        .enable_reg_reg(enable_reg_reg),
        .enable_reg_reg_0(E),
        .pc(pc),
        .reset(reset),
        .start_b(start_b),
        .stop_bap(stop_bap),
        .w_en(w_en));
  design_1_decodeur_traces_0_0_decode_i_sync u_decode_i_sync
       (.D(\output_data_3_reg[0] ),
        .E(E),
        .Q(Q),
        .SR(SR),
        .a(a),
        .clk(clk),
        .context_id(context_id),
        .enable_reg_reg(enable_reg_reg),
        .enable_reg_reg_0(enable_reg_reg_0),
        .\instruction_address_reg_reg[31] ({output_data_3,output_data_2,output_data_1,output_data_0}),
        .next_state_logic__48(next_state_logic__48),
        .\output_data_3_reg[7]_0 (en_s_instruction),
        .reset(reset),
        .state_next(state_next),
        .\state_reg_reg[0]_0 (\state_reg_reg[0] ),
        .\state_reg_reg[0]_1 (\state_reg_reg[0]_0 ),
        .\state_reg_reg[0]_2 (\state_reg_reg[0]_1 ),
        .\state_reg_reg[0]_3 (\state_reg_reg[0]_2 ),
        .\state_reg_reg[0]_4 (\state_reg_reg[0]_3 ),
        .\state_reg_reg[2]_0 (\state_reg_reg[2] ),
        .\temp_state_reg[0]_0 (\temp_state_reg[0] ),
        .\temp_state_reg[0]_1 (\temp_state_reg[0]_0 ),
        .\temp_state_reg[0]_2 (\temp_state_reg[0]_1 ),
        .\temp_state_reg[1]_0 (\temp_state_reg[1] ),
        .\temp_state_reg[1]_1 (\temp_state_reg[1]_0 ),
        .\temp_state_reg[2]_0 (D),
        .\temp_state_reg[3]_0 (\temp_state_reg[3] ),
        .w_ctxt_en(w_ctxt_en));
endmodule

module design_1_decodeur_traces_0_0_decode_bap
   (w_en,
    stop_bap,
    pc,
    reset,
    clk,
    E,
    start_b,
    enable_reg_reg,
    a,
    enable_reg_reg_0,
    D,
    \data_reg_s_reg[0] );
  output w_en;
  output stop_bap;
  output [31:0]pc;
  input reset;
  input clk;
  input [0:0]E;
  input start_b;
  input enable_reg_reg;
  input [6:0]a;
  input [0:0]enable_reg_reg_0;
  input [31:0]D;
  input [0:0]\data_reg_s_reg[0] ;

  wire [31:0]D;
  wire [0:0]E;
  wire [6:0]a;
  wire clk;
  wire [31:2]data1;
  wire [0:0]\data_reg_s_reg[0] ;
  wire enable_reg_reg;
  wire [0:0]enable_reg_reg_0;
  wire [31:0]instruction_address_reg;
  wire output_data_0_s;
  wire output_data_0_s_reg;
  wire output_data_1_s;
  wire output_data_1_s_reg;
  wire output_data_2_s;
  wire output_data_2_s_reg;
  wire output_data_3_s;
  wire output_data_3_s_reg;
  wire \output_data_4[0]_i_1_n_0 ;
  wire \output_data_4[1]_i_1_n_0 ;
  wire \output_data_4[2]_i_1_n_0 ;
  wire output_data_4_s;
  wire output_data_4_s_reg;
  wire [31:0]pc;
  wire \pc[14]_i_2_n_0 ;
  wire \pc[14]_i_3_n_0 ;
  wire \pc[21]_i_2_n_0 ;
  wire \pc[21]_i_3_n_0 ;
  wire \pc[28]_i_2_n_0 ;
  wire \pc[28]_i_3_n_0 ;
  wire \pc[28]_i_4_n_0 ;
  wire \pc[29]_i_2_n_0 ;
  wire \pc[30]_i_2_n_0 ;
  wire \pc[31]_i_1_n_0 ;
  wire \pc[31]_i_3_n_0 ;
  wire \pc[7]_i_2_n_0 ;
  wire [31:0]pc_s__0;
  wire pc_s_n_0;
  wire [31:0]pc_s_reg;
  wire reset;
  wire start_b;
  wire [3:0]state_next;
  wire [3:1]state_reg;
  wire \state_reg[0]_i_1_n_0 ;
  wire \state_reg[0]_i_2_n_0 ;
  wire \state_reg[1]_i_1_n_0 ;
  wire \state_reg[1]_i_2_n_0 ;
  wire \state_reg[2]_i_1__0_n_0 ;
  wire \state_reg[3]_i_1__0_n_0 ;
  wire \state_reg_reg_n_0_[0] ;
  wire stop_bap;
  wire [5:5]temp_signal;
  wire \temp_state[0]_i_2__1_n_0 ;
  wire \temp_state[0]_i_3__1_n_0 ;
  wire \temp_state[0]_i_4__0_n_0 ;
  wire \temp_state[1]_i_2__0_n_0 ;
  wire \temp_state[1]_i_3__0_n_0 ;
  wire \temp_state[1]_i_5__0_n_0 ;
  wire \temp_state[1]_i_6_n_0 ;
  wire \temp_state_reg_n_0_[0] ;
  wire \temp_state_reg_n_0_[1] ;
  wire \temp_state_reg_n_0_[2] ;
  wire \temp_state_reg_n_0_[3] ;
  wire w_en;

  FDRE enable_i_reg_reg
       (.C(clk),
        .CE(1'b1),
        .D(E),
        .Q(temp_signal),
        .R(reset));
  FDRE \instruction_address_reg_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(D[0]),
        .Q(instruction_address_reg[0]),
        .R(reset));
  FDRE \instruction_address_reg_reg[10] 
       (.C(clk),
        .CE(1'b1),
        .D(D[10]),
        .Q(instruction_address_reg[10]),
        .R(reset));
  FDRE \instruction_address_reg_reg[11] 
       (.C(clk),
        .CE(1'b1),
        .D(D[11]),
        .Q(instruction_address_reg[11]),
        .R(reset));
  FDRE \instruction_address_reg_reg[12] 
       (.C(clk),
        .CE(1'b1),
        .D(D[12]),
        .Q(instruction_address_reg[12]),
        .R(reset));
  FDRE \instruction_address_reg_reg[13] 
       (.C(clk),
        .CE(1'b1),
        .D(D[13]),
        .Q(instruction_address_reg[13]),
        .R(reset));
  FDRE \instruction_address_reg_reg[14] 
       (.C(clk),
        .CE(1'b1),
        .D(D[14]),
        .Q(instruction_address_reg[14]),
        .R(reset));
  FDRE \instruction_address_reg_reg[15] 
       (.C(clk),
        .CE(1'b1),
        .D(D[15]),
        .Q(instruction_address_reg[15]),
        .R(reset));
  FDRE \instruction_address_reg_reg[16] 
       (.C(clk),
        .CE(1'b1),
        .D(D[16]),
        .Q(instruction_address_reg[16]),
        .R(reset));
  FDRE \instruction_address_reg_reg[17] 
       (.C(clk),
        .CE(1'b1),
        .D(D[17]),
        .Q(instruction_address_reg[17]),
        .R(reset));
  FDRE \instruction_address_reg_reg[18] 
       (.C(clk),
        .CE(1'b1),
        .D(D[18]),
        .Q(instruction_address_reg[18]),
        .R(reset));
  FDRE \instruction_address_reg_reg[19] 
       (.C(clk),
        .CE(1'b1),
        .D(D[19]),
        .Q(instruction_address_reg[19]),
        .R(reset));
  FDRE \instruction_address_reg_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(D[1]),
        .Q(instruction_address_reg[1]),
        .R(reset));
  FDRE \instruction_address_reg_reg[20] 
       (.C(clk),
        .CE(1'b1),
        .D(D[20]),
        .Q(instruction_address_reg[20]),
        .R(reset));
  FDRE \instruction_address_reg_reg[21] 
       (.C(clk),
        .CE(1'b1),
        .D(D[21]),
        .Q(instruction_address_reg[21]),
        .R(reset));
  FDRE \instruction_address_reg_reg[22] 
       (.C(clk),
        .CE(1'b1),
        .D(D[22]),
        .Q(instruction_address_reg[22]),
        .R(reset));
  FDRE \instruction_address_reg_reg[23] 
       (.C(clk),
        .CE(1'b1),
        .D(D[23]),
        .Q(instruction_address_reg[23]),
        .R(reset));
  FDRE \instruction_address_reg_reg[24] 
       (.C(clk),
        .CE(1'b1),
        .D(D[24]),
        .Q(instruction_address_reg[24]),
        .R(reset));
  FDRE \instruction_address_reg_reg[25] 
       (.C(clk),
        .CE(1'b1),
        .D(D[25]),
        .Q(instruction_address_reg[25]),
        .R(reset));
  FDRE \instruction_address_reg_reg[26] 
       (.C(clk),
        .CE(1'b1),
        .D(D[26]),
        .Q(instruction_address_reg[26]),
        .R(reset));
  FDRE \instruction_address_reg_reg[27] 
       (.C(clk),
        .CE(1'b1),
        .D(D[27]),
        .Q(instruction_address_reg[27]),
        .R(reset));
  FDRE \instruction_address_reg_reg[28] 
       (.C(clk),
        .CE(1'b1),
        .D(D[28]),
        .Q(instruction_address_reg[28]),
        .R(reset));
  FDRE \instruction_address_reg_reg[29] 
       (.C(clk),
        .CE(1'b1),
        .D(D[29]),
        .Q(instruction_address_reg[29]),
        .R(reset));
  FDRE \instruction_address_reg_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(D[2]),
        .Q(instruction_address_reg[2]),
        .R(reset));
  FDRE \instruction_address_reg_reg[30] 
       (.C(clk),
        .CE(1'b1),
        .D(D[30]),
        .Q(instruction_address_reg[30]),
        .R(reset));
  FDRE \instruction_address_reg_reg[31] 
       (.C(clk),
        .CE(1'b1),
        .D(D[31]),
        .Q(instruction_address_reg[31]),
        .R(reset));
  FDRE \instruction_address_reg_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(D[3]),
        .Q(instruction_address_reg[3]),
        .R(reset));
  FDRE \instruction_address_reg_reg[4] 
       (.C(clk),
        .CE(1'b1),
        .D(D[4]),
        .Q(instruction_address_reg[4]),
        .R(reset));
  FDRE \instruction_address_reg_reg[5] 
       (.C(clk),
        .CE(1'b1),
        .D(D[5]),
        .Q(instruction_address_reg[5]),
        .R(reset));
  FDRE \instruction_address_reg_reg[6] 
       (.C(clk),
        .CE(1'b1),
        .D(D[6]),
        .Q(instruction_address_reg[6]),
        .R(reset));
  FDRE \instruction_address_reg_reg[7] 
       (.C(clk),
        .CE(1'b1),
        .D(D[7]),
        .Q(instruction_address_reg[7]),
        .R(reset));
  FDRE \instruction_address_reg_reg[8] 
       (.C(clk),
        .CE(1'b1),
        .D(D[8]),
        .Q(instruction_address_reg[8]),
        .R(reset));
  FDRE \instruction_address_reg_reg[9] 
       (.C(clk),
        .CE(1'b1),
        .D(D[9]),
        .Q(instruction_address_reg[9]),
        .R(reset));
  LUT5 #(
    .INIT(32'hFF010000)) 
    \output_data_0[5]_i_1 
       (.I0(state_reg[3]),
        .I1(state_reg[2]),
        .I2(state_reg[1]),
        .I3(\state_reg_reg_n_0_[0] ),
        .I4(start_b),
        .O(output_data_0_s));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_0_reg[0] 
       (.C(clk),
        .CE(output_data_0_s),
        .D(a[0]),
        .Q(data1[2]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_0_reg[1] 
       (.C(clk),
        .CE(output_data_0_s),
        .D(a[1]),
        .Q(data1[3]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_0_reg[2] 
       (.C(clk),
        .CE(output_data_0_s),
        .D(a[2]),
        .Q(data1[4]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_0_reg[3] 
       (.C(clk),
        .CE(output_data_0_s),
        .D(a[3]),
        .Q(data1[5]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_0_reg[4] 
       (.C(clk),
        .CE(output_data_0_s),
        .D(a[4]),
        .Q(data1[6]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_0_reg[5] 
       (.C(clk),
        .CE(output_data_0_s),
        .D(a[5]),
        .Q(data1[7]),
        .R(reset));
  FDRE output_data_0_s_reg_reg
       (.C(clk),
        .CE(1'b1),
        .D(output_data_0_s),
        .Q(output_data_0_s_reg),
        .R(reset));
  LUT4 #(
    .INIT(16'h0100)) 
    \output_data_1[6]_i_1 
       (.I0(state_reg[3]),
        .I1(state_reg[2]),
        .I2(\state_reg_reg_n_0_[0] ),
        .I3(state_reg[1]),
        .O(output_data_1_s));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_1_reg[0] 
       (.C(clk),
        .CE(output_data_1_s),
        .D(\data_reg_s_reg[0] ),
        .Q(data1[8]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_1_reg[1] 
       (.C(clk),
        .CE(output_data_1_s),
        .D(a[0]),
        .Q(data1[9]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_1_reg[2] 
       (.C(clk),
        .CE(output_data_1_s),
        .D(a[1]),
        .Q(data1[10]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_1_reg[3] 
       (.C(clk),
        .CE(output_data_1_s),
        .D(a[2]),
        .Q(data1[11]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_1_reg[4] 
       (.C(clk),
        .CE(output_data_1_s),
        .D(a[3]),
        .Q(data1[12]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_1_reg[5] 
       (.C(clk),
        .CE(output_data_1_s),
        .D(a[4]),
        .Q(data1[13]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_1_reg[6] 
       (.C(clk),
        .CE(output_data_1_s),
        .D(a[5]),
        .Q(data1[14]),
        .R(reset));
  FDRE output_data_1_s_reg_reg
       (.C(clk),
        .CE(1'b1),
        .D(output_data_1_s),
        .Q(output_data_1_s_reg),
        .R(reset));
  LUT4 #(
    .INIT(16'h0100)) 
    \output_data_2[6]_i_1 
       (.I0(state_reg[3]),
        .I1(state_reg[1]),
        .I2(\state_reg_reg_n_0_[0] ),
        .I3(state_reg[2]),
        .O(output_data_2_s));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_2_reg[0] 
       (.C(clk),
        .CE(output_data_2_s),
        .D(\data_reg_s_reg[0] ),
        .Q(data1[15]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_2_reg[1] 
       (.C(clk),
        .CE(output_data_2_s),
        .D(a[0]),
        .Q(data1[16]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_2_reg[2] 
       (.C(clk),
        .CE(output_data_2_s),
        .D(a[1]),
        .Q(data1[17]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_2_reg[3] 
       (.C(clk),
        .CE(output_data_2_s),
        .D(a[2]),
        .Q(data1[18]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_2_reg[4] 
       (.C(clk),
        .CE(output_data_2_s),
        .D(a[3]),
        .Q(data1[19]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_2_reg[5] 
       (.C(clk),
        .CE(output_data_2_s),
        .D(a[4]),
        .Q(data1[20]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_2_reg[6] 
       (.C(clk),
        .CE(output_data_2_s),
        .D(a[5]),
        .Q(data1[21]),
        .R(reset));
  FDRE output_data_2_s_reg_reg
       (.C(clk),
        .CE(1'b1),
        .D(output_data_2_s),
        .Q(output_data_2_s_reg),
        .R(reset));
  LUT4 #(
    .INIT(16'h0400)) 
    \output_data_3[6]_i_1 
       (.I0(state_reg[3]),
        .I1(state_reg[2]),
        .I2(\state_reg_reg_n_0_[0] ),
        .I3(state_reg[1]),
        .O(output_data_3_s));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_3_reg[0] 
       (.C(clk),
        .CE(output_data_3_s),
        .D(\data_reg_s_reg[0] ),
        .Q(data1[22]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_3_reg[1] 
       (.C(clk),
        .CE(output_data_3_s),
        .D(a[0]),
        .Q(data1[23]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_3_reg[2] 
       (.C(clk),
        .CE(output_data_3_s),
        .D(a[1]),
        .Q(data1[24]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_3_reg[3] 
       (.C(clk),
        .CE(output_data_3_s),
        .D(a[2]),
        .Q(data1[25]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_3_reg[4] 
       (.C(clk),
        .CE(output_data_3_s),
        .D(a[3]),
        .Q(data1[26]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_3_reg[5] 
       (.C(clk),
        .CE(output_data_3_s),
        .D(a[4]),
        .Q(data1[27]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_3_reg[6] 
       (.C(clk),
        .CE(output_data_3_s),
        .D(a[5]),
        .Q(data1[28]),
        .R(reset));
  FDRE output_data_3_s_reg_reg
       (.C(clk),
        .CE(1'b1),
        .D(output_data_3_s),
        .Q(output_data_3_s_reg),
        .R(reset));
  LUT6 #(
    .INIT(64'hFFFFFEFF00000200)) 
    \output_data_4[0]_i_1 
       (.I0(\data_reg_s_reg[0] ),
        .I1(state_reg[2]),
        .I2(\state_reg_reg_n_0_[0] ),
        .I3(state_reg[3]),
        .I4(state_reg[1]),
        .I5(data1[29]),
        .O(\output_data_4[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFEFF00000200)) 
    \output_data_4[1]_i_1 
       (.I0(a[0]),
        .I1(state_reg[2]),
        .I2(\state_reg_reg_n_0_[0] ),
        .I3(state_reg[3]),
        .I4(state_reg[1]),
        .I5(data1[30]),
        .O(\output_data_4[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFEFF00000200)) 
    \output_data_4[2]_i_1 
       (.I0(a[1]),
        .I1(state_reg[2]),
        .I2(\state_reg_reg_n_0_[0] ),
        .I3(state_reg[3]),
        .I4(state_reg[1]),
        .I5(data1[31]),
        .O(\output_data_4[2]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_4_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(\output_data_4[0]_i_1_n_0 ),
        .Q(data1[29]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_4_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\output_data_4[1]_i_1_n_0 ),
        .Q(data1[30]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_4_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(\output_data_4[2]_i_1_n_0 ),
        .Q(data1[31]),
        .R(reset));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'h0010)) 
    output_data_4_s_reg_i_1
       (.I0(state_reg[2]),
        .I1(\state_reg_reg_n_0_[0] ),
        .I2(state_reg[3]),
        .I3(state_reg[1]),
        .O(output_data_4_s));
  FDRE output_data_4_s_reg_reg
       (.C(clk),
        .CE(1'b1),
        .D(output_data_4_s),
        .Q(output_data_4_s_reg),
        .R(reset));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'h8F80)) 
    \pc[0]_i_1 
       (.I0(instruction_address_reg[0]),
        .I1(temp_signal),
        .I2(pc_s_n_0),
        .I3(pc_s_reg[0]),
        .O(pc_s__0[0]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc[10]_i_1 
       (.I0(\pc[14]_i_2_n_0 ),
        .I1(pc_s_reg[10]),
        .I2(\pc[28]_i_4_n_0 ),
        .I3(instruction_address_reg[10]),
        .I4(data1[10]),
        .I5(\pc[14]_i_3_n_0 ),
        .O(pc_s__0[10]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc[11]_i_1 
       (.I0(\pc[14]_i_2_n_0 ),
        .I1(pc_s_reg[11]),
        .I2(\pc[28]_i_4_n_0 ),
        .I3(instruction_address_reg[11]),
        .I4(data1[11]),
        .I5(\pc[14]_i_3_n_0 ),
        .O(pc_s__0[11]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc[12]_i_1 
       (.I0(\pc[14]_i_2_n_0 ),
        .I1(pc_s_reg[12]),
        .I2(\pc[28]_i_4_n_0 ),
        .I3(instruction_address_reg[12]),
        .I4(data1[12]),
        .I5(\pc[14]_i_3_n_0 ),
        .O(pc_s__0[12]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc[13]_i_1 
       (.I0(\pc[14]_i_2_n_0 ),
        .I1(pc_s_reg[13]),
        .I2(\pc[28]_i_4_n_0 ),
        .I3(instruction_address_reg[13]),
        .I4(data1[13]),
        .I5(\pc[14]_i_3_n_0 ),
        .O(pc_s__0[13]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc[14]_i_1 
       (.I0(\pc[14]_i_2_n_0 ),
        .I1(pc_s_reg[14]),
        .I2(\pc[28]_i_4_n_0 ),
        .I3(instruction_address_reg[14]),
        .I4(data1[14]),
        .I5(\pc[14]_i_3_n_0 ),
        .O(pc_s__0[14]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \pc[14]_i_2 
       (.I0(output_data_0_s_reg),
        .I1(pc_s_n_0),
        .O(\pc[14]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \pc[14]_i_3 
       (.I0(output_data_1_s_reg),
        .I1(output_data_3_s_reg),
        .I2(output_data_4_s_reg),
        .I3(output_data_2_s_reg),
        .I4(pc_s_n_0),
        .O(\pc[14]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc[15]_i_1 
       (.I0(\pc[21]_i_2_n_0 ),
        .I1(pc_s_reg[15]),
        .I2(\pc[28]_i_4_n_0 ),
        .I3(instruction_address_reg[15]),
        .I4(data1[15]),
        .I5(\pc[21]_i_3_n_0 ),
        .O(pc_s__0[15]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc[16]_i_1 
       (.I0(\pc[21]_i_2_n_0 ),
        .I1(pc_s_reg[16]),
        .I2(\pc[28]_i_4_n_0 ),
        .I3(instruction_address_reg[16]),
        .I4(data1[16]),
        .I5(\pc[21]_i_3_n_0 ),
        .O(pc_s__0[16]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc[17]_i_1 
       (.I0(\pc[21]_i_2_n_0 ),
        .I1(pc_s_reg[17]),
        .I2(\pc[28]_i_4_n_0 ),
        .I3(instruction_address_reg[17]),
        .I4(data1[17]),
        .I5(\pc[21]_i_3_n_0 ),
        .O(pc_s__0[17]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc[18]_i_1 
       (.I0(\pc[21]_i_2_n_0 ),
        .I1(pc_s_reg[18]),
        .I2(\pc[28]_i_4_n_0 ),
        .I3(instruction_address_reg[18]),
        .I4(data1[18]),
        .I5(\pc[21]_i_3_n_0 ),
        .O(pc_s__0[18]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc[19]_i_1 
       (.I0(\pc[21]_i_2_n_0 ),
        .I1(pc_s_reg[19]),
        .I2(\pc[28]_i_4_n_0 ),
        .I3(instruction_address_reg[19]),
        .I4(data1[19]),
        .I5(\pc[21]_i_3_n_0 ),
        .O(pc_s__0[19]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'h8F80)) 
    \pc[1]_i_1 
       (.I0(instruction_address_reg[1]),
        .I1(temp_signal),
        .I2(pc_s_n_0),
        .I3(pc_s_reg[1]),
        .O(pc_s__0[1]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc[20]_i_1 
       (.I0(\pc[21]_i_2_n_0 ),
        .I1(pc_s_reg[20]),
        .I2(\pc[28]_i_4_n_0 ),
        .I3(instruction_address_reg[20]),
        .I4(data1[20]),
        .I5(\pc[21]_i_3_n_0 ),
        .O(pc_s__0[20]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc[21]_i_1 
       (.I0(\pc[21]_i_2_n_0 ),
        .I1(pc_s_reg[21]),
        .I2(\pc[28]_i_4_n_0 ),
        .I3(instruction_address_reg[21]),
        .I4(data1[21]),
        .I5(\pc[21]_i_3_n_0 ),
        .O(pc_s__0[21]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'hEF)) 
    \pc[21]_i_2 
       (.I0(output_data_0_s_reg),
        .I1(output_data_1_s_reg),
        .I2(pc_s_n_0),
        .O(\pc[21]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'hFE00)) 
    \pc[21]_i_3 
       (.I0(output_data_2_s_reg),
        .I1(output_data_4_s_reg),
        .I2(output_data_3_s_reg),
        .I3(pc_s_n_0),
        .O(\pc[21]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc[22]_i_1 
       (.I0(\pc[28]_i_2_n_0 ),
        .I1(pc_s_reg[22]),
        .I2(\pc[28]_i_3_n_0 ),
        .I3(data1[22]),
        .I4(instruction_address_reg[22]),
        .I5(\pc[28]_i_4_n_0 ),
        .O(pc_s__0[22]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc[23]_i_1 
       (.I0(\pc[28]_i_2_n_0 ),
        .I1(pc_s_reg[23]),
        .I2(\pc[28]_i_3_n_0 ),
        .I3(data1[23]),
        .I4(instruction_address_reg[23]),
        .I5(\pc[28]_i_4_n_0 ),
        .O(pc_s__0[23]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc[24]_i_1 
       (.I0(\pc[28]_i_2_n_0 ),
        .I1(pc_s_reg[24]),
        .I2(\pc[28]_i_3_n_0 ),
        .I3(data1[24]),
        .I4(instruction_address_reg[24]),
        .I5(\pc[28]_i_4_n_0 ),
        .O(pc_s__0[24]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc[25]_i_1 
       (.I0(\pc[28]_i_2_n_0 ),
        .I1(pc_s_reg[25]),
        .I2(\pc[28]_i_3_n_0 ),
        .I3(data1[25]),
        .I4(instruction_address_reg[25]),
        .I5(\pc[28]_i_4_n_0 ),
        .O(pc_s__0[25]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc[26]_i_1 
       (.I0(\pc[28]_i_2_n_0 ),
        .I1(pc_s_reg[26]),
        .I2(\pc[28]_i_3_n_0 ),
        .I3(data1[26]),
        .I4(instruction_address_reg[26]),
        .I5(\pc[28]_i_4_n_0 ),
        .O(pc_s__0[26]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc[27]_i_1 
       (.I0(\pc[28]_i_2_n_0 ),
        .I1(pc_s_reg[27]),
        .I2(\pc[28]_i_3_n_0 ),
        .I3(data1[27]),
        .I4(instruction_address_reg[27]),
        .I5(\pc[28]_i_4_n_0 ),
        .O(pc_s__0[27]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc[28]_i_1 
       (.I0(\pc[28]_i_2_n_0 ),
        .I1(pc_s_reg[28]),
        .I2(\pc[28]_i_3_n_0 ),
        .I3(data1[28]),
        .I4(instruction_address_reg[28]),
        .I5(\pc[28]_i_4_n_0 ),
        .O(pc_s__0[28]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT4 #(
    .INIT(16'hFFFD)) 
    \pc[28]_i_2 
       (.I0(pc_s_n_0),
        .I1(output_data_1_s_reg),
        .I2(output_data_0_s_reg),
        .I3(output_data_2_s_reg),
        .O(\pc[28]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'hE0)) 
    \pc[28]_i_3 
       (.I0(output_data_3_s_reg),
        .I1(output_data_4_s_reg),
        .I2(pc_s_n_0),
        .O(\pc[28]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \pc[28]_i_4 
       (.I0(pc_s_n_0),
        .I1(temp_signal),
        .O(\pc[28]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFEAAAAAEAEAAAAA)) 
    \pc[29]_i_1 
       (.I0(\pc[29]_i_2_n_0 ),
        .I1(output_data_4_s_reg),
        .I2(data1[29]),
        .I3(instruction_address_reg[29]),
        .I4(pc_s_n_0),
        .I5(temp_signal),
        .O(pc_s__0[29]));
  LUT6 #(
    .INIT(64'hFFFFFFFB00000000)) 
    \pc[29]_i_2 
       (.I0(output_data_3_s_reg),
        .I1(pc_s_n_0),
        .I2(output_data_1_s_reg),
        .I3(output_data_0_s_reg),
        .I4(output_data_2_s_reg),
        .I5(pc_s_reg[29]),
        .O(\pc[29]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFE222E222E222)) 
    \pc[2]_i_1 
       (.I0(pc_s_reg[2]),
        .I1(pc_s_n_0),
        .I2(temp_signal),
        .I3(instruction_address_reg[2]),
        .I4(data1[2]),
        .I5(\pc[7]_i_2_n_0 ),
        .O(pc_s__0[2]));
  LUT6 #(
    .INIT(64'hFFEAAAAAEAEAAAAA)) 
    \pc[30]_i_1 
       (.I0(\pc[30]_i_2_n_0 ),
        .I1(output_data_4_s_reg),
        .I2(data1[30]),
        .I3(instruction_address_reg[30]),
        .I4(pc_s_n_0),
        .I5(temp_signal),
        .O(pc_s__0[30]));
  LUT6 #(
    .INIT(64'hFFFFFFFB00000000)) 
    \pc[30]_i_2 
       (.I0(output_data_3_s_reg),
        .I1(pc_s_n_0),
        .I2(output_data_1_s_reg),
        .I3(output_data_0_s_reg),
        .I4(output_data_2_s_reg),
        .I5(pc_s_reg[30]),
        .O(\pc[30]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \pc[31]_i_1 
       (.I0(temp_signal),
        .I1(\state_reg_reg_n_0_[0] ),
        .O(\pc[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFEAAAAAEAEAAAAA)) 
    \pc[31]_i_2 
       (.I0(\pc[31]_i_3_n_0 ),
        .I1(output_data_4_s_reg),
        .I2(data1[31]),
        .I3(instruction_address_reg[31]),
        .I4(pc_s_n_0),
        .I5(temp_signal),
        .O(pc_s__0[31]));
  LUT6 #(
    .INIT(64'hFFFFFFFB00000000)) 
    \pc[31]_i_3 
       (.I0(output_data_3_s_reg),
        .I1(pc_s_n_0),
        .I2(output_data_1_s_reg),
        .I3(output_data_0_s_reg),
        .I4(output_data_2_s_reg),
        .I5(pc_s_reg[31]),
        .O(\pc[31]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFE222E222E222)) 
    \pc[3]_i_1 
       (.I0(pc_s_reg[3]),
        .I1(pc_s_n_0),
        .I2(temp_signal),
        .I3(instruction_address_reg[3]),
        .I4(data1[3]),
        .I5(\pc[7]_i_2_n_0 ),
        .O(pc_s__0[3]));
  LUT6 #(
    .INIT(64'hFFFFE222E222E222)) 
    \pc[4]_i_1 
       (.I0(pc_s_reg[4]),
        .I1(pc_s_n_0),
        .I2(temp_signal),
        .I3(instruction_address_reg[4]),
        .I4(data1[4]),
        .I5(\pc[7]_i_2_n_0 ),
        .O(pc_s__0[4]));
  LUT6 #(
    .INIT(64'hFFFFE222E222E222)) 
    \pc[5]_i_1 
       (.I0(pc_s_reg[5]),
        .I1(pc_s_n_0),
        .I2(temp_signal),
        .I3(instruction_address_reg[5]),
        .I4(data1[5]),
        .I5(\pc[7]_i_2_n_0 ),
        .O(pc_s__0[5]));
  LUT6 #(
    .INIT(64'hFFFFE222E222E222)) 
    \pc[6]_i_1 
       (.I0(pc_s_reg[6]),
        .I1(pc_s_n_0),
        .I2(temp_signal),
        .I3(instruction_address_reg[6]),
        .I4(data1[6]),
        .I5(\pc[7]_i_2_n_0 ),
        .O(pc_s__0[6]));
  LUT6 #(
    .INIT(64'hFFFFE222E222E222)) 
    \pc[7]_i_1 
       (.I0(pc_s_reg[7]),
        .I1(pc_s_n_0),
        .I2(temp_signal),
        .I3(instruction_address_reg[7]),
        .I4(data1[7]),
        .I5(\pc[7]_i_2_n_0 ),
        .O(pc_s__0[7]));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000000)) 
    \pc[7]_i_2 
       (.I0(output_data_3_s_reg),
        .I1(output_data_4_s_reg),
        .I2(output_data_2_s_reg),
        .I3(output_data_1_s_reg),
        .I4(output_data_0_s_reg),
        .I5(pc_s_n_0),
        .O(\pc[7]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc[8]_i_1 
       (.I0(\pc[14]_i_2_n_0 ),
        .I1(pc_s_reg[8]),
        .I2(\pc[28]_i_4_n_0 ),
        .I3(instruction_address_reg[8]),
        .I4(data1[8]),
        .I5(\pc[14]_i_3_n_0 ),
        .O(pc_s__0[8]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc[9]_i_1 
       (.I0(\pc[14]_i_2_n_0 ),
        .I1(pc_s_reg[9]),
        .I2(\pc[28]_i_4_n_0 ),
        .I3(instruction_address_reg[9]),
        .I4(data1[9]),
        .I5(\pc[14]_i_3_n_0 ),
        .O(pc_s__0[9]));
  FDRE \pc_reg[0] 
       (.C(clk),
        .CE(\pc[31]_i_1_n_0 ),
        .D(pc_s__0[0]),
        .Q(pc[0]),
        .R(reset));
  FDRE \pc_reg[10] 
       (.C(clk),
        .CE(\pc[31]_i_1_n_0 ),
        .D(pc_s__0[10]),
        .Q(pc[10]),
        .R(reset));
  FDRE \pc_reg[11] 
       (.C(clk),
        .CE(\pc[31]_i_1_n_0 ),
        .D(pc_s__0[11]),
        .Q(pc[11]),
        .R(reset));
  FDRE \pc_reg[12] 
       (.C(clk),
        .CE(\pc[31]_i_1_n_0 ),
        .D(pc_s__0[12]),
        .Q(pc[12]),
        .R(reset));
  FDRE \pc_reg[13] 
       (.C(clk),
        .CE(\pc[31]_i_1_n_0 ),
        .D(pc_s__0[13]),
        .Q(pc[13]),
        .R(reset));
  FDRE \pc_reg[14] 
       (.C(clk),
        .CE(\pc[31]_i_1_n_0 ),
        .D(pc_s__0[14]),
        .Q(pc[14]),
        .R(reset));
  FDRE \pc_reg[15] 
       (.C(clk),
        .CE(\pc[31]_i_1_n_0 ),
        .D(pc_s__0[15]),
        .Q(pc[15]),
        .R(reset));
  FDRE \pc_reg[16] 
       (.C(clk),
        .CE(\pc[31]_i_1_n_0 ),
        .D(pc_s__0[16]),
        .Q(pc[16]),
        .R(reset));
  FDRE \pc_reg[17] 
       (.C(clk),
        .CE(\pc[31]_i_1_n_0 ),
        .D(pc_s__0[17]),
        .Q(pc[17]),
        .R(reset));
  FDRE \pc_reg[18] 
       (.C(clk),
        .CE(\pc[31]_i_1_n_0 ),
        .D(pc_s__0[18]),
        .Q(pc[18]),
        .R(reset));
  FDRE \pc_reg[19] 
       (.C(clk),
        .CE(\pc[31]_i_1_n_0 ),
        .D(pc_s__0[19]),
        .Q(pc[19]),
        .R(reset));
  FDRE \pc_reg[1] 
       (.C(clk),
        .CE(\pc[31]_i_1_n_0 ),
        .D(pc_s__0[1]),
        .Q(pc[1]),
        .R(reset));
  FDRE \pc_reg[20] 
       (.C(clk),
        .CE(\pc[31]_i_1_n_0 ),
        .D(pc_s__0[20]),
        .Q(pc[20]),
        .R(reset));
  FDRE \pc_reg[21] 
       (.C(clk),
        .CE(\pc[31]_i_1_n_0 ),
        .D(pc_s__0[21]),
        .Q(pc[21]),
        .R(reset));
  FDRE \pc_reg[22] 
       (.C(clk),
        .CE(\pc[31]_i_1_n_0 ),
        .D(pc_s__0[22]),
        .Q(pc[22]),
        .R(reset));
  FDRE \pc_reg[23] 
       (.C(clk),
        .CE(\pc[31]_i_1_n_0 ),
        .D(pc_s__0[23]),
        .Q(pc[23]),
        .R(reset));
  FDRE \pc_reg[24] 
       (.C(clk),
        .CE(\pc[31]_i_1_n_0 ),
        .D(pc_s__0[24]),
        .Q(pc[24]),
        .R(reset));
  FDRE \pc_reg[25] 
       (.C(clk),
        .CE(\pc[31]_i_1_n_0 ),
        .D(pc_s__0[25]),
        .Q(pc[25]),
        .R(reset));
  FDRE \pc_reg[26] 
       (.C(clk),
        .CE(\pc[31]_i_1_n_0 ),
        .D(pc_s__0[26]),
        .Q(pc[26]),
        .R(reset));
  FDRE \pc_reg[27] 
       (.C(clk),
        .CE(\pc[31]_i_1_n_0 ),
        .D(pc_s__0[27]),
        .Q(pc[27]),
        .R(reset));
  FDRE \pc_reg[28] 
       (.C(clk),
        .CE(\pc[31]_i_1_n_0 ),
        .D(pc_s__0[28]),
        .Q(pc[28]),
        .R(reset));
  FDRE \pc_reg[29] 
       (.C(clk),
        .CE(\pc[31]_i_1_n_0 ),
        .D(pc_s__0[29]),
        .Q(pc[29]),
        .R(reset));
  FDRE \pc_reg[2] 
       (.C(clk),
        .CE(\pc[31]_i_1_n_0 ),
        .D(pc_s__0[2]),
        .Q(pc[2]),
        .R(reset));
  FDRE \pc_reg[30] 
       (.C(clk),
        .CE(\pc[31]_i_1_n_0 ),
        .D(pc_s__0[30]),
        .Q(pc[30]),
        .R(reset));
  FDRE \pc_reg[31] 
       (.C(clk),
        .CE(\pc[31]_i_1_n_0 ),
        .D(pc_s__0[31]),
        .Q(pc[31]),
        .R(reset));
  FDRE \pc_reg[3] 
       (.C(clk),
        .CE(\pc[31]_i_1_n_0 ),
        .D(pc_s__0[3]),
        .Q(pc[3]),
        .R(reset));
  FDRE \pc_reg[4] 
       (.C(clk),
        .CE(\pc[31]_i_1_n_0 ),
        .D(pc_s__0[4]),
        .Q(pc[4]),
        .R(reset));
  FDRE \pc_reg[5] 
       (.C(clk),
        .CE(\pc[31]_i_1_n_0 ),
        .D(pc_s__0[5]),
        .Q(pc[5]),
        .R(reset));
  FDRE \pc_reg[6] 
       (.C(clk),
        .CE(\pc[31]_i_1_n_0 ),
        .D(pc_s__0[6]),
        .Q(pc[6]),
        .R(reset));
  FDRE \pc_reg[7] 
       (.C(clk),
        .CE(\pc[31]_i_1_n_0 ),
        .D(pc_s__0[7]),
        .Q(pc[7]),
        .R(reset));
  FDRE \pc_reg[8] 
       (.C(clk),
        .CE(\pc[31]_i_1_n_0 ),
        .D(pc_s__0[8]),
        .Q(pc[8]),
        .R(reset));
  FDRE \pc_reg[9] 
       (.C(clk),
        .CE(\pc[31]_i_1_n_0 ),
        .D(pc_s__0[9]),
        .Q(pc[9]),
        .R(reset));
  LUT6 #(
    .INIT(64'h0000000100010116)) 
    pc_s
       (.I0(output_data_0_s_reg),
        .I1(output_data_1_s_reg),
        .I2(output_data_2_s_reg),
        .I3(output_data_3_s_reg),
        .I4(output_data_4_s_reg),
        .I5(temp_signal),
        .O(pc_s_n_0));
  FDRE \pc_s_reg_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[0]),
        .Q(pc_s_reg[0]),
        .R(reset));
  FDRE \pc_s_reg_reg[10] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[10]),
        .Q(pc_s_reg[10]),
        .R(reset));
  FDRE \pc_s_reg_reg[11] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[11]),
        .Q(pc_s_reg[11]),
        .R(reset));
  FDRE \pc_s_reg_reg[12] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[12]),
        .Q(pc_s_reg[12]),
        .R(reset));
  FDRE \pc_s_reg_reg[13] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[13]),
        .Q(pc_s_reg[13]),
        .R(reset));
  FDRE \pc_s_reg_reg[14] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[14]),
        .Q(pc_s_reg[14]),
        .R(reset));
  FDRE \pc_s_reg_reg[15] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[15]),
        .Q(pc_s_reg[15]),
        .R(reset));
  FDRE \pc_s_reg_reg[16] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[16]),
        .Q(pc_s_reg[16]),
        .R(reset));
  FDRE \pc_s_reg_reg[17] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[17]),
        .Q(pc_s_reg[17]),
        .R(reset));
  FDRE \pc_s_reg_reg[18] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[18]),
        .Q(pc_s_reg[18]),
        .R(reset));
  FDRE \pc_s_reg_reg[19] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[19]),
        .Q(pc_s_reg[19]),
        .R(reset));
  FDRE \pc_s_reg_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[1]),
        .Q(pc_s_reg[1]),
        .R(reset));
  FDRE \pc_s_reg_reg[20] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[20]),
        .Q(pc_s_reg[20]),
        .R(reset));
  FDRE \pc_s_reg_reg[21] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[21]),
        .Q(pc_s_reg[21]),
        .R(reset));
  FDRE \pc_s_reg_reg[22] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[22]),
        .Q(pc_s_reg[22]),
        .R(reset));
  FDRE \pc_s_reg_reg[23] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[23]),
        .Q(pc_s_reg[23]),
        .R(reset));
  FDRE \pc_s_reg_reg[24] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[24]),
        .Q(pc_s_reg[24]),
        .R(reset));
  FDRE \pc_s_reg_reg[25] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[25]),
        .Q(pc_s_reg[25]),
        .R(reset));
  FDRE \pc_s_reg_reg[26] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[26]),
        .Q(pc_s_reg[26]),
        .R(reset));
  FDRE \pc_s_reg_reg[27] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[27]),
        .Q(pc_s_reg[27]),
        .R(reset));
  FDRE \pc_s_reg_reg[28] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[28]),
        .Q(pc_s_reg[28]),
        .R(reset));
  FDRE \pc_s_reg_reg[29] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[29]),
        .Q(pc_s_reg[29]),
        .R(reset));
  FDRE \pc_s_reg_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[2]),
        .Q(pc_s_reg[2]),
        .R(reset));
  FDRE \pc_s_reg_reg[30] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[30]),
        .Q(pc_s_reg[30]),
        .R(reset));
  FDRE \pc_s_reg_reg[31] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[31]),
        .Q(pc_s_reg[31]),
        .R(reset));
  FDRE \pc_s_reg_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[3]),
        .Q(pc_s_reg[3]),
        .R(reset));
  FDRE \pc_s_reg_reg[4] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[4]),
        .Q(pc_s_reg[4]),
        .R(reset));
  FDRE \pc_s_reg_reg[5] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[5]),
        .Q(pc_s_reg[5]),
        .R(reset));
  FDRE \pc_s_reg_reg[6] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[6]),
        .Q(pc_s_reg[6]),
        .R(reset));
  FDRE \pc_s_reg_reg[7] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[7]),
        .Q(pc_s_reg[7]),
        .R(reset));
  FDRE \pc_s_reg_reg[8] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[8]),
        .Q(pc_s_reg[8]),
        .R(reset));
  FDRE \pc_s_reg_reg[9] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[9]),
        .Q(pc_s_reg[9]),
        .R(reset));
  LUT5 #(
    .INIT(32'h44444000)) 
    \state_reg[0]_i_1 
       (.I0(reset),
        .I1(enable_reg_reg),
        .I2(\state_reg[0]_i_2_n_0 ),
        .I3(start_b),
        .I4(\temp_state[0]_i_2__1_n_0 ),
        .O(\state_reg[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h44444445)) 
    \state_reg[0]_i_2 
       (.I0(a[6]),
        .I1(\state_reg_reg_n_0_[0] ),
        .I2(state_reg[1]),
        .I3(state_reg[3]),
        .I4(state_reg[2]),
        .O(\state_reg[0]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFEEEFFFF)) 
    \state_reg[1]_i_1 
       (.I0(\temp_state[1]_i_6_n_0 ),
        .I1(\temp_state[1]_i_5__0_n_0 ),
        .I2(start_b),
        .I3(\state_reg[1]_i_2_n_0 ),
        .I4(enable_reg_reg),
        .O(\state_reg[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hAA00AA02)) 
    \state_reg[1]_i_2 
       (.I0(a[6]),
        .I1(state_reg[1]),
        .I2(state_reg[3]),
        .I3(\state_reg_reg_n_0_[0] ),
        .I4(state_reg[2]),
        .O(\state_reg[1]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \state_reg[2]_i_1__0 
       (.I0(state_next[2]),
        .I1(enable_reg_reg),
        .O(\state_reg[2]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \state_reg[3]_i_1__0 
       (.I0(state_next[3]),
        .I1(enable_reg_reg),
        .O(\state_reg[3]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \state_reg_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(\state_reg[0]_i_1_n_0 ),
        .Q(\state_reg_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \state_reg_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\state_reg[1]_i_1_n_0 ),
        .Q(state_reg[1]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \state_reg_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(\state_reg[2]_i_1__0_n_0 ),
        .Q(state_reg[2]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \state_reg_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(\state_reg[3]_i_1__0_n_0 ),
        .Q(state_reg[3]),
        .R(reset));
  LUT6 #(
    .INIT(64'hAAAAAAAAAEAEAEAA)) 
    \temp_state[0]_i_1 
       (.I0(\temp_state[0]_i_2__1_n_0 ),
        .I1(start_b),
        .I2(a[6]),
        .I3(\temp_state[0]_i_3__1_n_0 ),
        .I4(\state_reg_reg_n_0_[0] ),
        .I5(\temp_state[0]_i_4__0_n_0 ),
        .O(state_next[0]));
  LUT6 #(
    .INIT(64'h0055222722332222)) 
    \temp_state[0]_i_2__1 
       (.I0(\temp_state[0]_i_4__0_n_0 ),
        .I1(a[6]),
        .I2(a[5]),
        .I3(\state_reg_reg_n_0_[0] ),
        .I4(state_reg[2]),
        .I5(state_reg[3]),
        .O(\temp_state[0]_i_2__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \temp_state[0]_i_3__1 
       (.I0(state_reg[2]),
        .I1(state_reg[3]),
        .O(\temp_state[0]_i_3__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h00007F00)) 
    \temp_state[0]_i_4__0 
       (.I0(\temp_state_reg_n_0_[0] ),
        .I1(state_reg[2]),
        .I2(state_reg[3]),
        .I3(state_reg[1]),
        .I4(\state_reg_reg_n_0_[0] ),
        .O(\temp_state[0]_i_4__0_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFF4000)) 
    \temp_state[1]_i_1 
       (.I0(\temp_state[1]_i_2__0_n_0 ),
        .I1(\temp_state[1]_i_3__0_n_0 ),
        .I2(a[6]),
        .I3(start_b),
        .I4(\temp_state[1]_i_5__0_n_0 ),
        .I5(\temp_state[1]_i_6_n_0 ),
        .O(state_next[1]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \temp_state[1]_i_2__0 
       (.I0(state_reg[2]),
        .I1(\state_reg_reg_n_0_[0] ),
        .O(\temp_state[1]_i_2__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'hF1)) 
    \temp_state[1]_i_3__0 
       (.I0(state_reg[1]),
        .I1(state_reg[3]),
        .I2(\state_reg_reg_n_0_[0] ),
        .O(\temp_state[1]_i_3__0_n_0 ));
  LUT6 #(
    .INIT(64'h0000808A00000000)) 
    \temp_state[1]_i_5__0 
       (.I0(state_reg[1]),
        .I1(\temp_state_reg_n_0_[1] ),
        .I2(state_reg[3]),
        .I3(a[6]),
        .I4(\state_reg_reg_n_0_[0] ),
        .I5(state_reg[2]),
        .O(\temp_state[1]_i_5__0_n_0 ));
  LUT6 #(
    .INIT(64'h0011001110102200)) 
    \temp_state[1]_i_6 
       (.I0(state_reg[2]),
        .I1(\state_reg_reg_n_0_[0] ),
        .I2(a[5]),
        .I3(a[6]),
        .I4(state_reg[3]),
        .I5(state_reg[1]),
        .O(\temp_state[1]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h00008FD00000F0F0)) 
    \temp_state[2]_i_1__0 
       (.I0(state_reg[3]),
        .I1(\temp_state_reg_n_0_[2] ),
        .I2(state_reg[2]),
        .I3(a[6]),
        .I4(\state_reg_reg_n_0_[0] ),
        .I5(state_reg[1]),
        .O(state_next[2]));
  LUT6 #(
    .INIT(64'h00000000F5FF8800)) 
    \temp_state[3]_i_1__0 
       (.I0(state_reg[1]),
        .I1(a[6]),
        .I2(\temp_state_reg_n_0_[3] ),
        .I3(state_reg[2]),
        .I4(state_reg[3]),
        .I5(\state_reg_reg_n_0_[0] ),
        .O(state_next[3]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'hFF40)) 
    \temp_state[3]_i_3 
       (.I0(state_reg[1]),
        .I1(state_reg[2]),
        .I2(state_reg[3]),
        .I3(\state_reg_reg_n_0_[0] ),
        .O(stop_bap));
  FDRE #(
    .INIT(1'b0)) 
    \temp_state_reg[0] 
       (.C(clk),
        .CE(enable_reg_reg_0),
        .D(state_next[0]),
        .Q(\temp_state_reg_n_0_[0] ),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \temp_state_reg[1] 
       (.C(clk),
        .CE(enable_reg_reg_0),
        .D(state_next[1]),
        .Q(\temp_state_reg_n_0_[1] ),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \temp_state_reg[2] 
       (.C(clk),
        .CE(enable_reg_reg_0),
        .D(state_next[2]),
        .Q(\temp_state_reg_n_0_[2] ),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \temp_state_reg[3] 
       (.C(clk),
        .CE(enable_reg_reg_0),
        .D(state_next[3]),
        .Q(\temp_state_reg_n_0_[3] ),
        .R(reset));
  FDRE w_en_reg
       (.C(clk),
        .CE(1'b1),
        .D(\pc[31]_i_1_n_0 ),
        .Q(w_en),
        .R(reset));
endmodule

module design_1_decodeur_traces_0_0_decode_i_sync
   (a,
    D,
    \temp_state_reg[3]_0 ,
    E,
    \state_reg_reg[0]_0 ,
    \temp_state_reg[1]_0 ,
    \temp_state_reg[1]_1 ,
    \temp_state_reg[0]_0 ,
    \state_reg_reg[0]_1 ,
    \state_reg_reg[0]_2 ,
    \state_reg_reg[0]_3 ,
    \temp_state_reg[0]_1 ,
    \output_data_3_reg[7]_0 ,
    \state_reg_reg[2]_0 ,
    next_state_logic__48,
    SR,
    w_ctxt_en,
    \temp_state_reg[0]_2 ,
    context_id,
    \instruction_address_reg_reg[31] ,
    reset,
    Q,
    clk,
    enable_reg_reg,
    state_next,
    \state_reg_reg[0]_4 ,
    \temp_state_reg[2]_0 ,
    enable_reg_reg_0);
  output [6:0]a;
  output [0:0]D;
  output [1:0]\temp_state_reg[3]_0 ;
  output [0:0]E;
  output \state_reg_reg[0]_0 ;
  output \temp_state_reg[1]_0 ;
  output \temp_state_reg[1]_1 ;
  output \temp_state_reg[0]_0 ;
  output \state_reg_reg[0]_1 ;
  output \state_reg_reg[0]_2 ;
  output \state_reg_reg[0]_3 ;
  output \temp_state_reg[0]_1 ;
  output [0:0]\output_data_3_reg[7]_0 ;
  output \state_reg_reg[2]_0 ;
  output [0:0]next_state_logic__48;
  output [0:0]SR;
  output [0:0]w_ctxt_en;
  output \temp_state_reg[0]_2 ;
  output [31:0]context_id;
  output [31:0]\instruction_address_reg_reg[31] ;
  input reset;
  input [7:0]Q;
  input clk;
  input enable_reg_reg;
  input [0:0]state_next;
  input \state_reg_reg[0]_4 ;
  input [0:0]\temp_state_reg[2]_0 ;
  input enable_reg_reg_0;

  wire [0:0]D;
  wire [0:0]E;
  wire [7:0]Q;
  wire [0:0]SR;
  wire [6:0]a;
  wire clk;
  wire [31:0]context_id;
  wire [1:0]count_ctxt;
  wire \count_ctxt[0]_i_1_n_0 ;
  wire \count_ctxt[1]_i_1_n_0 ;
  wire en_0;
  wire en_1;
  wire en_2;
  wire en_4;
  wire en_5;
  wire en_6;
  wire enable_reg_reg;
  wire enable_reg_reg_0;
  wire [31:0]\instruction_address_reg_reg[31] ;
  wire [0:0]next_state_logic__48;
  wire [0:0]\output_data_3_reg[7]_0 ;
  wire reset;
  wire [0:0]state_next;
  wire [3:1]state_next_0;
  wire [1:0]state_reg;
  wire \state_reg[3]_i_1__1_n_0 ;
  wire \state_reg[3]_i_2_n_0 ;
  wire \state_reg_reg[0]_0 ;
  wire \state_reg_reg[0]_1 ;
  wire \state_reg_reg[0]_2 ;
  wire \state_reg_reg[0]_3 ;
  wire \state_reg_reg[0]_4 ;
  wire \state_reg_reg[2]_0 ;
  wire \temp_state[0]_i_8_n_0 ;
  wire \temp_state[0]_i_9_n_0 ;
  wire \temp_state[1]_i_4__0_n_0 ;
  wire \temp_state[1]_i_5_n_0 ;
  wire \temp_state[2]_i_10_n_0 ;
  wire \temp_state[2]_i_2__0_n_0 ;
  wire \temp_state[2]_i_9_n_0 ;
  wire \temp_state_reg[0]_0 ;
  wire \temp_state_reg[0]_1 ;
  wire \temp_state_reg[0]_2 ;
  wire \temp_state_reg[1]_0 ;
  wire \temp_state_reg[1]_1 ;
  wire [0:0]\temp_state_reg[2]_0 ;
  wire [1:0]\temp_state_reg[3]_0 ;
  wire \temp_state_reg_n_0_[0] ;
  wire \temp_state_reg_n_0_[1] ;
  wire \temp_state_reg_n_0_[2] ;
  wire \temp_state_reg_n_0_[3] ;
  wire [0:0]w_ctxt_en;

  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT5 #(
    .INIT(32'h1DDDE222)) 
    \count_ctxt[0]_i_1 
       (.I0(\temp_state_reg[3]_0 [1]),
        .I1(state_reg[0]),
        .I2(state_reg[1]),
        .I3(\temp_state_reg[3]_0 [0]),
        .I4(count_ctxt[0]),
        .O(\count_ctxt[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h7F557FFF80AA8000)) 
    \count_ctxt[1]_i_1 
       (.I0(count_ctxt[0]),
        .I1(\temp_state_reg[3]_0 [0]),
        .I2(state_reg[1]),
        .I3(state_reg[0]),
        .I4(\temp_state_reg[3]_0 [1]),
        .I5(count_ctxt[1]),
        .O(\count_ctxt[1]_i_1_n_0 ));
  FDRE \count_ctxt_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(\count_ctxt[0]_i_1_n_0 ),
        .Q(count_ctxt[0]),
        .R(reset));
  FDRE \count_ctxt_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\count_ctxt[1]_i_1_n_0 ),
        .Q(count_ctxt[1]),
        .R(reset));
  FDRE \data_reg_s_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(Q[0]),
        .Q(D),
        .R(reset));
  FDRE \data_reg_s_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(Q[1]),
        .Q(a[0]),
        .R(reset));
  FDRE \data_reg_s_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(Q[2]),
        .Q(a[1]),
        .R(reset));
  FDRE \data_reg_s_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(Q[3]),
        .Q(a[2]),
        .R(reset));
  FDRE \data_reg_s_reg[4] 
       (.C(clk),
        .CE(1'b1),
        .D(Q[4]),
        .Q(a[3]),
        .R(reset));
  FDRE \data_reg_s_reg[5] 
       (.C(clk),
        .CE(1'b1),
        .D(Q[5]),
        .Q(a[4]),
        .R(reset));
  FDRE \data_reg_s_reg[6] 
       (.C(clk),
        .CE(1'b1),
        .D(Q[6]),
        .Q(a[5]),
        .R(reset));
  FDRE \data_reg_s_reg[7] 
       (.C(clk),
        .CE(1'b1),
        .D(Q[7]),
        .Q(a[6]),
        .R(reset));
  LUT3 #(
    .INIT(8'h02)) 
    enable_i_reg_i_1
       (.I0(\temp_state_reg[3]_0 [0]),
        .I1(state_reg[0]),
        .I2(state_reg[1]),
        .O(\output_data_3_reg[7]_0 ));
  LUT4 #(
    .INIT(16'h0100)) 
    \output_data_0[7]_i_1 
       (.I0(\temp_state_reg[3]_0 [0]),
        .I1(state_reg[1]),
        .I2(\temp_state_reg[3]_0 [1]),
        .I3(state_reg[0]),
        .O(en_0));
  FDRE \output_data_0_reg[0] 
       (.C(clk),
        .CE(en_0),
        .D(D),
        .Q(\instruction_address_reg_reg[31] [0]),
        .R(reset));
  FDRE \output_data_0_reg[1] 
       (.C(clk),
        .CE(en_0),
        .D(a[0]),
        .Q(\instruction_address_reg_reg[31] [1]),
        .R(reset));
  FDRE \output_data_0_reg[2] 
       (.C(clk),
        .CE(en_0),
        .D(a[1]),
        .Q(\instruction_address_reg_reg[31] [2]),
        .R(reset));
  FDRE \output_data_0_reg[3] 
       (.C(clk),
        .CE(en_0),
        .D(a[2]),
        .Q(\instruction_address_reg_reg[31] [3]),
        .R(reset));
  FDRE \output_data_0_reg[4] 
       (.C(clk),
        .CE(en_0),
        .D(a[3]),
        .Q(\instruction_address_reg_reg[31] [4]),
        .R(reset));
  FDRE \output_data_0_reg[5] 
       (.C(clk),
        .CE(en_0),
        .D(a[4]),
        .Q(\instruction_address_reg_reg[31] [5]),
        .R(reset));
  FDRE \output_data_0_reg[6] 
       (.C(clk),
        .CE(en_0),
        .D(a[5]),
        .Q(\instruction_address_reg_reg[31] [6]),
        .R(reset));
  FDRE \output_data_0_reg[7] 
       (.C(clk),
        .CE(en_0),
        .D(a[6]),
        .Q(\instruction_address_reg_reg[31] [7]),
        .R(reset));
  LUT3 #(
    .INIT(8'h04)) 
    \output_data_1[7]_i_1 
       (.I0(state_reg[0]),
        .I1(state_reg[1]),
        .I2(\temp_state_reg[3]_0 [0]),
        .O(en_1));
  FDRE \output_data_1_reg[0] 
       (.C(clk),
        .CE(en_1),
        .D(D),
        .Q(\instruction_address_reg_reg[31] [8]),
        .R(reset));
  FDRE \output_data_1_reg[1] 
       (.C(clk),
        .CE(en_1),
        .D(a[0]),
        .Q(\instruction_address_reg_reg[31] [9]),
        .R(reset));
  FDRE \output_data_1_reg[2] 
       (.C(clk),
        .CE(en_1),
        .D(a[1]),
        .Q(\instruction_address_reg_reg[31] [10]),
        .R(reset));
  FDRE \output_data_1_reg[3] 
       (.C(clk),
        .CE(en_1),
        .D(a[2]),
        .Q(\instruction_address_reg_reg[31] [11]),
        .R(reset));
  FDRE \output_data_1_reg[4] 
       (.C(clk),
        .CE(en_1),
        .D(a[3]),
        .Q(\instruction_address_reg_reg[31] [12]),
        .R(reset));
  FDRE \output_data_1_reg[5] 
       (.C(clk),
        .CE(en_1),
        .D(a[4]),
        .Q(\instruction_address_reg_reg[31] [13]),
        .R(reset));
  FDRE \output_data_1_reg[6] 
       (.C(clk),
        .CE(en_1),
        .D(a[5]),
        .Q(\instruction_address_reg_reg[31] [14]),
        .R(reset));
  FDRE \output_data_1_reg[7] 
       (.C(clk),
        .CE(en_1),
        .D(a[6]),
        .Q(\instruction_address_reg_reg[31] [15]),
        .R(reset));
  LUT3 #(
    .INIT(8'h08)) 
    \output_data_2[7]_i_1 
       (.I0(state_reg[0]),
        .I1(state_reg[1]),
        .I2(\temp_state_reg[3]_0 [0]),
        .O(en_2));
  FDRE \output_data_2_reg[0] 
       (.C(clk),
        .CE(en_2),
        .D(D),
        .Q(\instruction_address_reg_reg[31] [16]),
        .R(reset));
  FDRE \output_data_2_reg[1] 
       (.C(clk),
        .CE(en_2),
        .D(a[0]),
        .Q(\instruction_address_reg_reg[31] [17]),
        .R(reset));
  FDRE \output_data_2_reg[2] 
       (.C(clk),
        .CE(en_2),
        .D(a[1]),
        .Q(\instruction_address_reg_reg[31] [18]),
        .R(reset));
  FDRE \output_data_2_reg[3] 
       (.C(clk),
        .CE(en_2),
        .D(a[2]),
        .Q(\instruction_address_reg_reg[31] [19]),
        .R(reset));
  FDRE \output_data_2_reg[4] 
       (.C(clk),
        .CE(en_2),
        .D(a[3]),
        .Q(\instruction_address_reg_reg[31] [20]),
        .R(reset));
  FDRE \output_data_2_reg[5] 
       (.C(clk),
        .CE(en_2),
        .D(a[4]),
        .Q(\instruction_address_reg_reg[31] [21]),
        .R(reset));
  FDRE \output_data_2_reg[6] 
       (.C(clk),
        .CE(en_2),
        .D(a[5]),
        .Q(\instruction_address_reg_reg[31] [22]),
        .R(reset));
  FDRE \output_data_2_reg[7] 
       (.C(clk),
        .CE(en_2),
        .D(a[6]),
        .Q(\instruction_address_reg_reg[31] [23]),
        .R(reset));
  FDRE \output_data_3_reg[0] 
       (.C(clk),
        .CE(\output_data_3_reg[7]_0 ),
        .D(D),
        .Q(\instruction_address_reg_reg[31] [24]),
        .R(reset));
  FDRE \output_data_3_reg[1] 
       (.C(clk),
        .CE(\output_data_3_reg[7]_0 ),
        .D(a[0]),
        .Q(\instruction_address_reg_reg[31] [25]),
        .R(reset));
  FDRE \output_data_3_reg[2] 
       (.C(clk),
        .CE(\output_data_3_reg[7]_0 ),
        .D(a[1]),
        .Q(\instruction_address_reg_reg[31] [26]),
        .R(reset));
  FDRE \output_data_3_reg[3] 
       (.C(clk),
        .CE(\output_data_3_reg[7]_0 ),
        .D(a[2]),
        .Q(\instruction_address_reg_reg[31] [27]),
        .R(reset));
  FDRE \output_data_3_reg[4] 
       (.C(clk),
        .CE(\output_data_3_reg[7]_0 ),
        .D(a[3]),
        .Q(\instruction_address_reg_reg[31] [28]),
        .R(reset));
  FDRE \output_data_3_reg[5] 
       (.C(clk),
        .CE(\output_data_3_reg[7]_0 ),
        .D(a[4]),
        .Q(\instruction_address_reg_reg[31] [29]),
        .R(reset));
  FDRE \output_data_3_reg[6] 
       (.C(clk),
        .CE(\output_data_3_reg[7]_0 ),
        .D(a[5]),
        .Q(\instruction_address_reg_reg[31] [30]),
        .R(reset));
  FDRE \output_data_3_reg[7] 
       (.C(clk),
        .CE(\output_data_3_reg[7]_0 ),
        .D(a[6]),
        .Q(\instruction_address_reg_reg[31] [31]),
        .R(reset));
  LUT6 #(
    .INIT(64'h00A000A000F100A0)) 
    \output_data_4[7]_i_1 
       (.I0(\temp_state_reg[3]_0 [0]),
        .I1(count_ctxt[0]),
        .I2(state_reg[1]),
        .I3(state_reg[0]),
        .I4(\temp_state_reg[3]_0 [1]),
        .I5(count_ctxt[1]),
        .O(en_4));
  FDRE \output_data_4_reg[0] 
       (.C(clk),
        .CE(en_4),
        .D(D),
        .Q(context_id[0]),
        .R(reset));
  FDRE \output_data_4_reg[1] 
       (.C(clk),
        .CE(en_4),
        .D(a[0]),
        .Q(context_id[1]),
        .R(reset));
  FDRE \output_data_4_reg[2] 
       (.C(clk),
        .CE(en_4),
        .D(a[1]),
        .Q(context_id[2]),
        .R(reset));
  FDRE \output_data_4_reg[3] 
       (.C(clk),
        .CE(en_4),
        .D(a[2]),
        .Q(context_id[3]),
        .R(reset));
  FDRE \output_data_4_reg[4] 
       (.C(clk),
        .CE(en_4),
        .D(a[3]),
        .Q(context_id[4]),
        .R(reset));
  FDRE \output_data_4_reg[5] 
       (.C(clk),
        .CE(en_4),
        .D(a[4]),
        .Q(context_id[5]),
        .R(reset));
  FDRE \output_data_4_reg[6] 
       (.C(clk),
        .CE(en_4),
        .D(a[5]),
        .Q(context_id[6]),
        .R(reset));
  FDRE \output_data_4_reg[7] 
       (.C(clk),
        .CE(en_4),
        .D(a[6]),
        .Q(context_id[7]),
        .R(reset));
  LUT6 #(
    .INIT(64'h888888880F000000)) 
    \output_data_5[7]_i_1 
       (.I0(\temp_state_reg[3]_0 [0]),
        .I1(state_reg[1]),
        .I2(count_ctxt[1]),
        .I3(count_ctxt[0]),
        .I4(\temp_state_reg[3]_0 [1]),
        .I5(state_reg[0]),
        .O(en_5));
  FDRE \output_data_5_reg[0] 
       (.C(clk),
        .CE(en_5),
        .D(D),
        .Q(context_id[8]),
        .R(reset));
  FDRE \output_data_5_reg[1] 
       (.C(clk),
        .CE(en_5),
        .D(a[0]),
        .Q(context_id[9]),
        .R(reset));
  FDRE \output_data_5_reg[2] 
       (.C(clk),
        .CE(en_5),
        .D(a[1]),
        .Q(context_id[10]),
        .R(reset));
  FDRE \output_data_5_reg[3] 
       (.C(clk),
        .CE(en_5),
        .D(a[2]),
        .Q(context_id[11]),
        .R(reset));
  FDRE \output_data_5_reg[4] 
       (.C(clk),
        .CE(en_5),
        .D(a[3]),
        .Q(context_id[12]),
        .R(reset));
  FDRE \output_data_5_reg[5] 
       (.C(clk),
        .CE(en_5),
        .D(a[4]),
        .Q(context_id[13]),
        .R(reset));
  FDRE \output_data_5_reg[6] 
       (.C(clk),
        .CE(en_5),
        .D(a[5]),
        .Q(context_id[14]),
        .R(reset));
  FDRE \output_data_5_reg[7] 
       (.C(clk),
        .CE(en_5),
        .D(a[6]),
        .Q(context_id[15]),
        .R(reset));
  LUT4 #(
    .INIT(16'h0400)) 
    \output_data_6[7]_i_1 
       (.I0(count_ctxt[0]),
        .I1(count_ctxt[1]),
        .I2(state_reg[0]),
        .I3(\temp_state_reg[3]_0 [1]),
        .O(en_6));
  FDRE \output_data_6_reg[0] 
       (.C(clk),
        .CE(en_6),
        .D(D),
        .Q(context_id[16]),
        .R(reset));
  FDRE \output_data_6_reg[1] 
       (.C(clk),
        .CE(en_6),
        .D(a[0]),
        .Q(context_id[17]),
        .R(reset));
  FDRE \output_data_6_reg[2] 
       (.C(clk),
        .CE(en_6),
        .D(a[1]),
        .Q(context_id[18]),
        .R(reset));
  FDRE \output_data_6_reg[3] 
       (.C(clk),
        .CE(en_6),
        .D(a[2]),
        .Q(context_id[19]),
        .R(reset));
  FDRE \output_data_6_reg[4] 
       (.C(clk),
        .CE(en_6),
        .D(a[3]),
        .Q(context_id[20]),
        .R(reset));
  FDRE \output_data_6_reg[5] 
       (.C(clk),
        .CE(en_6),
        .D(a[4]),
        .Q(context_id[21]),
        .R(reset));
  FDRE \output_data_6_reg[6] 
       (.C(clk),
        .CE(en_6),
        .D(a[5]),
        .Q(context_id[22]),
        .R(reset));
  FDRE \output_data_6_reg[7] 
       (.C(clk),
        .CE(en_6),
        .D(a[6]),
        .Q(context_id[23]),
        .R(reset));
  FDRE \output_data_7_reg[0] 
       (.C(clk),
        .CE(w_ctxt_en),
        .D(D),
        .Q(context_id[24]),
        .R(reset));
  FDRE \output_data_7_reg[1] 
       (.C(clk),
        .CE(w_ctxt_en),
        .D(a[0]),
        .Q(context_id[25]),
        .R(reset));
  FDRE \output_data_7_reg[2] 
       (.C(clk),
        .CE(w_ctxt_en),
        .D(a[1]),
        .Q(context_id[26]),
        .R(reset));
  FDRE \output_data_7_reg[3] 
       (.C(clk),
        .CE(w_ctxt_en),
        .D(a[2]),
        .Q(context_id[27]),
        .R(reset));
  FDRE \output_data_7_reg[4] 
       (.C(clk),
        .CE(w_ctxt_en),
        .D(a[3]),
        .Q(context_id[28]),
        .R(reset));
  FDRE \output_data_7_reg[5] 
       (.C(clk),
        .CE(w_ctxt_en),
        .D(a[4]),
        .Q(context_id[29]),
        .R(reset));
  FDRE \output_data_7_reg[6] 
       (.C(clk),
        .CE(w_ctxt_en),
        .D(a[5]),
        .Q(context_id[30]),
        .R(reset));
  FDRE \output_data_7_reg[7] 
       (.C(clk),
        .CE(w_ctxt_en),
        .D(a[6]),
        .Q(context_id[31]),
        .R(reset));
  LUT2 #(
    .INIT(4'hB)) 
    \state_reg[2]_i_1 
       (.I0(reset),
        .I1(enable_reg_reg),
        .O(SR));
  LUT6 #(
    .INIT(64'hAA2A002AFFFFFFFF)) 
    \state_reg[3]_i_1__1 
       (.I0(\temp_state_reg[3]_0 [1]),
        .I1(count_ctxt[0]),
        .I2(count_ctxt[1]),
        .I3(\state_reg[3]_i_2_n_0 ),
        .I4(\temp_state_reg_n_0_[3] ),
        .I5(enable_reg_reg),
        .O(\state_reg[3]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \state_reg[3]_i_2 
       (.I0(state_reg[0]),
        .I1(\temp_state_reg[3]_0 [1]),
        .I2(\temp_state_reg[3]_0 [0]),
        .O(\state_reg[3]_i_2_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \state_reg_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(enable_reg_reg_0),
        .Q(state_reg[0]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \state_reg_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(state_next_0[1]),
        .Q(state_reg[1]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \state_reg_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(state_next_0[2]),
        .Q(\temp_state_reg[3]_0 [0]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \state_reg_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(\state_reg[3]_i_1__1_n_0 ),
        .Q(\temp_state_reg[3]_0 [1]),
        .R(reset));
  LUT4 #(
    .INIT(16'h0001)) 
    \temp_state[0]_i_2 
       (.I0(state_reg[0]),
        .I1(\temp_state_reg[3]_0 [0]),
        .I2(\temp_state_reg[3]_0 [1]),
        .I3(state_next),
        .O(\temp_state_reg[0]_0 ));
  LUT6 #(
    .INIT(64'h0101FF5501010155)) 
    \temp_state[0]_i_3 
       (.I0(\temp_state[0]_i_8_n_0 ),
        .I1(a[2]),
        .I2(\temp_state[1]_i_5_n_0 ),
        .I3(a[0]),
        .I4(a[6]),
        .I5(\temp_state[0]_i_9_n_0 ),
        .O(\state_reg_reg[0]_0 ));
  LUT6 #(
    .INIT(64'h4004400400044004)) 
    \temp_state[0]_i_3__0 
       (.I0(\temp_state_reg[3]_0 [1]),
        .I1(\temp_state_reg[3]_0 [0]),
        .I2(state_reg[1]),
        .I3(state_reg[0]),
        .I4(count_ctxt[0]),
        .I5(count_ctxt[1]),
        .O(\temp_state_reg[0]_1 ));
  LUT5 #(
    .INIT(32'hA003A000)) 
    \temp_state[0]_i_4__1 
       (.I0(\temp_state_reg_n_0_[0] ),
        .I1(\temp_state_reg[3]_0 [0]),
        .I2(state_reg[0]),
        .I3(\temp_state_reg[3]_0 [1]),
        .I4(state_reg[1]),
        .O(\temp_state_reg[0]_2 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT5 #(
    .INIT(32'hAAAAAAA8)) 
    \temp_state[0]_i_5 
       (.I0(\state_reg_reg[0]_4 ),
        .I1(state_reg[1]),
        .I2(state_reg[0]),
        .I3(\temp_state_reg[3]_0 [0]),
        .I4(\temp_state_reg[3]_0 [1]),
        .O(\state_reg_reg[0]_1 ));
  LUT6 #(
    .INIT(64'h0100010001000001)) 
    \temp_state[0]_i_8 
       (.I0(a[2]),
        .I1(a[1]),
        .I2(a[3]),
        .I3(a[6]),
        .I4(a[4]),
        .I5(a[5]),
        .O(\temp_state[0]_i_8_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFBFFF)) 
    \temp_state[0]_i_9 
       (.I0(a[1]),
        .I1(a[4]),
        .I2(a[5]),
        .I3(a[3]),
        .I4(a[2]),
        .O(\temp_state[0]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hCCA00000CC0F00F0)) 
    \temp_state[1]_i_1__1 
       (.I0(\temp_state[2]_i_2__0_n_0 ),
        .I1(\temp_state_reg_n_0_[1] ),
        .I2(state_reg[1]),
        .I3(\temp_state_reg[3]_0 [1]),
        .I4(state_reg[0]),
        .I5(\temp_state_reg[3]_0 [0]),
        .O(state_next_0[1]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT5 #(
    .INIT(32'h41000000)) 
    \temp_state[1]_i_2 
       (.I0(D),
        .I1(a[5]),
        .I2(a[4]),
        .I3(a[2]),
        .I4(\temp_state[1]_i_4__0_n_0 ),
        .O(\temp_state_reg[1]_1 ));
  LUT5 #(
    .INIT(32'h44444440)) 
    \temp_state[1]_i_3 
       (.I0(D),
        .I1(a[6]),
        .I2(a[4]),
        .I3(a[5]),
        .I4(\temp_state[1]_i_5_n_0 ),
        .O(\temp_state_reg[1]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT4 #(
    .INIT(16'h2001)) 
    \temp_state[1]_i_4__0 
       (.I0(a[4]),
        .I1(a[3]),
        .I2(a[1]),
        .I3(a[0]),
        .O(\temp_state[1]_i_4__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \temp_state[1]_i_5 
       (.I0(a[3]),
        .I1(a[1]),
        .I2(a[0]),
        .O(\temp_state[1]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \temp_state[2]_i_10 
       (.I0(a[0]),
        .I1(a[6]),
        .O(\temp_state[2]_i_10_n_0 ));
  LUT4 #(
    .INIT(16'h0001)) 
    \temp_state[2]_i_11 
       (.I0(Q[7]),
        .I1(Q[6]),
        .I2(Q[5]),
        .I3(Q[4]),
        .O(\state_reg_reg[0]_3 ));
  LUT4 #(
    .INIT(16'h0001)) 
    \temp_state[2]_i_12 
       (.I0(Q[1]),
        .I1(Q[0]),
        .I2(Q[3]),
        .I3(Q[2]),
        .O(\state_reg_reg[0]_2 ));
  LUT6 #(
    .INIT(64'hA0A0A0A0C00FF000)) 
    \temp_state[2]_i_1__1 
       (.I0(\temp_state_reg_n_0_[2] ),
        .I1(\temp_state[2]_i_2__0_n_0 ),
        .I2(state_reg[0]),
        .I3(state_reg[1]),
        .I4(\temp_state_reg[3]_0 [0]),
        .I5(\temp_state_reg[3]_0 [1]),
        .O(state_next_0[2]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \temp_state[2]_i_2__0 
       (.I0(count_ctxt[1]),
        .I1(count_ctxt[0]),
        .O(\temp_state[2]_i_2__0_n_0 ));
  LUT6 #(
    .INIT(64'hAAAEABAAAAAAAAAA)) 
    \temp_state[2]_i_4 
       (.I0(D),
        .I1(a[1]),
        .I2(\temp_state[2]_i_9_n_0 ),
        .I3(a[3]),
        .I4(a[2]),
        .I5(\temp_state[2]_i_10_n_0 ),
        .O(next_state_logic__48));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \temp_state[2]_i_8 
       (.I0(state_reg[1]),
        .I1(state_reg[0]),
        .O(\state_reg_reg[2]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \temp_state[2]_i_9 
       (.I0(a[4]),
        .I1(a[5]),
        .O(\temp_state[2]_i_9_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \temp_state[3]_i_1 
       (.I0(enable_reg_reg),
        .O(E));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT5 #(
    .INIT(32'h80B0B0B0)) 
    \temp_state[3]_i_1__1 
       (.I0(\temp_state_reg_n_0_[3] ),
        .I1(state_reg[0]),
        .I2(\temp_state_reg[3]_0 [1]),
        .I3(count_ctxt[1]),
        .I4(count_ctxt[0]),
        .O(state_next_0[3]));
  FDRE #(
    .INIT(1'b0)) 
    \temp_state_reg[0] 
       (.C(clk),
        .CE(E),
        .D(\temp_state_reg[2]_0 ),
        .Q(\temp_state_reg_n_0_[0] ),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \temp_state_reg[1] 
       (.C(clk),
        .CE(E),
        .D(state_next_0[1]),
        .Q(\temp_state_reg_n_0_[1] ),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \temp_state_reg[2] 
       (.C(clk),
        .CE(E),
        .D(state_next_0[2]),
        .Q(\temp_state_reg_n_0_[2] ),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \temp_state_reg[3] 
       (.C(clk),
        .CE(E),
        .D(state_next_0[3]),
        .Q(\temp_state_reg_n_0_[3] ),
        .R(reset));
  LUT4 #(
    .INIT(16'h4000)) 
    w_ctxt_en_INST_0
       (.I0(state_reg[0]),
        .I1(\temp_state_reg[3]_0 [1]),
        .I2(count_ctxt[1]),
        .I3(count_ctxt[0]),
        .O(w_ctxt_en));
endmodule

module design_1_decodeur_traces_0_0_decodeur_traces
   (context_id,
    E,
    w_en,
    pc,
    reset,
    trace_data,
    clk,
    enable);
  output [31:0]context_id;
  output [0:0]E;
  output w_en;
  output [31:0]pc;
  input reset;
  input [7:0]trace_data;
  input clk;
  input enable;

  wire [0:0]E;
  wire [0:0]a;
  wire chemin_n_11;
  wire chemin_n_12;
  wire chemin_n_13;
  wire chemin_n_14;
  wire chemin_n_16;
  wire chemin_n_18;
  wire chemin_n_4;
  wire chemin_n_5;
  wire chemin_n_6;
  wire chemin_n_7;
  wire chemin_n_8;
  wire chemin_n_9;
  wire clk;
  wire [31:0]context_id;
  wire [7:0]data_in;
  wire enable;
  wire enable_reg_reg_n_0;
  wire fsm_n_0;
  wire fsm_n_4;
  wire [2:2]next_state_logic__48;
  wire [31:0]pc;
  wire reset;
  wire start_b;
  wire [3:3]state_next;
  wire stop_bap;
  wire [7:0]trace_data;
  wire [0:0]\u_decode_i_sync/state_next ;
  wire [3:2]\u_decode_i_sync/state_reg ;
  wire w_en;

  design_1_decodeur_traces_0_0_datapath chemin
       (.D(\u_decode_i_sync/state_next ),
        .E(chemin_n_4),
        .Q(data_in),
        .SR(chemin_n_16),
        .clk(clk),
        .context_id(context_id),
        .enable_reg_reg(enable_reg_reg_n_0),
        .enable_reg_reg_0(fsm_n_0),
        .next_state_logic__48(next_state_logic__48),
        .\output_data_3_reg[0] (a),
        .pc(pc),
        .reset(reset),
        .start_b(start_b),
        .state_next(state_next),
        .\state_reg_reg[0] (chemin_n_5),
        .\state_reg_reg[0]_0 (chemin_n_9),
        .\state_reg_reg[0]_1 (chemin_n_11),
        .\state_reg_reg[0]_2 (chemin_n_12),
        .\state_reg_reg[0]_3 (fsm_n_4),
        .\state_reg_reg[2] (chemin_n_14),
        .stop_bap(stop_bap),
        .\temp_state_reg[0] (chemin_n_8),
        .\temp_state_reg[0]_0 (chemin_n_13),
        .\temp_state_reg[0]_1 (chemin_n_18),
        .\temp_state_reg[1] (chemin_n_6),
        .\temp_state_reg[1]_0 (chemin_n_7),
        .\temp_state_reg[3] (\u_decode_i_sync/state_reg ),
        .w_ctxt_en(E),
        .w_en(w_en));
  FDRE \data_in_reg_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(trace_data[0]),
        .Q(data_in[0]),
        .R(reset));
  FDRE \data_in_reg_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(trace_data[1]),
        .Q(data_in[1]),
        .R(reset));
  FDRE \data_in_reg_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(trace_data[2]),
        .Q(data_in[2]),
        .R(reset));
  FDRE \data_in_reg_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(trace_data[3]),
        .Q(data_in[3]),
        .R(reset));
  FDRE \data_in_reg_reg[4] 
       (.C(clk),
        .CE(1'b1),
        .D(trace_data[4]),
        .Q(data_in[4]),
        .R(reset));
  FDRE \data_in_reg_reg[5] 
       (.C(clk),
        .CE(1'b1),
        .D(trace_data[5]),
        .Q(data_in[5]),
        .R(reset));
  FDRE \data_in_reg_reg[6] 
       (.C(clk),
        .CE(1'b1),
        .D(trace_data[6]),
        .Q(data_in[6]),
        .R(reset));
  FDRE \data_in_reg_reg[7] 
       (.C(clk),
        .CE(1'b1),
        .D(trace_data[7]),
        .Q(data_in[7]),
        .R(reset));
  FDRE enable_reg_reg
       (.C(clk),
        .CE(1'b1),
        .D(enable),
        .Q(enable_reg_reg_n_0),
        .R(reset));
  design_1_decodeur_traces_0_0_pft_decoder_v2 fsm
       (.D(\u_decode_i_sync/state_next ),
        .E(chemin_n_4),
        .SR({reset,chemin_n_16}),
        .clk(clk),
        .\data_in_reg_reg[1] (chemin_n_11),
        .\data_in_reg_reg[7] (chemin_n_12),
        .\data_reg_s_reg[0] (chemin_n_7),
        .\data_reg_s_reg[0]_0 (chemin_n_6),
        .\data_reg_s_reg[0]_1 (a),
        .\data_reg_s_reg[3] (chemin_n_5),
        .enable_reg_reg(enable_reg_reg_n_0),
        .next_state_logic__48(next_state_logic__48),
        .start_b(start_b),
        .\state_reg_reg[0]_0 (fsm_n_0),
        .\state_reg_reg[0]_1 (chemin_n_8),
        .\state_reg_reg[1]_0 (chemin_n_9),
        .\state_reg_reg[1]_1 (chemin_n_14),
        .\state_reg_reg[2]_0 (fsm_n_4),
        .\state_reg_reg[3]_0 (chemin_n_13),
        .\state_reg_reg[3]_1 (\u_decode_i_sync/state_reg ),
        .stop_bap(stop_bap),
        .\temp_state_reg[0]_0 (chemin_n_18),
        .\temp_state_reg[3]_0 (state_next));
endmodule

(* CHECK_LICENSE_TYPE = "design_1_decodeur_traces_1_0,decodeur_traces,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "decodeur_traces,Vivado 2018.2" *) 
(* NotValidForBitStream *)
module design_1_decodeur_traces_0_0
   (clk,
    reset,
    enable,
    trace_data,
    pc,
    w_en,
    w_ctxt_en,
    fifo_overflow,
    waypoint_address,
    context_id,
    waypoint_address_en);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk, ASSOCIATED_RESET reset, FREQ_HZ 2.5e+08, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK1" *) input clk;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 reset RST" *) (* x_interface_parameter = "XIL_INTERFACENAME reset, POLARITY ACTIVE_LOW" *) input reset;
  input enable;
  input [7:0]trace_data;
  output [31:0]pc;
  output w_en;
  output w_ctxt_en;
  output fifo_overflow;
  output [31:0]waypoint_address;
  output [31:0]context_id;
  output waypoint_address_en;

  wire \<const0> ;
  wire clk;
  wire [31:0]context_id;
  wire enable;
  wire [31:0]pc;
  wire reset;
  wire [7:0]trace_data;
  wire w_ctxt_en;
  wire w_en;

  assign fifo_overflow = \<const0> ;
  GND GND
       (.G(\<const0> ));
  design_1_decodeur_traces_0_0_decodeur_traces U0
       (.E(w_ctxt_en),
        .clk(clk),
        .context_id(context_id),
        .enable(enable),
        .pc(pc),
        .reset(reset),
        .trace_data(trace_data),
        .w_en(w_en));
endmodule

module design_1_decodeur_traces_0_0_pft_decoder_v2
   (\state_reg_reg[0]_0 ,
    D,
    \temp_state_reg[3]_0 ,
    start_b,
    \state_reg_reg[2]_0 ,
    enable_reg_reg,
    \state_reg_reg[0]_1 ,
    \state_reg_reg[3]_0 ,
    \temp_state_reg[0]_0 ,
    \data_reg_s_reg[3] ,
    \state_reg_reg[1]_0 ,
    \data_reg_s_reg[0] ,
    \data_reg_s_reg[0]_0 ,
    stop_bap,
    next_state_logic__48,
    \state_reg_reg[3]_1 ,
    \state_reg_reg[1]_1 ,
    \data_reg_s_reg[0]_1 ,
    \data_in_reg_reg[1] ,
    \data_in_reg_reg[7] ,
    SR,
    E,
    clk);
  output \state_reg_reg[0]_0 ;
  output [0:0]D;
  output [0:0]\temp_state_reg[3]_0 ;
  output start_b;
  output \state_reg_reg[2]_0 ;
  input enable_reg_reg;
  input \state_reg_reg[0]_1 ;
  input \state_reg_reg[3]_0 ;
  input \temp_state_reg[0]_0 ;
  input \data_reg_s_reg[3] ;
  input \state_reg_reg[1]_0 ;
  input \data_reg_s_reg[0] ;
  input \data_reg_s_reg[0]_0 ;
  input stop_bap;
  input [0:0]next_state_logic__48;
  input [1:0]\state_reg_reg[3]_1 ;
  input \state_reg_reg[1]_1 ;
  input [0:0]\data_reg_s_reg[0]_1 ;
  input \data_in_reg_reg[1] ;
  input \data_in_reg_reg[7] ;
  input [1:0]SR;
  input [0:0]E;
  input clk;

  wire [0:0]D;
  wire [0:0]E;
  wire [1:0]SR;
  wire clk;
  wire \data_in_reg_reg[1] ;
  wire \data_in_reg_reg[7] ;
  wire \data_reg_s_reg[0] ;
  wire \data_reg_s_reg[0]_0 ;
  wire [0:0]\data_reg_s_reg[0]_1 ;
  wire \data_reg_s_reg[3] ;
  wire enable_reg_reg;
  wire [0:0]next_state_logic__48;
  wire start_b;
  wire [2:0]state_next;
  wire \state_reg[1]_i_1__0_n_0 ;
  wire \state_reg[3]_i_1_n_0 ;
  wire \state_reg_reg[0]_0 ;
  wire \state_reg_reg[0]_1 ;
  wire \state_reg_reg[1]_0 ;
  wire \state_reg_reg[1]_1 ;
  wire \state_reg_reg[2]_0 ;
  wire \state_reg_reg[3]_0 ;
  wire [1:0]\state_reg_reg[3]_1 ;
  wire \state_reg_reg_n_0_[0] ;
  wire \state_reg_reg_n_0_[1] ;
  wire \state_reg_reg_n_0_[2] ;
  wire \state_reg_reg_n_0_[3] ;
  wire stop_bap;
  wire [3:0]temp_state;
  wire \temp_state[0]_i_2__0_n_0 ;
  wire \temp_state[0]_i_4_n_0 ;
  wire \temp_state[0]_i_6_n_0 ;
  wire \temp_state[0]_i_7_n_0 ;
  wire \temp_state[2]_i_2_n_0 ;
  wire \temp_state[2]_i_3_n_0 ;
  wire \temp_state[2]_i_5_n_0 ;
  wire \temp_state[2]_i_6_n_0 ;
  wire \temp_state_reg[0]_0 ;
  wire [0:0]\temp_state_reg[3]_0 ;

  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \state_reg[0]_i_1__0 
       (.I0(D),
        .I1(enable_reg_reg),
        .O(\state_reg_reg[0]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \state_reg[1]_i_1__0 
       (.I0(state_next[1]),
        .I1(enable_reg_reg),
        .O(\state_reg[1]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \state_reg[3]_i_1 
       (.I0(\temp_state_reg[3]_0 ),
        .I1(enable_reg_reg),
        .O(\state_reg[3]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \state_reg_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(state_next[0]),
        .Q(\state_reg_reg_n_0_[0] ),
        .R(SR[0]));
  FDRE #(
    .INIT(1'b0)) 
    \state_reg_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\state_reg[1]_i_1__0_n_0 ),
        .Q(\state_reg_reg_n_0_[1] ),
        .R(SR[1]));
  FDRE #(
    .INIT(1'b0)) 
    \state_reg_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(state_next[2]),
        .Q(\state_reg_reg_n_0_[2] ),
        .R(SR[0]));
  FDRE #(
    .INIT(1'b0)) 
    \state_reg_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(\state_reg[3]_i_1_n_0 ),
        .Q(\state_reg_reg_n_0_[3] ),
        .R(SR[1]));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFF9000)) 
    \temp_state[0]_i_1__0 
       (.I0(state_next[0]),
        .I1(state_next[2]),
        .I2(\state_reg_reg[0]_1 ),
        .I3(state_next[1]),
        .I4(\state_reg_reg[3]_0 ),
        .I5(\temp_state_reg[0]_0 ),
        .O(D));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFF2F2F2)) 
    \temp_state[0]_i_1__1 
       (.I0(\temp_state[0]_i_2__0_n_0 ),
        .I1(\data_reg_s_reg[3] ),
        .I2(\temp_state[0]_i_4_n_0 ),
        .I3(\state_reg_reg[1]_0 ),
        .I4(\temp_state[0]_i_6_n_0 ),
        .I5(\temp_state[0]_i_7_n_0 ),
        .O(state_next[0]));
  LUT6 #(
    .INIT(64'h00000000FFF7FEF3)) 
    \temp_state[0]_i_2__0 
       (.I0(\state_reg_reg_n_0_[1] ),
        .I1(\state_reg_reg_n_0_[3] ),
        .I2(\state_reg_reg_n_0_[0] ),
        .I3(\state_reg_reg_n_0_[2] ),
        .I4(stop_bap),
        .I5(\data_reg_s_reg[0]_1 ),
        .O(\temp_state[0]_i_2__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT4 #(
    .INIT(16'h0148)) 
    \temp_state[0]_i_4 
       (.I0(\state_reg_reg_n_0_[2] ),
        .I1(\state_reg_reg_n_0_[0] ),
        .I2(\state_reg_reg_n_0_[3] ),
        .I3(\state_reg_reg_n_0_[1] ),
        .O(\temp_state[0]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT4 #(
    .INIT(16'h2502)) 
    \temp_state[0]_i_6 
       (.I0(\state_reg_reg_n_0_[1] ),
        .I1(\state_reg_reg_n_0_[3] ),
        .I2(\state_reg_reg_n_0_[2] ),
        .I3(\state_reg_reg_n_0_[0] ),
        .O(\temp_state[0]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h00000000888800F0)) 
    \temp_state[0]_i_7 
       (.I0(\data_in_reg_reg[1] ),
        .I1(\data_in_reg_reg[7] ),
        .I2(temp_state[0]),
        .I3(\temp_state[2]_i_6_n_0 ),
        .I4(\temp_state[0]_i_6_n_0 ),
        .I5(\state_reg_reg[2]_0 ),
        .O(\temp_state[0]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFF8F8F8F8F8)) 
    \temp_state[1]_i_1__0 
       (.I0(\temp_state[2]_i_2_n_0 ),
        .I1(temp_state[1]),
        .I2(\temp_state[2]_i_3_n_0 ),
        .I3(\data_reg_s_reg[0] ),
        .I4(\data_reg_s_reg[0]_0 ),
        .I5(\temp_state[2]_i_5_n_0 ),
        .O(state_next[1]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT4 #(
    .INIT(16'h0014)) 
    \temp_state[1]_i_4 
       (.I0(state_next[0]),
        .I1(state_next[2]),
        .I2(\temp_state_reg[3]_0 ),
        .I3(state_next[1]),
        .O(start_b));
  LUT5 #(
    .INIT(32'hFFF8F8F8)) 
    \temp_state[2]_i_1 
       (.I0(\temp_state[2]_i_2_n_0 ),
        .I1(temp_state[2]),
        .I2(\temp_state[2]_i_3_n_0 ),
        .I3(next_state_logic__48),
        .I4(\temp_state[2]_i_5_n_0 ),
        .O(state_next[2]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT4 #(
    .INIT(16'h0008)) 
    \temp_state[2]_i_2 
       (.I0(\state_reg_reg_n_0_[1] ),
        .I1(\state_reg_reg_n_0_[3] ),
        .I2(\state_reg_reg_n_0_[2] ),
        .I3(\state_reg_reg_n_0_[0] ),
        .O(\temp_state[2]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h6666666200000000)) 
    \temp_state[2]_i_3 
       (.I0(\temp_state[2]_i_6_n_0 ),
        .I1(\state_reg_reg[2]_0 ),
        .I2(\state_reg_reg[3]_1 [1]),
        .I3(\state_reg_reg[3]_1 [0]),
        .I4(\state_reg_reg[1]_1 ),
        .I5(\temp_state[0]_i_6_n_0 ),
        .O(\temp_state[2]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0FE00FEA0FEA0FEA)) 
    \temp_state[2]_i_5 
       (.I0(\temp_state[0]_i_6_n_0 ),
        .I1(stop_bap),
        .I2(\state_reg_reg[2]_0 ),
        .I3(\temp_state[2]_i_6_n_0 ),
        .I4(\data_in_reg_reg[7] ),
        .I5(\data_in_reg_reg[1] ),
        .O(\temp_state[2]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT4 #(
    .INIT(16'hE7E9)) 
    \temp_state[2]_i_6 
       (.I0(\state_reg_reg_n_0_[2] ),
        .I1(\state_reg_reg_n_0_[0] ),
        .I2(\state_reg_reg_n_0_[3] ),
        .I3(\state_reg_reg_n_0_[1] ),
        .O(\temp_state[2]_i_6_n_0 ));
  LUT4 #(
    .INIT(16'h083C)) 
    \temp_state[2]_i_7 
       (.I0(\state_reg_reg_n_0_[0] ),
        .I1(\state_reg_reg_n_0_[2] ),
        .I2(\state_reg_reg_n_0_[3] ),
        .I3(\state_reg_reg_n_0_[1] ),
        .O(\state_reg_reg[2]_0 ));
  LUT6 #(
    .INIT(64'h02000CC002000FF0)) 
    \temp_state[3]_i_2 
       (.I0(temp_state[3]),
        .I1(\state_reg_reg_n_0_[0] ),
        .I2(\state_reg_reg_n_0_[2] ),
        .I3(\state_reg_reg_n_0_[3] ),
        .I4(\state_reg_reg_n_0_[1] ),
        .I5(stop_bap),
        .O(\temp_state_reg[3]_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \temp_state_reg[0] 
       (.C(clk),
        .CE(E),
        .D(state_next[0]),
        .Q(temp_state[0]),
        .R(SR[1]));
  FDRE #(
    .INIT(1'b0)) 
    \temp_state_reg[1] 
       (.C(clk),
        .CE(E),
        .D(state_next[1]),
        .Q(temp_state[1]),
        .R(SR[1]));
  FDRE #(
    .INIT(1'b0)) 
    \temp_state_reg[2] 
       (.C(clk),
        .CE(E),
        .D(state_next[2]),
        .Q(temp_state[2]),
        .R(SR[1]));
  FDRE #(
    .INIT(1'b0)) 
    \temp_state_reg[3] 
       (.C(clk),
        .CE(E),
        .D(\temp_state_reg[3]_0 ),
        .Q(temp_state[3]),
        .R(SR[1]));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
