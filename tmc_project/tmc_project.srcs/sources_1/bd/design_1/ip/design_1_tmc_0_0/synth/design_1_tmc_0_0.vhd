-- (c) Copyright 1995-2020 Xilinx, Inc. All rights reserved.
-- 
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
-- 
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
-- 
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
-- 
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES.
-- 
-- DO NOT MODIFY THIS FILE.

-- IP VLNV: lab:user:tmc:1.5
-- IP Revision: 2

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY design_1_tmc_0_0 IS
  PORT (
    clock : IN STD_LOGIC;
    reset_in : IN STD_LOGIC;
    axim_done : IN STD_LOGIC;
    axim_error_in : IN STD_LOGIC;
    axim_address : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    axim_data_out : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    axim_data_in : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    axim_read_en : OUT STD_LOGIC;
    axim_write_en : OUT STD_LOGIC;
    axim_start : OUT STD_LOGIC;
    enable_type_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    data_from_TMC_out : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    virtual_mem_address_out : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    t_opcode : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    pause_out : OUT STD_LOGIC;
    b_bus_p_out : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    reg_source_1_gp_out : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    reset_out1 : OUT STD_LOGIC;
    i_mem_clk : OUT STD_LOGIC;
    i_mem_reset : OUT STD_LOGIC;
    i_mem_sel : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    i_mem_en : OUT STD_LOGIC;
    i_mem_address : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    i_mem_data_w : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    i_mem_data_r : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    config_mem_clk : OUT STD_LOGIC;
    config_mem_reset : OUT STD_LOGIC;
    config_mem_address : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    config_mem_data_w : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    config_mem_data_r : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    config_mem_sel : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    config_mem_en : OUT STD_LOGIC;
    instrumentation_empty : IN STD_LOGIC;
    instrumentation_data_r : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    instrumentation_enable : OUT STD_LOGIC
  );
END design_1_tmc_0_0;

ARCHITECTURE design_1_tmc_0_0_arch OF design_1_tmc_0_0 IS
  ATTRIBUTE DowngradeIPIdentifiedWarnings : STRING;
  ATTRIBUTE DowngradeIPIdentifiedWarnings OF design_1_tmc_0_0_arch: ARCHITECTURE IS "yes";
  COMPONENT tmc IS
    GENERIC (
      data_width : INTEGER;
      addr_width : INTEGER
    );
    PORT (
      clock : IN STD_LOGIC;
      reset_in : IN STD_LOGIC;
      axim_done : IN STD_LOGIC;
      axim_error_in : IN STD_LOGIC;
      axim_address : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      axim_data_out : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      axim_data_in : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      axim_read_en : OUT STD_LOGIC;
      axim_write_en : OUT STD_LOGIC;
      axim_start : OUT STD_LOGIC;
      enable_type_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
      data_from_TMC_out : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      virtual_mem_address_out : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      t_opcode : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      pause_out : OUT STD_LOGIC;
      b_bus_p_out : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      reg_source_1_gp_out : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      reset_out1 : OUT STD_LOGIC;
      i_mem_clk : OUT STD_LOGIC;
      i_mem_reset : OUT STD_LOGIC;
      i_mem_sel : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
      i_mem_en : OUT STD_LOGIC;
      i_mem_address : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      i_mem_data_w : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      i_mem_data_r : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      config_mem_clk : OUT STD_LOGIC;
      config_mem_reset : OUT STD_LOGIC;
      config_mem_address : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      config_mem_data_w : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      config_mem_data_r : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      config_mem_sel : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
      config_mem_en : OUT STD_LOGIC;
      instrumentation_empty : IN STD_LOGIC;
      instrumentation_data_r : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      instrumentation_enable : OUT STD_LOGIC
    );
  END COMPONENT tmc;
  ATTRIBUTE X_CORE_INFO : STRING;
  ATTRIBUTE X_CORE_INFO OF design_1_tmc_0_0_arch: ARCHITECTURE IS "tmc,Vivado 2018.2";
  ATTRIBUTE CHECK_LICENSE_TYPE : STRING;
  ATTRIBUTE CHECK_LICENSE_TYPE OF design_1_tmc_0_0_arch : ARCHITECTURE IS "design_1_tmc_0_0,tmc,{}";
  ATTRIBUTE X_INTERFACE_INFO : STRING;
  ATTRIBUTE X_INTERFACE_PARAMETER : STRING;
  ATTRIBUTE X_INTERFACE_INFO OF instrumentation_enable: SIGNAL IS "xilinx.com:interface:fifo_read:1.0 instrumentation_int RD_EN";
  ATTRIBUTE X_INTERFACE_INFO OF instrumentation_data_r: SIGNAL IS "xilinx.com:interface:fifo_read:1.0 instrumentation_int RD_DATA";
  ATTRIBUTE X_INTERFACE_INFO OF instrumentation_empty: SIGNAL IS "xilinx.com:interface:fifo_read:1.0 instrumentation_int EMPTY";
  ATTRIBUTE X_INTERFACE_INFO OF config_mem_en: SIGNAL IS "xilinx.com:interface:bram:1.0 config_mem EN";
  ATTRIBUTE X_INTERFACE_INFO OF config_mem_sel: SIGNAL IS "xilinx.com:interface:bram:1.0 config_mem WE";
  ATTRIBUTE X_INTERFACE_INFO OF config_mem_data_r: SIGNAL IS "xilinx.com:interface:bram:1.0 config_mem DOUT";
  ATTRIBUTE X_INTERFACE_INFO OF config_mem_data_w: SIGNAL IS "xilinx.com:interface:bram:1.0 config_mem DIN";
  ATTRIBUTE X_INTERFACE_INFO OF config_mem_address: SIGNAL IS "xilinx.com:interface:bram:1.0 config_mem ADDR";
  ATTRIBUTE X_INTERFACE_PARAMETER OF config_mem_reset: SIGNAL IS "XIL_INTERFACENAME config_mem_reset, POLARITY ACTIVE_LOW";
  ATTRIBUTE X_INTERFACE_INFO OF config_mem_reset: SIGNAL IS "xilinx.com:signal:reset:1.0 config_mem_reset RST, xilinx.com:interface:bram:1.0 config_mem RST";
  ATTRIBUTE X_INTERFACE_PARAMETER OF config_mem_clk: SIGNAL IS "XIL_INTERFACENAME config_mem_clk, ASSOCIATED_RESET config_mem_reset, FREQ_HZ 100000000, PHASE 0.000, XIL_INTERFACENAME kblare_ps2pl_clk, ASSOCIATED_RESET kblare_ps2pl_reset, FREQ_HZ 100000000, PHASE 0.000, XIL_INTERFACENAME config_mem, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE BRAM_CTRL";
  ATTRIBUTE X_INTERFACE_INFO OF config_mem_clk: SIGNAL IS "xilinx.com:signal:clock:1.0 config_mem_clk CLK, xilinx.com:signal:clock:1.0 kblare_ps2pl_clk CLK, xilinx.com:interface:bram:1.0 config_mem CLK";
  ATTRIBUTE X_INTERFACE_INFO OF i_mem_data_r: SIGNAL IS "xilinx.com:interface:bram:1.0 instruction_mem DOUT";
  ATTRIBUTE X_INTERFACE_INFO OF i_mem_data_w: SIGNAL IS "xilinx.com:interface:bram:1.0 instruction_mem DIN";
  ATTRIBUTE X_INTERFACE_INFO OF i_mem_address: SIGNAL IS "xilinx.com:interface:bram:1.0 instruction_mem ADDR";
  ATTRIBUTE X_INTERFACE_INFO OF i_mem_en: SIGNAL IS "xilinx.com:interface:bram:1.0 instruction_mem EN";
  ATTRIBUTE X_INTERFACE_INFO OF i_mem_sel: SIGNAL IS "xilinx.com:interface:bram:1.0 instruction_mem WE";
  ATTRIBUTE X_INTERFACE_PARAMETER OF i_mem_reset: SIGNAL IS "XIL_INTERFACENAME i_mem_reset, POLARITY ACTIVE_LOW";
  ATTRIBUTE X_INTERFACE_INFO OF i_mem_reset: SIGNAL IS "xilinx.com:signal:reset:1.0 i_mem_reset RST, xilinx.com:interface:bram:1.0 instruction_mem RST";
  ATTRIBUTE X_INTERFACE_PARAMETER OF i_mem_clk: SIGNAL IS "XIL_INTERFACENAME i_mem_clk, ASSOCIATED_RESET i_mem_reset, FREQ_HZ 100000000, PHASE 0.000, XIL_INTERFACENAME instruction_mem, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER";
  ATTRIBUTE X_INTERFACE_INFO OF i_mem_clk: SIGNAL IS "xilinx.com:signal:clock:1.0 i_mem_clk CLK, xilinx.com:interface:bram:1.0 instruction_mem CLK";
  ATTRIBUTE X_INTERFACE_PARAMETER OF reset_out1: SIGNAL IS "XIL_INTERFACENAME reset_out1, POLARITY ACTIVE_LOW";
  ATTRIBUTE X_INTERFACE_INFO OF reset_out1: SIGNAL IS "xilinx.com:signal:reset:1.0 reset_out1 RST";
  ATTRIBUTE X_INTERFACE_PARAMETER OF reset_in: SIGNAL IS "XIL_INTERFACENAME reset_in, POLARITY ACTIVE_LOW";
  ATTRIBUTE X_INTERFACE_INFO OF reset_in: SIGNAL IS "xilinx.com:signal:reset:1.0 reset_in RST";
  ATTRIBUTE X_INTERFACE_PARAMETER OF clock: SIGNAL IS "XIL_INTERFACENAME clock, FREQ_HZ 102564102, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0";
  ATTRIBUTE X_INTERFACE_INFO OF clock: SIGNAL IS "xilinx.com:signal:clock:1.0 clock CLK";
BEGIN
  U0 : tmc
    GENERIC MAP (
      data_width => 32,
      addr_width => 32
    )
    PORT MAP (
      clock => clock,
      reset_in => reset_in,
      axim_done => axim_done,
      axim_error_in => axim_error_in,
      axim_address => axim_address,
      axim_data_out => axim_data_out,
      axim_data_in => axim_data_in,
      axim_read_en => axim_read_en,
      axim_write_en => axim_write_en,
      axim_start => axim_start,
      enable_type_out => enable_type_out,
      data_from_TMC_out => data_from_TMC_out,
      virtual_mem_address_out => virtual_mem_address_out,
      t_opcode => t_opcode,
      pause_out => pause_out,
      b_bus_p_out => b_bus_p_out,
      reg_source_1_gp_out => reg_source_1_gp_out,
      reset_out1 => reset_out1,
      i_mem_clk => i_mem_clk,
      i_mem_reset => i_mem_reset,
      i_mem_sel => i_mem_sel,
      i_mem_en => i_mem_en,
      i_mem_address => i_mem_address,
      i_mem_data_w => i_mem_data_w,
      i_mem_data_r => i_mem_data_r,
      config_mem_clk => config_mem_clk,
      config_mem_reset => config_mem_reset,
      config_mem_address => config_mem_address,
      config_mem_data_w => config_mem_data_w,
      config_mem_data_r => config_mem_data_r,
      config_mem_sel => config_mem_sel,
      config_mem_en => config_mem_en,
      instrumentation_empty => instrumentation_empty,
      instrumentation_data_r => instrumentation_data_r,
      instrumentation_enable => instrumentation_enable
    );
END design_1_tmc_0_0_arch;
