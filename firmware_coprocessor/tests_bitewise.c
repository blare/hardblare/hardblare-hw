#include <stdint.h>
#include <stdio.h>
#include "memory_tag_trf.h"

#define SIX_BITS		0b00000000000000000000000000111111
#define FIVE_BITS		0b00000000000000000000000000011111
#define FOUR_BITS		0b00000000000000000000000000001111
#define TWELVE_BITS		0b00000000000000000000111111111111
#define SIXTEEN_BITS	0b00000000000000001111111111111111 

static uint32_t TRF[32] = {0}; 

int main(int argc, char *argv[]) {

	// SW | t6 | g2 | 0
	// Opcode	DestReg	  SrcReg	  Immediate
	// 000010	  00110	  10001	   0000000000000000
	int initFormatTest = 0b00001000110100010000000000000000;

	printf("\n\n\n initFormatTest : \n ---------------------------\n ");

	int opcodeinitFormatTest = (initFormatTest >> 26) & SIX_BITS;
	int destReginitFormatTest = (initFormatTest >> 21) & FIVE_BITS;
  	int srcReginitFormatTest = (initFormatTest >> 16) & FIVE_BITS;
  	int immInitFormatTest = (initFormatTest >> 0 ) & SIXTEEN_BITS;

  	printf("\n opcodeinitFormatTest : %d",opcodeinitFormatTest);
  	printf("\n destReginitFormatTest : %d",destReginitFormatTest);
  	printf("\n srcReginitFormatTest : %d",srcReginitFormatTest);
  	printf("\n immInitFormatTest : %d",immInitFormatTest);



	// TRR | MOV | T2 | SP Val | T4 | OR
	// Opcode	ARM_OPCODE_TYPE	  DestReg	SrcReg1		SrcReg2		FUNC
	// 100011  		0001		  00010		 11110		 00100		11110
	uint32_t TRFormatTest =   0b10001100010001011110001000000100;

	printf("\n\n\n initFormatTest : \n ---------------------------\n ");



	//Tag_tr_mem | FP CMP | T1 | T7 | 0
	// Opcode	ARM_OPCODE_TYPE	   DestReg SrcReg	Immediate
	// 100101			0111		00001	00111	000000000000
	uint32_t TIFormatTest =   0b10010101110000100111000000000000;

	printf("\n\n\n initFormatTest : \n ---------------------------\n ");


	create_mem_entry_with_address(0x123);
	search_and_affect_value_of_address(0x123, 0x42);
	int val = search_value_of_address(0x123);

	printf("\n\n\n Test memory tag : \n ---------------------------\n ");
	printf("\n val mem[0x123] : %d",val);


	int regVal;

	TRF[0x1] = 0x42;
	TRF[0x2] = TRF[0x1];
	regVal = TRF[0x2];
	printf("\n\n\n Test TRF : \n ---------------------------\n ");
	printf("\n val mem[R2] : %d",regVal);


}