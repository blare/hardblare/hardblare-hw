----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03.02.2016 11:35:06
-- Design Name: 
-- Module Name: testbench - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity testbench is
--  Port ( );
end testbench;

architecture Behavioral of testbench is
signal clk_s, data_v_s, reset_s, en_s : std_logic;
signal data_in_s, addr_s, data_out_s : std_logic_vector(31 downto 0);
signal w_en_s : std_logic_vector(3 downto 0);

component mem_ctl is
port(
  clk : in std_logic;
  data_v : in std_logic;
  data_in : in std_logic_vector(31 downto 0);
  rst,en : out std_logic;
  w_en : out std_logic_vector(3 downto 0);
  addr, data_out : out std_logic_vector(31 downto 0)
);
end component;

begin

clock_tic: process begin
--wait for 1 ns;
loop
clk_s <= '1'; wait for 5 ns;
clk_s <= '0'; wait for 5 ns;
end loop;
end process;

U1 : mem_ctl port map (
 clk => clk_s,
 data_v => data_v_s,
 data_in => data_in_s,
 rst => reset_s, 
 en => en_s, 
 w_en => w_en_s,
 addr => addr_s,
 data_out => data_out_s
);
tb : PROCESS
    BEGIN
        data_v_s <= '0';
        data_in_s <= (others=>'0');
        --writing
        for test_vec in 0 to 17 loop
            data_v_s <= '1';
            data_in_s <= std_logic_vector(to_unsigned(test_vec,32));
            WAIT FOR 10 NS;
            data_v_s <= '0';
            WAIT FOR 10 NS;		
        end loop;	
    END PROCESS;
end Behavioral;
