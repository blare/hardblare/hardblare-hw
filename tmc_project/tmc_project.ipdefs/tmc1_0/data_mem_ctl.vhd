library ieee;
use ieee.std_logic_1164.all;
use work.dependencies_pack.all;

entity data_mem_ctrl is
   port(clk          : in std_logic;
        reset_in     : in std_logic;
 
       
        address_data : in std_logic_vector(31 downto 0);
        mem_source_2 : in mem_source_type;
        data_mem_address: in std_logic;
        data_write   : in std_logic_vector(31 downto 0);
        data_read    : out std_logic_vector(31 downto 0);
       
        pause_out    : out std_logic;
 
       
       
        mem_address  : out std_logic_vector(31 downto 0);
        mem_data_w   : out std_logic_vector(31 downto 0);
        mem_data_r   : in std_logic_vector(31 downto 0);
        tag_mem_data_r:in std_logic_vector(31 downto 0);
        mem_byte_sel : out std_logic_vector(3 downto 0);
        mem_write    : out std_logic;
        mem_read     : out std_logic
        );
end;

architecture logic of data_mem_ctrl is

   --"00" = big_endian; "11" = little_endian
    constant little_endian           : std_logic_vector(1 downto 0) := "00";   
--    signal setup_done                : std_logic;
    signal mem_op_done               : std_logic;
begin

    --mem_op_done <= master_i_if.done;
    mem_op_done <= '1'; -- no done signal in local BRAM interface
    -- it requries single clock cycle to read/write data 
mem_proc: process(clk, reset_in, mem_source_2, tag_mem_data_r, address_data,
                  data_write, mem_data_r, mem_op_done)
   variable data, datab   : std_logic_vector(31 downto 0);   
   variable byte_sel_temp : std_logic_vector(3 downto 0);
   variable write_temp    : std_logic;      
--   variable address_temp  : std_logic_vector(31 downto 0);
   variable bits          : std_logic_vector(1 downto 0);
   variable mem_data_w_v  : std_logic_vector(31 downto 0);
--   variable setup_done_var: std_logic;
   variable mem_read_v    : std_logic;
   variable pause         : std_logic;
--   variable enable        : std_logic;

begin
   byte_sel_temp := "0000";
   write_temp := '0';   
   pause := '0';
--   setup_done_var := setup_done;
--   address_temp := physical_address;
   data := mem_data_r;
   datab := ZERO;
   mem_data_w_v := ZERO; --"ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ";
   mem_read_v := '0';

   --case mem_source is
   --when tag_mem_read32 =>
   --   master_o_if.go <= '1'; 
   --   master_o_if.RNW <= '1'; 
   --   master_o_if.address <= address_temp;
   --   master_o_if.increment_burst <= '1'; -- very important
   --   master_o_if.clear_data_fifos <= '0';
   --   master_o_if.burst_length <= X"00"; -- data transfer size (1+1 transfer of 4 bytes) -- => the protocol adds 1 so there will be two beats of burst size ...
   --   master_o_if.burst_size <= "000" & X"4"; -- Transfer 4 bytes 
   --   read_fifo_en := '1';
   --   write_fifo_en := '0';
   --   --master_o_if.write_fifo_en <= '0';
   --   master_o_if.write_data <= (others => '0');
   --when tag_mem_write32 =>
   --   master_o_if.go <= '1'; 
   --   master_o_if.RNW <= '0';
   --   master_o_if.address <= address_temp;
   --   master_o_if.increment_burst <= '1'; -- very important
   --   master_o_if.clear_data_fifos <= '0';
   --   master_o_if.burst_length <= X"00"; -- data transfer size (1+1 transfer of 4 bytes) -- => the protocol adds 1 so there will be two beats of burst size ...
   --   master_o_if.burst_size <= "000" & X"4"; -- Transfer 4 bytes 
   --   --master_o_if.read_fifo_en <= '0';
   --   --master_o_if.write_fifo_en <= '1';
   --   read_fifo_en := '0';
   --   write_fifo_en := '1';
   --   master_o_if.write_data <= data_write;
   --when others =>
   --   master_o_if.go <= '0'; 
   --   master_o_if.RNW <= '0';
   --   master_o_if.address <= (others => '0');
   --   master_o_if.increment_burst <= '1'; -- very important
   --   master_o_if.clear_data_fifos <= '0';
   --   master_o_if.burst_length <= X"00"; -- data transfer size (1+1 transfer of 4 bytes) -- => the protocol adds 1 so there will be two beats of burst size ...
   --   master_o_if.burst_size <= "000" & X"0"; -- Transfer 4 bytes 
   --   --master_o_if.read_fifo_en <= '0';
   --   --master_o_if.write_fifo_en <= '0';
   --   read_fifo_en := '0';
   --   write_fifo_en := '0';
   --   master_o_if.write_data <= (others => '0');
   --end case;

    



   case mem_source_2 is 
      when mem_read32 => 
        byte_sel_temp := "1111";
        mem_read_v := '1';
      when mem_write32 => 
        byte_sel_temp := "1111";
        mem_data_w_v := data_write;
        write_temp := '1';
      when others => 
   end case;

   

   --if (read_fifo_en = '1' or write_fifo_en = '1') and mem_op_done = '0' then 
   --   pause := '1';
   --else 
   --   pause := '0';
   --end if;
   pause := '0';

   

   

    case mem_source_2 is 
    when mem_read32 => 
      data_read <= mem_data_r;
    when others => 
      data_read <= (others => '0');
    end case;

   pause_out <= pause;
   mem_byte_sel <= byte_sel_temp;
   mem_address <= address_data;
   mem_write <= write_temp;
   mem_read <= mem_read_v;
   mem_data_w <= mem_data_w_v;
   --master_o_if.read_fifo_en <= read_fifo_en;
   --master_o_if.write_fifo_en <= write_fifo_en;
--   master_o_if.en <= enable;
end process; --data_proc

end; --architecture logic
