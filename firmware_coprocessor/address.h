

#define PTM_TRACES_SIZE_BUFFER		2

uint32_t* getDebugAddress();
uint32_t* getPLtoPSAddress();
uint32_t* nextPTMtraceAddress();

#ifdef DEBUG_SIMULATION

void *memcpy(void *dst, const void *src, unsigned long bytes);

uint32_t loaded_annotations_second[999];

uint32_t debug_memory[999];

uint32_t instrumentation_memory[100];

uint32_t loaded_bbt_annotations[999];

uint32_t kernel_to_monitor_memory[999];

uint32_t monitor_to_kernel_memory[999];

uint32_t loaded_ptm_traces[999];

uint32_t config_security_policy_memory[999];

uint32_t ptops[999];

#define LOADED_BBT_ANNOTATION_ADDRESS        (uint32_t*)0x20000

#define LOADING_ANNOTATIONS_TO_SECOND_STAGE_ADDRESS  &loaded_annotations_second

#define PTM_TRACES_ADDRESS            (uint32_t*)0x30000

#define SECURITY_POLICY_CONFIG         (uint32_t*)0x40000

// Address for DEBUG BRAM
#define DEBUG_BRAM            &debug_memory

// Address for PLTOPS
#define PLTOPS                &pltops

// GENERATE INTERRUPT
#define PLTOPS_GENERATE_INTERRUPT       (uint32_t*)0x90000

// Address for FIFO PTM empty
#define FIFO_PTM_EMPTY             (uint32_t*)0x50000

// Address for FIFO PTM full
#define FIFO_PTM_FULL             (uint32_t*)0x50004

// Address for FIFO TMC empty
#define FIFO_TMC_EMPTY             (uint32_t*)0x50008

// Address for FIFO TMC full
#define FIFO_TMC_FULL             (uint32_t*)0x5000C

// Address for FIFO TMC full
#define FIFO_INSTRUMENTATION_EMPTY        (uint32_t*)0x500C8

// Address for Instrumentation
#define READ_INSTRUMENTATION            (uint32_t*)0x60000

// FIFO KERNEL TO MONITOR EMPTY
#define FIFO_KERNEL_TO_MONITOR_EMPTY     (uint32_t*)0x70000

// FIFO KERNEL TO MONITOR
#define KERNEL_TO_MONITOR     (uint32_t*)0x70008

// FIFO MONITOR TO KERNEL FULL
#define FIFO_MONITOR_TO_KERNEL_FULL     (uint32_t*)0x80000

// FIFO MONITOR TO KERNEL ALMOST_FULL
#define FIFO_MONITOR_TO_KERNEL_ALMOST_FULL     (uint32_t*)0x80004

// FIFO MONITOR TO KERNEL
#define MONITOR_TO_KERNEL     &monitor_to_kernel_memory



#else


// Addresses to read and containing the loaded Basic Block Table (BBT) and the loaded Annotations (ANNOTS)
#define LOADED_BBT_ANNOTATION_ADDRESS         (uint32_t*)0x10000000

// Address for writing annotations in order to be executed by the DIFT engine (TMC)
#define LOADING_ANNOTATIONS_TO_SECOND_STAGE_ADDRESS        (uint32_t*)0x60000000

// Address to read for reading traces from PTM
#define PTM_TRACES_ADDRESS            (uint32_t*)0x20000000

// Address for kernel 2 monitor communication
#define KERNEL_TO_MONITOR            (uint32_t*)0x40000000

// Address for monitor 2 kernel communication
#define MONITOR_TO_KERNEL            (uint32_t*)0x50000000

// Address for Instrumentation
#define READ_INSTRUMENTATION            (uint32_t*)0x30000000

// Address for DEBUG BRAM
#define DEBUG_BRAM            (uint32_t*)0x90000000

// Address for PL to PS BRAM
#define PLTOPS            (uint32_t*)0x70000000

// Address for Security Policy Config BRAM
#define SECURITY_POLICY_CONFIG          (uint32_t*)0xA0000000

// ****************************************************
//  FIFO PTM 0x82000000
// ****************************************************

// EMPTY
#define FIFO_PTM_EMPTY                    (uint32_t*)0x82100000

// ALMOST EMPTY
#define FIFO_PTM_ALMOST_EMPTY             (uint32_t*)0x82200000

// FULL
#define FIFO_PTM_FULL                     (uint32_t*)0x82300000

// ALMOST FULL
#define FIFO_PTM_ALMOST_FULL              (uint32_t*)0x82400000



// ****************************************************
//  FIFO INSTRUMENTATION 0x83000000
// ****************************************************

// EMPTY
#define FIFO_INSTRUMENTATION_EMPTY                    (uint32_t*)0x83100000

// ALMOST EMPTY
#define FIFO_INSTRUMENTATION_ALMOST_EMPTY             (uint32_t*)0x83200000

// FULL
#define FIFO_INSTRUMENTATION_FULL                     (uint32_t*)0x83300000

// ALMOST FULL
#define FIFO_INSTRUMENTATION_ALMOST_FULL              (uint32_t*)0x83400000



// 0****************************************************
//  FIFO TMC 0x86000000
// ****************************************************

// EMPTY
#define FIFO_TMC_EMPTY                    (uint32_t*)0x86100000

// ALMOST EMPTY
#define FIFO_TMC_ALMOST_EMPTY             (uint32_t*)0x86200000

// FULL
#define FIFO_TMC_FULL                     (uint32_t*)0x86300000

// ALMOST FULL
#define FIFO_TMC_ALMOST_FULL              (uint32_t*)0x86400000



// ****************************************************
//  FIFO KERNEL TO MONITOR 0x84000000
// ****************************************************

// EMPTY
#define FIFO_KERNEL_TO_MONITOR_EMPTY                    (uint32_t*)0x84100000

// ALMOST EMPTY
#define FIFO_KERNEL_TO_MONITOR_ALMOST_EMPTY             (uint32_t*)0x84200000

// FULL
#define FIFO_KERNEL_TO_MONITOR_FULL                     (uint32_t*)0x84300000

// ALMOST FULL
#define FIFO_KERNEL_TO_MONITOR_ALMOST_FULL              (uint32_t*)0x84400000



// ****************************************************
//  FIFO MONITOR TO KERNEL 0x85000000
// ****************************************************

// EMPTY
#define FIFO_MONITOR_TO_KERNEL_EMPTY                    (uint32_t*)0x85100000

// ALMOST EMPTY
#define FIFO_MONITOR_TO_KERNEL_ALMOST_EMPTY             (uint32_t*)0x85200000

// FULL
#define FIFO_MONITOR_TO_KERNEL_FULL                     (uint32_t*)0x85300000

// ALMOST FULL
#define FIFO_MONITOR_TO_KERNEL_ALMOST_FULL              (uint32_t*)0x85400000



// ****************************************************
//  TMC SIGNALS 0x86000000
// ****************************************************


// GENERATE INTERRUPT
//#define TMC_GENERATE_INTERRUPT       (uint32_t*)0x86500000

// ACK INTERRUPT
//#define TMC_ACK_INTERRUPT            (uint32_t*)0x86600000



// ****************************************************
//  PLTOPS 0x87000000
// ****************************************************


// EMPTY
#define PLTOPS_EMPTY                    (uint32_t*)0x87100000

// ALMOST EMPTY
#define PLTOPS_ALMOST_EMPTY             (uint32_t*)0x87200000

// FULL
#define PLTOPS_FULL                     (uint32_t*)0x87300000

// ALMOST FULL
#define PLTOPS_ALMOST_FULL              (uint32_t*)0x87400000

// GENERATE INTERRUPT
#define PLTOPS_GENERATE_INTERRUPT       (uint32_t*)0x87500000

// ACK INTERRUPT
#define PLTOPS_ACK_INTERRUPT            (uint32_t*)0x87600000



#endif
