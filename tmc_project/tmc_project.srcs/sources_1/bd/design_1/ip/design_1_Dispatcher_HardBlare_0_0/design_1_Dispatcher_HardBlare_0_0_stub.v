// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
// Date        : Sat Jun 20 13:50:31 2020
// Host        : HardBlare-Workstation running 64-bit Ubuntu 16.04.6 LTS
// Command     : write_verilog -force -mode synth_stub -rename_top design_1_Dispatcher_HardBlare_0_0 -prefix
//               design_1_Dispatcher_HardBlare_0_0_ design_1_Dispatcher_HardBlare_0_0_stub.v
// Design      : design_1_Dispatcher_HardBlare_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "Dispatcher_Plasma_MIPS_HardBlare,Vivado 2018.2" *)
module design_1_Dispatcher_HardBlare_0_0(clk, reset, bram_firmware_code_en, 
  bram_firmware_code_address, bram_firmware_code_data_write, 
  bram_firmware_code_bytes_selection, bram_firmware_code_data_read, 
  bram_firmware_code_clk, bram_firmware_code_reset, bram_bbt_annotations_en, 
  bram_bbt_annotations_data_read, bram_bbt_annotations_address, 
  bram_bbt_annotations_data_write, bram_bbt_annotations_bytes_selection, 
  bram_bbt_annotations_clk, bram_bbt_annotations_reset, fifo_ptm_en, fifo_ptm_data_read, 
  fifo_ptm_empty, fifo_ptm_almost_empty, fifo_ptm_full, fifo_ptm_almost_full, fifo_ptm_clk, 
  fifo_ptm_reset, fifo_monitor_to_kernel_en, fifo_monitor_to_kernel_data_write, 
  fifo_monitor_to_kernel_empty, fifo_monitor_to_kernel_almost_empty, 
  fifo_monitor_to_kernel_full, fifo_monitor_to_kernel_almost_full, 
  fifo_monitor_to_kernel_clk, fifo_monitor_to_kernel_reset, fifo_kernel_to_monitor_en, 
  fifo_kernel_to_monitor_data_read, fifo_kernel_to_monitor_empty, 
  fifo_kernel_to_monitor_almost_empty, fifo_kernel_to_monitor_full, 
  fifo_kernel_to_monitor_almost_full, fifo_kernel_to_monitor_clk, 
  fifo_kernel_to_monitor_reset, fifo_read_instrumentation_request_read_en, 
  fifo_read_instrumentation_data_read, fifo_instrumentation_empty, 
  fifo_instrumentation_almost_empty, fifo_instrumentation_full, 
  fifo_instrumentation_almost_full, bram_debug_en, bram_debug_address, 
  bram_debug_data_write, bram_debug_bytes_selection, bram_debug_data_read, 
  bram_debug_clk, bram_debug_reset, PLtoPS_clk, PLtoPS_reset, PLtoPS_enable, PLtoPS_address, 
  PLtoPS_write_data, PLtoPS_bytes_selection, PLtoPS_readed_data, 
  PLtoPS_generate_interrupt, bram_sec_policy_config_en, 
  bram_sec_policy_config_data_read, bram_sec_policy_config_address, 
  bram_sec_policy_config_data_write, bram_sec_policy_config_bytes_selection, 
  bram_sec_policy_config_clk, bram_sec_policy_config_reset)
/* synthesis syn_black_box black_box_pad_pin="clk,reset,bram_firmware_code_en,bram_firmware_code_address[31:0],bram_firmware_code_data_write[31:0],bram_firmware_code_bytes_selection[3:0],bram_firmware_code_data_read[31:0],bram_firmware_code_clk,bram_firmware_code_reset,bram_bbt_annotations_en,bram_bbt_annotations_data_read[31:0],bram_bbt_annotations_address[31:0],bram_bbt_annotations_data_write[31:0],bram_bbt_annotations_bytes_selection[3:0],bram_bbt_annotations_clk,bram_bbt_annotations_reset,fifo_ptm_en,fifo_ptm_data_read[31:0],fifo_ptm_empty,fifo_ptm_almost_empty,fifo_ptm_full,fifo_ptm_almost_full,fifo_ptm_clk,fifo_ptm_reset,fifo_monitor_to_kernel_en,fifo_monitor_to_kernel_data_write[31:0],fifo_monitor_to_kernel_empty,fifo_monitor_to_kernel_almost_empty,fifo_monitor_to_kernel_full,fifo_monitor_to_kernel_almost_full,fifo_monitor_to_kernel_clk,fifo_monitor_to_kernel_reset,fifo_kernel_to_monitor_en,fifo_kernel_to_monitor_data_read[31:0],fifo_kernel_to_monitor_empty,fifo_kernel_to_monitor_almost_empty,fifo_kernel_to_monitor_full,fifo_kernel_to_monitor_almost_full,fifo_kernel_to_monitor_clk,fifo_kernel_to_monitor_reset,fifo_read_instrumentation_request_read_en,fifo_read_instrumentation_data_read[31:0],fifo_instrumentation_empty,fifo_instrumentation_almost_empty,fifo_instrumentation_full,fifo_instrumentation_almost_full,bram_debug_en,bram_debug_address[31:0],bram_debug_data_write[31:0],bram_debug_bytes_selection[3:0],bram_debug_data_read[31:0],bram_debug_clk,bram_debug_reset,PLtoPS_clk,PLtoPS_reset,PLtoPS_enable,PLtoPS_address[31:0],PLtoPS_write_data[31:0],PLtoPS_bytes_selection[3:0],PLtoPS_readed_data[31:0],PLtoPS_generate_interrupt,bram_sec_policy_config_en,bram_sec_policy_config_data_read[31:0],bram_sec_policy_config_address[31:0],bram_sec_policy_config_data_write[31:0],bram_sec_policy_config_bytes_selection[3:0],bram_sec_policy_config_clk,bram_sec_policy_config_reset" */;
  input clk;
  input reset;
  output bram_firmware_code_en;
  output [31:0]bram_firmware_code_address;
  output [31:0]bram_firmware_code_data_write;
  output [3:0]bram_firmware_code_bytes_selection;
  input [31:0]bram_firmware_code_data_read;
  output bram_firmware_code_clk;
  output bram_firmware_code_reset;
  output bram_bbt_annotations_en;
  input [31:0]bram_bbt_annotations_data_read;
  output [31:0]bram_bbt_annotations_address;
  output [31:0]bram_bbt_annotations_data_write;
  output [3:0]bram_bbt_annotations_bytes_selection;
  output bram_bbt_annotations_clk;
  output bram_bbt_annotations_reset;
  output fifo_ptm_en;
  input [31:0]fifo_ptm_data_read;
  input fifo_ptm_empty;
  input fifo_ptm_almost_empty;
  input fifo_ptm_full;
  input fifo_ptm_almost_full;
  output fifo_ptm_clk;
  output fifo_ptm_reset;
  output fifo_monitor_to_kernel_en;
  output [31:0]fifo_monitor_to_kernel_data_write;
  input fifo_monitor_to_kernel_empty;
  input fifo_monitor_to_kernel_almost_empty;
  input fifo_monitor_to_kernel_full;
  input fifo_monitor_to_kernel_almost_full;
  output fifo_monitor_to_kernel_clk;
  output fifo_monitor_to_kernel_reset;
  output fifo_kernel_to_monitor_en;
  input [31:0]fifo_kernel_to_monitor_data_read;
  input fifo_kernel_to_monitor_empty;
  input fifo_kernel_to_monitor_almost_empty;
  input fifo_kernel_to_monitor_full;
  input fifo_kernel_to_monitor_almost_full;
  output fifo_kernel_to_monitor_clk;
  output fifo_kernel_to_monitor_reset;
  output fifo_read_instrumentation_request_read_en;
  input [31:0]fifo_read_instrumentation_data_read;
  input fifo_instrumentation_empty;
  input fifo_instrumentation_almost_empty;
  input fifo_instrumentation_full;
  input fifo_instrumentation_almost_full;
  output bram_debug_en;
  output [31:0]bram_debug_address;
  output [31:0]bram_debug_data_write;
  output [3:0]bram_debug_bytes_selection;
  input [31:0]bram_debug_data_read;
  output bram_debug_clk;
  output bram_debug_reset;
  output PLtoPS_clk;
  output PLtoPS_reset;
  output PLtoPS_enable;
  output [31:0]PLtoPS_address;
  output [31:0]PLtoPS_write_data;
  output [3:0]PLtoPS_bytes_selection;
  input [31:0]PLtoPS_readed_data;
  output PLtoPS_generate_interrupt;
  output bram_sec_policy_config_en;
  input [31:0]bram_sec_policy_config_data_read;
  output [31:0]bram_sec_policy_config_address;
  output [31:0]bram_sec_policy_config_data_write;
  output [3:0]bram_sec_policy_config_bytes_selection;
  output bram_sec_policy_config_clk;
  output bram_sec_policy_config_reset;
endmodule
