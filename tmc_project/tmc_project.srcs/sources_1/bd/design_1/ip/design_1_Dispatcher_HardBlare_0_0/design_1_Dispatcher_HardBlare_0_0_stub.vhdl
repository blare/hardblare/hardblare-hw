-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
-- Date        : Sat Jun 20 13:50:31 2020
-- Host        : HardBlare-Workstation running 64-bit Ubuntu 16.04.6 LTS
-- Command     : write_vhdl -force -mode synth_stub -rename_top design_1_Dispatcher_HardBlare_0_0 -prefix
--               design_1_Dispatcher_HardBlare_0_0_ design_1_Dispatcher_HardBlare_0_0_stub.vhdl
-- Design      : design_1_Dispatcher_HardBlare_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z020clg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity design_1_Dispatcher_HardBlare_0_0 is
  Port ( 
    clk : in STD_LOGIC;
    reset : in STD_LOGIC;
    bram_firmware_code_en : out STD_LOGIC;
    bram_firmware_code_address : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_firmware_code_data_write : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_firmware_code_bytes_selection : out STD_LOGIC_VECTOR ( 3 downto 0 );
    bram_firmware_code_data_read : in STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_firmware_code_clk : out STD_LOGIC;
    bram_firmware_code_reset : out STD_LOGIC;
    bram_bbt_annotations_en : out STD_LOGIC;
    bram_bbt_annotations_data_read : in STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_bbt_annotations_address : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_bbt_annotations_data_write : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_bbt_annotations_bytes_selection : out STD_LOGIC_VECTOR ( 3 downto 0 );
    bram_bbt_annotations_clk : out STD_LOGIC;
    bram_bbt_annotations_reset : out STD_LOGIC;
    fifo_ptm_en : out STD_LOGIC;
    fifo_ptm_data_read : in STD_LOGIC_VECTOR ( 31 downto 0 );
    fifo_ptm_empty : in STD_LOGIC;
    fifo_ptm_almost_empty : in STD_LOGIC;
    fifo_ptm_full : in STD_LOGIC;
    fifo_ptm_almost_full : in STD_LOGIC;
    fifo_ptm_clk : out STD_LOGIC;
    fifo_ptm_reset : out STD_LOGIC;
    fifo_monitor_to_kernel_en : out STD_LOGIC;
    fifo_monitor_to_kernel_data_write : out STD_LOGIC_VECTOR ( 31 downto 0 );
    fifo_monitor_to_kernel_empty : in STD_LOGIC;
    fifo_monitor_to_kernel_almost_empty : in STD_LOGIC;
    fifo_monitor_to_kernel_full : in STD_LOGIC;
    fifo_monitor_to_kernel_almost_full : in STD_LOGIC;
    fifo_monitor_to_kernel_clk : out STD_LOGIC;
    fifo_monitor_to_kernel_reset : out STD_LOGIC;
    fifo_kernel_to_monitor_en : out STD_LOGIC;
    fifo_kernel_to_monitor_data_read : in STD_LOGIC_VECTOR ( 31 downto 0 );
    fifo_kernel_to_monitor_empty : in STD_LOGIC;
    fifo_kernel_to_monitor_almost_empty : in STD_LOGIC;
    fifo_kernel_to_monitor_full : in STD_LOGIC;
    fifo_kernel_to_monitor_almost_full : in STD_LOGIC;
    fifo_kernel_to_monitor_clk : out STD_LOGIC;
    fifo_kernel_to_monitor_reset : out STD_LOGIC;
    fifo_read_instrumentation_request_read_en : out STD_LOGIC;
    fifo_read_instrumentation_data_read : in STD_LOGIC_VECTOR ( 31 downto 0 );
    fifo_instrumentation_empty : in STD_LOGIC;
    fifo_instrumentation_almost_empty : in STD_LOGIC;
    fifo_instrumentation_full : in STD_LOGIC;
    fifo_instrumentation_almost_full : in STD_LOGIC;
    bram_debug_en : out STD_LOGIC;
    bram_debug_address : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_debug_data_write : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_debug_bytes_selection : out STD_LOGIC_VECTOR ( 3 downto 0 );
    bram_debug_data_read : in STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_debug_clk : out STD_LOGIC;
    bram_debug_reset : out STD_LOGIC;
    PLtoPS_clk : out STD_LOGIC;
    PLtoPS_reset : out STD_LOGIC;
    PLtoPS_enable : out STD_LOGIC;
    PLtoPS_address : out STD_LOGIC_VECTOR ( 31 downto 0 );
    PLtoPS_write_data : out STD_LOGIC_VECTOR ( 31 downto 0 );
    PLtoPS_bytes_selection : out STD_LOGIC_VECTOR ( 3 downto 0 );
    PLtoPS_readed_data : in STD_LOGIC_VECTOR ( 31 downto 0 );
    PLtoPS_generate_interrupt : out STD_LOGIC;
    bram_sec_policy_config_en : out STD_LOGIC;
    bram_sec_policy_config_data_read : in STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_sec_policy_config_address : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_sec_policy_config_data_write : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_sec_policy_config_bytes_selection : out STD_LOGIC_VECTOR ( 3 downto 0 );
    bram_sec_policy_config_clk : out STD_LOGIC;
    bram_sec_policy_config_reset : out STD_LOGIC
  );

end design_1_Dispatcher_HardBlare_0_0;

architecture stub of design_1_Dispatcher_HardBlare_0_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "clk,reset,bram_firmware_code_en,bram_firmware_code_address[31:0],bram_firmware_code_data_write[31:0],bram_firmware_code_bytes_selection[3:0],bram_firmware_code_data_read[31:0],bram_firmware_code_clk,bram_firmware_code_reset,bram_bbt_annotations_en,bram_bbt_annotations_data_read[31:0],bram_bbt_annotations_address[31:0],bram_bbt_annotations_data_write[31:0],bram_bbt_annotations_bytes_selection[3:0],bram_bbt_annotations_clk,bram_bbt_annotations_reset,fifo_ptm_en,fifo_ptm_data_read[31:0],fifo_ptm_empty,fifo_ptm_almost_empty,fifo_ptm_full,fifo_ptm_almost_full,fifo_ptm_clk,fifo_ptm_reset,fifo_monitor_to_kernel_en,fifo_monitor_to_kernel_data_write[31:0],fifo_monitor_to_kernel_empty,fifo_monitor_to_kernel_almost_empty,fifo_monitor_to_kernel_full,fifo_monitor_to_kernel_almost_full,fifo_monitor_to_kernel_clk,fifo_monitor_to_kernel_reset,fifo_kernel_to_monitor_en,fifo_kernel_to_monitor_data_read[31:0],fifo_kernel_to_monitor_empty,fifo_kernel_to_monitor_almost_empty,fifo_kernel_to_monitor_full,fifo_kernel_to_monitor_almost_full,fifo_kernel_to_monitor_clk,fifo_kernel_to_monitor_reset,fifo_read_instrumentation_request_read_en,fifo_read_instrumentation_data_read[31:0],fifo_instrumentation_empty,fifo_instrumentation_almost_empty,fifo_instrumentation_full,fifo_instrumentation_almost_full,bram_debug_en,bram_debug_address[31:0],bram_debug_data_write[31:0],bram_debug_bytes_selection[3:0],bram_debug_data_read[31:0],bram_debug_clk,bram_debug_reset,PLtoPS_clk,PLtoPS_reset,PLtoPS_enable,PLtoPS_address[31:0],PLtoPS_write_data[31:0],PLtoPS_bytes_selection[3:0],PLtoPS_readed_data[31:0],PLtoPS_generate_interrupt,bram_sec_policy_config_en,bram_sec_policy_config_data_read[31:0],bram_sec_policy_config_address[31:0],bram_sec_policy_config_data_write[31:0],bram_sec_policy_config_bytes_selection[3:0],bram_sec_policy_config_clk,bram_sec_policy_config_reset";
attribute x_core_info : string;
attribute x_core_info of stub : architecture is "Dispatcher_Plasma_MIPS_HardBlare,Vivado 2018.2";
begin
end;
