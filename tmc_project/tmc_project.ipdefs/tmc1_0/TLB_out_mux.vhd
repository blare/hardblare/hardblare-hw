library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.byte_pkg.all;

entity out_sel is
generic ( n: integer := 32;
	  m: integer := 26;
	  TLB_ENTRIES: integer := 64;
	  OFFSET: integer := 12);
Port(clock, reset: in std_logic;
    inp:in byte_array_t(0 to TLB_ENTRIES-1);
    outp:out std_logic_vector(m-OFFSET-1 downto 0);
    selector:in std_logic_vector(0 to TLB_ENTRIES-1));
end out_sel;

architecture Behavioral of out_sel is
begin
--process(selector,inp)
--begin
--    CASE selector IS
--	with selector select
--    	outp <= inp(0) WHEN "00000001",
--    	  	inp(1) WHEN "00000010",
--		inp(2) WHEN "00000100",
--		inp(3) WHEN "00001000",
--		inp(4) WHEN "00010000",
--		inp(5) WHEN "00100000",
--		inp(6) WHEN "01000000",
--		inp(7) WHEN "10000000",
--    		"ZZZZZZZZ" WHEN OTHERS;
--END CASE;    
--end process;
--process(selector)
process (clock)
begin
	if (rising_edge(clock)) and reset = '0' then

	for i in 0 to (TLB_ENTRIES-1) loop
		if(selector(i) = '1') then
			outp <= inp(i);
		end if;
	end loop;
	end if;
end process;


end Behavioral;
