library ieee;
use ieee.std_logic_1164.all;
use work.mips_pack.all;

entity mips_top_level is
   port(clk                  : in std_logic;
        reset_in             : in std_logic;
        intr_in              : in std_logic;
        
        cpu_mem_en          : out std_logic;
        cpu_mem_addr        : out std_logic_vector(31 downto 2);
        cpu_mem_data        : out std_logic_vector(31 downto 0);
        cpu_data_read       : out std_logic_vector(31 downto 0);
            
         i_mem_en      : out std_logic;
         i_mem_address : out std_logic_vector(31 downto 0);         
         i_mem_data_r  : in  std_logic_vector(31 downto 0);
         i_mem_sel     : out std_logic_vector(3 downto 0);         
         i_mem_clk     : out std_logic;
         i_mem_reset   : out std_logic;
         
         mem_address   : out std_logic_vector(31 downto 0);
         mem_data_w    : out std_logic_vector(31 downto 0);
         mem_data_r    : in  std_logic_vector(31 downto 0);
         mem_sel       : out std_logic_vector(3 downto 0);
         mem_op        : out std_logic;
         mem_pause     : in  std_logic;
         data_mem_en   : out std_logic;
         data_mem_clk  : out std_logic;
         data_mem_reset: out std_logic;

         annotations_mem_address   : out std_logic_vector(31 downto 0);
         annotations_mem_data_w    : out std_logic_vector(31 downto 0);
         annotations_mem_data_r    : in  std_logic_vector(31 downto 0);
         annotations_mem_sel       : out std_logic_vector(3 downto 0);
         annotations_mem_en        : out std_logic;
         anntoations_mem_clk       : out std_logic;
         annotations_mem_reset     : out std_logic;
         
         annotations_bb_mem_address   : out std_logic_vector(31 downto 0);
         annotations_bb_mem_data_w    : out std_logic_vector(31 downto 0);
         annotations_bb_mem_data_r    : in  std_logic_vector(31 downto 0);
         annotations_bb_mem_sel       : out std_logic_vector(3 downto 0);
         annotations_bb_en            : out std_logic;
         annotations_bb_clk           : out std_logic;
         annotations_bb_reset         : out std_logic;
         
         kernel2monitor_mem_address   : out std_logic_vector(31 downto 0);
         kernel2monitor_mem_data_w    : out std_logic_vector(31 downto 0);
         kernel2monitor_mem_data_r    : in  std_logic_vector(31 downto 0);
         kernel2monitor_mem_sel       : out std_logic_vector(3 downto 0);
         kernel2monitor_mem_en            : out std_logic;
         kernel2monitor_mem_clk           : out std_logic;
         kernel2monitor_mem_reset         : out std_logic;
              
         monitor2kernel_mem_address   : out std_logic_vector(31 downto 0);
         monitor2kernel_mem_data_w    : out std_logic_vector(31 downto 0);
         monitor2kernel_mem_data_r    : in  std_logic_vector(31 downto 0);
         monitor2kernel_mem_sel       : out std_logic_vector(3 downto 0);
         monitor2kernel_mem_en            : out std_logic;
         monitor2kernel_mem_clk           : out std_logic;
         monitor2kernel_mem_reset         : out std_logic;
         

         trace_mem_address   : out std_logic_vector(31 downto 0);
         trace_mem_data_w    : out std_logic_vector(31 downto 0);
         trace_mem_data_r    : in  std_logic_vector(31 downto 0);
         trace_mem_sel       : out std_logic_vector(3 downto 0);
         trace_mem_clk       : out std_logic;
         trace_mem_reset     : out std_logic;
         trace_mem_en        : out std_logic );
end entity;

architecture structural of mips_top_level is
component mips_cpu is
   port(clk           : in std_logic;
        reset_in      : in std_logic;
        --stall         : in std_logic;
        intr_in       : in std_logic;

        i_mem_enable  : out std_logic;
        i_mem_address : out std_logic_vector(31 downto 0);        
        i_mem_data_r  : in std_logic_vector(31 downto 0);
        i_mem_sel     : out std_logic_vector(3 downto 0);
        
        mem_address   : out std_logic_vector(31 downto 0);
        mem_data_w    : out std_logic_vector(31 downto 0);
        mem_data_r    : in std_logic_vector(31 downto 0);
        mem_sel       : out std_logic_vector(3 downto 0);
        mem_write     : out std_logic;
        mem_read      : out std_logic;
        mem_pause     : in std_logic;

        data_mem_cs         : out std_logic;
        annotations_mem_cs  : out std_logic;
        annotations_bb_cs   : out std_logic;
        trace_mem_cs        : out std_logic;
        kernel2monitor_cs   : out std_logic;
        monitor2kernel_cs   : out std_logic
        );
end component; --entity mips_cpu

signal mem_write           :  std_logic;
signal mem_read            :  std_logic;
signal data_mem_cs         :  std_logic;
signal annotations_mem_cs  :  std_logic;
signal annotations_bb_cs   :  std_logic;
signal trace_mem_cs        :  std_logic;
signal kernel2monitor_mem_cs :  std_logic;
signal monitor2kernel_mem_cs : std_logic;
signal data_mem_cs_r       :  std_logic;
signal annotations_mem_cs_r:  std_logic;
signal annotations_bb_cs_r :  std_logic;
signal trace_mem_cs_r      :  std_logic;
signal kernel2monitor_cs_r :  std_logic;
signal monitor2kernel_cs_r : std_logic;
signal mem_op_s            :  std_logic;

signal cpu_mem_address   :  std_logic_vector(31 downto 0);
signal cpu_mem_data_w    :  std_logic_vector(31 downto 0);
signal cpu_mem_data_r    :  std_logic_vector(31 downto 0);
signal cpu_mem_sel       :  std_logic_vector(3 downto 0);

signal i_mem_address_s   : std_logic_vector(31 downto 0);
attribute keep : string;
attribute keep of i_mem_address_s : signal is "true";

begin

    i_mem_clk <= clk;
    data_mem_clk <= clk;
    anntoations_mem_clk <= clk;
    annotations_bb_clk <= clk;
    trace_mem_clk <= clk;
    monitor2kernel_mem_clk <= clk;
                    
    kernel2monitor_mem_clk <= clk;
    

    --i_mem_address <= i_mem_address_s(29 downto 0); -- 31 downto 2 for dift_coprocessor project (simulation) and 29 downto 0 for dispatcher project (implementation)
    i_mem_address <= i_mem_address_s(31 downto 0);
    i_mem_reset <= reset_in;
    data_mem_reset <= reset_in;
    annotations_mem_reset <= reset_in;
    annotations_bb_reset <= reset_in;
    trace_mem_reset <= reset_in;
    kernel2monitor_mem_reset <= reset_in;
    monitor2kernel_mem_reset <= reset_in;

    mem_op <= mem_op_s;
    mem_op_s <= mem_write or mem_read;

    mem_address <= cpu_mem_address(31 downto 0);
    mem_data_w <= cpu_mem_data_w;
    mem_sel <= cpu_mem_sel;
    data_mem_en <= mem_op_s and data_mem_cs;

    annotations_mem_address <= cpu_mem_address(31 downto 0);
    annotations_mem_data_w <= cpu_mem_data_w;
    annotations_mem_sel <= cpu_mem_sel;    
    annotations_mem_en <= mem_op_s and annotations_mem_cs;

    annotations_bb_mem_address <= cpu_mem_address(31 downto 0);
    annotations_bb_mem_data_w <= cpu_mem_data_w;
    annotations_bb_mem_sel <= cpu_mem_sel;
    annotations_bb_en <= mem_op_s and annotations_bb_cs;

    trace_mem_address <= cpu_mem_address(31 downto 0);
    trace_mem_data_w <= cpu_mem_data_w;
    trace_mem_sel <= cpu_mem_sel;
    trace_mem_en <= mem_op_s and trace_mem_cs;
    
    
    monitor2kernel_mem_address <= cpu_mem_address(31 downto 0);
    monitor2kernel_mem_data_w <= cpu_mem_data_w;
    monitor2kernel_mem_sel <= cpu_mem_sel;
    monitor2kernel_mem_en <= mem_op_s and trace_mem_cs;
    
    kernel2monitor_mem_address <= cpu_mem_address(31 downto 0);
    kernel2monitor_mem_data_w <= cpu_mem_data_w;
    kernel2monitor_mem_sel <= cpu_mem_sel;
    kernel2monitor_mem_en <= mem_op_s and trace_mem_cs;
--    process(clk)
--    begin
--        if rising_edge(clk) then
--            if reset_in = '1' then  
--                i_mem_en <= '0';
--            else
--                i_mem_en <= '1';
--            end if;
--        end if;
--    end process;
    
    process(clk)
    begin
        if rising_edge(clk) then 
            if reset_in = '1' then 
                data_mem_cs_r <= '0';
                annotations_mem_cs_r <= '0';
                annotations_bb_cs_r <= '0';
                trace_mem_cs_r <= '0';
                monitor2kernel_cs_r <= '0';
                kernel2monitor_cs_r <= '0';
            else
                data_mem_cs_r <= data_mem_cs;
                annotations_mem_cs_r <= annotations_mem_cs;
                annotations_bb_cs_r <= annotations_bb_cs;
                trace_mem_cs_r <= trace_mem_cs;
                monitor2kernel_cs_r <= monitor2kernel_mem_cs;
                kernel2monitor_cs_r <= kernel2monitor_mem_cs;
            end if;
        end if;
    end process;


    cpu_mem_data_r <= 
        mem_data_r when data_mem_cs_r = '1' else
        annotations_mem_data_r when annotations_mem_cs_r = '1' else 
        annotations_bb_mem_data_r when annotations_bb_cs_r = '1' else 
        trace_mem_data_r when trace_mem_cs_r = '1' else
        kernel2monitor_mem_data_r when kernel2monitor_cs_r = '1' else
        monitor2kernel_mem_data_r when monitor2kernel_cs_r = '1' else
        mem_data_r; -- default case


    ucpu: mips_cpu port map(
        clk                 => clk          ,
        reset_in            => reset_in     ,
        --stall             => --stall      ,
        intr_in             => intr_in      ,
        
        i_mem_enable        => i_mem_en,
        i_mem_address       => i_mem_address_s,        
        i_mem_data_r        => i_mem_data_r ,
        i_mem_sel           => i_mem_sel    ,
        
        mem_address         => cpu_mem_address  ,
        mem_data_w          => cpu_mem_data_w   ,
        mem_data_r          => cpu_mem_data_r   ,
        mem_sel             => cpu_mem_sel      ,
        mem_write           => mem_write    ,
        mem_read            => mem_read     ,
        mem_pause           => mem_pause    ,

        data_mem_cs         => data_mem_cs       ,
        annotations_mem_cs  => annotations_mem_cs,
        annotations_bb_cs   => annotations_bb_cs ,
        trace_mem_cs        => trace_mem_cs,
        kernel2monitor_cs   => kernel2monitor_mem_cs,
        monitor2kernel_cs   => monitor2kernel_mem_cs 
        );
        
        cpu_mem_en <= mem_write or mem_read; 
        cpu_mem_addr <= cpu_mem_address(31 downto 2);
        cpu_mem_data <= cpu_mem_data_w;
        cpu_data_read <= cpu_mem_data_r;
    
end architecture;