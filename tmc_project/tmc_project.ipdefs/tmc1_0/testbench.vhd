---------------------------------------------------------------------
-- TITLE: Test Bench
-- AUTHOR: MAW
-- DATE CREATED: 25/10/2017
-- FILENAME: tbench.vhd
-- PROJECT: DIFT Coprocessor
-- DESCRIPTION:
--    This entity provides a test bench for testing the MIPS CPU core.
---------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.math_real.all;
use ieee.numeric_std.all;
use work.dependencies_pack.all;

entity testbench is
  
end; --entity testbench

architecture logic of testbench is
  
component tmc is
  Generic
        (
        data_width : integer range 32 to 64 := 32;
        addr_width : integer := 32
        );

  Port
        (
	clock              : in  std_logic;
        reset_in           : in  std_logic;

	-- axi master interface
	axim_done	: in std_logic;
	axim_error_in: in std_logic;

	axim_address : out std_logic_vector(31 downto 0);
	axim_data_out : out std_logic_vector(31 downto 0);
	axim_data_in : in std_logic_vector(31 downto 0);
	axim_read_en : out std_logic;
	axim_write_en : out std_logic;

	axim_start	: out std_logic;

	reset_out1 : out std_logic;

	i_mem_sel : out  std_logic_vector(3 downto 0);
     --   mem_write    : in  std_logic;
	i_mem_en      : out std_logic;
        i_mem_address  : out  std_logic_vector(31 downto 0);
        i_mem_data_w  : out std_logic_vector(31 downto 0);
        i_mem_data_r  : in std_logic_vector(31 downto 0)        
        );
end component; --entity tmc
  
  component ram
    generic(load_file_name :    string); 
    port(clk, reset        : in std_logic;
      mem_byte_sel : in  std_logic_vector(3 downto 0);
      mem_write    : in  std_logic;
      mem_address  : in  std_logic_vector;
      mem_data_w   : in  std_logic_vector(31 downto 0);
      mem_data_r   : out std_logic_vector(31 downto 0));
  end component;
  
  
  signal clk            : std_logic := '1';
  signal reset_out          : std_logic; --, '0' after 100 ns;
  signal reset1          : std_logic := '1';
  
  
  
  signal i_mem_address  : std_logic_vector(31 downto 0);
  signal i_mem_data_w   : std_logic_vector(31 downto 0);
  signal i_mem_data_r   : std_logic_vector(31 downto 0);
  

      -- local BRAM interface
    signal i_mem_en : std_logic;
    signal i_mem_sel : std_logic_vector(3 downto 0);

	signal axim_done	: std_logic;
	signal axim_error_in: std_logic;

	signal axim_address : std_logic_vector(31 downto 0);
	signal axim_data_out : std_logic_vector(31 downto 0);
	signal axim_data_in : std_logic_vector(31 downto 0);
	signal axim_read_en : std_logic;
	signal axim_write_en : std_logic;

	signal axim_start	: std_logic;	
  

begin --architecture
  --  i_mem_en <= pc_enable;
  -- initialize security policy registers (TPRs and TCRs)
  -- only in simulation 

  clk         <= not clk after 10 ns;
  
  reset1       <= '0' after 200 ns; 	
  
    u1 : tmc 
  Generic map
        (
        32,
        32
        )

  Port map
        (
	clk,
	reset1,

	axim_done,
	axim_error_in,

	axim_address,
	axim_data_out,
	axim_data_in,
	axim_read_en,
	axim_write_en,

	axim_start,

	reset_out,
	
     	i_mem_sel,
	i_mem_en,
        i_mem_address,
        i_mem_data_w,
        i_mem_data_r
        );
  
    u2 : ram generic map ("/lab/home/labsticc/biswas/tmc/code_dependencies.txt")
    PORT MAP (
      clk          => clk,
      reset        => reset1,
      mem_byte_sel => i_mem_sel,
      mem_write    => '0',
      mem_address  => i_mem_address(inst_mem_address_width-1 downto 0),
      mem_data_w   => i_mem_data_w,
      mem_data_r   => i_mem_data_r);

  

end; --architecture logic

