----------
--
-- AUTHOR               : M.Abdul WAHAB
-- DATE                 : 2017
-- Description          : Pipeline registers for stage ID/IE
----------
library IEEE; 
use ieee.std_logic_1164.All;
use work.mips_pack.all;

entity pipeline_ID_IE is 
port    (
        clk, reset      : in std_logic;
        stall           : in std_logic;

        stall_if        : in  std_logic;
        stall_if_out    : out std_logic;

        a_out_in        : in std_logic_vector(31 downto 0);
        a_out           : out std_logic_vector(31 downto 0);
           
        b_out_in        : in std_logic_vector(31 downto 0);
        b_out           : out std_logic_vector(31 downto 0);

        a_source        : in  a_source_type;
        a_source_out    : out a_source_type;

        b_source        : in  b_source_type;
        b_source_out    : out b_source_type;

        c_source        : in  c_source_type;
        c_source_out    : out c_source_type;

        c_pc            : in  std_logic_vector(31 downto 0);
        c_pc_out        : out std_logic_vector(31 downto 0);

        imm_in          : in std_logic_vector(15 downto 0);
        imm_out         : out std_logic_vector(15 downto 0);
        
        reg_source      : in  std_logic_vector(31 downto 0);
        reg_source_out  : out std_logic_vector(31 downto 0);
        
        rs_index        : in std_logic_vector(5 downto 0);
        rs_index_out    : out std_logic_vector(5 downto 0);

        rt_index        : in std_logic_vector(5 downto 0);
        rt_index_out    : out std_logic_vector(5 downto 0);

        rd_index        : in std_logic_vector(5 downto 0);
        rd_index_out    : out std_logic_vector(5 downto 0);

        reg_target      : in  std_logic_vector(31 downto 0);
        reg_target_out  : out  std_logic_vector(31 downto 0);

        take_branch_in  : in std_logic;
        take_branch     : out std_logic;

        ldr_instr       : in  std_logic; 
        ldr_instr_out   : out std_logic;        

        alu_func_i      : in alu_function_type;
        shift_func_i    : in shift_function_type;
        mult_func_i     : in mult_function_type;
        branch_func_i   : in branch_function_type;        
        pc_source       : in pc_source_type;
        mem_source      : in mem_source_type;

        alu_func        : out alu_function_type;
        shift_func      : out shift_function_type;
        mult_func       : out mult_function_type;
        branch_func     : out branch_function_type;
        pc_source_out   : out pc_source_type;
        mem_source_out  : out mem_source_type );
end entity;

architecture behavioral of pipeline_ID_IE is 

signal a_out_reg                : std_logic_vector(31 downto 0);
signal b_out_reg                : std_logic_vector(31 downto 0);
signal imm_out_reg              : std_logic_vector(15 downto 0);
signal take_branch_reg          : std_logic;
signal alu_func_reg             : alu_function_type;
signal shift_func_reg           : shift_function_type;
signal mult_func_reg            : mult_function_type;
signal branch_func_reg          : branch_function_type;
signal pc_source_out_reg        : pc_source_type;
signal mem_source_out_reg       : mem_source_type;
signal a_source_out_reg         : a_source_type;
signal b_source_out_reg         : b_source_type;
signal c_source_out_reg         : c_source_type;
signal c_pc_out_reg             : std_logic_vector(31 downto 0);
signal rs_index_out_reg         : std_logic_vector(5 downto 0);
signal rt_index_out_reg         : std_logic_vector(5 downto 0);
signal rd_index_out_reg         : std_logic_vector(5 downto 0);
signal reg_source_reg           : std_logic_vector(31 downto 0);
signal reg_target_reg           : std_logic_vector(31 downto 0);
signal stall_r                  : std_logic;
signal stall_if_r               : std_logic;
signal ldr_instr_reg            : std_logic;

begin

process(clk) 
begin
        if rising_edge(clk) then 
                if (reset = '1' or stall = '1') then 
                        a_out_reg          <= (others => '0');
                        b_out_reg          <= (others => '0');
                        imm_out_reg        <= (others => '0');       
                        take_branch_reg    <= '0';
                        alu_func_reg       <= (others => '0');
                        shift_func_reg     <= (others => '0');
                        mult_func_reg      <= (others => '0');
                        branch_func_reg    <= (others => '0');                       
                        pc_source_out_reg  <= (others => '0');
                        mem_source_out_reg <= (others => '0');
                        a_source_out_reg   <= (others => '0');
                        b_source_out_reg   <= (others => '0');
                        c_source_out_reg   <= (others => '0');
                        c_pc_out_reg       <= (others => '0');
                        rs_index_out_reg   <= (others => '0');
                        rt_index_out_reg   <= (others => '0');
                        rd_index_out_reg   <= (others => '0');
                        reg_source_reg     <= (others => '0');
                        reg_target_reg     <= (others => '0');
                        ldr_instr_reg      <= '0';
                elsif stall  = '0' then        
                        a_out_reg          <= a_out_in;
                        b_out_reg          <= b_out_in;
                        imm_out_reg        <= imm_in;
                        take_branch_reg    <= take_branch_in;
                        alu_func_reg       <= alu_func_i;
                        shift_func_reg     <= shift_func_i;
                        mult_func_reg      <= mult_func_i;
                        branch_func_reg    <= branch_func_i;                        
                        pc_source_out_reg  <= pc_source ;
                        mem_source_out_reg <= mem_source;
                        a_source_out_reg   <= a_source;
                        b_source_out_reg   <= b_source;
                        c_source_out_reg   <= c_source;
                        c_pc_out_reg       <= c_pc;
                        rs_index_out_reg   <= rs_index;
                        rt_index_out_reg   <= rt_index;
                        rd_index_out_reg   <= rd_index;                                        
                        reg_source_reg     <=  reg_source;
                        reg_target_reg     <=  reg_target;
                        ldr_instr_reg      <= ldr_instr;
                end if;
        end if;
end process;

process(clk)
begin
        if rising_edge(clk) then
            if reset = '1' then
                stall_r <= '0';
                stall_if_r         <= '0';
            else
                stall_r <= stall;
                stall_if_r         <= stall_if;
            end if;
        end if;
end process;


a_out          <= a_out_reg         ;
b_out          <= b_out_reg         ;
imm_out        <= imm_out_reg       ;
take_branch    <= take_branch_reg   ;
alu_func       <= alu_func_reg      ;
shift_func     <= shift_func_reg    ;
mult_func      <= mult_func_reg     ;
branch_func    <= branch_func_reg   ;
pc_source_out  <= pc_source_out_reg ;
mem_source_out <= mem_source_out_reg;
a_source_out   <= a_source_out_reg  ;
b_source_out   <= b_source_out_reg  ;
c_source_out   <= c_source_out_reg  ;
c_pc_out       <= c_pc_out_reg      ;
rs_index_out   <= rs_index_out_reg  ;
rt_index_out   <= rt_index_out_reg  ;
rd_index_out   <= rd_index_out_reg  ;
reg_source_out <= reg_source_reg;
reg_target_out <= reg_target_reg;
ldr_instr_out  <= ldr_instr_reg;
stall_if_out   <= stall_if_r;

end architecture;