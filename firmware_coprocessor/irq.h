

#define IRQ_REASON_MEMORY_TAG_FULL                  0
#define IRQ_REASON_CHECK_FAILED                     1
#define IRQ_REASON_SYSCALL_REQUEST_ALLOC_MEMORY     2
#define IRQ_REASON_SYSCALL_REQUEST_DEALLOC_MEMORY   3
#define IRQ_REASON_MEMORY_TAG_UNFOUND_ADDRESS       4

inline void raise_irq_when_unfound_memory_address(uint32_t tagged_address, uint32_t address, uint32_t size, uint32_t tag);

inline void raise_irq_when_mem_tag_full(uint32_t tagged_address, uint32_t address, uint32_t size, uint32_t tag);

inline void raise_irq_when_check_failed(uint32_t address, uint32_t size, uint32_t tag_of_memory, uint32_t tag);

inline void raise_irq_when_alloc_memory_failed(uint32_t address, uint32_t size, uint32_t tag_of_memory, uint32_t tag);

inline void raise_irq_when_dealloc_memory_failed(uint32_t address, uint32_t size, uint32_t tag_of_memory, uint32_t tag);
