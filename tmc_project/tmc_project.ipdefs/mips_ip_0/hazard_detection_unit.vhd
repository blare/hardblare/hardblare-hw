library ieee;
use ieee.std_logic_1164.all;
use work.mips_pack.all;

entity hazard_detection_unit is 
port (
	rs_index_dec 	: in std_logic_vector(5 downto 0); -- exec stage
	rt_index_dec 	: in std_logic_vector(5 downto 0); -- exec stage
	rt_index_ex		: in std_logic_vector(5 downto 0); -- mem stage
	c_source_ex		: in c_source_type; 
	pc_pause		: out std_logic;	
	hazard 			: out std_logic	);
end entity;

architecture behavioral of hazard_detection_unit is 

begin 	

	process(rs_index_dec, rt_index_dec, rt_index_ex, c_source_ex)	
	begin
		if (c_source_ex = c_from_memory and 
			( rt_index_ex = rs_index_dec or 
			  rt_index_ex = rt_index_dec) ) then 			
			pc_pause <= '1';
			hazard <= '1'; 
		else 			
			pc_pause <= '0';
			hazard <= '0';
		end if;	
	end process;
end architecture;