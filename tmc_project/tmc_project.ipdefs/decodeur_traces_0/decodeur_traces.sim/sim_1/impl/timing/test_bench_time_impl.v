// Copyright 1986-2014 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2014.4 (win64) Build 1071353 Tue Nov 18 18:29:27 MST 2014
// Date        : Tue Jun 14 19:57:42 2016
// Host        : RPHPZB1401 running 64-bit Service Pack 1  (build 7601)
// Command     : write_verilog -mode timesim -nolib -sdf_anno true -force -file
//               C:/Users/wahab_muh.RPHPZB1401/Documents/these/these_05_06_2016/decodeur_traces/decodeur_traces.sim/sim_1/impl/timing/test_bench_time_impl.v
// Design      : decodeur_traces
// Purpose     : This verilog netlist is a timing simulation representation of the design and should not be modified or
//               synthesized. Please ensure that this netlist is used with the corresponding SDF file.
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps
`define XIL_TIMING

module datapath
   (clk,
    reset,
    enable,
    trace_data_in,
    start_b,
    start_i,
    start_w,
    stop_b,
    stop_i,
    stop_w,
    pc,
    out_en,
    fifo_overflow,
    waypoint_address,
    context_id,
    waypoint_address_en);
  input clk;
  input reset;
  input enable;
  input [7:0]trace_data_in;
  input start_b;
  input start_i;
  input start_w;
  output stop_b;
  output stop_i;
  output stop_w;
  output [31:0]pc;
  output out_en;
  output fifo_overflow;
  output [31:0]waypoint_address;
  output [31:0]context_id;
  output waypoint_address_en;

  wire clk;
  wire enable;
  wire enable_i;
  wire [31:0]instruction_address;
  wire out_en;
  wire [31:0]pc;
  wire reset;
  wire start_b;
  wire start_i;
  wire stop_b;
  wire stop_i;
  wire [7:0]trace_data_in;
  wire NLW_u_decode_bap_atom_output_e_UNCONNECTED;
  wire NLW_u_decode_i_sync_fifo_overflow_UNCONNECTED;
  wire [31:0]NLW_u_decode_i_sync_context_id_out_UNCONNECTED;

decode_bap u_decode_bap
       (.atom_output_e(NLW_u_decode_bap_atom_output_e_UNCONNECTED),
        .clk(clk),
        .data_in(trace_data_in),
        .enable(enable),
        .enable_i(enable_i),
        .instruction_address(instruction_address),
        .pc(pc),
        .reset(reset),
        .start_b(start_b),
        .stop_b(stop_b),
        .w_en(out_en));
decode_i_sync u_decode_i_sync
       (.clk(clk),
        .context_id_out(NLW_u_decode_i_sync_context_id_out_UNCONNECTED[31:0]),
        .data_in(trace_data_in),
        .data_out(instruction_address),
        .enable(enable),
        .fifo_overflow(NLW_u_decode_i_sync_fifo_overflow_UNCONNECTED),
        .out_en(enable_i),
        .reset(reset),
        .start_i(start_i),
        .stop_i(stop_i));
endmodule

module decode_bap
   (clk,
    reset,
    start_b,
    enable,
    enable_i,
    data_in,
    instruction_address,
    stop_b,
    w_en,
    atom_output_e,
    pc);
  input clk;
  input reset;
  input start_b;
  input enable;
  input enable_i;
  input [7:0]data_in;
  input [31:0]instruction_address;
  output stop_b;
  output w_en;
  output atom_output_e;
  output [31:0]pc;

  wire clk;
  wire [7:0]data_in;
  wire [7:0]data_reg_s;
  wire enable;
  wire enable_i;
  wire [31:0]instruction_address;
  wire [31:0]instruction_address_reg;
  wire \n_0_output_data_4[0]_i_1 ;
  wire \n_0_output_data_4[1]_i_1 ;
  wire \n_0_output_data_4[2]_i_1 ;
  wire \n_0_pc[10]_i_2 ;
  wire \n_0_pc[11]_i_2 ;
  wire \n_0_pc[12]_i_2 ;
  wire \n_0_pc[13]_i_2 ;
  wire \n_0_pc[14]_i_2 ;
  wire \n_0_pc[15]_i_2 ;
  wire \n_0_pc[16]_i_2 ;
  wire \n_0_pc[17]_i_2 ;
  wire \n_0_pc[18]_i_2 ;
  wire \n_0_pc[19]_i_2 ;
  wire \n_0_pc[20]_i_2 ;
  wire \n_0_pc[21]_i_2 ;
  wire \n_0_pc[22]_i_2 ;
  wire \n_0_pc[23]_i_2 ;
  wire \n_0_pc[24]_i_2 ;
  wire \n_0_pc[25]_i_2 ;
  wire \n_0_pc[26]_i_2 ;
  wire \n_0_pc[27]_i_2 ;
  wire \n_0_pc[28]_i_2 ;
  wire \n_0_pc[29]_i_2 ;
  wire \n_0_pc[30]_i_2 ;
  wire \n_0_pc[31]_i_2 ;
  wire \n_0_pc[31]_i_3 ;
  wire \n_0_pc[7]_i_2 ;
  wire \n_0_pc[8]_i_2 ;
  wire \n_0_pc[9]_i_2 ;
  wire \n_0_state_reg[0]_i_1 ;
  wire \n_0_state_reg[0]_i_2 ;
  wire \n_0_state_reg[1]_i_1 ;
  wire \n_0_state_reg[2]_i_1 ;
  wire \n_0_state_reg[3]_i_1 ;
  wire \n_0_state_reg_reg[0] ;
  wire \n_0_temp_state[0]_i_2 ;
  wire \n_0_temp_state[0]_i_3 ;
  wire \n_0_temp_state[0]_i_4 ;
  wire \n_0_temp_state[1]_i_2 ;
  wire \n_0_temp_state[1]_i_3 ;
  wire \n_0_temp_state[1]_i_4 ;
  wire \n_0_temp_state[3]_i_1 ;
  wire n_0_w_en_i_1;
  wire [5:0]output_data_0;
  wire output_data_0_s;
  wire output_data_0_s_reg;
  wire [6:0]output_data_1;
  wire output_data_1_s;
  wire output_data_1_s_reg;
  wire [6:0]output_data_2;
  wire output_data_2_s;
  wire output_data_2_s_reg;
  wire [6:0]output_data_3;
  wire output_data_3_s;
  wire output_data_3_s_reg;
  wire [2:0]output_data_4;
  wire output_data_4_s;
  wire output_data_4_s_reg;
  wire [31:0]pc;
  wire [31:0]pc_s;
  wire [31:0]pc_s_reg;
  wire reset;
  wire start_b;
  wire [3:0]state_next;
  wire [3:1]state_reg;
  wire stop_b;
  wire [5:5]temp_signal;
  wire [3:0]temp_state;
  wire w_en;

FDRE #(
    .INIT(1'b0)) 
     \data_reg_s_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(data_in[0]),
        .Q(data_reg_s[0]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \data_reg_s_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(data_in[1]),
        .Q(data_reg_s[1]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \data_reg_s_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(data_in[2]),
        .Q(data_reg_s[2]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \data_reg_s_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(data_in[3]),
        .Q(data_reg_s[3]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \data_reg_s_reg[4] 
       (.C(clk),
        .CE(1'b1),
        .D(data_in[4]),
        .Q(data_reg_s[4]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \data_reg_s_reg[5] 
       (.C(clk),
        .CE(1'b1),
        .D(data_in[5]),
        .Q(data_reg_s[5]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \data_reg_s_reg[6] 
       (.C(clk),
        .CE(1'b1),
        .D(data_in[6]),
        .Q(data_reg_s[6]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \data_reg_s_reg[7] 
       (.C(clk),
        .CE(1'b1),
        .D(data_in[7]),
        .Q(data_reg_s[7]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     enable_i_reg_reg
       (.C(clk),
        .CE(1'b1),
        .D(enable_i),
        .Q(temp_signal),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \instruction_address_reg_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(instruction_address[0]),
        .Q(instruction_address_reg[0]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \instruction_address_reg_reg[10] 
       (.C(clk),
        .CE(1'b1),
        .D(instruction_address[10]),
        .Q(instruction_address_reg[10]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \instruction_address_reg_reg[11] 
       (.C(clk),
        .CE(1'b1),
        .D(instruction_address[11]),
        .Q(instruction_address_reg[11]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \instruction_address_reg_reg[12] 
       (.C(clk),
        .CE(1'b1),
        .D(instruction_address[12]),
        .Q(instruction_address_reg[12]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \instruction_address_reg_reg[13] 
       (.C(clk),
        .CE(1'b1),
        .D(instruction_address[13]),
        .Q(instruction_address_reg[13]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \instruction_address_reg_reg[14] 
       (.C(clk),
        .CE(1'b1),
        .D(instruction_address[14]),
        .Q(instruction_address_reg[14]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \instruction_address_reg_reg[15] 
       (.C(clk),
        .CE(1'b1),
        .D(instruction_address[15]),
        .Q(instruction_address_reg[15]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \instruction_address_reg_reg[16] 
       (.C(clk),
        .CE(1'b1),
        .D(instruction_address[16]),
        .Q(instruction_address_reg[16]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \instruction_address_reg_reg[17] 
       (.C(clk),
        .CE(1'b1),
        .D(instruction_address[17]),
        .Q(instruction_address_reg[17]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \instruction_address_reg_reg[18] 
       (.C(clk),
        .CE(1'b1),
        .D(instruction_address[18]),
        .Q(instruction_address_reg[18]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \instruction_address_reg_reg[19] 
       (.C(clk),
        .CE(1'b1),
        .D(instruction_address[19]),
        .Q(instruction_address_reg[19]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \instruction_address_reg_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(instruction_address[1]),
        .Q(instruction_address_reg[1]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \instruction_address_reg_reg[20] 
       (.C(clk),
        .CE(1'b1),
        .D(instruction_address[20]),
        .Q(instruction_address_reg[20]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \instruction_address_reg_reg[21] 
       (.C(clk),
        .CE(1'b1),
        .D(instruction_address[21]),
        .Q(instruction_address_reg[21]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \instruction_address_reg_reg[22] 
       (.C(clk),
        .CE(1'b1),
        .D(instruction_address[22]),
        .Q(instruction_address_reg[22]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \instruction_address_reg_reg[23] 
       (.C(clk),
        .CE(1'b1),
        .D(instruction_address[23]),
        .Q(instruction_address_reg[23]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \instruction_address_reg_reg[24] 
       (.C(clk),
        .CE(1'b1),
        .D(instruction_address[24]),
        .Q(instruction_address_reg[24]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \instruction_address_reg_reg[25] 
       (.C(clk),
        .CE(1'b1),
        .D(instruction_address[25]),
        .Q(instruction_address_reg[25]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \instruction_address_reg_reg[26] 
       (.C(clk),
        .CE(1'b1),
        .D(instruction_address[26]),
        .Q(instruction_address_reg[26]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \instruction_address_reg_reg[27] 
       (.C(clk),
        .CE(1'b1),
        .D(instruction_address[27]),
        .Q(instruction_address_reg[27]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \instruction_address_reg_reg[28] 
       (.C(clk),
        .CE(1'b1),
        .D(instruction_address[28]),
        .Q(instruction_address_reg[28]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \instruction_address_reg_reg[29] 
       (.C(clk),
        .CE(1'b1),
        .D(instruction_address[29]),
        .Q(instruction_address_reg[29]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \instruction_address_reg_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(instruction_address[2]),
        .Q(instruction_address_reg[2]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \instruction_address_reg_reg[30] 
       (.C(clk),
        .CE(1'b1),
        .D(instruction_address[30]),
        .Q(instruction_address_reg[30]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \instruction_address_reg_reg[31] 
       (.C(clk),
        .CE(1'b1),
        .D(instruction_address[31]),
        .Q(instruction_address_reg[31]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \instruction_address_reg_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(instruction_address[3]),
        .Q(instruction_address_reg[3]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \instruction_address_reg_reg[4] 
       (.C(clk),
        .CE(1'b1),
        .D(instruction_address[4]),
        .Q(instruction_address_reg[4]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \instruction_address_reg_reg[5] 
       (.C(clk),
        .CE(1'b1),
        .D(instruction_address[5]),
        .Q(instruction_address_reg[5]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \instruction_address_reg_reg[6] 
       (.C(clk),
        .CE(1'b1),
        .D(instruction_address[6]),
        .Q(instruction_address_reg[6]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \instruction_address_reg_reg[7] 
       (.C(clk),
        .CE(1'b1),
        .D(instruction_address[7]),
        .Q(instruction_address_reg[7]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \instruction_address_reg_reg[8] 
       (.C(clk),
        .CE(1'b1),
        .D(instruction_address[8]),
        .Q(instruction_address_reg[8]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \instruction_address_reg_reg[9] 
       (.C(clk),
        .CE(1'b1),
        .D(instruction_address[9]),
        .Q(instruction_address_reg[9]),
        .R(reset));
LUT5 #(
    .INIT(32'hAAAB0000)) 
     \output_data_0[5]_i_1 
       (.I0(\n_0_state_reg_reg[0] ),
        .I1(state_reg[1]),
        .I2(state_reg[3]),
        .I3(state_reg[2]),
        .I4(start_b),
        .O(output_data_0_s));
FDRE #(
    .INIT(1'b0)) 
     \output_data_0_reg[0] 
       (.C(clk),
        .CE(output_data_0_s),
        .D(data_reg_s[1]),
        .Q(output_data_0[0]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \output_data_0_reg[1] 
       (.C(clk),
        .CE(output_data_0_s),
        .D(data_reg_s[2]),
        .Q(output_data_0[1]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \output_data_0_reg[2] 
       (.C(clk),
        .CE(output_data_0_s),
        .D(data_reg_s[3]),
        .Q(output_data_0[2]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \output_data_0_reg[3] 
       (.C(clk),
        .CE(output_data_0_s),
        .D(data_reg_s[4]),
        .Q(output_data_0[3]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \output_data_0_reg[4] 
       (.C(clk),
        .CE(output_data_0_s),
        .D(data_reg_s[5]),
        .Q(output_data_0[4]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \output_data_0_reg[5] 
       (.C(clk),
        .CE(output_data_0_s),
        .D(data_reg_s[6]),
        .Q(output_data_0[5]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     output_data_0_s_reg_reg
       (.C(clk),
        .CE(1'b1),
        .D(output_data_0_s),
        .Q(output_data_0_s_reg),
        .R(reset));
LUT4 #(
    .INIT(16'h0004)) 
     \output_data_1[6]_i_1 
       (.I0(state_reg[2]),
        .I1(state_reg[1]),
        .I2(state_reg[3]),
        .I3(\n_0_state_reg_reg[0] ),
        .O(output_data_1_s));
FDRE #(
    .INIT(1'b0)) 
     \output_data_1_reg[0] 
       (.C(clk),
        .CE(output_data_1_s),
        .D(data_reg_s[0]),
        .Q(output_data_1[0]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \output_data_1_reg[1] 
       (.C(clk),
        .CE(output_data_1_s),
        .D(data_reg_s[1]),
        .Q(output_data_1[1]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \output_data_1_reg[2] 
       (.C(clk),
        .CE(output_data_1_s),
        .D(data_reg_s[2]),
        .Q(output_data_1[2]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \output_data_1_reg[3] 
       (.C(clk),
        .CE(output_data_1_s),
        .D(data_reg_s[3]),
        .Q(output_data_1[3]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \output_data_1_reg[4] 
       (.C(clk),
        .CE(output_data_1_s),
        .D(data_reg_s[4]),
        .Q(output_data_1[4]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \output_data_1_reg[5] 
       (.C(clk),
        .CE(output_data_1_s),
        .D(data_reg_s[5]),
        .Q(output_data_1[5]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \output_data_1_reg[6] 
       (.C(clk),
        .CE(output_data_1_s),
        .D(data_reg_s[6]),
        .Q(output_data_1[6]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     output_data_1_s_reg_reg
       (.C(clk),
        .CE(1'b1),
        .D(output_data_1_s),
        .Q(output_data_1_s_reg),
        .R(reset));
LUT4 #(
    .INIT(16'h0004)) 
     \output_data_2[6]_i_1 
       (.I0(state_reg[1]),
        .I1(state_reg[2]),
        .I2(state_reg[3]),
        .I3(\n_0_state_reg_reg[0] ),
        .O(output_data_2_s));
FDRE #(
    .INIT(1'b0)) 
     \output_data_2_reg[0] 
       (.C(clk),
        .CE(output_data_2_s),
        .D(data_reg_s[0]),
        .Q(output_data_2[0]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \output_data_2_reg[1] 
       (.C(clk),
        .CE(output_data_2_s),
        .D(data_reg_s[1]),
        .Q(output_data_2[1]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \output_data_2_reg[2] 
       (.C(clk),
        .CE(output_data_2_s),
        .D(data_reg_s[2]),
        .Q(output_data_2[2]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \output_data_2_reg[3] 
       (.C(clk),
        .CE(output_data_2_s),
        .D(data_reg_s[3]),
        .Q(output_data_2[3]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \output_data_2_reg[4] 
       (.C(clk),
        .CE(output_data_2_s),
        .D(data_reg_s[4]),
        .Q(output_data_2[4]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \output_data_2_reg[5] 
       (.C(clk),
        .CE(output_data_2_s),
        .D(data_reg_s[5]),
        .Q(output_data_2[5]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \output_data_2_reg[6] 
       (.C(clk),
        .CE(output_data_2_s),
        .D(data_reg_s[6]),
        .Q(output_data_2[6]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     output_data_2_s_reg_reg
       (.C(clk),
        .CE(1'b1),
        .D(output_data_2_s),
        .Q(output_data_2_s_reg),
        .R(reset));
LUT4 #(
    .INIT(16'h1000)) 
     \output_data_3[6]_i_1 
       (.I0(state_reg[3]),
        .I1(\n_0_state_reg_reg[0] ),
        .I2(state_reg[2]),
        .I3(state_reg[1]),
        .O(output_data_3_s));
FDRE #(
    .INIT(1'b0)) 
     \output_data_3_reg[0] 
       (.C(clk),
        .CE(output_data_3_s),
        .D(data_reg_s[0]),
        .Q(output_data_3[0]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \output_data_3_reg[1] 
       (.C(clk),
        .CE(output_data_3_s),
        .D(data_reg_s[1]),
        .Q(output_data_3[1]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \output_data_3_reg[2] 
       (.C(clk),
        .CE(output_data_3_s),
        .D(data_reg_s[2]),
        .Q(output_data_3[2]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \output_data_3_reg[3] 
       (.C(clk),
        .CE(output_data_3_s),
        .D(data_reg_s[3]),
        .Q(output_data_3[3]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \output_data_3_reg[4] 
       (.C(clk),
        .CE(output_data_3_s),
        .D(data_reg_s[4]),
        .Q(output_data_3[4]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \output_data_3_reg[5] 
       (.C(clk),
        .CE(output_data_3_s),
        .D(data_reg_s[5]),
        .Q(output_data_3[5]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \output_data_3_reg[6] 
       (.C(clk),
        .CE(output_data_3_s),
        .D(data_reg_s[6]),
        .Q(output_data_3[6]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     output_data_3_s_reg_reg
       (.C(clk),
        .CE(1'b1),
        .D(output_data_3_s),
        .Q(output_data_3_s_reg),
        .R(reset));
LUT6 #(
    .INIT(64'hFFFFFEFF00000200)) 
     \output_data_4[0]_i_1 
       (.I0(data_reg_s[0]),
        .I1(state_reg[2]),
        .I2(state_reg[1]),
        .I3(state_reg[3]),
        .I4(\n_0_state_reg_reg[0] ),
        .I5(output_data_4[0]),
        .O(\n_0_output_data_4[0]_i_1 ));
LUT6 #(
    .INIT(64'hFFFFFEFF00000200)) 
     \output_data_4[1]_i_1 
       (.I0(data_reg_s[1]),
        .I1(state_reg[2]),
        .I2(state_reg[1]),
        .I3(state_reg[3]),
        .I4(\n_0_state_reg_reg[0] ),
        .I5(output_data_4[1]),
        .O(\n_0_output_data_4[1]_i_1 ));
LUT6 #(
    .INIT(64'hFFFFFEFF00000200)) 
     \output_data_4[2]_i_1 
       (.I0(data_reg_s[2]),
        .I1(state_reg[2]),
        .I2(state_reg[1]),
        .I3(state_reg[3]),
        .I4(\n_0_state_reg_reg[0] ),
        .I5(output_data_4[2]),
        .O(\n_0_output_data_4[2]_i_1 ));
FDRE #(
    .INIT(1'b0)) 
     \output_data_4_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(\n_0_output_data_4[0]_i_1 ),
        .Q(output_data_4[0]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \output_data_4_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\n_0_output_data_4[1]_i_1 ),
        .Q(output_data_4[1]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \output_data_4_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(\n_0_output_data_4[2]_i_1 ),
        .Q(output_data_4[2]),
        .R(reset));
LUT4 #(
    .INIT(16'h0010)) 
     output_data_4_s_reg_i_1
       (.I0(state_reg[2]),
        .I1(state_reg[1]),
        .I2(state_reg[3]),
        .I3(\n_0_state_reg_reg[0] ),
        .O(output_data_4_s));
FDRE #(
    .INIT(1'b0)) 
     output_data_4_s_reg_reg
       (.C(clk),
        .CE(1'b1),
        .D(output_data_4_s),
        .Q(output_data_4_s_reg),
        .R(reset));
LUT4 #(
    .INIT(16'hCA0A)) 
     \pc[0]_i_1 
       (.I0(pc_s_reg[0]),
        .I1(instruction_address_reg[0]),
        .I2(\n_0_pc[31]_i_3 ),
        .I3(temp_signal),
        .O(pc_s[0]));
LUT6 #(
    .INIT(64'hFFFFFFFFFFD58080)) 
     \pc[10]_i_1 
       (.I0(\n_0_pc[31]_i_3 ),
        .I1(instruction_address_reg[10]),
        .I2(temp_signal),
        .I3(output_data_0_s_reg),
        .I4(pc_s_reg[10]),
        .I5(\n_0_pc[10]_i_2 ),
        .O(pc_s[10]));
LUT6 #(
    .INIT(64'hAAAAAAA800000000)) 
     \pc[10]_i_2 
       (.I0(\n_0_pc[31]_i_3 ),
        .I1(output_data_1_s_reg),
        .I2(output_data_2_s_reg),
        .I3(output_data_3_s_reg),
        .I4(output_data_4_s_reg),
        .I5(output_data_1[2]),
        .O(\n_0_pc[10]_i_2 ));
LUT6 #(
    .INIT(64'hFFFFFFFFFFD58080)) 
     \pc[11]_i_1 
       (.I0(\n_0_pc[31]_i_3 ),
        .I1(instruction_address_reg[11]),
        .I2(temp_signal),
        .I3(output_data_0_s_reg),
        .I4(pc_s_reg[11]),
        .I5(\n_0_pc[11]_i_2 ),
        .O(pc_s[11]));
LUT6 #(
    .INIT(64'hAAAAAAA800000000)) 
     \pc[11]_i_2 
       (.I0(\n_0_pc[31]_i_3 ),
        .I1(output_data_1_s_reg),
        .I2(output_data_2_s_reg),
        .I3(output_data_3_s_reg),
        .I4(output_data_4_s_reg),
        .I5(output_data_1[3]),
        .O(\n_0_pc[11]_i_2 ));
LUT6 #(
    .INIT(64'hFFFFFFFFFFD58080)) 
     \pc[12]_i_1 
       (.I0(\n_0_pc[31]_i_3 ),
        .I1(instruction_address_reg[12]),
        .I2(temp_signal),
        .I3(output_data_0_s_reg),
        .I4(pc_s_reg[12]),
        .I5(\n_0_pc[12]_i_2 ),
        .O(pc_s[12]));
LUT6 #(
    .INIT(64'hAAAAAAA800000000)) 
     \pc[12]_i_2 
       (.I0(\n_0_pc[31]_i_3 ),
        .I1(output_data_1_s_reg),
        .I2(output_data_2_s_reg),
        .I3(output_data_3_s_reg),
        .I4(output_data_4_s_reg),
        .I5(output_data_1[4]),
        .O(\n_0_pc[12]_i_2 ));
LUT6 #(
    .INIT(64'hFFFFFFFFFFD58080)) 
     \pc[13]_i_1 
       (.I0(\n_0_pc[31]_i_3 ),
        .I1(instruction_address_reg[13]),
        .I2(temp_signal),
        .I3(output_data_0_s_reg),
        .I4(pc_s_reg[13]),
        .I5(\n_0_pc[13]_i_2 ),
        .O(pc_s[13]));
LUT6 #(
    .INIT(64'hAAAAAAA800000000)) 
     \pc[13]_i_2 
       (.I0(\n_0_pc[31]_i_3 ),
        .I1(output_data_1_s_reg),
        .I2(output_data_2_s_reg),
        .I3(output_data_3_s_reg),
        .I4(output_data_4_s_reg),
        .I5(output_data_1[5]),
        .O(\n_0_pc[13]_i_2 ));
LUT6 #(
    .INIT(64'hFFFFFFFFFFD58080)) 
     \pc[14]_i_1 
       (.I0(\n_0_pc[31]_i_3 ),
        .I1(instruction_address_reg[14]),
        .I2(temp_signal),
        .I3(output_data_0_s_reg),
        .I4(pc_s_reg[14]),
        .I5(\n_0_pc[14]_i_2 ),
        .O(pc_s[14]));
LUT6 #(
    .INIT(64'hAAAAAAA800000000)) 
     \pc[14]_i_2 
       (.I0(\n_0_pc[31]_i_3 ),
        .I1(output_data_1_s_reg),
        .I2(output_data_2_s_reg),
        .I3(output_data_3_s_reg),
        .I4(output_data_4_s_reg),
        .I5(output_data_1[6]),
        .O(\n_0_pc[14]_i_2 ));
LUT6 #(
    .INIT(64'hEEEEEEEAAAAAAAAA)) 
     \pc[15]_i_1 
       (.I0(\n_0_pc[15]_i_2 ),
        .I1(output_data_2[0]),
        .I2(output_data_2_s_reg),
        .I3(output_data_3_s_reg),
        .I4(output_data_4_s_reg),
        .I5(\n_0_pc[31]_i_3 ),
        .O(pc_s[15]));
LUT6 #(
    .INIT(64'hFFA8A8A8AAAAAAAA)) 
     \pc[15]_i_2 
       (.I0(pc_s_reg[15]),
        .I1(output_data_1_s_reg),
        .I2(output_data_0_s_reg),
        .I3(temp_signal),
        .I4(instruction_address_reg[15]),
        .I5(\n_0_pc[31]_i_3 ),
        .O(\n_0_pc[15]_i_2 ));
LUT6 #(
    .INIT(64'hEEEEEEEAAAAAAAAA)) 
     \pc[16]_i_1 
       (.I0(\n_0_pc[16]_i_2 ),
        .I1(output_data_2[1]),
        .I2(output_data_2_s_reg),
        .I3(output_data_3_s_reg),
        .I4(output_data_4_s_reg),
        .I5(\n_0_pc[31]_i_3 ),
        .O(pc_s[16]));
LUT6 #(
    .INIT(64'hFFA8A8A8AAAAAAAA)) 
     \pc[16]_i_2 
       (.I0(pc_s_reg[16]),
        .I1(output_data_1_s_reg),
        .I2(output_data_0_s_reg),
        .I3(temp_signal),
        .I4(instruction_address_reg[16]),
        .I5(\n_0_pc[31]_i_3 ),
        .O(\n_0_pc[16]_i_2 ));
LUT6 #(
    .INIT(64'hEEEEEEEAAAAAAAAA)) 
     \pc[17]_i_1 
       (.I0(\n_0_pc[17]_i_2 ),
        .I1(output_data_2[2]),
        .I2(output_data_2_s_reg),
        .I3(output_data_3_s_reg),
        .I4(output_data_4_s_reg),
        .I5(\n_0_pc[31]_i_3 ),
        .O(pc_s[17]));
LUT6 #(
    .INIT(64'hFFA8A8A8AAAAAAAA)) 
     \pc[17]_i_2 
       (.I0(pc_s_reg[17]),
        .I1(output_data_1_s_reg),
        .I2(output_data_0_s_reg),
        .I3(temp_signal),
        .I4(instruction_address_reg[17]),
        .I5(\n_0_pc[31]_i_3 ),
        .O(\n_0_pc[17]_i_2 ));
LUT6 #(
    .INIT(64'hEEEEEEEAAAAAAAAA)) 
     \pc[18]_i_1 
       (.I0(\n_0_pc[18]_i_2 ),
        .I1(output_data_2[3]),
        .I2(output_data_2_s_reg),
        .I3(output_data_3_s_reg),
        .I4(output_data_4_s_reg),
        .I5(\n_0_pc[31]_i_3 ),
        .O(pc_s[18]));
LUT6 #(
    .INIT(64'hFFA8A8A8AAAAAAAA)) 
     \pc[18]_i_2 
       (.I0(pc_s_reg[18]),
        .I1(output_data_1_s_reg),
        .I2(output_data_0_s_reg),
        .I3(temp_signal),
        .I4(instruction_address_reg[18]),
        .I5(\n_0_pc[31]_i_3 ),
        .O(\n_0_pc[18]_i_2 ));
LUT6 #(
    .INIT(64'hEEEEEEEAAAAAAAAA)) 
     \pc[19]_i_1 
       (.I0(\n_0_pc[19]_i_2 ),
        .I1(output_data_2[4]),
        .I2(output_data_2_s_reg),
        .I3(output_data_3_s_reg),
        .I4(output_data_4_s_reg),
        .I5(\n_0_pc[31]_i_3 ),
        .O(pc_s[19]));
LUT6 #(
    .INIT(64'hFFA8A8A8AAAAAAAA)) 
     \pc[19]_i_2 
       (.I0(pc_s_reg[19]),
        .I1(output_data_1_s_reg),
        .I2(output_data_0_s_reg),
        .I3(temp_signal),
        .I4(instruction_address_reg[19]),
        .I5(\n_0_pc[31]_i_3 ),
        .O(\n_0_pc[19]_i_2 ));
LUT4 #(
    .INIT(16'hCA0A)) 
     \pc[1]_i_1 
       (.I0(pc_s_reg[1]),
        .I1(instruction_address_reg[1]),
        .I2(\n_0_pc[31]_i_3 ),
        .I3(temp_signal),
        .O(pc_s[1]));
LUT6 #(
    .INIT(64'hEEEEEEEAAAAAAAAA)) 
     \pc[20]_i_1 
       (.I0(\n_0_pc[20]_i_2 ),
        .I1(output_data_2[5]),
        .I2(output_data_2_s_reg),
        .I3(output_data_3_s_reg),
        .I4(output_data_4_s_reg),
        .I5(\n_0_pc[31]_i_3 ),
        .O(pc_s[20]));
LUT6 #(
    .INIT(64'hFFA8A8A8AAAAAAAA)) 
     \pc[20]_i_2 
       (.I0(pc_s_reg[20]),
        .I1(output_data_1_s_reg),
        .I2(output_data_0_s_reg),
        .I3(temp_signal),
        .I4(instruction_address_reg[20]),
        .I5(\n_0_pc[31]_i_3 ),
        .O(\n_0_pc[20]_i_2 ));
LUT6 #(
    .INIT(64'hEEEEEEEAAAAAAAAA)) 
     \pc[21]_i_1 
       (.I0(\n_0_pc[21]_i_2 ),
        .I1(output_data_2[6]),
        .I2(output_data_2_s_reg),
        .I3(output_data_3_s_reg),
        .I4(output_data_4_s_reg),
        .I5(\n_0_pc[31]_i_3 ),
        .O(pc_s[21]));
LUT6 #(
    .INIT(64'hFFA8A8A8AAAAAAAA)) 
     \pc[21]_i_2 
       (.I0(pc_s_reg[21]),
        .I1(output_data_1_s_reg),
        .I2(output_data_0_s_reg),
        .I3(temp_signal),
        .I4(instruction_address_reg[21]),
        .I5(\n_0_pc[31]_i_3 ),
        .O(\n_0_pc[21]_i_2 ));
LUT6 #(
    .INIT(64'hFFFFFFFBAAAAAAAA)) 
     \pc[22]_i_1 
       (.I0(\n_0_pc[22]_i_2 ),
        .I1(\n_0_pc[31]_i_3 ),
        .I2(output_data_2_s_reg),
        .I3(output_data_0_s_reg),
        .I4(output_data_1_s_reg),
        .I5(pc_s_reg[22]),
        .O(pc_s[22]));
LUT6 #(
    .INIT(64'hFFF8888800000000)) 
     \pc[22]_i_2 
       (.I0(temp_signal),
        .I1(instruction_address_reg[22]),
        .I2(output_data_4_s_reg),
        .I3(output_data_3_s_reg),
        .I4(output_data_3[0]),
        .I5(\n_0_pc[31]_i_3 ),
        .O(\n_0_pc[22]_i_2 ));
LUT6 #(
    .INIT(64'hFFFFFFFBAAAAAAAA)) 
     \pc[23]_i_1 
       (.I0(\n_0_pc[23]_i_2 ),
        .I1(\n_0_pc[31]_i_3 ),
        .I2(output_data_2_s_reg),
        .I3(output_data_0_s_reg),
        .I4(output_data_1_s_reg),
        .I5(pc_s_reg[23]),
        .O(pc_s[23]));
LUT6 #(
    .INIT(64'hFFF8888800000000)) 
     \pc[23]_i_2 
       (.I0(temp_signal),
        .I1(instruction_address_reg[23]),
        .I2(output_data_4_s_reg),
        .I3(output_data_3_s_reg),
        .I4(output_data_3[1]),
        .I5(\n_0_pc[31]_i_3 ),
        .O(\n_0_pc[23]_i_2 ));
LUT6 #(
    .INIT(64'hFFFFFFFBAAAAAAAA)) 
     \pc[24]_i_1 
       (.I0(\n_0_pc[24]_i_2 ),
        .I1(\n_0_pc[31]_i_3 ),
        .I2(output_data_2_s_reg),
        .I3(output_data_0_s_reg),
        .I4(output_data_1_s_reg),
        .I5(pc_s_reg[24]),
        .O(pc_s[24]));
LUT6 #(
    .INIT(64'hFFF8888800000000)) 
     \pc[24]_i_2 
       (.I0(temp_signal),
        .I1(instruction_address_reg[24]),
        .I2(output_data_4_s_reg),
        .I3(output_data_3_s_reg),
        .I4(output_data_3[2]),
        .I5(\n_0_pc[31]_i_3 ),
        .O(\n_0_pc[24]_i_2 ));
LUT6 #(
    .INIT(64'hFFFFFFFBAAAAAAAA)) 
     \pc[25]_i_1 
       (.I0(\n_0_pc[25]_i_2 ),
        .I1(\n_0_pc[31]_i_3 ),
        .I2(output_data_2_s_reg),
        .I3(output_data_0_s_reg),
        .I4(output_data_1_s_reg),
        .I5(pc_s_reg[25]),
        .O(pc_s[25]));
LUT6 #(
    .INIT(64'hFFF8888800000000)) 
     \pc[25]_i_2 
       (.I0(temp_signal),
        .I1(instruction_address_reg[25]),
        .I2(output_data_4_s_reg),
        .I3(output_data_3_s_reg),
        .I4(output_data_3[3]),
        .I5(\n_0_pc[31]_i_3 ),
        .O(\n_0_pc[25]_i_2 ));
LUT6 #(
    .INIT(64'hFFFFFFFBAAAAAAAA)) 
     \pc[26]_i_1 
       (.I0(\n_0_pc[26]_i_2 ),
        .I1(\n_0_pc[31]_i_3 ),
        .I2(output_data_2_s_reg),
        .I3(output_data_0_s_reg),
        .I4(output_data_1_s_reg),
        .I5(pc_s_reg[26]),
        .O(pc_s[26]));
LUT6 #(
    .INIT(64'hFFF8888800000000)) 
     \pc[26]_i_2 
       (.I0(temp_signal),
        .I1(instruction_address_reg[26]),
        .I2(output_data_4_s_reg),
        .I3(output_data_3_s_reg),
        .I4(output_data_3[4]),
        .I5(\n_0_pc[31]_i_3 ),
        .O(\n_0_pc[26]_i_2 ));
LUT6 #(
    .INIT(64'hFFFFFFFBAAAAAAAA)) 
     \pc[27]_i_1 
       (.I0(\n_0_pc[27]_i_2 ),
        .I1(\n_0_pc[31]_i_3 ),
        .I2(output_data_2_s_reg),
        .I3(output_data_0_s_reg),
        .I4(output_data_1_s_reg),
        .I5(pc_s_reg[27]),
        .O(pc_s[27]));
LUT6 #(
    .INIT(64'hFFF8888800000000)) 
     \pc[27]_i_2 
       (.I0(temp_signal),
        .I1(instruction_address_reg[27]),
        .I2(output_data_4_s_reg),
        .I3(output_data_3_s_reg),
        .I4(output_data_3[5]),
        .I5(\n_0_pc[31]_i_3 ),
        .O(\n_0_pc[27]_i_2 ));
LUT6 #(
    .INIT(64'hFFFFFFFBAAAAAAAA)) 
     \pc[28]_i_1 
       (.I0(\n_0_pc[28]_i_2 ),
        .I1(\n_0_pc[31]_i_3 ),
        .I2(output_data_2_s_reg),
        .I3(output_data_0_s_reg),
        .I4(output_data_1_s_reg),
        .I5(pc_s_reg[28]),
        .O(pc_s[28]));
LUT6 #(
    .INIT(64'hFFF8888800000000)) 
     \pc[28]_i_2 
       (.I0(temp_signal),
        .I1(instruction_address_reg[28]),
        .I2(output_data_4_s_reg),
        .I3(output_data_3_s_reg),
        .I4(output_data_3[6]),
        .I5(\n_0_pc[31]_i_3 ),
        .O(\n_0_pc[28]_i_2 ));
LUT6 #(
    .INIT(64'hFFAAEAAAEAAAEAAA)) 
     \pc[29]_i_1 
       (.I0(\n_0_pc[29]_i_2 ),
        .I1(output_data_4_s_reg),
        .I2(output_data_4[0]),
        .I3(\n_0_pc[31]_i_3 ),
        .I4(instruction_address_reg[29]),
        .I5(temp_signal),
        .O(pc_s[29]));
LUT6 #(
    .INIT(64'hFFFEFFFF00000000)) 
     \pc[29]_i_2 
       (.I0(output_data_3_s_reg),
        .I1(output_data_2_s_reg),
        .I2(output_data_1_s_reg),
        .I3(output_data_0_s_reg),
        .I4(\n_0_pc[31]_i_3 ),
        .I5(pc_s_reg[29]),
        .O(\n_0_pc[29]_i_2 ));
LUT6 #(
    .INIT(64'hFFFFB380B380B380)) 
     \pc[2]_i_1 
       (.I0(temp_signal),
        .I1(\n_0_pc[31]_i_3 ),
        .I2(instruction_address_reg[2]),
        .I3(pc_s_reg[2]),
        .I4(output_data_0[0]),
        .I5(\n_0_pc[7]_i_2 ),
        .O(pc_s[2]));
LUT6 #(
    .INIT(64'hFFAAEAAAEAAAEAAA)) 
     \pc[30]_i_1 
       (.I0(\n_0_pc[30]_i_2 ),
        .I1(output_data_4_s_reg),
        .I2(output_data_4[1]),
        .I3(\n_0_pc[31]_i_3 ),
        .I4(instruction_address_reg[30]),
        .I5(temp_signal),
        .O(pc_s[30]));
LUT6 #(
    .INIT(64'hFFFEFFFF00000000)) 
     \pc[30]_i_2 
       (.I0(output_data_3_s_reg),
        .I1(output_data_2_s_reg),
        .I2(output_data_1_s_reg),
        .I3(output_data_0_s_reg),
        .I4(\n_0_pc[31]_i_3 ),
        .I5(pc_s_reg[30]),
        .O(\n_0_pc[30]_i_2 ));
LUT6 #(
    .INIT(64'hFFAAEAAAEAAAEAAA)) 
     \pc[31]_i_1 
       (.I0(\n_0_pc[31]_i_2 ),
        .I1(output_data_4_s_reg),
        .I2(output_data_4[2]),
        .I3(\n_0_pc[31]_i_3 ),
        .I4(instruction_address_reg[31]),
        .I5(temp_signal),
        .O(pc_s[31]));
LUT6 #(
    .INIT(64'hFFFEFFFF00000000)) 
     \pc[31]_i_2 
       (.I0(output_data_3_s_reg),
        .I1(output_data_2_s_reg),
        .I2(output_data_1_s_reg),
        .I3(output_data_0_s_reg),
        .I4(\n_0_pc[31]_i_3 ),
        .I5(pc_s_reg[31]),
        .O(\n_0_pc[31]_i_2 ));
LUT6 #(
    .INIT(64'h0000000100010116)) 
     \pc[31]_i_3 
       (.I0(output_data_0_s_reg),
        .I1(output_data_1_s_reg),
        .I2(output_data_2_s_reg),
        .I3(output_data_3_s_reg),
        .I4(output_data_4_s_reg),
        .I5(temp_signal),
        .O(\n_0_pc[31]_i_3 ));
LUT6 #(
    .INIT(64'hFFFFB380B380B380)) 
     \pc[3]_i_1 
       (.I0(temp_signal),
        .I1(\n_0_pc[31]_i_3 ),
        .I2(instruction_address_reg[3]),
        .I3(pc_s_reg[3]),
        .I4(output_data_0[1]),
        .I5(\n_0_pc[7]_i_2 ),
        .O(pc_s[3]));
LUT6 #(
    .INIT(64'hFFFFB380B380B380)) 
     \pc[4]_i_1 
       (.I0(temp_signal),
        .I1(\n_0_pc[31]_i_3 ),
        .I2(instruction_address_reg[4]),
        .I3(pc_s_reg[4]),
        .I4(output_data_0[2]),
        .I5(\n_0_pc[7]_i_2 ),
        .O(pc_s[4]));
LUT6 #(
    .INIT(64'hFFFFB380B380B380)) 
     \pc[5]_i_1 
       (.I0(temp_signal),
        .I1(\n_0_pc[31]_i_3 ),
        .I2(instruction_address_reg[5]),
        .I3(pc_s_reg[5]),
        .I4(output_data_0[3]),
        .I5(\n_0_pc[7]_i_2 ),
        .O(pc_s[5]));
LUT6 #(
    .INIT(64'hFFFFB380B380B380)) 
     \pc[6]_i_1 
       (.I0(temp_signal),
        .I1(\n_0_pc[31]_i_3 ),
        .I2(instruction_address_reg[6]),
        .I3(pc_s_reg[6]),
        .I4(output_data_0[4]),
        .I5(\n_0_pc[7]_i_2 ),
        .O(pc_s[6]));
LUT6 #(
    .INIT(64'hFFFFB380B380B380)) 
     \pc[7]_i_1 
       (.I0(temp_signal),
        .I1(\n_0_pc[31]_i_3 ),
        .I2(instruction_address_reg[7]),
        .I3(pc_s_reg[7]),
        .I4(output_data_0[5]),
        .I5(\n_0_pc[7]_i_2 ),
        .O(pc_s[7]));
LUT6 #(
    .INIT(64'hFFFFFFFE00000000)) 
     \pc[7]_i_2 
       (.I0(output_data_4_s_reg),
        .I1(output_data_3_s_reg),
        .I2(output_data_2_s_reg),
        .I3(output_data_0_s_reg),
        .I4(output_data_1_s_reg),
        .I5(\n_0_pc[31]_i_3 ),
        .O(\n_0_pc[7]_i_2 ));
LUT6 #(
    .INIT(64'hFFFFFFFFFFD58080)) 
     \pc[8]_i_1 
       (.I0(\n_0_pc[31]_i_3 ),
        .I1(instruction_address_reg[8]),
        .I2(temp_signal),
        .I3(output_data_0_s_reg),
        .I4(pc_s_reg[8]),
        .I5(\n_0_pc[8]_i_2 ),
        .O(pc_s[8]));
LUT6 #(
    .INIT(64'hAAAAAAA800000000)) 
     \pc[8]_i_2 
       (.I0(\n_0_pc[31]_i_3 ),
        .I1(output_data_1_s_reg),
        .I2(output_data_2_s_reg),
        .I3(output_data_3_s_reg),
        .I4(output_data_4_s_reg),
        .I5(output_data_1[0]),
        .O(\n_0_pc[8]_i_2 ));
LUT6 #(
    .INIT(64'hFFFFFFFFFFD58080)) 
     \pc[9]_i_1 
       (.I0(\n_0_pc[31]_i_3 ),
        .I1(instruction_address_reg[9]),
        .I2(temp_signal),
        .I3(output_data_0_s_reg),
        .I4(pc_s_reg[9]),
        .I5(\n_0_pc[9]_i_2 ),
        .O(pc_s[9]));
LUT6 #(
    .INIT(64'hAAAAAAA800000000)) 
     \pc[9]_i_2 
       (.I0(\n_0_pc[31]_i_3 ),
        .I1(output_data_1_s_reg),
        .I2(output_data_2_s_reg),
        .I3(output_data_3_s_reg),
        .I4(output_data_4_s_reg),
        .I5(output_data_1[1]),
        .O(\n_0_pc[9]_i_2 ));
FDRE #(
    .INIT(1'b0)) 
     \pc_reg[0] 
       (.C(clk),
        .CE(n_0_w_en_i_1),
        .D(pc_s[0]),
        .Q(pc[0]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_reg[10] 
       (.C(clk),
        .CE(n_0_w_en_i_1),
        .D(pc_s[10]),
        .Q(pc[10]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_reg[11] 
       (.C(clk),
        .CE(n_0_w_en_i_1),
        .D(pc_s[11]),
        .Q(pc[11]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_reg[12] 
       (.C(clk),
        .CE(n_0_w_en_i_1),
        .D(pc_s[12]),
        .Q(pc[12]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_reg[13] 
       (.C(clk),
        .CE(n_0_w_en_i_1),
        .D(pc_s[13]),
        .Q(pc[13]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_reg[14] 
       (.C(clk),
        .CE(n_0_w_en_i_1),
        .D(pc_s[14]),
        .Q(pc[14]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_reg[15] 
       (.C(clk),
        .CE(n_0_w_en_i_1),
        .D(pc_s[15]),
        .Q(pc[15]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_reg[16] 
       (.C(clk),
        .CE(n_0_w_en_i_1),
        .D(pc_s[16]),
        .Q(pc[16]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_reg[17] 
       (.C(clk),
        .CE(n_0_w_en_i_1),
        .D(pc_s[17]),
        .Q(pc[17]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_reg[18] 
       (.C(clk),
        .CE(n_0_w_en_i_1),
        .D(pc_s[18]),
        .Q(pc[18]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_reg[19] 
       (.C(clk),
        .CE(n_0_w_en_i_1),
        .D(pc_s[19]),
        .Q(pc[19]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_reg[1] 
       (.C(clk),
        .CE(n_0_w_en_i_1),
        .D(pc_s[1]),
        .Q(pc[1]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_reg[20] 
       (.C(clk),
        .CE(n_0_w_en_i_1),
        .D(pc_s[20]),
        .Q(pc[20]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_reg[21] 
       (.C(clk),
        .CE(n_0_w_en_i_1),
        .D(pc_s[21]),
        .Q(pc[21]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_reg[22] 
       (.C(clk),
        .CE(n_0_w_en_i_1),
        .D(pc_s[22]),
        .Q(pc[22]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_reg[23] 
       (.C(clk),
        .CE(n_0_w_en_i_1),
        .D(pc_s[23]),
        .Q(pc[23]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_reg[24] 
       (.C(clk),
        .CE(n_0_w_en_i_1),
        .D(pc_s[24]),
        .Q(pc[24]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_reg[25] 
       (.C(clk),
        .CE(n_0_w_en_i_1),
        .D(pc_s[25]),
        .Q(pc[25]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_reg[26] 
       (.C(clk),
        .CE(n_0_w_en_i_1),
        .D(pc_s[26]),
        .Q(pc[26]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_reg[27] 
       (.C(clk),
        .CE(n_0_w_en_i_1),
        .D(pc_s[27]),
        .Q(pc[27]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_reg[28] 
       (.C(clk),
        .CE(n_0_w_en_i_1),
        .D(pc_s[28]),
        .Q(pc[28]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_reg[29] 
       (.C(clk),
        .CE(n_0_w_en_i_1),
        .D(pc_s[29]),
        .Q(pc[29]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_reg[2] 
       (.C(clk),
        .CE(n_0_w_en_i_1),
        .D(pc_s[2]),
        .Q(pc[2]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_reg[30] 
       (.C(clk),
        .CE(n_0_w_en_i_1),
        .D(pc_s[30]),
        .Q(pc[30]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_reg[31] 
       (.C(clk),
        .CE(n_0_w_en_i_1),
        .D(pc_s[31]),
        .Q(pc[31]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_reg[3] 
       (.C(clk),
        .CE(n_0_w_en_i_1),
        .D(pc_s[3]),
        .Q(pc[3]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_reg[4] 
       (.C(clk),
        .CE(n_0_w_en_i_1),
        .D(pc_s[4]),
        .Q(pc[4]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_reg[5] 
       (.C(clk),
        .CE(n_0_w_en_i_1),
        .D(pc_s[5]),
        .Q(pc[5]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_reg[6] 
       (.C(clk),
        .CE(n_0_w_en_i_1),
        .D(pc_s[6]),
        .Q(pc[6]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_reg[7] 
       (.C(clk),
        .CE(n_0_w_en_i_1),
        .D(pc_s[7]),
        .Q(pc[7]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_reg[8] 
       (.C(clk),
        .CE(n_0_w_en_i_1),
        .D(pc_s[8]),
        .Q(pc[8]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_reg[9] 
       (.C(clk),
        .CE(n_0_w_en_i_1),
        .D(pc_s[9]),
        .Q(pc[9]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_s_reg_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s[0]),
        .Q(pc_s_reg[0]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_s_reg_reg[10] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s[10]),
        .Q(pc_s_reg[10]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_s_reg_reg[11] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s[11]),
        .Q(pc_s_reg[11]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_s_reg_reg[12] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s[12]),
        .Q(pc_s_reg[12]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_s_reg_reg[13] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s[13]),
        .Q(pc_s_reg[13]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_s_reg_reg[14] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s[14]),
        .Q(pc_s_reg[14]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_s_reg_reg[15] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s[15]),
        .Q(pc_s_reg[15]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_s_reg_reg[16] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s[16]),
        .Q(pc_s_reg[16]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_s_reg_reg[17] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s[17]),
        .Q(pc_s_reg[17]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_s_reg_reg[18] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s[18]),
        .Q(pc_s_reg[18]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_s_reg_reg[19] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s[19]),
        .Q(pc_s_reg[19]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_s_reg_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s[1]),
        .Q(pc_s_reg[1]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_s_reg_reg[20] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s[20]),
        .Q(pc_s_reg[20]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_s_reg_reg[21] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s[21]),
        .Q(pc_s_reg[21]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_s_reg_reg[22] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s[22]),
        .Q(pc_s_reg[22]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_s_reg_reg[23] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s[23]),
        .Q(pc_s_reg[23]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_s_reg_reg[24] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s[24]),
        .Q(pc_s_reg[24]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_s_reg_reg[25] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s[25]),
        .Q(pc_s_reg[25]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_s_reg_reg[26] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s[26]),
        .Q(pc_s_reg[26]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_s_reg_reg[27] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s[27]),
        .Q(pc_s_reg[27]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_s_reg_reg[28] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s[28]),
        .Q(pc_s_reg[28]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_s_reg_reg[29] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s[29]),
        .Q(pc_s_reg[29]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_s_reg_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s[2]),
        .Q(pc_s_reg[2]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_s_reg_reg[30] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s[30]),
        .Q(pc_s_reg[30]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_s_reg_reg[31] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s[31]),
        .Q(pc_s_reg[31]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_s_reg_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s[3]),
        .Q(pc_s_reg[3]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_s_reg_reg[4] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s[4]),
        .Q(pc_s_reg[4]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_s_reg_reg[5] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s[5]),
        .Q(pc_s_reg[5]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_s_reg_reg[6] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s[6]),
        .Q(pc_s_reg[6]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_s_reg_reg[7] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s[7]),
        .Q(pc_s_reg[7]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_s_reg_reg[8] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s[8]),
        .Q(pc_s_reg[8]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \pc_s_reg_reg[9] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s[9]),
        .Q(pc_s_reg[9]),
        .R(reset));
LUT6 #(
    .INIT(64'h00000000FFB80000)) 
     \state_reg[0]_i_1 
       (.I0(\n_0_temp_state[0]_i_4 ),
        .I1(start_b),
        .I2(\n_0_temp_state[0]_i_3 ),
        .I3(\n_0_state_reg[0]_i_2 ),
        .I4(enable),
        .I5(reset),
        .O(\n_0_state_reg[0]_i_1 ));
LUT4 #(
    .INIT(16'h8000)) 
     \state_reg[0]_i_2 
       (.I0(state_reg[2]),
        .I1(state_reg[3]),
        .I2(state_reg[1]),
        .I3(temp_state[0]),
        .O(\n_0_state_reg[0]_i_2 ));
LUT5 #(
    .INIT(32'hEEFEFFFF)) 
     \state_reg[1]_i_1 
       (.I0(\n_0_temp_state[1]_i_4 ),
        .I1(\n_0_temp_state[1]_i_3 ),
        .I2(start_b),
        .I3(\n_0_temp_state[1]_i_2 ),
        .I4(enable),
        .O(\n_0_state_reg[1]_i_1 ));
LUT2 #(
    .INIT(4'hB)) 
     \state_reg[2]_i_1 
       (.I0(state_next[2]),
        .I1(enable),
        .O(\n_0_state_reg[2]_i_1 ));
LUT2 #(
    .INIT(4'hB)) 
     \state_reg[3]_i_1 
       (.I0(state_next[3]),
        .I1(enable),
        .O(\n_0_state_reg[3]_i_1 ));
FDRE #(
    .INIT(1'b0)) 
     \state_reg_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(\n_0_state_reg[0]_i_1 ),
        .Q(\n_0_state_reg_reg[0] ),
        .R(1'b0));
FDRE #(
    .INIT(1'b0)) 
     \state_reg_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\n_0_state_reg[1]_i_1 ),
        .Q(state_reg[1]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \state_reg_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(\n_0_state_reg[2]_i_1 ),
        .Q(state_reg[2]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \state_reg_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(\n_0_state_reg[3]_i_1 ),
        .Q(state_reg[3]),
        .R(reset));
LUT4 #(
    .INIT(16'hFF40)) 
     stop_b_INST_0
       (.I0(state_reg[1]),
        .I1(state_reg[2]),
        .I2(state_reg[3]),
        .I3(\n_0_state_reg_reg[0] ),
        .O(stop_b));
LUT6 #(
    .INIT(64'hFFFFFF080808FF08)) 
     \temp_state[0]_i_1 
       (.I0(temp_state[0]),
        .I1(state_reg[1]),
        .I2(\n_0_temp_state[0]_i_2 ),
        .I3(\n_0_temp_state[0]_i_3 ),
        .I4(start_b),
        .I5(\n_0_temp_state[0]_i_4 ),
        .O(state_next[0]));
LUT2 #(
    .INIT(4'h7)) 
     \temp_state[0]_i_2 
       (.I0(state_reg[2]),
        .I1(state_reg[3]),
        .O(\n_0_temp_state[0]_i_2 ));
LUT6 #(
    .INIT(64'h000000000C043FF4)) 
     \temp_state[0]_i_3 
       (.I0(data_reg_s[6]),
        .I1(state_reg[3]),
        .I2(state_reg[1]),
        .I3(state_reg[2]),
        .I4(data_reg_s[7]),
        .I5(\n_0_state_reg_reg[0] ),
        .O(\n_0_temp_state[0]_i_3 ));
LUT6 #(
    .INIT(64'h000000D03FFF3FDF)) 
     \temp_state[0]_i_4 
       (.I0(data_reg_s[6]),
        .I1(state_reg[2]),
        .I2(state_reg[3]),
        .I3(state_reg[1]),
        .I4(\n_0_state_reg_reg[0] ),
        .I5(data_reg_s[7]),
        .O(\n_0_temp_state[0]_i_4 ));
LUT4 #(
    .INIT(16'hFFF4)) 
     \temp_state[1]_i_1 
       (.I0(\n_0_temp_state[1]_i_2 ),
        .I1(start_b),
        .I2(\n_0_temp_state[1]_i_3 ),
        .I3(\n_0_temp_state[1]_i_4 ),
        .O(state_next[1]));
LUT5 #(
    .INIT(32'hDF5D5F5D)) 
     \temp_state[1]_i_2 
       (.I0(data_reg_s[7]),
        .I1(state_reg[3]),
        .I2(\n_0_state_reg_reg[0] ),
        .I3(state_reg[1]),
        .I4(state_reg[2]),
        .O(\n_0_temp_state[1]_i_2 ));
LUT6 #(
    .INIT(64'hA000A00000300000)) 
     \temp_state[1]_i_3 
       (.I0(temp_state[1]),
        .I1(\n_0_state_reg_reg[0] ),
        .I2(state_reg[3]),
        .I3(state_reg[2]),
        .I4(data_reg_s[6]),
        .I5(state_reg[1]),
        .O(\n_0_temp_state[1]_i_3 ));
LUT5 #(
    .INIT(32'h00020700)) 
     \temp_state[1]_i_4 
       (.I0(state_reg[2]),
        .I1(state_reg[3]),
        .I2(\n_0_state_reg_reg[0] ),
        .I3(state_reg[1]),
        .I4(data_reg_s[7]),
        .O(\n_0_temp_state[1]_i_4 ));
LUT6 #(
    .INIT(64'hA000AC3C0000F0F0)) 
     \temp_state[2]_i_1 
       (.I0(temp_state[2]),
        .I1(data_reg_s[7]),
        .I2(state_reg[2]),
        .I3(state_reg[3]),
        .I4(\n_0_state_reg_reg[0] ),
        .I5(state_reg[1]),
        .O(state_next[2]));
LUT1 #(
    .INIT(2'h1)) 
     \temp_state[3]_i_1 
       (.I0(enable),
        .O(\n_0_temp_state[3]_i_1 ));
LUT6 #(
    .INIT(64'hC000FF8000003F80)) 
     \temp_state[3]_i_2 
       (.I0(data_reg_s[7]),
        .I1(state_reg[2]),
        .I2(state_reg[1]),
        .I3(state_reg[3]),
        .I4(\n_0_state_reg_reg[0] ),
        .I5(temp_state[3]),
        .O(state_next[3]));
FDRE #(
    .INIT(1'b0)) 
     \temp_state_reg[0] 
       (.C(clk),
        .CE(\n_0_temp_state[3]_i_1 ),
        .D(state_next[0]),
        .Q(temp_state[0]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \temp_state_reg[1] 
       (.C(clk),
        .CE(\n_0_temp_state[3]_i_1 ),
        .D(state_next[1]),
        .Q(temp_state[1]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \temp_state_reg[2] 
       (.C(clk),
        .CE(\n_0_temp_state[3]_i_1 ),
        .D(state_next[2]),
        .Q(temp_state[2]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \temp_state_reg[3] 
       (.C(clk),
        .CE(\n_0_temp_state[3]_i_1 ),
        .D(state_next[3]),
        .Q(temp_state[3]),
        .R(reset));
LUT2 #(
    .INIT(4'hE)) 
     w_en_i_1
       (.I0(temp_signal),
        .I1(\n_0_state_reg_reg[0] ),
        .O(n_0_w_en_i_1));
FDRE #(
    .INIT(1'b0)) 
     w_en_reg
       (.C(clk),
        .CE(1'b1),
        .D(n_0_w_en_i_1),
        .Q(w_en),
        .R(reset));
endmodule

module decode_i_sync
   (clk,
    reset,
    start_i,
    enable,
    data_in,
    data_out,
    context_id_out,
    stop_i,
    out_en,
    fifo_overflow);
  input clk;
  input reset;
  input start_i;
  input enable;
  input [7:0]data_in;
  output [31:0]data_out;
  output [31:0]context_id_out;
  output stop_i;
  output out_en;
  output fifo_overflow;

  wire clk;
  wire [7:0]data_in;
  wire [31:0]data_out;
  wire [7:0]data_reg_s;
  wire en_0;
  wire en_2;
  wire enable;
  wire \n_0_output_data_1[7]_i_1 ;
  wire \n_0_state_reg[0]_i_1 ;
  wire \n_0_state_reg[1]_i_1 ;
  wire \n_0_state_reg[2]_i_1 ;
  wire \n_0_temp_state[0]_i_1 ;
  wire \n_0_temp_state[1]_i_1 ;
  wire \n_0_temp_state[2]_i_1 ;
  wire out_en;
  wire reset;
  wire start_i;
  wire [0:0]state_next;
  wire [2:0]state_reg;
  wire stop_i;
  wire [2:0]temp_state;

FDRE #(
    .INIT(1'b0)) 
     \data_reg_s_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(data_in[0]),
        .Q(data_reg_s[0]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \data_reg_s_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(data_in[1]),
        .Q(data_reg_s[1]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \data_reg_s_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(data_in[2]),
        .Q(data_reg_s[2]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \data_reg_s_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(data_in[3]),
        .Q(data_reg_s[3]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \data_reg_s_reg[4] 
       (.C(clk),
        .CE(1'b1),
        .D(data_in[4]),
        .Q(data_reg_s[4]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \data_reg_s_reg[5] 
       (.C(clk),
        .CE(1'b1),
        .D(data_in[5]),
        .Q(data_reg_s[5]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \data_reg_s_reg[6] 
       (.C(clk),
        .CE(1'b1),
        .D(data_in[6]),
        .Q(data_reg_s[6]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \data_reg_s_reg[7] 
       (.C(clk),
        .CE(1'b1),
        .D(data_in[7]),
        .Q(data_reg_s[7]),
        .R(reset));
LUT3 #(
    .INIT(8'h02)) 
     out_en_INST_0
       (.I0(state_reg[2]),
        .I1(state_reg[0]),
        .I2(state_reg[1]),
        .O(out_en));
LUT3 #(
    .INIT(8'h02)) 
     \output_data_0[7]_i_1 
       (.I0(state_reg[0]),
        .I1(state_reg[2]),
        .I2(state_reg[1]),
        .O(en_0));
FDRE #(
    .INIT(1'b0)) 
     \output_data_0_reg[0] 
       (.C(clk),
        .CE(en_0),
        .D(data_reg_s[0]),
        .Q(data_out[0]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \output_data_0_reg[1] 
       (.C(clk),
        .CE(en_0),
        .D(data_reg_s[1]),
        .Q(data_out[1]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \output_data_0_reg[2] 
       (.C(clk),
        .CE(en_0),
        .D(data_reg_s[2]),
        .Q(data_out[2]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \output_data_0_reg[3] 
       (.C(clk),
        .CE(en_0),
        .D(data_reg_s[3]),
        .Q(data_out[3]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \output_data_0_reg[4] 
       (.C(clk),
        .CE(en_0),
        .D(data_reg_s[4]),
        .Q(data_out[4]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \output_data_0_reg[5] 
       (.C(clk),
        .CE(en_0),
        .D(data_reg_s[5]),
        .Q(data_out[5]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \output_data_0_reg[6] 
       (.C(clk),
        .CE(en_0),
        .D(data_reg_s[6]),
        .Q(data_out[6]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \output_data_0_reg[7] 
       (.C(clk),
        .CE(en_0),
        .D(data_reg_s[7]),
        .Q(data_out[7]),
        .R(reset));
LUT3 #(
    .INIT(8'h02)) 
     \output_data_1[7]_i_1 
       (.I0(state_reg[1]),
        .I1(state_reg[0]),
        .I2(state_reg[2]),
        .O(\n_0_output_data_1[7]_i_1 ));
FDRE #(
    .INIT(1'b0)) 
     \output_data_1_reg[0] 
       (.C(clk),
        .CE(\n_0_output_data_1[7]_i_1 ),
        .D(data_reg_s[0]),
        .Q(data_out[8]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \output_data_1_reg[1] 
       (.C(clk),
        .CE(\n_0_output_data_1[7]_i_1 ),
        .D(data_reg_s[1]),
        .Q(data_out[9]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \output_data_1_reg[2] 
       (.C(clk),
        .CE(\n_0_output_data_1[7]_i_1 ),
        .D(data_reg_s[2]),
        .Q(data_out[10]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \output_data_1_reg[3] 
       (.C(clk),
        .CE(\n_0_output_data_1[7]_i_1 ),
        .D(data_reg_s[3]),
        .Q(data_out[11]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \output_data_1_reg[4] 
       (.C(clk),
        .CE(\n_0_output_data_1[7]_i_1 ),
        .D(data_reg_s[4]),
        .Q(data_out[12]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \output_data_1_reg[5] 
       (.C(clk),
        .CE(\n_0_output_data_1[7]_i_1 ),
        .D(data_reg_s[5]),
        .Q(data_out[13]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \output_data_1_reg[6] 
       (.C(clk),
        .CE(\n_0_output_data_1[7]_i_1 ),
        .D(data_reg_s[6]),
        .Q(data_out[14]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \output_data_1_reg[7] 
       (.C(clk),
        .CE(\n_0_output_data_1[7]_i_1 ),
        .D(data_reg_s[7]),
        .Q(data_out[15]),
        .R(reset));
LUT2 #(
    .INIT(4'h8)) 
     \output_data_2[7]_i_1 
       (.I0(state_reg[0]),
        .I1(state_reg[1]),
        .O(en_2));
FDRE #(
    .INIT(1'b0)) 
     \output_data_2_reg[0] 
       (.C(clk),
        .CE(en_2),
        .D(data_reg_s[0]),
        .Q(data_out[16]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \output_data_2_reg[1] 
       (.C(clk),
        .CE(en_2),
        .D(data_reg_s[1]),
        .Q(data_out[17]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \output_data_2_reg[2] 
       (.C(clk),
        .CE(en_2),
        .D(data_reg_s[2]),
        .Q(data_out[18]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \output_data_2_reg[3] 
       (.C(clk),
        .CE(en_2),
        .D(data_reg_s[3]),
        .Q(data_out[19]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \output_data_2_reg[4] 
       (.C(clk),
        .CE(en_2),
        .D(data_reg_s[4]),
        .Q(data_out[20]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \output_data_2_reg[5] 
       (.C(clk),
        .CE(en_2),
        .D(data_reg_s[5]),
        .Q(data_out[21]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \output_data_2_reg[6] 
       (.C(clk),
        .CE(en_2),
        .D(data_reg_s[6]),
        .Q(data_out[22]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \output_data_2_reg[7] 
       (.C(clk),
        .CE(en_2),
        .D(data_reg_s[7]),
        .Q(data_out[23]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \output_data_3_reg[0] 
       (.C(clk),
        .CE(out_en),
        .D(data_reg_s[0]),
        .Q(data_out[24]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \output_data_3_reg[1] 
       (.C(clk),
        .CE(out_en),
        .D(data_reg_s[1]),
        .Q(data_out[25]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \output_data_3_reg[2] 
       (.C(clk),
        .CE(out_en),
        .D(data_reg_s[2]),
        .Q(data_out[26]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \output_data_3_reg[3] 
       (.C(clk),
        .CE(out_en),
        .D(data_reg_s[3]),
        .Q(data_out[27]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \output_data_3_reg[4] 
       (.C(clk),
        .CE(out_en),
        .D(data_reg_s[4]),
        .Q(data_out[28]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \output_data_3_reg[5] 
       (.C(clk),
        .CE(out_en),
        .D(data_reg_s[5]),
        .Q(data_out[29]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \output_data_3_reg[6] 
       (.C(clk),
        .CE(out_en),
        .D(data_reg_s[6]),
        .Q(data_out[30]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \output_data_3_reg[7] 
       (.C(clk),
        .CE(out_en),
        .D(data_reg_s[7]),
        .Q(data_out[31]),
        .R(reset));
LUT3 #(
    .INIT(8'h08)) 
     \state_reg[0]_i_1 
       (.I0(state_next),
        .I1(enable),
        .I2(reset),
        .O(\n_0_state_reg[0]_i_1 ));
LUT5 #(
    .INIT(32'h00F300EE)) 
     \state_reg[0]_i_2 
       (.I0(start_i),
        .I1(state_reg[2]),
        .I2(temp_state[0]),
        .I3(state_reg[0]),
        .I4(state_reg[1]),
        .O(state_next));
LUT5 #(
    .INIT(32'h3414FFFF)) 
     \state_reg[1]_i_1 
       (.I0(state_reg[2]),
        .I1(state_reg[0]),
        .I2(state_reg[1]),
        .I3(temp_state[1]),
        .I4(enable),
        .O(\n_0_state_reg[1]_i_1 ));
LUT5 #(
    .INIT(32'hE5A0FFFF)) 
     \state_reg[2]_i_1 
       (.I0(state_reg[0]),
        .I1(temp_state[2]),
        .I2(state_reg[1]),
        .I3(state_reg[2]),
        .I4(enable),
        .O(\n_0_state_reg[2]_i_1 ));
FDRE #(
    .INIT(1'b0)) 
     \state_reg_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(\n_0_state_reg[0]_i_1 ),
        .Q(state_reg[0]),
        .R(1'b0));
FDRE #(
    .INIT(1'b0)) 
     \state_reg_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\n_0_state_reg[1]_i_1 ),
        .Q(state_reg[1]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \state_reg_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(\n_0_state_reg[2]_i_1 ),
        .Q(state_reg[2]),
        .R(reset));
LUT3 #(
    .INIT(8'h01)) 
     stop_i_INST_0
       (.I0(state_reg[1]),
        .I1(state_reg[0]),
        .I2(state_reg[2]),
        .O(stop_i));
LUT6 #(
    .INIT(64'hFFFF0F0E0000030E)) 
     \temp_state[0]_i_1 
       (.I0(start_i),
        .I1(state_reg[2]),
        .I2(state_reg[0]),
        .I3(state_reg[1]),
        .I4(enable),
        .I5(temp_state[0]),
        .O(\n_0_temp_state[0]_i_1 ));
LUT5 #(
    .INIT(32'hFF260006)) 
     \temp_state[1]_i_1 
       (.I0(state_reg[1]),
        .I1(state_reg[0]),
        .I2(state_reg[2]),
        .I3(enable),
        .I4(temp_state[1]),
        .O(\n_0_temp_state[1]_i_1 ));
LUT5 #(
    .INIT(32'hFFCA00C2)) 
     \temp_state[2]_i_1 
       (.I0(state_reg[2]),
        .I1(state_reg[1]),
        .I2(state_reg[0]),
        .I3(enable),
        .I4(temp_state[2]),
        .O(\n_0_temp_state[2]_i_1 ));
FDRE #(
    .INIT(1'b0)) 
     \temp_state_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(\n_0_temp_state[0]_i_1 ),
        .Q(temp_state[0]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \temp_state_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\n_0_temp_state[1]_i_1 ),
        .Q(temp_state[1]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \temp_state_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(\n_0_temp_state[2]_i_1 ),
        .Q(temp_state[2]),
        .R(reset));
endmodule

(* ECO_CHECKSUM = "81fcd9ce" *) 
(* NotValidForBitStream *)
module decodeur_traces
   (clk,
    reset,
    enable,
    trace_data,
    pc,
    w_en,
    fifo_overflow,
    waypoint_address,
    waypoint_address_en);
  input clk;
  input reset;
  input enable;
  input [7:0]trace_data;
  output [31:0]pc;
  output w_en;
  output fifo_overflow;
  output [31:0]waypoint_address;
  output waypoint_address_en;

  wire clk;
  wire clk_IBUF;
  wire clk_IBUF_BUFG;
  wire enable;
  wire enable_IBUF;
  wire fifo_overflow;
  wire [31:0]pc;
  wire [31:0]pc_OBUF;
  wire reset;
  wire reset_IBUF;
  wire start_b;
  wire start_i;
  wire stop_b;
  wire stop_i;
  wire [7:0]trace_data;
  wire [7:0]trace_data_IBUF;
  wire w_en;
  wire w_en_OBUF;
  wire NLW_chemin_fifo_overflow_UNCONNECTED;
  wire NLW_chemin_start_w_UNCONNECTED;
  wire NLW_chemin_stop_w_UNCONNECTED;
  wire NLW_chemin_waypoint_address_en_UNCONNECTED;
  wire [31:0]NLW_chemin_context_id_UNCONNECTED;
  wire [31:0]NLW_chemin_waypoint_address_UNCONNECTED;
  wire NLW_fsm_start_waypoint_UNCONNECTED;
  wire NLW_fsm_stop_waypoint_UNCONNECTED;
  wire [4:0]NLW_fsm_atomes_UNCONNECTED;
  wire [7:0]NLW_fsm_data_out_UNCONNECTED;

initial begin
 $sdf_annotate("test_bench_time_impl.sdf",,,,"tool_control");
end
datapath chemin
       (.clk(clk_IBUF_BUFG),
        .context_id(NLW_chemin_context_id_UNCONNECTED[31:0]),
        .enable(enable_IBUF),
        .fifo_overflow(NLW_chemin_fifo_overflow_UNCONNECTED),
        .out_en(w_en_OBUF),
        .pc(pc_OBUF),
        .reset(reset_IBUF),
        .start_b(start_b),
        .start_i(start_i),
        .start_w(NLW_chemin_start_w_UNCONNECTED),
        .stop_b(stop_b),
        .stop_i(stop_i),
        .stop_w(NLW_chemin_stop_w_UNCONNECTED),
        .trace_data_in(trace_data_IBUF),
        .waypoint_address(NLW_chemin_waypoint_address_UNCONNECTED[31:0]),
        .waypoint_address_en(NLW_chemin_waypoint_address_en_UNCONNECTED));
BUFG clk_IBUF_BUFG_inst
       (.I(clk_IBUF),
        .O(clk_IBUF_BUFG));
IBUF clk_IBUF_inst
       (.I(clk),
        .O(clk_IBUF));
IBUF enable_IBUF_inst
       (.I(enable),
        .O(enable_IBUF));
OBUF fifo_overflow_OBUF_inst
       (.I(1'b0),
        .O(fifo_overflow));
pft_decoder_v2 fsm
       (.atomes(NLW_fsm_atomes_UNCONNECTED[4:0]),
        .clk(clk_IBUF_BUFG),
        .data_in(trace_data_IBUF),
        .data_out(NLW_fsm_data_out_UNCONNECTED[7:0]),
        .enable(enable_IBUF),
        .reset(reset_IBUF),
        .start_bap(start_b),
        .start_i_sync(start_i),
        .start_waypoint(NLW_fsm_start_waypoint_UNCONNECTED),
        .stop_bap(stop_b),
        .stop_i_sync(stop_i),
        .stop_waypoint(NLW_fsm_stop_waypoint_UNCONNECTED));
OBUF \pc_OBUF[0]_inst 
       (.I(pc_OBUF[0]),
        .O(pc[0]));
OBUF \pc_OBUF[10]_inst 
       (.I(pc_OBUF[10]),
        .O(pc[10]));
OBUF \pc_OBUF[11]_inst 
       (.I(pc_OBUF[11]),
        .O(pc[11]));
OBUF \pc_OBUF[12]_inst 
       (.I(pc_OBUF[12]),
        .O(pc[12]));
OBUF \pc_OBUF[13]_inst 
       (.I(pc_OBUF[13]),
        .O(pc[13]));
OBUF \pc_OBUF[14]_inst 
       (.I(pc_OBUF[14]),
        .O(pc[14]));
OBUF \pc_OBUF[15]_inst 
       (.I(pc_OBUF[15]),
        .O(pc[15]));
OBUF \pc_OBUF[16]_inst 
       (.I(pc_OBUF[16]),
        .O(pc[16]));
OBUF \pc_OBUF[17]_inst 
       (.I(pc_OBUF[17]),
        .O(pc[17]));
OBUF \pc_OBUF[18]_inst 
       (.I(pc_OBUF[18]),
        .O(pc[18]));
OBUF \pc_OBUF[19]_inst 
       (.I(pc_OBUF[19]),
        .O(pc[19]));
OBUF \pc_OBUF[1]_inst 
       (.I(pc_OBUF[1]),
        .O(pc[1]));
OBUF \pc_OBUF[20]_inst 
       (.I(pc_OBUF[20]),
        .O(pc[20]));
OBUF \pc_OBUF[21]_inst 
       (.I(pc_OBUF[21]),
        .O(pc[21]));
OBUF \pc_OBUF[22]_inst 
       (.I(pc_OBUF[22]),
        .O(pc[22]));
OBUF \pc_OBUF[23]_inst 
       (.I(pc_OBUF[23]),
        .O(pc[23]));
OBUF \pc_OBUF[24]_inst 
       (.I(pc_OBUF[24]),
        .O(pc[24]));
OBUF \pc_OBUF[25]_inst 
       (.I(pc_OBUF[25]),
        .O(pc[25]));
OBUF \pc_OBUF[26]_inst 
       (.I(pc_OBUF[26]),
        .O(pc[26]));
OBUF \pc_OBUF[27]_inst 
       (.I(pc_OBUF[27]),
        .O(pc[27]));
OBUF \pc_OBUF[28]_inst 
       (.I(pc_OBUF[28]),
        .O(pc[28]));
OBUF \pc_OBUF[29]_inst 
       (.I(pc_OBUF[29]),
        .O(pc[29]));
OBUF \pc_OBUF[2]_inst 
       (.I(pc_OBUF[2]),
        .O(pc[2]));
OBUF \pc_OBUF[30]_inst 
       (.I(pc_OBUF[30]),
        .O(pc[30]));
OBUF \pc_OBUF[31]_inst 
       (.I(pc_OBUF[31]),
        .O(pc[31]));
OBUF \pc_OBUF[3]_inst 
       (.I(pc_OBUF[3]),
        .O(pc[3]));
OBUF \pc_OBUF[4]_inst 
       (.I(pc_OBUF[4]),
        .O(pc[4]));
OBUF \pc_OBUF[5]_inst 
       (.I(pc_OBUF[5]),
        .O(pc[5]));
OBUF \pc_OBUF[6]_inst 
       (.I(pc_OBUF[6]),
        .O(pc[6]));
OBUF \pc_OBUF[7]_inst 
       (.I(pc_OBUF[7]),
        .O(pc[7]));
OBUF \pc_OBUF[8]_inst 
       (.I(pc_OBUF[8]),
        .O(pc[8]));
OBUF \pc_OBUF[9]_inst 
       (.I(pc_OBUF[9]),
        .O(pc[9]));
IBUF reset_IBUF_inst
       (.I(reset),
        .O(reset_IBUF));
IBUF \trace_data_IBUF[0]_inst 
       (.I(trace_data[0]),
        .O(trace_data_IBUF[0]));
IBUF \trace_data_IBUF[1]_inst 
       (.I(trace_data[1]),
        .O(trace_data_IBUF[1]));
IBUF \trace_data_IBUF[2]_inst 
       (.I(trace_data[2]),
        .O(trace_data_IBUF[2]));
IBUF \trace_data_IBUF[3]_inst 
       (.I(trace_data[3]),
        .O(trace_data_IBUF[3]));
IBUF \trace_data_IBUF[4]_inst 
       (.I(trace_data[4]),
        .O(trace_data_IBUF[4]));
IBUF \trace_data_IBUF[5]_inst 
       (.I(trace_data[5]),
        .O(trace_data_IBUF[5]));
IBUF \trace_data_IBUF[6]_inst 
       (.I(trace_data[6]),
        .O(trace_data_IBUF[6]));
IBUF \trace_data_IBUF[7]_inst 
       (.I(trace_data[7]),
        .O(trace_data_IBUF[7]));
OBUF w_en_OBUF_inst
       (.I(w_en_OBUF),
        .O(w_en));
endmodule

module pft_decoder_v2
   (clk,
    reset,
    data_in,
    data_out,
    enable,
    start_i_sync,
    stop_i_sync,
    atomes,
    start_bap,
    stop_bap,
    start_waypoint,
    stop_waypoint);
  input clk;
  input reset;
  input [7:0]data_in;
  output [7:0]data_out;
  input enable;
  output start_i_sync;
  input stop_i_sync;
  output [4:0]atomes;
  output start_bap;
  input stop_bap;
  output start_waypoint;
  input stop_waypoint;

  wire clk;
  wire [7:0]data_in;
  wire enable;
  wire \n_0_data_reg_s_reg[0] ;
  wire \n_0_data_reg_s_reg[7] ;
  wire n_0_start_bap_INST_0_i_1;
  wire n_0_start_bap_INST_0_i_2;
  wire n_0_start_bap_INST_0_i_3;
  wire n_0_start_i_sync_INST_0_i_1;
  wire n_0_start_i_sync_INST_0_i_2;
  wire n_0_start_i_sync_INST_0_i_3;
  wire n_0_start_i_sync_INST_0_i_4;
  wire n_0_start_i_sync_INST_0_i_5;
  wire n_0_start_i_sync_INST_0_i_6;
  wire \n_0_state_reg[0]_i_1 ;
  wire \n_0_state_reg[1]_i_1 ;
  wire \n_0_state_reg[2]_i_1 ;
  wire \n_0_state_reg[3]_i_1 ;
  wire \n_0_temp_state[0]_i_2 ;
  wire \n_0_temp_state[0]_i_3 ;
  wire \n_0_temp_state[0]_i_4 ;
  wire \n_0_temp_state[0]_i_5 ;
  wire \n_0_temp_state[1]_i_2 ;
  wire \n_0_temp_state[1]_i_3 ;
  wire \n_0_temp_state[1]_i_4 ;
  wire \n_0_temp_state[1]_i_5 ;
  wire \n_0_temp_state[1]_i_6 ;
  wire \n_0_temp_state[2]_i_2 ;
  wire \n_0_temp_state[2]_i_3 ;
  wire \n_0_temp_state[2]_i_4 ;
  wire \n_0_temp_state[2]_i_5 ;
  wire \n_0_temp_state[3]_i_1 ;
  wire p_1_in;
  wire p_2_in;
  wire p_3_in;
  wire p_4_in;
  wire p_5_in;
  wire p_6_in;
  wire reset;
  wire start_bap;
  wire start_i_sync;
  wire [3:0]state_next;
  wire [3:0]state_reg;
  wire stop_bap;
  wire stop_i_sync;
  wire [3:0]temp_state;

FDRE #(
    .INIT(1'b0)) 
     \data_reg_s_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(data_in[0]),
        .Q(\n_0_data_reg_s_reg[0] ),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \data_reg_s_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(data_in[1]),
        .Q(p_1_in),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \data_reg_s_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(data_in[2]),
        .Q(p_2_in),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \data_reg_s_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(data_in[3]),
        .Q(p_3_in),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \data_reg_s_reg[4] 
       (.C(clk),
        .CE(1'b1),
        .D(data_in[4]),
        .Q(p_4_in),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \data_reg_s_reg[5] 
       (.C(clk),
        .CE(1'b1),
        .D(data_in[5]),
        .Q(p_5_in),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \data_reg_s_reg[6] 
       (.C(clk),
        .CE(1'b1),
        .D(data_in[6]),
        .Q(p_6_in),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \data_reg_s_reg[7] 
       (.C(clk),
        .CE(1'b1),
        .D(data_in[7]),
        .Q(\n_0_data_reg_s_reg[7] ),
        .R(reset));
MUXF7 start_bap_INST_0
       (.I0(n_0_start_bap_INST_0_i_1),
        .I1(n_0_start_bap_INST_0_i_2),
        .O(start_bap),
        .S(state_reg[3]));
LUT6 #(
    .INIT(64'hAFF00FFF00000030)) 
     start_bap_INST_0_i_1
       (.I0(stop_i_sync),
        .I1(stop_bap),
        .I2(state_reg[2]),
        .I3(state_reg[0]),
        .I4(state_reg[1]),
        .I5(\n_0_data_reg_s_reg[0] ),
        .O(n_0_start_bap_INST_0_i_1));
LUT6 #(
    .INIT(64'hE2E2E2E2C0C0F0F3)) 
     start_bap_INST_0_i_2
       (.I0(n_0_start_bap_INST_0_i_3),
        .I1(state_reg[2]),
        .I2(\n_0_data_reg_s_reg[0] ),
        .I3(stop_bap),
        .I4(state_reg[0]),
        .I5(state_reg[1]),
        .O(n_0_start_bap_INST_0_i_2));
LUT6 #(
    .INIT(64'h88888888888B8B88)) 
     start_bap_INST_0_i_3
       (.I0(\n_0_data_reg_s_reg[0] ),
        .I1(state_reg[0]),
        .I2(temp_state[0]),
        .I3(temp_state[3]),
        .I4(temp_state[2]),
        .I5(temp_state[1]),
        .O(n_0_start_bap_INST_0_i_3));
MUXF7 start_i_sync_INST_0
       (.I0(n_0_start_i_sync_INST_0_i_1),
        .I1(n_0_start_i_sync_INST_0_i_2),
        .O(start_i_sync),
        .S(state_reg[3]));
LUT6 #(
    .INIT(64'hFFFF0FCF500F0000)) 
     start_i_sync_INST_0_i_1
       (.I0(stop_i_sync),
        .I1(stop_bap),
        .I2(state_reg[2]),
        .I3(state_reg[0]),
        .I4(state_reg[1]),
        .I5(n_0_start_i_sync_INST_0_i_3),
        .O(n_0_start_i_sync_INST_0_i_1));
LUT6 #(
    .INIT(64'hFAFA0000EFEA4040)) 
     start_i_sync_INST_0_i_2
       (.I0(state_reg[2]),
        .I1(n_0_start_i_sync_INST_0_i_4),
        .I2(state_reg[1]),
        .I3(stop_bap),
        .I4(n_0_start_i_sync_INST_0_i_3),
        .I5(state_reg[0]),
        .O(n_0_start_i_sync_INST_0_i_2));
LUT6 #(
    .INIT(64'h0000000000000400)) 
     start_i_sync_INST_0_i_3
       (.I0(\n_0_data_reg_s_reg[7] ),
        .I1(p_3_in),
        .I2(p_2_in),
        .I3(n_0_start_i_sync_INST_0_i_5),
        .I4(n_0_start_i_sync_INST_0_i_6),
        .I5(\n_0_data_reg_s_reg[0] ),
        .O(n_0_start_i_sync_INST_0_i_3));
LUT4 #(
    .INIT(16'h4100)) 
     start_i_sync_INST_0_i_4
       (.I0(temp_state[3]),
        .I1(temp_state[2]),
        .I2(temp_state[0]),
        .I3(temp_state[1]),
        .O(n_0_start_i_sync_INST_0_i_4));
LUT2 #(
    .INIT(4'h1)) 
     start_i_sync_INST_0_i_5
       (.I0(p_5_in),
        .I1(p_4_in),
        .O(n_0_start_i_sync_INST_0_i_5));
LUT2 #(
    .INIT(4'hE)) 
     start_i_sync_INST_0_i_6
       (.I0(p_6_in),
        .I1(p_1_in),
        .O(n_0_start_i_sync_INST_0_i_6));
LUT5 #(
    .INIT(32'h0000E200)) 
     \state_reg[0]_i_1 
       (.I0(\n_0_temp_state[0]_i_2 ),
        .I1(state_reg[3]),
        .I2(\n_0_temp_state[0]_i_3 ),
        .I3(enable),
        .I4(reset),
        .O(\n_0_state_reg[0]_i_1 ));
LUT4 #(
    .INIT(16'hE2FF)) 
     \state_reg[1]_i_1 
       (.I0(\n_0_temp_state[1]_i_2 ),
        .I1(state_reg[3]),
        .I2(\n_0_temp_state[1]_i_3 ),
        .I3(enable),
        .O(\n_0_state_reg[1]_i_1 ));
LUT5 #(
    .INIT(32'h0000E200)) 
     \state_reg[2]_i_1 
       (.I0(\n_0_temp_state[2]_i_2 ),
        .I1(state_reg[3]),
        .I2(\n_0_temp_state[2]_i_3 ),
        .I3(enable),
        .I4(reset),
        .O(\n_0_state_reg[2]_i_1 ));
LUT2 #(
    .INIT(4'hB)) 
     \state_reg[3]_i_1 
       (.I0(state_next[3]),
        .I1(enable),
        .O(\n_0_state_reg[3]_i_1 ));
FDRE #(
    .INIT(1'b0)) 
     \state_reg_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(\n_0_state_reg[0]_i_1 ),
        .Q(state_reg[0]),
        .R(1'b0));
FDRE #(
    .INIT(1'b0)) 
     \state_reg_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\n_0_state_reg[1]_i_1 ),
        .Q(state_reg[1]),
        .R(reset));
FDRE #(
    .INIT(1'b0)) 
     \state_reg_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(\n_0_state_reg[2]_i_1 ),
        .Q(state_reg[2]),
        .R(1'b0));
FDRE #(
    .INIT(1'b0)) 
     \state_reg_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(\n_0_state_reg[3]_i_1 ),
        .Q(state_reg[3]),
        .R(reset));
LUT6 #(
    .INIT(64'hFFFFFFCF500FF000)) 
     \temp_state[0]_i_2 
       (.I0(stop_i_sync),
        .I1(stop_bap),
        .I2(state_reg[2]),
        .I3(state_reg[0]),
        .I4(state_reg[1]),
        .I5(\n_0_temp_state[0]_i_4 ),
        .O(\n_0_temp_state[0]_i_2 ));
LUT6 #(
    .INIT(64'hFFFF0505EFEA4040)) 
     \temp_state[0]_i_3 
       (.I0(state_reg[2]),
        .I1(temp_state[0]),
        .I2(state_reg[1]),
        .I3(stop_bap),
        .I4(\n_0_temp_state[0]_i_4 ),
        .I5(state_reg[0]),
        .O(\n_0_temp_state[0]_i_3 ));
LUT4 #(
    .INIT(16'h00E2)) 
     \temp_state[0]_i_4 
       (.I0(\n_0_temp_state[0]_i_5 ),
        .I1(\n_0_data_reg_s_reg[7] ),
        .I2(\n_0_temp_state[1]_i_6 ),
        .I3(\n_0_data_reg_s_reg[0] ),
        .O(\n_0_temp_state[0]_i_4 ));
LUT6 #(
    .INIT(64'h0000000040000001)) 
     \temp_state[0]_i_5 
       (.I0(p_2_in),
        .I1(p_4_in),
        .I2(p_6_in),
        .I3(p_1_in),
        .I4(p_5_in),
        .I5(p_3_in),
        .O(\n_0_temp_state[0]_i_5 ));
LUT6 #(
    .INIT(64'hFFFF0FCF500F0000)) 
     \temp_state[1]_i_2 
       (.I0(stop_i_sync),
        .I1(stop_bap),
        .I2(state_reg[2]),
        .I3(state_reg[0]),
        .I4(state_reg[1]),
        .I5(\n_0_temp_state[1]_i_4 ),
        .O(\n_0_temp_state[1]_i_2 ));
LUT6 #(
    .INIT(64'hFAFA0000EFEA4040)) 
     \temp_state[1]_i_3 
       (.I0(state_reg[2]),
        .I1(temp_state[1]),
        .I2(state_reg[1]),
        .I3(stop_bap),
        .I4(\n_0_temp_state[1]_i_4 ),
        .I5(state_reg[0]),
        .O(\n_0_temp_state[1]_i_3 ));
LUT4 #(
    .INIT(16'h00E2)) 
     \temp_state[1]_i_4 
       (.I0(\n_0_temp_state[1]_i_5 ),
        .I1(\n_0_data_reg_s_reg[7] ),
        .I2(\n_0_temp_state[1]_i_6 ),
        .I3(\n_0_data_reg_s_reg[0] ),
        .O(\n_0_temp_state[1]_i_4 ));
LUT6 #(
    .INIT(64'h0080000100000000)) 
     \temp_state[1]_i_5 
       (.I0(p_6_in),
        .I1(p_1_in),
        .I2(p_5_in),
        .I3(p_4_in),
        .I4(p_2_in),
        .I5(p_3_in),
        .O(\n_0_temp_state[1]_i_5 ));
LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
     \temp_state[1]_i_6 
       (.I0(p_2_in),
        .I1(p_5_in),
        .I2(p_1_in),
        .I3(p_6_in),
        .I4(p_4_in),
        .I5(p_3_in),
        .O(\n_0_temp_state[1]_i_6 ));
LUT6 #(
    .INIT(64'hFFFF0FCF500F0000)) 
     \temp_state[2]_i_2 
       (.I0(stop_i_sync),
        .I1(stop_bap),
        .I2(state_reg[2]),
        .I3(state_reg[0]),
        .I4(state_reg[1]),
        .I5(\n_0_temp_state[2]_i_4 ),
        .O(\n_0_temp_state[2]_i_2 ));
LUT6 #(
    .INIT(64'hFAFA0000EFEA4040)) 
     \temp_state[2]_i_3 
       (.I0(state_reg[2]),
        .I1(temp_state[2]),
        .I2(state_reg[1]),
        .I3(stop_bap),
        .I4(\n_0_temp_state[2]_i_4 ),
        .I5(state_reg[0]),
        .O(\n_0_temp_state[2]_i_3 ));
LUT6 #(
    .INIT(64'hFFFFFFFF04001000)) 
     \temp_state[2]_i_4 
       (.I0(\n_0_data_reg_s_reg[7] ),
        .I1(p_3_in),
        .I2(p_4_in),
        .I3(\n_0_temp_state[2]_i_5 ),
        .I4(p_2_in),
        .I5(\n_0_data_reg_s_reg[0] ),
        .O(\n_0_temp_state[2]_i_4 ));
LUT3 #(
    .INIT(8'h80)) 
     \temp_state[2]_i_5 
       (.I0(p_6_in),
        .I1(p_1_in),
        .I2(p_5_in),
        .O(\n_0_temp_state[2]_i_5 ));
LUT1 #(
    .INIT(2'h1)) 
     \temp_state[3]_i_1 
       (.I0(enable),
        .O(\n_0_temp_state[3]_i_1 ));
LUT6 #(
    .INIT(64'h030003030C800C8C)) 
     \temp_state[3]_i_2 
       (.I0(temp_state[3]),
        .I1(state_reg[3]),
        .I2(state_reg[1]),
        .I3(state_reg[0]),
        .I4(stop_bap),
        .I5(state_reg[2]),
        .O(state_next[3]));
FDRE #(
    .INIT(1'b0)) 
     \temp_state_reg[0] 
       (.C(clk),
        .CE(\n_0_temp_state[3]_i_1 ),
        .D(state_next[0]),
        .Q(temp_state[0]),
        .R(reset));
MUXF7 \temp_state_reg[0]_i_1 
       (.I0(\n_0_temp_state[0]_i_2 ),
        .I1(\n_0_temp_state[0]_i_3 ),
        .O(state_next[0]),
        .S(state_reg[3]));
FDRE #(
    .INIT(1'b0)) 
     \temp_state_reg[1] 
       (.C(clk),
        .CE(\n_0_temp_state[3]_i_1 ),
        .D(state_next[1]),
        .Q(temp_state[1]),
        .R(reset));
MUXF7 \temp_state_reg[1]_i_1 
       (.I0(\n_0_temp_state[1]_i_2 ),
        .I1(\n_0_temp_state[1]_i_3 ),
        .O(state_next[1]),
        .S(state_reg[3]));
FDRE #(
    .INIT(1'b0)) 
     \temp_state_reg[2] 
       (.C(clk),
        .CE(\n_0_temp_state[3]_i_1 ),
        .D(state_next[2]),
        .Q(temp_state[2]),
        .R(reset));
MUXF7 \temp_state_reg[2]_i_1 
       (.I0(\n_0_temp_state[2]_i_2 ),
        .I1(\n_0_temp_state[2]_i_3 ),
        .O(state_next[2]),
        .S(state_reg[3]));
FDRE #(
    .INIT(1'b0)) 
     \temp_state_reg[3] 
       (.C(clk),
        .CE(\n_0_temp_state[3]_i_1 ),
        .D(state_next[3]),
        .Q(temp_state[3]),
        .R(reset));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (weak1, weak0) GSR = GSR_int;
    assign (weak1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
