
library ieee;
use ieee.std_logic_1164.all;
use ieee.math_real.all;
use ieee.numeric_std.all;
use work.dependencies_pack.all;

entity testbench1 is
  
end; --entity testbench

architecture logic of testbench1 is
  
component tmc is
  Generic
        (
        data_width : integer range 32 to 64 := 32;
        addr_width : integer := 32
        );

  Port
        (
	clock              : in  std_logic;
        reset_in           : in  std_logic;

	-- axi master interface
	axim_done	: in std_logic;
	axim_error_in: in std_logic;

	axim_address : out std_logic_vector(31 downto 0);
	axim_data_out : out std_logic_vector(31 downto 0);
	axim_data_in : in std_logic_vector(31 downto 0);
	axim_read_en : out std_logic;
	axim_write_en : out std_logic;

	axim_start	: out std_logic;

	enable_type_out: out std_logic_vector(1 downto 0);
--	data_read_out  : out  std_logic_vector(31 downto 0);
--	address_ram_store_out : out std_logic_vector(31 downto 0);
--	mdata_out_store_out: out std_logic_vector(31 downto 0);
--	state_out: out std_logic;

--	write_enable_out : out std_logic;
--	write_data_out : out std_logic_vector(31 downto 0);
--	address_from_MMU_out : out std_logic_vector(25 downto 0);
	data_from_TMC_out : out std_logic_vector(31 downto 0);
	virtual_mem_address_out : out std_logic_vector(31 downto 0);
--	virtual_mem_select_p_out : out std_logic_vector(1 downto 0);
--	c_alu_out : out std_logic_vector(31 downto 0);
	t_opcode      : out std_logic_vector(31 downto 0);
	pause_out    : out std_logic;
--	a_bus_p_out : out std_logic_vector(31 downto 0);
	b_bus_p_out : out std_logic_vector(31 downto 0);
--	c_alu_p_out : out std_logic_vector(31 downto 0);
--	c_alu_p_p_out : out std_logic_vector(31 downto 0);
	reg_source_1_gp_out : out std_logic_vector(31 downto 0);

	reset_out1 : out std_logic;

	i_mem_sel : out  std_logic_vector(3 downto 0);
     --   mem_write    : in  std_logic;
	i_mem_en      : out std_logic;
        i_mem_address  : out  std_logic_vector(31 downto 0);
        i_mem_data_w  : out std_logic_vector(31 downto 0);
        i_mem_data_r  : in std_logic_vector(31 downto 0)        
        );
end component; --entity tmc
  
  component ram
    generic(load_file_name :    string); 
    port(clk, reset        : in std_logic;
      mem_byte_sel : in  std_logic_vector(3 downto 0);
      mem_write    : in  std_logic;
      mem_address  : in  std_logic_vector;
      mem_data_w   : in  std_logic_vector(31 downto 0);
      mem_data_r   : out std_logic_vector(31 downto 0));
  end component;

component myip_v2_0 is
	generic (
		-- Users to add parameters here

		-- User parameters ends
		-- Do not modify the parameters beyond this line


		-- Parameters of Axi Master Bus Interface M00_AXI
		C_M00_AXI_START_DATA_VALUE	: std_logic_vector	:= x"AA000000";
		C_M00_AXI_TARGET_SLAVE_BASE_ADDR	: std_logic_vector	:= x"00001000";
		C_M00_AXI_ADDR_WIDTH	: integer	:= 32;
		C_M00_AXI_DATA_WIDTH	: integer	:= 32;
		C_M00_AXI_TRANSACTIONS_NUM	: integer	:= 1
	);
	port (
		-- Users to add ports here
		address_in : in std_logic_vector(C_M00_AXI_ADDR_WIDTH-1 downto 0);
		data_in	: in std_logic_vector(C_M00_AXI_DATA_WIDTH-1 downto 0);
		data_out : out std_logic_vector(C_M00_AXI_DATA_WIDTH-1 downto 0);
		read_en : in std_logic;
		write_en : in std_logic;

		-- User ports ends
		-- Do not modify the ports beyond this line


		-- Ports of Axi Master Bus Interface M00_AXI
		m00_axi_init_axi_txn	: in std_logic;
		m00_axi_error	: out std_logic;
		m00_axi_txn_done	: out std_logic;
		m00_axi_aclk	: in std_logic;
		m00_axi_aresetn	: in std_logic;
		m00_axi_awaddr	: out std_logic_vector(C_M00_AXI_ADDR_WIDTH-1 downto 0);
		m00_axi_awprot	: out std_logic_vector(2 downto 0);
		m00_axi_awvalid	: out std_logic;
		m00_axi_awready	: in std_logic;
		m00_axi_wdata	: out std_logic_vector(C_M00_AXI_DATA_WIDTH-1 downto 0);
		m00_axi_wstrb	: out std_logic_vector(C_M00_AXI_DATA_WIDTH/8-1 downto 0);
		m00_axi_wvalid	: out std_logic;
		m00_axi_wready	: in std_logic;
		m00_axi_bresp	: in std_logic_vector(1 downto 0);
		m00_axi_bvalid	: in std_logic;
		m00_axi_bready	: out std_logic;
		m00_axi_araddr	: out std_logic_vector(C_M00_AXI_ADDR_WIDTH-1 downto 0);
		m00_axi_arprot	: out std_logic_vector(2 downto 0);
		m00_axi_arvalid	: out std_logic;
		m00_axi_arready	: in std_logic;
		m00_axi_rdata	: in std_logic_vector(C_M00_AXI_DATA_WIDTH-1 downto 0);
		m00_axi_rresp	: in std_logic_vector(1 downto 0);
		m00_axi_rvalid	: in std_logic;
		m00_axi_rready	: out std_logic
	);
end component;

component reset_gen is
    Port
        (
	clock           : in std_logic;
        reset_in      : in std_logic;

	i_mem_sel : out  std_logic_vector(3 downto 0);
     	i_mem_en      : out std_logic;
        i_mem_address  : out  std_logic_vector(31 downto 0);
        i_mem_data_w  : out std_logic_vector(31 downto 0);
        i_mem_data_r  : in std_logic_vector(31 downto 0);

	reset_out1: out std_logic;	
	reset_out : out std_logic);
end component; 
  
  
  signal clk            : std_logic := '1';
--  signal reset_out          : std_logic; --, '0' after 100 ns;
  signal reset1          : std_logic := '0';
  
  
  
  signal i_mem_address  : std_logic_vector(31 downto 0);
  signal i_mem_data_w   : std_logic_vector(31 downto 0);
  signal i_mem_data_r   : std_logic_vector(31 downto 0);
  

      -- local BRAM interface
    signal i_mem_en : std_logic;
    signal i_mem_sel : std_logic_vector(3 downto 0);

	signal axim_done	: std_logic;
	signal axim_error_in: std_logic;

	signal axim_address : std_logic_vector(31 downto 0);
	signal axim_data_out : std_logic_vector(31 downto 0);
	signal axim_data_in : std_logic_vector(31 downto 0);
	signal axim_read_en : std_logic;
	signal axim_write_en : std_logic;

	signal axim_start	: std_logic;	

	signal reset_out1, reset_out2 : std_logic;
	signal reset_out : std_logic;

	signal i_mem_data_r1  : std_logic_vector(31 downto 0);

	signal m00_axi_awaddr	: std_logic_vector(31 downto 0);
	signal 	m00_axi_awprot	: std_logic_vector(2 downto 0);
	signal 	m00_axi_awvalid	: std_logic;
	signal 	m00_axi_awready	: std_logic;
	signal 	m00_axi_wdata	: std_logic_vector(31 downto 0);
	signal 	m00_axi_wstrb	: std_logic_vector(32/8-1 downto 0);
	signal 	m00_axi_wvalid	: std_logic;
	signal 	m00_axi_wready	: std_logic;
	signal 	m00_axi_bresp	: std_logic_vector(1 downto 0);
	signal 	m00_axi_bvalid	: std_logic;
	signal 	m00_axi_bready	: std_logic;
	signal 	m00_axi_araddr	: std_logic_vector(31 downto 0);
	signal 	m00_axi_arprot	: std_logic_vector(2 downto 0);
	signal 	m00_axi_arvalid	: std_logic;
	signal 	m00_axi_arready	: std_logic;
	signal 	m00_axi_rdata	: std_logic_vector(31 downto 0);
	signal 	m00_axi_rresp	: std_logic_vector(1 downto 0);
	signal 	m00_axi_rvalid	: std_logic;
	signal 	m00_axi_rready	: std_logic;

	signal 	i_mem_sel1 : std_logic_vector(3 downto 0);
     	signal 	i_mem_en1      : std_logic;
        signal 	i_mem_address1  : std_logic_vector(31 downto 0);
        signal 	i_mem_data_w1  : std_logic_vector(31 downto 0);

	signal enable_type_out: std_logic_vector(1 downto 0);
--	signal data_read_out  :  std_logic_vector(31 downto 0);
--	signal address_ram_store_out : std_logic_vector(31 downto 0);
--	signal mdata_out_store_out: std_logic_vector(31 downto 0);
--	signal state_out: std_logic;

--	signal write_enable_out : std_logic;
--	signal write_data_out : std_logic_vector(31 downto 0);
--	signal address_from_MMU_out : std_logic_vector(25 downto 0);
	signal data_from_TMC_out : std_logic_vector(31 downto 0);
	signal virtual_mem_address_out : std_logic_vector(31 downto 0);
--	signal virtual_mem_select_p_out : std_logic_vector(1 downto 0);
--	signal c_alu_out : std_logic_vector(31 downto 0);
	signal t_opcode      : std_logic_vector(31 downto 0);
	signal pause_out    : std_logic;
--	signal a_bus_p_out : std_logic_vector(31 downto 0);
	signal b_bus_p_out : std_logic_vector(31 downto 0);
--	signal c_alu_p_out : std_logic_vector(31 downto 0);
--	signal c_alu_p_p_out : std_logic_vector(31 downto 0);
	signal reg_source_1_gp_out : std_logic_vector(31 downto 0);
  

begin --architecture
  --  i_mem_en <= pc_enable;
  -- initialize security policy registers (TPRs and TCRs)
  -- only in simulation 

  clk         <= not clk after 10 ns;
  
  reset1       <= '1' after 200 ns; 	

i_mem_data_r1 <= x"FFFFFFFF";
  
    u1 : tmc 
  Generic map
        (
        32,
        32
        )

  Port map
        (
	clk,
	reset_out,

	axim_done,
	axim_error_in,

	axim_address,
	axim_data_out,
	axim_data_in,
	axim_read_en,
	axim_write_en,

	axim_start,

	enable_type_out,
	
	data_from_TMC_out,
	virtual_mem_address_out,
	
	t_opcode,
	pause_out,
--	a_bus_p_out,
	b_bus_p_out,
--	c_alu_p_out,
--	c_alu_p_p_out,
	reg_source_1_gp_out,

	reset_out1,
	
     	i_mem_sel,
	i_mem_en,
        i_mem_address,
        i_mem_data_w,
        i_mem_data_r
        );
  
    u2 : ram generic map ("/lab/home/labsticc/biswas/tmc/code_dependencies.txt")
    PORT MAP (
      clk          => clk,
      reset        => reset_out1,
      mem_byte_sel => i_mem_sel,
      mem_write    => '0',
      mem_address  => i_mem_address(inst_mem_address_width-1 downto 0),
      mem_data_w   => i_mem_data_w,
      mem_data_r   => i_mem_data_r);

u3 : myip_v2_0
	generic map (
		-- Users to add parameters here

		-- User parameters ends
		-- Do not modify the parameters beyond this line


		-- Parameters of Axi Master Bus Interface M00_AXI
		x"AA000000",
		x"00001000",
		32,
		32,
		1)
	port map (
		-- Users to add ports here
		axim_address,
		axim_data_out,
		axim_data_in,
		axim_read_en,
		axim_write_en,

		-- User ports ends
		-- Do not modify the ports beyond this line


		-- Ports of Axi Master Bus Interface M00_AXI
		axim_start,
		axim_error_in,
		axim_done,

		clk,
		reset1,

		m00_axi_awaddr,
		m00_axi_awprot,
		m00_axi_awvalid,
		m00_axi_awready,
		m00_axi_wdata,
		m00_axi_wstrb,
		m00_axi_wvalid,
		m00_axi_wready,
		m00_axi_bresp,
		m00_axi_bvalid,
		m00_axi_bready,
		m00_axi_araddr,
		m00_axi_arprot,
		m00_axi_arvalid,
		m00_axi_arready,
		m00_axi_rdata,
		m00_axi_rresp,
		m00_axi_rvalid,
		m00_axi_rready);


u4 : reset_gen
    Port map
        (
	clk,
        reset1,

	i_mem_sel1,
     	i_mem_en1,
        i_mem_address1,
        i_mem_data_w1,
        i_mem_data_r1,

	reset_out2,	
	reset_out);
  

end; --architecture logic

