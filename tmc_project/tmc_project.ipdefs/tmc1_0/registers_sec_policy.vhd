---------------------------------------------------------------------
-- TITLE: registers_sec_policy
-- AUTHOR: MAW
-- DATE CREATED: 24/10/2017 (updated: 28/12/2017)
-- FILENAME: registers_sec_policy.vhd
-- PROJECT: Dependencies
-- DESCRIPTION:
--    Implements TPRs and TCRs 
--    (register format similar to RAKSHA adapted to our need)
--    cf. DIFT coprocessor file for more details
---------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.dependencies_pack.all;

entity registers_sec_policy is
   port(clk            : in  std_logic;
        enable_sec_pol : in  std_logic_vector(nb_sec_policy-1 downto 0); -- which security policy to enable
        config_regs    : in  configuration_array;
        arm_opcode_t   : in  arm_opcode_type;
        tag_size       : in  tag_size_policies; --array containing tag size for each security policy
        tag_alu_func   : out tag_alu_function_type; 
        tag_prop_en    : out tag_prop_type;
        tag_check_en   : out tag_check_type;
        tag_alu_check  : out tag_alu_check_function_type );
end; --entity registers_sec_policy

architecture logic of registers_sec_policy is
    -- TPRs
    signal reg00, reg01, reg02, reg03 : std_logic_vector(31 downto 0); --:= (31 downto 2 => '1') & "01";
    -- TCRs
    signal reg04, reg05, reg06, reg07 : std_logic_vector(31 downto 0); -- := (31 downto 2 => '1') & "10";    


    function prop_tag(a: in std_logic_vector(2 downto 0)) return tag_prop_type is 
      variable result : tag_prop_type;
    begin
      case a is 
        when "000" => 
          result := tag_source_prop_off;
        when "001" => 
          result := tag_source_prop_en;
        when "010" =>
          result := tag_source_address_prop_en;
        when "100" =>
          result := tag_dest_address_prop_en;
        when others =>
          result := tag_source_prop_none;
      end case;
    return result;
    end function; -- prop_tag


    function prop_tag_custom(a: in std_logic_vector(1 downto 0)) return tag_prop_type is 
      variable result : tag_prop_type;
    begin
      case a is 
        when "01" => 
          result := tag_source_prop_en;
        when "10" =>
          result := tag_source_address_prop_en;
        when others =>
          result := tag_source_prop_none;
      end case;
    return result;
    end function; -- prop_tag_custom

    
    function check_tag(a: in std_logic_vector(1 downto 0)) return tag_check_type is 
      variable result : tag_check_type;
    begin
      case a is 
      when "01" => 
        result := tag_source_check_en;
      when "10" => 
        result := tag_dest_check_en;
      when others => 
        result := tag_source_check_none;
    end case;
    return result;
    end function; -- check_tag

    function check_tag_mov(a: in std_logic_vector(3 downto 0)) return tag_check_type is
    variable result : tag_check_type;
    begin
      case a is 
        when "0001" => 
          result := tag_source_check_en;
        when "0010" => 
          result := tag_source_address_check_en;
        when "0100" => 
          result := tag_dest_check_en;  
        when "1000" => 
          result := tag_dest_address_check_en;
        when others => 
          result := tag_source_check_none;    
      end case;
      return result;
    end function; --check_tag_mov

    function check_tag_custom(a: in std_logic_vector(2 downto 0)) return tag_check_type is
    variable result : tag_check_type;
    begin
      case a is
        when "001" =>
          result := tag_source_check_en; 
        when "010" =>
          result := tag_source_2_check_en; 
        when "100" =>
          result := tag_dest_check_en;
        when others => 
          result := tag_source_check_none;
      end case;
      return result;
    end function; --check_tag_custom

begin

  -- TPRs
  reg00 <= config_regs(0);
  reg01 <= config_regs(2);
  reg02 <= config_regs(4);
  reg03 <= config_regs(6);

  -- TCRs
  reg04 <= config_regs(1);
  reg05 <= config_regs(3);
  reg06 <= config_regs(5);
  reg07 <= config_regs(7);


reg_proc: process(clk,reg00, reg01, reg02, reg03, arm_opcode_t,
                  reg04, reg05, reg06, reg07, enable_sec_pol)
variable index_mode : integer := 0;
variable index_en   : integer := 0;
begin  
   
  if arm_opcode_t /= ZERO(3 downto 0) then 
    index_mode := to_integer(unsigned(arm_opcode_t) - 1);
  else
    index_mode := to_integer(unsigned(ZERO(3 downto 0)));
  end if;

  tag_alu_check <= tag_alu_check_eq;      
   case enable_sec_pol is            
      when "0001" => -- | "0010" | "0010" | "1000" 
        -- TPR 0 (reg00)
        case arm_opcode_t is
        when opcode_none => 
          tag_alu_func  <= tag_alu_nothing;
        when others => -- @TODO: what about combined case ? 
          case reg00(2*index_mode+1 downto 2*index_mode) is -- mode bits
          when "00" =>
            tag_alu_func  <= tag_alu_nothing;
          when "01" =>              
            tag_alu_func  <= tag_alu_and;
          when "10" =>               
            tag_alu_func  <= tag_alu_or;
          when "11" =>              
            tag_alu_func  <= tag_alu_xor;
          end case;
        end case;        

        -- bits 20 to 30 of TPR
        case arm_opcode_t is
          when opcode_mov => 
            tag_prop_en <= prop_tag(reg00(22 downto 20)); 
          
          when opcode_custom1 => 
            tag_prop_en <= prop_tag_custom(reg00(24 downto 23)); 
              
          when opcode_custom2 => 
            tag_prop_en <= prop_tag_custom(reg00(26 downto 25)); 
            
          when opcode_custom3 => 
            tag_prop_en <= prop_tag_custom(reg00(28 downto 27)); 
            
          when opcode_custom4 => 
            tag_prop_en <= prop_tag_custom(reg00(30 downto 29)); 

          when others =>   
            tag_prop_en <= tag_source_prop_off;
        end case;

        -- TCR 0 (reg04)        
        case reg04(1 downto 0) is 
          when "01" => 
            tag_check_en <= tag_pc_check;
          when "10" => 
            tag_check_en <= tag_instr_check;  
          when others => 
            tag_check_en <= tag_source_check_none;
        end case;

      case arm_opcode_t is
        when opcode_mov => 
          tag_check_en <= check_tag_mov(reg04(5 downto 2));

        when opcode_arith_log => 
          tag_check_en <= check_tag(reg04(7 downto 6));
            
        when opcode_comp => 
          tag_check_en <= check_tag(reg04(9 downto 8));
          
        when opcode_fp_mov => 
          tag_check_en <= check_tag_mov(reg04(13 downto 10));

        when opcode_fp_arith_log => 
          tag_check_en <= check_tag(reg04(15 downto 14));

        when opcode_fp_comp => 
          tag_check_en <= check_tag(reg04(17 downto 16));

        when opcode_custom1 => 
          tag_check_en <= check_tag_custom(reg04(20 downto 18));

        when opcode_custom2 => 
          tag_check_en <= check_tag_custom(reg04(23 downto 21));

        when opcode_custom3 => 
          tag_check_en <= check_tag_custom(reg04(26 downto 24));

        when opcode_custom4 => 
          tag_check_en <= check_tag_custom(reg04(29 downto 27));

        when others => 
          tag_check_en <= tag_source_check_none;
      end case;

      -------------------  
      when "0010" =>
        -- TPR 1 (reg01)
        case arm_opcode_t is
        when opcode_none => 
          tag_alu_func  <= tag_alu_nothing;
        when others => -- what about combined case ? 
          case reg01(2*index_mode+1 downto 2*index_mode) is -- mode bits
          when "00" =>
            tag_alu_func  <= tag_alu_nothing;
          when "01" =>              
            tag_alu_func  <= tag_alu_and;
          when "10" =>               
            tag_alu_func  <= tag_alu_or;
          when "11" =>              
            tag_alu_func  <= tag_alu_xor;
          end case;
        end case;

        -- bits 20 to 30 of TPR
        case arm_opcode_t is
          when opcode_mov => 
            tag_prop_en <= prop_tag(reg01(22 downto 20)); 
          
          when opcode_custom1 => 
            tag_prop_en <= prop_tag_custom(reg01(24 downto 23)); 
              
          when opcode_custom2 => 
            tag_prop_en <= prop_tag_custom(reg01(26 downto 25)); 
            
          when opcode_custom3 => 
            tag_prop_en <= prop_tag_custom(reg01(28 downto 27)); 
            
          when opcode_custom4 => 
            tag_prop_en <= prop_tag_custom(reg01(30 downto 29)); 

          when others =>   
            tag_prop_en <= tag_source_prop_en;
        end case;

        -- TCR 1 (reg05)
        case reg05(1 downto 0) is 
          when "01" => 
            tag_check_en <= tag_pc_check;
          when "10" => 
            tag_check_en <= tag_instr_check;  
          when others => 
            tag_check_en <= tag_source_check_none;
        end case;

      case arm_opcode_t is
        when opcode_mov => 
          tag_check_en <= check_tag_mov(reg05(5 downto 2));

        when opcode_arith_log => 
          tag_check_en <= check_tag(reg05(7 downto 6));
            
        when opcode_comp => 
          tag_check_en <= check_tag(reg05(9 downto 8));
          
        when opcode_fp_mov => 
          tag_check_en <= check_tag_mov(reg05(13 downto 10));

        when opcode_fp_arith_log => 
          tag_check_en <= check_tag(reg05(15 downto 14));

        when opcode_fp_comp => 
          tag_check_en <= check_tag(reg05(17 downto 16));

        when opcode_custom1 => 
          tag_check_en <= check_tag_custom(reg05(20 downto 18));

        when opcode_custom2 => 
          tag_check_en <= check_tag_custom(reg05(23 downto 21));

        when opcode_custom3 => 
          tag_check_en <= check_tag_custom(reg05(26 downto 24));

        when opcode_custom4 => 
          tag_check_en <= check_tag_custom(reg05(29 downto 27));

        when others => 
          tag_check_en <= tag_source_check_none;
      end case;

      --------  
      when "0100" =>
        -- TPR 2 (reg02)
        case arm_opcode_t is
        when opcode_none => 
          tag_alu_func  <= tag_alu_nothing;
        when others => -- what about combined case ? 
          case reg02(2*index_mode+1 downto 2*index_mode) is -- mode bits
          when "00" =>
            tag_alu_func  <= tag_alu_nothing;
          when "01" =>              
            tag_alu_func  <= tag_alu_and;
          when "10" =>               
            tag_alu_func  <= tag_alu_or;
          when "11" =>              
            tag_alu_func  <= tag_alu_xor;
          end case;
        end case;  

        -- bits 20 to 30 of TPR
        case arm_opcode_t is
          when opcode_mov => 
            tag_prop_en <= prop_tag(reg02(22 downto 20)); 
          
          when opcode_custom1 => 
            tag_prop_en <= prop_tag_custom(reg02(24 downto 23)); 
              
          when opcode_custom2 => 
            tag_prop_en <= prop_tag_custom(reg02(26 downto 25)); 
            
          when opcode_custom3 => 
            tag_prop_en <= prop_tag_custom(reg02(28 downto 27)); 
            
          when opcode_custom4 => 
            tag_prop_en <= prop_tag_custom(reg02(30 downto 29)); 

          when others =>   
            tag_prop_en <= tag_source_prop_en;
        end case;

      -- TCR 2 (reg06)
        case reg06(1 downto 0) is 
          when "01" => 
            tag_check_en <= tag_pc_check;
          when "10" => 
            tag_check_en <= tag_instr_check;  
          when others => 
            tag_check_en <= tag_source_check_none;
        end case;

      case arm_opcode_t is
        when opcode_mov => 
          tag_check_en <= check_tag_mov(reg06(5 downto 2));

        when opcode_arith_log => 
          tag_check_en <= check_tag(reg06(7 downto 6));
            
        when opcode_comp => 
          tag_check_en <= check_tag(reg06(9 downto 8));
          
        when opcode_fp_mov => 
          tag_check_en <= check_tag_mov(reg06(13 downto 10));

        when opcode_fp_arith_log => 
          tag_check_en <= check_tag(reg06(15 downto 14));

        when opcode_fp_comp => 
          tag_check_en <= check_tag(reg06(17 downto 16));

        when opcode_custom1 => 
          tag_check_en <= check_tag_custom(reg06(20 downto 18));

        when opcode_custom2 => 
          tag_check_en <= check_tag_custom(reg06(23 downto 21));

        when opcode_custom3 => 
          tag_check_en <= check_tag_custom(reg06(26 downto 24));

        when opcode_custom4 => 
          tag_check_en <= check_tag_custom(reg06(29 downto 27));

        when others => 
          tag_check_en <= tag_source_check_none;
      end case;
      
      ----------  
      when "1000" =>
        -- TPR 3 (reg03)
        case arm_opcode_t is
        when opcode_none => 
          tag_alu_func  <= tag_alu_nothing;
        when others => -- what about combined case ? 
          case reg03(2*index_mode+1 downto 2*index_mode) is -- mode bits
          when "00" =>
            tag_alu_func  <= tag_alu_nothing;
          when "01" =>              
            tag_alu_func  <= tag_alu_and;
          when "10" =>               
            tag_alu_func  <= tag_alu_or;
          when "11" =>              
            tag_alu_func  <= tag_alu_xor;
          end case;
        end case;        

        -- bits 20 to 30 of TPR
        case arm_opcode_t is
          when opcode_mov => 
            tag_prop_en <= prop_tag(reg03(22 downto 20)); 
          
          when opcode_custom1 => 
            tag_prop_en <= prop_tag_custom(reg03(24 downto 23)); 
              
          when opcode_custom2 => 
            tag_prop_en <= prop_tag_custom(reg03(26 downto 25)); 
            
          when opcode_custom3 => 
            tag_prop_en <= prop_tag_custom(reg03(28 downto 27)); 
            
          when opcode_custom4 => 
            tag_prop_en <= prop_tag_custom(reg03(30 downto 29)); 

          when others =>   
            tag_prop_en <= tag_source_prop_en;
        end case;


       -- TCR 3 (reg07)      
      case reg07(1 downto 0) is 
          when "01" => 
            tag_check_en <= tag_pc_check;
          when "10" => 
            tag_check_en <= tag_instr_check;  
          when others => 
            tag_check_en <= tag_source_check_none;
        end case;

      case arm_opcode_t is
        when opcode_mov => 
          tag_check_en <= check_tag_mov(reg07(5 downto 2));

        when opcode_arith_log => 
          tag_check_en <= check_tag(reg07(7 downto 6));
            
        when opcode_comp => 
          tag_check_en <= check_tag(reg07(9 downto 8));
          
        when opcode_fp_mov => 
          tag_check_en <= check_tag_mov(reg07(13 downto 10));

        when opcode_fp_arith_log => 
          tag_check_en <= check_tag(reg07(15 downto 14));

        when opcode_fp_comp => 
          tag_check_en <= check_tag(reg07(17 downto 16));

        when opcode_custom1 => 
          tag_check_en <= check_tag_custom(reg07(20 downto 18));

        when opcode_custom2 => 
          tag_check_en <= check_tag_custom(reg07(23 downto 21));

        when opcode_custom3 => 
          tag_check_en <= check_tag_custom(reg07(26 downto 24));

        when opcode_custom4 => 
          tag_check_en <= check_tag_custom(reg07(29 downto 27));

        when others => 
          tag_check_en <= tag_source_check_none;
      end case;

      --------  
      when others => 
        tag_prop_en <= tag_source_prop_off;
        tag_alu_func <= tag_alu_nothing;
        tag_check_en <= tag_source_check_none;
        tag_alu_check <= tag_alu_check_none;
   end case;

end process;

end; --architecture logic

