-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
-- Date        : Sun Jun  7 12:02:51 2020
-- Host        : HardBlare-Workstation running 64-bit Ubuntu 16.04.6 LTS
-- Command     : write_vhdl -force -mode synth_stub -rename_top design_1_BRAM_to_FIFO_0_1 -prefix
--               design_1_BRAM_to_FIFO_0_1_ design_1_BRAM_to_FIFO_0_2_stub.vhdl
-- Design      : design_1_BRAM_to_FIFO_0_2
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z020clg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity design_1_BRAM_to_FIFO_0_1 is
  Port ( 
    from_bram_address : in STD_LOGIC_VECTOR ( 31 downto 0 );
    from_bram_clk : in STD_LOGIC;
    from_bram_data_write : in STD_LOGIC_VECTOR ( 31 downto 0 );
    from_bram_enable : in STD_LOGIC;
    from_bram_reset : in STD_LOGIC;
    from_bram_bytes_selection : in STD_LOGIC_VECTOR ( 3 downto 0 );
    to_bram_data_read : out STD_LOGIC_VECTOR ( 31 downto 0 );
    to_fifo_data_write : out STD_LOGIC_VECTOR ( 31 downto 0 );
    to_fifo_write_enable : out STD_LOGIC;
    to_fifo_read_enable : out STD_LOGIC;
    to_fifo_clk : out STD_LOGIC;
    to_fifo_reset : out STD_LOGIC;
    from_fifo_data_read : in STD_LOGIC_VECTOR ( 31 downto 0 )
  );

end design_1_BRAM_to_FIFO_0_1;

architecture stub of design_1_BRAM_to_FIFO_0_1 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "from_bram_address[31:0],from_bram_clk,from_bram_data_write[31:0],from_bram_enable,from_bram_reset,from_bram_bytes_selection[3:0],to_bram_data_read[31:0],to_fifo_data_write[31:0],to_fifo_write_enable,to_fifo_read_enable,to_fifo_clk,to_fifo_reset,from_fifo_data_read[31:0]";
attribute x_core_info : string;
attribute x_core_info of stub : architecture is "BRAM_to_FIFO,Vivado 2018.2";
begin
end;
