library ieee;
use ieee.std_logic_1164.all;
use work.mips_pack.all;

entity forward_unit is 
port (
	a_in        : in  std_logic_vector(31 downto 0);
    b_in        : in  std_logic_vector(31 downto 0);
    alu_p       : in  std_logic_vector(31 downto 0);
    alu_p_p     : in  std_logic_vector(31 downto 0);
    data_read_m : in  std_logic_vector(31 downto 0);
    reg_source  : in  std_logic_vector(31 downto 0);
    reg_target  : in  std_logic_vector(31 downto 0);
    branch_func : in  branch_function_type;
	rs_index_dec: in std_logic_vector(5 downto 0); -- dec stage
	rt_index_dec: in std_logic_vector(5 downto 0); -- dec stage
	rs_index 	: in std_logic_vector(5 downto 0); -- exec stage
	rt_index 	: in std_logic_vector(5 downto 0); -- exec stage
	rd_index 	: in std_logic_vector(5 downto 0); -- exec stage
	rd_index_dec: in std_logic_vector(5 downto 0); -- dec stage
	rd_index_mem: in std_logic_vector(5 downto 0); -- mem stage
	rt_index_mem: in std_logic_vector(5 downto 0); -- mem stage
	rt_index_wb : in std_logic_vector(5 downto 0); -- wb stage
	rd_index_wb : in std_logic_vector(5 downto 0); -- wb stage
	ldr_instr 	: in std_logic; -- wb of load instruction
	a_source 	: in a_source_type; -- exec stage
	b_source 	: in b_source_type; -- exec stage
	mem_source_x: in mem_source_type; -- exec
	mem_source_m: in mem_source_type; -- mem
	mem_source_w: in mem_source_type; -- wb
	c_source_mem: in c_source_type; -- mem stage
	c_source_wb : in c_source_type; -- wb stage
	forward_c   : out std_logic_vector(1 downto 0);	
	forward_d   : out std_logic_vector(1 downto 0);
	forward_branch : out std_logic;
	take_branch : out std_logic;
	a_out       : out std_logic_vector(31 downto 0);
    b_out       : out std_logic_vector(31 downto 0)
	);
end entity;

architecture behavioral of forward_unit is 
signal temp : std_logic := '0';
signal temp_mem_s : std_logic;
signal forward_a_s : std_logic_vector(1 downto 0);
signal reg_source_s : std_logic_vector(31 downto 0);
signal reg_target_s : std_logic_vector(31 downto 0);
begin 		

	process(rs_index, rt_index, rd_index, rd_index_mem, rd_index_wb, c_source_mem, 
		a_in, b_in, alu_p, alu_p_p, data_read_m, rd_index_dec, rt_index_wb, ldr_instr,
		reg_source_s, reg_target_s, branch_func, rs_index_dec, reg_target, reg_source,
		rt_index_mem, c_source_wb, a_source, b_source, mem_source_m, mem_source_w)
	variable temp_ex : std_logic;
	variable temp_mem: std_logic;
	variable forward_a	: std_logic_vector(1 downto 0);
	variable forward_b	: std_logic_vector(1 downto 0);
	variable forward_c_v : std_logic_vector(1 downto 0);
    variable forward_d_v : std_logic_vector(1 downto 0);
    variable forward_branch_v : std_logic;
	variable a_output     : std_logic_vector(31 downto 0);
   	variable b_output     : std_logic_vector(31 downto 0);
   	variable aa, bb, sum : std_logic_vector(32 downto 0);
    variable is_equal : std_logic;
	begin
		--temp <= '0';
		-- problem in c_source conditions ... => @TODO	
		--if ( (c_source_mem = c_from_alu or 
		--	  c_source_mem = c_from_shift) and 
		--	  rd_index_mem /= ZERO(5 downto 0) ) then 
		--	temp_ex := '1'; 
		--else 
		--	temp_ex := '0';
		--end if;

		--if (c_source_wb = c_from_alu and rd_index_wb /= ZERO(5 downto 0) ) then
		--	temp_mem := '1'; 
		--else 
		--	temp_mem := '0';
		--end if;
		-- regwrite if mem_source=mem_none
		if ( mem_source_m = mem_none and rd_index_mem /=
			ZERO(5 downto 0) ) then
			temp_ex := '1'; 
		else 
			temp_ex := '0';
		end if;

		if ( mem_source_w = mem_none and rd_index_wb /= ZERO(5 downto 0) ) then
			temp_mem := '1'; 
		else 
			temp_mem := '0';
		end if;

		forward_a := "00";
		forward_b := "00";
		forward_c_v := "00";
		forward_d_v := "00";
		forward_branch_v := '0';

		if (temp_ex = '1' and rd_index_mem = rs_index and a_source = a_from_reg_source) then -- --EX/MEM HAZARD
			forward_a := "10";
		elsif (temp_mem = '1' and a_source = a_from_reg_source and rd_index_wb =
			   rs_index and rd_index_mem /= rs_index ) then -- --MEM/WB Hazard
			forward_a := "01";
		elsif (temp_mem = '1' and a_source = a_from_reg_source and rd_index_wb = 
		       rs_index and c_source_mem /= c_from_alu) then -- --MEM/WB Hazard
            forward_a := "01";
		elsif (temp_mem = '1' and rd_index_wb = rs_index  -- double data hazards
			   and (temp_ex = '0' and rd_index_mem = rs_index) and a_source = a_from_reg_source) then 
			forward_a := "01";
	    elsif (rt_index_wb = rs_index and ldr_instr = '1' and a_source =
               a_from_reg_source and rs_index /= ZERO(5 downto 0)) then
            forward_a := "11";
		end if;
		
		
		-- branch hazards are detected with forward_c_v        
        if (rd_index = rd_index_dec and rd_index /= ZERO(5 downto 0)) then 
            forward_c_v := "01";
        elsif (rd_index_dec = rd_index_mem and rd_index /= ZERO(5 downto 0)) then 
            forward_c_v := "10";
        elsif (rd_index_dec = rd_index_wb and rd_index /= ZERO(5 downto 0)) then 
            forward_c_v := "11";
        end if;
        
        if (temp_ex = '1' and rd_index_mem = rt_index and b_source = b_from_reg_target) then        
        --EX/MEM HAZARD
            forward_b := "10";
        elsif (temp_mem = '1' and rd_index_wb = rt_index and (rd_index_mem /= rt_index
               or c_source_mem /= c_from_alu)  and b_source = b_from_reg_target) then -- --MEM/WB Hazard
            forward_b := "01";
        elsif (temp_mem = '1' and rd_index_wb = rt_index -- double data hazards
               and (temp_ex = '0' and rd_index_mem = rt_index)  and b_source = b_from_reg_target) then 
            forward_b := "01";
        elsif (rt_index_wb = rt_index and ldr_instr = '1' and b_source = 
               b_from_reg_target and rt_index /= ZERO(5 downto 0)) then
            forward_b := "11";
        end if;
        
		if (rt_index_wb = rt_index_mem and rt_index_mem /= ZERO(5 downto 0) and -- rt_index /= ZERO(5 downto 0) condition is very important ! Do not forward if the rt register of sw is 0 ! 
 			rt_index /= ZERO(5 downto 0) ) then -- --and and mem_source_w /= mem_none 
		--ldr_instr = '1') then
			forward_d_v := "11";		
        elsif (temp_ex = '1' and rd_index_mem = rt_index) then 
            forward_d_v := "01";
        elsif (rd_index_wb = rt_index and rt_index /= ZERO(5 downto 0) and ldr_instr = '1') then 
            forward_d_v := "10";
        else
            forward_d_v := "00";
        end if;
        
        if (rd_index = rs_index_dec and rs_index_dec /= ZERO(5 downto 0)) then 
            forward_branch_v := '1';
        end if;

		--load-use data hazard
		--if (rd_index_wb = rs_index and c_source_wb = c_from_memory and a_source = a_from_reg_source) then 
		--	forward_a := "10";			
		--elsif (rd_index_wb = rt_index and c_source_wb = c_from_memory and b_source = b_from_reg_target) then
		--	forward_b := "10";		
		--end if;


		if forward_a = "00" then 
	      a_output := a_in;
	   elsif forward_a = "10" then 
	      a_output := alu_p;
	   elsif forward_a = "01" then
	      a_output := alu_p_p;
	   elsif forward_a = "11" then 
	      a_output := data_read_m; 
	   end if;

	   if forward_b = "00" then 
	      b_output := b_in;
	   elsif forward_b = "10" then 
	      b_output := alu_p;
	   elsif forward_b = "01" then
	      b_output := alu_p_p;
	   elsif forward_b = "11" then 
          b_output := data_read_m;
	   end if;
       
       reg_target_s <= reg_target;
       if (rd_index_mem = rt_index and rt_index /= zero(5 downto 0) and mem_source_m = mem_none) then 
            reg_target_s <= alu_p;
       elsif (rt_index_wb = rt_index and rt_index /= zero(5 downto 0) and ldr_instr = '1') then 
            reg_target_s <= data_read_m;
       end if;
       
       reg_source_s <= reg_source;
       if (rd_index_mem = rs_index and rs_index /= zero(5 downto 0) and mem_source_m = mem_none) then 
            reg_source_s <= alu_p;
       elsif (rs_index = rt_index_wb and rs_index /= zero(5 downto 0) and mem_source_w = mem_read32) then 
            reg_source_s <= data_read_m;
       end if;
       
       --sum := bv_adder(reg_source, reg_target_s, '0'); -- subtract operation always
       
--       pc_mux: process(branch_func, reg_source, reg_target_s)  --temp_sum
          
--          variable temp_reg_source : std_logic_vector(31 downto 0);
--       begin
--          temp_reg_source := reg_source;
--          if forward = "01" then 
--             reg_source := c_alu;
--          end if;  
          if reg_source_s = reg_target_s then
             is_equal := '1';
          --elsif temp_sum = '1' then 
          --   is_equal := '1';
          else
             is_equal := '0';
          end if;
          case branch_func is
          when branch_ltz =>
             take_branch <= reg_source_s(31);
          when branch_lez =>
             take_branch <= reg_source_s(31) or is_equal;
          when branch_eq =>
             take_branch <= is_equal;
          when branch_ne =>
             take_branch <= not is_equal;
          when branch_gez =>
             take_branch <= not reg_source_s(31);
          when branch_gtz =>
             take_branch <= not reg_source_s(31) and not is_equal;
          when branch_yes =>
             take_branch <= '1';
          when others =>
             take_branch <= is_equal;
          end case;
--       end process;
       
       
	   a_out <= a_output;
	   b_out <= b_output;
	   forward_c <= forward_c_v;
       forward_d <= forward_d_v;
       forward_branch <= forward_branch_v;
       forward_a_s <= forward_a;
       temp_mem_s <= temp_mem;
	end process;

end architecture;