---------------------------------------------------------------------
-- TITLE: Bus Multiplexer / Signal Router
-- AUTHOR: Steve Rhoads (rhoadss@yahoo.com)
-- DATE CREATED: 2/8/01
-- FILENAME: bus_mux.vhd
-- PROJECT: MIPS CPU core
-- COPYRIGHT: Software placed into the public domain by the author.
--    Software 'as is' without warranty.  Author liable for nothing.
-- DESCRIPTION:
--    This entity is the main signal router.  
--    It multiplexes signals from multiple sources to the correct location.
--    The outputs are as follows:
--       a_bus        : goes to the ALU
--       b_bus        : goes to the ALU
--       reg_dest_out : goes to the register bank
--       take_branch  : a signal to pc_next
---------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use work.mips_pack.all;

entity bus_mux is
   port(imm_in       : in  std_logic_vector(15 downto 0);
        reg_source   : in  std_logic_vector(31 downto 0);        
        forward      : in  std_logic_vector(1 downto 0);
        forward_branch: in std_logic;
        
        a_mux        : in  a_source_type;        
        a_out        : out std_logic_vector(31 downto 0);

        reg_target   : in  std_logic_vector(31 downto 0);
        b_mux        : in  b_source_type;
        b_out        : out std_logic_vector(31 downto 0);

        c_alu        : in  std_logic_vector(31 downto 0);
        c_bus        : in  std_logic_vector(31 downto 0);
        c_memory     : in  std_logic_vector(31 downto 0);
        c_pc_np      : in  std_logic_vector(31 downto 0);
        c_pc         : in  std_logic_vector(31 downto 0);
        c_imm_in     : in  std_logic_vector(15 downto 0);
        c_mux        : in  c_source_type;
        reg_dest_out : out std_logic_vector(31 downto 0);

        pc_new       : out std_logic_vector(31 downto 0) );
end; --entity bus_mux

architecture logic of bus_mux is
  signal temp_sum : std_logic;
begin
--   type a_source_type is (a_from_reg_source, a_from_imm10_6);
--   type b_source_type is (b_from_reg_target, b_from_imm, b_from_signed_imm);
--   type c_source_type is (c_from_null, c_from_alu, c_from_shift, 
--      c_from_mult, c_from_memory, c_from_pc, c_from_imm_shift16,
--      c_from_reg_source_nez, c_from_reg_source_eqz);
amux_bmux: process(reg_source, imm_in, a_mux, forward_branch, 
                   c_alu, reg_target, reg_source, imm_in, b_mux, c_pc_np) 
variable aa, bb, sum : std_logic_vector(32 downto 0);
variable a_o, b_o    : std_logic_vector(31 downto 0);
variable temp        : std_logic_vector(31 downto 0);
begin      
     a_o(31 downto 5) := reg_source(31 downto 5);
     case a_mux is
     when a_from_reg_source =>
        a_o(4 downto 0) := reg_source(4 downto 0);
     when a_from_imm10_6 =>
        a_o(4 downto 0) := imm_in(10 downto 6);
     when others =>  --a_from_pc
        a_o := c_pc_np;
     end case;   

     case b_mux is
     when b_from_reg_target  =>
        b_o := reg_target;
     when b_from_imm => 
        b_o := ZERO(31 downto 16) & imm_in;
     when b_from_signed_imm =>
        if imm_in(15) = '0' then
           b_o(31 downto 16) := ZERO(31 downto 16);
        else
           b_o(31 downto 16) := "1111111111111111";
        end if;
        b_o(15 downto 0) := imm_in;
     when b_null => 
        b_o := ZERO;
     when others =>             --b_from_immX4
        if imm_in(15) = '0' then
           b_o(31 downto 18) := "00000000000000";
        else
           b_o(31 downto 18) := "11111111111111";
        end if;
        b_o(17 downto 0) := imm_in & "00";
     end case;
      a_out <= a_o;
      b_out <= b_o;
--      aa := (a_o(31) and '1') & a_o;
--      bb := (b_o(31) and '1') & b_o;
--      -- @TODO: reduce branch delay ! 
--      -- an adder is added in decode stage for 
--      -- reducing branch delay to 1 cycle instead of 2  
--      sum := bv_adder(aa, bb, '0'); -- subtract operation always
      
--      if branch_func = branch_yes and forward_branch = '1' then 
--        pc_new <= c_alu;
--      elsif branch_func = branch_yes and forward_branch = '0' then
--        pc_new <= reg_source;
--      else
--        pc_new <= sum(31 downto 0);
--      end if;
      
--      case branch_func is 
--      when branch_yes =>
--        pc_new <= c_alu;
--      when others => 
--        pc_new <= sum(31 downto 0);
--      end case;
      --if forward = '1' then 
      --  temp_sum <= sum(31);
      --end if;

end process;

cmux: process(c_bus, c_memory, c_pc, c_imm_in, c_mux) 
begin
   case c_mux is
   when c_from_alu | c_from_shift | c_from_mult =>
      reg_dest_out <= c_bus;
   when c_from_memory =>
      reg_dest_out <= c_memory;
   when c_from_pc =>
      reg_dest_out <= c_pc;
   when c_from_imm_shift16 =>
      reg_dest_out <= c_imm_in & ZERO(15 downto 0);
--   when from_reg_source_nez =>
--????
--   when from_reg_source_eqz =>
--????
   when others =>
      reg_dest_out <= c_bus;
   end case;
end process;

end; --architecture logic
