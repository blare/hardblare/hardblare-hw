---------------------------------------------------------------------
-- TITLE: Program Counter Next
-- AUTHOR: Muhammad Abdul WAHAB
-- DATE CREATED: ?/11/2017
-- FILENAME: pc_next.vhd
-- PROJECT: DEPENDENCIES PU
-- COPYRIGHT: Software placed into the public domain by the author.
--    Software 'as is' without warranty.  Author liable for nothing.
-- DESCRIPTION:
--    Implements the Program Counter logic.
---------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use work.dependencies_pack.all;

entity pc_next is
   port(clk          : in std_logic;
        reset_in     : in std_logic;
        enable       : in std_logic;
        pc_new       : in std_logic_vector(31 downto 2);
        pause_in     : in std_logic;
        pc_out       : out std_logic_vector(31 downto 0));
end; --pc_next

architecture logic of pc_next is
   --signal en : std_logic;
   signal pc_reg : std_logic_vector(31 downto 2); --:= ZERO(31 downto 2);
begin
  --en <= enable;
--pc_next: process(clk, reset_in, pc_new, pause_in, pc_reg, enable)
--   variable pc_inc, pc_next : std_logic_vector(31 downto 2) := (others => '0');
--begin
--  if en = '1' then 
--    pc_inc := bv_increment(pc_reg);  --pc_reg+1
--    pc_next := pc_reg;
--  end if;

--  if pause_in = '0' then
--      pc_next := pc_inc;
--  end if;


--   if rising_edge(clk) then
--      if reset_in = '1' then
--          pc_reg <= ZERO(31 downto 2);
--      elsif en = '1' then 
--          pc_reg <= pc_next;
--      end if;
--   end if;

--   pc_out <= pc_reg & "00";
--end process;

process(clk)
begin
   if rising_edge(clk) then
      if reset_in = '1' then
          pc_reg <= ZERO(31 downto 2);
      elsif enable = '1' and pause_in = '0' then 
          pc_reg <= bv_increment(pc_reg);
      else 
          pc_reg <= pc_reg;
      end if;
   end if;
end process;

  pc_out <= pc_reg & "00";

end; --logic
