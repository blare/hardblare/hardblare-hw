----------
--
-- AUTHOR               : M.Abdul WAHAB
-- DATE                 : 2017
-- Description          : Pipeline registers for stage IF/ID
----------
library IEEE; 
use ieee.std_logic_1164.All;
use work.mips_pack.all;

entity pipeline_IF_ID is 
port    (
        clk, reset      : in  std_logic;
        stall           : in  std_logic;
        take_branch     : in  std_logic;
        pc_source       : in  pc_source_type;
        opcode          : in  std_logic_vector(31 downto 0);
        opcode_out      : out std_logic_vector(31 downto 0);
        pc              : in  std_logic_vector(31 downto 0);
        pc_out          : out std_logic_vector(31 downto 0)
         );
end entity;

architecture behavioral of pipeline_IF_ID is 

signal opcode_out_reg : std_logic_vector(31 downto 0);
signal pc_out_reg     : std_logic_vector(31 downto 0);
signal stall_r : std_logic;
begin

process(clk)
begin
    if rising_edge(clk) then 
        if reset = '1' or stall = '1' or stall_r = '1' then 
                opcode_out_reg      <= (others => '0');
        else
                opcode_out_reg      <= opcode;
        end if;
    end if;
end process;

process(clk)
begin
    if rising_edge(clk) then 
        if reset = '1' then 
            pc_out_reg <= ZERO;
        else
            pc_out_reg <= pc;
        end if;
    end if;
end process;

process(clk)
begin
    if rising_edge(clk) then 
        if reset = '0' or (take_branch = '0' and pc_source = from_branch) then 
            stall_r <= '0';
        else
            stall_r <= stall;
        end if;
    end if;
end process;

opcode_out      <= opcode_out_reg;
pc_out          <= pc_out_reg;

end architecture;