
#include "types.h"
#include "no_os.h"
#include "decode.h"
#include "memory_tag_trf.h"
#include "address.h"
#include "debug.h"
#include "irq.h"


// Raise IRQ when Memory/TRF doesnt found address
inline void raise_irq_when_unfound_memory_address(uint32_t tagged_address, uint32_t address, uint32_t size, uint32_t tag) {

  // The reason
  MemoryWrite(getPLtoPSAddress(), IRQ_REASON_MEMORY_TAG_UNFOUND_ADDRESS);
  // Size
  MemoryWrite(getPLtoPSAddress(), 4);
  // Address of the request
  MemoryWrite(getPLtoPSAddress(), tagged_address);
  // Address of the request
  MemoryWrite(getPLtoPSAddress(), address);
  // Size
  MemoryWrite(getPLtoPSAddress(), size);
  // Expected tag
  MemoryWrite(getPLtoPSAddress(), tag);
  // Raise IRQ
  MemoryWrite(PLTOPS_GENERATE_INTERRUPT, 0xffffffff);

}


// Raise IRQ when Memory/TRF is full
inline void raise_irq_when_mem_tag_full(uint32_t tagged_address, uint32_t address, uint32_t size, uint32_t tag){

  // The reason
  MemoryWrite(getPLtoPSAddress(), IRQ_REASON_MEMORY_TAG_FULL);
  // Size
  MemoryWrite(getPLtoPSAddress(), 4);
  // Address of the request
  MemoryWrite(getPLtoPSAddress(), tagged_address);
  // Address of the request
  MemoryWrite(getPLtoPSAddress(), address);
  // Size
  MemoryWrite(getPLtoPSAddress(), size);
  // Expected tag
  MemoryWrite(getPLtoPSAddress(), tag);
  // Raise IRQ
  MemoryWrite(PLTOPS_GENERATE_INTERRUPT, 0xffffffff);

}

// Raise IRQ when check failed
inline void raise_irq_when_check_failed(uint32_t address, uint32_t size, uint32_t tag_of_memory, uint32_t tag){

  // The reason
  MemoryWrite(getPLtoPSAddress(), IRQ_REASON_CHECK_FAILED );
  // Size
  MemoryWrite(getPLtoPSAddress(), 4);
  // Address of the request
  MemoryWrite(getPLtoPSAddress(), address);
  // Size
  MemoryWrite(getPLtoPSAddress(), size);
  // Resulting tag
  MemoryWrite(getPLtoPSAddress(), tag_of_memory);
  // Expected tag
  MemoryWrite(getPLtoPSAddress(), tag);
  // Raise IRQ
  MemoryWrite(PLTOPS_GENERATE_INTERRUPT, 0xffffffff);

}

// Raise IRQ when syscall protocol request dealloc memory failed
inline void raise_irq_when_dealloc_memory_failed(uint32_t address, uint32_t size, uint32_t tag_of_memory, uint32_t tag){

  // The reason
  MemoryWrite(getPLtoPSAddress(), IRQ_REASON_SYSCALL_REQUEST_DEALLOC_MEMORY);
  // Size
  MemoryWrite(getPLtoPSAddress(), 4);
  // Address of the request
  MemoryWrite(getPLtoPSAddress(), address);
  // Size
  MemoryWrite(getPLtoPSAddress(), size);
  // Resulting tag
  MemoryWrite(getPLtoPSAddress(), tag_of_memory);
  // Expected tag
  MemoryWrite(getPLtoPSAddress(), tag);
  // Raise IRQ
  MemoryWrite(PLTOPS_GENERATE_INTERRUPT, 0xffffffff);

}


// Raise IRQ when syscall protocol request alloc memory failed
inline void raise_irq_when_alloc_memory_failed(uint32_t address, uint32_t size, uint32_t tag_of_memory, uint32_t tag){

  // The reason
  MemoryWrite(getPLtoPSAddress(), IRQ_REASON_SYSCALL_REQUEST_ALLOC_MEMORY);
  // Size
  MemoryWrite(getPLtoPSAddress(), 4);
  // Address of the request
  MemoryWrite(getPLtoPSAddress(), address);
  // Size
  MemoryWrite(getPLtoPSAddress(), size);
  // Resulting tag
  MemoryWrite(getPLtoPSAddress(), tag_of_memory);
  // Expected tag
  MemoryWrite(getPLtoPSAddress(), tag);
  // Raise IRQ
  MemoryWrite(PLTOPS_GENERATE_INTERRUPT, 0xffffffff);

}
