// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
// Date        : Fri Jun  4 10:30:45 2021
// Host        : HardBlare-Workstation running 64-bit Ubuntu 16.04.6 LTS
// Command     : write_verilog -force -mode funcsim
//               /home/mounir/HardBlare/hardblare-hw/tmc_project/tmc_project.srcs/sources_1/bd/design_1/ip/design_1_decodeur_traces_1_1/design_1_decodeur_traces_1_1_sim_netlist.v
// Design      : design_1_decodeur_traces_1_1
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_decodeur_traces_1_1,decodeur_traces,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* ip_definition_source = "package_project" *) 
(* x_core_info = "decodeur_traces,Vivado 2018.2" *) 
(* NotValidForBitStream *)
module design_1_decodeur_traces_1_1
   (clk,
    reset,
    enable,
    trace_data,
    pc,
    w_en,
    w_ctxt_en,
    fifo_overflow,
    waypoint_address,
    context_id,
    waypoint_address_en);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk, ASSOCIATED_RESET reset, FREQ_HZ 190476196, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK1" *) input clk;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 reset RST" *) (* x_interface_parameter = "XIL_INTERFACENAME reset, POLARITY ACTIVE_LOW" *) input reset;
  input enable;
  input [7:0]trace_data;
  output [31:0]pc;
  output w_en;
  output w_ctxt_en;
  output fifo_overflow;
  output [31:0]waypoint_address;
  output [31:0]context_id;
  output waypoint_address_en;

  wire \<const0> ;
  wire clk;
  wire [31:0]context_id;
  wire enable;
  wire [31:0]pc;
  wire reset;
  wire [7:0]trace_data;
  wire w_ctxt_en;
  wire w_en;
  wire [31:2]\^waypoint_address ;
  wire waypoint_address_en;

  assign fifo_overflow = \<const0> ;
  assign waypoint_address[31:22] = \^waypoint_address [31:22];
  assign waypoint_address[21] = \<const0> ;
  assign waypoint_address[20] = \<const0> ;
  assign waypoint_address[19] = \<const0> ;
  assign waypoint_address[18] = \<const0> ;
  assign waypoint_address[17] = \<const0> ;
  assign waypoint_address[16] = \<const0> ;
  assign waypoint_address[15] = \<const0> ;
  assign waypoint_address[14:2] = \^waypoint_address [14:2];
  assign waypoint_address[1] = \<const0> ;
  assign waypoint_address[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  design_1_decodeur_traces_1_1_decodeur_traces U0
       (.clk(clk),
        .context_id(context_id),
        .enable(enable),
        .pc(pc),
        .reset(reset),
        .trace_data(trace_data),
        .w_ctxt_en(w_ctxt_en),
        .w_en(w_en),
        .waypoint_address({\^waypoint_address [31:22],\^waypoint_address [14:2]}),
        .waypoint_address_en(waypoint_address_en));
endmodule

(* ORIG_REF_NAME = "address_compute" *) 
module design_1_decodeur_traces_1_1_address_compute
   (D,
    \output_data_0_reg[5] ,
    pc,
    \pc_s_reg_reg[22]_0 ,
    \output_data_3_reg[6] ,
    \pc_s_reg_reg[21]_0 ,
    enable_reg_reg_reg,
    Q,
    \pc_reg[31] ,
    out_en_reg,
    out_en_reg_0,
    out_en_b_i,
    reset,
    waypoint_address,
    clk,
    en_0_reg_reg);
  output [0:0]D;
  output [0:0]\output_data_0_reg[5] ;
  output [31:0]pc;
  output \pc_s_reg_reg[22]_0 ;
  output \output_data_3_reg[6] ;
  output [0:0]\pc_s_reg_reg[21]_0 ;
  input enable_reg_reg_reg;
  input [1:0]Q;
  input [31:0]\pc_reg[31] ;
  input out_en_reg;
  input out_en_reg_0;
  input out_en_b_i;
  input reset;
  input [22:0]waypoint_address;
  input clk;
  input [4:0]en_0_reg_reg;

  wire [0:0]D;
  wire [1:0]Q;
  wire clk;
  wire [4:0]en_0_reg_reg;
  wire enable_reg_reg_reg;
  wire out_en_b_i;
  wire out_en_reg;
  wire out_en_reg_0;
  wire \output_data_0[5]_i_3_n_0 ;
  wire [0:0]\output_data_0_reg[5] ;
  wire \output_data_1[6]_i_3__0_n_0 ;
  wire \output_data_3_reg[6] ;
  wire [31:0]pc;
  wire [31:0]\pc_reg[31] ;
  wire [28:7]pc_s;
  wire [31:0]pc_s_0;
  wire \pc_s_reg[14]_i_2__0_n_0 ;
  wire \pc_s_reg[14]_i_3__0_n_0 ;
  wire \pc_s_reg[21]_i_2__0_n_0 ;
  wire \pc_s_reg[21]_i_3__0_n_0 ;
  wire \pc_s_reg[28]_i_3__0_n_0 ;
  wire \pc_s_reg[28]_i_4__0_n_0 ;
  wire \pc_s_reg[28]_i_5_n_0 ;
  wire \pc_s_reg[31]_i_10_n_0 ;
  wire \pc_s_reg[31]_i_2__0_n_0 ;
  wire \pc_s_reg[31]_i_3_n_0 ;
  wire \pc_s_reg[31]_i_4_n_0 ;
  wire \pc_s_reg[31]_i_5_n_0 ;
  wire \pc_s_reg[31]_i_6_n_0 ;
  wire \pc_s_reg[31]_i_7_n_0 ;
  wire \pc_s_reg[31]_i_8_n_0 ;
  wire \pc_s_reg[31]_i_9_n_0 ;
  wire \pc_s_reg[7]_i_2__0_n_0 ;
  wire \pc_s_reg[7]_i_3_n_0 ;
  wire [0:0]\pc_s_reg_reg[21]_0 ;
  wire \pc_s_reg_reg[22]_0 ;
  wire reset;
  wire [22:0]waypoint_address;
  wire [4:0]waypoint_address_cs_reg;
  wire [31:2]waypoint_address_reg;

  LUT6 #(
    .INIT(64'hF2F2F2F2F2D0D0D0)) 
    \output_data_0[5]_i_2__0 
       (.I0(enable_reg_reg_reg),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(waypoint_address_reg[7]),
        .I4(\pc_s_reg[7]_i_2__0_n_0 ),
        .I5(\output_data_0[5]_i_3_n_0 ),
        .O(\output_data_0_reg[5] ));
  LUT4 #(
    .INIT(16'hF888)) 
    \output_data_0[5]_i_3 
       (.I0(pc[7]),
        .I1(\pc_s_reg[31]_i_3_n_0 ),
        .I2(\pc_reg[31] [7]),
        .I3(\pc_s_reg[7]_i_3_n_0 ),
        .O(\output_data_0[5]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hF2F2F2F2F2D0D0D0)) 
    \output_data_1[6]_i_2__0 
       (.I0(enable_reg_reg_reg),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(waypoint_address_reg[14]),
        .I4(\pc_s_reg[14]_i_2__0_n_0 ),
        .I5(\output_data_1[6]_i_3__0_n_0 ),
        .O(D));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'hF888)) 
    \output_data_1[6]_i_3__0 
       (.I0(pc[14]),
        .I1(\pc_s_reg[31]_i_3_n_0 ),
        .I2(\pc_reg[31] [14]),
        .I3(\pc_s_reg[14]_i_3__0_n_0 ),
        .O(\output_data_1[6]_i_3__0_n_0 ));
  LUT4 #(
    .INIT(16'hF888)) 
    \output_data_3[6]_i_3 
       (.I0(waypoint_address_reg[28]),
        .I1(\pc_s_reg[28]_i_3__0_n_0 ),
        .I2(pc[28]),
        .I3(\pc_s_reg[31]_i_3_n_0 ),
        .O(\output_data_3_reg[6] ));
  LUT4 #(
    .INIT(16'hF888)) 
    \pc_s_reg[0]_i_1__0 
       (.I0(pc[0]),
        .I1(\pc_s_reg[31]_i_3_n_0 ),
        .I2(\pc_reg[31] [0]),
        .I3(\pc_s_reg[7]_i_3_n_0 ),
        .O(pc_s_0[0]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc_s_reg[10]_i_1__0 
       (.I0(\pc_s_reg[14]_i_2__0_n_0 ),
        .I1(waypoint_address_reg[10]),
        .I2(\pc_s_reg[14]_i_3__0_n_0 ),
        .I3(\pc_reg[31] [10]),
        .I4(pc[10]),
        .I5(\pc_s_reg[31]_i_3_n_0 ),
        .O(pc_s_0[10]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc_s_reg[11]_i_1__0 
       (.I0(\pc_s_reg[14]_i_2__0_n_0 ),
        .I1(waypoint_address_reg[11]),
        .I2(\pc_s_reg[14]_i_3__0_n_0 ),
        .I3(\pc_reg[31] [11]),
        .I4(pc[11]),
        .I5(\pc_s_reg[31]_i_3_n_0 ),
        .O(pc_s_0[11]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc_s_reg[12]_i_1__0 
       (.I0(\pc_s_reg[14]_i_2__0_n_0 ),
        .I1(waypoint_address_reg[12]),
        .I2(\pc_s_reg[14]_i_3__0_n_0 ),
        .I3(\pc_reg[31] [12]),
        .I4(pc[12]),
        .I5(\pc_s_reg[31]_i_3_n_0 ),
        .O(pc_s_0[12]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc_s_reg[13]_i_1__0 
       (.I0(\pc_s_reg[14]_i_2__0_n_0 ),
        .I1(waypoint_address_reg[13]),
        .I2(\pc_s_reg[14]_i_3__0_n_0 ),
        .I3(\pc_reg[31] [13]),
        .I4(pc[13]),
        .I5(\pc_s_reg[31]_i_3_n_0 ),
        .O(pc_s_0[13]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc_s_reg[14]_i_1__0 
       (.I0(waypoint_address_reg[14]),
        .I1(\pc_s_reg[14]_i_2__0_n_0 ),
        .I2(\pc_s_reg[14]_i_3__0_n_0 ),
        .I3(\pc_reg[31] [14]),
        .I4(\pc_s_reg[31]_i_3_n_0 ),
        .I5(pc[14]),
        .O(pc_s[14]));
  LUT2 #(
    .INIT(4'hE)) 
    \pc_s_reg[14]_i_2__0 
       (.I0(\pc_s_reg[31]_i_2__0_n_0 ),
        .I1(\pc_s_reg[31]_i_9_n_0 ),
        .O(\pc_s_reg[14]_i_2__0_n_0 ));
  LUT6 #(
    .INIT(64'h0000000200000100)) 
    \pc_s_reg[14]_i_3__0 
       (.I0(waypoint_address_cs_reg[4]),
        .I1(waypoint_address_cs_reg[0]),
        .I2(\pc_s_reg[21]_i_3__0_n_0 ),
        .I3(out_en_b_i),
        .I4(\pc_s_reg_reg[21]_0 ),
        .I5(out_en_reg_0),
        .O(\pc_s_reg[14]_i_3__0_n_0 ));
  LUT4 #(
    .INIT(16'hF888)) 
    \pc_s_reg[15]_i_1__0 
       (.I0(pc[15]),
        .I1(\pc_s_reg[31]_i_3_n_0 ),
        .I2(\pc_reg[31] [15]),
        .I3(\pc_s_reg[21]_i_2__0_n_0 ),
        .O(pc_s_0[15]));
  LUT4 #(
    .INIT(16'hF888)) 
    \pc_s_reg[16]_i_1__0 
       (.I0(pc[16]),
        .I1(\pc_s_reg[31]_i_3_n_0 ),
        .I2(\pc_reg[31] [16]),
        .I3(\pc_s_reg[21]_i_2__0_n_0 ),
        .O(pc_s_0[16]));
  LUT4 #(
    .INIT(16'hF888)) 
    \pc_s_reg[17]_i_1__0 
       (.I0(pc[17]),
        .I1(\pc_s_reg[31]_i_3_n_0 ),
        .I2(\pc_reg[31] [17]),
        .I3(\pc_s_reg[21]_i_2__0_n_0 ),
        .O(pc_s_0[17]));
  LUT4 #(
    .INIT(16'hF888)) 
    \pc_s_reg[18]_i_1__0 
       (.I0(pc[18]),
        .I1(\pc_s_reg[31]_i_3_n_0 ),
        .I2(\pc_reg[31] [18]),
        .I3(\pc_s_reg[21]_i_2__0_n_0 ),
        .O(pc_s_0[18]));
  LUT4 #(
    .INIT(16'hF888)) 
    \pc_s_reg[19]_i_1__0 
       (.I0(pc[19]),
        .I1(\pc_s_reg[31]_i_3_n_0 ),
        .I2(\pc_reg[31] [19]),
        .I3(\pc_s_reg[21]_i_2__0_n_0 ),
        .O(pc_s_0[19]));
  LUT4 #(
    .INIT(16'hF888)) 
    \pc_s_reg[1]_i_1__0 
       (.I0(pc[1]),
        .I1(\pc_s_reg[31]_i_3_n_0 ),
        .I2(\pc_reg[31] [1]),
        .I3(\pc_s_reg[7]_i_3_n_0 ),
        .O(pc_s_0[1]));
  LUT4 #(
    .INIT(16'hF888)) 
    \pc_s_reg[20]_i_1__0 
       (.I0(pc[20]),
        .I1(\pc_s_reg[31]_i_3_n_0 ),
        .I2(\pc_reg[31] [20]),
        .I3(\pc_s_reg[21]_i_2__0_n_0 ),
        .O(pc_s_0[20]));
  LUT4 #(
    .INIT(16'hF888)) 
    \pc_s_reg[21]_i_1__0 
       (.I0(pc[21]),
        .I1(\pc_s_reg[31]_i_3_n_0 ),
        .I2(\pc_reg[31] [21]),
        .I3(\pc_s_reg[21]_i_2__0_n_0 ),
        .O(pc_s[21]));
  LUT6 #(
    .INIT(64'h0000000200000018)) 
    \pc_s_reg[21]_i_2__0 
       (.I0(out_en_reg_0),
        .I1(\pc_s_reg_reg[21]_0 ),
        .I2(out_en_b_i),
        .I3(\pc_s_reg[21]_i_3__0_n_0 ),
        .I4(waypoint_address_cs_reg[0]),
        .I5(waypoint_address_cs_reg[4]),
        .O(\pc_s_reg[21]_i_2__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \pc_s_reg[21]_i_3__0 
       (.I0(waypoint_address_cs_reg[1]),
        .I1(waypoint_address_cs_reg[2]),
        .O(\pc_s_reg[21]_i_3__0_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc_s_reg[22]_i_1__0 
       (.I0(\pc_s_reg[28]_i_3__0_n_0 ),
        .I1(waypoint_address_reg[22]),
        .I2(\pc_s_reg[31]_i_3_n_0 ),
        .I3(pc[22]),
        .I4(\pc_reg[31] [22]),
        .I5(\pc_s_reg_reg[22]_0 ),
        .O(pc_s_0[22]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc_s_reg[23]_i_1__0 
       (.I0(\pc_s_reg[28]_i_3__0_n_0 ),
        .I1(waypoint_address_reg[23]),
        .I2(\pc_s_reg[31]_i_3_n_0 ),
        .I3(pc[23]),
        .I4(\pc_reg[31] [23]),
        .I5(\pc_s_reg_reg[22]_0 ),
        .O(pc_s_0[23]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc_s_reg[24]_i_1__0 
       (.I0(\pc_s_reg[28]_i_3__0_n_0 ),
        .I1(waypoint_address_reg[24]),
        .I2(\pc_s_reg[31]_i_3_n_0 ),
        .I3(pc[24]),
        .I4(\pc_reg[31] [24]),
        .I5(\pc_s_reg_reg[22]_0 ),
        .O(pc_s_0[24]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc_s_reg[25]_i_1__0 
       (.I0(\pc_s_reg[28]_i_3__0_n_0 ),
        .I1(waypoint_address_reg[25]),
        .I2(\pc_s_reg[31]_i_3_n_0 ),
        .I3(pc[25]),
        .I4(\pc_reg[31] [25]),
        .I5(\pc_s_reg_reg[22]_0 ),
        .O(pc_s_0[25]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc_s_reg[26]_i_1__0 
       (.I0(\pc_s_reg[28]_i_3__0_n_0 ),
        .I1(waypoint_address_reg[26]),
        .I2(\pc_s_reg[31]_i_3_n_0 ),
        .I3(pc[26]),
        .I4(\pc_reg[31] [26]),
        .I5(\pc_s_reg_reg[22]_0 ),
        .O(pc_s_0[26]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc_s_reg[27]_i_1__0 
       (.I0(\pc_s_reg[28]_i_3__0_n_0 ),
        .I1(waypoint_address_reg[27]),
        .I2(\pc_s_reg[31]_i_3_n_0 ),
        .I3(pc[27]),
        .I4(\pc_reg[31] [27]),
        .I5(\pc_s_reg_reg[22]_0 ),
        .O(pc_s_0[27]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc_s_reg[28]_i_1__0 
       (.I0(\pc_reg[31] [28]),
        .I1(\pc_s_reg_reg[22]_0 ),
        .I2(\pc_s_reg[31]_i_3_n_0 ),
        .I3(pc[28]),
        .I4(\pc_s_reg[28]_i_3__0_n_0 ),
        .I5(waypoint_address_reg[28]),
        .O(pc_s[28]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'hFFFFBAAA)) 
    \pc_s_reg[28]_i_2__0 
       (.I0(\pc_s_reg[28]_i_4__0_n_0 ),
        .I1(waypoint_address_cs_reg[1]),
        .I2(waypoint_address_cs_reg[2]),
        .I3(\pc_s_reg[28]_i_5_n_0 ),
        .I4(\pc_s_reg[14]_i_3__0_n_0 ),
        .O(\pc_s_reg_reg[22]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h00000220)) 
    \pc_s_reg[28]_i_3__0 
       (.I0(out_en_reg),
        .I1(waypoint_address_cs_reg[2]),
        .I2(waypoint_address_cs_reg[1]),
        .I3(waypoint_address_cs_reg[0]),
        .I4(waypoint_address_cs_reg[4]),
        .O(\pc_s_reg[28]_i_3__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT4 #(
    .INIT(16'h1000)) 
    \pc_s_reg[28]_i_4__0 
       (.I0(out_en_b_i),
        .I1(\pc_s_reg[31]_i_8_n_0 ),
        .I2(\pc_s_reg_reg[21]_0 ),
        .I3(out_en_reg_0),
        .O(\pc_s_reg[28]_i_4__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'h00000010)) 
    \pc_s_reg[28]_i_5 
       (.I0(\pc_s_reg_reg[21]_0 ),
        .I1(out_en_b_i),
        .I2(out_en_reg_0),
        .I3(waypoint_address_cs_reg[4]),
        .I4(waypoint_address_cs_reg[0]),
        .O(\pc_s_reg[28]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc_s_reg[29]_i_1__0 
       (.I0(\pc_s_reg[31]_i_2__0_n_0 ),
        .I1(waypoint_address_reg[29]),
        .I2(\pc_s_reg[31]_i_3_n_0 ),
        .I3(pc[29]),
        .I4(\pc_reg[31] [29]),
        .I5(\pc_s_reg[31]_i_4_n_0 ),
        .O(pc_s_0[29]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc_s_reg[2]_i_1__0 
       (.I0(\pc_s_reg[7]_i_2__0_n_0 ),
        .I1(waypoint_address_reg[2]),
        .I2(\pc_s_reg[7]_i_3_n_0 ),
        .I3(\pc_reg[31] [2]),
        .I4(pc[2]),
        .I5(\pc_s_reg[31]_i_3_n_0 ),
        .O(pc_s_0[2]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc_s_reg[30]_i_1__0 
       (.I0(\pc_s_reg[31]_i_2__0_n_0 ),
        .I1(waypoint_address_reg[30]),
        .I2(\pc_s_reg[31]_i_3_n_0 ),
        .I3(pc[30]),
        .I4(\pc_reg[31] [30]),
        .I5(\pc_s_reg[31]_i_4_n_0 ),
        .O(pc_s_0[30]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \pc_s_reg[31]_i_10 
       (.I0(waypoint_address_cs_reg[0]),
        .I1(waypoint_address_cs_reg[4]),
        .O(\pc_s_reg[31]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc_s_reg[31]_i_1__0 
       (.I0(\pc_s_reg[31]_i_2__0_n_0 ),
        .I1(waypoint_address_reg[31]),
        .I2(\pc_s_reg[31]_i_3_n_0 ),
        .I3(pc[31]),
        .I4(\pc_reg[31] [31]),
        .I5(\pc_s_reg[31]_i_4_n_0 ),
        .O(pc_s_0[31]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \pc_s_reg[31]_i_2__0 
       (.I0(waypoint_address_cs_reg[4]),
        .I1(waypoint_address_cs_reg[0]),
        .I2(\pc_s_reg[31]_i_5_n_0 ),
        .O(\pc_s_reg[31]_i_2__0_n_0 ));
  LUT5 #(
    .INIT(32'hFFFEEEEF)) 
    \pc_s_reg[31]_i_3 
       (.I0(\pc_s_reg[31]_i_6_n_0 ),
        .I1(\pc_s_reg[31]_i_7_n_0 ),
        .I2(\pc_s_reg_reg[21]_0 ),
        .I3(out_en_b_i),
        .I4(\pc_s_reg[31]_i_8_n_0 ),
        .O(\pc_s_reg[31]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \pc_s_reg[31]_i_4 
       (.I0(\pc_s_reg[31]_i_9_n_0 ),
        .I1(\pc_s_reg[14]_i_3__0_n_0 ),
        .O(\pc_s_reg[31]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h00000010)) 
    \pc_s_reg[31]_i_5 
       (.I0(\pc_s_reg_reg[21]_0 ),
        .I1(out_en_b_i),
        .I2(out_en_reg_0),
        .I3(waypoint_address_cs_reg[2]),
        .I4(waypoint_address_cs_reg[1]),
        .O(\pc_s_reg[31]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'hF8F8F888)) 
    \pc_s_reg[31]_i_6 
       (.I0(waypoint_address_cs_reg[2]),
        .I1(waypoint_address_cs_reg[1]),
        .I2(out_en_b_i),
        .I3(out_en_reg_0),
        .I4(\pc_s_reg_reg[21]_0 ),
        .O(\pc_s_reg[31]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFEEEFEEEF000F)) 
    \pc_s_reg[31]_i_7 
       (.I0(waypoint_address_cs_reg[1]),
        .I1(waypoint_address_cs_reg[2]),
        .I2(out_en_reg_0),
        .I3(out_en_b_i),
        .I4(waypoint_address_cs_reg[0]),
        .I5(waypoint_address_cs_reg[4]),
        .O(\pc_s_reg[31]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \pc_s_reg[31]_i_8 
       (.I0(waypoint_address_cs_reg[4]),
        .I1(waypoint_address_cs_reg[0]),
        .I2(waypoint_address_cs_reg[2]),
        .I3(waypoint_address_cs_reg[1]),
        .O(\pc_s_reg[31]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000228)) 
    \pc_s_reg[31]_i_9 
       (.I0(out_en_reg_0),
        .I1(\pc_s_reg_reg[21]_0 ),
        .I2(waypoint_address_cs_reg[1]),
        .I3(waypoint_address_cs_reg[2]),
        .I4(\pc_s_reg[31]_i_10_n_0 ),
        .I5(out_en_b_i),
        .O(\pc_s_reg[31]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc_s_reg[3]_i_1__0 
       (.I0(\pc_s_reg[7]_i_2__0_n_0 ),
        .I1(waypoint_address_reg[3]),
        .I2(\pc_s_reg[7]_i_3_n_0 ),
        .I3(\pc_reg[31] [3]),
        .I4(pc[3]),
        .I5(\pc_s_reg[31]_i_3_n_0 ),
        .O(pc_s_0[3]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc_s_reg[4]_i_1__0 
       (.I0(\pc_s_reg[7]_i_2__0_n_0 ),
        .I1(waypoint_address_reg[4]),
        .I2(\pc_s_reg[7]_i_3_n_0 ),
        .I3(\pc_reg[31] [4]),
        .I4(pc[4]),
        .I5(\pc_s_reg[31]_i_3_n_0 ),
        .O(pc_s_0[4]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc_s_reg[5]_i_1__0 
       (.I0(\pc_s_reg[7]_i_2__0_n_0 ),
        .I1(waypoint_address_reg[5]),
        .I2(\pc_s_reg[7]_i_3_n_0 ),
        .I3(\pc_reg[31] [5]),
        .I4(pc[5]),
        .I5(\pc_s_reg[31]_i_3_n_0 ),
        .O(pc_s_0[5]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc_s_reg[6]_i_1__0 
       (.I0(\pc_s_reg[7]_i_2__0_n_0 ),
        .I1(waypoint_address_reg[6]),
        .I2(\pc_s_reg[7]_i_3_n_0 ),
        .I3(\pc_reg[31] [6]),
        .I4(pc[6]),
        .I5(\pc_s_reg[31]_i_3_n_0 ),
        .O(pc_s_0[6]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc_s_reg[7]_i_1__0 
       (.I0(waypoint_address_reg[7]),
        .I1(\pc_s_reg[7]_i_2__0_n_0 ),
        .I2(\pc_s_reg[7]_i_3_n_0 ),
        .I3(\pc_reg[31] [7]),
        .I4(\pc_s_reg[31]_i_3_n_0 ),
        .I5(pc[7]),
        .O(pc_s[7]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'hFF28)) 
    \pc_s_reg[7]_i_2__0 
       (.I0(\pc_s_reg[31]_i_5_n_0 ),
        .I1(waypoint_address_cs_reg[0]),
        .I2(waypoint_address_cs_reg[4]),
        .I3(\pc_s_reg[31]_i_9_n_0 ),
        .O(\pc_s_reg[7]_i_2__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT4 #(
    .INIT(16'h0010)) 
    \pc_s_reg[7]_i_3 
       (.I0(out_en_reg_0),
        .I1(\pc_s_reg_reg[21]_0 ),
        .I2(out_en_b_i),
        .I3(\pc_s_reg[31]_i_8_n_0 ),
        .O(\pc_s_reg[7]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc_s_reg[8]_i_1__0 
       (.I0(\pc_s_reg[14]_i_2__0_n_0 ),
        .I1(waypoint_address_reg[8]),
        .I2(\pc_s_reg[14]_i_3__0_n_0 ),
        .I3(\pc_reg[31] [8]),
        .I4(pc[8]),
        .I5(\pc_s_reg[31]_i_3_n_0 ),
        .O(pc_s_0[8]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc_s_reg[9]_i_1__0 
       (.I0(\pc_s_reg[14]_i_2__0_n_0 ),
        .I1(waypoint_address_reg[9]),
        .I2(\pc_s_reg[14]_i_3__0_n_0 ),
        .I3(\pc_reg[31] [9]),
        .I4(pc[9]),
        .I5(\pc_s_reg[31]_i_3_n_0 ),
        .O(pc_s_0[9]));
  FDRE \pc_s_reg_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s_0[0]),
        .Q(pc[0]),
        .R(reset));
  FDRE \pc_s_reg_reg[10] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s_0[10]),
        .Q(pc[10]),
        .R(reset));
  FDRE \pc_s_reg_reg[11] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s_0[11]),
        .Q(pc[11]),
        .R(reset));
  FDRE \pc_s_reg_reg[12] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s_0[12]),
        .Q(pc[12]),
        .R(reset));
  FDRE \pc_s_reg_reg[13] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s_0[13]),
        .Q(pc[13]),
        .R(reset));
  FDRE \pc_s_reg_reg[14] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s[14]),
        .Q(pc[14]),
        .R(reset));
  FDRE \pc_s_reg_reg[15] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s_0[15]),
        .Q(pc[15]),
        .R(reset));
  FDRE \pc_s_reg_reg[16] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s_0[16]),
        .Q(pc[16]),
        .R(reset));
  FDRE \pc_s_reg_reg[17] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s_0[17]),
        .Q(pc[17]),
        .R(reset));
  FDRE \pc_s_reg_reg[18] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s_0[18]),
        .Q(pc[18]),
        .R(reset));
  FDRE \pc_s_reg_reg[19] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s_0[19]),
        .Q(pc[19]),
        .R(reset));
  FDRE \pc_s_reg_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s_0[1]),
        .Q(pc[1]),
        .R(reset));
  FDRE \pc_s_reg_reg[20] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s_0[20]),
        .Q(pc[20]),
        .R(reset));
  FDRE \pc_s_reg_reg[21] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s[21]),
        .Q(pc[21]),
        .R(reset));
  FDRE \pc_s_reg_reg[22] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s_0[22]),
        .Q(pc[22]),
        .R(reset));
  FDRE \pc_s_reg_reg[23] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s_0[23]),
        .Q(pc[23]),
        .R(reset));
  FDRE \pc_s_reg_reg[24] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s_0[24]),
        .Q(pc[24]),
        .R(reset));
  FDRE \pc_s_reg_reg[25] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s_0[25]),
        .Q(pc[25]),
        .R(reset));
  FDRE \pc_s_reg_reg[26] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s_0[26]),
        .Q(pc[26]),
        .R(reset));
  FDRE \pc_s_reg_reg[27] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s_0[27]),
        .Q(pc[27]),
        .R(reset));
  FDRE \pc_s_reg_reg[28] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s[28]),
        .Q(pc[28]),
        .R(reset));
  FDRE \pc_s_reg_reg[29] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s_0[29]),
        .Q(pc[29]),
        .R(reset));
  FDRE \pc_s_reg_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s_0[2]),
        .Q(pc[2]),
        .R(reset));
  FDRE \pc_s_reg_reg[30] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s_0[30]),
        .Q(pc[30]),
        .R(reset));
  FDRE \pc_s_reg_reg[31] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s_0[31]),
        .Q(pc[31]),
        .R(reset));
  FDRE \pc_s_reg_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s_0[3]),
        .Q(pc[3]),
        .R(reset));
  FDRE \pc_s_reg_reg[4] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s_0[4]),
        .Q(pc[4]),
        .R(reset));
  FDRE \pc_s_reg_reg[5] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s_0[5]),
        .Q(pc[5]),
        .R(reset));
  FDRE \pc_s_reg_reg[6] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s_0[6]),
        .Q(pc[6]),
        .R(reset));
  FDRE \pc_s_reg_reg[7] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s[7]),
        .Q(pc[7]),
        .R(reset));
  FDRE \pc_s_reg_reg[8] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s_0[8]),
        .Q(pc[8]),
        .R(reset));
  FDRE \pc_s_reg_reg[9] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s_0[9]),
        .Q(pc[9]),
        .R(reset));
  FDRE \waypoint_address_cs_reg_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(en_0_reg_reg[0]),
        .Q(waypoint_address_cs_reg[0]),
        .R(reset));
  FDRE \waypoint_address_cs_reg_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(en_0_reg_reg[1]),
        .Q(waypoint_address_cs_reg[1]),
        .R(reset));
  FDRE \waypoint_address_cs_reg_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(en_0_reg_reg[2]),
        .Q(waypoint_address_cs_reg[2]),
        .R(reset));
  FDRE \waypoint_address_cs_reg_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(en_0_reg_reg[3]),
        .Q(\pc_s_reg_reg[21]_0 ),
        .R(reset));
  FDRE \waypoint_address_cs_reg_reg[4] 
       (.C(clk),
        .CE(1'b1),
        .D(en_0_reg_reg[4]),
        .Q(waypoint_address_cs_reg[4]),
        .R(reset));
  FDRE \waypoint_address_reg_reg[10] 
       (.C(clk),
        .CE(1'b1),
        .D(waypoint_address[8]),
        .Q(waypoint_address_reg[10]),
        .R(reset));
  FDRE \waypoint_address_reg_reg[11] 
       (.C(clk),
        .CE(1'b1),
        .D(waypoint_address[9]),
        .Q(waypoint_address_reg[11]),
        .R(reset));
  FDRE \waypoint_address_reg_reg[12] 
       (.C(clk),
        .CE(1'b1),
        .D(waypoint_address[10]),
        .Q(waypoint_address_reg[12]),
        .R(reset));
  FDRE \waypoint_address_reg_reg[13] 
       (.C(clk),
        .CE(1'b1),
        .D(waypoint_address[11]),
        .Q(waypoint_address_reg[13]),
        .R(reset));
  FDRE \waypoint_address_reg_reg[14] 
       (.C(clk),
        .CE(1'b1),
        .D(waypoint_address[12]),
        .Q(waypoint_address_reg[14]),
        .R(reset));
  FDRE \waypoint_address_reg_reg[22] 
       (.C(clk),
        .CE(1'b1),
        .D(waypoint_address[13]),
        .Q(waypoint_address_reg[22]),
        .R(reset));
  FDRE \waypoint_address_reg_reg[23] 
       (.C(clk),
        .CE(1'b1),
        .D(waypoint_address[14]),
        .Q(waypoint_address_reg[23]),
        .R(reset));
  FDRE \waypoint_address_reg_reg[24] 
       (.C(clk),
        .CE(1'b1),
        .D(waypoint_address[15]),
        .Q(waypoint_address_reg[24]),
        .R(reset));
  FDRE \waypoint_address_reg_reg[25] 
       (.C(clk),
        .CE(1'b1),
        .D(waypoint_address[16]),
        .Q(waypoint_address_reg[25]),
        .R(reset));
  FDRE \waypoint_address_reg_reg[26] 
       (.C(clk),
        .CE(1'b1),
        .D(waypoint_address[17]),
        .Q(waypoint_address_reg[26]),
        .R(reset));
  FDRE \waypoint_address_reg_reg[27] 
       (.C(clk),
        .CE(1'b1),
        .D(waypoint_address[18]),
        .Q(waypoint_address_reg[27]),
        .R(reset));
  FDRE \waypoint_address_reg_reg[28] 
       (.C(clk),
        .CE(1'b1),
        .D(waypoint_address[19]),
        .Q(waypoint_address_reg[28]),
        .R(reset));
  FDRE \waypoint_address_reg_reg[29] 
       (.C(clk),
        .CE(1'b1),
        .D(waypoint_address[20]),
        .Q(waypoint_address_reg[29]),
        .R(reset));
  FDRE \waypoint_address_reg_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(waypoint_address[0]),
        .Q(waypoint_address_reg[2]),
        .R(reset));
  FDRE \waypoint_address_reg_reg[30] 
       (.C(clk),
        .CE(1'b1),
        .D(waypoint_address[21]),
        .Q(waypoint_address_reg[30]),
        .R(reset));
  FDRE \waypoint_address_reg_reg[31] 
       (.C(clk),
        .CE(1'b1),
        .D(waypoint_address[22]),
        .Q(waypoint_address_reg[31]),
        .R(reset));
  FDRE \waypoint_address_reg_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(waypoint_address[1]),
        .Q(waypoint_address_reg[3]),
        .R(reset));
  FDRE \waypoint_address_reg_reg[4] 
       (.C(clk),
        .CE(1'b1),
        .D(waypoint_address[2]),
        .Q(waypoint_address_reg[4]),
        .R(reset));
  FDRE \waypoint_address_reg_reg[5] 
       (.C(clk),
        .CE(1'b1),
        .D(waypoint_address[3]),
        .Q(waypoint_address_reg[5]),
        .R(reset));
  FDRE \waypoint_address_reg_reg[6] 
       (.C(clk),
        .CE(1'b1),
        .D(waypoint_address[4]),
        .Q(waypoint_address_reg[6]),
        .R(reset));
  FDRE \waypoint_address_reg_reg[7] 
       (.C(clk),
        .CE(1'b1),
        .D(waypoint_address[5]),
        .Q(waypoint_address_reg[7]),
        .R(reset));
  FDRE \waypoint_address_reg_reg[8] 
       (.C(clk),
        .CE(1'b1),
        .D(waypoint_address[6]),
        .Q(waypoint_address_reg[8]),
        .R(reset));
  FDRE \waypoint_address_reg_reg[9] 
       (.C(clk),
        .CE(1'b1),
        .D(waypoint_address[7]),
        .Q(waypoint_address_reg[9]),
        .R(reset));
endmodule

(* ORIG_REF_NAME = "datapath" *) 
module design_1_decodeur_traces_1_1_datapath
   (out,
    \FSM_onehot_state_reg_reg[7] ,
    \output_data_0_reg[0] ,
    waypoint_address_en,
    \output_data_7_reg[0] ,
    \FSM_onehot_state_reg_reg[0] ,
    \pc_reg[0] ,
    output_data_0_s_reg_reg,
    \state_reg_reg[0] ,
    \state_reg_reg[0]_0 ,
    D,
    \FSM_onehot_state_reg_reg[1] ,
    \FSM_onehot_state_reg_reg[1]_0 ,
    \state_reg_reg[0]_1 ,
    out_en_reg,
    \state_reg_reg[0]_2 ,
    pc,
    out_en_s,
    \FSM_onehot_state_reg_reg[0]_0 ,
    \FSM_onehot_state_reg_reg[1]_1 ,
    \state_reg_reg[1] ,
    \FSM_onehot_state_reg_reg[0]_1 ,
    \state_reg_reg[0]_3 ,
    \state_reg_reg[1]_0 ,
    waypoint_address,
    \context_id_reg[31] ,
    reset,
    clk,
    E,
    enable_reg_reg_reg,
    \FSM_onehot_state_reg_reg[0]_2 ,
    \state_reg_reg[1]_1 ,
    \state_reg_reg[1]_2 ,
    \state_reg_reg[1]_3 ,
    \state_reg_reg[1]_4 ,
    \FSM_onehot_state_reg_reg[7]_0 ,
    \state_reg_reg[1]_5 ,
    \state_reg_reg[0]_4 ,
    \state_reg_reg[2] ,
    \state_reg_reg[3] ,
    \state_reg_reg[1]_6 ,
    \state_reg_reg[2]_0 ,
    enable_reg_reg_reg_0,
    \state_reg_reg[0]_5 ,
    \state_reg_reg[2]_1 ,
    Q,
    \state_reg_reg[2]_2 ,
    \FSM_onehot_state_reg_reg[1]_2 );
  output [0:0]out;
  output [1:0]\FSM_onehot_state_reg_reg[7] ;
  output [1:0]\output_data_0_reg[0] ;
  output waypoint_address_en;
  output [0:0]\output_data_7_reg[0] ;
  output \FSM_onehot_state_reg_reg[0] ;
  output \pc_reg[0] ;
  output output_data_0_s_reg_reg;
  output \state_reg_reg[0] ;
  output \state_reg_reg[0]_0 ;
  output [0:0]D;
  output \FSM_onehot_state_reg_reg[1] ;
  output \FSM_onehot_state_reg_reg[1]_0 ;
  output \state_reg_reg[0]_1 ;
  output out_en_reg;
  output \state_reg_reg[0]_2 ;
  output [31:0]pc;
  output out_en_s;
  output \FSM_onehot_state_reg_reg[0]_0 ;
  output \FSM_onehot_state_reg_reg[1]_1 ;
  output \state_reg_reg[1] ;
  output \FSM_onehot_state_reg_reg[0]_1 ;
  output \state_reg_reg[0]_3 ;
  output \state_reg_reg[1]_0 ;
  output [22:0]waypoint_address;
  output [31:0]\context_id_reg[31] ;
  input reset;
  input clk;
  input [0:0]E;
  input enable_reg_reg_reg;
  input \FSM_onehot_state_reg_reg[0]_2 ;
  input \state_reg_reg[1]_1 ;
  input \state_reg_reg[1]_2 ;
  input \state_reg_reg[1]_3 ;
  input \state_reg_reg[1]_4 ;
  input \FSM_onehot_state_reg_reg[7]_0 ;
  input \state_reg_reg[1]_5 ;
  input \state_reg_reg[0]_4 ;
  input \state_reg_reg[2] ;
  input \state_reg_reg[3] ;
  input \state_reg_reg[1]_6 ;
  input \state_reg_reg[2]_0 ;
  input enable_reg_reg_reg_0;
  input \state_reg_reg[0]_5 ;
  input \state_reg_reg[2]_1 ;
  input [7:0]Q;
  input [0:0]\state_reg_reg[2]_2 ;
  input [1:0]\FSM_onehot_state_reg_reg[1]_2 ;

  wire [0:0]D;
  wire [0:0]E;
  wire \FSM_onehot_state_reg_reg[0] ;
  wire \FSM_onehot_state_reg_reg[0]_0 ;
  wire \FSM_onehot_state_reg_reg[0]_1 ;
  wire \FSM_onehot_state_reg_reg[0]_2 ;
  wire \FSM_onehot_state_reg_reg[1] ;
  wire \FSM_onehot_state_reg_reg[1]_0 ;
  wire \FSM_onehot_state_reg_reg[1]_1 ;
  wire [1:0]\FSM_onehot_state_reg_reg[1]_2 ;
  wire [1:0]\FSM_onehot_state_reg_reg[7] ;
  wire \FSM_onehot_state_reg_reg[7]_0 ;
  wire [7:0]Q;
  wire clk;
  wire [31:0]\context_id_reg[31] ;
  wire en_0_reg;
  wire en_1_reg;
  wire en_2_reg;
  wire en_3_reg;
  wire en_4_reg;
  wire en_s_instruction;
  wire enable_reg_reg_reg;
  wire enable_reg_reg_reg_0;
  wire [31:0]instruction_address;
  wire [0:0]out;
  wire out_en_b_i;
  wire out_en_reg;
  wire out_en_s;
  wire [1:0]\output_data_0_reg[0] ;
  wire output_data_0_s_reg_reg;
  wire [0:0]\output_data_7_reg[0] ;
  wire p_0_in;
  wire p_1_in;
  wire [31:0]pc;
  wire \pc_reg[0] ;
  wire [28:21]pc_s__0;
  wire reset;
  wire \state_reg_reg[0] ;
  wire \state_reg_reg[0]_0 ;
  wire \state_reg_reg[0]_1 ;
  wire \state_reg_reg[0]_2 ;
  wire \state_reg_reg[0]_3 ;
  wire \state_reg_reg[0]_4 ;
  wire \state_reg_reg[0]_5 ;
  wire \state_reg_reg[1] ;
  wire \state_reg_reg[1]_0 ;
  wire \state_reg_reg[1]_1 ;
  wire \state_reg_reg[1]_2 ;
  wire \state_reg_reg[1]_3 ;
  wire \state_reg_reg[1]_4 ;
  wire \state_reg_reg[1]_5 ;
  wire \state_reg_reg[1]_6 ;
  wire \state_reg_reg[2] ;
  wire \state_reg_reg[2]_0 ;
  wire \state_reg_reg[2]_1 ;
  wire [0:0]\state_reg_reg[2]_2 ;
  wire \state_reg_reg[3] ;
  wire u_address_compue_n_0;
  wire u_address_compue_n_1;
  wire u_address_compue_n_34;
  wire u_address_compue_n_35;
  wire u_decode_bap_n_1;
  wire u_decode_bap_n_14;
  wire u_decode_bap_n_15;
  wire u_decode_bap_n_16;
  wire u_decode_bap_n_17;
  wire u_decode_bap_n_18;
  wire u_decode_bap_n_19;
  wire u_decode_bap_n_2;
  wire u_decode_bap_n_20;
  wire u_decode_bap_n_21;
  wire u_decode_bap_n_22;
  wire u_decode_bap_n_23;
  wire u_decode_bap_n_24;
  wire u_decode_bap_n_25;
  wire u_decode_bap_n_26;
  wire u_decode_bap_n_27;
  wire u_decode_bap_n_28;
  wire u_decode_bap_n_29;
  wire u_decode_bap_n_3;
  wire u_decode_bap_n_30;
  wire u_decode_bap_n_31;
  wire u_decode_bap_n_32;
  wire u_decode_bap_n_33;
  wire u_decode_bap_n_34;
  wire u_decode_bap_n_35;
  wire u_decode_bap_n_36;
  wire u_decode_bap_n_37;
  wire u_decode_bap_n_38;
  wire u_decode_bap_n_39;
  wire u_decode_bap_n_40;
  wire u_decode_bap_n_41;
  wire u_decode_bap_n_42;
  wire u_decode_bap_n_43;
  wire u_decode_bap_n_44;
  wire u_decode_bap_n_45;
  wire u_decode_bap_n_46;
  wire u_decode_bap_n_7;
  wire u_decode_i_sync_n_10;
  wire u_decode_i_sync_n_11;
  wire u_decode_i_sync_n_12;
  wire u_decode_i_sync_n_13;
  wire u_decode_i_sync_n_14;
  wire u_decode_i_sync_n_27;
  wire u_decode_i_sync_n_28;
  wire u_decode_i_sync_n_29;
  wire u_decode_i_sync_n_3;
  wire u_decode_i_sync_n_4;
  wire u_decode_i_sync_n_5;
  wire u_decode_i_sync_n_6;
  wire u_decode_i_sync_n_7;
  wire u_decode_i_sync_n_8;
  wire u_decode_i_sync_n_9;
  wire u_decode_waypoint_n_0;
  wire u_decode_waypoint_n_9;
  wire [22:0]waypoint_address;
  wire [3:3]waypoint_address_cs_reg;
  wire waypoint_address_en;

  design_1_decodeur_traces_1_1_address_compute u_address_compue
       (.D(u_address_compue_n_0),
        .Q({p_0_in,p_1_in}),
        .clk(clk),
        .en_0_reg_reg({en_0_reg,en_1_reg,en_2_reg,en_3_reg,en_4_reg}),
        .enable_reg_reg_reg(enable_reg_reg_reg),
        .out_en_b_i(out_en_b_i),
        .out_en_reg(u_decode_waypoint_n_9),
        .out_en_reg_0(waypoint_address_en),
        .\output_data_0_reg[5] (u_address_compue_n_1),
        .\output_data_3_reg[6] (u_address_compue_n_35),
        .pc(pc),
        .\pc_reg[31] ({u_decode_bap_n_15,u_decode_bap_n_16,u_decode_bap_n_17,u_decode_bap_n_18,u_decode_bap_n_19,u_decode_bap_n_20,u_decode_bap_n_21,u_decode_bap_n_22,u_decode_bap_n_23,u_decode_bap_n_24,u_decode_bap_n_25,u_decode_bap_n_26,u_decode_bap_n_27,u_decode_bap_n_28,u_decode_bap_n_29,u_decode_bap_n_30,u_decode_bap_n_31,u_decode_bap_n_32,u_decode_bap_n_33,u_decode_bap_n_34,u_decode_bap_n_35,u_decode_bap_n_36,u_decode_bap_n_37,u_decode_bap_n_38,u_decode_bap_n_39,u_decode_bap_n_40,u_decode_bap_n_41,u_decode_bap_n_42,u_decode_bap_n_43,u_decode_bap_n_44,u_decode_bap_n_45,u_decode_bap_n_46}),
        .\pc_s_reg_reg[21]_0 (waypoint_address_cs_reg),
        .\pc_s_reg_reg[22]_0 (u_address_compue_n_34),
        .reset(reset),
        .waypoint_address(waypoint_address));
  design_1_decodeur_traces_1_1_decode_bap u_decode_bap
       (.D({pc_s__0[28],pc_s__0[21]}),
        .E(en_s_instruction),
        .\FSM_onehot_state_reg_reg[0]_0 (\FSM_onehot_state_reg_reg[0] ),
        .\FSM_onehot_state_reg_reg[1]_0 (u_decode_bap_n_7),
        .\FSM_onehot_state_reg_reg[2]_0 (u_decode_bap_n_14),
        .\FSM_onehot_state_reg_reg[7]_0 (\FSM_onehot_state_reg_reg[7]_0 ),
        .Q({p_0_in,p_1_in,u_decode_i_sync_n_3,u_decode_i_sync_n_4,u_decode_i_sync_n_5,u_decode_i_sync_n_6,u_decode_i_sync_n_7,u_decode_i_sync_n_8}),
        .clk(clk),
        .\data_reg_s_reg[6] (u_decode_i_sync_n_27),
        .\data_reg_s_reg[6]_0 (u_decode_i_sync_n_28),
        .\data_reg_s_reg[6]_1 (u_decode_i_sync_n_29),
        .\data_reg_s_reg[7] (u_decode_i_sync_n_13),
        .\data_reg_s_reg[7]_0 ({u_decode_i_sync_n_9,u_decode_i_sync_n_10,u_decode_i_sync_n_11}),
        .enable_reg_reg_reg(E),
        .enable_reg_reg_reg_0(enable_reg_reg_reg),
        .out({\FSM_onehot_state_reg_reg[7] [1],u_decode_bap_n_1,u_decode_bap_n_2,u_decode_bap_n_3,\FSM_onehot_state_reg_reg[7] [0]}),
        .out_en_b_i(out_en_b_i),
        .out_en_reg(waypoint_address_en),
        .out_en_s(out_en_s),
        .output_data_0_s_reg_reg_0(output_data_0_s_reg_reg),
        .\output_data_3_reg[7] (instruction_address),
        .\pc_reg[0]_0 (\pc_reg[0] ),
        .\pc_s_reg_reg[31]_0 ({u_decode_bap_n_15,u_decode_bap_n_16,u_decode_bap_n_17,u_decode_bap_n_18,u_decode_bap_n_19,u_decode_bap_n_20,u_decode_bap_n_21,u_decode_bap_n_22,u_decode_bap_n_23,u_decode_bap_n_24,u_decode_bap_n_25,u_decode_bap_n_26,u_decode_bap_n_27,u_decode_bap_n_28,u_decode_bap_n_29,u_decode_bap_n_30,u_decode_bap_n_31,u_decode_bap_n_32,u_decode_bap_n_33,u_decode_bap_n_34,u_decode_bap_n_35,u_decode_bap_n_36,u_decode_bap_n_37,u_decode_bap_n_38,u_decode_bap_n_39,u_decode_bap_n_40,u_decode_bap_n_41,u_decode_bap_n_42,u_decode_bap_n_43,u_decode_bap_n_44,u_decode_bap_n_45,u_decode_bap_n_46}),
        .reset(reset),
        .\state_reg_reg[0] (\state_reg_reg[0]_0 ),
        .\state_reg_reg[1] (\state_reg_reg[1]_3 ),
        .\state_reg_reg[1]_0 (\state_reg_reg[1]_4 ),
        .\state_reg_reg[2] (\state_reg_reg[2]_1 ));
  design_1_decodeur_traces_1_1_decode_i_sync u_decode_i_sync
       (.D(u_decode_i_sync_n_12),
        .E(\output_data_7_reg[0] ),
        .\FSM_onehot_state_reg_reg[0]_0 (\FSM_onehot_state_reg_reg[0]_1 ),
        .\FSM_onehot_state_reg_reg[0]_1 (\FSM_onehot_state_reg_reg[0]_2 ),
        .\FSM_onehot_state_reg_reg[13] (\FSM_onehot_state_reg_reg[0] ),
        .\FSM_onehot_state_reg_reg[1]_0 (\FSM_onehot_state_reg_reg[1] ),
        .\FSM_onehot_state_reg_reg[1]_1 (\FSM_onehot_state_reg_reg[1]_0 ),
        .\FSM_onehot_state_reg_reg[1]_2 (\pc_reg[0] ),
        .\FSM_onehot_state_reg_reg[2]_0 (u_decode_bap_n_14),
        .\FSM_onehot_state_reg_reg[3]_0 ({u_decode_waypoint_n_0,\output_data_0_reg[0] [0]}),
        .\FSM_onehot_state_reg_reg[6]_0 (u_decode_i_sync_n_27),
        .\FSM_onehot_state_reg_reg[6]_1 ({u_decode_bap_n_1,u_decode_bap_n_2,u_decode_bap_n_3,\FSM_onehot_state_reg_reg[7] [0]}),
        .\FSM_onehot_state_reg_reg[7]_0 (\state_reg_reg[0]_0 ),
        .\FSM_onehot_state_reg_reg[9] ({u_decode_i_sync_n_9,u_decode_i_sync_n_10,u_decode_i_sync_n_11}),
        .\FSM_onehot_state_reg_reg[9]_0 (out_en_reg),
        .Q({p_0_in,p_1_in,u_decode_i_sync_n_3,u_decode_i_sync_n_4,u_decode_i_sync_n_5,u_decode_i_sync_n_6,u_decode_i_sync_n_7,u_decode_i_sync_n_8}),
        .clk(clk),
        .\context_id_reg[31] (\context_id_reg[31] ),
        .\data_in_reg_reg[7] (Q),
        .enable_reg_reg_reg(enable_reg_reg_reg),
        .enable_reg_reg_reg_0(u_decode_bap_n_7),
        .enable_reg_reg_reg_1(enable_reg_reg_reg_0),
        .\instruction_address_reg_reg[31] (instruction_address),
        .out(out),
        .\output_data_1_reg[6]_0 (u_decode_i_sync_n_13),
        .\output_data_2_reg[6]_0 (u_decode_i_sync_n_28),
        .\output_data_3_reg[6]_0 (u_decode_i_sync_n_14),
        .\output_data_3_reg[6]_1 (u_decode_i_sync_n_29),
        .\output_data_3_reg[7]_0 (en_s_instruction),
        .\pc_reg[28] (u_decode_bap_n_18),
        .\pc_s_reg_reg[28] ({pc_s__0[28],pc_s__0[21]}),
        .reset(reset),
        .\state_reg_reg[0] (\state_reg_reg[0] ),
        .\state_reg_reg[0]_0 (\state_reg_reg[0]_1 ),
        .\state_reg_reg[0]_1 (\state_reg_reg[0]_2 ),
        .\state_reg_reg[0]_2 (\state_reg_reg[0]_3 ),
        .\state_reg_reg[0]_3 (\state_reg_reg[0]_4 ),
        .\state_reg_reg[0]_4 (\state_reg_reg[0]_5 ),
        .\state_reg_reg[1] (\state_reg_reg[1] ),
        .\state_reg_reg[1]_0 (\state_reg_reg[1]_0 ),
        .\state_reg_reg[1]_1 (\state_reg_reg[1]_1 ),
        .\state_reg_reg[1]_2 (\state_reg_reg[1]_2 ),
        .\state_reg_reg[1]_3 (\state_reg_reg[1]_5 ),
        .\state_reg_reg[1]_4 (\state_reg_reg[1]_6 ),
        .\state_reg_reg[1]_5 (\state_reg_reg[1]_4 ),
        .\state_reg_reg[2] (D),
        .\state_reg_reg[2]_0 (\state_reg_reg[2] ),
        .\state_reg_reg[2]_1 (\state_reg_reg[2]_0 ),
        .\state_reg_reg[2]_2 (\state_reg_reg[2]_2 ),
        .\state_reg_reg[3] (\state_reg_reg[3] ),
        .\waypoint_address_cs_reg_reg[1] (u_address_compue_n_34),
        .\waypoint_address_reg_reg[28] (u_address_compue_n_35));
  design_1_decodeur_traces_1_1_decode_waypoint u_decode_waypoint
       (.D({u_decode_i_sync_n_12,\FSM_onehot_state_reg_reg[1]_2 }),
        .\FSM_onehot_state_reg_reg[0]_0 (\FSM_onehot_state_reg_reg[0]_0 ),
        .\FSM_onehot_state_reg_reg[1]_0 (\FSM_onehot_state_reg_reg[1]_1 ),
        .Q({p_0_in,p_1_in,u_decode_i_sync_n_3,u_decode_i_sync_n_4,u_decode_i_sync_n_5,u_decode_i_sync_n_6,u_decode_i_sync_n_7,u_decode_i_sync_n_8}),
        .clk(clk),
        .\data_in_reg_reg[7] (Q),
        .\data_reg_s_reg[2] (\state_reg_reg[0]_1 ),
        .enable_reg_reg_reg(enable_reg_reg_reg),
        .enable_reg_reg_reg_0(u_address_compue_n_1),
        .enable_reg_reg_reg_1(u_address_compue_n_0),
        .enable_reg_reg_reg_2(u_decode_i_sync_n_14),
        .out({u_decode_waypoint_n_0,\output_data_0_reg[0] }),
        .out_en_b_i(out_en_b_i),
        .out_en_reg_0(out_en_reg),
        .\pc_s_reg_reg[28] (u_decode_waypoint_n_9),
        .reset(reset),
        .waypoint_address(waypoint_address),
        .\waypoint_address_cs_reg_reg[3] (waypoint_address_cs_reg),
        .\waypoint_address_cs_reg_reg[4] ({en_0_reg,en_1_reg,en_2_reg,en_3_reg,en_4_reg}),
        .waypoint_address_en(waypoint_address_en));
endmodule

(* ORIG_REF_NAME = "decode_bap" *) 
module design_1_decodeur_traces_1_1_decode_bap
   (out,
    out_en_b_i,
    \pc_reg[0]_0 ,
    \FSM_onehot_state_reg_reg[1]_0 ,
    output_data_0_s_reg_reg_0,
    \state_reg_reg[0] ,
    \FSM_onehot_state_reg_reg[0]_0 ,
    D,
    out_en_s,
    \FSM_onehot_state_reg_reg[2]_0 ,
    \pc_s_reg_reg[31]_0 ,
    reset,
    E,
    clk,
    enable_reg_reg_reg,
    enable_reg_reg_reg_0,
    Q,
    \state_reg_reg[1] ,
    \state_reg_reg[1]_0 ,
    \FSM_onehot_state_reg_reg[7]_0 ,
    \state_reg_reg[2] ,
    \data_reg_s_reg[7] ,
    out_en_reg,
    \data_reg_s_reg[7]_0 ,
    \data_reg_s_reg[6] ,
    \output_data_3_reg[7] ,
    \data_reg_s_reg[6]_0 ,
    \data_reg_s_reg[6]_1 );
  output [4:0]out;
  output out_en_b_i;
  output \pc_reg[0]_0 ;
  output \FSM_onehot_state_reg_reg[1]_0 ;
  output output_data_0_s_reg_reg_0;
  output \state_reg_reg[0] ;
  output \FSM_onehot_state_reg_reg[0]_0 ;
  output [1:0]D;
  output out_en_s;
  output \FSM_onehot_state_reg_reg[2]_0 ;
  output [31:0]\pc_s_reg_reg[31]_0 ;
  input reset;
  input [0:0]E;
  input clk;
  input [0:0]enable_reg_reg_reg;
  input enable_reg_reg_reg_0;
  input [7:0]Q;
  input \state_reg_reg[1] ;
  input \state_reg_reg[1]_0 ;
  input \FSM_onehot_state_reg_reg[7]_0 ;
  input \state_reg_reg[2] ;
  input \data_reg_s_reg[7] ;
  input out_en_reg;
  input [2:0]\data_reg_s_reg[7]_0 ;
  input \data_reg_s_reg[6] ;
  input [31:0]\output_data_3_reg[7] ;
  input [0:0]\data_reg_s_reg[6]_0 ;
  input [0:0]\data_reg_s_reg[6]_1 ;

  wire [1:0]D;
  wire [0:0]E;
  wire \FSM_onehot_state_reg[0]_i_1__0_n_0 ;
  wire \FSM_onehot_state_reg[0]_i_2__0_n_0 ;
  wire \FSM_onehot_state_reg[0]_i_6__0_n_0 ;
  wire \FSM_onehot_state_reg[10]_i_1_n_0 ;
  wire \FSM_onehot_state_reg[11]_i_1__0_n_0 ;
  wire \FSM_onehot_state_reg[12]_i_1__0_n_0 ;
  wire \FSM_onehot_state_reg[13]_i_1_n_0 ;
  wire \FSM_onehot_state_reg[3]_i_1__0_n_0 ;
  wire \FSM_onehot_state_reg[4]_i_1__0_n_0 ;
  wire \FSM_onehot_state_reg[5]_i_1__0_n_0 ;
  wire \FSM_onehot_state_reg[7]_i_1__0_n_0 ;
  wire \FSM_onehot_state_reg[8]_i_1__1_n_0 ;
  wire \FSM_onehot_state_reg_reg[0]_0 ;
  wire \FSM_onehot_state_reg_reg[1]_0 ;
  wire \FSM_onehot_state_reg_reg[2]_0 ;
  wire \FSM_onehot_state_reg_reg[7]_0 ;
  (* RTL_KEEP = "yes" *) wire \FSM_onehot_state_reg_reg_n_0_[10] ;
  (* RTL_KEEP = "yes" *) wire \FSM_onehot_state_reg_reg_n_0_[11] ;
  (* RTL_KEEP = "yes" *) wire \FSM_onehot_state_reg_reg_n_0_[12] ;
  (* RTL_KEEP = "yes" *) wire \FSM_onehot_state_reg_reg_n_0_[13] ;
  (* RTL_KEEP = "yes" *) wire \FSM_onehot_state_reg_reg_n_0_[1] ;
  (* RTL_KEEP = "yes" *) wire \FSM_onehot_state_reg_reg_n_0_[3] ;
  (* RTL_KEEP = "yes" *) wire \FSM_onehot_state_reg_reg_n_0_[4] ;
  (* RTL_KEEP = "yes" *) wire \FSM_onehot_state_reg_reg_n_0_[8] ;
  (* RTL_KEEP = "yes" *) wire \FSM_onehot_state_reg_reg_n_0_[9] ;
  wire [7:0]Q;
  wire clk;
  wire [31:2]data1;
  wire \data_reg_s_reg[6] ;
  wire [0:0]\data_reg_s_reg[6]_0 ;
  wire [0:0]\data_reg_s_reg[6]_1 ;
  wire \data_reg_s_reg[7] ;
  wire [2:0]\data_reg_s_reg[7]_0 ;
  wire [0:0]enable_reg_reg_reg;
  wire enable_reg_reg_reg_0;
  wire [31:0]instruction_address_reg;
  (* RTL_KEEP = "yes" *) wire [4:0]out;
  wire out_en_b_i;
  wire out_en_reg;
  wire out_en_s;
  wire output_data_0_s_reg;
  wire output_data_0_s_reg_reg_0;
  wire \output_data_1[6]_i_1__0_n_0 ;
  wire \output_data_1[6]_i_2_n_0 ;
  wire \output_data_1[6]_i_3_n_0 ;
  wire \output_data_1[6]_i_4_n_0 ;
  wire output_data_1_s_reg;
  wire \output_data_2[6]_i_1_n_0 ;
  wire output_data_2_s_reg;
  wire \output_data_3[6]_i_1__0_n_0 ;
  wire [31:0]\output_data_3_reg[7] ;
  wire output_data_3_s_reg;
  wire \output_data_4[0]_i_1_n_0 ;
  wire \output_data_4[1]_i_1_n_0 ;
  wire \output_data_4[2]_i_1_n_0 ;
  wire output_data_4_s;
  wire output_data_4_s_reg;
  wire \pc_reg[0]_0 ;
  wire [31:0]pc_s__0;
  wire [14:14]pc_s__0__0;
  wire pc_s_n_0;
  wire [31:0]pc_s_reg;
  wire \pc_s_reg[14]_i_2_n_0 ;
  wire \pc_s_reg[14]_i_3_n_0 ;
  wire \pc_s_reg[21]_i_2_n_0 ;
  wire \pc_s_reg[21]_i_3_n_0 ;
  wire \pc_s_reg[28]_i_2_n_0 ;
  wire \pc_s_reg[28]_i_3_n_0 ;
  wire \pc_s_reg[28]_i_4_n_0 ;
  wire \pc_s_reg[29]_i_2_n_0 ;
  wire \pc_s_reg[30]_i_2_n_0 ;
  wire \pc_s_reg[31]_i_2_n_0 ;
  wire \pc_s_reg[7]_i_2_n_0 ;
  wire [31:0]\pc_s_reg_reg[31]_0 ;
  wire reset;
  wire \state_reg_reg[0] ;
  wire \state_reg_reg[1] ;
  wire \state_reg_reg[1]_0 ;
  wire \state_reg_reg[2] ;
  wire [5:5]temp_signal;
  wire w_en_i_1_n_0;

  LUT5 #(
    .INIT(32'hFFEAAAAA)) 
    \FSM_onehot_state_reg[0]_i_1__0 
       (.I0(\FSM_onehot_state_reg[0]_i_2__0_n_0 ),
        .I1(enable_reg_reg_reg_0),
        .I2(\pc_reg[0]_0 ),
        .I3(out[0]),
        .I4(\state_reg_reg[1] ),
        .O(\FSM_onehot_state_reg[0]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'h5C5C4C4CFCFCCC4C)) 
    \FSM_onehot_state_reg[0]_i_2__0 
       (.I0(\state_reg_reg[1]_0 ),
        .I1(out[0]),
        .I2(enable_reg_reg_reg_0),
        .I3(\FSM_onehot_state_reg_reg[7]_0 ),
        .I4(\pc_reg[0]_0 ),
        .I5(Q[0]),
        .O(\FSM_onehot_state_reg[0]_i_2__0_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \FSM_onehot_state_reg[0]_i_3 
       (.I0(\FSM_onehot_state_reg[0]_i_6__0_n_0 ),
        .I1(\FSM_onehot_state_reg_reg_n_0_[1] ),
        .I2(\FSM_onehot_state_reg_reg_n_0_[12] ),
        .I3(\FSM_onehot_state_reg_reg_n_0_[13] ),
        .O(\pc_reg[0]_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \FSM_onehot_state_reg[0]_i_6__0 
       (.I0(\FSM_onehot_state_reg_reg_n_0_[9] ),
        .I1(\FSM_onehot_state_reg_reg_n_0_[8] ),
        .I2(\FSM_onehot_state_reg_reg_n_0_[11] ),
        .I3(\FSM_onehot_state_reg_reg_n_0_[10] ),
        .O(\FSM_onehot_state_reg[0]_i_6__0_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    \FSM_onehot_state_reg[10]_i_1 
       (.I0(enable_reg_reg_reg_0),
        .I1(out[2]),
        .I2(Q[6]),
        .O(\FSM_onehot_state_reg[10]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    \FSM_onehot_state_reg[11]_i_1__0 
       (.I0(\FSM_onehot_state_reg_reg_n_0_[4] ),
        .I1(enable_reg_reg_reg_0),
        .I2(Q[7]),
        .O(\FSM_onehot_state_reg[11]_i_1__0_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    \FSM_onehot_state_reg[12]_i_1__0 
       (.I0(\FSM_onehot_state_reg_reg_n_0_[3] ),
        .I1(enable_reg_reg_reg_0),
        .I2(Q[7]),
        .O(\FSM_onehot_state_reg[12]_i_1__0_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    \FSM_onehot_state_reg[13]_i_1 
       (.I0(out[1]),
        .I1(enable_reg_reg_reg_0),
        .I2(Q[7]),
        .O(\FSM_onehot_state_reg[13]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \FSM_onehot_state_reg[1]_i_2 
       (.I0(\pc_reg[0]_0 ),
        .I1(enable_reg_reg_reg_0),
        .O(\FSM_onehot_state_reg_reg[1]_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \FSM_onehot_state_reg[1]_i_6 
       (.I0(\FSM_onehot_state_reg_reg_n_0_[13] ),
        .I1(\FSM_onehot_state_reg_reg_n_0_[12] ),
        .I2(\FSM_onehot_state_reg_reg_n_0_[1] ),
        .I3(\FSM_onehot_state_reg[0]_i_6__0_n_0 ),
        .I4(out[4]),
        .O(\FSM_onehot_state_reg_reg[0]_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFEFFFFFFFF)) 
    \FSM_onehot_state_reg[1]_i_8 
       (.I0(out[4]),
        .I1(\FSM_onehot_state_reg[0]_i_6__0_n_0 ),
        .I2(\FSM_onehot_state_reg_reg_n_0_[1] ),
        .I3(\FSM_onehot_state_reg_reg_n_0_[12] ),
        .I4(\FSM_onehot_state_reg_reg_n_0_[13] ),
        .I5(\state_reg_reg[2] ),
        .O(\state_reg_reg[0] ));
  LUT2 #(
    .INIT(4'h2)) 
    \FSM_onehot_state_reg[2]_i_2 
       (.I0(out[1]),
        .I1(enable_reg_reg_reg_0),
        .O(\FSM_onehot_state_reg_reg[2]_0 ));
  LUT4 #(
    .INIT(16'h8F80)) 
    \FSM_onehot_state_reg[3]_i_1__0 
       (.I0(out[1]),
        .I1(Q[7]),
        .I2(enable_reg_reg_reg_0),
        .I3(\FSM_onehot_state_reg_reg_n_0_[3] ),
        .O(\FSM_onehot_state_reg[3]_i_1__0_n_0 ));
  LUT4 #(
    .INIT(16'h8F80)) 
    \FSM_onehot_state_reg[4]_i_1__0 
       (.I0(\FSM_onehot_state_reg_reg_n_0_[3] ),
        .I1(Q[7]),
        .I2(enable_reg_reg_reg_0),
        .I3(\FSM_onehot_state_reg_reg_n_0_[4] ),
        .O(\FSM_onehot_state_reg[4]_i_1__0_n_0 ));
  LUT4 #(
    .INIT(16'h8F80)) 
    \FSM_onehot_state_reg[5]_i_1__0 
       (.I0(\FSM_onehot_state_reg_reg_n_0_[4] ),
        .I1(Q[7]),
        .I2(enable_reg_reg_reg_0),
        .I3(out[2]),
        .O(\FSM_onehot_state_reg[5]_i_1__0_n_0 ));
  LUT4 #(
    .INIT(16'h8F80)) 
    \FSM_onehot_state_reg[7]_i_1__0 
       (.I0(out[3]),
        .I1(Q[7]),
        .I2(enable_reg_reg_reg_0),
        .I3(out[4]),
        .O(\FSM_onehot_state_reg[7]_i_1__0_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \FSM_onehot_state_reg[8]_i_1__1 
       (.I0(out[4]),
        .I1(enable_reg_reg_reg_0),
        .O(\FSM_onehot_state_reg[8]_i_1__1_n_0 ));
  (* FSM_ENCODED_STATES = "bap_20:10000000000000,bap_21:00000000001000,bap_11:00000000000100,bap_exception_2:00000100000000,bap_exception_11:00000010000000,bap_exception_10:00001000000000,bap_51:00000001000000,bap_10:00000000000010,wait_state:00000000000001,bap_50:00010000000000,bap_40:00100000000000,bap_41:00000000100000,bap_31:00000000010000,bap_30:01000000000000" *) 
  (* KEEP = "yes" *) 
  FDSE #(
    .INIT(1'b1)) 
    \FSM_onehot_state_reg_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(\FSM_onehot_state_reg[0]_i_1__0_n_0 ),
        .Q(out[0]),
        .S(reset));
  (* FSM_ENCODED_STATES = "bap_20:10000000000000,bap_21:00000000001000,bap_11:00000000000100,bap_exception_2:00000100000000,bap_exception_11:00000010000000,bap_exception_10:00001000000000,bap_51:00000001000000,bap_10:00000000000010,wait_state:00000000000001,bap_50:00010000000000,bap_40:00100000000000,bap_41:00000000100000,bap_31:00000000010000,bap_30:01000000000000" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg_reg[10] 
       (.C(clk),
        .CE(1'b1),
        .D(\FSM_onehot_state_reg[10]_i_1_n_0 ),
        .Q(\FSM_onehot_state_reg_reg_n_0_[10] ),
        .R(reset));
  (* FSM_ENCODED_STATES = "bap_20:10000000000000,bap_21:00000000001000,bap_11:00000000000100,bap_exception_2:00000100000000,bap_exception_11:00000010000000,bap_exception_10:00001000000000,bap_51:00000001000000,bap_10:00000000000010,wait_state:00000000000001,bap_50:00010000000000,bap_40:00100000000000,bap_41:00000000100000,bap_31:00000000010000,bap_30:01000000000000" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg_reg[11] 
       (.C(clk),
        .CE(1'b1),
        .D(\FSM_onehot_state_reg[11]_i_1__0_n_0 ),
        .Q(\FSM_onehot_state_reg_reg_n_0_[11] ),
        .R(reset));
  (* FSM_ENCODED_STATES = "bap_20:10000000000000,bap_21:00000000001000,bap_11:00000000000100,bap_exception_2:00000100000000,bap_exception_11:00000010000000,bap_exception_10:00001000000000,bap_51:00000001000000,bap_10:00000000000010,wait_state:00000000000001,bap_50:00010000000000,bap_40:00100000000000,bap_41:00000000100000,bap_31:00000000010000,bap_30:01000000000000" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg_reg[12] 
       (.C(clk),
        .CE(1'b1),
        .D(\FSM_onehot_state_reg[12]_i_1__0_n_0 ),
        .Q(\FSM_onehot_state_reg_reg_n_0_[12] ),
        .R(reset));
  (* FSM_ENCODED_STATES = "bap_20:10000000000000,bap_21:00000000001000,bap_11:00000000000100,bap_exception_2:00000100000000,bap_exception_11:00000010000000,bap_exception_10:00001000000000,bap_51:00000001000000,bap_10:00000000000010,wait_state:00000000000001,bap_50:00010000000000,bap_40:00100000000000,bap_41:00000000100000,bap_31:00000000010000,bap_30:01000000000000" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg_reg[13] 
       (.C(clk),
        .CE(1'b1),
        .D(\FSM_onehot_state_reg[13]_i_1_n_0 ),
        .Q(\FSM_onehot_state_reg_reg_n_0_[13] ),
        .R(reset));
  (* FSM_ENCODED_STATES = "bap_20:10000000000000,bap_21:00000000001000,bap_11:00000000000100,bap_exception_2:00000100000000,bap_exception_11:00000010000000,bap_exception_10:00001000000000,bap_51:00000001000000,bap_10:00000000000010,wait_state:00000000000001,bap_50:00010000000000,bap_40:00100000000000,bap_41:00000000100000,bap_31:00000000010000,bap_30:01000000000000" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\data_reg_s_reg[7]_0 [0]),
        .Q(\FSM_onehot_state_reg_reg_n_0_[1] ),
        .R(reset));
  (* FSM_ENCODED_STATES = "bap_20:10000000000000,bap_21:00000000001000,bap_11:00000000000100,bap_exception_2:00000100000000,bap_exception_11:00000010000000,bap_exception_10:00001000000000,bap_51:00000001000000,bap_10:00000000000010,wait_state:00000000000001,bap_50:00010000000000,bap_40:00100000000000,bap_41:00000000100000,bap_31:00000000010000,bap_30:01000000000000" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(\data_reg_s_reg[7]_0 [1]),
        .Q(out[1]),
        .R(reset));
  (* FSM_ENCODED_STATES = "bap_20:10000000000000,bap_21:00000000001000,bap_11:00000000000100,bap_exception_2:00000100000000,bap_exception_11:00000010000000,bap_exception_10:00001000000000,bap_51:00000001000000,bap_10:00000000000010,wait_state:00000000000001,bap_50:00010000000000,bap_40:00100000000000,bap_41:00000000100000,bap_31:00000000010000,bap_30:01000000000000" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(\FSM_onehot_state_reg[3]_i_1__0_n_0 ),
        .Q(\FSM_onehot_state_reg_reg_n_0_[3] ),
        .R(reset));
  (* FSM_ENCODED_STATES = "bap_20:10000000000000,bap_21:00000000001000,bap_11:00000000000100,bap_exception_2:00000100000000,bap_exception_11:00000010000000,bap_exception_10:00001000000000,bap_51:00000001000000,bap_10:00000000000010,wait_state:00000000000001,bap_50:00010000000000,bap_40:00100000000000,bap_41:00000000100000,bap_31:00000000010000,bap_30:01000000000000" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg_reg[4] 
       (.C(clk),
        .CE(1'b1),
        .D(\FSM_onehot_state_reg[4]_i_1__0_n_0 ),
        .Q(\FSM_onehot_state_reg_reg_n_0_[4] ),
        .R(reset));
  (* FSM_ENCODED_STATES = "bap_20:10000000000000,bap_21:00000000001000,bap_11:00000000000100,bap_exception_2:00000100000000,bap_exception_11:00000010000000,bap_exception_10:00001000000000,bap_51:00000001000000,bap_10:00000000000010,wait_state:00000000000001,bap_50:00010000000000,bap_40:00100000000000,bap_41:00000000100000,bap_31:00000000010000,bap_30:01000000000000" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg_reg[5] 
       (.C(clk),
        .CE(1'b1),
        .D(\FSM_onehot_state_reg[5]_i_1__0_n_0 ),
        .Q(out[2]),
        .R(reset));
  (* FSM_ENCODED_STATES = "bap_20:10000000000000,bap_21:00000000001000,bap_11:00000000000100,bap_exception_2:00000100000000,bap_exception_11:00000010000000,bap_exception_10:00001000000000,bap_51:00000001000000,bap_10:00000000000010,wait_state:00000000000001,bap_50:00010000000000,bap_40:00100000000000,bap_41:00000000100000,bap_31:00000000010000,bap_30:01000000000000" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg_reg[6] 
       (.C(clk),
        .CE(enable_reg_reg_reg_0),
        .D(\data_reg_s_reg[6] ),
        .Q(out[3]),
        .R(reset));
  (* FSM_ENCODED_STATES = "bap_20:10000000000000,bap_21:00000000001000,bap_11:00000000000100,bap_exception_2:00000100000000,bap_exception_11:00000010000000,bap_exception_10:00001000000000,bap_51:00000001000000,bap_10:00000000000010,wait_state:00000000000001,bap_50:00010000000000,bap_40:00100000000000,bap_41:00000000100000,bap_31:00000000010000,bap_30:01000000000000" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg_reg[7] 
       (.C(clk),
        .CE(1'b1),
        .D(\FSM_onehot_state_reg[7]_i_1__0_n_0 ),
        .Q(out[4]),
        .R(reset));
  (* FSM_ENCODED_STATES = "bap_20:10000000000000,bap_21:00000000001000,bap_11:00000000000100,bap_exception_2:00000100000000,bap_exception_11:00000010000000,bap_exception_10:00001000000000,bap_51:00000001000000,bap_10:00000000000010,wait_state:00000000000001,bap_50:00010000000000,bap_40:00100000000000,bap_41:00000000100000,bap_31:00000000010000,bap_30:01000000000000" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg_reg[8] 
       (.C(clk),
        .CE(1'b1),
        .D(\FSM_onehot_state_reg[8]_i_1__1_n_0 ),
        .Q(\FSM_onehot_state_reg_reg_n_0_[8] ),
        .R(reset));
  (* FSM_ENCODED_STATES = "bap_20:10000000000000,bap_21:00000000001000,bap_11:00000000000100,bap_exception_2:00000100000000,bap_exception_11:00000010000000,bap_exception_10:00001000000000,bap_51:00000001000000,bap_10:00000000000010,wait_state:00000000000001,bap_50:00010000000000,bap_40:00100000000000,bap_41:00000000100000,bap_31:00000000010000,bap_30:01000000000000" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg_reg[9] 
       (.C(clk),
        .CE(1'b1),
        .D(\data_reg_s_reg[7]_0 [2]),
        .Q(\FSM_onehot_state_reg_reg_n_0_[9] ),
        .R(reset));
  FDRE enable_i_reg_reg
       (.C(clk),
        .CE(1'b1),
        .D(E),
        .Q(temp_signal),
        .R(reset));
  FDRE \instruction_address_reg_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(\output_data_3_reg[7] [0]),
        .Q(instruction_address_reg[0]),
        .R(reset));
  FDRE \instruction_address_reg_reg[10] 
       (.C(clk),
        .CE(1'b1),
        .D(\output_data_3_reg[7] [10]),
        .Q(instruction_address_reg[10]),
        .R(reset));
  FDRE \instruction_address_reg_reg[11] 
       (.C(clk),
        .CE(1'b1),
        .D(\output_data_3_reg[7] [11]),
        .Q(instruction_address_reg[11]),
        .R(reset));
  FDRE \instruction_address_reg_reg[12] 
       (.C(clk),
        .CE(1'b1),
        .D(\output_data_3_reg[7] [12]),
        .Q(instruction_address_reg[12]),
        .R(reset));
  FDRE \instruction_address_reg_reg[13] 
       (.C(clk),
        .CE(1'b1),
        .D(\output_data_3_reg[7] [13]),
        .Q(instruction_address_reg[13]),
        .R(reset));
  FDRE \instruction_address_reg_reg[14] 
       (.C(clk),
        .CE(1'b1),
        .D(\output_data_3_reg[7] [14]),
        .Q(instruction_address_reg[14]),
        .R(reset));
  FDRE \instruction_address_reg_reg[15] 
       (.C(clk),
        .CE(1'b1),
        .D(\output_data_3_reg[7] [15]),
        .Q(instruction_address_reg[15]),
        .R(reset));
  FDRE \instruction_address_reg_reg[16] 
       (.C(clk),
        .CE(1'b1),
        .D(\output_data_3_reg[7] [16]),
        .Q(instruction_address_reg[16]),
        .R(reset));
  FDRE \instruction_address_reg_reg[17] 
       (.C(clk),
        .CE(1'b1),
        .D(\output_data_3_reg[7] [17]),
        .Q(instruction_address_reg[17]),
        .R(reset));
  FDRE \instruction_address_reg_reg[18] 
       (.C(clk),
        .CE(1'b1),
        .D(\output_data_3_reg[7] [18]),
        .Q(instruction_address_reg[18]),
        .R(reset));
  FDRE \instruction_address_reg_reg[19] 
       (.C(clk),
        .CE(1'b1),
        .D(\output_data_3_reg[7] [19]),
        .Q(instruction_address_reg[19]),
        .R(reset));
  FDRE \instruction_address_reg_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\output_data_3_reg[7] [1]),
        .Q(instruction_address_reg[1]),
        .R(reset));
  FDRE \instruction_address_reg_reg[20] 
       (.C(clk),
        .CE(1'b1),
        .D(\output_data_3_reg[7] [20]),
        .Q(instruction_address_reg[20]),
        .R(reset));
  FDRE \instruction_address_reg_reg[21] 
       (.C(clk),
        .CE(1'b1),
        .D(\output_data_3_reg[7] [21]),
        .Q(instruction_address_reg[21]),
        .R(reset));
  FDRE \instruction_address_reg_reg[22] 
       (.C(clk),
        .CE(1'b1),
        .D(\output_data_3_reg[7] [22]),
        .Q(instruction_address_reg[22]),
        .R(reset));
  FDRE \instruction_address_reg_reg[23] 
       (.C(clk),
        .CE(1'b1),
        .D(\output_data_3_reg[7] [23]),
        .Q(instruction_address_reg[23]),
        .R(reset));
  FDRE \instruction_address_reg_reg[24] 
       (.C(clk),
        .CE(1'b1),
        .D(\output_data_3_reg[7] [24]),
        .Q(instruction_address_reg[24]),
        .R(reset));
  FDRE \instruction_address_reg_reg[25] 
       (.C(clk),
        .CE(1'b1),
        .D(\output_data_3_reg[7] [25]),
        .Q(instruction_address_reg[25]),
        .R(reset));
  FDRE \instruction_address_reg_reg[26] 
       (.C(clk),
        .CE(1'b1),
        .D(\output_data_3_reg[7] [26]),
        .Q(instruction_address_reg[26]),
        .R(reset));
  FDRE \instruction_address_reg_reg[27] 
       (.C(clk),
        .CE(1'b1),
        .D(\output_data_3_reg[7] [27]),
        .Q(instruction_address_reg[27]),
        .R(reset));
  FDRE \instruction_address_reg_reg[28] 
       (.C(clk),
        .CE(1'b1),
        .D(\output_data_3_reg[7] [28]),
        .Q(instruction_address_reg[28]),
        .R(reset));
  FDRE \instruction_address_reg_reg[29] 
       (.C(clk),
        .CE(1'b1),
        .D(\output_data_3_reg[7] [29]),
        .Q(instruction_address_reg[29]),
        .R(reset));
  FDRE \instruction_address_reg_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(\output_data_3_reg[7] [2]),
        .Q(instruction_address_reg[2]),
        .R(reset));
  FDRE \instruction_address_reg_reg[30] 
       (.C(clk),
        .CE(1'b1),
        .D(\output_data_3_reg[7] [30]),
        .Q(instruction_address_reg[30]),
        .R(reset));
  FDRE \instruction_address_reg_reg[31] 
       (.C(clk),
        .CE(1'b1),
        .D(\output_data_3_reg[7] [31]),
        .Q(instruction_address_reg[31]),
        .R(reset));
  FDRE \instruction_address_reg_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(\output_data_3_reg[7] [3]),
        .Q(instruction_address_reg[3]),
        .R(reset));
  FDRE \instruction_address_reg_reg[4] 
       (.C(clk),
        .CE(1'b1),
        .D(\output_data_3_reg[7] [4]),
        .Q(instruction_address_reg[4]),
        .R(reset));
  FDRE \instruction_address_reg_reg[5] 
       (.C(clk),
        .CE(1'b1),
        .D(\output_data_3_reg[7] [5]),
        .Q(instruction_address_reg[5]),
        .R(reset));
  FDRE \instruction_address_reg_reg[6] 
       (.C(clk),
        .CE(1'b1),
        .D(\output_data_3_reg[7] [6]),
        .Q(instruction_address_reg[6]),
        .R(reset));
  FDRE \instruction_address_reg_reg[7] 
       (.C(clk),
        .CE(1'b1),
        .D(\output_data_3_reg[7] [7]),
        .Q(instruction_address_reg[7]),
        .R(reset));
  FDRE \instruction_address_reg_reg[8] 
       (.C(clk),
        .CE(1'b1),
        .D(\output_data_3_reg[7] [8]),
        .Q(instruction_address_reg[8]),
        .R(reset));
  FDRE \instruction_address_reg_reg[9] 
       (.C(clk),
        .CE(1'b1),
        .D(\output_data_3_reg[7] [9]),
        .Q(instruction_address_reg[9]),
        .R(reset));
  LUT4 #(
    .INIT(16'h8880)) 
    \output_data_0[5]_i_2 
       (.I0(Q[0]),
        .I1(enable_reg_reg_reg_0),
        .I2(out[0]),
        .I3(\pc_reg[0]_0 ),
        .O(output_data_0_s_reg_reg_0));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_0_reg[0] 
       (.C(clk),
        .CE(enable_reg_reg_reg),
        .D(Q[1]),
        .Q(data1[2]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_0_reg[1] 
       (.C(clk),
        .CE(enable_reg_reg_reg),
        .D(Q[2]),
        .Q(data1[3]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_0_reg[2] 
       (.C(clk),
        .CE(enable_reg_reg_reg),
        .D(Q[3]),
        .Q(data1[4]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_0_reg[3] 
       (.C(clk),
        .CE(enable_reg_reg_reg),
        .D(Q[4]),
        .Q(data1[5]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_0_reg[4] 
       (.C(clk),
        .CE(enable_reg_reg_reg),
        .D(Q[5]),
        .Q(data1[6]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_0_reg[5] 
       (.C(clk),
        .CE(enable_reg_reg_reg),
        .D(Q[6]),
        .Q(data1[7]),
        .R(reset));
  FDRE output_data_0_s_reg_reg
       (.C(clk),
        .CE(1'b1),
        .D(enable_reg_reg_reg),
        .Q(output_data_0_s_reg),
        .R(reset));
  LUT2 #(
    .INIT(4'h8)) 
    \output_data_1[6]_i_1__0 
       (.I0(enable_reg_reg_reg_0),
        .I1(out[1]),
        .O(\output_data_1[6]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF0000FF80FF80)) 
    \output_data_1[6]_i_2 
       (.I0(pc_s_n_0),
        .I1(data1[14]),
        .I2(\output_data_1[6]_i_3_n_0 ),
        .I3(\output_data_1[6]_i_4_n_0 ),
        .I4(Q[6]),
        .I5(\data_reg_s_reg[7] ),
        .O(\output_data_1[6]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \output_data_1[6]_i_3 
       (.I0(output_data_2_s_reg),
        .I1(output_data_3_s_reg),
        .I2(output_data_4_s_reg),
        .I3(output_data_1_s_reg),
        .O(\output_data_1[6]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT5 #(
    .INIT(32'hF8AA88AA)) 
    \output_data_1[6]_i_4 
       (.I0(pc_s_reg[14]),
        .I1(output_data_0_s_reg),
        .I2(instruction_address_reg[14]),
        .I3(pc_s_n_0),
        .I4(temp_signal),
        .O(\output_data_1[6]_i_4_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_1_reg[0] 
       (.C(clk),
        .CE(\output_data_1[6]_i_1__0_n_0 ),
        .D(Q[0]),
        .Q(data1[8]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_1_reg[1] 
       (.C(clk),
        .CE(\output_data_1[6]_i_1__0_n_0 ),
        .D(Q[1]),
        .Q(data1[9]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_1_reg[2] 
       (.C(clk),
        .CE(\output_data_1[6]_i_1__0_n_0 ),
        .D(Q[2]),
        .Q(data1[10]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_1_reg[3] 
       (.C(clk),
        .CE(\output_data_1[6]_i_1__0_n_0 ),
        .D(Q[3]),
        .Q(data1[11]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_1_reg[4] 
       (.C(clk),
        .CE(\output_data_1[6]_i_1__0_n_0 ),
        .D(Q[4]),
        .Q(data1[12]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_1_reg[5] 
       (.C(clk),
        .CE(\output_data_1[6]_i_1__0_n_0 ),
        .D(Q[5]),
        .Q(data1[13]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_1_reg[6] 
       (.C(clk),
        .CE(\output_data_1[6]_i_1__0_n_0 ),
        .D(\output_data_1[6]_i_2_n_0 ),
        .Q(data1[14]),
        .R(reset));
  FDRE output_data_1_s_reg_reg
       (.C(clk),
        .CE(1'b1),
        .D(\FSM_onehot_state_reg[13]_i_1_n_0 ),
        .Q(output_data_1_s_reg),
        .R(reset));
  LUT2 #(
    .INIT(4'h8)) 
    \output_data_2[6]_i_1 
       (.I0(enable_reg_reg_reg_0),
        .I1(\FSM_onehot_state_reg_reg_n_0_[3] ),
        .O(\output_data_2[6]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_2_reg[0] 
       (.C(clk),
        .CE(\output_data_2[6]_i_1_n_0 ),
        .D(Q[0]),
        .Q(data1[15]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_2_reg[1] 
       (.C(clk),
        .CE(\output_data_2[6]_i_1_n_0 ),
        .D(Q[1]),
        .Q(data1[16]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_2_reg[2] 
       (.C(clk),
        .CE(\output_data_2[6]_i_1_n_0 ),
        .D(Q[2]),
        .Q(data1[17]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_2_reg[3] 
       (.C(clk),
        .CE(\output_data_2[6]_i_1_n_0 ),
        .D(Q[3]),
        .Q(data1[18]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_2_reg[4] 
       (.C(clk),
        .CE(\output_data_2[6]_i_1_n_0 ),
        .D(Q[4]),
        .Q(data1[19]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_2_reg[5] 
       (.C(clk),
        .CE(\output_data_2[6]_i_1_n_0 ),
        .D(Q[5]),
        .Q(data1[20]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_2_reg[6] 
       (.C(clk),
        .CE(\output_data_2[6]_i_1_n_0 ),
        .D(\data_reg_s_reg[6]_0 ),
        .Q(data1[21]),
        .R(reset));
  FDRE output_data_2_s_reg_reg
       (.C(clk),
        .CE(1'b1),
        .D(\FSM_onehot_state_reg[12]_i_1__0_n_0 ),
        .Q(output_data_2_s_reg),
        .R(reset));
  LUT2 #(
    .INIT(4'h8)) 
    \output_data_3[6]_i_1__0 
       (.I0(enable_reg_reg_reg_0),
        .I1(\FSM_onehot_state_reg_reg_n_0_[4] ),
        .O(\output_data_3[6]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_3_reg[0] 
       (.C(clk),
        .CE(\output_data_3[6]_i_1__0_n_0 ),
        .D(Q[0]),
        .Q(data1[22]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_3_reg[1] 
       (.C(clk),
        .CE(\output_data_3[6]_i_1__0_n_0 ),
        .D(Q[1]),
        .Q(data1[23]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_3_reg[2] 
       (.C(clk),
        .CE(\output_data_3[6]_i_1__0_n_0 ),
        .D(Q[2]),
        .Q(data1[24]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_3_reg[3] 
       (.C(clk),
        .CE(\output_data_3[6]_i_1__0_n_0 ),
        .D(Q[3]),
        .Q(data1[25]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_3_reg[4] 
       (.C(clk),
        .CE(\output_data_3[6]_i_1__0_n_0 ),
        .D(Q[4]),
        .Q(data1[26]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_3_reg[5] 
       (.C(clk),
        .CE(\output_data_3[6]_i_1__0_n_0 ),
        .D(Q[5]),
        .Q(data1[27]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_3_reg[6] 
       (.C(clk),
        .CE(\output_data_3[6]_i_1__0_n_0 ),
        .D(\data_reg_s_reg[6]_1 ),
        .Q(data1[28]),
        .R(reset));
  FDRE output_data_3_s_reg_reg
       (.C(clk),
        .CE(1'b1),
        .D(\FSM_onehot_state_reg[11]_i_1__0_n_0 ),
        .Q(output_data_3_s_reg),
        .R(reset));
  LUT4 #(
    .INIT(16'hBF80)) 
    \output_data_4[0]_i_1 
       (.I0(Q[0]),
        .I1(out[2]),
        .I2(enable_reg_reg_reg_0),
        .I3(data1[29]),
        .O(\output_data_4[0]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hBF80)) 
    \output_data_4[1]_i_1 
       (.I0(Q[1]),
        .I1(out[2]),
        .I2(enable_reg_reg_reg_0),
        .I3(data1[30]),
        .O(\output_data_4[1]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hBF80)) 
    \output_data_4[2]_i_1 
       (.I0(Q[2]),
        .I1(out[2]),
        .I2(enable_reg_reg_reg_0),
        .I3(data1[31]),
        .O(\output_data_4[2]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_4_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(\output_data_4[0]_i_1_n_0 ),
        .Q(data1[29]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_4_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\output_data_4[1]_i_1_n_0 ),
        .Q(data1[30]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \output_data_4_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(\output_data_4[2]_i_1_n_0 ),
        .Q(data1[31]),
        .R(reset));
  LUT2 #(
    .INIT(4'h8)) 
    output_data_4_s_reg_i_1
       (.I0(out[2]),
        .I1(enable_reg_reg_reg_0),
        .O(output_data_4_s));
  FDRE output_data_4_s_reg_reg
       (.C(clk),
        .CE(1'b1),
        .D(output_data_4_s),
        .Q(output_data_4_s_reg),
        .R(reset));
  FDRE \pc_reg[0] 
       (.C(clk),
        .CE(w_en_i_1_n_0),
        .D(pc_s__0[0]),
        .Q(\pc_s_reg_reg[31]_0 [0]),
        .R(reset));
  FDRE \pc_reg[10] 
       (.C(clk),
        .CE(w_en_i_1_n_0),
        .D(pc_s__0[10]),
        .Q(\pc_s_reg_reg[31]_0 [10]),
        .R(reset));
  FDRE \pc_reg[11] 
       (.C(clk),
        .CE(w_en_i_1_n_0),
        .D(pc_s__0[11]),
        .Q(\pc_s_reg_reg[31]_0 [11]),
        .R(reset));
  FDRE \pc_reg[12] 
       (.C(clk),
        .CE(w_en_i_1_n_0),
        .D(pc_s__0[12]),
        .Q(\pc_s_reg_reg[31]_0 [12]),
        .R(reset));
  FDRE \pc_reg[13] 
       (.C(clk),
        .CE(w_en_i_1_n_0),
        .D(pc_s__0[13]),
        .Q(\pc_s_reg_reg[31]_0 [13]),
        .R(reset));
  FDRE \pc_reg[14] 
       (.C(clk),
        .CE(w_en_i_1_n_0),
        .D(pc_s__0__0),
        .Q(\pc_s_reg_reg[31]_0 [14]),
        .R(reset));
  FDRE \pc_reg[15] 
       (.C(clk),
        .CE(w_en_i_1_n_0),
        .D(pc_s__0[15]),
        .Q(\pc_s_reg_reg[31]_0 [15]),
        .R(reset));
  FDRE \pc_reg[16] 
       (.C(clk),
        .CE(w_en_i_1_n_0),
        .D(pc_s__0[16]),
        .Q(\pc_s_reg_reg[31]_0 [16]),
        .R(reset));
  FDRE \pc_reg[17] 
       (.C(clk),
        .CE(w_en_i_1_n_0),
        .D(pc_s__0[17]),
        .Q(\pc_s_reg_reg[31]_0 [17]),
        .R(reset));
  FDRE \pc_reg[18] 
       (.C(clk),
        .CE(w_en_i_1_n_0),
        .D(pc_s__0[18]),
        .Q(\pc_s_reg_reg[31]_0 [18]),
        .R(reset));
  FDRE \pc_reg[19] 
       (.C(clk),
        .CE(w_en_i_1_n_0),
        .D(pc_s__0[19]),
        .Q(\pc_s_reg_reg[31]_0 [19]),
        .R(reset));
  FDRE \pc_reg[1] 
       (.C(clk),
        .CE(w_en_i_1_n_0),
        .D(pc_s__0[1]),
        .Q(\pc_s_reg_reg[31]_0 [1]),
        .R(reset));
  FDRE \pc_reg[20] 
       (.C(clk),
        .CE(w_en_i_1_n_0),
        .D(pc_s__0[20]),
        .Q(\pc_s_reg_reg[31]_0 [20]),
        .R(reset));
  FDRE \pc_reg[21] 
       (.C(clk),
        .CE(w_en_i_1_n_0),
        .D(D[0]),
        .Q(\pc_s_reg_reg[31]_0 [21]),
        .R(reset));
  FDRE \pc_reg[22] 
       (.C(clk),
        .CE(w_en_i_1_n_0),
        .D(pc_s__0[22]),
        .Q(\pc_s_reg_reg[31]_0 [22]),
        .R(reset));
  FDRE \pc_reg[23] 
       (.C(clk),
        .CE(w_en_i_1_n_0),
        .D(pc_s__0[23]),
        .Q(\pc_s_reg_reg[31]_0 [23]),
        .R(reset));
  FDRE \pc_reg[24] 
       (.C(clk),
        .CE(w_en_i_1_n_0),
        .D(pc_s__0[24]),
        .Q(\pc_s_reg_reg[31]_0 [24]),
        .R(reset));
  FDRE \pc_reg[25] 
       (.C(clk),
        .CE(w_en_i_1_n_0),
        .D(pc_s__0[25]),
        .Q(\pc_s_reg_reg[31]_0 [25]),
        .R(reset));
  FDRE \pc_reg[26] 
       (.C(clk),
        .CE(w_en_i_1_n_0),
        .D(pc_s__0[26]),
        .Q(\pc_s_reg_reg[31]_0 [26]),
        .R(reset));
  FDRE \pc_reg[27] 
       (.C(clk),
        .CE(w_en_i_1_n_0),
        .D(pc_s__0[27]),
        .Q(\pc_s_reg_reg[31]_0 [27]),
        .R(reset));
  FDRE \pc_reg[28] 
       (.C(clk),
        .CE(w_en_i_1_n_0),
        .D(D[1]),
        .Q(\pc_s_reg_reg[31]_0 [28]),
        .R(reset));
  FDRE \pc_reg[29] 
       (.C(clk),
        .CE(w_en_i_1_n_0),
        .D(pc_s__0[29]),
        .Q(\pc_s_reg_reg[31]_0 [29]),
        .R(reset));
  FDRE \pc_reg[2] 
       (.C(clk),
        .CE(w_en_i_1_n_0),
        .D(pc_s__0[2]),
        .Q(\pc_s_reg_reg[31]_0 [2]),
        .R(reset));
  FDRE \pc_reg[30] 
       (.C(clk),
        .CE(w_en_i_1_n_0),
        .D(pc_s__0[30]),
        .Q(\pc_s_reg_reg[31]_0 [30]),
        .R(reset));
  FDRE \pc_reg[31] 
       (.C(clk),
        .CE(w_en_i_1_n_0),
        .D(pc_s__0[31]),
        .Q(\pc_s_reg_reg[31]_0 [31]),
        .R(reset));
  FDRE \pc_reg[3] 
       (.C(clk),
        .CE(w_en_i_1_n_0),
        .D(pc_s__0[3]),
        .Q(\pc_s_reg_reg[31]_0 [3]),
        .R(reset));
  FDRE \pc_reg[4] 
       (.C(clk),
        .CE(w_en_i_1_n_0),
        .D(pc_s__0[4]),
        .Q(\pc_s_reg_reg[31]_0 [4]),
        .R(reset));
  FDRE \pc_reg[5] 
       (.C(clk),
        .CE(w_en_i_1_n_0),
        .D(pc_s__0[5]),
        .Q(\pc_s_reg_reg[31]_0 [5]),
        .R(reset));
  FDRE \pc_reg[6] 
       (.C(clk),
        .CE(w_en_i_1_n_0),
        .D(pc_s__0[6]),
        .Q(\pc_s_reg_reg[31]_0 [6]),
        .R(reset));
  FDRE \pc_reg[7] 
       (.C(clk),
        .CE(w_en_i_1_n_0),
        .D(pc_s__0[7]),
        .Q(\pc_s_reg_reg[31]_0 [7]),
        .R(reset));
  FDRE \pc_reg[8] 
       (.C(clk),
        .CE(w_en_i_1_n_0),
        .D(pc_s__0[8]),
        .Q(\pc_s_reg_reg[31]_0 [8]),
        .R(reset));
  FDRE \pc_reg[9] 
       (.C(clk),
        .CE(w_en_i_1_n_0),
        .D(pc_s__0[9]),
        .Q(\pc_s_reg_reg[31]_0 [9]),
        .R(reset));
  LUT6 #(
    .INIT(64'h0000000100010116)) 
    pc_s
       (.I0(output_data_0_s_reg),
        .I1(output_data_1_s_reg),
        .I2(output_data_2_s_reg),
        .I3(output_data_3_s_reg),
        .I4(output_data_4_s_reg),
        .I5(temp_signal),
        .O(pc_s_n_0));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT4 #(
    .INIT(16'hCA0A)) 
    \pc_s_reg[0]_i_1 
       (.I0(pc_s_reg[0]),
        .I1(instruction_address_reg[0]),
        .I2(pc_s_n_0),
        .I3(temp_signal),
        .O(pc_s__0[0]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc_s_reg[10]_i_1 
       (.I0(\pc_s_reg[28]_i_3_n_0 ),
        .I1(instruction_address_reg[10]),
        .I2(\pc_s_reg[14]_i_3_n_0 ),
        .I3(pc_s_reg[10]),
        .I4(data1[10]),
        .I5(\pc_s_reg[14]_i_2_n_0 ),
        .O(pc_s__0[10]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc_s_reg[11]_i_1 
       (.I0(\pc_s_reg[28]_i_3_n_0 ),
        .I1(instruction_address_reg[11]),
        .I2(\pc_s_reg[14]_i_3_n_0 ),
        .I3(pc_s_reg[11]),
        .I4(data1[11]),
        .I5(\pc_s_reg[14]_i_2_n_0 ),
        .O(pc_s__0[11]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc_s_reg[12]_i_1 
       (.I0(\pc_s_reg[28]_i_3_n_0 ),
        .I1(instruction_address_reg[12]),
        .I2(\pc_s_reg[14]_i_3_n_0 ),
        .I3(pc_s_reg[12]),
        .I4(data1[12]),
        .I5(\pc_s_reg[14]_i_2_n_0 ),
        .O(pc_s__0[12]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc_s_reg[13]_i_1 
       (.I0(\pc_s_reg[28]_i_3_n_0 ),
        .I1(instruction_address_reg[13]),
        .I2(\pc_s_reg[14]_i_3_n_0 ),
        .I3(pc_s_reg[13]),
        .I4(data1[13]),
        .I5(\pc_s_reg[14]_i_2_n_0 ),
        .O(pc_s__0[13]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc_s_reg[14]_i_1 
       (.I0(data1[14]),
        .I1(\pc_s_reg[14]_i_2_n_0 ),
        .I2(\pc_s_reg[28]_i_3_n_0 ),
        .I3(instruction_address_reg[14]),
        .I4(\pc_s_reg[14]_i_3_n_0 ),
        .I5(pc_s_reg[14]),
        .O(pc_s__0__0));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    \pc_s_reg[14]_i_2 
       (.I0(output_data_1_s_reg),
        .I1(output_data_4_s_reg),
        .I2(output_data_3_s_reg),
        .I3(output_data_2_s_reg),
        .I4(pc_s_n_0),
        .O(\pc_s_reg[14]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \pc_s_reg[14]_i_3 
       (.I0(output_data_0_s_reg),
        .I1(pc_s_n_0),
        .O(\pc_s_reg[14]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc_s_reg[15]_i_1 
       (.I0(\pc_s_reg[28]_i_3_n_0 ),
        .I1(instruction_address_reg[15]),
        .I2(\pc_s_reg[21]_i_3_n_0 ),
        .I3(pc_s_reg[15]),
        .I4(data1[15]),
        .I5(\pc_s_reg[21]_i_2_n_0 ),
        .O(pc_s__0[15]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc_s_reg[16]_i_1 
       (.I0(\pc_s_reg[28]_i_3_n_0 ),
        .I1(instruction_address_reg[16]),
        .I2(\pc_s_reg[21]_i_3_n_0 ),
        .I3(pc_s_reg[16]),
        .I4(data1[16]),
        .I5(\pc_s_reg[21]_i_2_n_0 ),
        .O(pc_s__0[16]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc_s_reg[17]_i_1 
       (.I0(\pc_s_reg[28]_i_3_n_0 ),
        .I1(instruction_address_reg[17]),
        .I2(\pc_s_reg[21]_i_3_n_0 ),
        .I3(pc_s_reg[17]),
        .I4(data1[17]),
        .I5(\pc_s_reg[21]_i_2_n_0 ),
        .O(pc_s__0[17]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc_s_reg[18]_i_1 
       (.I0(\pc_s_reg[28]_i_3_n_0 ),
        .I1(instruction_address_reg[18]),
        .I2(\pc_s_reg[21]_i_3_n_0 ),
        .I3(pc_s_reg[18]),
        .I4(data1[18]),
        .I5(\pc_s_reg[21]_i_2_n_0 ),
        .O(pc_s__0[18]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc_s_reg[19]_i_1 
       (.I0(\pc_s_reg[28]_i_3_n_0 ),
        .I1(instruction_address_reg[19]),
        .I2(\pc_s_reg[21]_i_3_n_0 ),
        .I3(pc_s_reg[19]),
        .I4(data1[19]),
        .I5(\pc_s_reg[21]_i_2_n_0 ),
        .O(pc_s__0[19]));
  LUT4 #(
    .INIT(16'hCA0A)) 
    \pc_s_reg[1]_i_1 
       (.I0(pc_s_reg[1]),
        .I1(instruction_address_reg[1]),
        .I2(pc_s_n_0),
        .I3(temp_signal),
        .O(pc_s__0[1]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc_s_reg[20]_i_1 
       (.I0(\pc_s_reg[28]_i_3_n_0 ),
        .I1(instruction_address_reg[20]),
        .I2(\pc_s_reg[21]_i_3_n_0 ),
        .I3(pc_s_reg[20]),
        .I4(data1[20]),
        .I5(\pc_s_reg[21]_i_2_n_0 ),
        .O(pc_s__0[20]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc_s_reg[21]_i_1 
       (.I0(data1[21]),
        .I1(\pc_s_reg[21]_i_2_n_0 ),
        .I2(\pc_s_reg[28]_i_3_n_0 ),
        .I3(instruction_address_reg[21]),
        .I4(\pc_s_reg[21]_i_3_n_0 ),
        .I5(pc_s_reg[21]),
        .O(D[0]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT4 #(
    .INIT(16'hFE00)) 
    \pc_s_reg[21]_i_2 
       (.I0(output_data_2_s_reg),
        .I1(output_data_3_s_reg),
        .I2(output_data_4_s_reg),
        .I3(pc_s_n_0),
        .O(\pc_s_reg[21]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hEF)) 
    \pc_s_reg[21]_i_3 
       (.I0(output_data_1_s_reg),
        .I1(output_data_0_s_reg),
        .I2(pc_s_n_0),
        .O(\pc_s_reg[21]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc_s_reg[22]_i_1 
       (.I0(\pc_s_reg[28]_i_3_n_0 ),
        .I1(instruction_address_reg[22]),
        .I2(\pc_s_reg[28]_i_4_n_0 ),
        .I3(data1[22]),
        .I4(pc_s_reg[22]),
        .I5(\pc_s_reg[28]_i_2_n_0 ),
        .O(pc_s__0[22]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc_s_reg[23]_i_1 
       (.I0(\pc_s_reg[28]_i_3_n_0 ),
        .I1(instruction_address_reg[23]),
        .I2(\pc_s_reg[28]_i_4_n_0 ),
        .I3(data1[23]),
        .I4(pc_s_reg[23]),
        .I5(\pc_s_reg[28]_i_2_n_0 ),
        .O(pc_s__0[23]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc_s_reg[24]_i_1 
       (.I0(\pc_s_reg[28]_i_3_n_0 ),
        .I1(instruction_address_reg[24]),
        .I2(\pc_s_reg[28]_i_4_n_0 ),
        .I3(data1[24]),
        .I4(pc_s_reg[24]),
        .I5(\pc_s_reg[28]_i_2_n_0 ),
        .O(pc_s__0[24]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc_s_reg[25]_i_1 
       (.I0(\pc_s_reg[28]_i_3_n_0 ),
        .I1(instruction_address_reg[25]),
        .I2(\pc_s_reg[28]_i_4_n_0 ),
        .I3(data1[25]),
        .I4(pc_s_reg[25]),
        .I5(\pc_s_reg[28]_i_2_n_0 ),
        .O(pc_s__0[25]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc_s_reg[26]_i_1 
       (.I0(\pc_s_reg[28]_i_3_n_0 ),
        .I1(instruction_address_reg[26]),
        .I2(\pc_s_reg[28]_i_4_n_0 ),
        .I3(data1[26]),
        .I4(pc_s_reg[26]),
        .I5(\pc_s_reg[28]_i_2_n_0 ),
        .O(pc_s__0[26]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc_s_reg[27]_i_1 
       (.I0(\pc_s_reg[28]_i_3_n_0 ),
        .I1(instruction_address_reg[27]),
        .I2(\pc_s_reg[28]_i_4_n_0 ),
        .I3(data1[27]),
        .I4(pc_s_reg[27]),
        .I5(\pc_s_reg[28]_i_2_n_0 ),
        .O(pc_s__0[27]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc_s_reg[28]_i_1 
       (.I0(pc_s_reg[28]),
        .I1(\pc_s_reg[28]_i_2_n_0 ),
        .I2(\pc_s_reg[28]_i_3_n_0 ),
        .I3(instruction_address_reg[28]),
        .I4(\pc_s_reg[28]_i_4_n_0 ),
        .I5(data1[28]),
        .O(D[1]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT4 #(
    .INIT(16'hFFFD)) 
    \pc_s_reg[28]_i_2 
       (.I0(pc_s_n_0),
        .I1(output_data_0_s_reg),
        .I2(output_data_1_s_reg),
        .I3(output_data_2_s_reg),
        .O(\pc_s_reg[28]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \pc_s_reg[28]_i_3 
       (.I0(pc_s_n_0),
        .I1(temp_signal),
        .O(\pc_s_reg[28]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hE0)) 
    \pc_s_reg[28]_i_4 
       (.I0(output_data_4_s_reg),
        .I1(output_data_3_s_reg),
        .I2(pc_s_n_0),
        .O(\pc_s_reg[28]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFAAEAAAEAAAEAAA)) 
    \pc_s_reg[29]_i_1 
       (.I0(\pc_s_reg[29]_i_2_n_0 ),
        .I1(output_data_4_s_reg),
        .I2(data1[29]),
        .I3(pc_s_n_0),
        .I4(temp_signal),
        .I5(instruction_address_reg[29]),
        .O(pc_s__0[29]));
  LUT6 #(
    .INIT(64'hFFFFFFFB00000000)) 
    \pc_s_reg[29]_i_2 
       (.I0(output_data_3_s_reg),
        .I1(pc_s_n_0),
        .I2(output_data_0_s_reg),
        .I3(output_data_1_s_reg),
        .I4(output_data_2_s_reg),
        .I5(pc_s_reg[29]),
        .O(\pc_s_reg[29]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFB380B380B380)) 
    \pc_s_reg[2]_i_1 
       (.I0(temp_signal),
        .I1(pc_s_n_0),
        .I2(instruction_address_reg[2]),
        .I3(pc_s_reg[2]),
        .I4(data1[2]),
        .I5(\pc_s_reg[7]_i_2_n_0 ),
        .O(pc_s__0[2]));
  LUT6 #(
    .INIT(64'hFFAAEAAAEAAAEAAA)) 
    \pc_s_reg[30]_i_1 
       (.I0(\pc_s_reg[30]_i_2_n_0 ),
        .I1(output_data_4_s_reg),
        .I2(data1[30]),
        .I3(pc_s_n_0),
        .I4(temp_signal),
        .I5(instruction_address_reg[30]),
        .O(pc_s__0[30]));
  LUT6 #(
    .INIT(64'hFFFFFFFB00000000)) 
    \pc_s_reg[30]_i_2 
       (.I0(output_data_3_s_reg),
        .I1(pc_s_n_0),
        .I2(output_data_0_s_reg),
        .I3(output_data_1_s_reg),
        .I4(output_data_2_s_reg),
        .I5(pc_s_reg[30]),
        .O(\pc_s_reg[30]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFAAEAAAEAAAEAAA)) 
    \pc_s_reg[31]_i_1 
       (.I0(\pc_s_reg[31]_i_2_n_0 ),
        .I1(output_data_4_s_reg),
        .I2(data1[31]),
        .I3(pc_s_n_0),
        .I4(temp_signal),
        .I5(instruction_address_reg[31]),
        .O(pc_s__0[31]));
  LUT6 #(
    .INIT(64'hFFFFFFFB00000000)) 
    \pc_s_reg[31]_i_2 
       (.I0(output_data_3_s_reg),
        .I1(pc_s_n_0),
        .I2(output_data_0_s_reg),
        .I3(output_data_1_s_reg),
        .I4(output_data_2_s_reg),
        .I5(pc_s_reg[31]),
        .O(\pc_s_reg[31]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFB380B380B380)) 
    \pc_s_reg[3]_i_1 
       (.I0(temp_signal),
        .I1(pc_s_n_0),
        .I2(instruction_address_reg[3]),
        .I3(pc_s_reg[3]),
        .I4(data1[3]),
        .I5(\pc_s_reg[7]_i_2_n_0 ),
        .O(pc_s__0[3]));
  LUT6 #(
    .INIT(64'hFFFFB380B380B380)) 
    \pc_s_reg[4]_i_1 
       (.I0(temp_signal),
        .I1(pc_s_n_0),
        .I2(instruction_address_reg[4]),
        .I3(pc_s_reg[4]),
        .I4(data1[4]),
        .I5(\pc_s_reg[7]_i_2_n_0 ),
        .O(pc_s__0[4]));
  LUT6 #(
    .INIT(64'hFFFFB380B380B380)) 
    \pc_s_reg[5]_i_1 
       (.I0(temp_signal),
        .I1(pc_s_n_0),
        .I2(instruction_address_reg[5]),
        .I3(pc_s_reg[5]),
        .I4(data1[5]),
        .I5(\pc_s_reg[7]_i_2_n_0 ),
        .O(pc_s__0[5]));
  LUT6 #(
    .INIT(64'hFFFFB380B380B380)) 
    \pc_s_reg[6]_i_1 
       (.I0(temp_signal),
        .I1(pc_s_n_0),
        .I2(instruction_address_reg[6]),
        .I3(pc_s_reg[6]),
        .I4(data1[6]),
        .I5(\pc_s_reg[7]_i_2_n_0 ),
        .O(pc_s__0[6]));
  LUT6 #(
    .INIT(64'hFFFFB380B380B380)) 
    \pc_s_reg[7]_i_1 
       (.I0(temp_signal),
        .I1(pc_s_n_0),
        .I2(instruction_address_reg[7]),
        .I3(pc_s_reg[7]),
        .I4(data1[7]),
        .I5(\pc_s_reg[7]_i_2_n_0 ),
        .O(pc_s__0[7]));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000000)) 
    \pc_s_reg[7]_i_2 
       (.I0(output_data_4_s_reg),
        .I1(output_data_3_s_reg),
        .I2(output_data_2_s_reg),
        .I3(output_data_0_s_reg),
        .I4(output_data_1_s_reg),
        .I5(pc_s_n_0),
        .O(\pc_s_reg[7]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc_s_reg[8]_i_1 
       (.I0(\pc_s_reg[28]_i_3_n_0 ),
        .I1(instruction_address_reg[8]),
        .I2(\pc_s_reg[14]_i_3_n_0 ),
        .I3(pc_s_reg[8]),
        .I4(data1[8]),
        .I5(\pc_s_reg[14]_i_2_n_0 ),
        .O(pc_s__0[8]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    \pc_s_reg[9]_i_1 
       (.I0(\pc_s_reg[28]_i_3_n_0 ),
        .I1(instruction_address_reg[9]),
        .I2(\pc_s_reg[14]_i_3_n_0 ),
        .I3(pc_s_reg[9]),
        .I4(data1[9]),
        .I5(\pc_s_reg[14]_i_2_n_0 ),
        .O(pc_s__0[9]));
  FDRE \pc_s_reg_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[0]),
        .Q(pc_s_reg[0]),
        .R(reset));
  FDRE \pc_s_reg_reg[10] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[10]),
        .Q(pc_s_reg[10]),
        .R(reset));
  FDRE \pc_s_reg_reg[11] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[11]),
        .Q(pc_s_reg[11]),
        .R(reset));
  FDRE \pc_s_reg_reg[12] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[12]),
        .Q(pc_s_reg[12]),
        .R(reset));
  FDRE \pc_s_reg_reg[13] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[13]),
        .Q(pc_s_reg[13]),
        .R(reset));
  FDRE \pc_s_reg_reg[14] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0__0),
        .Q(pc_s_reg[14]),
        .R(reset));
  FDRE \pc_s_reg_reg[15] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[15]),
        .Q(pc_s_reg[15]),
        .R(reset));
  FDRE \pc_s_reg_reg[16] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[16]),
        .Q(pc_s_reg[16]),
        .R(reset));
  FDRE \pc_s_reg_reg[17] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[17]),
        .Q(pc_s_reg[17]),
        .R(reset));
  FDRE \pc_s_reg_reg[18] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[18]),
        .Q(pc_s_reg[18]),
        .R(reset));
  FDRE \pc_s_reg_reg[19] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[19]),
        .Q(pc_s_reg[19]),
        .R(reset));
  FDRE \pc_s_reg_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[1]),
        .Q(pc_s_reg[1]),
        .R(reset));
  FDRE \pc_s_reg_reg[20] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[20]),
        .Q(pc_s_reg[20]),
        .R(reset));
  FDRE \pc_s_reg_reg[21] 
       (.C(clk),
        .CE(1'b1),
        .D(D[0]),
        .Q(pc_s_reg[21]),
        .R(reset));
  FDRE \pc_s_reg_reg[22] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[22]),
        .Q(pc_s_reg[22]),
        .R(reset));
  FDRE \pc_s_reg_reg[23] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[23]),
        .Q(pc_s_reg[23]),
        .R(reset));
  FDRE \pc_s_reg_reg[24] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[24]),
        .Q(pc_s_reg[24]),
        .R(reset));
  FDRE \pc_s_reg_reg[25] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[25]),
        .Q(pc_s_reg[25]),
        .R(reset));
  FDRE \pc_s_reg_reg[26] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[26]),
        .Q(pc_s_reg[26]),
        .R(reset));
  FDRE \pc_s_reg_reg[27] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[27]),
        .Q(pc_s_reg[27]),
        .R(reset));
  FDRE \pc_s_reg_reg[28] 
       (.C(clk),
        .CE(1'b1),
        .D(D[1]),
        .Q(pc_s_reg[28]),
        .R(reset));
  FDRE \pc_s_reg_reg[29] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[29]),
        .Q(pc_s_reg[29]),
        .R(reset));
  FDRE \pc_s_reg_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[2]),
        .Q(pc_s_reg[2]),
        .R(reset));
  FDRE \pc_s_reg_reg[30] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[30]),
        .Q(pc_s_reg[30]),
        .R(reset));
  FDRE \pc_s_reg_reg[31] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[31]),
        .Q(pc_s_reg[31]),
        .R(reset));
  FDRE \pc_s_reg_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[3]),
        .Q(pc_s_reg[3]),
        .R(reset));
  FDRE \pc_s_reg_reg[4] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[4]),
        .Q(pc_s_reg[4]),
        .R(reset));
  FDRE \pc_s_reg_reg[5] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[5]),
        .Q(pc_s_reg[5]),
        .R(reset));
  FDRE \pc_s_reg_reg[6] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[6]),
        .Q(pc_s_reg[6]),
        .R(reset));
  FDRE \pc_s_reg_reg[7] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[7]),
        .Q(pc_s_reg[7]),
        .R(reset));
  FDRE \pc_s_reg_reg[8] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[8]),
        .Q(pc_s_reg[8]),
        .R(reset));
  FDRE \pc_s_reg_reg[9] 
       (.C(clk),
        .CE(1'b1),
        .D(pc_s__0[9]),
        .Q(pc_s_reg[9]),
        .R(reset));
  LUT3 #(
    .INIT(8'hF8)) 
    w_en_i_1
       (.I0(enable_reg_reg_reg_0),
        .I1(\pc_reg[0]_0 ),
        .I2(temp_signal),
        .O(w_en_i_1_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    w_en_i_1__0
       (.I0(out_en_b_i),
        .I1(out_en_reg),
        .O(out_en_s));
  FDRE w_en_reg
       (.C(clk),
        .CE(1'b1),
        .D(w_en_i_1_n_0),
        .Q(out_en_b_i),
        .R(reset));
endmodule

(* ORIG_REF_NAME = "decode_i_sync" *) 
module design_1_decodeur_traces_1_1_decode_i_sync
   (out,
    Q,
    \FSM_onehot_state_reg_reg[9] ,
    D,
    \output_data_1_reg[6]_0 ,
    \output_data_3_reg[6]_0 ,
    E,
    \output_data_3_reg[7]_0 ,
    \state_reg_reg[0] ,
    \state_reg_reg[2] ,
    \FSM_onehot_state_reg_reg[1]_0 ,
    \FSM_onehot_state_reg_reg[1]_1 ,
    \state_reg_reg[0]_0 ,
    \state_reg_reg[0]_1 ,
    \state_reg_reg[1] ,
    \FSM_onehot_state_reg_reg[0]_0 ,
    \state_reg_reg[0]_2 ,
    \state_reg_reg[1]_0 ,
    \FSM_onehot_state_reg_reg[6]_0 ,
    \output_data_2_reg[6]_0 ,
    \output_data_3_reg[6]_1 ,
    \instruction_address_reg_reg[31] ,
    \context_id_reg[31] ,
    enable_reg_reg_reg,
    \FSM_onehot_state_reg_reg[6]_1 ,
    \FSM_onehot_state_reg_reg[3]_0 ,
    \pc_reg[28] ,
    \waypoint_address_cs_reg_reg[1] ,
    \waypoint_address_reg_reg[28] ,
    \FSM_onehot_state_reg_reg[0]_1 ,
    \state_reg_reg[1]_1 ,
    \state_reg_reg[1]_2 ,
    \FSM_onehot_state_reg_reg[13] ,
    enable_reg_reg_reg_0,
    \state_reg_reg[1]_3 ,
    \state_reg_reg[0]_3 ,
    \state_reg_reg[2]_0 ,
    \FSM_onehot_state_reg_reg[1]_2 ,
    \FSM_onehot_state_reg_reg[2]_0 ,
    \FSM_onehot_state_reg_reg[7]_0 ,
    \state_reg_reg[3] ,
    \state_reg_reg[1]_4 ,
    \state_reg_reg[2]_1 ,
    \state_reg_reg[1]_5 ,
    enable_reg_reg_reg_1,
    \state_reg_reg[0]_4 ,
    \FSM_onehot_state_reg_reg[9]_0 ,
    \state_reg_reg[2]_2 ,
    \pc_s_reg_reg[28] ,
    reset,
    clk,
    \data_in_reg_reg[7] );
  output [0:0]out;
  output [7:0]Q;
  output [2:0]\FSM_onehot_state_reg_reg[9] ;
  output [0:0]D;
  output \output_data_1_reg[6]_0 ;
  output [0:0]\output_data_3_reg[6]_0 ;
  output [0:0]E;
  output [0:0]\output_data_3_reg[7]_0 ;
  output \state_reg_reg[0] ;
  output [0:0]\state_reg_reg[2] ;
  output \FSM_onehot_state_reg_reg[1]_0 ;
  output \FSM_onehot_state_reg_reg[1]_1 ;
  output \state_reg_reg[0]_0 ;
  output \state_reg_reg[0]_1 ;
  output \state_reg_reg[1] ;
  output \FSM_onehot_state_reg_reg[0]_0 ;
  output \state_reg_reg[0]_2 ;
  output \state_reg_reg[1]_0 ;
  output \FSM_onehot_state_reg_reg[6]_0 ;
  output [0:0]\output_data_2_reg[6]_0 ;
  output [0:0]\output_data_3_reg[6]_1 ;
  output [31:0]\instruction_address_reg_reg[31] ;
  output [31:0]\context_id_reg[31] ;
  input enable_reg_reg_reg;
  input [3:0]\FSM_onehot_state_reg_reg[6]_1 ;
  input [1:0]\FSM_onehot_state_reg_reg[3]_0 ;
  input [0:0]\pc_reg[28] ;
  input \waypoint_address_cs_reg_reg[1] ;
  input \waypoint_address_reg_reg[28] ;
  input \FSM_onehot_state_reg_reg[0]_1 ;
  input \state_reg_reg[1]_1 ;
  input \state_reg_reg[1]_2 ;
  input \FSM_onehot_state_reg_reg[13] ;
  input enable_reg_reg_reg_0;
  input \state_reg_reg[1]_3 ;
  input \state_reg_reg[0]_3 ;
  input \state_reg_reg[2]_0 ;
  input \FSM_onehot_state_reg_reg[1]_2 ;
  input \FSM_onehot_state_reg_reg[2]_0 ;
  input \FSM_onehot_state_reg_reg[7]_0 ;
  input \state_reg_reg[3] ;
  input \state_reg_reg[1]_4 ;
  input \state_reg_reg[2]_1 ;
  input \state_reg_reg[1]_5 ;
  input enable_reg_reg_reg_1;
  input \state_reg_reg[0]_4 ;
  input \FSM_onehot_state_reg_reg[9]_0 ;
  input [0:0]\state_reg_reg[2]_2 ;
  input [1:0]\pc_s_reg_reg[28] ;
  input reset;
  input clk;
  input [7:0]\data_in_reg_reg[7] ;

  wire [0:0]D;
  wire [0:0]E;
  wire \FSM_onehot_state_reg[0]_i_1_n_0 ;
  wire \FSM_onehot_state_reg[0]_i_2_n_0 ;
  wire \FSM_onehot_state_reg[0]_i_4__1_n_0 ;
  wire \FSM_onehot_state_reg[0]_i_5__1_n_0 ;
  wire \FSM_onehot_state_reg[0]_i_7_n_0 ;
  wire \FSM_onehot_state_reg[1]_i_1__1_n_0 ;
  wire \FSM_onehot_state_reg[1]_i_2__1_n_0 ;
  wire \FSM_onehot_state_reg[1]_i_3__1_n_0 ;
  wire \FSM_onehot_state_reg[1]_i_4_n_0 ;
  wire \FSM_onehot_state_reg[1]_i_5__0_n_0 ;
  wire \FSM_onehot_state_reg[1]_i_9__0_n_0 ;
  wire \FSM_onehot_state_reg[2]_i_3_n_0 ;
  wire \FSM_onehot_state_reg[2]_i_6_n_0 ;
  wire \FSM_onehot_state_reg[6]_i_1__1_n_0 ;
  wire \FSM_onehot_state_reg[7]_i_1_n_0 ;
  wire \FSM_onehot_state_reg[8]_i_1_n_0 ;
  wire \FSM_onehot_state_reg_reg[0]_0 ;
  wire \FSM_onehot_state_reg_reg[0]_1 ;
  wire \FSM_onehot_state_reg_reg[13] ;
  wire \FSM_onehot_state_reg_reg[1]_0 ;
  wire \FSM_onehot_state_reg_reg[1]_1 ;
  wire \FSM_onehot_state_reg_reg[1]_2 ;
  wire \FSM_onehot_state_reg_reg[2]_0 ;
  wire [1:0]\FSM_onehot_state_reg_reg[3]_0 ;
  wire \FSM_onehot_state_reg_reg[6]_0 ;
  wire [3:0]\FSM_onehot_state_reg_reg[6]_1 ;
  wire \FSM_onehot_state_reg_reg[7]_0 ;
  wire [2:0]\FSM_onehot_state_reg_reg[9] ;
  wire \FSM_onehot_state_reg_reg[9]_0 ;
  (* RTL_KEEP = "yes" *) wire \FSM_onehot_state_reg_reg_n_0_[1] ;
  (* RTL_KEEP = "yes" *) wire \FSM_onehot_state_reg_reg_n_0_[2] ;
  (* RTL_KEEP = "yes" *) wire \FSM_onehot_state_reg_reg_n_0_[3] ;
  (* RTL_KEEP = "yes" *) wire \FSM_onehot_state_reg_reg_n_0_[4] ;
  (* RTL_KEEP = "yes" *) wire \FSM_onehot_state_reg_reg_n_0_[5] ;
  (* RTL_KEEP = "yes" *) wire \FSM_onehot_state_reg_reg_n_0_[6] ;
  (* RTL_KEEP = "yes" *) wire \FSM_onehot_state_reg_reg_n_0_[7] ;
  (* RTL_KEEP = "yes" *) wire \FSM_onehot_state_reg_reg_n_0_[8] ;
  wire [7:0]Q;
  wire clk;
  wire [31:0]\context_id_reg[31] ;
  wire [1:0]count_ctxt;
  wire \count_ctxt[0]_i_1_n_0 ;
  wire \count_ctxt[1]_i_1_n_0 ;
  wire [7:0]\data_in_reg_reg[7] ;
  wire en_0;
  wire en_1;
  wire en_2;
  wire en_4;
  wire en_5;
  wire en_6;
  wire enable_reg_reg_reg;
  wire enable_reg_reg_reg_0;
  wire enable_reg_reg_reg_1;
  wire [31:0]\instruction_address_reg_reg[31] ;
  (* RTL_KEEP = "yes" *) wire [0:0]out;
  wire \output_data_1_reg[6]_0 ;
  wire [0:0]\output_data_2_reg[6]_0 ;
  wire [0:0]\output_data_3_reg[6]_0 ;
  wire [0:0]\output_data_3_reg[6]_1 ;
  wire [0:0]\output_data_3_reg[7]_0 ;
  wire [0:0]\pc_reg[28] ;
  wire [1:0]\pc_s_reg_reg[28] ;
  wire reset;
  wire \state_reg[0]_i_8_n_0 ;
  wire \state_reg[0]_i_9_n_0 ;
  wire \state_reg[2]_i_2_n_0 ;
  wire \state_reg[2]_i_3_n_0 ;
  wire \state_reg[2]_i_5_n_0 ;
  wire \state_reg[2]_i_7_n_0 ;
  wire \state_reg_reg[0] ;
  wire \state_reg_reg[0]_0 ;
  wire \state_reg_reg[0]_1 ;
  wire \state_reg_reg[0]_2 ;
  wire \state_reg_reg[0]_3 ;
  wire \state_reg_reg[0]_4 ;
  wire \state_reg_reg[1] ;
  wire \state_reg_reg[1]_0 ;
  wire \state_reg_reg[1]_1 ;
  wire \state_reg_reg[1]_2 ;
  wire \state_reg_reg[1]_3 ;
  wire \state_reg_reg[1]_4 ;
  wire \state_reg_reg[1]_5 ;
  wire [0:0]\state_reg_reg[2] ;
  wire \state_reg_reg[2]_0 ;
  wire \state_reg_reg[2]_1 ;
  wire [0:0]\state_reg_reg[2]_2 ;
  wire \state_reg_reg[3] ;
  wire \waypoint_address_cs_reg_reg[1] ;
  wire \waypoint_address_reg_reg[28] ;

  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \FSM_onehot_state_reg[0]_i_1 
       (.I0(\FSM_onehot_state_reg[0]_i_2_n_0 ),
        .I1(\FSM_onehot_state_reg_reg[0]_1 ),
        .I2(\FSM_onehot_state_reg[0]_i_4__1_n_0 ),
        .I3(\FSM_onehot_state_reg[0]_i_5__1_n_0 ),
        .I4(\state_reg_reg[1]_1 ),
        .I5(E),
        .O(\FSM_onehot_state_reg[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h44F444F4FFFF44F4)) 
    \FSM_onehot_state_reg[0]_i_2 
       (.I0(\FSM_onehot_state_reg[0]_i_7_n_0 ),
        .I1(\FSM_onehot_state_reg_reg_n_0_[7] ),
        .I2(\state_reg_reg[1]_2 ),
        .I3(\FSM_onehot_state_reg_reg[13] ),
        .I4(out),
        .I5(enable_reg_reg_reg),
        .O(\FSM_onehot_state_reg[0]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \FSM_onehot_state_reg[0]_i_4__1 
       (.I0(\FSM_onehot_state_reg_reg_n_0_[6] ),
        .I1(enable_reg_reg_reg),
        .O(\FSM_onehot_state_reg[0]_i_4__1_n_0 ));
  LUT6 #(
    .INIT(64'h8888888888888088)) 
    \FSM_onehot_state_reg[0]_i_5__1 
       (.I0(out),
        .I1(\state_reg_reg[1]_5 ),
        .I2(Q[0]),
        .I3(Q[3]),
        .I4(\FSM_onehot_state_reg[1]_i_5__0_n_0 ),
        .I5(Q[7]),
        .O(\FSM_onehot_state_reg[0]_i_5__1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFEF)) 
    \FSM_onehot_state_reg[0]_i_6__1 
       (.I0(Q[0]),
        .I1(Q[3]),
        .I2(Q[4]),
        .I3(Q[2]),
        .I4(\state_reg[2]_i_7_n_0 ),
        .I5(Q[7]),
        .O(\FSM_onehot_state_reg_reg[0]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'hF7)) 
    \FSM_onehot_state_reg[0]_i_7 
       (.I0(enable_reg_reg_reg),
        .I1(count_ctxt[0]),
        .I2(count_ctxt[1]),
        .O(\FSM_onehot_state_reg[0]_i_7_n_0 ));
  LUT2 #(
    .INIT(4'hB)) 
    \FSM_onehot_state_reg[0]_i_8 
       (.I0(out),
        .I1(\state_reg_reg[2]_2 ),
        .O(\state_reg_reg[0]_2 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \FSM_onehot_state_reg[10]_i_1__0 
       (.I0(Q[7]),
        .I1(enable_reg_reg_reg),
        .I2(\FSM_onehot_state_reg_reg[3]_0 [1]),
        .O(D));
  LUT6 #(
    .INIT(64'hFFEAFFEAFFEAEAEA)) 
    \FSM_onehot_state_reg[1]_i_1 
       (.I0(enable_reg_reg_reg_0),
        .I1(\FSM_onehot_state_reg[1]_i_3__1_n_0 ),
        .I2(\state_reg_reg[1]_3 ),
        .I3(\FSM_onehot_state_reg[1]_i_4_n_0 ),
        .I4(\state_reg_reg[0]_3 ),
        .I5(\state_reg_reg[2]_0 ),
        .O(\FSM_onehot_state_reg_reg[9] [0]));
  LUT6 #(
    .INIT(64'h88F8FFFF88F88888)) 
    \FSM_onehot_state_reg[1]_i_1__1 
       (.I0(\FSM_onehot_state_reg[1]_i_2__1_n_0 ),
        .I1(\state_reg_reg[2]_1 ),
        .I2(out),
        .I3(\state_reg_reg[1]_5 ),
        .I4(enable_reg_reg_reg),
        .I5(\FSM_onehot_state_reg_reg_n_0_[1] ),
        .O(\FSM_onehot_state_reg[1]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT4 #(
    .INIT(16'h0400)) 
    \FSM_onehot_state_reg[1]_i_2__0 
       (.I0(\state_reg_reg[0]_0 ),
        .I1(enable_reg_reg_reg),
        .I2(Q[7]),
        .I3(\FSM_onehot_state_reg_reg[9]_0 ),
        .O(\FSM_onehot_state_reg_reg[1]_1 ));
  LUT6 #(
    .INIT(64'h0000000004000000)) 
    \FSM_onehot_state_reg[1]_i_2__1 
       (.I0(Q[0]),
        .I1(Q[3]),
        .I2(Q[7]),
        .I3(enable_reg_reg_reg),
        .I4(out),
        .I5(\FSM_onehot_state_reg[1]_i_5__0_n_0 ),
        .O(\FSM_onehot_state_reg[1]_i_2__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \FSM_onehot_state_reg[1]_i_3__1 
       (.I0(Q[7]),
        .I1(enable_reg_reg_reg),
        .I2(\FSM_onehot_state_reg_reg[6]_1 [0]),
        .O(\FSM_onehot_state_reg[1]_i_3__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT5 #(
    .INIT(32'h55400000)) 
    \FSM_onehot_state_reg[1]_i_4 
       (.I0(Q[7]),
        .I1(enable_reg_reg_reg),
        .I2(\FSM_onehot_state_reg_reg[6]_1 [0]),
        .I3(\FSM_onehot_state_reg_reg[1]_2 ),
        .I4(Q[0]),
        .O(\FSM_onehot_state_reg[1]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \FSM_onehot_state_reg[1]_i_5__0 
       (.I0(Q[1]),
        .I1(Q[5]),
        .I2(Q[6]),
        .I3(Q[4]),
        .I4(Q[2]),
        .O(\FSM_onehot_state_reg[1]_i_5__0_n_0 ));
  LUT6 #(
    .INIT(64'h0000010000000000)) 
    \FSM_onehot_state_reg[1]_i_6__0 
       (.I0(\FSM_onehot_state_reg[1]_i_9__0_n_0 ),
        .I1(Q[3]),
        .I2(Q[0]),
        .I3(enable_reg_reg_reg),
        .I4(Q[7]),
        .I5(\FSM_onehot_state_reg_reg[3]_0 [0]),
        .O(\FSM_onehot_state_reg_reg[1]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT5 #(
    .INIT(32'hDFFFFFFF)) 
    \FSM_onehot_state_reg[1]_i_9__0 
       (.I0(Q[4]),
        .I1(Q[2]),
        .I2(Q[1]),
        .I3(Q[5]),
        .I4(Q[6]),
        .O(\FSM_onehot_state_reg[1]_i_9__0_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFEEEAEEEAEEEA)) 
    \FSM_onehot_state_reg[2]_i_1 
       (.I0(\FSM_onehot_state_reg_reg[2]_0 ),
        .I1(\FSM_onehot_state_reg[2]_i_3_n_0 ),
        .I2(\state_reg_reg[0]_3 ),
        .I3(\state_reg_reg[2]_0 ),
        .I4(\FSM_onehot_state_reg[2]_i_6_n_0 ),
        .I5(\state_reg_reg[1]_3 ),
        .O(\FSM_onehot_state_reg_reg[9] [1]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT5 #(
    .INIT(32'h80808000)) 
    \FSM_onehot_state_reg[2]_i_3 
       (.I0(Q[0]),
        .I1(Q[7]),
        .I2(enable_reg_reg_reg),
        .I3(\FSM_onehot_state_reg_reg[6]_1 [0]),
        .I4(\FSM_onehot_state_reg_reg[1]_2 ),
        .O(\FSM_onehot_state_reg[2]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \FSM_onehot_state_reg[2]_i_6 
       (.I0(enable_reg_reg_reg),
        .I1(Q[7]),
        .I2(\FSM_onehot_state_reg_reg[6]_1 [0]),
        .O(\FSM_onehot_state_reg[2]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \FSM_onehot_state_reg[6]_i_1__0 
       (.I0(Q[6]),
        .I1(\FSM_onehot_state_reg_reg[6]_1 [2]),
        .O(\FSM_onehot_state_reg_reg[6]_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \FSM_onehot_state_reg[6]_i_1__1 
       (.I0(\FSM_onehot_state_reg_reg_n_0_[6] ),
        .I1(enable_reg_reg_reg),
        .O(\FSM_onehot_state_reg[6]_i_1__1_n_0 ));
  LUT4 #(
    .INIT(16'hBF00)) 
    \FSM_onehot_state_reg[7]_i_1 
       (.I0(count_ctxt[1]),
        .I1(count_ctxt[0]),
        .I2(enable_reg_reg_reg),
        .I3(\FSM_onehot_state_reg_reg_n_0_[7] ),
        .O(\FSM_onehot_state_reg[7]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFAA2AAA)) 
    \FSM_onehot_state_reg[8]_i_1 
       (.I0(\FSM_onehot_state_reg_reg_n_0_[8] ),
        .I1(count_ctxt[0]),
        .I2(count_ctxt[1]),
        .I3(enable_reg_reg_reg),
        .I4(\FSM_onehot_state_reg_reg_n_0_[5] ),
        .O(\FSM_onehot_state_reg[8]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \FSM_onehot_state_reg[9]_i_1__0 
       (.I0(Q[7]),
        .I1(enable_reg_reg_reg),
        .I2(\FSM_onehot_state_reg_reg[6]_1 [3]),
        .O(\FSM_onehot_state_reg_reg[9] [2]));
  (* FSM_ENCODED_STATES = "i_sync_count_03:000001000,i_sync_count_04:000010000,i_sync_count_02:000000100,i_sync_count_01:000000010,wait_state:000000001,ctxtid_2:010000000,ctxtid_1:001000000,ctxtid_3:100000000,i_sync_ib:000100000" *) 
  (* KEEP = "yes" *) 
  FDSE #(
    .INIT(1'b1)) 
    \FSM_onehot_state_reg_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(\FSM_onehot_state_reg[0]_i_1_n_0 ),
        .Q(out),
        .S(reset));
  (* FSM_ENCODED_STATES = "i_sync_count_03:000001000,i_sync_count_04:000010000,i_sync_count_02:000000100,i_sync_count_01:000000010,wait_state:000000001,ctxtid_2:010000000,ctxtid_1:001000000,ctxtid_3:100000000,i_sync_ib:000100000" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\FSM_onehot_state_reg[1]_i_1__1_n_0 ),
        .Q(\FSM_onehot_state_reg_reg_n_0_[1] ),
        .R(reset));
  (* FSM_ENCODED_STATES = "i_sync_count_03:000001000,i_sync_count_04:000010000,i_sync_count_02:000000100,i_sync_count_01:000000010,wait_state:000000001,ctxtid_2:010000000,ctxtid_1:001000000,ctxtid_3:100000000,i_sync_ib:000100000" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg_reg[2] 
       (.C(clk),
        .CE(enable_reg_reg_reg),
        .D(\FSM_onehot_state_reg_reg_n_0_[1] ),
        .Q(\FSM_onehot_state_reg_reg_n_0_[2] ),
        .R(reset));
  (* FSM_ENCODED_STATES = "i_sync_count_03:000001000,i_sync_count_04:000010000,i_sync_count_02:000000100,i_sync_count_01:000000010,wait_state:000000001,ctxtid_2:010000000,ctxtid_1:001000000,ctxtid_3:100000000,i_sync_ib:000100000" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg_reg[3] 
       (.C(clk),
        .CE(enable_reg_reg_reg),
        .D(\FSM_onehot_state_reg_reg_n_0_[2] ),
        .Q(\FSM_onehot_state_reg_reg_n_0_[3] ),
        .R(reset));
  (* FSM_ENCODED_STATES = "i_sync_count_03:000001000,i_sync_count_04:000010000,i_sync_count_02:000000100,i_sync_count_01:000000010,wait_state:000000001,ctxtid_2:010000000,ctxtid_1:001000000,ctxtid_3:100000000,i_sync_ib:000100000" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg_reg[4] 
       (.C(clk),
        .CE(enable_reg_reg_reg),
        .D(\FSM_onehot_state_reg_reg_n_0_[3] ),
        .Q(\FSM_onehot_state_reg_reg_n_0_[4] ),
        .R(reset));
  (* FSM_ENCODED_STATES = "i_sync_count_03:000001000,i_sync_count_04:000010000,i_sync_count_02:000000100,i_sync_count_01:000000010,wait_state:000000001,ctxtid_2:010000000,ctxtid_1:001000000,ctxtid_3:100000000,i_sync_ib:000100000" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg_reg[5] 
       (.C(clk),
        .CE(enable_reg_reg_reg),
        .D(\FSM_onehot_state_reg_reg_n_0_[4] ),
        .Q(\FSM_onehot_state_reg_reg_n_0_[5] ),
        .R(reset));
  (* FSM_ENCODED_STATES = "i_sync_count_03:000001000,i_sync_count_04:000010000,i_sync_count_02:000000100,i_sync_count_01:000000010,wait_state:000000001,ctxtid_2:010000000,ctxtid_1:001000000,ctxtid_3:100000000,i_sync_ib:000100000" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg_reg[6] 
       (.C(clk),
        .CE(1'b1),
        .D(\FSM_onehot_state_reg[6]_i_1__1_n_0 ),
        .Q(\FSM_onehot_state_reg_reg_n_0_[6] ),
        .R(reset));
  (* FSM_ENCODED_STATES = "i_sync_count_03:000001000,i_sync_count_04:000010000,i_sync_count_02:000000100,i_sync_count_01:000000010,wait_state:000000001,ctxtid_2:010000000,ctxtid_1:001000000,ctxtid_3:100000000,i_sync_ib:000100000" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg_reg[7] 
       (.C(clk),
        .CE(1'b1),
        .D(\FSM_onehot_state_reg[7]_i_1_n_0 ),
        .Q(\FSM_onehot_state_reg_reg_n_0_[7] ),
        .R(reset));
  (* FSM_ENCODED_STATES = "i_sync_count_03:000001000,i_sync_count_04:000010000,i_sync_count_02:000000100,i_sync_count_01:000000010,wait_state:000000001,ctxtid_2:010000000,ctxtid_1:001000000,ctxtid_3:100000000,i_sync_ib:000100000" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg_reg[8] 
       (.C(clk),
        .CE(1'b1),
        .D(\FSM_onehot_state_reg[8]_i_1_n_0 ),
        .Q(\FSM_onehot_state_reg_reg_n_0_[8] ),
        .R(reset));
  LUT4 #(
    .INIT(16'h37C8)) 
    \count_ctxt[0]_i_1 
       (.I0(\FSM_onehot_state_reg_reg_n_0_[8] ),
        .I1(enable_reg_reg_reg),
        .I2(\FSM_onehot_state_reg_reg_n_0_[7] ),
        .I3(count_ctxt[0]),
        .O(\count_ctxt[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h5F7FA080)) 
    \count_ctxt[1]_i_1 
       (.I0(count_ctxt[0]),
        .I1(\FSM_onehot_state_reg_reg_n_0_[7] ),
        .I2(enable_reg_reg_reg),
        .I3(\FSM_onehot_state_reg_reg_n_0_[8] ),
        .I4(count_ctxt[1]),
        .O(\count_ctxt[1]_i_1_n_0 ));
  FDRE \count_ctxt_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(\count_ctxt[0]_i_1_n_0 ),
        .Q(count_ctxt[0]),
        .R(reset));
  FDRE \count_ctxt_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\count_ctxt[1]_i_1_n_0 ),
        .Q(count_ctxt[1]),
        .R(reset));
  FDRE \data_reg_s_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(\data_in_reg_reg[7] [0]),
        .Q(Q[0]),
        .R(reset));
  FDRE \data_reg_s_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\data_in_reg_reg[7] [1]),
        .Q(Q[1]),
        .R(reset));
  FDRE \data_reg_s_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(\data_in_reg_reg[7] [2]),
        .Q(Q[2]),
        .R(reset));
  FDRE \data_reg_s_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(\data_in_reg_reg[7] [3]),
        .Q(Q[3]),
        .R(reset));
  FDRE \data_reg_s_reg[4] 
       (.C(clk),
        .CE(1'b1),
        .D(\data_in_reg_reg[7] [4]),
        .Q(Q[4]),
        .R(reset));
  FDRE \data_reg_s_reg[5] 
       (.C(clk),
        .CE(1'b1),
        .D(\data_in_reg_reg[7] [5]),
        .Q(Q[5]),
        .R(reset));
  FDRE \data_reg_s_reg[6] 
       (.C(clk),
        .CE(1'b1),
        .D(\data_in_reg_reg[7] [6]),
        .Q(Q[6]),
        .R(reset));
  FDRE \data_reg_s_reg[7] 
       (.C(clk),
        .CE(1'b1),
        .D(\data_in_reg_reg[7] [7]),
        .Q(Q[7]),
        .R(reset));
  LUT2 #(
    .INIT(4'h8)) 
    enable_i_reg_i_1
       (.I0(\FSM_onehot_state_reg_reg_n_0_[4] ),
        .I1(enable_reg_reg_reg),
        .O(\output_data_3_reg[7]_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \output_data_0[7]_i_1 
       (.I0(\FSM_onehot_state_reg_reg_n_0_[1] ),
        .I1(enable_reg_reg_reg),
        .O(en_0));
  FDRE \output_data_0_reg[0] 
       (.C(clk),
        .CE(en_0),
        .D(Q[0]),
        .Q(\instruction_address_reg_reg[31] [0]),
        .R(reset));
  FDRE \output_data_0_reg[1] 
       (.C(clk),
        .CE(en_0),
        .D(Q[1]),
        .Q(\instruction_address_reg_reg[31] [1]),
        .R(reset));
  FDRE \output_data_0_reg[2] 
       (.C(clk),
        .CE(en_0),
        .D(Q[2]),
        .Q(\instruction_address_reg_reg[31] [2]),
        .R(reset));
  FDRE \output_data_0_reg[3] 
       (.C(clk),
        .CE(en_0),
        .D(Q[3]),
        .Q(\instruction_address_reg_reg[31] [3]),
        .R(reset));
  FDRE \output_data_0_reg[4] 
       (.C(clk),
        .CE(en_0),
        .D(Q[4]),
        .Q(\instruction_address_reg_reg[31] [4]),
        .R(reset));
  FDRE \output_data_0_reg[5] 
       (.C(clk),
        .CE(en_0),
        .D(Q[5]),
        .Q(\instruction_address_reg_reg[31] [5]),
        .R(reset));
  FDRE \output_data_0_reg[6] 
       (.C(clk),
        .CE(en_0),
        .D(Q[6]),
        .Q(\instruction_address_reg_reg[31] [6]),
        .R(reset));
  FDRE \output_data_0_reg[7] 
       (.C(clk),
        .CE(en_0),
        .D(Q[7]),
        .Q(\instruction_address_reg_reg[31] [7]),
        .R(reset));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'hBF)) 
    \output_data_1[6]_i_5 
       (.I0(Q[7]),
        .I1(enable_reg_reg_reg),
        .I2(\FSM_onehot_state_reg_reg[6]_1 [1]),
        .O(\output_data_1_reg[6]_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \output_data_1[7]_i_1 
       (.I0(\FSM_onehot_state_reg_reg_n_0_[2] ),
        .I1(enable_reg_reg_reg),
        .O(en_1));
  FDRE \output_data_1_reg[0] 
       (.C(clk),
        .CE(en_1),
        .D(Q[0]),
        .Q(\instruction_address_reg_reg[31] [8]),
        .R(reset));
  FDRE \output_data_1_reg[1] 
       (.C(clk),
        .CE(en_1),
        .D(Q[1]),
        .Q(\instruction_address_reg_reg[31] [9]),
        .R(reset));
  FDRE \output_data_1_reg[2] 
       (.C(clk),
        .CE(en_1),
        .D(Q[2]),
        .Q(\instruction_address_reg_reg[31] [10]),
        .R(reset));
  FDRE \output_data_1_reg[3] 
       (.C(clk),
        .CE(en_1),
        .D(Q[3]),
        .Q(\instruction_address_reg_reg[31] [11]),
        .R(reset));
  FDRE \output_data_1_reg[4] 
       (.C(clk),
        .CE(en_1),
        .D(Q[4]),
        .Q(\instruction_address_reg_reg[31] [12]),
        .R(reset));
  FDRE \output_data_1_reg[5] 
       (.C(clk),
        .CE(en_1),
        .D(Q[5]),
        .Q(\instruction_address_reg_reg[31] [13]),
        .R(reset));
  FDRE \output_data_1_reg[6] 
       (.C(clk),
        .CE(en_1),
        .D(Q[6]),
        .Q(\instruction_address_reg_reg[31] [14]),
        .R(reset));
  FDRE \output_data_1_reg[7] 
       (.C(clk),
        .CE(en_1),
        .D(Q[7]),
        .Q(\instruction_address_reg_reg[31] [15]),
        .R(reset));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \output_data_2[6]_i_2 
       (.I0(Q[6]),
        .I1(Q[7]),
        .I2(\pc_s_reg_reg[28] [0]),
        .O(\output_data_2_reg[6]_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \output_data_2[7]_i_1 
       (.I0(\FSM_onehot_state_reg_reg_n_0_[3] ),
        .I1(enable_reg_reg_reg),
        .O(en_2));
  FDRE \output_data_2_reg[0] 
       (.C(clk),
        .CE(en_2),
        .D(Q[0]),
        .Q(\instruction_address_reg_reg[31] [16]),
        .R(reset));
  FDRE \output_data_2_reg[1] 
       (.C(clk),
        .CE(en_2),
        .D(Q[1]),
        .Q(\instruction_address_reg_reg[31] [17]),
        .R(reset));
  FDRE \output_data_2_reg[2] 
       (.C(clk),
        .CE(en_2),
        .D(Q[2]),
        .Q(\instruction_address_reg_reg[31] [18]),
        .R(reset));
  FDRE \output_data_2_reg[3] 
       (.C(clk),
        .CE(en_2),
        .D(Q[3]),
        .Q(\instruction_address_reg_reg[31] [19]),
        .R(reset));
  FDRE \output_data_2_reg[4] 
       (.C(clk),
        .CE(en_2),
        .D(Q[4]),
        .Q(\instruction_address_reg_reg[31] [20]),
        .R(reset));
  FDRE \output_data_2_reg[5] 
       (.C(clk),
        .CE(en_2),
        .D(Q[5]),
        .Q(\instruction_address_reg_reg[31] [21]),
        .R(reset));
  FDRE \output_data_2_reg[6] 
       (.C(clk),
        .CE(en_2),
        .D(Q[6]),
        .Q(\instruction_address_reg_reg[31] [22]),
        .R(reset));
  FDRE \output_data_2_reg[7] 
       (.C(clk),
        .CE(en_2),
        .D(Q[7]),
        .Q(\instruction_address_reg_reg[31] [23]),
        .R(reset));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \output_data_3[6]_i_2 
       (.I0(Q[6]),
        .I1(Q[7]),
        .I2(\pc_s_reg_reg[28] [1]),
        .O(\output_data_3_reg[6]_1 ));
  LUT6 #(
    .INIT(64'hF2F2F2F2F2D0D0D0)) 
    \output_data_3[6]_i_2__0 
       (.I0(enable_reg_reg_reg),
        .I1(Q[7]),
        .I2(Q[6]),
        .I3(\pc_reg[28] ),
        .I4(\waypoint_address_cs_reg_reg[1] ),
        .I5(\waypoint_address_reg_reg[28] ),
        .O(\output_data_3_reg[6]_0 ));
  FDRE \output_data_3_reg[0] 
       (.C(clk),
        .CE(\output_data_3_reg[7]_0 ),
        .D(Q[0]),
        .Q(\instruction_address_reg_reg[31] [24]),
        .R(reset));
  FDRE \output_data_3_reg[1] 
       (.C(clk),
        .CE(\output_data_3_reg[7]_0 ),
        .D(Q[1]),
        .Q(\instruction_address_reg_reg[31] [25]),
        .R(reset));
  FDRE \output_data_3_reg[2] 
       (.C(clk),
        .CE(\output_data_3_reg[7]_0 ),
        .D(Q[2]),
        .Q(\instruction_address_reg_reg[31] [26]),
        .R(reset));
  FDRE \output_data_3_reg[3] 
       (.C(clk),
        .CE(\output_data_3_reg[7]_0 ),
        .D(Q[3]),
        .Q(\instruction_address_reg_reg[31] [27]),
        .R(reset));
  FDRE \output_data_3_reg[4] 
       (.C(clk),
        .CE(\output_data_3_reg[7]_0 ),
        .D(Q[4]),
        .Q(\instruction_address_reg_reg[31] [28]),
        .R(reset));
  FDRE \output_data_3_reg[5] 
       (.C(clk),
        .CE(\output_data_3_reg[7]_0 ),
        .D(Q[5]),
        .Q(\instruction_address_reg_reg[31] [29]),
        .R(reset));
  FDRE \output_data_3_reg[6] 
       (.C(clk),
        .CE(\output_data_3_reg[7]_0 ),
        .D(Q[6]),
        .Q(\instruction_address_reg_reg[31] [30]),
        .R(reset));
  FDRE \output_data_3_reg[7] 
       (.C(clk),
        .CE(\output_data_3_reg[7]_0 ),
        .D(Q[7]),
        .Q(\instruction_address_reg_reg[31] [31]),
        .R(reset));
  LUT5 #(
    .INIT(32'hAB00AA00)) 
    \output_data_4[7]_i_1 
       (.I0(\FSM_onehot_state_reg_reg_n_0_[6] ),
        .I1(count_ctxt[0]),
        .I2(count_ctxt[1]),
        .I3(enable_reg_reg_reg),
        .I4(\FSM_onehot_state_reg_reg_n_0_[8] ),
        .O(en_4));
  FDRE \output_data_4_reg[0] 
       (.C(clk),
        .CE(en_4),
        .D(Q[0]),
        .Q(\context_id_reg[31] [0]),
        .R(reset));
  FDRE \output_data_4_reg[1] 
       (.C(clk),
        .CE(en_4),
        .D(Q[1]),
        .Q(\context_id_reg[31] [1]),
        .R(reset));
  FDRE \output_data_4_reg[2] 
       (.C(clk),
        .CE(en_4),
        .D(Q[2]),
        .Q(\context_id_reg[31] [2]),
        .R(reset));
  FDRE \output_data_4_reg[3] 
       (.C(clk),
        .CE(en_4),
        .D(Q[3]),
        .Q(\context_id_reg[31] [3]),
        .R(reset));
  FDRE \output_data_4_reg[4] 
       (.C(clk),
        .CE(en_4),
        .D(Q[4]),
        .Q(\context_id_reg[31] [4]),
        .R(reset));
  FDRE \output_data_4_reg[5] 
       (.C(clk),
        .CE(en_4),
        .D(Q[5]),
        .Q(\context_id_reg[31] [5]),
        .R(reset));
  FDRE \output_data_4_reg[6] 
       (.C(clk),
        .CE(en_4),
        .D(Q[6]),
        .Q(\context_id_reg[31] [6]),
        .R(reset));
  FDRE \output_data_4_reg[7] 
       (.C(clk),
        .CE(en_4),
        .D(Q[7]),
        .Q(\context_id_reg[31] [7]),
        .R(reset));
  LUT5 #(
    .INIT(32'h88C88888)) 
    \output_data_5[7]_i_1 
       (.I0(\FSM_onehot_state_reg_reg_n_0_[7] ),
        .I1(enable_reg_reg_reg),
        .I2(count_ctxt[0]),
        .I3(count_ctxt[1]),
        .I4(\FSM_onehot_state_reg_reg_n_0_[8] ),
        .O(en_5));
  FDRE \output_data_5_reg[0] 
       (.C(clk),
        .CE(en_5),
        .D(Q[0]),
        .Q(\context_id_reg[31] [8]),
        .R(reset));
  FDRE \output_data_5_reg[1] 
       (.C(clk),
        .CE(en_5),
        .D(Q[1]),
        .Q(\context_id_reg[31] [9]),
        .R(reset));
  FDRE \output_data_5_reg[2] 
       (.C(clk),
        .CE(en_5),
        .D(Q[2]),
        .Q(\context_id_reg[31] [10]),
        .R(reset));
  FDRE \output_data_5_reg[3] 
       (.C(clk),
        .CE(en_5),
        .D(Q[3]),
        .Q(\context_id_reg[31] [11]),
        .R(reset));
  FDRE \output_data_5_reg[4] 
       (.C(clk),
        .CE(en_5),
        .D(Q[4]),
        .Q(\context_id_reg[31] [12]),
        .R(reset));
  FDRE \output_data_5_reg[5] 
       (.C(clk),
        .CE(en_5),
        .D(Q[5]),
        .Q(\context_id_reg[31] [13]),
        .R(reset));
  FDRE \output_data_5_reg[6] 
       (.C(clk),
        .CE(en_5),
        .D(Q[6]),
        .Q(\context_id_reg[31] [14]),
        .R(reset));
  FDRE \output_data_5_reg[7] 
       (.C(clk),
        .CE(en_5),
        .D(Q[7]),
        .Q(\context_id_reg[31] [15]),
        .R(reset));
  LUT4 #(
    .INIT(16'h4000)) 
    \output_data_6[7]_i_1 
       (.I0(count_ctxt[0]),
        .I1(\FSM_onehot_state_reg_reg_n_0_[8] ),
        .I2(enable_reg_reg_reg),
        .I3(count_ctxt[1]),
        .O(en_6));
  FDRE \output_data_6_reg[0] 
       (.C(clk),
        .CE(en_6),
        .D(Q[0]),
        .Q(\context_id_reg[31] [16]),
        .R(reset));
  FDRE \output_data_6_reg[1] 
       (.C(clk),
        .CE(en_6),
        .D(Q[1]),
        .Q(\context_id_reg[31] [17]),
        .R(reset));
  FDRE \output_data_6_reg[2] 
       (.C(clk),
        .CE(en_6),
        .D(Q[2]),
        .Q(\context_id_reg[31] [18]),
        .R(reset));
  FDRE \output_data_6_reg[3] 
       (.C(clk),
        .CE(en_6),
        .D(Q[3]),
        .Q(\context_id_reg[31] [19]),
        .R(reset));
  FDRE \output_data_6_reg[4] 
       (.C(clk),
        .CE(en_6),
        .D(Q[4]),
        .Q(\context_id_reg[31] [20]),
        .R(reset));
  FDRE \output_data_6_reg[5] 
       (.C(clk),
        .CE(en_6),
        .D(Q[5]),
        .Q(\context_id_reg[31] [21]),
        .R(reset));
  FDRE \output_data_6_reg[6] 
       (.C(clk),
        .CE(en_6),
        .D(Q[6]),
        .Q(\context_id_reg[31] [22]),
        .R(reset));
  FDRE \output_data_6_reg[7] 
       (.C(clk),
        .CE(en_6),
        .D(Q[7]),
        .Q(\context_id_reg[31] [23]),
        .R(reset));
  FDRE \output_data_7_reg[0] 
       (.C(clk),
        .CE(E),
        .D(Q[0]),
        .Q(\context_id_reg[31] [24]),
        .R(reset));
  FDRE \output_data_7_reg[1] 
       (.C(clk),
        .CE(E),
        .D(Q[1]),
        .Q(\context_id_reg[31] [25]),
        .R(reset));
  FDRE \output_data_7_reg[2] 
       (.C(clk),
        .CE(E),
        .D(Q[2]),
        .Q(\context_id_reg[31] [26]),
        .R(reset));
  FDRE \output_data_7_reg[3] 
       (.C(clk),
        .CE(E),
        .D(Q[3]),
        .Q(\context_id_reg[31] [27]),
        .R(reset));
  FDRE \output_data_7_reg[4] 
       (.C(clk),
        .CE(E),
        .D(Q[4]),
        .Q(\context_id_reg[31] [28]),
        .R(reset));
  FDRE \output_data_7_reg[5] 
       (.C(clk),
        .CE(E),
        .D(Q[5]),
        .Q(\context_id_reg[31] [29]),
        .R(reset));
  FDRE \output_data_7_reg[6] 
       (.C(clk),
        .CE(E),
        .D(Q[6]),
        .Q(\context_id_reg[31] [30]),
        .R(reset));
  FDRE \output_data_7_reg[7] 
       (.C(clk),
        .CE(E),
        .D(Q[7]),
        .Q(\context_id_reg[31] [31]),
        .R(reset));
  LUT5 #(
    .INIT(32'hFFFFFFEF)) 
    \state_reg[0]_i_3 
       (.I0(\state_reg[2]_i_7_n_0 ),
        .I1(Q[2]),
        .I2(Q[4]),
        .I3(Q[3]),
        .I4(Q[0]),
        .O(\state_reg_reg[0]_0 ));
  LUT6 #(
    .INIT(64'hFFFFFF00A800A800)) 
    \state_reg[0]_i_5 
       (.I0(enable_reg_reg_reg),
        .I1(\FSM_onehot_state_reg_reg[7]_0 ),
        .I2(\state_reg_reg[3] ),
        .I3(\state_reg[0]_i_8_n_0 ),
        .I4(\state_reg[0]_i_9_n_0 ),
        .I5(\state_reg_reg[1]_4 ),
        .O(\state_reg_reg[0] ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT5 #(
    .INIT(32'h00000010)) 
    \state_reg[0]_i_7 
       (.I0(Q[0]),
        .I1(Q[3]),
        .I2(enable_reg_reg_reg),
        .I3(Q[7]),
        .I4(\FSM_onehot_state_reg[1]_i_5__0_n_0 ),
        .O(\state_reg_reg[0]_1 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT4 #(
    .INIT(16'h5400)) 
    \state_reg[0]_i_8 
       (.I0(Q[0]),
        .I1(Q[3]),
        .I2(\FSM_onehot_state_reg[1]_i_5__0_n_0 ),
        .I3(Q[7]),
        .O(\state_reg[0]_i_8_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT5 #(
    .INIT(32'h0001FFFF)) 
    \state_reg[0]_i_9 
       (.I0(Q[0]),
        .I1(Q[3]),
        .I2(\FSM_onehot_state_reg[1]_i_5__0_n_0 ),
        .I3(Q[7]),
        .I4(enable_reg_reg_reg),
        .O(\state_reg[0]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'h0000000004000000)) 
    \state_reg[1]_i_2 
       (.I0(Q[0]),
        .I1(Q[3]),
        .I2(Q[4]),
        .I3(enable_reg_reg_reg),
        .I4(Q[2]),
        .I5(\state_reg[2]_i_7_n_0 ),
        .O(\state_reg_reg[1] ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT5 #(
    .INIT(32'h44400040)) 
    \state_reg[1]_i_3 
       (.I0(Q[0]),
        .I1(enable_reg_reg_reg),
        .I2(Q[3]),
        .I3(\FSM_onehot_state_reg[1]_i_5__0_n_0 ),
        .I4(Q[7]),
        .O(\state_reg_reg[1]_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEF0F0)) 
    \state_reg[2]_i_1 
       (.I0(\state_reg[2]_i_2_n_0 ),
        .I1(\state_reg[2]_i_3_n_0 ),
        .I2(enable_reg_reg_reg_1),
        .I3(\state_reg[2]_i_5_n_0 ),
        .I4(\state_reg_reg[2]_1 ),
        .I5(\state_reg_reg[0]_4 ),
        .O(\state_reg_reg[2] ));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \state_reg[2]_i_2 
       (.I0(Q[0]),
        .I1(enable_reg_reg_reg),
        .O(\state_reg[2]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000400000000)) 
    \state_reg[2]_i_3 
       (.I0(Q[3]),
        .I1(enable_reg_reg_reg),
        .I2(Q[7]),
        .I3(\state_reg[2]_i_7_n_0 ),
        .I4(Q[2]),
        .I5(Q[4]),
        .O(\state_reg[2]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000002000000)) 
    \state_reg[2]_i_5 
       (.I0(enable_reg_reg_reg),
        .I1(Q[7]),
        .I2(Q[4]),
        .I3(Q[3]),
        .I4(Q[2]),
        .I5(\state_reg[2]_i_7_n_0 ),
        .O(\state_reg[2]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    \state_reg[2]_i_7 
       (.I0(Q[6]),
        .I1(Q[5]),
        .I2(Q[1]),
        .O(\state_reg[2]_i_7_n_0 ));
  LUT4 #(
    .INIT(16'h8000)) 
    w_ctxt_en_i_1
       (.I0(\FSM_onehot_state_reg_reg_n_0_[8] ),
        .I1(count_ctxt[1]),
        .I2(count_ctxt[0]),
        .I3(enable_reg_reg_reg),
        .O(E));
endmodule

(* ORIG_REF_NAME = "decode_waypoint" *) 
module design_1_decodeur_traces_1_1_decode_waypoint
   (out,
    waypoint_address_en,
    \waypoint_address_cs_reg_reg[4] ,
    \pc_s_reg_reg[28] ,
    \FSM_onehot_state_reg_reg[0]_0 ,
    out_en_reg_0,
    \FSM_onehot_state_reg_reg[1]_0 ,
    waypoint_address,
    clk,
    D,
    Q,
    enable_reg_reg_reg,
    out_en_b_i,
    \waypoint_address_cs_reg_reg[3] ,
    reset,
    \data_reg_s_reg[2] ,
    \data_in_reg_reg[7] ,
    enable_reg_reg_reg_0,
    enable_reg_reg_reg_1,
    enable_reg_reg_reg_2);
  output [2:0]out;
  output waypoint_address_en;
  output [4:0]\waypoint_address_cs_reg_reg[4] ;
  output \pc_s_reg_reg[28] ;
  output \FSM_onehot_state_reg_reg[0]_0 ;
  output out_en_reg_0;
  output \FSM_onehot_state_reg_reg[1]_0 ;
  output [22:0]waypoint_address;
  input clk;
  input [2:0]D;
  input [7:0]Q;
  input enable_reg_reg_reg;
  input out_en_b_i;
  input [0:0]\waypoint_address_cs_reg_reg[3] ;
  input reset;
  input \data_reg_s_reg[2] ;
  input [7:0]\data_in_reg_reg[7] ;
  input [0:0]enable_reg_reg_reg_0;
  input [0:0]enable_reg_reg_reg_1;
  input [0:0]enable_reg_reg_reg_2;

  wire [2:0]D;
  wire \FSM_onehot_state_reg[11]_i_1_n_0 ;
  wire \FSM_onehot_state_reg[12]_i_1_n_0 ;
  wire \FSM_onehot_state_reg[12]_i_2_n_0 ;
  wire \FSM_onehot_state_reg[1]_i_9_n_0 ;
  wire \FSM_onehot_state_reg[2]_i_1__0_n_0 ;
  wire \FSM_onehot_state_reg[3]_i_1_n_0 ;
  wire \FSM_onehot_state_reg[4]_i_1_n_0 ;
  wire \FSM_onehot_state_reg[5]_i_1_n_0 ;
  wire \FSM_onehot_state_reg[6]_i_1_n_0 ;
  wire \FSM_onehot_state_reg[8]_i_1__0_n_0 ;
  wire \FSM_onehot_state_reg[9]_i_1_n_0 ;
  wire \FSM_onehot_state_reg_reg[0]_0 ;
  wire \FSM_onehot_state_reg_reg[1]_0 ;
  (* RTL_KEEP = "yes" *) wire \FSM_onehot_state_reg_reg_n_0_[10] ;
  (* RTL_KEEP = "yes" *) wire \FSM_onehot_state_reg_reg_n_0_[11] ;
  (* RTL_KEEP = "yes" *) wire \FSM_onehot_state_reg_reg_n_0_[12] ;
  (* RTL_KEEP = "yes" *) wire \FSM_onehot_state_reg_reg_n_0_[2] ;
  (* RTL_KEEP = "yes" *) wire \FSM_onehot_state_reg_reg_n_0_[4] ;
  (* RTL_KEEP = "yes" *) wire \FSM_onehot_state_reg_reg_n_0_[5] ;
  (* RTL_KEEP = "yes" *) wire \FSM_onehot_state_reg_reg_n_0_[6] ;
  (* RTL_KEEP = "yes" *) wire \FSM_onehot_state_reg_reg_n_0_[7] ;
  (* RTL_KEEP = "yes" *) wire \FSM_onehot_state_reg_reg_n_0_[8] ;
  (* RTL_KEEP = "yes" *) wire \FSM_onehot_state_reg_reg_n_0_[9] ;
  wire [7:0]Q;
  wire clk;
  wire [7:0]\data_in_reg_reg[7] ;
  wire \data_reg_s_reg[2] ;
  wire en_0_reg_i_1_n_0;
  wire en_4;
  wire enable_reg_reg_reg;
  wire [0:0]enable_reg_reg_reg_0;
  wire [0:0]enable_reg_reg_reg_1;
  wire [0:0]enable_reg_reg_reg_2;
  (* RTL_KEEP = "yes" *) wire [2:0]out;
  wire out_en_b_i;
  wire out_en_i_1_n_0;
  wire out_en_reg_0;
  wire \output_data_0[5]_i_1__0_n_0 ;
  wire \output_data_1[6]_i_1_n_0 ;
  wire \output_data_3[6]_i_1_n_0 ;
  wire \output_data_4[0]_i_1_n_0 ;
  wire \output_data_4[1]_i_1_n_0 ;
  wire \output_data_4[2]_i_1_n_0 ;
  wire \pc_s_reg_reg[28] ;
  wire reset;
  wire stop_waypoint;
  wire [22:0]waypoint_address;
  wire [0:0]\waypoint_address_cs_reg_reg[3] ;
  wire [4:0]\waypoint_address_cs_reg_reg[4] ;
  wire waypoint_address_en;

  LUT5 #(
    .INIT(32'hFC00AAAA)) 
    \FSM_onehot_state_reg[0]_i_2__1 
       (.I0(out[0]),
        .I1(\data_reg_s_reg[2] ),
        .I2(Q[7]),
        .I3(out_en_reg_0),
        .I4(enable_reg_reg_reg),
        .O(\FSM_onehot_state_reg_reg[0]_0 ));
  LUT3 #(
    .INIT(8'h20)) 
    \FSM_onehot_state_reg[11]_i_1 
       (.I0(\FSM_onehot_state_reg_reg_n_0_[2] ),
        .I1(Q[7]),
        .I2(enable_reg_reg_reg),
        .O(\FSM_onehot_state_reg[11]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0CAA)) 
    \FSM_onehot_state_reg[12]_i_1 
       (.I0(\FSM_onehot_state_reg[12]_i_2_n_0 ),
        .I1(out[1]),
        .I2(Q[7]),
        .I3(enable_reg_reg_reg),
        .O(\FSM_onehot_state_reg[12]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \FSM_onehot_state_reg[12]_i_2 
       (.I0(\FSM_onehot_state_reg_reg_n_0_[8] ),
        .I1(\FSM_onehot_state_reg_reg_n_0_[11] ),
        .I2(\FSM_onehot_state_reg_reg_n_0_[12] ),
        .I3(\FSM_onehot_state_reg_reg_n_0_[10] ),
        .I4(\FSM_onehot_state_reg_reg_n_0_[9] ),
        .O(\FSM_onehot_state_reg[12]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \FSM_onehot_state_reg[1]_i_7__0 
       (.I0(\data_in_reg_reg[7] [2]),
        .I1(\data_in_reg_reg[7] [3]),
        .I2(\data_in_reg_reg[7] [0]),
        .I3(\data_in_reg_reg[7] [1]),
        .I4(\FSM_onehot_state_reg[1]_i_9_n_0 ),
        .O(\FSM_onehot_state_reg_reg[1]_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \FSM_onehot_state_reg[1]_i_9 
       (.I0(\data_in_reg_reg[7] [5]),
        .I1(\data_in_reg_reg[7] [4]),
        .I2(\data_in_reg_reg[7] [7]),
        .I3(\data_in_reg_reg[7] [6]),
        .O(\FSM_onehot_state_reg[1]_i_9_n_0 ));
  LUT4 #(
    .INIT(16'h8F80)) 
    \FSM_onehot_state_reg[2]_i_1__0 
       (.I0(out[1]),
        .I1(Q[7]),
        .I2(enable_reg_reg_reg),
        .I3(\FSM_onehot_state_reg_reg_n_0_[2] ),
        .O(\FSM_onehot_state_reg[2]_i_1__0_n_0 ));
  LUT4 #(
    .INIT(16'h8F80)) 
    \FSM_onehot_state_reg[3]_i_1 
       (.I0(\FSM_onehot_state_reg_reg_n_0_[2] ),
        .I1(Q[7]),
        .I2(enable_reg_reg_reg),
        .I3(out[2]),
        .O(\FSM_onehot_state_reg[3]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h8F80)) 
    \FSM_onehot_state_reg[4]_i_1 
       (.I0(out[2]),
        .I1(Q[7]),
        .I2(enable_reg_reg_reg),
        .I3(\FSM_onehot_state_reg_reg_n_0_[4] ),
        .O(\FSM_onehot_state_reg[4]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h8F80)) 
    \FSM_onehot_state_reg[5]_i_1 
       (.I0(\FSM_onehot_state_reg_reg_n_0_[4] ),
        .I1(Q[7]),
        .I2(enable_reg_reg_reg),
        .I3(\FSM_onehot_state_reg_reg_n_0_[5] ),
        .O(\FSM_onehot_state_reg[5]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h8F80)) 
    \FSM_onehot_state_reg[6]_i_1 
       (.I0(Q[6]),
        .I1(\FSM_onehot_state_reg_reg_n_0_[5] ),
        .I2(enable_reg_reg_reg),
        .I3(\FSM_onehot_state_reg_reg_n_0_[6] ),
        .O(\FSM_onehot_state_reg[6]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    \FSM_onehot_state_reg[8]_i_1__0 
       (.I0(enable_reg_reg_reg),
        .I1(\FSM_onehot_state_reg_reg_n_0_[5] ),
        .I2(Q[6]),
        .O(\FSM_onehot_state_reg[8]_i_1__0_n_0 ));
  LUT3 #(
    .INIT(8'h20)) 
    \FSM_onehot_state_reg[9]_i_1 
       (.I0(\FSM_onehot_state_reg_reg_n_0_[4] ),
        .I1(Q[7]),
        .I2(enable_reg_reg_reg),
        .O(\FSM_onehot_state_reg[9]_i_1_n_0 ));
  (* FSM_ENCODED_STATES = "waypoint_01:0000000000100,waypoint_10:0100000000000,waypoint_00:1000000000000,waypoint_ib:0000010000000,waypoint_4_ib:0000001000000,waypoint_4:0000100000000,waypoint:0000000000010,wait_state:0000000000001,waypoint_21:0000000010000,waypoint_31:0000000100000,waypoint_20:0010000000000,waypoint_30:0001000000000,waypoint_11:0000000001000" *) 
  (* KEEP = "yes" *) 
  FDSE #(
    .INIT(1'b1)) 
    \FSM_onehot_state_reg_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(D[0]),
        .Q(out[0]),
        .S(reset));
  (* FSM_ENCODED_STATES = "waypoint_01:0000000000100,waypoint_10:0100000000000,waypoint_00:1000000000000,waypoint_ib:0000010000000,waypoint_4_ib:0000001000000,waypoint_4:0000100000000,waypoint:0000000000010,wait_state:0000000000001,waypoint_21:0000000010000,waypoint_31:0000000100000,waypoint_20:0010000000000,waypoint_30:0001000000000,waypoint_11:0000000001000" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg_reg[10] 
       (.C(clk),
        .CE(1'b1),
        .D(D[2]),
        .Q(\FSM_onehot_state_reg_reg_n_0_[10] ),
        .R(reset));
  (* FSM_ENCODED_STATES = "waypoint_01:0000000000100,waypoint_10:0100000000000,waypoint_00:1000000000000,waypoint_ib:0000010000000,waypoint_4_ib:0000001000000,waypoint_4:0000100000000,waypoint:0000000000010,wait_state:0000000000001,waypoint_21:0000000010000,waypoint_31:0000000100000,waypoint_20:0010000000000,waypoint_30:0001000000000,waypoint_11:0000000001000" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg_reg[11] 
       (.C(clk),
        .CE(1'b1),
        .D(\FSM_onehot_state_reg[11]_i_1_n_0 ),
        .Q(\FSM_onehot_state_reg_reg_n_0_[11] ),
        .R(reset));
  (* FSM_ENCODED_STATES = "waypoint_01:0000000000100,waypoint_10:0100000000000,waypoint_00:1000000000000,waypoint_ib:0000010000000,waypoint_4_ib:0000001000000,waypoint_4:0000100000000,waypoint:0000000000010,wait_state:0000000000001,waypoint_21:0000000010000,waypoint_31:0000000100000,waypoint_20:0010000000000,waypoint_30:0001000000000,waypoint_11:0000000001000" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg_reg[12] 
       (.C(clk),
        .CE(1'b1),
        .D(\FSM_onehot_state_reg[12]_i_1_n_0 ),
        .Q(\FSM_onehot_state_reg_reg_n_0_[12] ),
        .R(reset));
  (* FSM_ENCODED_STATES = "waypoint_01:0000000000100,waypoint_10:0100000000000,waypoint_00:1000000000000,waypoint_ib:0000010000000,waypoint_4_ib:0000001000000,waypoint_4:0000100000000,waypoint:0000000000010,wait_state:0000000000001,waypoint_21:0000000010000,waypoint_31:0000000100000,waypoint_20:0010000000000,waypoint_30:0001000000000,waypoint_11:0000000001000" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(D[1]),
        .Q(out[1]),
        .R(reset));
  (* FSM_ENCODED_STATES = "waypoint_01:0000000000100,waypoint_10:0100000000000,waypoint_00:1000000000000,waypoint_ib:0000010000000,waypoint_4_ib:0000001000000,waypoint_4:0000100000000,waypoint:0000000000010,wait_state:0000000000001,waypoint_21:0000000010000,waypoint_31:0000000100000,waypoint_20:0010000000000,waypoint_30:0001000000000,waypoint_11:0000000001000" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(\FSM_onehot_state_reg[2]_i_1__0_n_0 ),
        .Q(\FSM_onehot_state_reg_reg_n_0_[2] ),
        .R(reset));
  (* FSM_ENCODED_STATES = "waypoint_01:0000000000100,waypoint_10:0100000000000,waypoint_00:1000000000000,waypoint_ib:0000010000000,waypoint_4_ib:0000001000000,waypoint_4:0000100000000,waypoint:0000000000010,wait_state:0000000000001,waypoint_21:0000000010000,waypoint_31:0000000100000,waypoint_20:0010000000000,waypoint_30:0001000000000,waypoint_11:0000000001000" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(\FSM_onehot_state_reg[3]_i_1_n_0 ),
        .Q(out[2]),
        .R(reset));
  (* FSM_ENCODED_STATES = "waypoint_01:0000000000100,waypoint_10:0100000000000,waypoint_00:1000000000000,waypoint_ib:0000010000000,waypoint_4_ib:0000001000000,waypoint_4:0000100000000,waypoint:0000000000010,wait_state:0000000000001,waypoint_21:0000000010000,waypoint_31:0000000100000,waypoint_20:0010000000000,waypoint_30:0001000000000,waypoint_11:0000000001000" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg_reg[4] 
       (.C(clk),
        .CE(1'b1),
        .D(\FSM_onehot_state_reg[4]_i_1_n_0 ),
        .Q(\FSM_onehot_state_reg_reg_n_0_[4] ),
        .R(reset));
  (* FSM_ENCODED_STATES = "waypoint_01:0000000000100,waypoint_10:0100000000000,waypoint_00:1000000000000,waypoint_ib:0000010000000,waypoint_4_ib:0000001000000,waypoint_4:0000100000000,waypoint:0000000000010,wait_state:0000000000001,waypoint_21:0000000010000,waypoint_31:0000000100000,waypoint_20:0010000000000,waypoint_30:0001000000000,waypoint_11:0000000001000" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg_reg[5] 
       (.C(clk),
        .CE(1'b1),
        .D(\FSM_onehot_state_reg[5]_i_1_n_0 ),
        .Q(\FSM_onehot_state_reg_reg_n_0_[5] ),
        .R(reset));
  (* FSM_ENCODED_STATES = "waypoint_01:0000000000100,waypoint_10:0100000000000,waypoint_00:1000000000000,waypoint_ib:0000010000000,waypoint_4_ib:0000001000000,waypoint_4:0000100000000,waypoint:0000000000010,wait_state:0000000000001,waypoint_21:0000000010000,waypoint_31:0000000100000,waypoint_20:0010000000000,waypoint_30:0001000000000,waypoint_11:0000000001000" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg_reg[6] 
       (.C(clk),
        .CE(1'b1),
        .D(\FSM_onehot_state_reg[6]_i_1_n_0 ),
        .Q(\FSM_onehot_state_reg_reg_n_0_[6] ),
        .R(reset));
  (* FSM_ENCODED_STATES = "waypoint_01:0000000000100,waypoint_10:0100000000000,waypoint_00:1000000000000,waypoint_ib:0000010000000,waypoint_4_ib:0000001000000,waypoint_4:0000100000000,waypoint:0000000000010,wait_state:0000000000001,waypoint_21:0000000010000,waypoint_31:0000000100000,waypoint_20:0010000000000,waypoint_30:0001000000000,waypoint_11:0000000001000" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg_reg[7] 
       (.C(clk),
        .CE(enable_reg_reg_reg),
        .D(\FSM_onehot_state_reg_reg_n_0_[6] ),
        .Q(\FSM_onehot_state_reg_reg_n_0_[7] ),
        .R(reset));
  (* FSM_ENCODED_STATES = "waypoint_01:0000000000100,waypoint_10:0100000000000,waypoint_00:1000000000000,waypoint_ib:0000010000000,waypoint_4_ib:0000001000000,waypoint_4:0000100000000,waypoint:0000000000010,wait_state:0000000000001,waypoint_21:0000000010000,waypoint_31:0000000100000,waypoint_20:0010000000000,waypoint_30:0001000000000,waypoint_11:0000000001000" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg_reg[8] 
       (.C(clk),
        .CE(1'b1),
        .D(\FSM_onehot_state_reg[8]_i_1__0_n_0 ),
        .Q(\FSM_onehot_state_reg_reg_n_0_[8] ),
        .R(reset));
  (* FSM_ENCODED_STATES = "waypoint_01:0000000000100,waypoint_10:0100000000000,waypoint_00:1000000000000,waypoint_ib:0000010000000,waypoint_4_ib:0000001000000,waypoint_4:0000100000000,waypoint:0000000000010,wait_state:0000000000001,waypoint_21:0000000010000,waypoint_31:0000000100000,waypoint_20:0010000000000,waypoint_30:0001000000000,waypoint_11:0000000001000" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg_reg[9] 
       (.C(clk),
        .CE(1'b1),
        .D(\FSM_onehot_state_reg[9]_i_1_n_0 ),
        .Q(\FSM_onehot_state_reg_reg_n_0_[9] ),
        .R(reset));
  LUT3 #(
    .INIT(8'h20)) 
    en_0_reg_i_1
       (.I0(out[1]),
        .I1(Q[7]),
        .I2(enable_reg_reg_reg),
        .O(en_0_reg_i_1_n_0));
  FDRE en_0_reg_reg
       (.C(clk),
        .CE(out_en_i_1_n_0),
        .D(en_0_reg_i_1_n_0),
        .Q(\waypoint_address_cs_reg_reg[4] [4]),
        .R(1'b0));
  FDRE en_1_reg_reg
       (.C(clk),
        .CE(out_en_i_1_n_0),
        .D(\FSM_onehot_state_reg[11]_i_1_n_0 ),
        .Q(\waypoint_address_cs_reg_reg[4] [3]),
        .R(1'b0));
  FDRE en_2_reg_reg
       (.C(clk),
        .CE(out_en_i_1_n_0),
        .D(D[2]),
        .Q(\waypoint_address_cs_reg_reg[4] [2]),
        .R(1'b0));
  FDRE en_3_reg_reg
       (.C(clk),
        .CE(out_en_i_1_n_0),
        .D(\FSM_onehot_state_reg[9]_i_1_n_0 ),
        .Q(\waypoint_address_cs_reg_reg[4] [1]),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h8)) 
    en_4_reg_i_1
       (.I0(\FSM_onehot_state_reg_reg_n_0_[5] ),
        .I1(enable_reg_reg_reg),
        .O(en_4));
  FDRE en_4_reg_reg
       (.C(clk),
        .CE(out_en_i_1_n_0),
        .D(en_4),
        .Q(\waypoint_address_cs_reg_reg[4] [0]),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    out_en_i_1
       (.I0(reset),
        .O(out_en_i_1_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    out_en_i_2
       (.I0(out_en_reg_0),
        .I1(enable_reg_reg_reg),
        .O(stop_waypoint));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    out_en_i_3
       (.I0(\FSM_onehot_state_reg_reg_n_0_[9] ),
        .I1(\FSM_onehot_state_reg_reg_n_0_[10] ),
        .I2(\FSM_onehot_state_reg_reg_n_0_[12] ),
        .I3(\FSM_onehot_state_reg_reg_n_0_[11] ),
        .I4(\FSM_onehot_state_reg_reg_n_0_[8] ),
        .I5(\FSM_onehot_state_reg_reg_n_0_[7] ),
        .O(out_en_reg_0));
  FDRE out_en_reg
       (.C(clk),
        .CE(out_en_i_1_n_0),
        .D(stop_waypoint),
        .Q(waypoint_address_en),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h8)) 
    \output_data_0[5]_i_1__0 
       (.I0(enable_reg_reg_reg),
        .I1(out[1]),
        .O(\output_data_0[5]_i_1__0_n_0 ));
  FDRE \output_data_0_reg[0] 
       (.C(clk),
        .CE(\output_data_0[5]_i_1__0_n_0 ),
        .D(Q[1]),
        .Q(waypoint_address[0]),
        .R(reset));
  FDRE \output_data_0_reg[1] 
       (.C(clk),
        .CE(\output_data_0[5]_i_1__0_n_0 ),
        .D(Q[2]),
        .Q(waypoint_address[1]),
        .R(reset));
  FDRE \output_data_0_reg[2] 
       (.C(clk),
        .CE(\output_data_0[5]_i_1__0_n_0 ),
        .D(Q[3]),
        .Q(waypoint_address[2]),
        .R(reset));
  FDRE \output_data_0_reg[3] 
       (.C(clk),
        .CE(\output_data_0[5]_i_1__0_n_0 ),
        .D(Q[4]),
        .Q(waypoint_address[3]),
        .R(reset));
  FDRE \output_data_0_reg[4] 
       (.C(clk),
        .CE(\output_data_0[5]_i_1__0_n_0 ),
        .D(Q[5]),
        .Q(waypoint_address[4]),
        .R(reset));
  FDRE \output_data_0_reg[5] 
       (.C(clk),
        .CE(\output_data_0[5]_i_1__0_n_0 ),
        .D(enable_reg_reg_reg_0),
        .Q(waypoint_address[5]),
        .R(reset));
  LUT2 #(
    .INIT(4'h8)) 
    \output_data_1[6]_i_1 
       (.I0(enable_reg_reg_reg),
        .I1(\FSM_onehot_state_reg_reg_n_0_[2] ),
        .O(\output_data_1[6]_i_1_n_0 ));
  FDRE \output_data_1_reg[0] 
       (.C(clk),
        .CE(\output_data_1[6]_i_1_n_0 ),
        .D(Q[0]),
        .Q(waypoint_address[6]),
        .R(reset));
  FDRE \output_data_1_reg[1] 
       (.C(clk),
        .CE(\output_data_1[6]_i_1_n_0 ),
        .D(Q[1]),
        .Q(waypoint_address[7]),
        .R(reset));
  FDRE \output_data_1_reg[2] 
       (.C(clk),
        .CE(\output_data_1[6]_i_1_n_0 ),
        .D(Q[2]),
        .Q(waypoint_address[8]),
        .R(reset));
  FDRE \output_data_1_reg[3] 
       (.C(clk),
        .CE(\output_data_1[6]_i_1_n_0 ),
        .D(Q[3]),
        .Q(waypoint_address[9]),
        .R(reset));
  FDRE \output_data_1_reg[4] 
       (.C(clk),
        .CE(\output_data_1[6]_i_1_n_0 ),
        .D(Q[4]),
        .Q(waypoint_address[10]),
        .R(reset));
  FDRE \output_data_1_reg[5] 
       (.C(clk),
        .CE(\output_data_1[6]_i_1_n_0 ),
        .D(Q[5]),
        .Q(waypoint_address[11]),
        .R(reset));
  FDRE \output_data_1_reg[6] 
       (.C(clk),
        .CE(\output_data_1[6]_i_1_n_0 ),
        .D(enable_reg_reg_reg_1),
        .Q(waypoint_address[12]),
        .R(reset));
  LUT2 #(
    .INIT(4'h8)) 
    \output_data_3[6]_i_1 
       (.I0(enable_reg_reg_reg),
        .I1(\FSM_onehot_state_reg_reg_n_0_[4] ),
        .O(\output_data_3[6]_i_1_n_0 ));
  FDRE \output_data_3_reg[0] 
       (.C(clk),
        .CE(\output_data_3[6]_i_1_n_0 ),
        .D(Q[0]),
        .Q(waypoint_address[13]),
        .R(reset));
  FDRE \output_data_3_reg[1] 
       (.C(clk),
        .CE(\output_data_3[6]_i_1_n_0 ),
        .D(Q[1]),
        .Q(waypoint_address[14]),
        .R(reset));
  FDRE \output_data_3_reg[2] 
       (.C(clk),
        .CE(\output_data_3[6]_i_1_n_0 ),
        .D(Q[2]),
        .Q(waypoint_address[15]),
        .R(reset));
  FDRE \output_data_3_reg[3] 
       (.C(clk),
        .CE(\output_data_3[6]_i_1_n_0 ),
        .D(Q[3]),
        .Q(waypoint_address[16]),
        .R(reset));
  FDRE \output_data_3_reg[4] 
       (.C(clk),
        .CE(\output_data_3[6]_i_1_n_0 ),
        .D(Q[4]),
        .Q(waypoint_address[17]),
        .R(reset));
  FDRE \output_data_3_reg[5] 
       (.C(clk),
        .CE(\output_data_3[6]_i_1_n_0 ),
        .D(Q[5]),
        .Q(waypoint_address[18]),
        .R(reset));
  FDRE \output_data_3_reg[6] 
       (.C(clk),
        .CE(\output_data_3[6]_i_1_n_0 ),
        .D(enable_reg_reg_reg_2),
        .Q(waypoint_address[19]),
        .R(reset));
  LUT5 #(
    .INIT(32'hFFBF0080)) 
    \output_data_4[0]_i_1 
       (.I0(Q[0]),
        .I1(enable_reg_reg_reg),
        .I2(\FSM_onehot_state_reg_reg_n_0_[5] ),
        .I3(reset),
        .I4(waypoint_address[20]),
        .O(\output_data_4[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFBF0080)) 
    \output_data_4[1]_i_1 
       (.I0(Q[1]),
        .I1(enable_reg_reg_reg),
        .I2(\FSM_onehot_state_reg_reg_n_0_[5] ),
        .I3(reset),
        .I4(waypoint_address[21]),
        .O(\output_data_4[1]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFBF0080)) 
    \output_data_4[2]_i_1 
       (.I0(Q[2]),
        .I1(enable_reg_reg_reg),
        .I2(\FSM_onehot_state_reg_reg_n_0_[5] ),
        .I3(reset),
        .I4(waypoint_address[22]),
        .O(\output_data_4[2]_i_1_n_0 ));
  FDRE \output_data_4_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(\output_data_4[0]_i_1_n_0 ),
        .Q(waypoint_address[20]),
        .R(1'b0));
  FDRE \output_data_4_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\output_data_4[1]_i_1_n_0 ),
        .Q(waypoint_address[21]),
        .R(1'b0));
  FDRE \output_data_4_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(\output_data_4[2]_i_1_n_0 ),
        .Q(waypoint_address[22]),
        .R(1'b0));
  LUT3 #(
    .INIT(8'h02)) 
    \pc_s_reg[28]_i_6 
       (.I0(waypoint_address_en),
        .I1(out_en_b_i),
        .I2(\waypoint_address_cs_reg_reg[3] ),
        .O(\pc_s_reg_reg[28] ));
endmodule

(* ORIG_REF_NAME = "decodeur_traces" *) 
module design_1_decodeur_traces_1_1_decodeur_traces
   (waypoint_address_en,
    waypoint_address,
    pc,
    w_en,
    w_ctxt_en,
    context_id,
    reset,
    clk,
    trace_data,
    enable);
  output waypoint_address_en;
  output [22:0]waypoint_address;
  output [31:0]pc;
  output w_en;
  output w_ctxt_en;
  output [31:0]context_id;
  input reset;
  input clk;
  input [7:0]trace_data;
  input enable;

  wire chemin_n_0;
  wire chemin_n_1;
  wire chemin_n_10;
  wire chemin_n_11;
  wire chemin_n_13;
  wire chemin_n_14;
  wire chemin_n_15;
  wire chemin_n_16;
  wire chemin_n_17;
  wire chemin_n_2;
  wire chemin_n_3;
  wire chemin_n_4;
  wire chemin_n_51;
  wire chemin_n_52;
  wire chemin_n_53;
  wire chemin_n_54;
  wire chemin_n_55;
  wire chemin_n_56;
  wire chemin_n_7;
  wire chemin_n_8;
  wire chemin_n_9;
  wire clk;
  wire [31:0]context_id;
  wire count;
  wire \count[0]_i_1_n_0 ;
  wire \count[0]_i_2_n_0 ;
  wire [7:0]data_in;
  wire enable;
  wire enable_reg;
  wire enable_reg_i_1_n_0;
  wire enable_reg_reg_reg_n_0;
  wire fsm_n_1;
  wire fsm_n_10;
  wire fsm_n_11;
  wire fsm_n_12;
  wire fsm_n_13;
  wire fsm_n_14;
  wire fsm_n_15;
  wire fsm_n_16;
  wire fsm_n_17;
  wire fsm_n_18;
  wire fsm_n_2;
  wire fsm_n_3;
  wire fsm_n_4;
  wire fsm_n_5;
  wire fsm_n_7;
  wire fsm_n_8;
  wire fsm_n_9;
  wire out_en_s;
  wire [7:0]output_data_4;
  wire [7:0]output_data_5;
  wire [7:0]output_data_6;
  wire [7:0]output_data_7;
  wire [31:0]pc;
  wire reset;
  wire [2:2]state_next;
  wire [2:2]state_reg;
  wire [7:0]trace_data;
  wire \u_decode_bap/output_data_0_s ;
  wire w_ctxt_en;
  wire w_ctxt_en_s;
  wire w_en;
  wire [22:0]waypoint_address;
  wire waypoint_address_en;

  design_1_decodeur_traces_1_1_datapath chemin
       (.D(state_next),
        .E(\u_decode_bap/output_data_0_s ),
        .\FSM_onehot_state_reg_reg[0] (chemin_n_7),
        .\FSM_onehot_state_reg_reg[0]_0 (chemin_n_51),
        .\FSM_onehot_state_reg_reg[0]_1 (chemin_n_54),
        .\FSM_onehot_state_reg_reg[0]_2 (fsm_n_12),
        .\FSM_onehot_state_reg_reg[1] (chemin_n_13),
        .\FSM_onehot_state_reg_reg[1]_0 (chemin_n_14),
        .\FSM_onehot_state_reg_reg[1]_1 (chemin_n_52),
        .\FSM_onehot_state_reg_reg[1]_2 ({fsm_n_4,fsm_n_5}),
        .\FSM_onehot_state_reg_reg[7] ({chemin_n_1,chemin_n_2}),
        .\FSM_onehot_state_reg_reg[7]_0 (fsm_n_14),
        .Q(data_in),
        .clk(clk),
        .\context_id_reg[31] ({output_data_7,output_data_6,output_data_5,output_data_4}),
        .enable_reg_reg_reg(enable_reg_reg_reg_n_0),
        .enable_reg_reg_reg_0(fsm_n_8),
        .out(chemin_n_0),
        .out_en_reg(chemin_n_16),
        .out_en_s(out_en_s),
        .\output_data_0_reg[0] ({chemin_n_3,chemin_n_4}),
        .output_data_0_s_reg_reg(chemin_n_9),
        .\output_data_7_reg[0] (w_ctxt_en_s),
        .pc(pc),
        .\pc_reg[0] (chemin_n_8),
        .reset(reset),
        .\state_reg_reg[0] (chemin_n_10),
        .\state_reg_reg[0]_0 (chemin_n_11),
        .\state_reg_reg[0]_1 (chemin_n_15),
        .\state_reg_reg[0]_2 (chemin_n_17),
        .\state_reg_reg[0]_3 (chemin_n_55),
        .\state_reg_reg[0]_4 (fsm_n_2),
        .\state_reg_reg[0]_5 (fsm_n_17),
        .\state_reg_reg[1] (chemin_n_53),
        .\state_reg_reg[1]_0 (chemin_n_56),
        .\state_reg_reg[1]_1 (fsm_n_11),
        .\state_reg_reg[1]_2 (fsm_n_18),
        .\state_reg_reg[1]_3 (fsm_n_7),
        .\state_reg_reg[1]_4 (fsm_n_15),
        .\state_reg_reg[1]_5 (fsm_n_3),
        .\state_reg_reg[1]_6 (fsm_n_13),
        .\state_reg_reg[2] (fsm_n_1),
        .\state_reg_reg[2]_0 (fsm_n_10),
        .\state_reg_reg[2]_1 (fsm_n_16),
        .\state_reg_reg[2]_2 (state_reg),
        .\state_reg_reg[3] (fsm_n_9),
        .waypoint_address(waypoint_address),
        .waypoint_address_en(waypoint_address_en));
  FDRE \context_id_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(output_data_4[0]),
        .Q(context_id[0]),
        .R(reset));
  FDRE \context_id_reg[10] 
       (.C(clk),
        .CE(1'b1),
        .D(output_data_5[2]),
        .Q(context_id[10]),
        .R(reset));
  FDRE \context_id_reg[11] 
       (.C(clk),
        .CE(1'b1),
        .D(output_data_5[3]),
        .Q(context_id[11]),
        .R(reset));
  FDRE \context_id_reg[12] 
       (.C(clk),
        .CE(1'b1),
        .D(output_data_5[4]),
        .Q(context_id[12]),
        .R(reset));
  FDRE \context_id_reg[13] 
       (.C(clk),
        .CE(1'b1),
        .D(output_data_5[5]),
        .Q(context_id[13]),
        .R(reset));
  FDRE \context_id_reg[14] 
       (.C(clk),
        .CE(1'b1),
        .D(output_data_5[6]),
        .Q(context_id[14]),
        .R(reset));
  FDRE \context_id_reg[15] 
       (.C(clk),
        .CE(1'b1),
        .D(output_data_5[7]),
        .Q(context_id[15]),
        .R(reset));
  FDRE \context_id_reg[16] 
       (.C(clk),
        .CE(1'b1),
        .D(output_data_6[0]),
        .Q(context_id[16]),
        .R(reset));
  FDRE \context_id_reg[17] 
       (.C(clk),
        .CE(1'b1),
        .D(output_data_6[1]),
        .Q(context_id[17]),
        .R(reset));
  FDRE \context_id_reg[18] 
       (.C(clk),
        .CE(1'b1),
        .D(output_data_6[2]),
        .Q(context_id[18]),
        .R(reset));
  FDRE \context_id_reg[19] 
       (.C(clk),
        .CE(1'b1),
        .D(output_data_6[3]),
        .Q(context_id[19]),
        .R(reset));
  FDRE \context_id_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(output_data_4[1]),
        .Q(context_id[1]),
        .R(reset));
  FDRE \context_id_reg[20] 
       (.C(clk),
        .CE(1'b1),
        .D(output_data_6[4]),
        .Q(context_id[20]),
        .R(reset));
  FDRE \context_id_reg[21] 
       (.C(clk),
        .CE(1'b1),
        .D(output_data_6[5]),
        .Q(context_id[21]),
        .R(reset));
  FDRE \context_id_reg[22] 
       (.C(clk),
        .CE(1'b1),
        .D(output_data_6[6]),
        .Q(context_id[22]),
        .R(reset));
  FDRE \context_id_reg[23] 
       (.C(clk),
        .CE(1'b1),
        .D(output_data_6[7]),
        .Q(context_id[23]),
        .R(reset));
  FDRE \context_id_reg[24] 
       (.C(clk),
        .CE(1'b1),
        .D(output_data_7[0]),
        .Q(context_id[24]),
        .R(reset));
  FDRE \context_id_reg[25] 
       (.C(clk),
        .CE(1'b1),
        .D(output_data_7[1]),
        .Q(context_id[25]),
        .R(reset));
  FDRE \context_id_reg[26] 
       (.C(clk),
        .CE(1'b1),
        .D(output_data_7[2]),
        .Q(context_id[26]),
        .R(reset));
  FDRE \context_id_reg[27] 
       (.C(clk),
        .CE(1'b1),
        .D(output_data_7[3]),
        .Q(context_id[27]),
        .R(reset));
  FDRE \context_id_reg[28] 
       (.C(clk),
        .CE(1'b1),
        .D(output_data_7[4]),
        .Q(context_id[28]),
        .R(reset));
  FDRE \context_id_reg[29] 
       (.C(clk),
        .CE(1'b1),
        .D(output_data_7[5]),
        .Q(context_id[29]),
        .R(reset));
  FDRE \context_id_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(output_data_4[2]),
        .Q(context_id[2]),
        .R(reset));
  FDRE \context_id_reg[30] 
       (.C(clk),
        .CE(1'b1),
        .D(output_data_7[6]),
        .Q(context_id[30]),
        .R(reset));
  FDRE \context_id_reg[31] 
       (.C(clk),
        .CE(1'b1),
        .D(output_data_7[7]),
        .Q(context_id[31]),
        .R(reset));
  FDRE \context_id_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(output_data_4[3]),
        .Q(context_id[3]),
        .R(reset));
  FDRE \context_id_reg[4] 
       (.C(clk),
        .CE(1'b1),
        .D(output_data_4[4]),
        .Q(context_id[4]),
        .R(reset));
  FDRE \context_id_reg[5] 
       (.C(clk),
        .CE(1'b1),
        .D(output_data_4[5]),
        .Q(context_id[5]),
        .R(reset));
  FDRE \context_id_reg[6] 
       (.C(clk),
        .CE(1'b1),
        .D(output_data_4[6]),
        .Q(context_id[6]),
        .R(reset));
  FDRE \context_id_reg[7] 
       (.C(clk),
        .CE(1'b1),
        .D(output_data_4[7]),
        .Q(context_id[7]),
        .R(reset));
  FDRE \context_id_reg[8] 
       (.C(clk),
        .CE(1'b1),
        .D(output_data_5[0]),
        .Q(context_id[8]),
        .R(reset));
  FDRE \context_id_reg[9] 
       (.C(clk),
        .CE(1'b1),
        .D(output_data_5[1]),
        .Q(context_id[9]),
        .R(reset));
  LUT6 #(
    .INIT(64'hFFFFFFFF00000001)) 
    \count[0]_i_1 
       (.I0(trace_data[5]),
        .I1(trace_data[4]),
        .I2(trace_data[6]),
        .I3(trace_data[0]),
        .I4(\count[0]_i_2_n_0 ),
        .I5(count),
        .O(\count[0]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFFEF)) 
    \count[0]_i_2 
       (.I0(trace_data[2]),
        .I1(trace_data[3]),
        .I2(trace_data[7]),
        .I3(trace_data[1]),
        .O(\count[0]_i_2_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(\count[0]_i_1_n_0 ),
        .Q(count),
        .R(1'b0));
  FDRE \data_in_reg_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(trace_data[0]),
        .Q(data_in[0]),
        .R(reset));
  FDRE \data_in_reg_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(trace_data[1]),
        .Q(data_in[1]),
        .R(reset));
  FDRE \data_in_reg_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(trace_data[2]),
        .Q(data_in[2]),
        .R(reset));
  FDRE \data_in_reg_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(trace_data[3]),
        .Q(data_in[3]),
        .R(reset));
  FDRE \data_in_reg_reg[4] 
       (.C(clk),
        .CE(1'b1),
        .D(trace_data[4]),
        .Q(data_in[4]),
        .R(reset));
  FDRE \data_in_reg_reg[5] 
       (.C(clk),
        .CE(1'b1),
        .D(trace_data[5]),
        .Q(data_in[5]),
        .R(reset));
  FDRE \data_in_reg_reg[6] 
       (.C(clk),
        .CE(1'b1),
        .D(trace_data[6]),
        .Q(data_in[6]),
        .R(reset));
  FDRE \data_in_reg_reg[7] 
       (.C(clk),
        .CE(1'b1),
        .D(trace_data[7]),
        .Q(data_in[7]),
        .R(reset));
  LUT3 #(
    .INIT(8'hB8)) 
    enable_reg_i_1
       (.I0(enable),
        .I1(count),
        .I2(enable_reg),
        .O(enable_reg_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    enable_reg_reg
       (.C(clk),
        .CE(1'b1),
        .D(enable_reg_i_1_n_0),
        .Q(enable_reg),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    enable_reg_reg_reg
       (.C(clk),
        .CE(1'b1),
        .D(enable_reg),
        .Q(enable_reg_reg_reg_n_0),
        .R(reset));
  design_1_decodeur_traces_1_1_pft_decoder_v2 fsm
       (.D(state_next),
        .E(\u_decode_bap/output_data_0_s ),
        .\FSM_onehot_state_reg_reg[0] (fsm_n_11),
        .\FSM_onehot_state_reg_reg[0]_0 (fsm_n_12),
        .\FSM_onehot_state_reg_reg[0]_1 (fsm_n_14),
        .\FSM_onehot_state_reg_reg[0]_2 (fsm_n_18),
        .\FSM_onehot_state_reg_reg[0]_3 (chemin_n_51),
        .\FSM_onehot_state_reg_reg[0]_4 (chemin_n_55),
        .\FSM_onehot_state_reg_reg[13] (chemin_n_7),
        .\FSM_onehot_state_reg_reg[1] ({fsm_n_4,fsm_n_5}),
        .\FSM_onehot_state_reg_reg[1]_0 (fsm_n_15),
        .\FSM_onehot_state_reg_reg[1]_1 ({chemin_n_3,chemin_n_4}),
        .\FSM_onehot_state_reg_reg[1]_2 (chemin_n_8),
        .\FSM_onehot_state_reg_reg[7] ({chemin_n_1,chemin_n_2}),
        .\FSM_onehot_state_reg_reg[7]_0 (chemin_n_11),
        .\FSM_onehot_state_reg_reg[9] (chemin_n_16),
        .Q(state_reg),
        .clk(clk),
        .\data_in_reg_reg[2] (chemin_n_52),
        .\data_reg_s_reg[0] (chemin_n_9),
        .\data_reg_s_reg[0]_0 (chemin_n_17),
        .\data_reg_s_reg[0]_1 (chemin_n_53),
        .\data_reg_s_reg[0]_2 (chemin_n_56),
        .\data_reg_s_reg[0]_3 (chemin_n_54),
        .\data_reg_s_reg[2] (chemin_n_15),
        .\data_reg_s_reg[3] (chemin_n_13),
        .enable_reg_reg_reg(enable_reg_reg_reg_n_0),
        .enable_reg_reg_reg_0(chemin_n_14),
        .enable_reg_reg_reg_1(chemin_n_10),
        .out(chemin_n_0),
        .output_data_0_s_reg_reg(fsm_n_1),
        .output_data_0_s_reg_reg_0(fsm_n_2),
        .output_data_0_s_reg_reg_1(fsm_n_3),
        .reset(reset),
        .\state_reg_reg[0]_0 (fsm_n_7),
        .\state_reg_reg[0]_1 (fsm_n_8),
        .\state_reg_reg[0]_2 (fsm_n_9),
        .\state_reg_reg[0]_3 (fsm_n_13),
        .\state_reg_reg[0]_4 (fsm_n_16),
        .\state_reg_reg[1]_0 (fsm_n_10),
        .\state_reg_reg[2]_0 (fsm_n_17));
  FDRE w_ctxt_en_reg
       (.C(clk),
        .CE(1'b1),
        .D(w_ctxt_en_s),
        .Q(w_ctxt_en),
        .R(reset));
  FDRE w_en_reg
       (.C(clk),
        .CE(1'b1),
        .D(out_en_s),
        .Q(w_en),
        .R(reset));
endmodule

(* ORIG_REF_NAME = "pft_decoder_v2" *) 
module design_1_decodeur_traces_1_1_pft_decoder_v2
   (E,
    output_data_0_s_reg_reg,
    output_data_0_s_reg_reg_0,
    output_data_0_s_reg_reg_1,
    \FSM_onehot_state_reg_reg[1] ,
    Q,
    \state_reg_reg[0]_0 ,
    \state_reg_reg[0]_1 ,
    \state_reg_reg[0]_2 ,
    \state_reg_reg[1]_0 ,
    \FSM_onehot_state_reg_reg[0] ,
    \FSM_onehot_state_reg_reg[0]_0 ,
    \state_reg_reg[0]_3 ,
    \FSM_onehot_state_reg_reg[0]_1 ,
    \FSM_onehot_state_reg_reg[1]_0 ,
    \state_reg_reg[0]_4 ,
    \state_reg_reg[2]_0 ,
    \FSM_onehot_state_reg_reg[0]_2 ,
    \data_reg_s_reg[0] ,
    enable_reg_reg_reg,
    \FSM_onehot_state_reg_reg[7] ,
    enable_reg_reg_reg_0,
    \FSM_onehot_state_reg_reg[1]_1 ,
    \FSM_onehot_state_reg_reg[7]_0 ,
    \data_reg_s_reg[3] ,
    \data_in_reg_reg[2] ,
    D,
    \data_reg_s_reg[2] ,
    enable_reg_reg_reg_1,
    \data_reg_s_reg[0]_0 ,
    \FSM_onehot_state_reg_reg[13] ,
    \data_reg_s_reg[0]_1 ,
    \data_reg_s_reg[0]_2 ,
    out,
    \FSM_onehot_state_reg_reg[0]_3 ,
    \FSM_onehot_state_reg_reg[1]_2 ,
    \FSM_onehot_state_reg_reg[9] ,
    \FSM_onehot_state_reg_reg[0]_4 ,
    \data_reg_s_reg[0]_3 ,
    reset,
    clk);
  output [0:0]E;
  output output_data_0_s_reg_reg;
  output output_data_0_s_reg_reg_0;
  output output_data_0_s_reg_reg_1;
  output [1:0]\FSM_onehot_state_reg_reg[1] ;
  output [0:0]Q;
  output \state_reg_reg[0]_0 ;
  output \state_reg_reg[0]_1 ;
  output \state_reg_reg[0]_2 ;
  output \state_reg_reg[1]_0 ;
  output \FSM_onehot_state_reg_reg[0] ;
  output \FSM_onehot_state_reg_reg[0]_0 ;
  output \state_reg_reg[0]_3 ;
  output \FSM_onehot_state_reg_reg[0]_1 ;
  output \FSM_onehot_state_reg_reg[1]_0 ;
  output \state_reg_reg[0]_4 ;
  output \state_reg_reg[2]_0 ;
  output \FSM_onehot_state_reg_reg[0]_2 ;
  input \data_reg_s_reg[0] ;
  input enable_reg_reg_reg;
  input [1:0]\FSM_onehot_state_reg_reg[7] ;
  input enable_reg_reg_reg_0;
  input [1:0]\FSM_onehot_state_reg_reg[1]_1 ;
  input \FSM_onehot_state_reg_reg[7]_0 ;
  input \data_reg_s_reg[3] ;
  input \data_in_reg_reg[2] ;
  input [0:0]D;
  input \data_reg_s_reg[2] ;
  input enable_reg_reg_reg_1;
  input \data_reg_s_reg[0]_0 ;
  input \FSM_onehot_state_reg_reg[13] ;
  input \data_reg_s_reg[0]_1 ;
  input \data_reg_s_reg[0]_2 ;
  input [0:0]out;
  input \FSM_onehot_state_reg_reg[0]_3 ;
  input \FSM_onehot_state_reg_reg[1]_2 ;
  input \FSM_onehot_state_reg_reg[9] ;
  input \FSM_onehot_state_reg_reg[0]_4 ;
  input \data_reg_s_reg[0]_3 ;
  input reset;
  input clk;

  wire [0:0]D;
  wire [0:0]E;
  wire \FSM_onehot_state_reg[0]_i_3__1_n_0 ;
  wire \FSM_onehot_state_reg[0]_i_4_n_0 ;
  wire \FSM_onehot_state_reg[0]_i_5_n_0 ;
  wire \FSM_onehot_state_reg[0]_i_7__0_n_0 ;
  wire \FSM_onehot_state_reg[0]_i_9_n_0 ;
  wire \FSM_onehot_state_reg[1]_i_3__0_n_0 ;
  wire \FSM_onehot_state_reg[1]_i_4__0_n_0 ;
  wire \FSM_onehot_state_reg[1]_i_5_n_0 ;
  wire \FSM_onehot_state_reg[1]_i_7_n_0 ;
  wire \FSM_onehot_state_reg[1]_i_8__0_n_0 ;
  wire \FSM_onehot_state_reg_reg[0] ;
  wire \FSM_onehot_state_reg_reg[0]_0 ;
  wire \FSM_onehot_state_reg_reg[0]_1 ;
  wire \FSM_onehot_state_reg_reg[0]_2 ;
  wire \FSM_onehot_state_reg_reg[0]_3 ;
  wire \FSM_onehot_state_reg_reg[0]_4 ;
  wire \FSM_onehot_state_reg_reg[13] ;
  wire [1:0]\FSM_onehot_state_reg_reg[1] ;
  wire \FSM_onehot_state_reg_reg[1]_0 ;
  wire [1:0]\FSM_onehot_state_reg_reg[1]_1 ;
  wire \FSM_onehot_state_reg_reg[1]_2 ;
  wire [1:0]\FSM_onehot_state_reg_reg[7] ;
  wire \FSM_onehot_state_reg_reg[7]_0 ;
  wire \FSM_onehot_state_reg_reg[9] ;
  wire [0:0]Q;
  wire clk;
  wire \data_in_reg_reg[2] ;
  wire \data_reg_s_reg[0] ;
  wire \data_reg_s_reg[0]_0 ;
  wire \data_reg_s_reg[0]_1 ;
  wire \data_reg_s_reg[0]_2 ;
  wire \data_reg_s_reg[0]_3 ;
  wire \data_reg_s_reg[2] ;
  wire \data_reg_s_reg[3] ;
  wire enable_reg_reg_reg;
  wire enable_reg_reg_reg_0;
  wire enable_reg_reg_reg_1;
  wire [0:0]out;
  wire output_data_0_s_reg_reg;
  wire output_data_0_s_reg_reg_0;
  wire output_data_0_s_reg_reg_1;
  wire reset;
  wire [3:0]state_next;
  wire [3:0]state_reg;
  wire \state_reg[0]_i_2_n_0 ;
  wire \state_reg[0]_i_4_n_0 ;
  wire \state_reg[1]_i_4_n_0 ;
  wire \state_reg[3]_i_2_n_0 ;
  wire \state_reg_reg[0]_0 ;
  wire \state_reg_reg[0]_1 ;
  wire \state_reg_reg[0]_2 ;
  wire \state_reg_reg[0]_3 ;
  wire \state_reg_reg[0]_4 ;
  wire \state_reg_reg[1]_0 ;
  wire \state_reg_reg[2]_0 ;

  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFAEA)) 
    \FSM_onehot_state_reg[0]_i_1__1 
       (.I0(\FSM_onehot_state_reg_reg[0]_3 ),
        .I1(\FSM_onehot_state_reg[0]_i_3__1_n_0 ),
        .I2(\FSM_onehot_state_reg_reg[1]_1 [0]),
        .I3(output_data_0_s_reg_reg_1),
        .I4(\FSM_onehot_state_reg[0]_i_4_n_0 ),
        .I5(\FSM_onehot_state_reg[0]_i_5_n_0 ),
        .O(\FSM_onehot_state_reg_reg[1] [0]));
  LUT6 #(
    .INIT(64'h0088080000880888)) 
    \FSM_onehot_state_reg[0]_i_3__0 
       (.I0(out),
        .I1(\FSM_onehot_state_reg[0]_i_9_n_0 ),
        .I2(\FSM_onehot_state_reg_reg[9] ),
        .I3(state_reg[3]),
        .I4(Q),
        .I5(\data_in_reg_reg[2] ),
        .O(\FSM_onehot_state_reg_reg[0]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT5 #(
    .INIT(32'h04030000)) 
    \FSM_onehot_state_reg[0]_i_3__1 
       (.I0(out),
        .I1(state_reg[0]),
        .I2(state_reg[3]),
        .I3(Q),
        .I4(state_reg[1]),
        .O(\FSM_onehot_state_reg[0]_i_3__1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000001000000)) 
    \FSM_onehot_state_reg[0]_i_4 
       (.I0(Q),
        .I1(state_reg[3]),
        .I2(state_reg[1]),
        .I3(state_reg[0]),
        .I4(\FSM_onehot_state_reg_reg[1]_1 [0]),
        .I5(\data_in_reg_reg[2] ),
        .O(\FSM_onehot_state_reg[0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h5050000051FF0000)) 
    \FSM_onehot_state_reg[0]_i_4__0 
       (.I0(state_reg[1]),
        .I1(\data_in_reg_reg[2] ),
        .I2(\FSM_onehot_state_reg[0]_i_7__0_n_0 ),
        .I3(\FSM_onehot_state_reg_reg[0]_4 ),
        .I4(state_reg[0]),
        .I5(state_reg[3]),
        .O(\state_reg_reg[0]_0 ));
  LUT6 #(
    .INIT(64'hAA82AAAA00000000)) 
    \FSM_onehot_state_reg[0]_i_5 
       (.I0(\FSM_onehot_state_reg_reg[1]_1 [0]),
        .I1(Q),
        .I2(state_reg[3]),
        .I3(state_reg[1]),
        .I4(state_reg[0]),
        .I5(\data_reg_s_reg[0]_3 ),
        .O(\FSM_onehot_state_reg[0]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT5 #(
    .INIT(32'hFFFFFEEF)) 
    \FSM_onehot_state_reg[0]_i_5__0 
       (.I0(\FSM_onehot_state_reg_reg[7] [1]),
        .I1(state_reg[0]),
        .I2(Q),
        .I3(state_reg[3]),
        .I4(state_reg[1]),
        .O(\FSM_onehot_state_reg_reg[0]_1 ));
  LUT6 #(
    .INIT(64'h0000000000001000)) 
    \FSM_onehot_state_reg[0]_i_6 
       (.I0(state_reg[1]),
        .I1(Q),
        .I2(out),
        .I3(state_reg[3]),
        .I4(state_reg[0]),
        .I5(\FSM_onehot_state_reg_reg[13] ),
        .O(\FSM_onehot_state_reg_reg[0] ));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT3 #(
    .INIT(8'h34)) 
    \FSM_onehot_state_reg[0]_i_7__0 
       (.I0(\FSM_onehot_state_reg_reg[9] ),
        .I1(state_reg[3]),
        .I2(Q),
        .O(\FSM_onehot_state_reg[0]_i_7__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT4 #(
    .INIT(16'h0400)) 
    \FSM_onehot_state_reg[0]_i_8__0 
       (.I0(state_reg[1]),
        .I1(out),
        .I2(state_reg[3]),
        .I3(Q),
        .O(\FSM_onehot_state_reg_reg[0]_2 ));
  LUT2 #(
    .INIT(4'h2)) 
    \FSM_onehot_state_reg[0]_i_9 
       (.I0(state_reg[0]),
        .I1(state_reg[1]),
        .O(\FSM_onehot_state_reg[0]_i_9_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \FSM_onehot_state_reg[1]_i_10 
       (.I0(Q),
        .I1(state_reg[3]),
        .O(\state_reg_reg[0]_4 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFEFEFFFE)) 
    \FSM_onehot_state_reg[1]_i_1__0 
       (.I0(enable_reg_reg_reg_0),
        .I1(\FSM_onehot_state_reg[1]_i_3__0_n_0 ),
        .I2(\FSM_onehot_state_reg[1]_i_4__0_n_0 ),
        .I3(\FSM_onehot_state_reg_reg[1]_1 [1]),
        .I4(enable_reg_reg_reg),
        .I5(\FSM_onehot_state_reg[1]_i_5_n_0 ),
        .O(\FSM_onehot_state_reg_reg[1] [1]));
  LUT6 #(
    .INIT(64'hFFFFFFFF11F100F1)) 
    \FSM_onehot_state_reg[1]_i_3 
       (.I0(Q),
        .I1(state_reg[3]),
        .I2(\FSM_onehot_state_reg_reg[13] ),
        .I3(state_reg[0]),
        .I4(\data_in_reg_reg[2] ),
        .I5(\FSM_onehot_state_reg[1]_i_8__0_n_0 ),
        .O(\state_reg_reg[1]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT5 #(
    .INIT(32'h8AA00000)) 
    \FSM_onehot_state_reg[1]_i_3__0 
       (.I0(state_reg[1]),
        .I1(out),
        .I2(state_reg[0]),
        .I3(Q),
        .I4(\data_reg_s_reg[3] ),
        .O(\FSM_onehot_state_reg[1]_i_3__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT4 #(
    .INIT(16'hA800)) 
    \FSM_onehot_state_reg[1]_i_4__0 
       (.I0(state_reg[3]),
        .I1(state_reg[1]),
        .I2(Q),
        .I3(\data_reg_s_reg[3] ),
        .O(\FSM_onehot_state_reg[1]_i_4__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT4 #(
    .INIT(16'hFFFD)) 
    \FSM_onehot_state_reg[1]_i_4__1 
       (.I0(state_reg[1]),
        .I1(Q),
        .I2(state_reg[3]),
        .I3(state_reg[0]),
        .O(\FSM_onehot_state_reg_reg[1]_0 ));
  LUT6 #(
    .INIT(64'hAAFEAAAAAAAEAAAA)) 
    \FSM_onehot_state_reg[1]_i_5 
       (.I0(\FSM_onehot_state_reg[1]_i_7_n_0 ),
        .I1(\FSM_onehot_state_reg_reg[7]_0 ),
        .I2(state_reg[0]),
        .I3(state_reg[1]),
        .I4(\data_reg_s_reg[3] ),
        .I5(\data_in_reg_reg[2] ),
        .O(\FSM_onehot_state_reg[1]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000808000)) 
    \FSM_onehot_state_reg[1]_i_7 
       (.I0(\FSM_onehot_state_reg[0]_i_9_n_0 ),
        .I1(enable_reg_reg_reg),
        .I2(\FSM_onehot_state_reg_reg[1]_1 [0]),
        .I3(Q),
        .I4(state_reg[3]),
        .I5(\FSM_onehot_state_reg_reg[9] ),
        .O(\FSM_onehot_state_reg[1]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT5 #(
    .INIT(32'hFFF8F0F0)) 
    \FSM_onehot_state_reg[1]_i_8__0 
       (.I0(\FSM_onehot_state_reg_reg[9] ),
        .I1(state_reg[0]),
        .I2(state_reg[1]),
        .I3(Q),
        .I4(state_reg[3]),
        .O(\FSM_onehot_state_reg[1]_i_8__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT5 #(
    .INIT(32'hF5F5F151)) 
    \FSM_onehot_state_reg[2]_i_4 
       (.I0(state_reg[0]),
        .I1(state_reg[1]),
        .I2(state_reg[3]),
        .I3(\FSM_onehot_state_reg_reg[9] ),
        .I4(Q),
        .O(output_data_0_s_reg_reg_0));
  LUT6 #(
    .INIT(64'hD0D0D5D000000000)) 
    \FSM_onehot_state_reg[2]_i_5 
       (.I0(Q),
        .I1(out),
        .I2(state_reg[1]),
        .I3(\data_in_reg_reg[2] ),
        .I4(state_reg[3]),
        .I5(state_reg[0]),
        .O(output_data_0_s_reg_reg));
  LUT6 #(
    .INIT(64'h0000000000000014)) 
    \FSM_onehot_state_reg[2]_i_7 
       (.I0(state_reg[1]),
        .I1(Q),
        .I2(state_reg[3]),
        .I3(state_reg[0]),
        .I4(\FSM_onehot_state_reg_reg[1]_2 ),
        .I5(\FSM_onehot_state_reg_reg[7] [1]),
        .O(output_data_0_s_reg_reg_1));
  LUT6 #(
    .INIT(64'hFFE0E0E0E0E0E0E0)) 
    \output_data_0[5]_i_1 
       (.I0(output_data_0_s_reg_reg),
        .I1(output_data_0_s_reg_reg_0),
        .I2(\data_reg_s_reg[0] ),
        .I3(output_data_0_s_reg_reg_1),
        .I4(enable_reg_reg_reg),
        .I5(\FSM_onehot_state_reg_reg[7] [0]),
        .O(E));
  LUT6 #(
    .INIT(64'hFFFFFFFFFEFFFEFE)) 
    \state_reg[0]_i_1 
       (.I0(\state_reg[0]_i_2_n_0 ),
        .I1(\state_reg_reg[0]_0 ),
        .I2(\state_reg_reg[0]_1 ),
        .I3(\data_reg_s_reg[2] ),
        .I4(\state_reg[0]_i_4_n_0 ),
        .I5(enable_reg_reg_reg_1),
        .O(state_next[0]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT3 #(
    .INIT(8'h04)) 
    \state_reg[0]_i_10 
       (.I0(state_reg[1]),
        .I1(state_reg[0]),
        .I2(Q),
        .O(\state_reg_reg[0]_3 ));
  LUT6 #(
    .INIT(64'hFFFFFF0404040404)) 
    \state_reg[0]_i_2 
       (.I0(enable_reg_reg_reg),
        .I1(state_reg[0]),
        .I2(state_reg[3]),
        .I3(\FSM_onehot_state_reg_reg[7]_0 ),
        .I4(\state_reg_reg[0]_2 ),
        .I5(\data_reg_s_reg[0]_0 ),
        .O(\state_reg[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hCCCCCCEACCC0CCEE)) 
    \state_reg[0]_i_4 
       (.I0(state_reg[0]),
        .I1(enable_reg_reg_reg),
        .I2(\FSM_onehot_state_reg_reg[13] ),
        .I3(state_reg[1]),
        .I4(Q),
        .I5(state_reg[3]),
        .O(\state_reg[0]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT3 #(
    .INIT(8'hF8)) 
    \state_reg[0]_i_6 
       (.I0(state_reg[3]),
        .I1(Q),
        .I2(state_reg[1]),
        .O(\state_reg_reg[0]_2 ));
  LUT5 #(
    .INIT(32'hFFFFFEAA)) 
    \state_reg[1]_i_1 
       (.I0(\FSM_onehot_state_reg[0]_i_3__1_n_0 ),
        .I1(\data_reg_s_reg[0]_1 ),
        .I2(\data_reg_s_reg[0]_2 ),
        .I3(\state_reg_reg[1]_0 ),
        .I4(\state_reg[1]_i_4_n_0 ),
        .O(state_next[1]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT3 #(
    .INIT(8'h04)) 
    \state_reg[1]_i_4 
       (.I0(state_reg[3]),
        .I1(state_reg[1]),
        .I2(enable_reg_reg_reg),
        .O(\state_reg[1]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT5 #(
    .INIT(32'h00020000)) 
    \state_reg[2]_i_4 
       (.I0(enable_reg_reg_reg),
        .I1(state_reg[0]),
        .I2(state_reg[3]),
        .I3(Q),
        .I4(state_reg[1]),
        .O(\state_reg_reg[0]_1 ));
  LUT6 #(
    .INIT(64'h000008000000FF00)) 
    \state_reg[2]_i_6 
       (.I0(state_reg[0]),
        .I1(state_reg[1]),
        .I2(out),
        .I3(Q),
        .I4(state_reg[3]),
        .I5(enable_reg_reg_reg),
        .O(\state_reg_reg[2]_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF01000000)) 
    \state_reg[3]_i_1 
       (.I0(\FSM_onehot_state_reg_reg[9] ),
        .I1(Q),
        .I2(state_reg[1]),
        .I3(state_reg[0]),
        .I4(state_reg[3]),
        .I5(\state_reg[3]_i_2_n_0 ),
        .O(state_next[3]));
  LUT6 #(
    .INIT(64'h0000013331000000)) 
    \state_reg[3]_i_2 
       (.I0(\FSM_onehot_state_reg_reg[13] ),
        .I1(state_reg[1]),
        .I2(state_reg[0]),
        .I3(enable_reg_reg_reg),
        .I4(Q),
        .I5(state_reg[3]),
        .O(\state_reg[3]_i_2_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \state_reg_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(state_next[0]),
        .Q(state_reg[0]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \state_reg_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(state_next[1]),
        .Q(state_reg[1]),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \state_reg_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(D),
        .Q(Q),
        .R(reset));
  FDRE #(
    .INIT(1'b0)) 
    \state_reg_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(state_next[3]),
        .Q(state_reg[3]),
        .R(reset));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
