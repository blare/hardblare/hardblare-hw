library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity PLtoPS is
	generic (
		-- Users to add parameters here

		-- User parameters ends
		-- Do not modify the parameters beyond this line


		-- Parameters of Axi Slave Bus Interface S00_AXI
		C_S00_AXI_DATA_WIDTH	: integer	:= 32;
		C_S00_AXI_ADDR_WIDTH	: integer	:= 32;

		-- Parameters of Axi Slave Bus Interface S_AXI_INTR
		C_S_AXI_INTR_DATA_WIDTH	: integer	:= 32;
		C_S_AXI_INTR_ADDR_WIDTH	: integer	:= 32;
		C_NUM_OF_INTR	: integer	:= 6;
		C_INTR_SENSITIVITY	: std_logic_vector	:= x"FFFFFFFF";
		C_INTR_ACTIVE_STATE	: std_logic_vector	:= x"FFFFFFFF";
		C_IRQ_SENSITIVITY	: integer	:= 1;
		C_IRQ_ACTIVE_STATE	: integer	:= 1
	);
	port (
		-- Users to add ports here
        
        -- FIFO instrumentation		
        fifo_instrumentation_empty            : in std_logic;
        fifo_instrumentation_almost_empty     : in std_logic;
        fifo_instrumentation_full             : in std_logic; 
        fifo_instrumentation_almost_full      : in std_logic;  
        
        -- FIFO TMC      
        fifo_tmc_empty              : in std_logic;
        fifo_tmc_almost_empty       : in std_logic;             
        fifo_tmc_full               : in std_logic;
        fifo_tmc_almost_full        : in std_logic;           
                    
        -- FIFO Monitor to Kernel        
        fifo_monitor_to_kernel_empty    	    : in std_logic;
        fifo_monitor_to_kernel_almost_empty     : in std_logic;
        fifo_monitor_to_kernel_full         	: in std_logic;
        fifo_monitor_to_kernel_almost_full      : in std_logic;  
                
        -- FIFO Kernel to Monitor
        fifo_kernel_to_monitor_empty    	        : in std_logic;
        fifo_kernel_to_monitor_almost_empty         : in std_logic;     
        fifo_kernel_to_monitor_full    	            : in std_logic;
        fifo_kernel_to_monitor_almost_full          : in std_logic;      
                    
        -- FIFO PTM
        fifo_ptm_empty                           : in std_logic;
        fifo_ptm_almost_empty                    : in std_logic;
        fifo_ptm_full                            : in std_logic;
        fifo_ptm_almost_full                     : in std_logic;   
                
        -- Dispatcher
        dispatcher_read_enable               : in std_logic;
        dispatcher_write_enable              : in std_logic;
        dispatcher_address                   : in std_logic_vector(31 downto 0);
        dispatcher_write_data_in             : in std_logic_vector(31 downto 0);
        dispatcher_data_out                  : out std_logic_vector(31 downto 0);
        dispatcher_interruption_generate     : in std_logic;
        to_dispatcher_interruption_ack       : in std_logic;
         
        -- TMC
        tmc_read_enable         : in std_logic;
        tmc_write_enable        : in std_logic;
        tmc_address             : in std_logic_vector(31 downto 0);
        tmc_write_data_in       : in std_logic_vector(31 downto 0);
        tmc_interruption        : in std_logic;
        
        -- User ports ends
		-- Do not modify the ports beyond this line


		-- Ports of Axi Slave Bus Interface S00_AXI
		s00_axi_aclk	: in std_logic;
		s00_axi_aresetn	: in std_logic;
		s00_axi_awaddr	: in std_logic_vector(C_S00_AXI_ADDR_WIDTH-1 downto 0);
		s00_axi_awprot	: in std_logic_vector(2 downto 0);
		s00_axi_awvalid	: in std_logic;
		s00_axi_awready	: out std_logic;
		s00_axi_wdata	: in std_logic_vector(C_S00_AXI_DATA_WIDTH-1 downto 0);
		s00_axi_wstrb	: in std_logic_vector((C_S00_AXI_DATA_WIDTH/8)-1 downto 0);
		s00_axi_wvalid	: in std_logic;
		s00_axi_wready	: out std_logic;
		s00_axi_bresp	: out std_logic_vector(1 downto 0);
		s00_axi_bvalid	: out std_logic;
		s00_axi_bready	: in std_logic;
		s00_axi_araddr	: in std_logic_vector(C_S00_AXI_ADDR_WIDTH-1 downto 0);
		s00_axi_arprot	: in std_logic_vector(2 downto 0);
		s00_axi_arvalid	: in std_logic;
		s00_axi_arready	: out std_logic;
		s00_axi_rdata	: out std_logic_vector(C_S00_AXI_DATA_WIDTH-1 downto 0);
		s00_axi_rresp	: out std_logic_vector(1 downto 0);
		s00_axi_rvalid	: out std_logic;
		s00_axi_rready	: in std_logic;

		-- Ports of Axi Slave Bus Interface S_AXI_INTR
		s_axi_intr_aclk	: in std_logic;
		s_axi_intr_aresetn	: in std_logic;
		s_axi_intr_awaddr	: in std_logic_vector(C_S_AXI_INTR_ADDR_WIDTH-1 downto 0);
		s_axi_intr_awprot	: in std_logic_vector(2 downto 0);
		s_axi_intr_awvalid	: in std_logic;
		s_axi_intr_awready	: out std_logic;
		s_axi_intr_wdata	: in std_logic_vector(C_S_AXI_INTR_DATA_WIDTH-1 downto 0);
		s_axi_intr_wstrb	: in std_logic_vector((C_S_AXI_INTR_DATA_WIDTH/8)-1 downto 0);
		s_axi_intr_wvalid	: in std_logic;
		s_axi_intr_wready	: out std_logic;
		s_axi_intr_bresp	: out std_logic_vector(1 downto 0);
		s_axi_intr_bvalid	: out std_logic;
		s_axi_intr_bready	: in std_logic;
		s_axi_intr_araddr	: in std_logic_vector(C_S_AXI_INTR_ADDR_WIDTH-1 downto 0);
		s_axi_intr_arprot	: in std_logic_vector(2 downto 0);
		s_axi_intr_arvalid	: in std_logic;
		s_axi_intr_arready	: out std_logic;
		s_axi_intr_rdata	: out std_logic_vector(C_S_AXI_INTR_DATA_WIDTH-1 downto 0);
		s_axi_intr_rresp	: out std_logic_vector(1 downto 0);
		s_axi_intr_rvalid	: out std_logic;
		s_axi_intr_rready	: in std_logic;
		
		irq	: out std_logic
	);
end PLtoPS;

architecture arch_imp of PLtoPS is

	-- component declaration
	component PLtoPS_S00_AXI is
		generic (
		C_S_AXI_DATA_WIDTH	: integer	:= 32;
		C_S_AXI_ADDR_WIDTH	: integer	:= 32
		);
		port (
				
        -- FIFO instrumentation		
        fifo_instrumentation_empty            : in std_logic;
        fifo_instrumentation_almost_empty     : in std_logic;
        fifo_instrumentation_full             : in std_logic; 
        fifo_instrumentation_almost_full      : in std_logic;  
          
        -- FIFO TMC      
        fifo_tmc_empty              : in std_logic;
        fifo_tmc_almost_empty       : in std_logic;             
        fifo_tmc_full               : in std_logic;
        fifo_tmc_almost_full        : in std_logic;               
                    
        -- FIFO Monitor to Kernel        
        fifo_monitor_to_kernel_empty            : in std_logic;
        fifo_monitor_to_kernel_almost_empty     : in std_logic;
        fifo_monitor_to_kernel_full             : in std_logic;
        fifo_monitor_to_kernel_almost_full      : in std_logic;     
        
        -- FIFO Kernel to Monitor
        fifo_kernel_to_monitor_empty                : in std_logic;
        fifo_kernel_to_monitor_almost_empty         : in std_logic;     
        fifo_kernel_to_monitor_full                    : in std_logic;
        fifo_kernel_to_monitor_almost_full          : in std_logic;      
                    
        -- FIFO PTM
        fifo_ptm_empty                           : in std_logic;
        fifo_ptm_almost_empty                    : in std_logic;
        fifo_ptm_full                            : in std_logic;
        fifo_ptm_almost_full                     : in std_logic; 
                
        -- Dispatcher
        dispatcher_read_enable         : in std_logic;
        dispatcher_write_enable        : in std_logic;
        dispatcher_address       : in std_logic_vector(31 downto 0);
        dispatcher_write_data_in       : in std_logic_vector(31 downto 0);
        dispatcher_data_out      : out std_logic_vector(31 downto 0);
        
        dispatcher_interruption_generate        : in std_logic;
        to_dispatcher_interruption_ack             : in std_logic;
 
        -- TMC
        tmc_read_enable         : in std_logic;
        tmc_write_enable        : in std_logic;
        tmc_address       : in std_logic_vector(31 downto 0);
        tmc_write_data_in       : in std_logic_vector(31 downto 0);
        tmc_interruption        : in std_logic;
        
        
		S_AXI_ACLK	: in std_logic;
		S_AXI_ARESETN	: in std_logic;
		S_AXI_AWADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_AWPROT	: in std_logic_vector(2 downto 0);
		S_AXI_AWVALID	: in std_logic;
		S_AXI_AWREADY	: out std_logic;
		S_AXI_WDATA	: in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_WSTRB	: in std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
		S_AXI_WVALID	: in std_logic;
		S_AXI_WREADY	: out std_logic;
		S_AXI_BRESP	: out std_logic_vector(1 downto 0);
		S_AXI_BVALID	: out std_logic;
		S_AXI_BREADY	: in std_logic;
		S_AXI_ARADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_ARPROT	: in std_logic_vector(2 downto 0);
		S_AXI_ARVALID	: in std_logic;
		S_AXI_ARREADY	: out std_logic;
		S_AXI_RDATA	: out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_RRESP	: out std_logic_vector(1 downto 0);
		S_AXI_RVALID	: out std_logic;
		S_AXI_RREADY	: in std_logic
		);
	end component PLtoPS_S00_AXI;

	component PLtoPS_S_AXI_INTR is
		generic (
		C_S_AXI_DATA_WIDTH	: integer	:= 32;
		C_S_AXI_ADDR_WIDTH	: integer	:= 32;
		C_NUM_OF_INTR	: integer	:= 5;
		C_INTR_SENSITIVITY	: std_logic_vector	:= x"FFFFFFFF";
		C_INTR_ACTIVE_STATE	: std_logic_vector	:= x"FFFFFFFF";
		C_IRQ_SENSITIVITY	: integer	:= 1;
		C_IRQ_ACTIVE_STATE	: integer	:= 1
		);
		port (
		S_AXI_ACLK	: in std_logic;
		S_AXI_ARESETN	: in std_logic;
		S_AXI_AWADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_AWPROT	: in std_logic_vector(2 downto 0);
		S_AXI_AWVALID	: in std_logic;
		S_AXI_AWREADY	: out std_logic;
		S_AXI_WDATA	: in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_WSTRB	: in std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
		S_AXI_WVALID	: in std_logic;
		S_AXI_WREADY	: out std_logic;
		S_AXI_BRESP	: out std_logic_vector(1 downto 0);
		S_AXI_BVALID	: out std_logic;
		S_AXI_BREADY	: in std_logic;
		S_AXI_ARADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_ARPROT	: in std_logic_vector(2 downto 0);
		S_AXI_ARVALID	: in std_logic;
		S_AXI_ARREADY	: out std_logic;
		S_AXI_RDATA	: out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_RRESP	: out std_logic_vector(1 downto 0);
		S_AXI_RVALID	: out std_logic;
		S_AXI_RREADY	: in std_logic;
		
		-- FIFO instrumentation		
        fifo_instrumentation_empty            : in std_logic;
        fifo_instrumentation_almost_empty     : in std_logic;
        fifo_instrumentation_full             : in std_logic; 
        fifo_instrumentation_almost_full      : in std_logic;  
        
        -- FIFO TMC      
        fifo_tmc_empty              : in std_logic;
        fifo_tmc_almost_empty       : in std_logic;             
        fifo_tmc_full               : in std_logic;
        fifo_tmc_almost_full        : in std_logic;         
        
        -- FIFO Monitor to Kernel        
        fifo_monitor_to_kernel_empty            : in std_logic;
        fifo_monitor_to_kernel_almost_empty     : in std_logic;
        fifo_monitor_to_kernel_full             : in std_logic;
        fifo_monitor_to_kernel_almost_full      : in std_logic;     
                
        -- FIFO Kernel to Monitor
        fifo_kernel_to_monitor_empty                : in std_logic;
        fifo_kernel_to_monitor_almost_empty         : in std_logic;     
        fifo_kernel_to_monitor_full                    : in std_logic;
        fifo_kernel_to_monitor_almost_full          : in std_logic;      
                    
        -- FIFO PTM
        fifo_ptm_empty                           : in std_logic;
        fifo_ptm_almost_empty                    : in std_logic;
        fifo_ptm_full                            : in std_logic;
        fifo_ptm_almost_full                     : in std_logic;  
                
        -- Dispatcher
        dispatcher_interruption_generate        : in std_logic;
        to_dispatcher_interruption_ack             : in std_logic;
 
        -- TMC
        tmc_interruption        : in std_logic;
		
		irq	: out std_logic
		);
	end component PLtoPS_S_AXI_INTR;

begin

-- Instantiation of Axi Bus Interface S00_AXI
PLtoPS_S00_AXI_inst : PLtoPS_S00_AXI
	generic map (
		C_S_AXI_DATA_WIDTH	=> C_S00_AXI_DATA_WIDTH,
		C_S_AXI_ADDR_WIDTH	=> C_S00_AXI_ADDR_WIDTH
	)
	port map (
		S_AXI_ACLK	=> s00_axi_aclk,
		S_AXI_ARESETN	=> s00_axi_aresetn,
		S_AXI_AWADDR	=> s00_axi_awaddr,
		S_AXI_AWPROT	=> s00_axi_awprot,
		S_AXI_AWVALID	=> s00_axi_awvalid,
		S_AXI_AWREADY	=> s00_axi_awready,
		S_AXI_WDATA	=> s00_axi_wdata,
		S_AXI_WSTRB	=> s00_axi_wstrb,
		S_AXI_WVALID	=> s00_axi_wvalid,
		S_AXI_WREADY	=> s00_axi_wready,
		S_AXI_BRESP	=> s00_axi_bresp,
		S_AXI_BVALID	=> s00_axi_bvalid,
		S_AXI_BREADY	=> s00_axi_bready,
		S_AXI_ARADDR	=> s00_axi_araddr,
		S_AXI_ARPROT	=> s00_axi_arprot,
		S_AXI_ARVALID	=> s00_axi_arvalid,
		S_AXI_ARREADY	=> s00_axi_arready,
		S_AXI_RDATA	=> s00_axi_rdata,
		S_AXI_RRESP	=> s00_axi_rresp,
		S_AXI_RVALID	=> s00_axi_rvalid,
		S_AXI_RREADY	=> s00_axi_rready,
		

        -- FIFO instrumentation		
        fifo_instrumentation_empty => fifo_instrumentation_empty ,
        fifo_instrumentation_almost_empty => fifo_instrumentation_almost_empty ,
        fifo_instrumentation_full => fifo_instrumentation_full ,
        fifo_instrumentation_almost_full => fifo_instrumentation_almost_full ,
          
        -- FIFO TMC      
        fifo_tmc_empty => fifo_tmc_empty ,
        fifo_tmc_almost_empty => fifo_tmc_almost_empty ,           
        fifo_tmc_full => fifo_tmc_full ,
        fifo_tmc_almost_full => fifo_tmc_almost_full ,      
                    
        -- FIFO Monitor to Kernel        
        fifo_monitor_to_kernel_empty => fifo_monitor_to_kernel_empty ,
        fifo_monitor_to_kernel_almost_empty => fifo_monitor_to_kernel_almost_empty ,
        fifo_monitor_to_kernel_full   => fifo_monitor_to_kernel_full ,
        fifo_monitor_to_kernel_almost_full => fifo_monitor_to_kernel_almost_full ,   
                
        -- FIFO Kernel to Monitor
        fifo_kernel_to_monitor_empty => fifo_kernel_to_monitor_empty ,
        fifo_kernel_to_monitor_almost_empty => fifo_kernel_to_monitor_almost_empty ,
        fifo_kernel_to_monitor_full   => fifo_kernel_to_monitor_full  ,
        fifo_kernel_to_monitor_almost_full => fifo_kernel_to_monitor_almost_full ,
                    
        -- FIFO PTM
        fifo_ptm_empty => fifo_ptm_empty ,
        fifo_ptm_almost_empty => fifo_ptm_almost_empty ,
        fifo_ptm_full => fifo_ptm_full ,
        fifo_ptm_almost_full => fifo_ptm_almost_full ,
                
        -- Dispatcher
        dispatcher_write_enable => dispatcher_write_enable ,
        dispatcher_read_enable => dispatcher_read_enable ,
        dispatcher_address => dispatcher_address ,
        dispatcher_write_data_in  => dispatcher_write_data_in ,
        dispatcher_data_out  => dispatcher_data_out ,
        
        dispatcher_interruption_generate  => dispatcher_interruption_generate ,
        to_dispatcher_interruption_ack  => to_dispatcher_interruption_ack ,
 
        -- TMC
        tmc_write_enable => tmc_write_enable,
        tmc_read_enable => tmc_read_enable,
        tmc_address => tmc_address,
        tmc_write_data_in => tmc_write_data_in,
        tmc_interruption  => tmc_interruption 
     
	);

-- Instantiation of Axi Bus Interface S_AXI_INTR
PLtoPS_S_AXI_INTR_inst : PLtoPS_S_AXI_INTR
	generic map (
		C_S_AXI_DATA_WIDTH	=> C_S_AXI_INTR_DATA_WIDTH,
		C_S_AXI_ADDR_WIDTH	=> C_S_AXI_INTR_ADDR_WIDTH,
		C_NUM_OF_INTR	=> C_NUM_OF_INTR,
		C_INTR_SENSITIVITY	=> C_INTR_SENSITIVITY,
		C_INTR_ACTIVE_STATE	=> C_INTR_ACTIVE_STATE,
		C_IRQ_SENSITIVITY	=> C_IRQ_SENSITIVITY,
		C_IRQ_ACTIVE_STATE	=> C_IRQ_ACTIVE_STATE
	)
	port map (
		S_AXI_ACLK	=> s_axi_intr_aclk,
		S_AXI_ARESETN	=> s_axi_intr_aresetn,
		S_AXI_AWADDR	=> s_axi_intr_awaddr,
		S_AXI_AWPROT	=> s_axi_intr_awprot,
		S_AXI_AWVALID	=> s_axi_intr_awvalid,
		S_AXI_AWREADY	=> s_axi_intr_awready,
		S_AXI_WDATA	=> s_axi_intr_wdata,
		S_AXI_WSTRB	=> s_axi_intr_wstrb,
		S_AXI_WVALID	=> s_axi_intr_wvalid,
		S_AXI_WREADY	=> s_axi_intr_wready,
		S_AXI_BRESP	=> s_axi_intr_bresp,
		S_AXI_BVALID	=> s_axi_intr_bvalid,
		S_AXI_BREADY	=> s_axi_intr_bready,
		S_AXI_ARADDR	=> s_axi_intr_araddr,
		S_AXI_ARPROT	=> s_axi_intr_arprot,
		S_AXI_ARVALID	=> s_axi_intr_arvalid,
		S_AXI_ARREADY	=> s_axi_intr_arready,
		S_AXI_RDATA	=> s_axi_intr_rdata,
		S_AXI_RRESP	=> s_axi_intr_rresp,
		S_AXI_RVALID	=> s_axi_intr_rvalid,
		S_AXI_RREADY	=> s_axi_intr_rready,
		
		-- FIFO instrumentation		
        fifo_instrumentation_empty => fifo_instrumentation_empty ,
        fifo_instrumentation_almost_empty => fifo_instrumentation_almost_empty ,
        fifo_instrumentation_full => fifo_instrumentation_full ,
        fifo_instrumentation_almost_full => fifo_instrumentation_almost_full ,
          
        -- FIFO TMC      
        fifo_tmc_empty => fifo_tmc_empty ,
        fifo_tmc_almost_empty => fifo_tmc_almost_empty ,           
        fifo_tmc_full => fifo_tmc_full ,
        fifo_tmc_almost_full => fifo_tmc_almost_full ,             
                    
        -- FIFO Monitor to Kernel        
        fifo_monitor_to_kernel_empty => fifo_monitor_to_kernel_empty ,
        fifo_monitor_to_kernel_almost_empty => fifo_monitor_to_kernel_almost_empty ,
        fifo_monitor_to_kernel_full   => fifo_monitor_to_kernel_full ,
        fifo_monitor_to_kernel_almost_full => fifo_monitor_to_kernel_almost_full ,    
                
        -- FIFO Kernel to Monitor
        fifo_kernel_to_monitor_empty => fifo_kernel_to_monitor_empty ,
        fifo_kernel_to_monitor_almost_empty => fifo_kernel_to_monitor_almost_empty ,
        fifo_kernel_to_monitor_full   => fifo_kernel_to_monitor_full  ,
        fifo_kernel_to_monitor_almost_full => fifo_kernel_to_monitor_almost_full ,
                    
        -- FIFO PTM
        fifo_ptm_empty => fifo_ptm_empty ,
        fifo_ptm_almost_empty => fifo_ptm_almost_empty ,
        fifo_ptm_full => fifo_ptm_full ,
        fifo_ptm_almost_full => fifo_ptm_almost_full ,
        
        -- TMC and Dispatcher interrupt signal
		dispatcher_interruption_generate  => dispatcher_interruption_generate ,
        to_dispatcher_interruption_ack  => to_dispatcher_interruption_ack ,
        
		tmc_interruption  => tmc_interruption,
		
		irq	=> irq
	);

	-- Add user logic here

	-- User logic ends

end arch_imp;
