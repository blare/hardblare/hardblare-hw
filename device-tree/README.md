# Device tree

The device-tree source is in device-tree/zynq-zed.dts

We need to compile it in order to have the dtb file with the command:

```
cd device-tree
dtc -I dts -O dtb -o zynq-zed.dtb zynq-zed.dts
```

The resulting dtb file should be (with the uEnv.txt file) into the root of your SD card (for the ZedBoard).
