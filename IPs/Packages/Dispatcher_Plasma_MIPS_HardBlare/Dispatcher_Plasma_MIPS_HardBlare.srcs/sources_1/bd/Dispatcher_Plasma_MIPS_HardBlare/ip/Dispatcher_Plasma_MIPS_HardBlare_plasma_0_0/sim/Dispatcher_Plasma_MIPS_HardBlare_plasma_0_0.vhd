-- (c) Copyright 1995-2019 Xilinx, Inc. All rights reserved.
-- 
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
-- 
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
-- 
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
-- 
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES.
-- 
-- DO NOT MODIFY THIS FILE.

-- IP VLNV: xilinx.com:module_ref:plasma:1.0
-- IP Revision: 1

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY Dispatcher_Plasma_MIPS_HardBlare_plasma_0_0 IS
  PORT (
    clk : IN STD_LOGIC;
    reset : IN STD_LOGIC;
    bram_firmware_code_en : OUT STD_LOGIC;
    bram_firmware_code_reset : OUT STD_LOGIC;
    bram_firmware_code_clk : OUT STD_LOGIC;
    bram_firmware_code_address : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    bram_firmware_code_data_write : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    bram_firmware_code_bytes_selection : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    bram_firmware_code_data_read : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    bram_ptm_traces_en : OUT STD_LOGIC;
    bram_ptm_traces_reset : OUT STD_LOGIC;
    bram_ptm_traces_clk : OUT STD_LOGIC;
    bram_ptm_traces_address : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    bram_ptm_traces_data_write : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    bram_ptm_traces_bytes_selection : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    bram_ptm_traces_data_read : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    bram_bbt_annotations_en : OUT STD_LOGIC;
    bram_bbt_annotations_reset : OUT STD_LOGIC;
    bram_bbt_annotations_clk : OUT STD_LOGIC;
    bram_bbt_annotations_address : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    bram_bbt_annotations_data_write : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    bram_bbt_annotations_bytes_selection : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    bram_bbt_annotations_data_read : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    bram_annotations_en : OUT STD_LOGIC;
    bram_annotations_reset : OUT STD_LOGIC;
    bram_annotations_clk : OUT STD_LOGIC;
    bram_annotations_address : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    bram_annotations_data_write : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    bram_annotations_bytes_selection : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    bram_annotations_data_read : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    bram_monitor_to_kernel_en : OUT STD_LOGIC;
    bram_monitor_to_kernel_reset : OUT STD_LOGIC;
    bram_monitor_to_kernel_clk : OUT STD_LOGIC;
    bram_monitor_to_kernel_address : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    bram_monitor_to_kernel_data_write : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    bram_monitor_to_kernel_bytes_selection : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    bram_monitor_to_kernel_data_read : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    bram_kernel_to_monitor_en : OUT STD_LOGIC;
    bram_kernel_to_monitor_reset : OUT STD_LOGIC;
    bram_kernel_to_monitor_clk : OUT STD_LOGIC;
    bram_kernel_to_monitor_address : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    bram_kernel_to_monitor_data_write : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    bram_kernel_to_monitor_bytes_selection : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    bram_kernel_to_monitor_data_read : IN STD_LOGIC_VECTOR(31 DOWNTO 0)
  );
END Dispatcher_Plasma_MIPS_HardBlare_plasma_0_0;

ARCHITECTURE Dispatcher_Plasma_MIPS_HardBlare_plasma_0_0_arch OF Dispatcher_Plasma_MIPS_HardBlare_plasma_0_0 IS
  ATTRIBUTE DowngradeIPIdentifiedWarnings : STRING;
  ATTRIBUTE DowngradeIPIdentifiedWarnings OF Dispatcher_Plasma_MIPS_HardBlare_plasma_0_0_arch: ARCHITECTURE IS "yes";
  COMPONENT plasma IS
    GENERIC (
      memory_type : STRING;
      log_file : STRING;
      ethernet : STD_LOGIC;
      use_cache : STD_LOGIC
    );
    PORT (
      clk : IN STD_LOGIC;
      reset : IN STD_LOGIC;
      bram_firmware_code_en : OUT STD_LOGIC;
      bram_firmware_code_reset : OUT STD_LOGIC;
      bram_firmware_code_clk : OUT STD_LOGIC;
      bram_firmware_code_address : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      bram_firmware_code_data_write : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      bram_firmware_code_bytes_selection : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
      bram_firmware_code_data_read : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      bram_ptm_traces_en : OUT STD_LOGIC;
      bram_ptm_traces_reset : OUT STD_LOGIC;
      bram_ptm_traces_clk : OUT STD_LOGIC;
      bram_ptm_traces_address : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      bram_ptm_traces_data_write : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      bram_ptm_traces_bytes_selection : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
      bram_ptm_traces_data_read : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      bram_bbt_annotations_en : OUT STD_LOGIC;
      bram_bbt_annotations_reset : OUT STD_LOGIC;
      bram_bbt_annotations_clk : OUT STD_LOGIC;
      bram_bbt_annotations_address : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      bram_bbt_annotations_data_write : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      bram_bbt_annotations_bytes_selection : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
      bram_bbt_annotations_data_read : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      bram_annotations_en : OUT STD_LOGIC;
      bram_annotations_reset : OUT STD_LOGIC;
      bram_annotations_clk : OUT STD_LOGIC;
      bram_annotations_address : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      bram_annotations_data_write : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      bram_annotations_bytes_selection : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
      bram_annotations_data_read : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      bram_monitor_to_kernel_en : OUT STD_LOGIC;
      bram_monitor_to_kernel_reset : OUT STD_LOGIC;
      bram_monitor_to_kernel_clk : OUT STD_LOGIC;
      bram_monitor_to_kernel_address : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      bram_monitor_to_kernel_data_write : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      bram_monitor_to_kernel_bytes_selection : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
      bram_monitor_to_kernel_data_read : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      bram_kernel_to_monitor_en : OUT STD_LOGIC;
      bram_kernel_to_monitor_reset : OUT STD_LOGIC;
      bram_kernel_to_monitor_clk : OUT STD_LOGIC;
      bram_kernel_to_monitor_address : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      bram_kernel_to_monitor_data_write : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      bram_kernel_to_monitor_bytes_selection : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
      bram_kernel_to_monitor_data_read : IN STD_LOGIC_VECTOR(31 DOWNTO 0)
    );
  END COMPONENT plasma;
  ATTRIBUTE IP_DEFINITION_SOURCE : STRING;
  ATTRIBUTE IP_DEFINITION_SOURCE OF Dispatcher_Plasma_MIPS_HardBlare_plasma_0_0_arch: ARCHITECTURE IS "module_ref";
  ATTRIBUTE X_INTERFACE_INFO : STRING;
  ATTRIBUTE X_INTERFACE_PARAMETER : STRING;
  ATTRIBUTE X_INTERFACE_PARAMETER OF bram_kernel_to_monitor_clk: SIGNAL IS "XIL_INTERFACENAME bram_kernel_to_monitor_clk, ASSOCIATED_RESET bram_kernel_to_monitor_reset, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN Dispatcher_Plasma_MIPS_HardBlare_plasma_0_0_bram_kernel_to_monitor_clk";
  ATTRIBUTE X_INTERFACE_INFO OF bram_kernel_to_monitor_clk: SIGNAL IS "xilinx.com:signal:clock:1.0 bram_kernel_to_monitor_clk CLK";
  ATTRIBUTE X_INTERFACE_PARAMETER OF bram_kernel_to_monitor_reset: SIGNAL IS "XIL_INTERFACENAME bram_kernel_to_monitor_reset, POLARITY ACTIVE_LOW";
  ATTRIBUTE X_INTERFACE_INFO OF bram_kernel_to_monitor_reset: SIGNAL IS "xilinx.com:signal:reset:1.0 bram_kernel_to_monitor_reset RST";
  ATTRIBUTE X_INTERFACE_PARAMETER OF bram_monitor_to_kernel_clk: SIGNAL IS "XIL_INTERFACENAME bram_monitor_to_kernel_clk, ASSOCIATED_RESET bram_monitor_to_kernel_reset, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN Dispatcher_Plasma_MIPS_HardBlare_plasma_0_0_bram_monitor_to_kernel_clk";
  ATTRIBUTE X_INTERFACE_INFO OF bram_monitor_to_kernel_clk: SIGNAL IS "xilinx.com:signal:clock:1.0 bram_monitor_to_kernel_clk CLK";
  ATTRIBUTE X_INTERFACE_PARAMETER OF bram_monitor_to_kernel_reset: SIGNAL IS "XIL_INTERFACENAME bram_monitor_to_kernel_reset, POLARITY ACTIVE_LOW";
  ATTRIBUTE X_INTERFACE_INFO OF bram_monitor_to_kernel_reset: SIGNAL IS "xilinx.com:signal:reset:1.0 bram_monitor_to_kernel_reset RST";
  ATTRIBUTE X_INTERFACE_PARAMETER OF bram_annotations_clk: SIGNAL IS "XIL_INTERFACENAME bram_annotations_clk, ASSOCIATED_RESET bram_annotations_reset, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN Dispatcher_Plasma_MIPS_HardBlare_plasma_0_0_bram_annotations_clk";
  ATTRIBUTE X_INTERFACE_INFO OF bram_annotations_clk: SIGNAL IS "xilinx.com:signal:clock:1.0 bram_annotations_clk CLK";
  ATTRIBUTE X_INTERFACE_PARAMETER OF bram_annotations_reset: SIGNAL IS "XIL_INTERFACENAME bram_annotations_reset, POLARITY ACTIVE_LOW";
  ATTRIBUTE X_INTERFACE_INFO OF bram_annotations_reset: SIGNAL IS "xilinx.com:signal:reset:1.0 bram_annotations_reset RST";
  ATTRIBUTE X_INTERFACE_PARAMETER OF bram_bbt_annotations_clk: SIGNAL IS "XIL_INTERFACENAME bram_bbt_annotations_clk, ASSOCIATED_RESET bram_bbt_annotations_reset, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN Dispatcher_Plasma_MIPS_HardBlare_plasma_0_0_bram_bbt_annotations_clk";
  ATTRIBUTE X_INTERFACE_INFO OF bram_bbt_annotations_clk: SIGNAL IS "xilinx.com:signal:clock:1.0 bram_bbt_annotations_clk CLK";
  ATTRIBUTE X_INTERFACE_PARAMETER OF bram_bbt_annotations_reset: SIGNAL IS "XIL_INTERFACENAME bram_bbt_annotations_reset, POLARITY ACTIVE_LOW";
  ATTRIBUTE X_INTERFACE_INFO OF bram_bbt_annotations_reset: SIGNAL IS "xilinx.com:signal:reset:1.0 bram_bbt_annotations_reset RST";
  ATTRIBUTE X_INTERFACE_PARAMETER OF bram_ptm_traces_clk: SIGNAL IS "XIL_INTERFACENAME bram_ptm_traces_clk, ASSOCIATED_RESET bram_ptm_traces_reset, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN Dispatcher_Plasma_MIPS_HardBlare_plasma_0_0_bram_ptm_traces_clk";
  ATTRIBUTE X_INTERFACE_INFO OF bram_ptm_traces_clk: SIGNAL IS "xilinx.com:signal:clock:1.0 bram_ptm_traces_clk CLK";
  ATTRIBUTE X_INTERFACE_PARAMETER OF bram_ptm_traces_reset: SIGNAL IS "XIL_INTERFACENAME bram_ptm_traces_reset, POLARITY ACTIVE_LOW";
  ATTRIBUTE X_INTERFACE_INFO OF bram_ptm_traces_reset: SIGNAL IS "xilinx.com:signal:reset:1.0 bram_ptm_traces_reset RST";
  ATTRIBUTE X_INTERFACE_PARAMETER OF bram_firmware_code_clk: SIGNAL IS "XIL_INTERFACENAME bram_firmware_code_clk, ASSOCIATED_RESET bram_firmware_code_reset, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN Dispatcher_Plasma_MIPS_HardBlare_plasma_0_0_bram_firmware_code_clk";
  ATTRIBUTE X_INTERFACE_INFO OF bram_firmware_code_clk: SIGNAL IS "xilinx.com:signal:clock:1.0 bram_firmware_code_clk CLK";
  ATTRIBUTE X_INTERFACE_PARAMETER OF bram_firmware_code_reset: SIGNAL IS "XIL_INTERFACENAME bram_firmware_code_reset, POLARITY ACTIVE_LOW";
  ATTRIBUTE X_INTERFACE_INFO OF bram_firmware_code_reset: SIGNAL IS "xilinx.com:signal:reset:1.0 bram_firmware_code_reset RST";
  ATTRIBUTE X_INTERFACE_PARAMETER OF reset: SIGNAL IS "XIL_INTERFACENAME reset, POLARITY ACTIVE_LOW";
  ATTRIBUTE X_INTERFACE_INFO OF reset: SIGNAL IS "xilinx.com:signal:reset:1.0 reset RST";
  ATTRIBUTE X_INTERFACE_PARAMETER OF clk: SIGNAL IS "XIL_INTERFACENAME clk, ASSOCIATED_RESET reset, FREQ_HZ 100000000, PHASE 0.000";
  ATTRIBUTE X_INTERFACE_INFO OF clk: SIGNAL IS "xilinx.com:signal:clock:1.0 clk CLK";
BEGIN
  U0 : plasma
    GENERIC MAP (
      memory_type => "XILINX_16X",
      log_file => "UNUSED",
      ethernet => '0',
      use_cache => '0'
    )
    PORT MAP (
      clk => clk,
      reset => reset,
      bram_firmware_code_en => bram_firmware_code_en,
      bram_firmware_code_reset => bram_firmware_code_reset,
      bram_firmware_code_clk => bram_firmware_code_clk,
      bram_firmware_code_address => bram_firmware_code_address,
      bram_firmware_code_data_write => bram_firmware_code_data_write,
      bram_firmware_code_bytes_selection => bram_firmware_code_bytes_selection,
      bram_firmware_code_data_read => bram_firmware_code_data_read,
      bram_ptm_traces_en => bram_ptm_traces_en,
      bram_ptm_traces_reset => bram_ptm_traces_reset,
      bram_ptm_traces_clk => bram_ptm_traces_clk,
      bram_ptm_traces_address => bram_ptm_traces_address,
      bram_ptm_traces_data_write => bram_ptm_traces_data_write,
      bram_ptm_traces_bytes_selection => bram_ptm_traces_bytes_selection,
      bram_ptm_traces_data_read => bram_ptm_traces_data_read,
      bram_bbt_annotations_en => bram_bbt_annotations_en,
      bram_bbt_annotations_reset => bram_bbt_annotations_reset,
      bram_bbt_annotations_clk => bram_bbt_annotations_clk,
      bram_bbt_annotations_address => bram_bbt_annotations_address,
      bram_bbt_annotations_data_write => bram_bbt_annotations_data_write,
      bram_bbt_annotations_bytes_selection => bram_bbt_annotations_bytes_selection,
      bram_bbt_annotations_data_read => bram_bbt_annotations_data_read,
      bram_annotations_en => bram_annotations_en,
      bram_annotations_reset => bram_annotations_reset,
      bram_annotations_clk => bram_annotations_clk,
      bram_annotations_address => bram_annotations_address,
      bram_annotations_data_write => bram_annotations_data_write,
      bram_annotations_bytes_selection => bram_annotations_bytes_selection,
      bram_annotations_data_read => bram_annotations_data_read,
      bram_monitor_to_kernel_en => bram_monitor_to_kernel_en,
      bram_monitor_to_kernel_reset => bram_monitor_to_kernel_reset,
      bram_monitor_to_kernel_clk => bram_monitor_to_kernel_clk,
      bram_monitor_to_kernel_address => bram_monitor_to_kernel_address,
      bram_monitor_to_kernel_data_write => bram_monitor_to_kernel_data_write,
      bram_monitor_to_kernel_bytes_selection => bram_monitor_to_kernel_bytes_selection,
      bram_monitor_to_kernel_data_read => bram_monitor_to_kernel_data_read,
      bram_kernel_to_monitor_en => bram_kernel_to_monitor_en,
      bram_kernel_to_monitor_reset => bram_kernel_to_monitor_reset,
      bram_kernel_to_monitor_clk => bram_kernel_to_monitor_clk,
      bram_kernel_to_monitor_address => bram_kernel_to_monitor_address,
      bram_kernel_to_monitor_data_write => bram_kernel_to_monitor_data_write,
      bram_kernel_to_monitor_bytes_selection => bram_kernel_to_monitor_bytes_selection,
      bram_kernel_to_monitor_data_read => bram_kernel_to_monitor_data_read
    );
END Dispatcher_Plasma_MIPS_HardBlare_plasma_0_0_arch;
