---------------------------------------------------------------------
-- TITLE: Plasma (CPU core with memory)
-- AUTHOR: Steve Rhoads (rhoadss@yahoo.com), Mounir NASR ALLAH (mounir.nasrallah@centralesupelec.fr)
-- DATE CREATED: 6/4/02
-- FILENAME: plasma.vhd
-- PROJECT: Plasma CPU core
-- COPYRIGHT: Software placed into the public domain by the author.
--    Software 'as is' without warranty.  Author liable for nothing.
-- DESCRIPTION:
--    This entity combines the CPU core with memory and a UART.
--
-- Memory Map:
--   0x00000000 - 0x0000ffff   Internal RAM (8KB)
--   0x10000000 - 0x100fffff   External RAM (1MB)
--   Access all Misc registers with 32-bit accesses
--   0x20000000  Uart Write (will pause CPU if busy)
--   0x20000000  Uart Read
--   0x20000010  IRQ Mask
--   0x20000020  IRQ Status
--   0x20000030  GPIO0 Out Set bits
--   0x20000040  GPIO0 Out Clear bits
--   0x20000050  GPIOA In
--   0x20000060  Counter
--   0x20000070  Ethernet transmit count
--   IRQ bits:
--      7   GPIO31
--      6  ^GPIO31
--      5   EthernetSendDone
--      4   EthernetReceive
--      3   Counter(18)
--      2  ^Counter(18)
--      1  ^UartWriteBusy
--      0   UartDataAvailable
---------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use work.mlite_pack.all;

entity Dispatcher_Plasma_MIPS_HardBlare is
  generic(memory_type : string := "XILINX_16X"; --"DUAL_PORT_" "ALTERA_LPM";
         log_file    : string := "UNUSED";
         ethernet    : std_logic := '0';
         use_cache   : std_logic := '0');
   port(clk          : in std_logic;
        reset        : in std_logic;

       --
       --
       --  Firmware Code BRAM
       --------------------------------------------------------------------------
       bram_firmware_code_en: out std_logic;
       bram_firmware_code_address: out std_logic_vector(31 downto 0);
       bram_firmware_code_data_write: out std_logic_vector(31 downto 0);
       bram_firmware_code_bytes_selection: out std_logic_vector(3 downto 0);
       
       bram_firmware_code_data_read: in std_logic_vector(31 downto 0);

       bram_firmware_code_clk: out std_logic;
       bram_firmware_code_reset: out std_logic;


        --
        --  Basic Block Table + Annotations BRAM
        --------------------------------------------------------------------------
        bram_bbt_annotations_en: out std_logic;
        bram_bbt_annotations_data_read: in std_logic_vector(31 downto 0);
         
        bram_bbt_annotations_address: out std_logic_vector(31 downto 0);
        bram_bbt_annotations_data_write: out std_logic_vector(31 downto 0);
        bram_bbt_annotations_bytes_selection: out std_logic_vector(3 downto 0);
        
        bram_bbt_annotations_clk: out std_logic;
        bram_bbt_annotations_reset: out std_logic; 
         
        --
        --  FIFO PTM Traces (READ Mode)
        --------------------------------------------------------------------------
        fifo_ptm_en: out std_logic;
        fifo_ptm_data_read: in std_logic_vector(31 downto 0); 
        
        fifo_ptm_empty                           : in std_logic;
        fifo_ptm_almost_empty                    : in std_logic;
        fifo_ptm_full                            : in std_logic;
        fifo_ptm_almost_full                     : in std_logic;
        
        fifo_ptm_clk: out std_logic;
        fifo_ptm_reset: out std_logic;

        --
        --  FIFO TMC (Write mode)
        --------------------------------------------------------------------------
--        fifo_tmc_en: out std_logic;
--        fifo_tmc_data_write: out std_logic_vector(31 downto 0);
        
--        fifo_tmc_empty              : in std_logic;
--        fifo_tmc_almost_empty       : in std_logic;             
--        fifo_tmc_full               : in std_logic;
--        fifo_tmc_almost_full        : in std_logic;     
 
--        fifo_tmc_clk: out std_logic;
--        fifo_tmc_reset: out std_logic;       
                
        --
        --  Monitor to Kernel FIFO (Write mode)
        --------------------------------------------------------------------------
        fifo_monitor_to_kernel_en: out std_logic;
        fifo_monitor_to_kernel_data_write: out std_logic_vector(31 downto 0);
       
        fifo_monitor_to_kernel_empty            : in std_logic;
        fifo_monitor_to_kernel_almost_empty     : in std_logic;
        fifo_monitor_to_kernel_full             : in std_logic;
        fifo_monitor_to_kernel_almost_full      : in std_logic;      

        fifo_monitor_to_kernel_clk: out std_logic;
        fifo_monitor_to_kernel_reset: out std_logic;        
        
        --
        --  Kernel to Monitor FIFO (READ Mode)
        --------------------------------------------------------------------------
        fifo_kernel_to_monitor_en: out std_logic;
        fifo_kernel_to_monitor_data_read: in std_logic_vector(31 downto 0);
       
        fifo_kernel_to_monitor_empty                : in std_logic;
        fifo_kernel_to_monitor_almost_empty         : in std_logic;     
        fifo_kernel_to_monitor_full                 : in std_logic;
        fifo_kernel_to_monitor_almost_full          : in std_logic;    

        fifo_kernel_to_monitor_clk: out std_logic;
        fifo_kernel_to_monitor_reset: out std_logic;

        --
        --  Instrumentation FIFO
        --------------------------------------------------------------------------
        fifo_read_instrumentation_request_read_en: out std_logic;
        fifo_read_instrumentation_data_read: in std_logic_vector(31 downto 0);
        
        fifo_instrumentation_empty            : in std_logic;
        fifo_instrumentation_almost_empty     : in std_logic;
        fifo_instrumentation_full             : in std_logic; 
        fifo_instrumentation_almost_full      : in std_logic;  
   
        
        --
        --  Debug BRAM
        --------------------------------------------------------------------------
        bram_debug_en: out std_logic;
        bram_debug_address: out std_logic_vector(31 downto 0);
        bram_debug_data_write: out std_logic_vector(31 downto 0);
        bram_debug_bytes_selection: out std_logic_vector(3 downto 0);   
        bram_debug_data_read: in std_logic_vector(31 downto 0); 
      
        bram_debug_clk: out std_logic;
        bram_debug_reset: out std_logic;
      
        --
        --  PLtoPS (Interrupt)
        --------------------------------------------------------------------------  
        PLtoPS_clk: out std_logic;
        PLtoPS_reset: out std_logic;
        PLtoPS_enable: out std_logic;
        PLtoPS_address: out std_logic_vector(31 downto 0);
        PLtoPS_write_data: out std_logic_vector(31 downto 0);
        PLtoPS_bytes_selection: out std_logic_vector(3 downto 0);
        PLtoPS_readed_data: in std_logic_vector(31 downto 0);
        PLtoPS_generate_interrupt: out std_logic;
        
        --
        --  Security Policy Config BRAM
        --------------------------------------------------------------------------
        bram_sec_policy_config_en: out std_logic;
        bram_sec_policy_config_data_read: in std_logic_vector(31 downto 0);
         
        bram_sec_policy_config_address: out std_logic_vector(31 downto 0);
        bram_sec_policy_config_data_write: out std_logic_vector(31 downto 0);
        bram_sec_policy_config_bytes_selection: out std_logic_vector(3 downto 0);
        
        bram_sec_policy_config_clk: out std_logic;
        bram_sec_policy_config_reset: out std_logic
      
);
        
end; --entity plasma

architecture logic of Dispatcher_Plasma_MIPS_HardBlare is

   signal address_next      : std_logic_vector(31 downto 2);
   signal byte_we_next      : std_logic_vector(3 downto 0);
   
   signal complete_address_next      : std_logic_vector(31 downto 0);
   
   signal cpu_address       : std_logic_vector(31 downto 0);
   
   signal cache_word_read       : std_logic_vector(31 downto 0);   
   
   signal cpu_address_selection_of_memory_area : std_logic_vector(3 downto 0);
   signal cpu_address_misc_ip_selection_of_memory : std_logic_vector(3 downto 0);
   signal cpu_address_misc_signals_selection_of_memory : std_logic_vector(3 downto 0);
   signal cpu_address_cleaned       : std_logic_vector(31 downto 0);
   
   signal address_next_selection_of_memory_area : std_logic_vector(3 downto 0);
   signal address_next_misc_ip_selection_of_memory : std_logic_vector(3 downto 0);
   signal address_next_misc_signals_selection_of_memory : std_logic_vector(3 downto 0);
   signal address_next_cleaned       : std_logic_vector(31 downto 0);
   
   -- *****************************
   -- Memories
   -- *****************************
   -- [0x00000000] - BRAM Firmware code 
   constant MEMORY_BRAM_FIRMWARE : std_logic_vector(3 downto 0):= "0000";
   -- [0x10000000] - BRAM - Basic Block Table + Annotations  
   constant MEMORY_BRAM_BBT_ANNOTATIONS : std_logic_vector(3 downto 0):= "0001";
   -- [0x20000000] - FIFO PTM Traces  
   constant MEMORY_FIFO_PTM : std_logic_vector(3 downto 0):= "0010";
   -- [0x30000000] - FIFO Instrumentation read  
   constant MEMORY_FIFO_INSTRUMENTATION : std_logic_vector(3 downto 0):= "0011";
   -- [0x40000000] - FIFO Kernel to Monitor  
   constant MEMORY_FIFO_KERNEL_TO_MONITOR : std_logic_vector(3 downto 0):= "0100";
   -- [0x50000000] - FIFO Monitor to Kernel  
   constant MEMORY_FIFO_MONITOR_TO_KERNEL : std_logic_vector(3 downto 0):= "0101";
   -- [0x60000000] - FIFO TMC
--   constant MEMORY_FIFO_TMC : std_logic_vector(3 downto 0):= "0110";
   -- [0x70000000] - PL to PS data  
   constant MEMORY_PLTOPS : std_logic_vector(3 downto 0):= "0111";
   -- [0x80000000] - Misc Area / Signals 
   constant MEMORY_MISC : std_logic_vector(3 downto 0):= "1000";
   -- [0x90000000] - Debug
   constant MEMORY_DEBUG : std_logic_vector(3 downto 0):= "1001";
   -- [0xA0000000] - Security Policy Config 
   constant MEMORY_SECURITY_POLICY_CONFIG : std_logic_vector(3 downto 0):= "1010";

   
   -- *****************************
   -- IP Selection of memory
   -- *****************************
   -- [0x80000000] - BRAM Firmware code 
   constant MISC_BRAM_FIRMWARE : std_logic_vector(3 downto 0):= "0000";
   -- [0x81000000] - BRAM - Basic Block Table + Annotations a l'adresse  
   constant MISC_BRAM_BBT_ANNOTATIONS : std_logic_vector(3 downto 0):= "0001";
   -- [0x82000000] - FIFO PTM Traces a l'adresse  
   constant MISC_FIFO_PTM : std_logic_vector(3 downto 0):= "0010";
   -- [0x83000000] - FIFO Instrumentation read  
   constant MISC_FIFO_INSTRUMENTATION : std_logic_vector(3 downto 0):= "0011";
   -- [0x84000000] - FIFO Kernel to Monitor a l'adresse 
   constant MISC_FIFO_KERNEL_TO_MONITOR : std_logic_vector(3 downto 0):= "0100";
   -- [0x85000000] - FIFO Monitor to Kernel a l'adresse 
   constant MISC_FIFO_MONITOR_TO_KERNEL : std_logic_vector(3 downto 0):= "0101";
   -- [0x86000000] - FIFO TMC
--   constant MISC_FIFO_TMC : std_logic_vector(3 downto 0):= "0110";
   -- [0x87000000] - PL to PS data  
   constant MISC_PLTOPS : std_logic_vector(3 downto 0):= "0111";
   -- [0x88000000] - Misc Area / Signals a l'adresse 
   constant MISC_MISC : std_logic_vector(3 downto 0):= "1000";
   -- [0x89000000] - Debug a l'adresse
   constant MISC_DEBUG : std_logic_vector(3 downto 0):= "1001";
   
   
   -- *****************************
   -- Misc Signals 
   -- *****************************
   constant EMPTY_SIGNAL : std_logic_vector(3 downto 0):= "0001";
   constant ALMOST_EMPTY_SIGNAL : std_logic_vector(3 downto 0):= "0010";
   constant FULL_SIGNAL : std_logic_vector(3 downto 0):= "0011";
   constant ALMOST_FULL_SIGNAL : std_logic_vector(3 downto 0):= "0100";
   constant GENERATE_INTERRUPT_SIGNAL : std_logic_vector(3 downto 0):= "0101";
   constant ACK_INTERRUPT_SIGNAL : std_logic_vector(3 downto 0):= "0110";
   
   
   signal cpu_byte_we       : std_logic_vector(3 downto 0);
   signal cpu_data_w        : std_logic_vector(31 downto 0);
   signal cpu_data_r        : std_logic_vector(31 downto 0);
   signal cpu_pause         : std_logic;

   signal data_read_uart    : std_logic_vector(7 downto 0);
   signal write_enable      : std_logic;
   signal eth_pause_in      : std_logic;
   signal eth_pause         : std_logic;
   signal mem_busy          : std_logic;

   signal enable_misc       : std_logic;
   signal enable_uart       : std_logic;
   signal enable_uart_read  : std_logic;
   signal enable_uart_write : std_logic;
   signal enable_eth        : std_logic;

   signal uart_write_busy   : std_logic;
   signal uart_data_avail   : std_logic;
   signal irq_mask_reg      : std_logic_vector(7 downto 0);
   signal irq_status        : std_logic_vector(7 downto 0);
   signal irq               : std_logic;
   signal irq_eth_rec       : std_logic;
   signal irq_eth_send      : std_logic;
   signal counter_reg       : std_logic_vector(31 downto 0);

   signal ram_enable        : std_logic;
   signal ram_byte_we       : std_logic_vector(3 downto 0);
   signal ram_address       : std_logic_vector(31 downto 2);
   signal ram_data_w        : std_logic_vector(31 downto 0);
   signal ram_data_r        : std_logic_vector(31 downto 0);

   signal cache_access      : std_logic;
   signal cache_checking    : std_logic;
   signal cache_miss        : std_logic;
   signal cache_hit         : std_logic;
   

begin  --architecture

   write_enable <= '1' when cpu_byte_we /= "0000" else '0';
   
   mem_busy <= '0'; -- mem_pause_in;
   
   cache_hit <= cache_checking and not cache_miss;
      
   cpu_pause <= '0';
      
   cpu_address(1 downto 0) <= "00";
   
   complete_address_next <= address_next & "00";
   
   
   -- ***********************************************************************
   -- **  CPU Address
   -- ***********************************************************************
   -- On determine la BRAM impliquee dans l'acces memoire
   cpu_address_selection_of_memory_area <= cpu_address(31 downto 28);
   -- Misc IP selection
   cpu_address_misc_ip_selection_of_memory <= cpu_address(27 downto 24);
   -- Misc Signals selection
   cpu_address_misc_signals_selection_of_memory <= cpu_address(23 downto 20);
   -- On nettoie l'adresse pour avoir une adresse basse (0x00000004)
   cpu_address_cleaned <= (cpu_address_selection_of_memory_area & cpu_address_misc_ip_selection_of_memory & cpu_address_misc_signals_selection_of_memory & "00000000000000000000") xor (cpu_address) ;
  
  
   -- ***********************************************************************
   -- **  Address Next
   -- ***********************************************************************
   -- On determine la BRAM impliquee dans l'acces memoire
   address_next_selection_of_memory_area <= complete_address_next(31 downto 28);
   -- Misc IP selection
   address_next_misc_ip_selection_of_memory <= complete_address_next(27 downto 24);
   -- Misc Signals selection
   address_next_misc_signals_selection_of_memory <= complete_address_next(23 downto 20);
   -- On nettoie l'adresse pour avoir une adresse basse (0x00000004)
   address_next_cleaned <= (address_next_selection_of_memory_area & address_next_misc_ip_selection_of_memory & address_next_misc_signals_selection_of_memory & "00000000000000000000") xor (complete_address_next) ;
   

   --------------------------------------------------------
   
   u1_cpu: mlite_cpu 
      PORT MAP (
         clk          => clk,
         reset_in     => reset,
         intr_in      => irq,
 
         address_next => address_next,             --before rising_edge(clk)
         byte_we_next => byte_we_next,

         address      => cpu_address(31 downto 2), --after rising_edge(clk)
         byte_we      => cpu_byte_we,
         data_w       => cpu_data_w,
         data_r       => cpu_data_r,
         mem_pause    => cpu_pause);

   
   opt_cache: if use_cache = '0' generate
        cache_access <= '0';
        cache_checking <= '0';
        cache_miss <= '0';
   end generate;

   
      misc_proc: process(clk, 
      reset,
      cpu_address,
      cpu_address_selection_of_memory_area,
      cpu_address_misc_ip_selection_of_memory,
      cpu_address_misc_signals_selection_of_memory,
      cpu_address_cleaned,
      
      fifo_ptm_data_read, 
      bram_bbt_annotations_data_read, 
      fifo_kernel_to_monitor_data_read , 
      bram_firmware_code_data_read,
      bram_debug_data_read,
      fifo_read_instrumentation_data_read,
      PLtoPS_readed_data,
      bram_sec_policy_config_data_read,
    
      cpu_pause, 
      write_enable,
      cache_checking,
      counter_reg, 
      cpu_data_w
      
      )
   begin

    case cpu_address_selection_of_memory_area is 

        -- [0x00000000] - BRAM Firmware code 
        when MEMORY_BRAM_FIRMWARE => 
            cpu_data_r <= bram_firmware_code_data_read;
      
        -- [0x10000000] - BRAM - Basic Block Table + Annotations a l'adresse  
        when MEMORY_BRAM_BBT_ANNOTATIONS =>                
            cpu_data_r <= bram_bbt_annotations_data_read;
                             
        -- [0x20000000] - FIFO PTM Traces a l'adresse   
        when MEMORY_FIFO_PTM => 
            cpu_data_r <= fifo_ptm_data_read;
                       
        -- [0x30000000] - FIFO Instrumentation read  
        when MEMORY_FIFO_INSTRUMENTATION => 
            cpu_data_r <= fifo_read_instrumentation_data_read;    
                                    
        -- [0x40000000] - FIFO Kernel to Monitor a l'adresse 
        when MEMORY_FIFO_KERNEL_TO_MONITOR => 
            cpu_data_r <= fifo_kernel_to_monitor_data_read;
                   
        -- [0x50000000] - FIFO Monitor to Kernel a l'adresse 
        when MEMORY_FIFO_MONITOR_TO_KERNEL => 
            -- nothing to do because we just write data to PS.
            cpu_data_r <= ZERO;
            --cpu_data_r <= fifo_monitor_to_kernel_data_read;                   
                    
        -- [0x60000000] - FIFO TMC     
--        when MEMORY_FIFO_TMC =>  -- NOTHING TO DO       
--            cpu_data_r <= ZERO;    
                                                            
        -- [0x70000000] - PL to PS data     
        when MEMORY_PLTOPS => 
            cpu_data_r <= PLtoPS_readed_data;
                            
        -- [0x80000000] - Misc Area / Signals a l'adresse 
        when MEMORY_MISC => 
    
            -- Selection du signal 
            case cpu_address_misc_ip_selection_of_memory is
            
                -- FIFO PTMl
                ------------
                when MISC_FIFO_PTM =>
                    case cpu_address_misc_signals_selection_of_memory is
                        when EMPTY_SIGNAL => 
                            cpu_data_r <= "0000000000000000000000000000000" & fifo_ptm_empty;
                        when ALMOST_EMPTY_SIGNAL => 
                            cpu_data_r <= "0000000000000000000000000000000" & fifo_ptm_almost_empty;   
                        when FULL_SIGNAL => 
                            cpu_data_r <= "0000000000000000000000000000000" & fifo_ptm_full;   
                        when ALMOST_FULL_SIGNAL => 
                            cpu_data_r <= "0000000000000000000000000000000" & fifo_ptm_almost_full;  
                        when others =>  
                            cpu_data_r <= ZERO;
                    end case;
                    
                -- FIFO INSTRUMENTATION
                ------------
                when MISC_FIFO_INSTRUMENTATION =>
                    case cpu_address_misc_signals_selection_of_memory is
                        when EMPTY_SIGNAL => 
                            cpu_data_r <= "0000000000000000000000000000000" & fifo_instrumentation_empty;
                        when ALMOST_EMPTY_SIGNAL => 
                            cpu_data_r <= "0000000000000000000000000000000" & fifo_instrumentation_almost_empty;   
                        when FULL_SIGNAL => 
                            cpu_data_r <= "0000000000000000000000000000000" & fifo_instrumentation_full;   
                        when ALMOST_FULL_SIGNAL => 
                            cpu_data_r <= "0000000000000000000000000000000" & fifo_instrumentation_almost_full; 
                        when others =>  
                            cpu_data_r <= ZERO;
                    end case;
                
                -- FIFO TMC
                ------------
--                when MISC_FIFO_TMC =>
--                    case cpu_address_misc_signals_selection_of_memory is
--                        when EMPTY_SIGNAL => 
--                            cpu_data_r <= "0000000000000000000000000000000" & fifo_tmc_empty;
--                        when ALMOST_EMPTY_SIGNAL => 
--                            cpu_data_r <= "0000000000000000000000000000000" & fifo_tmc_almost_empty;   
--                        when FULL_SIGNAL => 
--                            cpu_data_r <= "0000000000000000000000000000000" & fifo_tmc_full;   
--                        when ALMOST_FULL_SIGNAL => 
--                            cpu_data_r <= "0000000000000000000000000000000" & fifo_tmc_almost_full;  
--                        when others =>  
--                            cpu_data_r <= ZERO;
--                    end case;                    
                    
                              
                -- FIFO KERNEL TO MONITOR
                ------------
                when MISC_FIFO_KERNEL_TO_MONITOR =>
                    case cpu_address_misc_signals_selection_of_memory is
                        when EMPTY_SIGNAL => 
                            cpu_data_r <= "0000000000000000000000000000000" & fifo_kernel_to_monitor_empty;
                        when ALMOST_EMPTY_SIGNAL => 
                            cpu_data_r <= "0000000000000000000000000000000" & fifo_kernel_to_monitor_almost_empty;   
                        when FULL_SIGNAL => 
                            cpu_data_r <= "0000000000000000000000000000000" & fifo_kernel_to_monitor_full;   
                        when ALMOST_FULL_SIGNAL => 
                            cpu_data_r <= "0000000000000000000000000000000" & fifo_kernel_to_monitor_almost_full; 
                        when others =>  
                            cpu_data_r <= ZERO;
                    end case;
        
              
                -- FIFO MONITOR TO KERNEL
                ------------
                when MISC_FIFO_MONITOR_TO_KERNEL =>
                    case cpu_address_misc_signals_selection_of_memory is
                        when EMPTY_SIGNAL => 
                            cpu_data_r <= "0000000000000000000000000000000" & fifo_monitor_to_kernel_empty;
                        when ALMOST_EMPTY_SIGNAL => 
                            cpu_data_r <= "0000000000000000000000000000000" & fifo_monitor_to_kernel_almost_empty;   
                        when FULL_SIGNAL => 
                            cpu_data_r <= "0000000000000000000000000000000" & fifo_monitor_to_kernel_full;   
                        when ALMOST_FULL_SIGNAL => 
                            cpu_data_r <= "0000000000000000000000000000000" & fifo_monitor_to_kernel_almost_full;  
                        when others =>  
                            cpu_data_r <= ZERO;
                    end case;      
                
              when others => 
                 cpu_data_r <= ZERO;
        end case;                                                                                            
                          
        -- [0x90000000] - Debug 
        when MEMORY_DEBUG => 
            cpu_data_r <= bram_debug_data_read;
        
        -- [0xA0000000] - Security Policy Config BRAM
        when MEMORY_SECURITY_POLICY_CONFIG => 
            cpu_data_r <= bram_sec_policy_config_data_read;
            
        when others =>
            cpu_data_r <= ZERO;
            
    end case;             

    if reset = '1' then
        counter_reg <= ZERO;
    elsif rising_edge(clk) then
        counter_reg <= bv_inc(counter_reg);
    end if;   
  end process;

   ram_proc: process(address_next, cpu_address,
                     byte_we_next, cpu_data_w,
                     address_next,
                     address_next_selection_of_memory_area,
                     address_next_misc_ip_selection_of_memory,
                     address_next_misc_signals_selection_of_memory,
                     address_next_cleaned,
                     
                     bram_firmware_code_data_read,
                     bram_bbt_annotations_data_read,
                     fifo_ptm_data_read, 
                     fifo_kernel_to_monitor_data_read,
                     fifo_read_instrumentation_data_read,
                     bram_debug_data_read, 
                     PLtoPS_readed_data,
                     bram_sec_policy_config_data_read
           )
   begin
  
    --if(PLtoPS_ack_interrupt = '1') then
        --PLtoPS_generate_interrupt <= '0';
    --end if;
    
    case address_next_selection_of_memory_area is
    
    -- [0x00000000] - BRAM Firmware code 
    when MEMORY_BRAM_FIRMWARE  =>
         fifo_ptm_en <= '0';
--         fifo_tmc_en <= '0';
         bram_bbt_annotations_en <= '0';     
         fifo_monitor_to_kernel_en <= '0';
         fifo_kernel_to_monitor_en <= '0';
         bram_firmware_code_en <= '1';
         bram_debug_en <= '0';
         fifo_read_instrumentation_request_read_en <= '0';
         PLtoPS_enable <= '0';
         PLtoPS_generate_interrupt <= '0';
         bram_sec_policy_config_en <= '0';
                 
         bram_firmware_code_bytes_selection <= byte_we_next; 
         bram_firmware_code_address <= address_next_cleaned;
         bram_firmware_code_data_write <= cpu_data_w;
    
    -- [0x10000000] - BRAM - Basic Block Table + Annotations a l'adresse  
    when MEMORY_BRAM_BBT_ANNOTATIONS => 
        fifo_ptm_en <= '0';
        bram_bbt_annotations_en <= '1';
--        fifo_tmc_en <= '0';
        fifo_monitor_to_kernel_en <= '0';
        fifo_kernel_to_monitor_en <= '0';
        bram_firmware_code_en <= '0';
        bram_debug_en <= '0';
        fifo_read_instrumentation_request_read_en <= '0';
        PLtoPS_enable <= '0';
        PLtoPS_generate_interrupt <= '0';
        bram_sec_policy_config_en <= '0';
                
        bram_bbt_annotations_bytes_selection <= byte_we_next; 
        bram_bbt_annotations_address <= address_next_cleaned;
        bram_bbt_annotations_data_write <= cpu_data_w;    
          
    --  [0x20000000] - PTM Traces a l'adresse  
    when MEMORY_FIFO_PTM =>
        bram_bbt_annotations_en <= '0';
--        fifo_tmc_en <= '0';
        fifo_monitor_to_kernel_en <= '0';
        fifo_kernel_to_monitor_en <= '0';
        bram_firmware_code_en <= '0';
        bram_debug_en <= '0';
        fifo_read_instrumentation_request_read_en <= '0';
        PLtoPS_enable <= '0';
        PLtoPS_generate_interrupt <= '0';
        bram_sec_policy_config_en <= '0';
        
        if byte_we_next = "0000" then
            fifo_ptm_en <= '1';
        else 
            fifo_ptm_en <= '0';
        end if;

    
    -- [0x30000000] - FIFO Instrumentation read  
    when MEMORY_FIFO_INSTRUMENTATION  =>
        bram_bbt_annotations_en <= '0';
--        fifo_tmc_en <= '0';
        fifo_monitor_to_kernel_en <= '0';
        fifo_kernel_to_monitor_en <= '0';
        bram_firmware_code_en <= '0';
        bram_debug_en <= '0';
        PLtoPS_enable <= '0';
        PLtoPS_generate_interrupt <= '0';
        bram_sec_policy_config_en <= '0';
                
        fifo_ptm_en <= '0';
        
        fifo_read_instrumentation_request_read_en <= '1';  


    --  [0x40000000] - FIFO Kernel to Monitor  
    when MEMORY_FIFO_KERNEL_TO_MONITOR =>
        bram_bbt_annotations_en <= '0';
--        fifo_tmc_en <= '0';
        fifo_monitor_to_kernel_en <= '0';
        bram_firmware_code_en <= '0';
        bram_debug_en <= '0';
        fifo_read_instrumentation_request_read_en <= '0';
        PLtoPS_enable <= '0'; 
        PLtoPS_generate_interrupt <= '0';
        bram_sec_policy_config_en <= '0';        
        fifo_ptm_en <= '0';
        
        if byte_we_next= "0000" then
            fifo_kernel_to_monitor_en <= '1';
        else
            fifo_kernel_to_monitor_en <= '0';
        end if;
         
    --  [0x50000000] - Monitor to Kernel
    when  MEMORY_FIFO_MONITOR_TO_KERNEL  =>         
        fifo_ptm_en <= '0';
--        fifo_tmc_en <= '0';
        bram_bbt_annotations_en <= '0';       
        fifo_kernel_to_monitor_en <= '0';
        bram_firmware_code_en <= '0';
        bram_debug_en <= '0';
        fifo_read_instrumentation_request_read_en <= '0';
        PLtoPS_enable <= '0';
        PLtoPS_generate_interrupt <= '0';
        bram_sec_policy_config_en <= '0';
               
        fifo_monitor_to_kernel_en <= '1';                   -- WRITE ONLY
        fifo_monitor_to_kernel_data_write <= cpu_data_w;    -- WRITE ONLY
       

    -- [0x60000000] - FIFO TMC a l'adresse 0x60000000
--    when MEMORY_FIFO_TMC  =>
--        fifo_ptm_en <= '0';
--        bram_bbt_annotations_en <= '0';
--        fifo_monitor_to_kernel_en <= '0';
--        fifo_kernel_to_monitor_en <= '0';
--        bram_firmware_code_en <= '0';
--        bram_debug_en <= '0';
--        fifo_read_instrumentation_request_read_en <= '0';
--        PLtoPS_enable <= '0';
--        PLtoPS_generate_interrupt <= '0';
--        bram_sec_policy_config_en <= '0';
        
--        if byte_we_next= "1111" then
--            fifo_tmc_en <= '1';
--        else
--            fifo_tmc_en <= '0';
--        end if;
               
--        fifo_tmc_data_write <= cpu_data_w;
 
 
    -- [0x70000000] - PL to PS data
    when MEMORY_PLTOPS  =>
    
        fifo_ptm_en <= '0';
        bram_bbt_annotations_en <= '0';
--        fifo_tmc_en <= '0';
        fifo_monitor_to_kernel_en <= '0';
        fifo_kernel_to_monitor_en <= '0';
        bram_firmware_code_en <= '0';
        bram_debug_en <= '0';
        fifo_read_instrumentation_request_read_en <= '0';
        PLtoPS_generate_interrupt <= '0';
        bram_sec_policy_config_en <= '0';
                        
                    --
        --  PLtoPS (Interrupt)
        --------------------------------------------------------------------------          
        if byte_we_next= "1111" then
            -- Write
            PLtoPS_enable <= '1';
            PLtoPS_address <= address_next_cleaned;
            PLtoPS_write_data <= cpu_data_w;
            PLtoPS_bytes_selection <= byte_we_next;
            
        else
            -- Read
            PLtoPS_enable <= '1';
            PLtoPS_address <= address_next_cleaned;
            PLtoPS_bytes_selection <= byte_we_next;
            
        end if;          
                
    -- [0x80000000] - Misc Area / Signals a l'adresse 
    when MEMORY_MISC => 
    
        fifo_ptm_en <= '0';
        bram_bbt_annotations_en <= '0';
--        fifo_tmc_en <= '0';
        fifo_monitor_to_kernel_en <= '0';
        fifo_kernel_to_monitor_en <= '0';
        bram_firmware_code_en <= '0';
        bram_debug_en <= '0';
        fifo_read_instrumentation_request_read_en <= '0';
        PLtoPS_enable <= '0';
        PLtoPS_generate_interrupt <= '0';
        bram_sec_policy_config_en <= '0';
                    
          -- Selection du signal 
        case address_next_misc_ip_selection_of_memory is

           -- FIFO PTM
           ------------
           when MISC_FIFO_PTM =>
                -- Nothing to do

           -- FIFO INSTRUMENTATION
           ------------
           when MISC_FIFO_INSTRUMENTATION =>
                -- Nothing to do
                
           -- FIFO TMC
           ------------
--           when MISC_FIFO_TMC =>
                -- Nothing to do
                
           -- FIFO KERNEL TO MONITOR
           ------------
           when MISC_FIFO_KERNEL_TO_MONITOR =>
                -- Nothing to do
                
           -- FIFO MONITOR TO KERNEL
           ------------
           when MISC_FIFO_MONITOR_TO_KERNEL =>
                -- Nothing to do
                
           -- TMC
           ------------
--           when MISC_FIFO_TMC =>  
--                case address_next_misc_signals_selection_of_memory is
--                    -- when ACK_INTERRUPT_SIGNAL => 
--                    --  signal_TMC_ack_interrupt <= '1';  
--                    when others =>   
--                end case; 
                                  
           -- PL to PS
           ------------
           when MISC_PLTOPS =>  
                case address_next_misc_signals_selection_of_memory is
                    when GENERATE_INTERRUPT_SIGNAL =>   
                           -- If an interrupt is generated
                           PLtoPS_generate_interrupt <= '1';  
                    when ACK_INTERRUPT_SIGNAL => 
--                        
                    when others =>   
                end case; 
                                                     
            when others =>
               
        end case;
        
        
                              
        -- Debug 0x90000000
        when MEMORY_DEBUG =>
            fifo_ptm_en <= '0';
--            fifo_tmc_en <= '0';
            bram_bbt_annotations_en <= '0';        
            fifo_monitor_to_kernel_en <= '0';
            fifo_kernel_to_monitor_en <= '0';
            bram_firmware_code_en <= '0';
            bram_debug_en <= '1';
            fifo_read_instrumentation_request_read_en <= '0';
            PLtoPS_enable <= '0';
            PLtoPS_generate_interrupt <= '0';
            bram_sec_policy_config_en <= '0';
            
            bram_debug_bytes_selection <= byte_we_next; 
            bram_debug_address <= address_next_cleaned;
            bram_debug_data_write <= cpu_data_w;
            
            
        -- Security Policy Config 0xA0000000
        when MEMORY_SECURITY_POLICY_CONFIG =>
        
            fifo_ptm_en <= '0';
--            fifo_tmc_en <= '0';
            bram_bbt_annotations_en <= '0';        
            fifo_monitor_to_kernel_en <= '0';
            fifo_kernel_to_monitor_en <= '0';
            bram_firmware_code_en <= '0';
            bram_debug_en <= '0';
            fifo_read_instrumentation_request_read_en <= '0';
            PLtoPS_enable <= '0';
            PLtoPS_generate_interrupt <= '0';
            bram_sec_policy_config_en <= '1';
         
            bram_sec_policy_config_bytes_selection <= byte_we_next; 
            bram_sec_policy_config_address <= address_next_cleaned;
            bram_sec_policy_config_data_write <= cpu_data_w;
        when others =>     

    end case;
 
   end process;
   
    bram_sec_policy_config_clk <= clk ;
    bram_sec_policy_config_reset <= reset ;
    bram_firmware_code_clk <= clk ;
    bram_firmware_code_reset <= reset ;
    bram_bbt_annotations_clk <= clk ;
    bram_bbt_annotations_reset <= reset ;
    fifo_ptm_clk <= clk ;
    fifo_ptm_reset <= reset ;
--    fifo_tmc_clk <= clk ;
--    fifo_tmc_reset <= reset ;                  
    fifo_monitor_to_kernel_clk <= clk ;
    fifo_monitor_to_kernel_reset <= reset ;            
    fifo_kernel_to_monitor_clk <= clk ;
    fifo_kernel_to_monitor_reset <= reset ;
    bram_debug_clk <= clk ;
    bram_debug_reset <= reset ;            
    PLtoPS_clk <= clk ;
    PLtoPS_reset <= reset ;   
                                             
                                                                       
end; --architecture logic
