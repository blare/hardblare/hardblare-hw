-- Copyright 1986-2014 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2014.4 (win64) Build 1071353 Tue Nov 18 18:29:27 MST 2014
-- Date        : Tue Jun 14 16:26:45 2016
-- Host        : RPHPZB1401 running 64-bit Service Pack 1  (build 7601)
-- Command     : write_vhdl -mode funcsim -nolib -force -file
--               C:/Users/wahab_muh.RPHPZB1401/Documents/these/these_05_06_2016/decodeur_traces/decodeur_traces.sim/sim_1/synth/func/test_bench_func_synth.vhd
-- Design      : decodeur_traces
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decode_bap is
  port (
    clk : in STD_LOGIC;
    reset : in STD_LOGIC;
    start_b : in STD_LOGIC;
    enable : in STD_LOGIC;
    enable_i : in STD_LOGIC;
    data_in : in STD_LOGIC_VECTOR ( 7 downto 0 );
    instruction_address : in STD_LOGIC_VECTOR ( 31 downto 0 );
    stop_b : out STD_LOGIC;
    w_en : out STD_LOGIC;
    atom_output_e : out STD_LOGIC;
    pc : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
end decode_bap;

architecture STRUCTURE of decode_bap is
  signal \<const0>\ : STD_LOGIC;
  signal data_reg_s : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal instruction_address_reg : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \n_0_output_data_4[0]_i_1\ : STD_LOGIC;
  signal \n_0_output_data_4[1]_i_1\ : STD_LOGIC;
  signal \n_0_output_data_4[2]_i_1\ : STD_LOGIC;
  signal \n_0_pc[10]_i_2\ : STD_LOGIC;
  signal \n_0_pc[11]_i_2\ : STD_LOGIC;
  signal \n_0_pc[12]_i_2\ : STD_LOGIC;
  signal \n_0_pc[13]_i_2\ : STD_LOGIC;
  signal \n_0_pc[14]_i_2\ : STD_LOGIC;
  signal \n_0_pc[15]_i_2\ : STD_LOGIC;
  signal \n_0_pc[16]_i_2\ : STD_LOGIC;
  signal \n_0_pc[17]_i_2\ : STD_LOGIC;
  signal \n_0_pc[18]_i_2\ : STD_LOGIC;
  signal \n_0_pc[19]_i_2\ : STD_LOGIC;
  signal \n_0_pc[20]_i_2\ : STD_LOGIC;
  signal \n_0_pc[21]_i_2\ : STD_LOGIC;
  signal \n_0_pc[22]_i_2\ : STD_LOGIC;
  signal \n_0_pc[23]_i_2\ : STD_LOGIC;
  signal \n_0_pc[24]_i_2\ : STD_LOGIC;
  signal \n_0_pc[25]_i_2\ : STD_LOGIC;
  signal \n_0_pc[26]_i_2\ : STD_LOGIC;
  signal \n_0_pc[27]_i_2\ : STD_LOGIC;
  signal \n_0_pc[28]_i_2\ : STD_LOGIC;
  signal \n_0_pc[29]_i_2\ : STD_LOGIC;
  signal \n_0_pc[30]_i_2\ : STD_LOGIC;
  signal \n_0_pc[31]_i_2\ : STD_LOGIC;
  signal \n_0_pc[31]_i_3\ : STD_LOGIC;
  signal \n_0_pc[7]_i_2\ : STD_LOGIC;
  signal \n_0_pc[8]_i_2\ : STD_LOGIC;
  signal \n_0_pc[9]_i_2\ : STD_LOGIC;
  signal \n_0_state_next_reg[0]_i_1\ : STD_LOGIC;
  signal \n_0_state_next_reg[0]_i_2\ : STD_LOGIC;
  signal \n_0_state_next_reg[1]_i_1\ : STD_LOGIC;
  signal \n_0_state_next_reg[1]_i_2\ : STD_LOGIC;
  signal \n_0_state_next_reg[1]_i_3\ : STD_LOGIC;
  signal \n_0_state_next_reg[2]_i_1\ : STD_LOGIC;
  signal \n_0_state_next_reg[3]_i_1\ : STD_LOGIC;
  signal \n_0_state_next_reg[3]_i_2\ : STD_LOGIC;
  signal \n_0_state_reg[0]_i_1\ : STD_LOGIC;
  signal \n_0_state_reg[1]_i_1\ : STD_LOGIC;
  signal \n_0_state_reg[2]_i_1\ : STD_LOGIC;
  signal \n_0_state_reg[3]_i_1\ : STD_LOGIC;
  signal \n_0_state_reg_reg[0]\ : STD_LOGIC;
  signal n_0_w_en_i_1 : STD_LOGIC;
  signal output_data_0 : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal output_data_0_s : STD_LOGIC;
  signal output_data_0_s_reg : STD_LOGIC;
  signal output_data_1 : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal output_data_1_s : STD_LOGIC;
  signal output_data_1_s_reg : STD_LOGIC;
  signal output_data_2 : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal output_data_2_s : STD_LOGIC;
  signal output_data_2_s_reg : STD_LOGIC;
  signal output_data_3 : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal output_data_3_s : STD_LOGIC;
  signal output_data_3_s_reg : STD_LOGIC;
  signal output_data_4 : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal output_data_4_s : STD_LOGIC;
  signal output_data_4_s_reg : STD_LOGIC;
  signal pc_s : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal pc_s_reg : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal state_next : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal state_reg : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal temp_signal : STD_LOGIC_VECTOR ( 5 to 5 );
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of \state_next_reg[0]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \state_next_reg[1]\ : label is "LD";
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \state_next_reg[1]_i_3\ : label is "soft_lutpair14";
  attribute XILINX_LEGACY_PRIM of \state_next_reg[2]\ : label is "LD";
  attribute SOFT_HLUTNM of \state_next_reg[2]_i_1\ : label is "soft_lutpair13";
  attribute XILINX_LEGACY_PRIM of \state_next_reg[3]\ : label is "LD";
  attribute SOFT_HLUTNM of \state_next_reg[3]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \state_reg[1]_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \state_reg[2]_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of stop_b_INST_0 : label is "soft_lutpair14";
begin
  atom_output_e <= \<const0>\;
GND: unisim.vcomponents.GND
    port map (
      G => \<const0>\
    );
\data_reg_s_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => data_in(0),
      Q => data_reg_s(0),
      R => reset
    );
\data_reg_s_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => data_in(1),
      Q => data_reg_s(1),
      R => reset
    );
\data_reg_s_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => data_in(2),
      Q => data_reg_s(2),
      R => reset
    );
\data_reg_s_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => data_in(3),
      Q => data_reg_s(3),
      R => reset
    );
\data_reg_s_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => data_in(4),
      Q => data_reg_s(4),
      R => reset
    );
\data_reg_s_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => data_in(5),
      Q => data_reg_s(5),
      R => reset
    );
\data_reg_s_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => data_in(6),
      Q => data_reg_s(6),
      R => reset
    );
\data_reg_s_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => data_in(7),
      Q => data_reg_s(7),
      R => reset
    );
enable_i_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => enable_i,
      Q => temp_signal(5),
      R => reset
    );
\instruction_address_reg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => instruction_address(0),
      Q => instruction_address_reg(0),
      R => reset
    );
\instruction_address_reg_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => instruction_address(10),
      Q => instruction_address_reg(10),
      R => reset
    );
\instruction_address_reg_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => instruction_address(11),
      Q => instruction_address_reg(11),
      R => reset
    );
\instruction_address_reg_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => instruction_address(12),
      Q => instruction_address_reg(12),
      R => reset
    );
\instruction_address_reg_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => instruction_address(13),
      Q => instruction_address_reg(13),
      R => reset
    );
\instruction_address_reg_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => instruction_address(14),
      Q => instruction_address_reg(14),
      R => reset
    );
\instruction_address_reg_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => instruction_address(15),
      Q => instruction_address_reg(15),
      R => reset
    );
\instruction_address_reg_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => instruction_address(16),
      Q => instruction_address_reg(16),
      R => reset
    );
\instruction_address_reg_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => instruction_address(17),
      Q => instruction_address_reg(17),
      R => reset
    );
\instruction_address_reg_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => instruction_address(18),
      Q => instruction_address_reg(18),
      R => reset
    );
\instruction_address_reg_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => instruction_address(19),
      Q => instruction_address_reg(19),
      R => reset
    );
\instruction_address_reg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => instruction_address(1),
      Q => instruction_address_reg(1),
      R => reset
    );
\instruction_address_reg_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => instruction_address(20),
      Q => instruction_address_reg(20),
      R => reset
    );
\instruction_address_reg_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => instruction_address(21),
      Q => instruction_address_reg(21),
      R => reset
    );
\instruction_address_reg_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => instruction_address(22),
      Q => instruction_address_reg(22),
      R => reset
    );
\instruction_address_reg_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => instruction_address(23),
      Q => instruction_address_reg(23),
      R => reset
    );
\instruction_address_reg_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => instruction_address(24),
      Q => instruction_address_reg(24),
      R => reset
    );
\instruction_address_reg_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => instruction_address(25),
      Q => instruction_address_reg(25),
      R => reset
    );
\instruction_address_reg_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => instruction_address(26),
      Q => instruction_address_reg(26),
      R => reset
    );
\instruction_address_reg_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => instruction_address(27),
      Q => instruction_address_reg(27),
      R => reset
    );
\instruction_address_reg_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => instruction_address(28),
      Q => instruction_address_reg(28),
      R => reset
    );
\instruction_address_reg_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => instruction_address(29),
      Q => instruction_address_reg(29),
      R => reset
    );
\instruction_address_reg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => instruction_address(2),
      Q => instruction_address_reg(2),
      R => reset
    );
\instruction_address_reg_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => instruction_address(30),
      Q => instruction_address_reg(30),
      R => reset
    );
\instruction_address_reg_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => instruction_address(31),
      Q => instruction_address_reg(31),
      R => reset
    );
\instruction_address_reg_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => instruction_address(3),
      Q => instruction_address_reg(3),
      R => reset
    );
\instruction_address_reg_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => instruction_address(4),
      Q => instruction_address_reg(4),
      R => reset
    );
\instruction_address_reg_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => instruction_address(5),
      Q => instruction_address_reg(5),
      R => reset
    );
\instruction_address_reg_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => instruction_address(6),
      Q => instruction_address_reg(6),
      R => reset
    );
\instruction_address_reg_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => instruction_address(7),
      Q => instruction_address_reg(7),
      R => reset
    );
\instruction_address_reg_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => instruction_address(8),
      Q => instruction_address_reg(8),
      R => reset
    );
\instruction_address_reg_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => instruction_address(9),
      Q => instruction_address_reg(9),
      R => reset
    );
\output_data_0[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AA00AB00"
    )
    port map (
      I0 => \n_0_state_reg_reg[0]\,
      I1 => state_reg(2),
      I2 => state_reg(3),
      I3 => start_b,
      I4 => state_reg(1),
      O => output_data_0_s
    );
\output_data_0_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => output_data_0_s,
      D => data_reg_s(1),
      Q => output_data_0(0),
      R => reset
    );
\output_data_0_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => output_data_0_s,
      D => data_reg_s(2),
      Q => output_data_0(1),
      R => reset
    );
\output_data_0_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => output_data_0_s,
      D => data_reg_s(3),
      Q => output_data_0(2),
      R => reset
    );
\output_data_0_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => output_data_0_s,
      D => data_reg_s(4),
      Q => output_data_0(3),
      R => reset
    );
\output_data_0_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => output_data_0_s,
      D => data_reg_s(5),
      Q => output_data_0(4),
      R => reset
    );
\output_data_0_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => output_data_0_s,
      D => data_reg_s(6),
      Q => output_data_0(5),
      R => reset
    );
output_data_0_s_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => output_data_0_s,
      Q => output_data_0_s_reg,
      R => reset
    );
\output_data_1[6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0100"
    )
    port map (
      I0 => state_reg(3),
      I1 => state_reg(2),
      I2 => \n_0_state_reg_reg[0]\,
      I3 => state_reg(1),
      O => output_data_1_s
    );
\output_data_1_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => output_data_1_s,
      D => data_reg_s(0),
      Q => output_data_1(0),
      R => reset
    );
\output_data_1_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => output_data_1_s,
      D => data_reg_s(1),
      Q => output_data_1(1),
      R => reset
    );
\output_data_1_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => output_data_1_s,
      D => data_reg_s(2),
      Q => output_data_1(2),
      R => reset
    );
\output_data_1_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => output_data_1_s,
      D => data_reg_s(3),
      Q => output_data_1(3),
      R => reset
    );
\output_data_1_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => output_data_1_s,
      D => data_reg_s(4),
      Q => output_data_1(4),
      R => reset
    );
\output_data_1_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => output_data_1_s,
      D => data_reg_s(5),
      Q => output_data_1(5),
      R => reset
    );
\output_data_1_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => output_data_1_s,
      D => data_reg_s(6),
      Q => output_data_1(6),
      R => reset
    );
output_data_1_s_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => output_data_1_s,
      Q => output_data_1_s_reg,
      R => reset
    );
\output_data_2[6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0100"
    )
    port map (
      I0 => state_reg(3),
      I1 => state_reg(1),
      I2 => \n_0_state_reg_reg[0]\,
      I3 => state_reg(2),
      O => output_data_2_s
    );
\output_data_2_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => output_data_2_s,
      D => data_reg_s(0),
      Q => output_data_2(0),
      R => reset
    );
\output_data_2_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => output_data_2_s,
      D => data_reg_s(1),
      Q => output_data_2(1),
      R => reset
    );
\output_data_2_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => output_data_2_s,
      D => data_reg_s(2),
      Q => output_data_2(2),
      R => reset
    );
\output_data_2_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => output_data_2_s,
      D => data_reg_s(3),
      Q => output_data_2(3),
      R => reset
    );
\output_data_2_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => output_data_2_s,
      D => data_reg_s(4),
      Q => output_data_2(4),
      R => reset
    );
\output_data_2_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => output_data_2_s,
      D => data_reg_s(5),
      Q => output_data_2(5),
      R => reset
    );
\output_data_2_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => output_data_2_s,
      D => data_reg_s(6),
      Q => output_data_2(6),
      R => reset
    );
output_data_2_s_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => output_data_2_s,
      Q => output_data_2_s_reg,
      R => reset
    );
\output_data_3[6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0400"
    )
    port map (
      I0 => state_reg(3),
      I1 => state_reg(2),
      I2 => \n_0_state_reg_reg[0]\,
      I3 => state_reg(1),
      O => output_data_3_s
    );
\output_data_3_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => output_data_3_s,
      D => data_reg_s(0),
      Q => output_data_3(0),
      R => reset
    );
\output_data_3_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => output_data_3_s,
      D => data_reg_s(1),
      Q => output_data_3(1),
      R => reset
    );
\output_data_3_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => output_data_3_s,
      D => data_reg_s(2),
      Q => output_data_3(2),
      R => reset
    );
\output_data_3_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => output_data_3_s,
      D => data_reg_s(3),
      Q => output_data_3(3),
      R => reset
    );
\output_data_3_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => output_data_3_s,
      D => data_reg_s(4),
      Q => output_data_3(4),
      R => reset
    );
\output_data_3_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => output_data_3_s,
      D => data_reg_s(5),
      Q => output_data_3(5),
      R => reset
    );
\output_data_3_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => output_data_3_s,
      D => data_reg_s(6),
      Q => output_data_3(6),
      R => reset
    );
output_data_3_s_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => output_data_3_s,
      Q => output_data_3_s_reg,
      R => reset
    );
\output_data_4[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFEFF00000200"
    )
    port map (
      I0 => data_reg_s(0),
      I1 => state_reg(2),
      I2 => state_reg(1),
      I3 => state_reg(3),
      I4 => \n_0_state_reg_reg[0]\,
      I5 => output_data_4(0),
      O => \n_0_output_data_4[0]_i_1\
    );
\output_data_4[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFEFF00000200"
    )
    port map (
      I0 => data_reg_s(1),
      I1 => state_reg(2),
      I2 => state_reg(1),
      I3 => state_reg(3),
      I4 => \n_0_state_reg_reg[0]\,
      I5 => output_data_4(1),
      O => \n_0_output_data_4[1]_i_1\
    );
\output_data_4[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFEFF00000200"
    )
    port map (
      I0 => data_reg_s(2),
      I1 => state_reg(2),
      I2 => state_reg(1),
      I3 => state_reg(3),
      I4 => \n_0_state_reg_reg[0]\,
      I5 => output_data_4(2),
      O => \n_0_output_data_4[2]_i_1\
    );
\output_data_4_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => \n_0_output_data_4[0]_i_1\,
      Q => output_data_4(0),
      R => reset
    );
\output_data_4_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => \n_0_output_data_4[1]_i_1\,
      Q => output_data_4(1),
      R => reset
    );
\output_data_4_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => \n_0_output_data_4[2]_i_1\,
      Q => output_data_4(2),
      R => reset
    );
output_data_4_s_reg_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0010"
    )
    port map (
      I0 => state_reg(2),
      I1 => state_reg(1),
      I2 => state_reg(3),
      I3 => \n_0_state_reg_reg[0]\,
      O => output_data_4_s
    );
output_data_4_s_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => output_data_4_s,
      Q => output_data_4_s_reg,
      R => reset
    );
\pc[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"C0AA"
    )
    port map (
      I0 => pc_s_reg(0),
      I1 => temp_signal(5),
      I2 => instruction_address_reg(0),
      I3 => \n_0_pc[31]_i_3\,
      O => pc_s(0)
    );
\pc[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFD58080"
    )
    port map (
      I0 => \n_0_pc[31]_i_3\,
      I1 => instruction_address_reg(10),
      I2 => temp_signal(5),
      I3 => output_data_0_s_reg,
      I4 => pc_s_reg(10),
      I5 => \n_0_pc[10]_i_2\,
      O => pc_s(10)
    );
\pc[10]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAA800000000"
    )
    port map (
      I0 => \n_0_pc[31]_i_3\,
      I1 => output_data_1_s_reg,
      I2 => output_data_2_s_reg,
      I3 => output_data_3_s_reg,
      I4 => output_data_4_s_reg,
      I5 => output_data_1(2),
      O => \n_0_pc[10]_i_2\
    );
\pc[11]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFD58080"
    )
    port map (
      I0 => \n_0_pc[31]_i_3\,
      I1 => instruction_address_reg(11),
      I2 => temp_signal(5),
      I3 => output_data_0_s_reg,
      I4 => pc_s_reg(11),
      I5 => \n_0_pc[11]_i_2\,
      O => pc_s(11)
    );
\pc[11]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAA800000000"
    )
    port map (
      I0 => \n_0_pc[31]_i_3\,
      I1 => output_data_1_s_reg,
      I2 => output_data_2_s_reg,
      I3 => output_data_3_s_reg,
      I4 => output_data_4_s_reg,
      I5 => output_data_1(3),
      O => \n_0_pc[11]_i_2\
    );
\pc[12]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFD58080"
    )
    port map (
      I0 => \n_0_pc[31]_i_3\,
      I1 => instruction_address_reg(12),
      I2 => temp_signal(5),
      I3 => output_data_0_s_reg,
      I4 => pc_s_reg(12),
      I5 => \n_0_pc[12]_i_2\,
      O => pc_s(12)
    );
\pc[12]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAA800000000"
    )
    port map (
      I0 => \n_0_pc[31]_i_3\,
      I1 => output_data_1_s_reg,
      I2 => output_data_2_s_reg,
      I3 => output_data_3_s_reg,
      I4 => output_data_4_s_reg,
      I5 => output_data_1(4),
      O => \n_0_pc[12]_i_2\
    );
\pc[13]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFD58080"
    )
    port map (
      I0 => \n_0_pc[31]_i_3\,
      I1 => instruction_address_reg(13),
      I2 => temp_signal(5),
      I3 => output_data_0_s_reg,
      I4 => pc_s_reg(13),
      I5 => \n_0_pc[13]_i_2\,
      O => pc_s(13)
    );
\pc[13]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAA800000000"
    )
    port map (
      I0 => \n_0_pc[31]_i_3\,
      I1 => output_data_1_s_reg,
      I2 => output_data_2_s_reg,
      I3 => output_data_3_s_reg,
      I4 => output_data_4_s_reg,
      I5 => output_data_1(5),
      O => \n_0_pc[13]_i_2\
    );
\pc[14]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFD58080"
    )
    port map (
      I0 => \n_0_pc[31]_i_3\,
      I1 => instruction_address_reg(14),
      I2 => temp_signal(5),
      I3 => output_data_0_s_reg,
      I4 => pc_s_reg(14),
      I5 => \n_0_pc[14]_i_2\,
      O => pc_s(14)
    );
\pc[14]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAA800000000"
    )
    port map (
      I0 => \n_0_pc[31]_i_3\,
      I1 => output_data_1_s_reg,
      I2 => output_data_2_s_reg,
      I3 => output_data_3_s_reg,
      I4 => output_data_4_s_reg,
      I5 => output_data_1(6),
      O => \n_0_pc[14]_i_2\
    );
\pc[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEEEEEEAAAAAAAAA"
    )
    port map (
      I0 => \n_0_pc[15]_i_2\,
      I1 => output_data_2(0),
      I2 => output_data_2_s_reg,
      I3 => output_data_3_s_reg,
      I4 => output_data_4_s_reg,
      I5 => \n_0_pc[31]_i_3\,
      O => pc_s(15)
    );
\pc[15]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFA8A8A8AAAAAAAA"
    )
    port map (
      I0 => pc_s_reg(15),
      I1 => output_data_1_s_reg,
      I2 => output_data_0_s_reg,
      I3 => temp_signal(5),
      I4 => instruction_address_reg(15),
      I5 => \n_0_pc[31]_i_3\,
      O => \n_0_pc[15]_i_2\
    );
\pc[16]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEEEEEEAAAAAAAAA"
    )
    port map (
      I0 => \n_0_pc[16]_i_2\,
      I1 => output_data_2(1),
      I2 => output_data_2_s_reg,
      I3 => output_data_3_s_reg,
      I4 => output_data_4_s_reg,
      I5 => \n_0_pc[31]_i_3\,
      O => pc_s(16)
    );
\pc[16]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFA8A8A8AAAAAAAA"
    )
    port map (
      I0 => pc_s_reg(16),
      I1 => output_data_1_s_reg,
      I2 => output_data_0_s_reg,
      I3 => temp_signal(5),
      I4 => instruction_address_reg(16),
      I5 => \n_0_pc[31]_i_3\,
      O => \n_0_pc[16]_i_2\
    );
\pc[17]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEEEEEEAAAAAAAAA"
    )
    port map (
      I0 => \n_0_pc[17]_i_2\,
      I1 => output_data_2(2),
      I2 => output_data_2_s_reg,
      I3 => output_data_3_s_reg,
      I4 => output_data_4_s_reg,
      I5 => \n_0_pc[31]_i_3\,
      O => pc_s(17)
    );
\pc[17]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFA8A8A8AAAAAAAA"
    )
    port map (
      I0 => pc_s_reg(17),
      I1 => output_data_1_s_reg,
      I2 => output_data_0_s_reg,
      I3 => temp_signal(5),
      I4 => instruction_address_reg(17),
      I5 => \n_0_pc[31]_i_3\,
      O => \n_0_pc[17]_i_2\
    );
\pc[18]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEEEEEEAAAAAAAAA"
    )
    port map (
      I0 => \n_0_pc[18]_i_2\,
      I1 => output_data_2(3),
      I2 => output_data_2_s_reg,
      I3 => output_data_3_s_reg,
      I4 => output_data_4_s_reg,
      I5 => \n_0_pc[31]_i_3\,
      O => pc_s(18)
    );
\pc[18]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFA8A8A8AAAAAAAA"
    )
    port map (
      I0 => pc_s_reg(18),
      I1 => output_data_1_s_reg,
      I2 => output_data_0_s_reg,
      I3 => temp_signal(5),
      I4 => instruction_address_reg(18),
      I5 => \n_0_pc[31]_i_3\,
      O => \n_0_pc[18]_i_2\
    );
\pc[19]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEEEEEEAAAAAAAAA"
    )
    port map (
      I0 => \n_0_pc[19]_i_2\,
      I1 => output_data_2(4),
      I2 => output_data_2_s_reg,
      I3 => output_data_3_s_reg,
      I4 => output_data_4_s_reg,
      I5 => \n_0_pc[31]_i_3\,
      O => pc_s(19)
    );
\pc[19]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFA8A8A8AAAAAAAA"
    )
    port map (
      I0 => pc_s_reg(19),
      I1 => output_data_1_s_reg,
      I2 => output_data_0_s_reg,
      I3 => temp_signal(5),
      I4 => instruction_address_reg(19),
      I5 => \n_0_pc[31]_i_3\,
      O => \n_0_pc[19]_i_2\
    );
\pc[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"C0AA"
    )
    port map (
      I0 => pc_s_reg(1),
      I1 => temp_signal(5),
      I2 => instruction_address_reg(1),
      I3 => \n_0_pc[31]_i_3\,
      O => pc_s(1)
    );
\pc[20]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEEEEEEAAAAAAAAA"
    )
    port map (
      I0 => \n_0_pc[20]_i_2\,
      I1 => output_data_2(5),
      I2 => output_data_2_s_reg,
      I3 => output_data_3_s_reg,
      I4 => output_data_4_s_reg,
      I5 => \n_0_pc[31]_i_3\,
      O => pc_s(20)
    );
\pc[20]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFA8A8A8AAAAAAAA"
    )
    port map (
      I0 => pc_s_reg(20),
      I1 => output_data_1_s_reg,
      I2 => output_data_0_s_reg,
      I3 => temp_signal(5),
      I4 => instruction_address_reg(20),
      I5 => \n_0_pc[31]_i_3\,
      O => \n_0_pc[20]_i_2\
    );
\pc[21]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEEEEEEAAAAAAAAA"
    )
    port map (
      I0 => \n_0_pc[21]_i_2\,
      I1 => output_data_2(6),
      I2 => output_data_2_s_reg,
      I3 => output_data_3_s_reg,
      I4 => output_data_4_s_reg,
      I5 => \n_0_pc[31]_i_3\,
      O => pc_s(21)
    );
\pc[21]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFA8A8A8AAAAAAAA"
    )
    port map (
      I0 => pc_s_reg(21),
      I1 => output_data_1_s_reg,
      I2 => output_data_0_s_reg,
      I3 => temp_signal(5),
      I4 => instruction_address_reg(21),
      I5 => \n_0_pc[31]_i_3\,
      O => \n_0_pc[21]_i_2\
    );
\pc[22]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFBAAAAAAAA"
    )
    port map (
      I0 => \n_0_pc[22]_i_2\,
      I1 => \n_0_pc[31]_i_3\,
      I2 => output_data_2_s_reg,
      I3 => output_data_0_s_reg,
      I4 => output_data_1_s_reg,
      I5 => pc_s_reg(22),
      O => pc_s(22)
    );
\pc[22]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFF8888800000000"
    )
    port map (
      I0 => temp_signal(5),
      I1 => instruction_address_reg(22),
      I2 => output_data_4_s_reg,
      I3 => output_data_3_s_reg,
      I4 => output_data_3(0),
      I5 => \n_0_pc[31]_i_3\,
      O => \n_0_pc[22]_i_2\
    );
\pc[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFBAAAAAAAA"
    )
    port map (
      I0 => \n_0_pc[23]_i_2\,
      I1 => \n_0_pc[31]_i_3\,
      I2 => output_data_2_s_reg,
      I3 => output_data_0_s_reg,
      I4 => output_data_1_s_reg,
      I5 => pc_s_reg(23),
      O => pc_s(23)
    );
\pc[23]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFF8888800000000"
    )
    port map (
      I0 => temp_signal(5),
      I1 => instruction_address_reg(23),
      I2 => output_data_4_s_reg,
      I3 => output_data_3_s_reg,
      I4 => output_data_3(1),
      I5 => \n_0_pc[31]_i_3\,
      O => \n_0_pc[23]_i_2\
    );
\pc[24]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFBAAAAAAAA"
    )
    port map (
      I0 => \n_0_pc[24]_i_2\,
      I1 => \n_0_pc[31]_i_3\,
      I2 => output_data_2_s_reg,
      I3 => output_data_0_s_reg,
      I4 => output_data_1_s_reg,
      I5 => pc_s_reg(24),
      O => pc_s(24)
    );
\pc[24]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFF8888800000000"
    )
    port map (
      I0 => temp_signal(5),
      I1 => instruction_address_reg(24),
      I2 => output_data_4_s_reg,
      I3 => output_data_3_s_reg,
      I4 => output_data_3(2),
      I5 => \n_0_pc[31]_i_3\,
      O => \n_0_pc[24]_i_2\
    );
\pc[25]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFBAAAAAAAA"
    )
    port map (
      I0 => \n_0_pc[25]_i_2\,
      I1 => \n_0_pc[31]_i_3\,
      I2 => output_data_2_s_reg,
      I3 => output_data_0_s_reg,
      I4 => output_data_1_s_reg,
      I5 => pc_s_reg(25),
      O => pc_s(25)
    );
\pc[25]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFF8888800000000"
    )
    port map (
      I0 => temp_signal(5),
      I1 => instruction_address_reg(25),
      I2 => output_data_4_s_reg,
      I3 => output_data_3_s_reg,
      I4 => output_data_3(3),
      I5 => \n_0_pc[31]_i_3\,
      O => \n_0_pc[25]_i_2\
    );
\pc[26]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFBAAAAAAAA"
    )
    port map (
      I0 => \n_0_pc[26]_i_2\,
      I1 => \n_0_pc[31]_i_3\,
      I2 => output_data_2_s_reg,
      I3 => output_data_0_s_reg,
      I4 => output_data_1_s_reg,
      I5 => pc_s_reg(26),
      O => pc_s(26)
    );
\pc[26]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFF8888800000000"
    )
    port map (
      I0 => temp_signal(5),
      I1 => instruction_address_reg(26),
      I2 => output_data_4_s_reg,
      I3 => output_data_3_s_reg,
      I4 => output_data_3(4),
      I5 => \n_0_pc[31]_i_3\,
      O => \n_0_pc[26]_i_2\
    );
\pc[27]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFBAAAAAAAA"
    )
    port map (
      I0 => \n_0_pc[27]_i_2\,
      I1 => \n_0_pc[31]_i_3\,
      I2 => output_data_2_s_reg,
      I3 => output_data_0_s_reg,
      I4 => output_data_1_s_reg,
      I5 => pc_s_reg(27),
      O => pc_s(27)
    );
\pc[27]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFF8888800000000"
    )
    port map (
      I0 => temp_signal(5),
      I1 => instruction_address_reg(27),
      I2 => output_data_4_s_reg,
      I3 => output_data_3_s_reg,
      I4 => output_data_3(5),
      I5 => \n_0_pc[31]_i_3\,
      O => \n_0_pc[27]_i_2\
    );
\pc[28]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFBAAAAAAAA"
    )
    port map (
      I0 => \n_0_pc[28]_i_2\,
      I1 => \n_0_pc[31]_i_3\,
      I2 => output_data_2_s_reg,
      I3 => output_data_0_s_reg,
      I4 => output_data_1_s_reg,
      I5 => pc_s_reg(28),
      O => pc_s(28)
    );
\pc[28]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFF8888800000000"
    )
    port map (
      I0 => temp_signal(5),
      I1 => instruction_address_reg(28),
      I2 => output_data_4_s_reg,
      I3 => output_data_3_s_reg,
      I4 => output_data_3(6),
      I5 => \n_0_pc[31]_i_3\,
      O => \n_0_pc[28]_i_2\
    );
\pc[29]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFAAEAAAEAAAEAAA"
    )
    port map (
      I0 => \n_0_pc[29]_i_2\,
      I1 => output_data_4_s_reg,
      I2 => output_data_4(0),
      I3 => \n_0_pc[31]_i_3\,
      I4 => instruction_address_reg(29),
      I5 => temp_signal(5),
      O => pc_s(29)
    );
\pc[29]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFFFF00000000"
    )
    port map (
      I0 => output_data_3_s_reg,
      I1 => output_data_2_s_reg,
      I2 => output_data_1_s_reg,
      I3 => output_data_0_s_reg,
      I4 => \n_0_pc[31]_i_3\,
      I5 => pc_s_reg(29),
      O => \n_0_pc[29]_i_2\
    );
\pc[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFF088F088F088F0"
    )
    port map (
      I0 => instruction_address_reg(2),
      I1 => temp_signal(5),
      I2 => pc_s_reg(2),
      I3 => \n_0_pc[31]_i_3\,
      I4 => output_data_0(0),
      I5 => \n_0_pc[7]_i_2\,
      O => pc_s(2)
    );
\pc[30]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFAAEAAAEAAAEAAA"
    )
    port map (
      I0 => \n_0_pc[30]_i_2\,
      I1 => output_data_4_s_reg,
      I2 => output_data_4(1),
      I3 => \n_0_pc[31]_i_3\,
      I4 => instruction_address_reg(30),
      I5 => temp_signal(5),
      O => pc_s(30)
    );
\pc[30]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFFFF00000000"
    )
    port map (
      I0 => output_data_3_s_reg,
      I1 => output_data_2_s_reg,
      I2 => output_data_1_s_reg,
      I3 => output_data_0_s_reg,
      I4 => \n_0_pc[31]_i_3\,
      I5 => pc_s_reg(30),
      O => \n_0_pc[30]_i_2\
    );
\pc[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFAAEAAAEAAAEAAA"
    )
    port map (
      I0 => \n_0_pc[31]_i_2\,
      I1 => output_data_4_s_reg,
      I2 => output_data_4(2),
      I3 => \n_0_pc[31]_i_3\,
      I4 => instruction_address_reg(31),
      I5 => temp_signal(5),
      O => pc_s(31)
    );
\pc[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFFFF00000000"
    )
    port map (
      I0 => output_data_3_s_reg,
      I1 => output_data_2_s_reg,
      I2 => output_data_1_s_reg,
      I3 => output_data_0_s_reg,
      I4 => \n_0_pc[31]_i_3\,
      I5 => pc_s_reg(31),
      O => \n_0_pc[31]_i_2\
    );
\pc[31]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000100010116"
    )
    port map (
      I0 => output_data_0_s_reg,
      I1 => output_data_1_s_reg,
      I2 => output_data_2_s_reg,
      I3 => output_data_3_s_reg,
      I4 => output_data_4_s_reg,
      I5 => temp_signal(5),
      O => \n_0_pc[31]_i_3\
    );
\pc[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFF088F088F088F0"
    )
    port map (
      I0 => instruction_address_reg(3),
      I1 => temp_signal(5),
      I2 => pc_s_reg(3),
      I3 => \n_0_pc[31]_i_3\,
      I4 => output_data_0(1),
      I5 => \n_0_pc[7]_i_2\,
      O => pc_s(3)
    );
\pc[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFF088F088F088F0"
    )
    port map (
      I0 => instruction_address_reg(4),
      I1 => temp_signal(5),
      I2 => pc_s_reg(4),
      I3 => \n_0_pc[31]_i_3\,
      I4 => output_data_0(2),
      I5 => \n_0_pc[7]_i_2\,
      O => pc_s(4)
    );
\pc[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFF088F088F088F0"
    )
    port map (
      I0 => instruction_address_reg(5),
      I1 => temp_signal(5),
      I2 => pc_s_reg(5),
      I3 => \n_0_pc[31]_i_3\,
      I4 => output_data_0(3),
      I5 => \n_0_pc[7]_i_2\,
      O => pc_s(5)
    );
\pc[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFF088F088F088F0"
    )
    port map (
      I0 => instruction_address_reg(6),
      I1 => temp_signal(5),
      I2 => pc_s_reg(6),
      I3 => \n_0_pc[31]_i_3\,
      I4 => output_data_0(4),
      I5 => \n_0_pc[7]_i_2\,
      O => pc_s(6)
    );
\pc[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFF088F088F088F0"
    )
    port map (
      I0 => instruction_address_reg(7),
      I1 => temp_signal(5),
      I2 => pc_s_reg(7),
      I3 => \n_0_pc[31]_i_3\,
      I4 => output_data_0(5),
      I5 => \n_0_pc[7]_i_2\,
      O => pc_s(7)
    );
\pc[7]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
    port map (
      I0 => output_data_2_s_reg,
      I1 => output_data_3_s_reg,
      I2 => output_data_4_s_reg,
      I3 => output_data_1_s_reg,
      I4 => output_data_0_s_reg,
      O => \n_0_pc[7]_i_2\
    );
\pc[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFD58080"
    )
    port map (
      I0 => \n_0_pc[31]_i_3\,
      I1 => instruction_address_reg(8),
      I2 => temp_signal(5),
      I3 => output_data_0_s_reg,
      I4 => pc_s_reg(8),
      I5 => \n_0_pc[8]_i_2\,
      O => pc_s(8)
    );
\pc[8]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAA800000000"
    )
    port map (
      I0 => \n_0_pc[31]_i_3\,
      I1 => output_data_1_s_reg,
      I2 => output_data_2_s_reg,
      I3 => output_data_3_s_reg,
      I4 => output_data_4_s_reg,
      I5 => output_data_1(0),
      O => \n_0_pc[8]_i_2\
    );
\pc[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFD58080"
    )
    port map (
      I0 => \n_0_pc[31]_i_3\,
      I1 => instruction_address_reg(9),
      I2 => temp_signal(5),
      I3 => output_data_0_s_reg,
      I4 => pc_s_reg(9),
      I5 => \n_0_pc[9]_i_2\,
      O => pc_s(9)
    );
\pc[9]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAA800000000"
    )
    port map (
      I0 => \n_0_pc[31]_i_3\,
      I1 => output_data_1_s_reg,
      I2 => output_data_2_s_reg,
      I3 => output_data_3_s_reg,
      I4 => output_data_4_s_reg,
      I5 => output_data_1(1),
      O => \n_0_pc[9]_i_2\
    );
\pc_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => n_0_w_en_i_1,
      D => pc_s(0),
      Q => pc(0),
      R => reset
    );
\pc_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => n_0_w_en_i_1,
      D => pc_s(10),
      Q => pc(10),
      R => reset
    );
\pc_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => n_0_w_en_i_1,
      D => pc_s(11),
      Q => pc(11),
      R => reset
    );
\pc_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => n_0_w_en_i_1,
      D => pc_s(12),
      Q => pc(12),
      R => reset
    );
\pc_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => n_0_w_en_i_1,
      D => pc_s(13),
      Q => pc(13),
      R => reset
    );
\pc_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => n_0_w_en_i_1,
      D => pc_s(14),
      Q => pc(14),
      R => reset
    );
\pc_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => n_0_w_en_i_1,
      D => pc_s(15),
      Q => pc(15),
      R => reset
    );
\pc_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => n_0_w_en_i_1,
      D => pc_s(16),
      Q => pc(16),
      R => reset
    );
\pc_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => n_0_w_en_i_1,
      D => pc_s(17),
      Q => pc(17),
      R => reset
    );
\pc_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => n_0_w_en_i_1,
      D => pc_s(18),
      Q => pc(18),
      R => reset
    );
\pc_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => n_0_w_en_i_1,
      D => pc_s(19),
      Q => pc(19),
      R => reset
    );
\pc_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => n_0_w_en_i_1,
      D => pc_s(1),
      Q => pc(1),
      R => reset
    );
\pc_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => n_0_w_en_i_1,
      D => pc_s(20),
      Q => pc(20),
      R => reset
    );
\pc_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => n_0_w_en_i_1,
      D => pc_s(21),
      Q => pc(21),
      R => reset
    );
\pc_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => n_0_w_en_i_1,
      D => pc_s(22),
      Q => pc(22),
      R => reset
    );
\pc_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => n_0_w_en_i_1,
      D => pc_s(23),
      Q => pc(23),
      R => reset
    );
\pc_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => n_0_w_en_i_1,
      D => pc_s(24),
      Q => pc(24),
      R => reset
    );
\pc_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => n_0_w_en_i_1,
      D => pc_s(25),
      Q => pc(25),
      R => reset
    );
\pc_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => n_0_w_en_i_1,
      D => pc_s(26),
      Q => pc(26),
      R => reset
    );
\pc_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => n_0_w_en_i_1,
      D => pc_s(27),
      Q => pc(27),
      R => reset
    );
\pc_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => n_0_w_en_i_1,
      D => pc_s(28),
      Q => pc(28),
      R => reset
    );
\pc_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => n_0_w_en_i_1,
      D => pc_s(29),
      Q => pc(29),
      R => reset
    );
\pc_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => n_0_w_en_i_1,
      D => pc_s(2),
      Q => pc(2),
      R => reset
    );
\pc_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => n_0_w_en_i_1,
      D => pc_s(30),
      Q => pc(30),
      R => reset
    );
\pc_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => n_0_w_en_i_1,
      D => pc_s(31),
      Q => pc(31),
      R => reset
    );
\pc_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => n_0_w_en_i_1,
      D => pc_s(3),
      Q => pc(3),
      R => reset
    );
\pc_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => n_0_w_en_i_1,
      D => pc_s(4),
      Q => pc(4),
      R => reset
    );
\pc_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => n_0_w_en_i_1,
      D => pc_s(5),
      Q => pc(5),
      R => reset
    );
\pc_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => n_0_w_en_i_1,
      D => pc_s(6),
      Q => pc(6),
      R => reset
    );
\pc_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => n_0_w_en_i_1,
      D => pc_s(7),
      Q => pc(7),
      R => reset
    );
\pc_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => n_0_w_en_i_1,
      D => pc_s(8),
      Q => pc(8),
      R => reset
    );
\pc_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => n_0_w_en_i_1,
      D => pc_s(9),
      Q => pc(9),
      R => reset
    );
\pc_s_reg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => pc_s(0),
      Q => pc_s_reg(0),
      R => reset
    );
\pc_s_reg_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => pc_s(10),
      Q => pc_s_reg(10),
      R => reset
    );
\pc_s_reg_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => pc_s(11),
      Q => pc_s_reg(11),
      R => reset
    );
\pc_s_reg_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => pc_s(12),
      Q => pc_s_reg(12),
      R => reset
    );
\pc_s_reg_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => pc_s(13),
      Q => pc_s_reg(13),
      R => reset
    );
\pc_s_reg_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => pc_s(14),
      Q => pc_s_reg(14),
      R => reset
    );
\pc_s_reg_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => pc_s(15),
      Q => pc_s_reg(15),
      R => reset
    );
\pc_s_reg_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => pc_s(16),
      Q => pc_s_reg(16),
      R => reset
    );
\pc_s_reg_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => pc_s(17),
      Q => pc_s_reg(17),
      R => reset
    );
\pc_s_reg_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => pc_s(18),
      Q => pc_s_reg(18),
      R => reset
    );
\pc_s_reg_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => pc_s(19),
      Q => pc_s_reg(19),
      R => reset
    );
\pc_s_reg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => pc_s(1),
      Q => pc_s_reg(1),
      R => reset
    );
\pc_s_reg_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => pc_s(20),
      Q => pc_s_reg(20),
      R => reset
    );
\pc_s_reg_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => pc_s(21),
      Q => pc_s_reg(21),
      R => reset
    );
\pc_s_reg_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => pc_s(22),
      Q => pc_s_reg(22),
      R => reset
    );
\pc_s_reg_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => pc_s(23),
      Q => pc_s_reg(23),
      R => reset
    );
\pc_s_reg_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => pc_s(24),
      Q => pc_s_reg(24),
      R => reset
    );
\pc_s_reg_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => pc_s(25),
      Q => pc_s_reg(25),
      R => reset
    );
\pc_s_reg_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => pc_s(26),
      Q => pc_s_reg(26),
      R => reset
    );
\pc_s_reg_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => pc_s(27),
      Q => pc_s_reg(27),
      R => reset
    );
\pc_s_reg_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => pc_s(28),
      Q => pc_s_reg(28),
      R => reset
    );
\pc_s_reg_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => pc_s(29),
      Q => pc_s_reg(29),
      R => reset
    );
\pc_s_reg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => pc_s(2),
      Q => pc_s_reg(2),
      R => reset
    );
\pc_s_reg_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => pc_s(30),
      Q => pc_s_reg(30),
      R => reset
    );
\pc_s_reg_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => pc_s(31),
      Q => pc_s_reg(31),
      R => reset
    );
\pc_s_reg_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => pc_s(3),
      Q => pc_s_reg(3),
      R => reset
    );
\pc_s_reg_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => pc_s(4),
      Q => pc_s_reg(4),
      R => reset
    );
\pc_s_reg_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => pc_s(5),
      Q => pc_s_reg(5),
      R => reset
    );
\pc_s_reg_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => pc_s(6),
      Q => pc_s_reg(6),
      R => reset
    );
\pc_s_reg_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => pc_s(7),
      Q => pc_s_reg(7),
      R => reset
    );
\pc_s_reg_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => pc_s(8),
      Q => pc_s_reg(8),
      R => reset
    );
\pc_s_reg_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => pc_s(9),
      Q => pc_s_reg(9),
      R => reset
    );
\state_next_reg[0]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
    port map (
      CLR => '0',
      D => \n_0_state_next_reg[0]_i_1\,
      G => \n_0_state_next_reg[3]_i_2\,
      GE => '1',
      Q => state_next(0)
    );
\state_next_reg[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAEAFFEAFA"
    )
    port map (
      I0 => \n_0_state_next_reg[0]_i_2\,
      I1 => state_reg(3),
      I2 => state_reg(2),
      I3 => data_reg_s(7),
      I4 => state_reg(1),
      I5 => \n_0_state_reg_reg[0]\,
      O => \n_0_state_next_reg[0]_i_1\
    );
\state_next_reg[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"01000100F1FF0100"
    )
    port map (
      I0 => data_reg_s(6),
      I1 => state_reg(1),
      I2 => \n_0_state_reg_reg[0]\,
      I3 => state_reg(3),
      I4 => start_b,
      I5 => data_reg_s(7),
      O => \n_0_state_next_reg[0]_i_2\
    );
\state_next_reg[1]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
    port map (
      CLR => '0',
      D => \n_0_state_next_reg[1]_i_1\,
      G => \n_0_state_next_reg[3]_i_2\,
      GE => '1',
      Q => state_next(1)
    );
\state_next_reg[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFF80"
    )
    port map (
      I0 => start_b,
      I1 => data_reg_s(7),
      I2 => \n_0_state_reg_reg[0]\,
      I3 => \n_0_state_next_reg[1]_i_2\,
      I4 => \n_0_state_next_reg[1]_i_3\,
      O => \n_0_state_next_reg[1]_i_1\
    );
\state_next_reg[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000F411550000"
    )
    port map (
      I0 => \n_0_state_reg_reg[0]\,
      I1 => state_reg(2),
      I2 => start_b,
      I3 => state_reg(3),
      I4 => state_reg(1),
      I5 => data_reg_s(7),
      O => \n_0_state_next_reg[1]_i_2\
    );
\state_next_reg[1]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000200"
    )
    port map (
      I0 => data_reg_s(6),
      I1 => state_reg(1),
      I2 => state_reg(2),
      I3 => state_reg(3),
      I4 => \n_0_state_reg_reg[0]\,
      O => \n_0_state_next_reg[1]_i_3\
    );
\state_next_reg[2]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
    port map (
      CLR => '0',
      D => \n_0_state_next_reg[2]_i_1\,
      G => \n_0_state_next_reg[3]_i_2\,
      GE => '1',
      Q => state_next(2)
    );
\state_next_reg[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0B0C0C0C"
    )
    port map (
      I0 => state_reg(3),
      I1 => state_reg(2),
      I2 => \n_0_state_reg_reg[0]\,
      I3 => data_reg_s(7),
      I4 => state_reg(1),
      O => \n_0_state_next_reg[2]_i_1\
    );
\state_next_reg[3]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
    port map (
      CLR => '0',
      D => \n_0_state_next_reg[3]_i_1\,
      G => \n_0_state_next_reg[3]_i_2\,
      GE => '1',
      Q => state_next(3)
    );
\state_next_reg[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0000FF80"
    )
    port map (
      I0 => state_reg(1),
      I1 => data_reg_s(7),
      I2 => state_reg(2),
      I3 => state_reg(3),
      I4 => \n_0_state_reg_reg[0]\,
      O => \n_0_state_next_reg[3]_i_1\
    );
\state_next_reg[3]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"7F"
    )
    port map (
      I0 => state_reg(3),
      I1 => state_reg(2),
      I2 => state_reg(1),
      O => \n_0_state_next_reg[3]_i_2\
    );
\state_reg[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
    port map (
      I0 => state_next(0),
      I1 => enable,
      I2 => reset,
      O => \n_0_state_reg[0]_i_1\
    );
\state_reg[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
    port map (
      I0 => state_next(1),
      I1 => enable,
      O => \n_0_state_reg[1]_i_1\
    );
\state_reg[2]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
    port map (
      I0 => state_next(2),
      I1 => enable,
      O => \n_0_state_reg[2]_i_1\
    );
\state_reg[3]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
    port map (
      I0 => state_next(3),
      I1 => enable,
      O => \n_0_state_reg[3]_i_1\
    );
\state_reg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => \n_0_state_reg[0]_i_1\,
      Q => \n_0_state_reg_reg[0]\,
      R => \<const0>\
    );
\state_reg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => \n_0_state_reg[1]_i_1\,
      Q => state_reg(1),
      R => reset
    );
\state_reg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => \n_0_state_reg[2]_i_1\,
      Q => state_reg(2),
      R => reset
    );
\state_reg_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => \n_0_state_reg[3]_i_1\,
      Q => state_reg(3),
      R => reset
    );
stop_b_INST_0: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF40"
    )
    port map (
      I0 => state_reg(1),
      I1 => state_reg(2),
      I2 => state_reg(3),
      I3 => \n_0_state_reg_reg[0]\,
      O => stop_b
    );
w_en_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
    port map (
      I0 => temp_signal(5),
      I1 => \n_0_state_reg_reg[0]\,
      O => n_0_w_en_i_1
    );
w_en_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => n_0_w_en_i_1,
      Q => w_en,
      R => reset
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decode_i_sync is
  port (
    clk : in STD_LOGIC;
    reset : in STD_LOGIC;
    start_i : in STD_LOGIC;
    enable : in STD_LOGIC;
    data_in : in STD_LOGIC_VECTOR ( 7 downto 0 );
    data_out : out STD_LOGIC_VECTOR ( 31 downto 0 );
    context_id_out : out STD_LOGIC_VECTOR ( 31 downto 0 );
    stop_i : out STD_LOGIC;
    out_en : out STD_LOGIC;
    fifo_overflow : out STD_LOGIC
  );
end decode_i_sync;

architecture STRUCTURE of decode_i_sync is
  signal \<const0>\ : STD_LOGIC;
  signal data_reg_s : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal en_0 : STD_LOGIC;
  signal en_1 : STD_LOGIC;
  signal en_2 : STD_LOGIC;
  signal en_3 : STD_LOGIC;
  signal \n_0_state_next_reg[0]_i_1\ : STD_LOGIC;
  signal \n_0_state_next_reg[1]_i_1\ : STD_LOGIC;
  signal \n_0_state_next_reg[2]_i_1\ : STD_LOGIC;
  signal \n_0_state_next_reg[2]_i_2\ : STD_LOGIC;
  signal \n_0_state_reg[0]_i_1\ : STD_LOGIC;
  signal \n_0_state_reg[1]_i_1\ : STD_LOGIC;
  signal \n_0_state_reg[2]_i_1\ : STD_LOGIC;
  signal state_next : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal state_reg : STD_LOGIC_VECTOR ( 2 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of out_en_INST_0 : label is "soft_lutpair10";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of \state_next_reg[0]\ : label is "LD";
  attribute SOFT_HLUTNM of \state_next_reg[0]_i_1\ : label is "soft_lutpair9";
  attribute XILINX_LEGACY_PRIM of \state_next_reg[1]\ : label is "LD";
  attribute SOFT_HLUTNM of \state_next_reg[1]_i_1\ : label is "soft_lutpair10";
  attribute XILINX_LEGACY_PRIM of \state_next_reg[2]\ : label is "LD";
  attribute SOFT_HLUTNM of \state_next_reg[2]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \state_next_reg[2]_i_2\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \state_reg[0]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \state_reg[1]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of stop_i_INST_0 : label is "soft_lutpair9";
begin
  context_id_out(31) <= \<const0>\;
  context_id_out(30) <= \<const0>\;
  context_id_out(29) <= \<const0>\;
  context_id_out(28) <= \<const0>\;
  context_id_out(27) <= \<const0>\;
  context_id_out(26) <= \<const0>\;
  context_id_out(25) <= \<const0>\;
  context_id_out(24) <= \<const0>\;
  context_id_out(23) <= \<const0>\;
  context_id_out(22) <= \<const0>\;
  context_id_out(21) <= \<const0>\;
  context_id_out(20) <= \<const0>\;
  context_id_out(19) <= \<const0>\;
  context_id_out(18) <= \<const0>\;
  context_id_out(17) <= \<const0>\;
  context_id_out(16) <= \<const0>\;
  context_id_out(15) <= \<const0>\;
  context_id_out(14) <= \<const0>\;
  context_id_out(13) <= \<const0>\;
  context_id_out(12) <= \<const0>\;
  context_id_out(11) <= \<const0>\;
  context_id_out(10) <= \<const0>\;
  context_id_out(9) <= \<const0>\;
  context_id_out(8) <= \<const0>\;
  context_id_out(7) <= \<const0>\;
  context_id_out(6) <= \<const0>\;
  context_id_out(5) <= \<const0>\;
  context_id_out(4) <= \<const0>\;
  context_id_out(3) <= \<const0>\;
  context_id_out(2) <= \<const0>\;
  context_id_out(1) <= \<const0>\;
  context_id_out(0) <= \<const0>\;
  fifo_overflow <= \<const0>\;
GND: unisim.vcomponents.GND
    port map (
      G => \<const0>\
    );
\data_reg_s_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => data_in(0),
      Q => data_reg_s(0),
      R => reset
    );
\data_reg_s_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => data_in(1),
      Q => data_reg_s(1),
      R => reset
    );
\data_reg_s_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => data_in(2),
      Q => data_reg_s(2),
      R => reset
    );
\data_reg_s_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => data_in(3),
      Q => data_reg_s(3),
      R => reset
    );
\data_reg_s_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => data_in(4),
      Q => data_reg_s(4),
      R => reset
    );
\data_reg_s_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => data_in(5),
      Q => data_reg_s(5),
      R => reset
    );
\data_reg_s_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => data_in(6),
      Q => data_reg_s(6),
      R => reset
    );
\data_reg_s_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => data_in(7),
      Q => data_reg_s(7),
      R => reset
    );
out_en_INST_0: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
    port map (
      I0 => state_reg(1),
      I1 => state_reg(0),
      I2 => state_reg(2),
      O => out_en
    );
\output_data_0[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
    port map (
      I0 => state_reg(0),
      I1 => state_reg(2),
      I2 => state_reg(1),
      O => en_0
    );
\output_data_0_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => en_0,
      D => data_reg_s(0),
      Q => data_out(0),
      R => reset
    );
\output_data_0_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => en_0,
      D => data_reg_s(1),
      Q => data_out(1),
      R => reset
    );
\output_data_0_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => en_0,
      D => data_reg_s(2),
      Q => data_out(2),
      R => reset
    );
\output_data_0_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => en_0,
      D => data_reg_s(3),
      Q => data_out(3),
      R => reset
    );
\output_data_0_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => en_0,
      D => data_reg_s(4),
      Q => data_out(4),
      R => reset
    );
\output_data_0_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => en_0,
      D => data_reg_s(5),
      Q => data_out(5),
      R => reset
    );
\output_data_0_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => en_0,
      D => data_reg_s(6),
      Q => data_out(6),
      R => reset
    );
\output_data_0_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => en_0,
      D => data_reg_s(7),
      Q => data_out(7),
      R => reset
    );
\output_data_1[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
    port map (
      I0 => state_reg(1),
      I1 => state_reg(2),
      I2 => state_reg(0),
      O => en_1
    );
\output_data_1_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => en_1,
      D => data_reg_s(0),
      Q => data_out(8),
      R => reset
    );
\output_data_1_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => en_1,
      D => data_reg_s(1),
      Q => data_out(9),
      R => reset
    );
\output_data_1_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => en_1,
      D => data_reg_s(2),
      Q => data_out(10),
      R => reset
    );
\output_data_1_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => en_1,
      D => data_reg_s(3),
      Q => data_out(11),
      R => reset
    );
\output_data_1_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => en_1,
      D => data_reg_s(4),
      Q => data_out(12),
      R => reset
    );
\output_data_1_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => en_1,
      D => data_reg_s(5),
      Q => data_out(13),
      R => reset
    );
\output_data_1_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => en_1,
      D => data_reg_s(6),
      Q => data_out(14),
      R => reset
    );
\output_data_1_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => en_1,
      D => data_reg_s(7),
      Q => data_out(15),
      R => reset
    );
\output_data_2[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
    port map (
      I0 => state_reg(2),
      I1 => state_reg(0),
      I2 => state_reg(1),
      O => en_2
    );
\output_data_2_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => en_2,
      D => data_reg_s(0),
      Q => data_out(16),
      R => reset
    );
\output_data_2_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => en_2,
      D => data_reg_s(1),
      Q => data_out(17),
      R => reset
    );
\output_data_2_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => en_2,
      D => data_reg_s(2),
      Q => data_out(18),
      R => reset
    );
\output_data_2_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => en_2,
      D => data_reg_s(3),
      Q => data_out(19),
      R => reset
    );
\output_data_2_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => en_2,
      D => data_reg_s(4),
      Q => data_out(20),
      R => reset
    );
\output_data_2_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => en_2,
      D => data_reg_s(5),
      Q => data_out(21),
      R => reset
    );
\output_data_2_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => en_2,
      D => data_reg_s(6),
      Q => data_out(22),
      R => reset
    );
\output_data_2_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => en_2,
      D => data_reg_s(7),
      Q => data_out(23),
      R => reset
    );
\output_data_3[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
    port map (
      I0 => state_reg(2),
      I1 => state_reg(0),
      I2 => state_reg(1),
      O => en_3
    );
\output_data_3_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => en_3,
      D => data_reg_s(0),
      Q => data_out(24),
      R => reset
    );
\output_data_3_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => en_3,
      D => data_reg_s(1),
      Q => data_out(25),
      R => reset
    );
\output_data_3_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => en_3,
      D => data_reg_s(2),
      Q => data_out(26),
      R => reset
    );
\output_data_3_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => en_3,
      D => data_reg_s(3),
      Q => data_out(27),
      R => reset
    );
\output_data_3_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => en_3,
      D => data_reg_s(4),
      Q => data_out(28),
      R => reset
    );
\output_data_3_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => en_3,
      D => data_reg_s(5),
      Q => data_out(29),
      R => reset
    );
\output_data_3_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => en_3,
      D => data_reg_s(6),
      Q => data_out(30),
      R => reset
    );
\output_data_3_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => en_3,
      D => data_reg_s(7),
      Q => data_out(31),
      R => reset
    );
\state_next_reg[0]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
    port map (
      CLR => '0',
      D => \n_0_state_next_reg[0]_i_1\,
      G => \n_0_state_next_reg[2]_i_2\,
      GE => '1',
      Q => state_next(0)
    );
\state_next_reg[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1514"
    )
    port map (
      I0 => state_reg(0),
      I1 => state_reg(1),
      I2 => state_reg(2),
      I3 => start_i,
      O => \n_0_state_next_reg[0]_i_1\
    );
\state_next_reg[1]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
    port map (
      CLR => '0',
      D => \n_0_state_next_reg[1]_i_1\,
      G => \n_0_state_next_reg[2]_i_2\,
      GE => '1',
      Q => state_next(1)
    );
\state_next_reg[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"26"
    )
    port map (
      I0 => state_reg(0),
      I1 => state_reg(1),
      I2 => state_reg(2),
      O => \n_0_state_next_reg[1]_i_1\
    );
\state_next_reg[2]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
    port map (
      CLR => '0',
      D => \n_0_state_next_reg[2]_i_1\,
      G => \n_0_state_next_reg[2]_i_2\,
      GE => '1',
      Q => state_next(2)
    );
\state_next_reg[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
    port map (
      I0 => state_reg(0),
      I1 => state_reg(1),
      I2 => state_reg(2),
      O => \n_0_state_next_reg[2]_i_1\
    );
\state_next_reg[2]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"7F"
    )
    port map (
      I0 => state_reg(0),
      I1 => state_reg(2),
      I2 => state_reg(1),
      O => \n_0_state_next_reg[2]_i_2\
    );
\state_reg[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
    port map (
      I0 => state_next(0),
      I1 => enable,
      O => \n_0_state_reg[0]_i_1\
    );
\state_reg[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
    port map (
      I0 => state_next(1),
      I1 => enable,
      O => \n_0_state_reg[1]_i_1\
    );
\state_reg[2]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
    port map (
      I0 => state_next(2),
      I1 => enable,
      O => \n_0_state_reg[2]_i_1\
    );
\state_reg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => \n_0_state_reg[0]_i_1\,
      Q => state_reg(0),
      R => reset
    );
\state_reg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => \n_0_state_reg[1]_i_1\,
      Q => state_reg(1),
      R => reset
    );
\state_reg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => \n_0_state_reg[2]_i_1\,
      Q => state_reg(2),
      R => reset
    );
stop_i_INST_0: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
    port map (
      I0 => state_reg(0),
      I1 => state_reg(2),
      I2 => state_reg(1),
      O => stop_i
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity pft_decoder_v2 is
  port (
    clk : in STD_LOGIC;
    reset : in STD_LOGIC;
    data_in : in STD_LOGIC_VECTOR ( 7 downto 0 );
    data_out : out STD_LOGIC_VECTOR ( 7 downto 0 );
    enable : in STD_LOGIC;
    start_i_sync : out STD_LOGIC;
    stop_i_sync : in STD_LOGIC;
    atomes : out STD_LOGIC_VECTOR ( 4 downto 0 );
    start_bap : out STD_LOGIC;
    stop_bap : in STD_LOGIC;
    start_waypoint : out STD_LOGIC;
    stop_waypoint : in STD_LOGIC
  );
end pft_decoder_v2;

architecture STRUCTURE of pft_decoder_v2 is
  signal \<const0>\ : STD_LOGIC;
  signal \n_0_state_next_reg[0]_i_1\ : STD_LOGIC;
  signal \n_0_state_next_reg[0]_i_2\ : STD_LOGIC;
  signal \n_0_state_next_reg[0]_i_3\ : STD_LOGIC;
  signal \n_0_state_next_reg[0]_i_4\ : STD_LOGIC;
  signal \n_0_state_next_reg[0]_i_5\ : STD_LOGIC;
  signal \n_0_state_next_reg[1]_i_1\ : STD_LOGIC;
  signal \n_0_state_next_reg[1]_i_2\ : STD_LOGIC;
  signal \n_0_state_next_reg[1]_i_3\ : STD_LOGIC;
  signal \n_0_state_next_reg[1]_i_4\ : STD_LOGIC;
  signal \n_0_state_next_reg[1]_i_5\ : STD_LOGIC;
  signal \n_0_state_next_reg[2]_i_1\ : STD_LOGIC;
  signal \n_0_state_next_reg[2]_i_2\ : STD_LOGIC;
  signal \n_0_state_next_reg[2]_i_3\ : STD_LOGIC;
  signal \n_0_state_next_reg[2]_i_4\ : STD_LOGIC;
  signal \n_0_state_next_reg[2]_i_5\ : STD_LOGIC;
  signal \n_0_state_next_reg[3]_i_1\ : STD_LOGIC;
  signal \n_0_state_next_reg[3]_i_2\ : STD_LOGIC;
  signal \n_0_state_reg[0]_i_1\ : STD_LOGIC;
  signal \n_0_state_reg[1]_i_1\ : STD_LOGIC;
  signal \n_0_state_reg[2]_i_1\ : STD_LOGIC;
  signal \n_0_state_reg[3]_i_1\ : STD_LOGIC;
  signal state_next : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal state_reg : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of start_bap_INST_0 : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of start_i_sync_INST_0 : label is "soft_lutpair3";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of \state_next_reg[0]\ : label is "LD";
  attribute SOFT_HLUTNM of \state_next_reg[0]_i_2\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \state_next_reg[0]_i_5\ : label is "soft_lutpair4";
  attribute XILINX_LEGACY_PRIM of \state_next_reg[1]\ : label is "LD";
  attribute SOFT_HLUTNM of \state_next_reg[1]_i_2\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \state_next_reg[1]_i_3\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \state_next_reg[1]_i_4\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \state_next_reg[1]_i_5\ : label is "soft_lutpair2";
  attribute XILINX_LEGACY_PRIM of \state_next_reg[2]\ : label is "LD";
  attribute SOFT_HLUTNM of \state_next_reg[2]_i_2\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \state_next_reg[2]_i_3\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \state_next_reg[2]_i_4\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \state_next_reg[2]_i_5\ : label is "soft_lutpair0";
  attribute XILINX_LEGACY_PRIM of \state_next_reg[3]\ : label is "LD";
  attribute SOFT_HLUTNM of \state_next_reg[3]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \state_next_reg[3]_i_2\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \state_reg[0]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \state_reg[1]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \state_reg[2]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \state_reg[3]_i_1\ : label is "soft_lutpair8";
begin
  atomes(4) <= \<const0>\;
  atomes(3) <= \<const0>\;
  atomes(2) <= \<const0>\;
  atomes(1) <= \<const0>\;
  atomes(0) <= \<const0>\;
  data_out(7) <= \<const0>\;
  data_out(6) <= \<const0>\;
  data_out(5) <= \<const0>\;
  data_out(4) <= \<const0>\;
  data_out(3) <= \<const0>\;
  data_out(2) <= \<const0>\;
  data_out(1) <= \<const0>\;
  data_out(0) <= \<const0>\;
  start_waypoint <= \<const0>\;
GND: unisim.vcomponents.GND
    port map (
      G => \<const0>\
    );
start_bap_INST_0: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0006"
    )
    port map (
      I0 => state_next(3),
      I1 => state_next(2),
      I2 => state_next(0),
      I3 => state_next(1),
      O => start_bap
    );
start_i_sync_INST_0: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4004"
    )
    port map (
      I0 => state_next(3),
      I1 => state_next(1),
      I2 => state_next(0),
      I3 => state_next(2),
      O => start_i_sync
    );
\state_next_reg[0]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
    port map (
      CLR => '0',
      D => \n_0_state_next_reg[0]_i_1\,
      G => \n_0_state_next_reg[3]_i_2\,
      GE => '1',
      Q => state_next(0)
    );
\state_next_reg[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAFEEFAAAA"
    )
    port map (
      I0 => \n_0_state_next_reg[0]_i_2\,
      I1 => \n_0_state_next_reg[0]_i_3\,
      I2 => data_in(7),
      I3 => \n_0_state_next_reg[0]_i_4\,
      I4 => \n_0_state_next_reg[0]_i_5\,
      I5 => data_in(0),
      O => \n_0_state_next_reg[0]_i_1\
    );
\state_next_reg[0]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"04033C00"
    )
    port map (
      I0 => stop_i_sync,
      I1 => state_reg(2),
      I2 => state_reg(3),
      I3 => state_reg(0),
      I4 => state_reg(1),
      O => \n_0_state_next_reg[0]_i_2\
    );
\state_next_reg[0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0020000000000000"
    )
    port map (
      I0 => data_in(4),
      I1 => data_in(3),
      I2 => data_in(1),
      I3 => data_in(2),
      I4 => data_in(6),
      I5 => data_in(5),
      O => \n_0_state_next_reg[0]_i_3\
    );
\state_next_reg[0]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
    port map (
      I0 => data_in(2),
      I1 => data_in(4),
      I2 => data_in(6),
      I3 => data_in(5),
      I4 => data_in(1),
      I5 => data_in(3),
      O => \n_0_state_next_reg[0]_i_4\
    );
\state_next_reg[0]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFEB"
    )
    port map (
      I0 => state_reg(1),
      I1 => state_reg(2),
      I2 => state_reg(3),
      I3 => stop_bap,
      O => \n_0_state_next_reg[0]_i_5\
    );
\state_next_reg[1]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
    port map (
      CLR => '0',
      D => \n_0_state_next_reg[1]_i_1\,
      G => \n_0_state_next_reg[3]_i_2\,
      GE => '1',
      Q => state_next(1)
    );
\state_next_reg[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEEEEAAAEAAAEAAA"
    )
    port map (
      I0 => \n_0_state_next_reg[2]_i_4\,
      I1 => \n_0_state_next_reg[2]_i_5\,
      I2 => \n_0_state_next_reg[1]_i_2\,
      I3 => \n_0_state_next_reg[1]_i_3\,
      I4 => \n_0_state_next_reg[1]_i_4\,
      I5 => \n_0_state_next_reg[1]_i_5\,
      O => \n_0_state_next_reg[1]_i_1\
    );
\state_next_reg[1]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => data_in(3),
      I1 => data_in(0),
      O => \n_0_state_next_reg[1]_i_2\
    );
\state_next_reg[1]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"20000001"
    )
    port map (
      I0 => data_in(2),
      I1 => data_in(4),
      I2 => data_in(6),
      I3 => data_in(5),
      I4 => data_in(1),
      O => \n_0_state_next_reg[1]_i_3\
    );
\state_next_reg[1]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => data_in(7),
      I1 => data_in(0),
      O => \n_0_state_next_reg[1]_i_4\
    );
\state_next_reg[1]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
    port map (
      I0 => data_in(1),
      I1 => data_in(5),
      I2 => data_in(6),
      I3 => data_in(4),
      I4 => data_in(2),
      O => \n_0_state_next_reg[1]_i_5\
    );
\state_next_reg[2]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
    port map (
      CLR => '0',
      D => \n_0_state_next_reg[2]_i_1\,
      G => \n_0_state_next_reg[3]_i_2\,
      GE => '1',
      Q => state_next(2)
    );
\state_next_reg[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF8F0F0"
    )
    port map (
      I0 => \n_0_state_next_reg[2]_i_2\,
      I1 => \n_0_state_next_reg[2]_i_3\,
      I2 => \n_0_state_next_reg[2]_i_4\,
      I3 => data_in(0),
      I4 => \n_0_state_next_reg[2]_i_5\,
      O => \n_0_state_next_reg[2]_i_1\
    );
\state_next_reg[2]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0800"
    )
    port map (
      I0 => data_in(5),
      I1 => data_in(1),
      I2 => data_in(7),
      I3 => data_in(6),
      O => \n_0_state_next_reg[2]_i_2\
    );
\state_next_reg[2]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"18"
    )
    port map (
      I0 => data_in(2),
      I1 => data_in(3),
      I2 => data_in(4),
      O => \n_0_state_next_reg[2]_i_3\
    );
\state_next_reg[2]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00044004"
    )
    port map (
      I0 => state_reg(3),
      I1 => state_reg(1),
      I2 => state_reg(0),
      I3 => state_reg(2),
      I4 => stop_i_sync,
      O => \n_0_state_next_reg[2]_i_4\
    );
\state_next_reg[2]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF4F4FF"
    )
    port map (
      I0 => state_reg(0),
      I1 => stop_bap,
      I2 => state_reg(1),
      I3 => state_reg(2),
      I4 => state_reg(3),
      O => \n_0_state_next_reg[2]_i_5\
    );
\state_next_reg[3]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
    port map (
      CLR => '0',
      D => \n_0_state_next_reg[3]_i_1\,
      G => \n_0_state_next_reg[3]_i_2\,
      GE => '1',
      Q => state_next(3)
    );
\state_next_reg[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"06000606"
    )
    port map (
      I0 => state_reg(3),
      I1 => state_reg(2),
      I2 => state_reg(1),
      I3 => state_reg(0),
      I4 => stop_bap,
      O => \n_0_state_next_reg[3]_i_1\
    );
\state_next_reg[3]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FDFF"
    )
    port map (
      I0 => state_reg(3),
      I1 => state_reg(2),
      I2 => state_reg(0),
      I3 => state_reg(1),
      O => \n_0_state_next_reg[3]_i_2\
    );
\state_reg[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
    port map (
      I0 => state_next(0),
      I1 => enable,
      I2 => reset,
      O => \n_0_state_reg[0]_i_1\
    );
\state_reg[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
    port map (
      I0 => state_next(1),
      I1 => enable,
      O => \n_0_state_reg[1]_i_1\
    );
\state_reg[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
    port map (
      I0 => state_next(2),
      I1 => enable,
      I2 => reset,
      O => \n_0_state_reg[2]_i_1\
    );
\state_reg[3]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
    port map (
      I0 => state_next(3),
      I1 => enable,
      O => \n_0_state_reg[3]_i_1\
    );
\state_reg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => \n_0_state_reg[0]_i_1\,
      Q => state_reg(0),
      R => \<const0>\
    );
\state_reg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => \n_0_state_reg[1]_i_1\,
      Q => state_reg(1),
      R => reset
    );
\state_reg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => \n_0_state_reg[2]_i_1\,
      Q => state_reg(2),
      R => \<const0>\
    );
\state_reg_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => clk,
      CE => '1',
      D => \n_0_state_reg[3]_i_1\,
      Q => state_reg(3),
      R => reset
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity datapath is
  port (
    clk : in STD_LOGIC;
    reset : in STD_LOGIC;
    enable : in STD_LOGIC;
    trace_data_in : in STD_LOGIC_VECTOR ( 7 downto 0 );
    start_b : in STD_LOGIC;
    start_i : in STD_LOGIC;
    start_w : in STD_LOGIC;
    stop_b : out STD_LOGIC;
    stop_i : out STD_LOGIC;
    stop_w : out STD_LOGIC;
    pc : out STD_LOGIC_VECTOR ( 31 downto 0 );
    out_en : out STD_LOGIC;
    fifo_overflow : out STD_LOGIC;
    waypoint_address : out STD_LOGIC_VECTOR ( 31 downto 0 );
    context_id : out STD_LOGIC_VECTOR ( 31 downto 0 );
    waypoint_address_en : out STD_LOGIC
  );
end datapath;

architecture STRUCTURE of datapath is
  signal \<const0>\ : STD_LOGIC;
  signal enable_i : STD_LOGIC;
  signal instruction_address : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_u_decode_bap_atom_output_e_UNCONNECTED : STD_LOGIC;
  signal NLW_u_decode_i_sync_fifo_overflow_UNCONNECTED : STD_LOGIC;
  signal NLW_u_decode_i_sync_context_id_out_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
begin
  context_id(31) <= \<const0>\;
  context_id(30) <= \<const0>\;
  context_id(29) <= \<const0>\;
  context_id(28) <= \<const0>\;
  context_id(27) <= \<const0>\;
  context_id(26) <= \<const0>\;
  context_id(25) <= \<const0>\;
  context_id(24) <= \<const0>\;
  context_id(23) <= \<const0>\;
  context_id(22) <= \<const0>\;
  context_id(21) <= \<const0>\;
  context_id(20) <= \<const0>\;
  context_id(19) <= \<const0>\;
  context_id(18) <= \<const0>\;
  context_id(17) <= \<const0>\;
  context_id(16) <= \<const0>\;
  context_id(15) <= \<const0>\;
  context_id(14) <= \<const0>\;
  context_id(13) <= \<const0>\;
  context_id(12) <= \<const0>\;
  context_id(11) <= \<const0>\;
  context_id(10) <= \<const0>\;
  context_id(9) <= \<const0>\;
  context_id(8) <= \<const0>\;
  context_id(7) <= \<const0>\;
  context_id(6) <= \<const0>\;
  context_id(5) <= \<const0>\;
  context_id(4) <= \<const0>\;
  context_id(3) <= \<const0>\;
  context_id(2) <= \<const0>\;
  context_id(1) <= \<const0>\;
  context_id(0) <= \<const0>\;
  fifo_overflow <= \<const0>\;
  stop_w <= \<const0>\;
  waypoint_address(31) <= \<const0>\;
  waypoint_address(30) <= \<const0>\;
  waypoint_address(29) <= \<const0>\;
  waypoint_address(28) <= \<const0>\;
  waypoint_address(27) <= \<const0>\;
  waypoint_address(26) <= \<const0>\;
  waypoint_address(25) <= \<const0>\;
  waypoint_address(24) <= \<const0>\;
  waypoint_address(23) <= \<const0>\;
  waypoint_address(22) <= \<const0>\;
  waypoint_address(21) <= \<const0>\;
  waypoint_address(20) <= \<const0>\;
  waypoint_address(19) <= \<const0>\;
  waypoint_address(18) <= \<const0>\;
  waypoint_address(17) <= \<const0>\;
  waypoint_address(16) <= \<const0>\;
  waypoint_address(15) <= \<const0>\;
  waypoint_address(14) <= \<const0>\;
  waypoint_address(13) <= \<const0>\;
  waypoint_address(12) <= \<const0>\;
  waypoint_address(11) <= \<const0>\;
  waypoint_address(10) <= \<const0>\;
  waypoint_address(9) <= \<const0>\;
  waypoint_address(8) <= \<const0>\;
  waypoint_address(7) <= \<const0>\;
  waypoint_address(6) <= \<const0>\;
  waypoint_address(5) <= \<const0>\;
  waypoint_address(4) <= \<const0>\;
  waypoint_address(3) <= \<const0>\;
  waypoint_address(2) <= \<const0>\;
  waypoint_address(1) <= \<const0>\;
  waypoint_address(0) <= \<const0>\;
  waypoint_address_en <= \<const0>\;
GND: unisim.vcomponents.GND
    port map (
      G => \<const0>\
    );
u_decode_bap: entity work.decode_bap
    port map (
      atom_output_e => NLW_u_decode_bap_atom_output_e_UNCONNECTED,
      clk => clk,
      data_in(7 downto 0) => trace_data_in(7 downto 0),
      enable => enable,
      enable_i => enable_i,
      instruction_address(31 downto 0) => instruction_address(31 downto 0),
      pc(31 downto 0) => pc(31 downto 0),
      reset => reset,
      start_b => start_b,
      stop_b => stop_b,
      w_en => out_en
    );
u_decode_i_sync: entity work.decode_i_sync
    port map (
      clk => clk,
      context_id_out(31 downto 0) => NLW_u_decode_i_sync_context_id_out_UNCONNECTED(31 downto 0),
      data_in(7 downto 0) => trace_data_in(7 downto 0),
      data_out(31 downto 0) => instruction_address(31 downto 0),
      enable => enable,
      fifo_overflow => NLW_u_decode_i_sync_fifo_overflow_UNCONNECTED,
      out_en => enable_i,
      reset => reset,
      start_i => start_i,
      stop_i => stop_i
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decodeur_traces is
  port (
    clk : in STD_LOGIC;
    reset : in STD_LOGIC;
    enable : in STD_LOGIC;
    trace_data : in STD_LOGIC_VECTOR ( 7 downto 0 );
    pc : out STD_LOGIC_VECTOR ( 31 downto 0 );
    w_en : out STD_LOGIC;
    fifo_overflow : out STD_LOGIC;
    waypoint_address : out STD_LOGIC_VECTOR ( 31 downto 0 );
    waypoint_address_en : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decodeur_traces : entity is true;
end decodeur_traces;

architecture STRUCTURE of decodeur_traces is
  signal clk_IBUF : STD_LOGIC;
  signal clk_IBUF_BUFG : STD_LOGIC;
  signal enable_IBUF : STD_LOGIC;
  signal pc_OBUF : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal reset_IBUF : STD_LOGIC;
  signal start_b : STD_LOGIC;
  signal start_i : STD_LOGIC;
  signal stop_b : STD_LOGIC;
  signal stop_i : STD_LOGIC;
  signal trace_data_IBUF : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal w_en_OBUF : STD_LOGIC;
  signal NLW_chemin_fifo_overflow_UNCONNECTED : STD_LOGIC;
  signal NLW_chemin_stop_w_UNCONNECTED : STD_LOGIC;
  signal NLW_chemin_waypoint_address_en_UNCONNECTED : STD_LOGIC;
  signal NLW_chemin_context_id_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_chemin_waypoint_address_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_fsm_start_waypoint_UNCONNECTED : STD_LOGIC;
  signal NLW_fsm_atomes_UNCONNECTED : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal NLW_fsm_data_out_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
begin
chemin: entity work.datapath
    port map (
      clk => clk_IBUF_BUFG,
      context_id(31 downto 0) => NLW_chemin_context_id_UNCONNECTED(31 downto 0),
      enable => enable_IBUF,
      fifo_overflow => NLW_chemin_fifo_overflow_UNCONNECTED,
      out_en => w_en_OBUF,
      pc(31 downto 0) => pc_OBUF(31 downto 0),
      reset => reset_IBUF,
      start_b => start_b,
      start_i => start_i,
      start_w => '0',
      stop_b => stop_b,
      stop_i => stop_i,
      stop_w => NLW_chemin_stop_w_UNCONNECTED,
      trace_data_in(7 downto 0) => trace_data_IBUF(7 downto 0),
      waypoint_address(31 downto 0) => NLW_chemin_waypoint_address_UNCONNECTED(31 downto 0),
      waypoint_address_en => NLW_chemin_waypoint_address_en_UNCONNECTED
    );
clk_IBUF_BUFG_inst: unisim.vcomponents.BUFG
    port map (
      I => clk_IBUF,
      O => clk_IBUF_BUFG
    );
clk_IBUF_inst: unisim.vcomponents.IBUF
    port map (
      I => clk,
      O => clk_IBUF
    );
enable_IBUF_inst: unisim.vcomponents.IBUF
    port map (
      I => enable,
      O => enable_IBUF
    );
fifo_overflow_OBUF_inst: unisim.vcomponents.OBUF
    port map (
      I => '0',
      O => fifo_overflow
    );
fsm: entity work.pft_decoder_v2
    port map (
      atomes(4 downto 0) => NLW_fsm_atomes_UNCONNECTED(4 downto 0),
      clk => clk_IBUF_BUFG,
      data_in(7 downto 0) => trace_data_IBUF(7 downto 0),
      data_out(7 downto 0) => NLW_fsm_data_out_UNCONNECTED(7 downto 0),
      enable => enable_IBUF,
      reset => reset_IBUF,
      start_bap => start_b,
      start_i_sync => start_i,
      start_waypoint => NLW_fsm_start_waypoint_UNCONNECTED,
      stop_bap => stop_b,
      stop_i_sync => stop_i,
      stop_waypoint => '0'
    );
\pc_OBUF[0]_inst\: unisim.vcomponents.OBUF
    port map (
      I => pc_OBUF(0),
      O => pc(0)
    );
\pc_OBUF[10]_inst\: unisim.vcomponents.OBUF
    port map (
      I => pc_OBUF(10),
      O => pc(10)
    );
\pc_OBUF[11]_inst\: unisim.vcomponents.OBUF
    port map (
      I => pc_OBUF(11),
      O => pc(11)
    );
\pc_OBUF[12]_inst\: unisim.vcomponents.OBUF
    port map (
      I => pc_OBUF(12),
      O => pc(12)
    );
\pc_OBUF[13]_inst\: unisim.vcomponents.OBUF
    port map (
      I => pc_OBUF(13),
      O => pc(13)
    );
\pc_OBUF[14]_inst\: unisim.vcomponents.OBUF
    port map (
      I => pc_OBUF(14),
      O => pc(14)
    );
\pc_OBUF[15]_inst\: unisim.vcomponents.OBUF
    port map (
      I => pc_OBUF(15),
      O => pc(15)
    );
\pc_OBUF[16]_inst\: unisim.vcomponents.OBUF
    port map (
      I => pc_OBUF(16),
      O => pc(16)
    );
\pc_OBUF[17]_inst\: unisim.vcomponents.OBUF
    port map (
      I => pc_OBUF(17),
      O => pc(17)
    );
\pc_OBUF[18]_inst\: unisim.vcomponents.OBUF
    port map (
      I => pc_OBUF(18),
      O => pc(18)
    );
\pc_OBUF[19]_inst\: unisim.vcomponents.OBUF
    port map (
      I => pc_OBUF(19),
      O => pc(19)
    );
\pc_OBUF[1]_inst\: unisim.vcomponents.OBUF
    port map (
      I => pc_OBUF(1),
      O => pc(1)
    );
\pc_OBUF[20]_inst\: unisim.vcomponents.OBUF
    port map (
      I => pc_OBUF(20),
      O => pc(20)
    );
\pc_OBUF[21]_inst\: unisim.vcomponents.OBUF
    port map (
      I => pc_OBUF(21),
      O => pc(21)
    );
\pc_OBUF[22]_inst\: unisim.vcomponents.OBUF
    port map (
      I => pc_OBUF(22),
      O => pc(22)
    );
\pc_OBUF[23]_inst\: unisim.vcomponents.OBUF
    port map (
      I => pc_OBUF(23),
      O => pc(23)
    );
\pc_OBUF[24]_inst\: unisim.vcomponents.OBUF
    port map (
      I => pc_OBUF(24),
      O => pc(24)
    );
\pc_OBUF[25]_inst\: unisim.vcomponents.OBUF
    port map (
      I => pc_OBUF(25),
      O => pc(25)
    );
\pc_OBUF[26]_inst\: unisim.vcomponents.OBUF
    port map (
      I => pc_OBUF(26),
      O => pc(26)
    );
\pc_OBUF[27]_inst\: unisim.vcomponents.OBUF
    port map (
      I => pc_OBUF(27),
      O => pc(27)
    );
\pc_OBUF[28]_inst\: unisim.vcomponents.OBUF
    port map (
      I => pc_OBUF(28),
      O => pc(28)
    );
\pc_OBUF[29]_inst\: unisim.vcomponents.OBUF
    port map (
      I => pc_OBUF(29),
      O => pc(29)
    );
\pc_OBUF[2]_inst\: unisim.vcomponents.OBUF
    port map (
      I => pc_OBUF(2),
      O => pc(2)
    );
\pc_OBUF[30]_inst\: unisim.vcomponents.OBUF
    port map (
      I => pc_OBUF(30),
      O => pc(30)
    );
\pc_OBUF[31]_inst\: unisim.vcomponents.OBUF
    port map (
      I => pc_OBUF(31),
      O => pc(31)
    );
\pc_OBUF[3]_inst\: unisim.vcomponents.OBUF
    port map (
      I => pc_OBUF(3),
      O => pc(3)
    );
\pc_OBUF[4]_inst\: unisim.vcomponents.OBUF
    port map (
      I => pc_OBUF(4),
      O => pc(4)
    );
\pc_OBUF[5]_inst\: unisim.vcomponents.OBUF
    port map (
      I => pc_OBUF(5),
      O => pc(5)
    );
\pc_OBUF[6]_inst\: unisim.vcomponents.OBUF
    port map (
      I => pc_OBUF(6),
      O => pc(6)
    );
\pc_OBUF[7]_inst\: unisim.vcomponents.OBUF
    port map (
      I => pc_OBUF(7),
      O => pc(7)
    );
\pc_OBUF[8]_inst\: unisim.vcomponents.OBUF
    port map (
      I => pc_OBUF(8),
      O => pc(8)
    );
\pc_OBUF[9]_inst\: unisim.vcomponents.OBUF
    port map (
      I => pc_OBUF(9),
      O => pc(9)
    );
reset_IBUF_inst: unisim.vcomponents.IBUF
    port map (
      I => reset,
      O => reset_IBUF
    );
\trace_data_IBUF[0]_inst\: unisim.vcomponents.IBUF
    port map (
      I => trace_data(0),
      O => trace_data_IBUF(0)
    );
\trace_data_IBUF[1]_inst\: unisim.vcomponents.IBUF
    port map (
      I => trace_data(1),
      O => trace_data_IBUF(1)
    );
\trace_data_IBUF[2]_inst\: unisim.vcomponents.IBUF
    port map (
      I => trace_data(2),
      O => trace_data_IBUF(2)
    );
\trace_data_IBUF[3]_inst\: unisim.vcomponents.IBUF
    port map (
      I => trace_data(3),
      O => trace_data_IBUF(3)
    );
\trace_data_IBUF[4]_inst\: unisim.vcomponents.IBUF
    port map (
      I => trace_data(4),
      O => trace_data_IBUF(4)
    );
\trace_data_IBUF[5]_inst\: unisim.vcomponents.IBUF
    port map (
      I => trace_data(5),
      O => trace_data_IBUF(5)
    );
\trace_data_IBUF[6]_inst\: unisim.vcomponents.IBUF
    port map (
      I => trace_data(6),
      O => trace_data_IBUF(6)
    );
\trace_data_IBUF[7]_inst\: unisim.vcomponents.IBUF
    port map (
      I => trace_data(7),
      O => trace_data_IBUF(7)
    );
w_en_OBUF_inst: unisim.vcomponents.OBUF
    port map (
      I => w_en_OBUF,
      O => w_en
    );
\waypoint_address_OBUF[0]_inst\: unisim.vcomponents.OBUFT
    port map (
      I => '0',
      O => waypoint_address(0),
      T => '1'
    );
\waypoint_address_OBUF[10]_inst\: unisim.vcomponents.OBUFT
    port map (
      I => '0',
      O => waypoint_address(10),
      T => '1'
    );
\waypoint_address_OBUF[11]_inst\: unisim.vcomponents.OBUFT
    port map (
      I => '0',
      O => waypoint_address(11),
      T => '1'
    );
\waypoint_address_OBUF[12]_inst\: unisim.vcomponents.OBUFT
    port map (
      I => '0',
      O => waypoint_address(12),
      T => '1'
    );
\waypoint_address_OBUF[13]_inst\: unisim.vcomponents.OBUFT
    port map (
      I => '0',
      O => waypoint_address(13),
      T => '1'
    );
\waypoint_address_OBUF[14]_inst\: unisim.vcomponents.OBUFT
    port map (
      I => '0',
      O => waypoint_address(14),
      T => '1'
    );
\waypoint_address_OBUF[15]_inst\: unisim.vcomponents.OBUFT
    port map (
      I => '0',
      O => waypoint_address(15),
      T => '1'
    );
\waypoint_address_OBUF[16]_inst\: unisim.vcomponents.OBUFT
    port map (
      I => '0',
      O => waypoint_address(16),
      T => '1'
    );
\waypoint_address_OBUF[17]_inst\: unisim.vcomponents.OBUFT
    port map (
      I => '0',
      O => waypoint_address(17),
      T => '1'
    );
\waypoint_address_OBUF[18]_inst\: unisim.vcomponents.OBUFT
    port map (
      I => '0',
      O => waypoint_address(18),
      T => '1'
    );
\waypoint_address_OBUF[19]_inst\: unisim.vcomponents.OBUFT
    port map (
      I => '0',
      O => waypoint_address(19),
      T => '1'
    );
\waypoint_address_OBUF[1]_inst\: unisim.vcomponents.OBUFT
    port map (
      I => '0',
      O => waypoint_address(1),
      T => '1'
    );
\waypoint_address_OBUF[20]_inst\: unisim.vcomponents.OBUFT
    port map (
      I => '0',
      O => waypoint_address(20),
      T => '1'
    );
\waypoint_address_OBUF[21]_inst\: unisim.vcomponents.OBUFT
    port map (
      I => '0',
      O => waypoint_address(21),
      T => '1'
    );
\waypoint_address_OBUF[22]_inst\: unisim.vcomponents.OBUFT
    port map (
      I => '0',
      O => waypoint_address(22),
      T => '1'
    );
\waypoint_address_OBUF[23]_inst\: unisim.vcomponents.OBUFT
    port map (
      I => '0',
      O => waypoint_address(23),
      T => '1'
    );
\waypoint_address_OBUF[24]_inst\: unisim.vcomponents.OBUFT
    port map (
      I => '0',
      O => waypoint_address(24),
      T => '1'
    );
\waypoint_address_OBUF[25]_inst\: unisim.vcomponents.OBUFT
    port map (
      I => '0',
      O => waypoint_address(25),
      T => '1'
    );
\waypoint_address_OBUF[26]_inst\: unisim.vcomponents.OBUFT
    port map (
      I => '0',
      O => waypoint_address(26),
      T => '1'
    );
\waypoint_address_OBUF[27]_inst\: unisim.vcomponents.OBUFT
    port map (
      I => '0',
      O => waypoint_address(27),
      T => '1'
    );
\waypoint_address_OBUF[28]_inst\: unisim.vcomponents.OBUFT
    port map (
      I => '0',
      O => waypoint_address(28),
      T => '1'
    );
\waypoint_address_OBUF[29]_inst\: unisim.vcomponents.OBUFT
    port map (
      I => '0',
      O => waypoint_address(29),
      T => '1'
    );
\waypoint_address_OBUF[2]_inst\: unisim.vcomponents.OBUFT
    port map (
      I => '0',
      O => waypoint_address(2),
      T => '1'
    );
\waypoint_address_OBUF[30]_inst\: unisim.vcomponents.OBUFT
    port map (
      I => '0',
      O => waypoint_address(30),
      T => '1'
    );
\waypoint_address_OBUF[31]_inst\: unisim.vcomponents.OBUFT
    port map (
      I => '0',
      O => waypoint_address(31),
      T => '1'
    );
\waypoint_address_OBUF[3]_inst\: unisim.vcomponents.OBUFT
    port map (
      I => '0',
      O => waypoint_address(3),
      T => '1'
    );
\waypoint_address_OBUF[4]_inst\: unisim.vcomponents.OBUFT
    port map (
      I => '0',
      O => waypoint_address(4),
      T => '1'
    );
\waypoint_address_OBUF[5]_inst\: unisim.vcomponents.OBUFT
    port map (
      I => '0',
      O => waypoint_address(5),
      T => '1'
    );
\waypoint_address_OBUF[6]_inst\: unisim.vcomponents.OBUFT
    port map (
      I => '0',
      O => waypoint_address(6),
      T => '1'
    );
\waypoint_address_OBUF[7]_inst\: unisim.vcomponents.OBUFT
    port map (
      I => '0',
      O => waypoint_address(7),
      T => '1'
    );
\waypoint_address_OBUF[8]_inst\: unisim.vcomponents.OBUFT
    port map (
      I => '0',
      O => waypoint_address(8),
      T => '1'
    );
\waypoint_address_OBUF[9]_inst\: unisim.vcomponents.OBUFT
    port map (
      I => '0',
      O => waypoint_address(9),
      T => '1'
    );
waypoint_address_en_OBUF_inst: unisim.vcomponents.OBUFT
    port map (
      I => '0',
      O => waypoint_address_en,
      T => '1'
    );
end STRUCTURE;
