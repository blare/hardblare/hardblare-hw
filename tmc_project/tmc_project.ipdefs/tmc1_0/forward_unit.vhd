library ieee;
use ieee.std_logic_1164.all;
use work.dependencies_pack.all;

entity forward_unit is 
port (
	null_instruction: in std_logic;
	rs1_index_dec: in std_logic_vector(4 downto 0); -- dec stage
	rs1_index 	: in std_logic_vector(4 downto 0); -- exec stage

	rs2_index_dec: in std_logic_vector(4 downto 0); -- dec stage
	rs2_index 	: in std_logic_vector(4 downto 0); -- exec stage
	--rs2_index_mem: in std_logic_vector(5 downto 0); -- mem stage
	--rs2_index_wb : in std_logic_vector(5 downto 0); -- wb stage

	rd_index 	: in std_logic_vector(4 downto 0); -- exec stage
	rd_index_wb : in std_logic_vector(4 downto 0); -- wb stage
	rd_index_dec: in std_logic_vector(4 downto 0); -- dec stage
	rd_index_mem: in std_logic_vector(4 downto 0); -- mem stage	

	tag_mem_source_ex 	: in  tag_mem_source_type; -- exec stage

	--ldr_instr 	: in std_logic; -- wb of load instruction	
	forward_a	: out std_logic_vector(1 downto 0);
	forward_b	: out std_logic_vector(1 downto 0)
	);
end entity;

architecture behavioral of forward_unit is 

begin 	


--- Forwarding conditions for TMC core
--    - Init format 
--        - If not memory operation then rd_index_mem = rs_index_dec then forward rs.
--    - TR format
--        - If not memory operation then (It's the case for all instructions of this type)
--            - if rd_index_mem = rs1_index_ex then forward rs1.
--            - if rd_index_mem = rs2_index_ex then forward rs2.
	process(rs1_index, rs2_index, rd_index_mem, rd_index_wb, tag_mem_source_ex)
	variable mem_instr	: std_logic;
	begin

		mem_instr := '0';

		if (tag_mem_source_ex = tag_mem_none or tag_mem_source_ex = tag_mem_tlb_write) then 
			mem_instr := '0';
		else
			mem_instr := '1';
		end if;

		if (mem_instr = '1' and rd_index_mem = rs1_index) then 
			forward_a <= "01"; -- mem stage forwarding
		elsif (mem_instr = '1' and rd_index_mem = rs2_index) then 
			forward_b <= "01"; -- mem stage forwarding
		end if;

		if (mem_instr = '1' and rd_index_wb = rs1_index) then 
			forward_a <= "10"; -- write back stage forwarding
		elsif (mem_instr = '1' and rd_index_wb = rs2_index) then 
			forward_b <= "10"; -- write back stage forwarding
		end if;
	end process;

end architecture;
