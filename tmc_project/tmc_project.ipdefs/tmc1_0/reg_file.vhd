---------------------------------------------------------------------
-- TITLE: Register File
-- AUTHOR: MAW
-- DATE CREATED: 25/10/2017
-- FILENAME: reg_file.vhd
-- PROJECT: DIFT coprocessor
-- DESCRIPTION:
--    Implements a register bank with 32 registers that are 32-bits wide.
--    There are three read-ports and one write port.
---------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.dependencies_pack.all;

entity reg_file is
port (
  clk         : in  std_logic;
  enable      : in  std_logic;
  enable_2    : in  std_logic;
  w_en        : in  std_logic; -- for registers 0 - 23
  --sp_en       : in  std_logic; -- for register 30
  --pc_en       : in  std_logic; -- for register 31
  data_in     : in  std_logic_vector(31 downto 0); 
  data_out_a  : out std_logic_vector(31 downto 0);
  data_out_b  : out std_logic_vector(31 downto 0);
  data_out_c  : out std_logic_vector(31 downto 0); -- pc tag value output
  sel_a       : in  std_logic_vector(4 downto 0); 
  sel_b       : in  std_logic_vector(4 downto 0);
  sel_d       : in  std_logic_vector(4 downto 0) );
end entity; --reg_file

architecture beahvioral of reg_file is
  type reg_array_t is array (0 to 31) of std_logic_vector(31 downto 0);
  signal regf_s : reg_array_t := (others => (others=>'0'));
  begin
    process(clk)
    begin
      if rising_edge(clk) then
        if w_en = '1' then
          regf_s(to_integer(unsigned(sel_d))) <= data_in;
          --elsif pc_en = '1' then  
          --  regf_s(to_integer(unsigned("11110"))) <= regf_s(to_integer(unsigned("11110"))) + 4; -- update SP
          --elsif pc_en = '1' then 
          --  regf_s(to_integer(unsigned("11111"))) <= regf_s(to_integer(unsigned("11111"))) + 4; -- update PC
        end if;
      end if; -- clk and enable 
    end process;

    read_proc_a: process(enable, sel_a, sel_d, data_in, regf_s)
    begin
      if enable = '1' then 
        if sel_a = sel_d then 
          data_out_a <= data_in;
        else
          data_out_a <= regf_s(to_integer(unsigned(sel_a)));
        end if;
      else
        data_out_a <= ZERO;
      end if;
    end process; 

    read_proc_b: process(enable_2, sel_b, sel_d, data_in, regf_s)
    begin
      if enable_2 = '1' then 
        if sel_b = sel_d then 
          data_out_b <= data_in;
        else
          data_out_b <= regf_s(to_integer(unsigned(sel_b)));
        end if;
      else
        data_out_b <= ZERO;
      end if;
    end process; 

    read_proc_pc: process(clk)
    begin
      if rising_edge(clk) then
        data_out_c <= regf_s(15); -- tag of ARM pc reg
      end if;
    end process; 

end architecture; -- behavioral
