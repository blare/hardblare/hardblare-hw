library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.byte_pkg.all;
use work.dependencies_pack.all;

entity cache_top is
generic(n: integer := 32;
	m: integer := 26;
        TLB_ENTRIES: integer := 64;
	OFFSET: integer := 12);
Port(clock, reset:in std_logic;
    address_in:in std_logic_vector(31 downto 0);
    data_out:out std_logic_vector(n-1 downto 0);
    enable_type: in tag_mem_source_type;
--    r_enable:in std_logic;
    r_valid_out:out std_logic;

    data_in:in std_logic_vector(n-1 downto 0); 
--    w_enable:in std_logic;
--    TLB_update_enable:in std_logic;
--	address_ram_store_out : out std_logic_vector(31 downto 0);
--	mdata_out_store_out: out std_logic_vector(31 downto 0);
--	state_out: out std_logic;

--	write_enable_out : out std_logic;
--	write_data_out : out std_logic_vector(31 downto 0);
--	address_from_MMU_out : out std_logic_vector(25 downto 0);

	axim_done	: in std_logic;
	axim_error_in: in std_logic;
	axim_address : out std_logic_vector(31 downto 0);
	axim_data_out : out std_logic_vector(31 downto 0);
	axim_data_in : in std_logic_vector(31 downto 0);
	axim_read_en : out std_logic;
	axim_write_en : out std_logic;

	axim_start	: out std_logic);        
       
  --  memory_out: out BRAM_out_interface;
  --  memory_in: in BRAM_in_interface
  --  mdata_valid:in std_logic);
end cache_top;

architecture Behavioral of cache_top is

component MMU is
generic ( n: integer := 32;
	  m: integer := 26;
	  TLB_ENTRIES: integer := 64;
	  OFFSET: integer := 12);
Port(clock, reset:in std_logic;
    address_in:in std_logic_vector(31 downto 0);
    address_out:out std_logic_vector(m-1 downto 0);
    address_valid:out STD_LOGIC := '0';
    ram_address:out std_logic_vector(m-1 downto 0);
    r_enable_from_MMU:out STD_LOGIC := '0';
    ram_data:in std_logic_vector(n-1 downto 0); 
    ram_data_valid:in STD_LOGIC := '0';
    r:in std_logic;         --TLB read request from processor
--    TLB_frame_number:in std_logic_vector(m-OFFSET-1 downto 0); -- Frame number update from processor
    TLB_update_enable:in std_logic);
end component;

component cache is
generic(n: integer := 32);
Port(clock, reset:in std_logic;

    address_in:in std_logic_vector(25 downto 0); --26 bit physical address from MMU
    data_out:out std_logic_vector(n-1 downto 0); -- Data to processor
    r_enable:in std_logic; -- Processor wants to read
    r_enable_out:out std_logic:= '0'; -- Read enable to memory
    r_valid_out:out std_logic:= '0';
    mdata_valid: in std_logic;

    address_out:out std_logic_vector(25 downto 0); -- 26 bit address to memory
    mdata_in:in std_logic_vector(n-1 downto 0);  -- Data from memory
    mdata_out:out std_logic_vector(n-1 downto 0); -- Data to memory when processor wants to write
    w_enable_out:out std_logic:= '0'; -- Write enable to memory

--    write_enable_out : out std_logic;

    data_in:in std_logic_vector(n-1 downto 0); -- Data from processor for write through cache
    w_enable:in std_logic); --Processor wants to write to memory
    
end component;

--component ram is
--    generic(n: integer := 32);
--    port(clock, reset, w_enable, r_enable:in STD_LOGIC;
--         -- Address size is equal to index + tag
--         address:in STD_LOGIC_VECTOR(31 downto 0);
--         data_in:in STD_LOGIC_VECTOR(n-1 downto 0);
--         data_out:out STD_LOGIC_VECTOR(n-1 downto 0);
--         data_valid:out STD_LOGIC := '0'
--     );
--end component;

signal 	      axim_error_in_sig            : std_logic; 

signal r_enable_ram, w_enable_out, MMU_r_enable, r_enable_from_MMU, r_enable_out, address_valid, w_enable_cache, r_enable, w_enable, TLB_update_enable, w_enable_out_store, r_enable_ram_store : std_logic;
signal r_enable_cache : std_logic := '0';
signal address_ram, address_ram_store : std_logic_vector(31 downto 0);
signal MMU_address_out, ram_address, address_out: std_logic_vector(25 downto 0);
signal mdata_in, mdata_out, mdata_out_store: std_logic_vector(n-1 downto 0);
signal generated_frame_number: integer range 0 to TLB_ENTRIES-1 := 0;
--signal TLB_frame_number: std_logic_vector(m-OFFSET-1 downto 0);

TYPE STATE_TYPE IS (INITIAL, CACHE_READ_ENABLE, CACHE_WRITE_ENABLE);
SIGNAL state   : STATE_TYPE := INITIAL;

TYPE AXI_STATE_TYPE IS (INITIAL, CONTINUE_AXI);
SIGNAL state_axi   : AXI_STATE_TYPE := INITIAL;
signal axi_counter: integer range 0 to 7 := 0;

signal w_data: std_logic_vector(31 downto 0);

begin

--write_data_out <= w_data;
--address_from_MMU_out <= MMU_address_out;

--address_ram_store_out <= address_ram_store;
--mdata_out_store_out <= mdata_out_store;
--state_out <= '1' when state_axi = CONTINUE_AXI else
--	     '0';

--data_valid <= mdata_valid;
--address_to_memory <= address_out;
MMU_r_enable <= r_enable or w_enable;
r_enable_ram <= r_enable_from_MMU or r_enable_out;

address_ram <= "000111" & ram_address when r_enable_from_MMU = '1' else
		"000111" & address_out when (r_enable_out = '1' or w_enable_out = '1') else
		(others => '0');

--memory_out.w_en <= "1111" when w_enable_out = '1' else
--		   "0000";

mdata_in <= axim_data_in;

axim_error_in_sig <= axim_error_in;

--mdata_valid <= memory_in.valid;

r_enable <= '1' when enable_type = tag_mem_read32 else
	    '0';
w_enable <= '1' when enable_type = tag_mem_write32 else
	    '0';
TLB_update_enable <= '1' when enable_type = tag_mem_tlb_write else
	    	     '0';

--r_enable_cache <= r_enable and address_valid;
--w_enable_cache <= w_enable and address_valid;

mmu1:MMU generic map (n, m, TLB_ENTRIES, OFFSET) port map (clock, reset, address_in, MMU_address_out, address_valid, ram_address, r_enable_from_MMU, mdata_in, axim_done, MMU_r_enable, TLB_update_enable);
cache1:cache generic map (n) port map (clock, reset, MMU_address_out, data_out, r_enable_cache, r_enable_out, r_valid_out, axim_done, address_out, mdata_in, mdata_out, w_enable_out, w_data, w_enable_cache);
--ram1:ram generic map(n) port map(clock, reset, w_enable_out, r_enable_ram, address_ram, mdata_out, mdata_in, mdata_valid);

process(clock)
begin
	if (rising_edge(clock)) and (reset = '0') then
--		r_enable_cache <= address_valid;
		CASE state IS
            	WHEN INITIAL=>
			w_enable_cache <= '0';
			if enable_type = tag_mem_write32 then  
				w_data <= data_in;  --changed
			end if;
			if address_valid = '1' and r_enable = '1' then
				state <= CACHE_READ_ENABLE;
				r_enable_cache <= '1';
			end if;
			if w_enable = '1' then
				state <= CACHE_WRITE_ENABLE;
			end if;
		WHEN CACHE_READ_ENABLE=>
			if r_enable = '1' then
				r_enable_cache <= '1';
			else
				state <= INITIAL;
				r_enable_cache <= '0';
			end if;
		WHEN CACHE_WRITE_ENABLE=>
			if address_valid = '1' then
				w_enable_cache <= '1';
				state <= INITIAL;
			end if;
		when others =>
                		state <= INITIAL;
        	END CASE;
	end if;
end process;

process(clock)
begin
	if (rising_edge(clock)) and (reset = '0') then
		case state_axi is
		when INITIAL =>
			
			axim_address <= (others => '0');
			axim_data_out <= (others => '0');
	
			axim_read_en <= '0';
			axim_write_en <= '0';

			axim_start	<= '0';

			if w_enable_out = '1' then
				w_enable_out_store <= '1';
			else
				w_enable_out_store <= '0';
			end if;

			if r_enable_ram = '1' then
				r_enable_ram_store <= '1';
			else
				r_enable_ram_store <= '0';
			end if;

			if w_enable_out = '1' or r_enable_ram = '1' then
				address_ram_store <= address_ram;
				mdata_out_store <= mdata_out;
				state_axi <= CONTINUE_AXI;
			else
				address_ram_store <= (others => '0');
				mdata_out_store <= (others => '0');
				state_axi <= INITIAL;
			end if;

		when CONTINUE_AXI =>
			axi_counter <= axi_counter + 1;
			if axi_counter < 1 then
				state_axi <= CONTINUE_AXI;
			else
				state_axi <= INITIAL;
			end if;
					
			if r_enable_ram_store = '1' then
				axim_read_en <= '1';				
			else
				axim_read_en <= '0';				
			end if;
		
			if w_enable_out_store = '1' then
				axim_write_en <= '1';
			else
				axim_write_en <= '0';
			end if;
		
			axim_start <= '1';
			axim_address <= address_ram_store;
			axim_data_out <= mdata_out_store;			
		end case;			
	end if;
end process;

end Behavioral;


