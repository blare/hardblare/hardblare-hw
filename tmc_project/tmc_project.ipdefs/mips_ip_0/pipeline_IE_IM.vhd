----------
--
-- AUTHOR               : M.Abdul WAHAB
-- DATE                 : 2017
-- Description          : Pipeline registers for stage IE/IM
----------
library IEEE; 
use ieee.std_logic_1164.All;
use work.mips_pack.all;

entity pipeline_IE_IM is
	port(
		clk, reset 	 	: in std_logic ;
		stall		 	: in std_logic ;
		hazard          : in std_logic;
		forward_d    	: in std_logic_vector(1 downto 0);	
		c_alu_i      	: in std_logic_vector(31 downto 0);
		c_shift_i    	: in std_logic_vector(31 downto 0);
		c_mult_i     	: in std_logic_vector(31 downto 0);
		c_source		: in c_source_type;
		pc_source       : in pc_source_type;
		c_pc            : in  std_logic_vector(31 downto 0);
		imm_in          : in std_logic_vector(15 downto 0);        
		rt_index		: in std_logic_vector(5 downto 0);
		rd_index		: in std_logic_vector(5 downto 0);
		reg_target      : in  std_logic_vector(31 downto 0);       
		mem_source_out_i: in mem_source_type;


        ldr_instr       : in  std_logic; 
        ldr_instr_out   : out std_logic;

		c_alu      	 	: out std_logic_vector(31 downto 0);
		c_shift		 	: out std_logic_vector(31 downto 0);
		c_mult       	: out std_logic_vector(31 downto 0);
		c_source_out	: out c_source_type;
		pc_source_out   : out pc_source_type;
		c_pc_out        : out std_logic_vector(31 downto 0);
		imm_out         : out std_logic_vector(15 downto 0);
		rt_index_out	: out std_logic_vector(5 downto 0);
		rd_index_out	: out std_logic_vector(5 downto 0);
		reg_target_out  : out std_logic_vector(31 downto 0);
		forward_d_out   : out std_logic_vector(1 downto 0);
		mem_source_out  : out mem_source_type );
end entity;

architecture behavioral of pipeline_IE_IM is 

signal c_alu_reg 			: std_logic_vector(31 downto 0);
signal c_shift_reg 			: std_logic_vector(31 downto 0);
signal c_mult_reg 			: std_logic_vector(31 downto 0);
signal c_source_out_reg 	: c_source_type;
signal pc_source_out_reg 	: pc_source_type;
signal mem_source_out_reg 	: mem_source_type;
signal c_pc_out_reg 		: std_logic_vector(31 downto 0);
signal imm_out_reg 			: std_logic_vector(15 downto 0);
signal rt_index_out_reg 	: std_logic_vector(5 downto 0);
signal rd_index_out_reg 	: std_logic_vector(5 downto 0);
signal reg_target_reg		: std_logic_vector(31 downto 0);
signal forward_d_reg        : std_logic_vector(1 downto 0);
signal stall_r              : std_logic;
signal ldr_instr_reg        : std_logic;

begin 

process(clk)
begin
        if rising_edge(clk) then 
                if (reset = '1' or stall = '1') then
                	c_alu_reg 				<= (others => '0');
					c_shift_reg 			<= (others => '0');
					c_mult_reg 				<= (others => '0');
					c_source_out_reg 		<= (others => '0');
					pc_source_out_reg 		<= (others => '0');
					mem_source_out_reg 		<= (others => '0');
					c_pc_out_reg 			<= (others => '0');
					imm_out_reg 			<= (others => '0');
					rt_index_out_reg 		<= (others => '0');
					rd_index_out_reg 		<= (others => '0');
					reg_target_reg 			<= (others => '0');
					forward_d_reg           <= "00";
					ldr_instr_reg       	<= '0';
                elsif stall  = '0' then        
                    c_alu_reg 			<= c_alu_i;
					c_shift_reg 		<= c_shift_i;
					c_mult_reg 			<= c_mult_i;
					c_source_out_reg 	<= c_source;
					pc_source_out_reg 	<= pc_source;
					mem_source_out_reg 	<= mem_source_out_i;
					c_pc_out_reg 		<= c_pc;
					imm_out_reg 		<= imm_in;
					rt_index_out_reg 	<= rt_index;
					rd_index_out_reg 	<= rd_index;
					reg_target_reg		<= reg_target;
					forward_d_reg       <= forward_d;
                    ldr_instr_reg       <= ldr_instr;
                end if;
        end if;
end process;

c_alu 			<=    c_alu_reg;
c_shift 		<=    c_shift_reg;
c_mult 			<=    c_mult_reg;
c_source_out 	<=    c_source_out_reg;
pc_source_out 	<=    pc_source_out_reg;
mem_source_out  <=    mem_source_out_reg;
c_pc_out 		<=    c_pc_out_reg;
imm_out			<=    imm_out_reg;
rt_index_out	<=    rt_index_out_reg;
rd_index_out	<=    rd_index_out_reg;
reg_target_out  <= 	  reg_target_reg;
ldr_instr_out   <=    ldr_instr_reg;
forward_d_out   <=    forward_d_reg;
	
	--process (forward_d, c_alu_reg, reg_target_reg)
	--begin
	--	if forward_d = '1' then 
	--		reg_target_out <= c_alu_reg;
	--	else
	--		reg_target_out  <= reg_target_reg;
	--	end if;	
	--end process;

end architecture;