// Copyright 1986-2014 Xilinx, Inc. All Rights Reserved.

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
module tmc(clock, reset_in, axim_done, axim_error_in, axim_address, axim_data_out, axim_data_in, axim_read_en, axim_write_en, axim_start, enable_type_out, data_from_TMC_out, virtual_mem_address_out, t_opcode, pause_out, a_bus_p_out, b_bus_p_out, c_alu_p_out, c_alu_p_p_out, a_source_out, enable_reg_file_s_out, reg_source_1_gp_out, reset_out1, i_mem_sel, i_mem_en, i_mem_address, i_mem_data_w, i_mem_data_r);
  input clock;
  input reset_in;
  input axim_done;
  input axim_error_in;
  output [31:0]axim_address;
  output [31:0]axim_data_out;
  input [31:0]axim_data_in;
  output axim_read_en;
  output axim_write_en;
  output axim_start;
  output [1:0]enable_type_out;
  output [31:0]data_from_TMC_out;
  output [31:0]virtual_mem_address_out;
  output [31:0]t_opcode;
  output pause_out;
  output [31:0]a_bus_p_out;
  output [31:0]b_bus_p_out;
  output [31:0]c_alu_p_out;
  output [31:0]c_alu_p_p_out;
  output [1:0]a_source_out;
  output enable_reg_file_s_out;
  output [31:0]reg_source_1_gp_out;
  output reset_out1;
  output [3:0]i_mem_sel;
  output i_mem_en;
  output [31:0]i_mem_address;
  output [31:0]i_mem_data_w;
  input [31:0]i_mem_data_r;
endmodule
