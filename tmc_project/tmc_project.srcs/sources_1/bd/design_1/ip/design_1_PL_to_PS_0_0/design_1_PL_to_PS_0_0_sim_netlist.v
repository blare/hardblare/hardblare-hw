// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
// Date        : Fri Jun  4 10:31:58 2021
// Host        : HardBlare-Workstation running 64-bit Ubuntu 16.04.6 LTS
// Command     : write_verilog -force -mode funcsim
//               /home/mounir/HardBlare/hardblare-hw/tmc_project/tmc_project.srcs/sources_1/bd/design_1/ip/design_1_PL_to_PS_0_0/design_1_PL_to_PS_0_0_sim_netlist.v
// Design      : design_1_PL_to_PS_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_PL_to_PS_0_0,PL_to_PS,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* ip_definition_source = "package_project" *) 
(* x_core_info = "PL_to_PS,Vivado 2018.2" *) 
(* NotValidForBitStream *)
module design_1_PL_to_PS_0_0
   (from_Dispatcher_address,
    from_Dispatcher_clk,
    from_Dispatcher_data_in,
    from_Dispatcher_enable,
    from_Dispatcher_reset,
    from_Dispatcher_bytes_selection,
    to_Dispatcher_data_out,
    fifo_ptm_empty,
    fifo_ptm_almost_empty,
    fifo_ptm_full,
    fifo_ptm_almost_full,
    fifo_monitor_to_kernel_empty,
    fifo_monitor_to_kernel_almost_empty,
    fifo_monitor_to_kernel_full,
    fifo_monitor_to_kernel_almost_full,
    fifo_kernel_to_monitor_empty,
    fifo_kernel_to_monitor_almost_empty,
    fifo_kernel_to_monitor_full,
    fifo_kernel_to_monitor_almost_full,
    fifo_instrumentation_empty,
    fifo_instrumentation_almost_empty,
    fifo_instrumentation_full,
    fifo_instrumentation_almost_full,
    dispatcher_generate_interrupt,
    s00_axi_aclk,
    s00_axi_aresetn,
    s00_axi_awid,
    s00_axi_awaddr,
    s00_axi_awlen,
    s00_axi_awsize,
    s00_axi_awburst,
    s00_axi_awlock,
    s00_axi_awcache,
    s00_axi_awprot,
    s00_axi_awqos,
    s00_axi_awregion,
    s00_axi_awuser,
    s00_axi_awvalid,
    s00_axi_awready,
    s00_axi_wdata,
    s00_axi_wstrb,
    s00_axi_wlast,
    s00_axi_wuser,
    s00_axi_wvalid,
    s00_axi_wready,
    s00_axi_bid,
    s00_axi_bresp,
    s00_axi_buser,
    s00_axi_bvalid,
    s00_axi_bready,
    s00_axi_arid,
    s00_axi_araddr,
    s00_axi_arlen,
    s00_axi_arsize,
    s00_axi_arburst,
    s00_axi_arlock,
    s00_axi_arcache,
    s00_axi_arprot,
    s00_axi_arqos,
    s00_axi_arregion,
    s00_axi_aruser,
    s00_axi_arvalid,
    s00_axi_arready,
    s00_axi_rid,
    s00_axi_rdata,
    s00_axi_rresp,
    s00_axi_rlast,
    s00_axi_ruser,
    s00_axi_rvalid,
    s00_axi_rready,
    s_axi_intr_aclk,
    s_axi_intr_aresetn,
    s_axi_intr_awaddr,
    s_axi_intr_awprot,
    s_axi_intr_awvalid,
    s_axi_intr_awready,
    s_axi_intr_wdata,
    s_axi_intr_wstrb,
    s_axi_intr_wvalid,
    s_axi_intr_wready,
    s_axi_intr_bresp,
    s_axi_intr_bvalid,
    s_axi_intr_bready,
    s_axi_intr_araddr,
    s_axi_intr_arprot,
    s_axi_intr_arvalid,
    s_axi_intr_arready,
    s_axi_intr_rdata,
    s_axi_intr_rresp,
    s_axi_intr_rvalid,
    s_axi_intr_rready,
    irq);
  input [31:0]from_Dispatcher_address;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 from_Dispatcher_clk CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME from_Dispatcher_clk, ASSOCIATED_RESET from_Dispatcher_reset, FREQ_HZ 100000000, PHASE 0.000" *) input from_Dispatcher_clk;
  input [31:0]from_Dispatcher_data_in;
  input from_Dispatcher_enable;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 from_Dispatcher_reset RST" *) (* x_interface_parameter = "XIL_INTERFACENAME from_Dispatcher_reset, POLARITY ACTIVE_LOW" *) input from_Dispatcher_reset;
  input [3:0]from_Dispatcher_bytes_selection;
  output [31:0]to_Dispatcher_data_out;
  input fifo_ptm_empty;
  input fifo_ptm_almost_empty;
  input fifo_ptm_full;
  input fifo_ptm_almost_full;
  input fifo_monitor_to_kernel_empty;
  input fifo_monitor_to_kernel_almost_empty;
  input fifo_monitor_to_kernel_full;
  input fifo_monitor_to_kernel_almost_full;
  input fifo_kernel_to_monitor_empty;
  input fifo_kernel_to_monitor_almost_empty;
  input fifo_kernel_to_monitor_full;
  input fifo_kernel_to_monitor_almost_full;
  input fifo_instrumentation_empty;
  input fifo_instrumentation_almost_empty;
  input fifo_instrumentation_full;
  input fifo_instrumentation_almost_full;
  (* x_interface_info = "xilinx.com:signal:interrupt:1.0 dispatcher_generate_interrupt INTERRUPT" *) (* x_interface_parameter = "XIL_INTERFACENAME dispatcher_generate_interrupt, SENSITIVITY LEVEL_HIGH, PortWidth 1" *) input dispatcher_generate_interrupt;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 s00_axi_aclk CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME s00_axi_aclk, ASSOCIATED_BUSIF s00_axi, ASSOCIATED_RESET s00_axi_aresetn, FREQ_HZ 102564102, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0" *) input s00_axi_aclk;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 s00_axi_aresetn RST" *) (* x_interface_parameter = "XIL_INTERFACENAME s00_axi_aresetn, POLARITY ACTIVE_LOW" *) input s00_axi_aresetn;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi AWID" *) (* x_interface_parameter = "XIL_INTERFACENAME s00_axi, DATA_WIDTH 32, PROTOCOL AXI4, FREQ_HZ 102564102, ID_WIDTH 12, ADDR_WIDTH 32, AWUSER_WIDTH 1, ARUSER_WIDTH 1, WUSER_WIDTH 1, RUSER_WIDTH 1, BUSER_WIDTH 1, READ_WRITE_MODE READ_WRITE, HAS_BURST 1, HAS_LOCK 1, HAS_PROT 1, HAS_CACHE 1, HAS_QOS 1, HAS_REGION 1, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 1, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 256, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0" *) input [11:0]s00_axi_awid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi AWADDR" *) input [31:0]s00_axi_awaddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi AWLEN" *) input [7:0]s00_axi_awlen;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi AWSIZE" *) input [2:0]s00_axi_awsize;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi AWBURST" *) input [1:0]s00_axi_awburst;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi AWLOCK" *) input s00_axi_awlock;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi AWCACHE" *) input [3:0]s00_axi_awcache;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi AWPROT" *) input [2:0]s00_axi_awprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi AWQOS" *) input [3:0]s00_axi_awqos;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi AWREGION" *) input [3:0]s00_axi_awregion;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi AWUSER" *) input [0:0]s00_axi_awuser;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi AWVALID" *) input s00_axi_awvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi AWREADY" *) output s00_axi_awready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi WDATA" *) input [31:0]s00_axi_wdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi WSTRB" *) input [3:0]s00_axi_wstrb;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi WLAST" *) input s00_axi_wlast;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi WUSER" *) input [0:0]s00_axi_wuser;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi WVALID" *) input s00_axi_wvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi WREADY" *) output s00_axi_wready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi BID" *) output [11:0]s00_axi_bid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi BRESP" *) output [1:0]s00_axi_bresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi BUSER" *) output [0:0]s00_axi_buser;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi BVALID" *) output s00_axi_bvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi BREADY" *) input s00_axi_bready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi ARID" *) input [11:0]s00_axi_arid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi ARADDR" *) input [31:0]s00_axi_araddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi ARLEN" *) input [7:0]s00_axi_arlen;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi ARSIZE" *) input [2:0]s00_axi_arsize;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi ARBURST" *) input [1:0]s00_axi_arburst;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi ARLOCK" *) input s00_axi_arlock;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi ARCACHE" *) input [3:0]s00_axi_arcache;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi ARPROT" *) input [2:0]s00_axi_arprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi ARQOS" *) input [3:0]s00_axi_arqos;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi ARREGION" *) input [3:0]s00_axi_arregion;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi ARUSER" *) input [0:0]s00_axi_aruser;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi ARVALID" *) input s00_axi_arvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi ARREADY" *) output s00_axi_arready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi RID" *) output [11:0]s00_axi_rid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi RDATA" *) output [31:0]s00_axi_rdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi RRESP" *) output [1:0]s00_axi_rresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi RLAST" *) output s00_axi_rlast;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi RUSER" *) output [0:0]s00_axi_ruser;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi RVALID" *) output s00_axi_rvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s00_axi RREADY" *) input s00_axi_rready;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 s_axi_intr_aclk CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME s_axi_intr_aclk, ASSOCIATED_BUSIF s_axi_intr, ASSOCIATED_RESET s_axi_intr_aresetn, FREQ_HZ 102564102, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0" *) input s_axi_intr_aclk;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 s_axi_intr_aresetn RST" *) (* x_interface_parameter = "XIL_INTERFACENAME s_axi_intr_aresetn, POLARITY ACTIVE_LOW" *) input s_axi_intr_aresetn;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_intr AWADDR" *) (* x_interface_parameter = "XIL_INTERFACENAME s_axi_intr, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 102564102, ID_WIDTH 0, ADDR_WIDTH 32, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 0, NUM_READ_OUTSTANDING 1, NUM_WRITE_OUTSTANDING 1, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0" *) input [31:0]s_axi_intr_awaddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_intr AWPROT" *) input [2:0]s_axi_intr_awprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_intr AWVALID" *) input s_axi_intr_awvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_intr AWREADY" *) output s_axi_intr_awready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_intr WDATA" *) input [31:0]s_axi_intr_wdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_intr WSTRB" *) input [3:0]s_axi_intr_wstrb;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_intr WVALID" *) input s_axi_intr_wvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_intr WREADY" *) output s_axi_intr_wready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_intr BRESP" *) output [1:0]s_axi_intr_bresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_intr BVALID" *) output s_axi_intr_bvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_intr BREADY" *) input s_axi_intr_bready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_intr ARADDR" *) input [31:0]s_axi_intr_araddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_intr ARPROT" *) input [2:0]s_axi_intr_arprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_intr ARVALID" *) input s_axi_intr_arvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_intr ARREADY" *) output s_axi_intr_arready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_intr RDATA" *) output [31:0]s_axi_intr_rdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_intr RRESP" *) output [1:0]s_axi_intr_rresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_intr RVALID" *) output s_axi_intr_rvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 s_axi_intr RREADY" *) input s_axi_intr_rready;
  (* x_interface_info = "xilinx.com:signal:interrupt:1.0 irq INTERRUPT" *) (* x_interface_parameter = "XIL_INTERFACENAME irq, SENSITIVITY LEVEL_HIGH, PortWidth 1" *) output irq;

  wire \<const0> ;
  wire \PL_to_PS_S00_AXI_inst/p_0_in ;
  wire U0_n_0;
  wire U0_n_1;
  wire U0_n_10;
  wire U0_n_11;
  wire U0_n_12;
  wire U0_n_13;
  wire U0_n_14;
  wire U0_n_15;
  wire U0_n_16;
  wire U0_n_17;
  wire U0_n_18;
  wire U0_n_19;
  wire U0_n_2;
  wire U0_n_20;
  wire U0_n_21;
  wire U0_n_22;
  wire U0_n_23;
  wire U0_n_24;
  wire U0_n_25;
  wire U0_n_26;
  wire U0_n_27;
  wire U0_n_28;
  wire U0_n_29;
  wire U0_n_3;
  wire U0_n_30;
  wire U0_n_31;
  wire U0_n_32;
  wire U0_n_4;
  wire U0_n_5;
  wire U0_n_6;
  wire U0_n_7;
  wire U0_n_8;
  wire U0_n_9;
  wire dispatcher_generate_interrupt;
  wire fifo_instrumentation_almost_empty;
  wire fifo_instrumentation_almost_full;
  wire fifo_instrumentation_empty;
  wire fifo_instrumentation_full;
  wire fifo_kernel_to_monitor_almost_empty;
  wire fifo_kernel_to_monitor_almost_full;
  wire fifo_kernel_to_monitor_empty;
  wire fifo_kernel_to_monitor_full;
  wire fifo_monitor_to_kernel_almost_empty;
  wire fifo_monitor_to_kernel_almost_full;
  wire fifo_monitor_to_kernel_empty;
  wire fifo_monitor_to_kernel_full;
  wire fifo_ptm_almost_empty;
  wire fifo_ptm_almost_full;
  wire fifo_ptm_empty;
  wire fifo_ptm_full;
  wire [31:0]from_Dispatcher_address;
  wire [3:0]from_Dispatcher_bytes_selection;
  wire from_Dispatcher_clk;
  wire [31:0]from_Dispatcher_data_in;
  wire from_Dispatcher_enable;
  wire from_Dispatcher_reset;
  wire irq;
  wire s00_axi_aclk;
  wire [31:0]s00_axi_araddr;
  wire [1:0]s00_axi_arburst;
  wire s00_axi_aresetn;
  wire [11:0]s00_axi_arid;
  wire [7:0]s00_axi_arlen;
  wire s00_axi_arready;
  wire s00_axi_arvalid;
  wire [31:0]s00_axi_awaddr;
  wire [1:0]s00_axi_awburst;
  wire [11:0]s00_axi_awid;
  wire [7:0]s00_axi_awlen;
  wire s00_axi_awready;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rlast;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire s00_axi_wlast;
  wire s00_axi_wready;
  wire s00_axi_wvalid;
  wire s_axi_intr_aclk;
  wire [31:0]s_axi_intr_araddr;
  wire s_axi_intr_aresetn;
  wire s_axi_intr_arready;
  wire s_axi_intr_arvalid;
  wire [31:0]s_axi_intr_awaddr;
  wire s_axi_intr_awready;
  wire s_axi_intr_awvalid;
  wire s_axi_intr_bready;
  wire s_axi_intr_bvalid;
  wire [5:0]\^s_axi_intr_rdata ;
  wire s_axi_intr_rready;
  wire s_axi_intr_rvalid;
  wire [31:0]s_axi_intr_wdata;
  wire s_axi_intr_wready;
  wire s_axi_intr_wvalid;
  wire [31:0]to_Dispatcher_data_out;
  wire \to_Dispatcher_data_out[0]_INST_0_i_1_n_0 ;
  wire \to_Dispatcher_data_out[10]_INST_0_i_1_n_0 ;
  wire \to_Dispatcher_data_out[11]_INST_0_i_1_n_0 ;
  wire \to_Dispatcher_data_out[12]_INST_0_i_1_n_0 ;
  wire \to_Dispatcher_data_out[13]_INST_0_i_1_n_0 ;
  wire \to_Dispatcher_data_out[14]_INST_0_i_1_n_0 ;
  wire \to_Dispatcher_data_out[15]_INST_0_i_1_n_0 ;
  wire \to_Dispatcher_data_out[16]_INST_0_i_1_n_0 ;
  wire \to_Dispatcher_data_out[17]_INST_0_i_1_n_0 ;
  wire \to_Dispatcher_data_out[18]_INST_0_i_1_n_0 ;
  wire \to_Dispatcher_data_out[19]_INST_0_i_1_n_0 ;
  wire \to_Dispatcher_data_out[1]_INST_0_i_1_n_0 ;
  wire \to_Dispatcher_data_out[20]_INST_0_i_1_n_0 ;
  wire \to_Dispatcher_data_out[21]_INST_0_i_1_n_0 ;
  wire \to_Dispatcher_data_out[22]_INST_0_i_1_n_0 ;
  wire \to_Dispatcher_data_out[23]_INST_0_i_1_n_0 ;
  wire \to_Dispatcher_data_out[24]_INST_0_i_1_n_0 ;
  wire \to_Dispatcher_data_out[25]_INST_0_i_1_n_0 ;
  wire \to_Dispatcher_data_out[26]_INST_0_i_1_n_0 ;
  wire \to_Dispatcher_data_out[27]_INST_0_i_1_n_0 ;
  wire \to_Dispatcher_data_out[28]_INST_0_i_1_n_0 ;
  wire \to_Dispatcher_data_out[29]_INST_0_i_1_n_0 ;
  wire \to_Dispatcher_data_out[2]_INST_0_i_1_n_0 ;
  wire \to_Dispatcher_data_out[30]_INST_0_i_1_n_0 ;
  wire \to_Dispatcher_data_out[31]_INST_0_i_1_n_0 ;
  wire \to_Dispatcher_data_out[31]_INST_0_i_2_n_0 ;
  wire \to_Dispatcher_data_out[3]_INST_0_i_1_n_0 ;
  wire \to_Dispatcher_data_out[4]_INST_0_i_1_n_0 ;
  wire \to_Dispatcher_data_out[5]_INST_0_i_1_n_0 ;
  wire \to_Dispatcher_data_out[6]_INST_0_i_1_n_0 ;
  wire \to_Dispatcher_data_out[7]_INST_0_i_1_n_0 ;
  wire \to_Dispatcher_data_out[8]_INST_0_i_1_n_0 ;
  wire \to_Dispatcher_data_out[9]_INST_0_i_1_n_0 ;

  assign s00_axi_bid[11:0] = s00_axi_awid;
  assign s00_axi_bresp[1] = \<const0> ;
  assign s00_axi_bresp[0] = \<const0> ;
  assign s00_axi_buser[0] = \<const0> ;
  assign s00_axi_rid[11:0] = s00_axi_arid;
  assign s00_axi_rresp[1] = \<const0> ;
  assign s00_axi_rresp[0] = \<const0> ;
  assign s00_axi_ruser[0] = \<const0> ;
  assign s_axi_intr_bresp[1] = \<const0> ;
  assign s_axi_intr_bresp[0] = \<const0> ;
  assign s_axi_intr_rdata[31] = \<const0> ;
  assign s_axi_intr_rdata[30] = \<const0> ;
  assign s_axi_intr_rdata[29] = \<const0> ;
  assign s_axi_intr_rdata[28] = \<const0> ;
  assign s_axi_intr_rdata[27] = \<const0> ;
  assign s_axi_intr_rdata[26] = \<const0> ;
  assign s_axi_intr_rdata[25] = \<const0> ;
  assign s_axi_intr_rdata[24] = \<const0> ;
  assign s_axi_intr_rdata[23] = \<const0> ;
  assign s_axi_intr_rdata[22] = \<const0> ;
  assign s_axi_intr_rdata[21] = \<const0> ;
  assign s_axi_intr_rdata[20] = \<const0> ;
  assign s_axi_intr_rdata[19] = \<const0> ;
  assign s_axi_intr_rdata[18] = \<const0> ;
  assign s_axi_intr_rdata[17] = \<const0> ;
  assign s_axi_intr_rdata[16] = \<const0> ;
  assign s_axi_intr_rdata[15] = \<const0> ;
  assign s_axi_intr_rdata[14] = \<const0> ;
  assign s_axi_intr_rdata[13] = \<const0> ;
  assign s_axi_intr_rdata[12] = \<const0> ;
  assign s_axi_intr_rdata[11] = \<const0> ;
  assign s_axi_intr_rdata[10] = \<const0> ;
  assign s_axi_intr_rdata[9] = \<const0> ;
  assign s_axi_intr_rdata[8] = \<const0> ;
  assign s_axi_intr_rdata[7] = \<const0> ;
  assign s_axi_intr_rdata[6] = \<const0> ;
  assign s_axi_intr_rdata[5:0] = \^s_axi_intr_rdata [5:0];
  assign s_axi_intr_rresp[1] = \<const0> ;
  assign s_axi_intr_rresp[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  design_1_PL_to_PS_0_0_PL_to_PS U0
       (.DOADO({U0_n_0,U0_n_1,U0_n_2,U0_n_3,U0_n_4,U0_n_5,U0_n_6,U0_n_7}),
        .WEA(U0_n_8),
        .dispatcher_generate_interrupt(dispatcher_generate_interrupt),
        .fifo_instrumentation_almost_empty(fifo_instrumentation_almost_empty),
        .fifo_instrumentation_almost_full(fifo_instrumentation_almost_full),
        .fifo_instrumentation_empty(fifo_instrumentation_empty),
        .fifo_instrumentation_full(fifo_instrumentation_full),
        .fifo_kernel_to_monitor_almost_empty(fifo_kernel_to_monitor_almost_empty),
        .fifo_kernel_to_monitor_almost_full(fifo_kernel_to_monitor_almost_full),
        .fifo_kernel_to_monitor_empty(fifo_kernel_to_monitor_empty),
        .fifo_kernel_to_monitor_full(fifo_kernel_to_monitor_full),
        .fifo_monitor_to_kernel_almost_empty(fifo_monitor_to_kernel_almost_empty),
        .fifo_monitor_to_kernel_almost_full(fifo_monitor_to_kernel_almost_full),
        .fifo_monitor_to_kernel_empty(fifo_monitor_to_kernel_empty),
        .fifo_monitor_to_kernel_full(fifo_monitor_to_kernel_full),
        .fifo_ptm_almost_empty(fifo_ptm_almost_empty),
        .fifo_ptm_almost_full(fifo_ptm_almost_full),
        .fifo_ptm_empty(fifo_ptm_empty),
        .fifo_ptm_full(fifo_ptm_full),
        .from_Dispatcher_address(from_Dispatcher_address[8:2]),
        .from_Dispatcher_bytes_selection(from_Dispatcher_bytes_selection),
        .from_Dispatcher_clk(from_Dispatcher_clk),
        .from_Dispatcher_data_in(from_Dispatcher_data_in),
        .from_Dispatcher_enable(from_Dispatcher_enable),
        .from_Dispatcher_reset(from_Dispatcher_reset),
        .irq(irq),
        .p_0_in(\PL_to_PS_S00_AXI_inst/p_0_in ),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr[9:2]),
        .s00_axi_arburst(s00_axi_arburst),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_arlen(s00_axi_arlen),
        .s00_axi_arready(s00_axi_arready),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr[9:2]),
        .s00_axi_awburst(s00_axi_awburst),
        .s00_axi_awlen(s00_axi_awlen),
        .s00_axi_awready(s00_axi_awready),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bready(s00_axi_bready),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rlast(s00_axi_rlast),
        .s00_axi_rready(s00_axi_rready),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wlast(s00_axi_wlast),
        .s00_axi_wready(s00_axi_wready),
        .s00_axi_wvalid(s00_axi_wvalid),
        .s_axi_intr_aclk(s_axi_intr_aclk),
        .s_axi_intr_araddr(s_axi_intr_araddr[6:2]),
        .s_axi_intr_aresetn(s_axi_intr_aresetn),
        .s_axi_intr_arready(s_axi_intr_arready),
        .s_axi_intr_arvalid(s_axi_intr_arvalid),
        .s_axi_intr_awaddr(s_axi_intr_awaddr[4:2]),
        .s_axi_intr_awready(s_axi_intr_awready),
        .s_axi_intr_awvalid(s_axi_intr_awvalid),
        .s_axi_intr_bready(s_axi_intr_bready),
        .s_axi_intr_bvalid(s_axi_intr_bvalid),
        .s_axi_intr_rdata(\^s_axi_intr_rdata ),
        .s_axi_intr_rready(s_axi_intr_rready),
        .s_axi_intr_rvalid(s_axi_intr_rvalid),
        .s_axi_intr_wdata(s_axi_intr_wdata[5:0]),
        .s_axi_intr_wready(s_axi_intr_wready),
        .s_axi_intr_wvalid(s_axi_intr_wvalid),
        .to_Dispatcher_data_out(to_Dispatcher_data_out),
        .\to_Dispatcher_data_out[0]_INST_0_i_1 (\to_Dispatcher_data_out[0]_INST_0_i_1_n_0 ),
        .\to_Dispatcher_data_out[10]_INST_0_i_1 (\to_Dispatcher_data_out[10]_INST_0_i_1_n_0 ),
        .\to_Dispatcher_data_out[11]_INST_0_i_1 (\to_Dispatcher_data_out[11]_INST_0_i_1_n_0 ),
        .\to_Dispatcher_data_out[12]_INST_0_i_1 (\to_Dispatcher_data_out[12]_INST_0_i_1_n_0 ),
        .\to_Dispatcher_data_out[13]_INST_0_i_1 (\to_Dispatcher_data_out[13]_INST_0_i_1_n_0 ),
        .\to_Dispatcher_data_out[14]_INST_0_i_1 (\to_Dispatcher_data_out[14]_INST_0_i_1_n_0 ),
        .\to_Dispatcher_data_out[15]_INST_0_i_1 ({U0_n_9,U0_n_10,U0_n_11,U0_n_12,U0_n_13,U0_n_14,U0_n_15,U0_n_16}),
        .\to_Dispatcher_data_out[15]_INST_0_i_1_0 (\to_Dispatcher_data_out[15]_INST_0_i_1_n_0 ),
        .\to_Dispatcher_data_out[16]_INST_0_i_1 (\to_Dispatcher_data_out[16]_INST_0_i_1_n_0 ),
        .\to_Dispatcher_data_out[17]_INST_0_i_1 (\to_Dispatcher_data_out[17]_INST_0_i_1_n_0 ),
        .\to_Dispatcher_data_out[18]_INST_0_i_1 (\to_Dispatcher_data_out[18]_INST_0_i_1_n_0 ),
        .\to_Dispatcher_data_out[19]_INST_0_i_1 (\to_Dispatcher_data_out[19]_INST_0_i_1_n_0 ),
        .\to_Dispatcher_data_out[1]_INST_0_i_1 (\to_Dispatcher_data_out[1]_INST_0_i_1_n_0 ),
        .\to_Dispatcher_data_out[20]_INST_0_i_1 (\to_Dispatcher_data_out[20]_INST_0_i_1_n_0 ),
        .\to_Dispatcher_data_out[21]_INST_0_i_1 (\to_Dispatcher_data_out[21]_INST_0_i_1_n_0 ),
        .\to_Dispatcher_data_out[22]_INST_0_i_1 (\to_Dispatcher_data_out[22]_INST_0_i_1_n_0 ),
        .\to_Dispatcher_data_out[23]_INST_0_i_1 ({U0_n_17,U0_n_18,U0_n_19,U0_n_20,U0_n_21,U0_n_22,U0_n_23,U0_n_24}),
        .\to_Dispatcher_data_out[23]_INST_0_i_1_0 (\to_Dispatcher_data_out[23]_INST_0_i_1_n_0 ),
        .\to_Dispatcher_data_out[24]_INST_0_i_1 (\to_Dispatcher_data_out[24]_INST_0_i_1_n_0 ),
        .\to_Dispatcher_data_out[25]_INST_0_i_1 (\to_Dispatcher_data_out[25]_INST_0_i_1_n_0 ),
        .\to_Dispatcher_data_out[26]_INST_0_i_1 (\to_Dispatcher_data_out[26]_INST_0_i_1_n_0 ),
        .\to_Dispatcher_data_out[27]_INST_0_i_1 (\to_Dispatcher_data_out[27]_INST_0_i_1_n_0 ),
        .\to_Dispatcher_data_out[28]_INST_0_i_1 (\to_Dispatcher_data_out[28]_INST_0_i_1_n_0 ),
        .\to_Dispatcher_data_out[29]_INST_0_i_1 (\to_Dispatcher_data_out[29]_INST_0_i_1_n_0 ),
        .\to_Dispatcher_data_out[2]_INST_0_i_1 (\to_Dispatcher_data_out[2]_INST_0_i_1_n_0 ),
        .\to_Dispatcher_data_out[30]_INST_0_i_1 (\to_Dispatcher_data_out[30]_INST_0_i_1_n_0 ),
        .\to_Dispatcher_data_out[31]_INST_0_i_1 (\to_Dispatcher_data_out[31]_INST_0_i_1_n_0 ),
        .\to_Dispatcher_data_out[31]_INST_0_i_2 ({U0_n_25,U0_n_26,U0_n_27,U0_n_28,U0_n_29,U0_n_30,U0_n_31,U0_n_32}),
        .\to_Dispatcher_data_out[31]_INST_0_i_2_0 (\to_Dispatcher_data_out[31]_INST_0_i_2_n_0 ),
        .\to_Dispatcher_data_out[3]_INST_0_i_1 (\to_Dispatcher_data_out[3]_INST_0_i_1_n_0 ),
        .\to_Dispatcher_data_out[4]_INST_0_i_1 (\to_Dispatcher_data_out[4]_INST_0_i_1_n_0 ),
        .\to_Dispatcher_data_out[5]_INST_0_i_1 (\to_Dispatcher_data_out[5]_INST_0_i_1_n_0 ),
        .\to_Dispatcher_data_out[6]_INST_0_i_1 (\to_Dispatcher_data_out[6]_INST_0_i_1_n_0 ),
        .\to_Dispatcher_data_out[7]_INST_0_i_1 (\to_Dispatcher_data_out[7]_INST_0_i_1_n_0 ),
        .\to_Dispatcher_data_out[8]_INST_0_i_1 (\to_Dispatcher_data_out[8]_INST_0_i_1_n_0 ),
        .\to_Dispatcher_data_out[9]_INST_0_i_1 (\to_Dispatcher_data_out[9]_INST_0_i_1_n_0 ));
  FDRE \to_Dispatcher_data_out[0]_INST_0_i_1 
       (.C(from_Dispatcher_clk),
        .CE(\to_Dispatcher_data_out[31]_INST_0_i_1_n_0 ),
        .D(U0_n_7),
        .Q(\to_Dispatcher_data_out[0]_INST_0_i_1_n_0 ),
        .R(1'b0));
  FDRE \to_Dispatcher_data_out[10]_INST_0_i_1 
       (.C(from_Dispatcher_clk),
        .CE(\to_Dispatcher_data_out[31]_INST_0_i_1_n_0 ),
        .D(U0_n_14),
        .Q(\to_Dispatcher_data_out[10]_INST_0_i_1_n_0 ),
        .R(1'b0));
  FDRE \to_Dispatcher_data_out[11]_INST_0_i_1 
       (.C(from_Dispatcher_clk),
        .CE(\to_Dispatcher_data_out[31]_INST_0_i_1_n_0 ),
        .D(U0_n_13),
        .Q(\to_Dispatcher_data_out[11]_INST_0_i_1_n_0 ),
        .R(1'b0));
  FDRE \to_Dispatcher_data_out[12]_INST_0_i_1 
       (.C(from_Dispatcher_clk),
        .CE(\to_Dispatcher_data_out[31]_INST_0_i_1_n_0 ),
        .D(U0_n_12),
        .Q(\to_Dispatcher_data_out[12]_INST_0_i_1_n_0 ),
        .R(1'b0));
  FDRE \to_Dispatcher_data_out[13]_INST_0_i_1 
       (.C(from_Dispatcher_clk),
        .CE(\to_Dispatcher_data_out[31]_INST_0_i_1_n_0 ),
        .D(U0_n_11),
        .Q(\to_Dispatcher_data_out[13]_INST_0_i_1_n_0 ),
        .R(1'b0));
  FDRE \to_Dispatcher_data_out[14]_INST_0_i_1 
       (.C(from_Dispatcher_clk),
        .CE(\to_Dispatcher_data_out[31]_INST_0_i_1_n_0 ),
        .D(U0_n_10),
        .Q(\to_Dispatcher_data_out[14]_INST_0_i_1_n_0 ),
        .R(1'b0));
  FDRE \to_Dispatcher_data_out[15]_INST_0_i_1 
       (.C(from_Dispatcher_clk),
        .CE(\to_Dispatcher_data_out[31]_INST_0_i_1_n_0 ),
        .D(U0_n_9),
        .Q(\to_Dispatcher_data_out[15]_INST_0_i_1_n_0 ),
        .R(1'b0));
  FDRE \to_Dispatcher_data_out[16]_INST_0_i_1 
       (.C(from_Dispatcher_clk),
        .CE(\to_Dispatcher_data_out[31]_INST_0_i_1_n_0 ),
        .D(U0_n_24),
        .Q(\to_Dispatcher_data_out[16]_INST_0_i_1_n_0 ),
        .R(1'b0));
  FDRE \to_Dispatcher_data_out[17]_INST_0_i_1 
       (.C(from_Dispatcher_clk),
        .CE(\to_Dispatcher_data_out[31]_INST_0_i_1_n_0 ),
        .D(U0_n_23),
        .Q(\to_Dispatcher_data_out[17]_INST_0_i_1_n_0 ),
        .R(1'b0));
  FDRE \to_Dispatcher_data_out[18]_INST_0_i_1 
       (.C(from_Dispatcher_clk),
        .CE(\to_Dispatcher_data_out[31]_INST_0_i_1_n_0 ),
        .D(U0_n_22),
        .Q(\to_Dispatcher_data_out[18]_INST_0_i_1_n_0 ),
        .R(1'b0));
  FDRE \to_Dispatcher_data_out[19]_INST_0_i_1 
       (.C(from_Dispatcher_clk),
        .CE(\to_Dispatcher_data_out[31]_INST_0_i_1_n_0 ),
        .D(U0_n_21),
        .Q(\to_Dispatcher_data_out[19]_INST_0_i_1_n_0 ),
        .R(1'b0));
  FDRE \to_Dispatcher_data_out[1]_INST_0_i_1 
       (.C(from_Dispatcher_clk),
        .CE(\to_Dispatcher_data_out[31]_INST_0_i_1_n_0 ),
        .D(U0_n_6),
        .Q(\to_Dispatcher_data_out[1]_INST_0_i_1_n_0 ),
        .R(1'b0));
  FDRE \to_Dispatcher_data_out[20]_INST_0_i_1 
       (.C(from_Dispatcher_clk),
        .CE(\to_Dispatcher_data_out[31]_INST_0_i_1_n_0 ),
        .D(U0_n_20),
        .Q(\to_Dispatcher_data_out[20]_INST_0_i_1_n_0 ),
        .R(1'b0));
  FDRE \to_Dispatcher_data_out[21]_INST_0_i_1 
       (.C(from_Dispatcher_clk),
        .CE(\to_Dispatcher_data_out[31]_INST_0_i_1_n_0 ),
        .D(U0_n_19),
        .Q(\to_Dispatcher_data_out[21]_INST_0_i_1_n_0 ),
        .R(1'b0));
  FDRE \to_Dispatcher_data_out[22]_INST_0_i_1 
       (.C(from_Dispatcher_clk),
        .CE(\to_Dispatcher_data_out[31]_INST_0_i_1_n_0 ),
        .D(U0_n_18),
        .Q(\to_Dispatcher_data_out[22]_INST_0_i_1_n_0 ),
        .R(1'b0));
  FDRE \to_Dispatcher_data_out[23]_INST_0_i_1 
       (.C(from_Dispatcher_clk),
        .CE(\to_Dispatcher_data_out[31]_INST_0_i_1_n_0 ),
        .D(U0_n_17),
        .Q(\to_Dispatcher_data_out[23]_INST_0_i_1_n_0 ),
        .R(1'b0));
  FDRE \to_Dispatcher_data_out[24]_INST_0_i_1 
       (.C(from_Dispatcher_clk),
        .CE(\to_Dispatcher_data_out[31]_INST_0_i_1_n_0 ),
        .D(U0_n_32),
        .Q(\to_Dispatcher_data_out[24]_INST_0_i_1_n_0 ),
        .R(1'b0));
  FDRE \to_Dispatcher_data_out[25]_INST_0_i_1 
       (.C(from_Dispatcher_clk),
        .CE(\to_Dispatcher_data_out[31]_INST_0_i_1_n_0 ),
        .D(U0_n_31),
        .Q(\to_Dispatcher_data_out[25]_INST_0_i_1_n_0 ),
        .R(1'b0));
  FDRE \to_Dispatcher_data_out[26]_INST_0_i_1 
       (.C(from_Dispatcher_clk),
        .CE(\to_Dispatcher_data_out[31]_INST_0_i_1_n_0 ),
        .D(U0_n_30),
        .Q(\to_Dispatcher_data_out[26]_INST_0_i_1_n_0 ),
        .R(1'b0));
  FDRE \to_Dispatcher_data_out[27]_INST_0_i_1 
       (.C(from_Dispatcher_clk),
        .CE(\to_Dispatcher_data_out[31]_INST_0_i_1_n_0 ),
        .D(U0_n_29),
        .Q(\to_Dispatcher_data_out[27]_INST_0_i_1_n_0 ),
        .R(1'b0));
  FDRE \to_Dispatcher_data_out[28]_INST_0_i_1 
       (.C(from_Dispatcher_clk),
        .CE(\to_Dispatcher_data_out[31]_INST_0_i_1_n_0 ),
        .D(U0_n_28),
        .Q(\to_Dispatcher_data_out[28]_INST_0_i_1_n_0 ),
        .R(1'b0));
  FDRE \to_Dispatcher_data_out[29]_INST_0_i_1 
       (.C(from_Dispatcher_clk),
        .CE(\to_Dispatcher_data_out[31]_INST_0_i_1_n_0 ),
        .D(U0_n_27),
        .Q(\to_Dispatcher_data_out[29]_INST_0_i_1_n_0 ),
        .R(1'b0));
  FDRE \to_Dispatcher_data_out[2]_INST_0_i_1 
       (.C(from_Dispatcher_clk),
        .CE(\to_Dispatcher_data_out[31]_INST_0_i_1_n_0 ),
        .D(U0_n_5),
        .Q(\to_Dispatcher_data_out[2]_INST_0_i_1_n_0 ),
        .R(1'b0));
  FDRE \to_Dispatcher_data_out[30]_INST_0_i_1 
       (.C(from_Dispatcher_clk),
        .CE(\to_Dispatcher_data_out[31]_INST_0_i_1_n_0 ),
        .D(U0_n_26),
        .Q(\to_Dispatcher_data_out[30]_INST_0_i_1_n_0 ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \to_Dispatcher_data_out[31]_INST_0_i_1 
       (.C(from_Dispatcher_clk),
        .CE(1'b1),
        .D(\PL_to_PS_S00_AXI_inst/p_0_in ),
        .Q(\to_Dispatcher_data_out[31]_INST_0_i_1_n_0 ),
        .R(U0_n_8));
  FDRE \to_Dispatcher_data_out[31]_INST_0_i_2 
       (.C(from_Dispatcher_clk),
        .CE(\to_Dispatcher_data_out[31]_INST_0_i_1_n_0 ),
        .D(U0_n_25),
        .Q(\to_Dispatcher_data_out[31]_INST_0_i_2_n_0 ),
        .R(1'b0));
  FDRE \to_Dispatcher_data_out[3]_INST_0_i_1 
       (.C(from_Dispatcher_clk),
        .CE(\to_Dispatcher_data_out[31]_INST_0_i_1_n_0 ),
        .D(U0_n_4),
        .Q(\to_Dispatcher_data_out[3]_INST_0_i_1_n_0 ),
        .R(1'b0));
  FDRE \to_Dispatcher_data_out[4]_INST_0_i_1 
       (.C(from_Dispatcher_clk),
        .CE(\to_Dispatcher_data_out[31]_INST_0_i_1_n_0 ),
        .D(U0_n_3),
        .Q(\to_Dispatcher_data_out[4]_INST_0_i_1_n_0 ),
        .R(1'b0));
  FDRE \to_Dispatcher_data_out[5]_INST_0_i_1 
       (.C(from_Dispatcher_clk),
        .CE(\to_Dispatcher_data_out[31]_INST_0_i_1_n_0 ),
        .D(U0_n_2),
        .Q(\to_Dispatcher_data_out[5]_INST_0_i_1_n_0 ),
        .R(1'b0));
  FDRE \to_Dispatcher_data_out[6]_INST_0_i_1 
       (.C(from_Dispatcher_clk),
        .CE(\to_Dispatcher_data_out[31]_INST_0_i_1_n_0 ),
        .D(U0_n_1),
        .Q(\to_Dispatcher_data_out[6]_INST_0_i_1_n_0 ),
        .R(1'b0));
  FDRE \to_Dispatcher_data_out[7]_INST_0_i_1 
       (.C(from_Dispatcher_clk),
        .CE(\to_Dispatcher_data_out[31]_INST_0_i_1_n_0 ),
        .D(U0_n_0),
        .Q(\to_Dispatcher_data_out[7]_INST_0_i_1_n_0 ),
        .R(1'b0));
  FDRE \to_Dispatcher_data_out[8]_INST_0_i_1 
       (.C(from_Dispatcher_clk),
        .CE(\to_Dispatcher_data_out[31]_INST_0_i_1_n_0 ),
        .D(U0_n_16),
        .Q(\to_Dispatcher_data_out[8]_INST_0_i_1_n_0 ),
        .R(1'b0));
  FDRE \to_Dispatcher_data_out[9]_INST_0_i_1 
       (.C(from_Dispatcher_clk),
        .CE(\to_Dispatcher_data_out[31]_INST_0_i_1_n_0 ),
        .D(U0_n_15),
        .Q(\to_Dispatcher_data_out[9]_INST_0_i_1_n_0 ),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "PL_to_PS" *) 
module design_1_PL_to_PS_0_0_PL_to_PS
   (DOADO,
    WEA,
    \to_Dispatcher_data_out[15]_INST_0_i_1 ,
    \to_Dispatcher_data_out[23]_INST_0_i_1 ,
    \to_Dispatcher_data_out[31]_INST_0_i_2 ,
    s_axi_intr_wready,
    s_axi_intr_awready,
    s_axi_intr_arready,
    irq,
    s00_axi_wready,
    s00_axi_awready,
    s00_axi_rvalid,
    s00_axi_arready,
    s00_axi_bvalid,
    s00_axi_rlast,
    s_axi_intr_bvalid,
    s_axi_intr_rvalid,
    to_Dispatcher_data_out,
    s_axi_intr_rdata,
    p_0_in,
    s00_axi_rdata,
    s_axi_intr_aclk,
    from_Dispatcher_clk,
    s00_axi_aclk,
    from_Dispatcher_address,
    from_Dispatcher_data_in,
    s_axi_intr_wdata,
    \to_Dispatcher_data_out[31]_INST_0_i_1 ,
    \to_Dispatcher_data_out[0]_INST_0_i_1 ,
    \to_Dispatcher_data_out[1]_INST_0_i_1 ,
    \to_Dispatcher_data_out[2]_INST_0_i_1 ,
    \to_Dispatcher_data_out[3]_INST_0_i_1 ,
    \to_Dispatcher_data_out[4]_INST_0_i_1 ,
    \to_Dispatcher_data_out[5]_INST_0_i_1 ,
    \to_Dispatcher_data_out[6]_INST_0_i_1 ,
    \to_Dispatcher_data_out[7]_INST_0_i_1 ,
    \to_Dispatcher_data_out[8]_INST_0_i_1 ,
    \to_Dispatcher_data_out[9]_INST_0_i_1 ,
    \to_Dispatcher_data_out[10]_INST_0_i_1 ,
    \to_Dispatcher_data_out[11]_INST_0_i_1 ,
    \to_Dispatcher_data_out[12]_INST_0_i_1 ,
    \to_Dispatcher_data_out[13]_INST_0_i_1 ,
    \to_Dispatcher_data_out[14]_INST_0_i_1 ,
    \to_Dispatcher_data_out[15]_INST_0_i_1_0 ,
    \to_Dispatcher_data_out[16]_INST_0_i_1 ,
    \to_Dispatcher_data_out[17]_INST_0_i_1 ,
    \to_Dispatcher_data_out[18]_INST_0_i_1 ,
    \to_Dispatcher_data_out[19]_INST_0_i_1 ,
    \to_Dispatcher_data_out[20]_INST_0_i_1 ,
    \to_Dispatcher_data_out[21]_INST_0_i_1 ,
    \to_Dispatcher_data_out[22]_INST_0_i_1 ,
    \to_Dispatcher_data_out[23]_INST_0_i_1_0 ,
    \to_Dispatcher_data_out[24]_INST_0_i_1 ,
    \to_Dispatcher_data_out[25]_INST_0_i_1 ,
    \to_Dispatcher_data_out[26]_INST_0_i_1 ,
    \to_Dispatcher_data_out[27]_INST_0_i_1 ,
    \to_Dispatcher_data_out[28]_INST_0_i_1 ,
    \to_Dispatcher_data_out[29]_INST_0_i_1 ,
    \to_Dispatcher_data_out[30]_INST_0_i_1 ,
    \to_Dispatcher_data_out[31]_INST_0_i_2_0 ,
    s00_axi_arvalid,
    s00_axi_wvalid,
    s00_axi_wlast,
    s00_axi_awvalid,
    s00_axi_rready,
    s00_axi_bready,
    s_axi_intr_awvalid,
    s_axi_intr_wvalid,
    s_axi_intr_bready,
    s_axi_intr_arvalid,
    s_axi_intr_rready,
    dispatcher_generate_interrupt,
    from_Dispatcher_reset,
    s_axi_intr_aresetn,
    s00_axi_arlen,
    s00_axi_awburst,
    s00_axi_awlen,
    s00_axi_arburst,
    s_axi_intr_awaddr,
    s_axi_intr_araddr,
    from_Dispatcher_bytes_selection,
    from_Dispatcher_enable,
    fifo_ptm_full,
    fifo_ptm_almost_full,
    fifo_instrumentation_full,
    fifo_instrumentation_almost_full,
    fifo_kernel_to_monitor_full,
    fifo_kernel_to_monitor_almost_full,
    fifo_monitor_to_kernel_full,
    fifo_monitor_to_kernel_almost_full,
    fifo_kernel_to_monitor_empty,
    fifo_kernel_to_monitor_almost_empty,
    fifo_instrumentation_empty,
    fifo_instrumentation_almost_empty,
    fifo_monitor_to_kernel_almost_empty,
    fifo_monitor_to_kernel_empty,
    fifo_ptm_empty,
    fifo_ptm_almost_empty,
    s00_axi_awaddr,
    s00_axi_aresetn,
    s00_axi_araddr);
  output [7:0]DOADO;
  output [0:0]WEA;
  output [7:0]\to_Dispatcher_data_out[15]_INST_0_i_1 ;
  output [7:0]\to_Dispatcher_data_out[23]_INST_0_i_1 ;
  output [7:0]\to_Dispatcher_data_out[31]_INST_0_i_2 ;
  output s_axi_intr_wready;
  output s_axi_intr_awready;
  output s_axi_intr_arready;
  output irq;
  output s00_axi_wready;
  output s00_axi_awready;
  output s00_axi_rvalid;
  output s00_axi_arready;
  output s00_axi_bvalid;
  output s00_axi_rlast;
  output s_axi_intr_bvalid;
  output s_axi_intr_rvalid;
  output [31:0]to_Dispatcher_data_out;
  output [5:0]s_axi_intr_rdata;
  output p_0_in;
  output [31:0]s00_axi_rdata;
  input s_axi_intr_aclk;
  input from_Dispatcher_clk;
  input s00_axi_aclk;
  input [6:0]from_Dispatcher_address;
  input [31:0]from_Dispatcher_data_in;
  input [5:0]s_axi_intr_wdata;
  input \to_Dispatcher_data_out[31]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[0]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[1]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[2]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[3]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[4]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[5]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[6]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[7]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[8]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[9]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[10]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[11]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[12]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[13]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[14]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[15]_INST_0_i_1_0 ;
  input \to_Dispatcher_data_out[16]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[17]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[18]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[19]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[20]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[21]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[22]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[23]_INST_0_i_1_0 ;
  input \to_Dispatcher_data_out[24]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[25]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[26]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[27]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[28]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[29]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[30]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[31]_INST_0_i_2_0 ;
  input s00_axi_arvalid;
  input s00_axi_wvalid;
  input s00_axi_wlast;
  input s00_axi_awvalid;
  input s00_axi_rready;
  input s00_axi_bready;
  input s_axi_intr_awvalid;
  input s_axi_intr_wvalid;
  input s_axi_intr_bready;
  input s_axi_intr_arvalid;
  input s_axi_intr_rready;
  input dispatcher_generate_interrupt;
  input from_Dispatcher_reset;
  input s_axi_intr_aresetn;
  input [7:0]s00_axi_arlen;
  input [1:0]s00_axi_awburst;
  input [7:0]s00_axi_awlen;
  input [1:0]s00_axi_arburst;
  input [2:0]s_axi_intr_awaddr;
  input [4:0]s_axi_intr_araddr;
  input [3:0]from_Dispatcher_bytes_selection;
  input from_Dispatcher_enable;
  input fifo_ptm_full;
  input fifo_ptm_almost_full;
  input fifo_instrumentation_full;
  input fifo_instrumentation_almost_full;
  input fifo_kernel_to_monitor_full;
  input fifo_kernel_to_monitor_almost_full;
  input fifo_monitor_to_kernel_full;
  input fifo_monitor_to_kernel_almost_full;
  input fifo_kernel_to_monitor_empty;
  input fifo_kernel_to_monitor_almost_empty;
  input fifo_instrumentation_empty;
  input fifo_instrumentation_almost_empty;
  input fifo_monitor_to_kernel_almost_empty;
  input fifo_monitor_to_kernel_empty;
  input fifo_ptm_empty;
  input fifo_ptm_almost_empty;
  input [7:0]s00_axi_awaddr;
  input s00_axi_aresetn;
  input [7:0]s00_axi_araddr;

  wire [7:0]DOADO;
  wire [0:0]WEA;
  wire dispatcher_generate_interrupt;
  wire fifo_instrumentation_almost_empty;
  wire fifo_instrumentation_almost_full;
  wire fifo_instrumentation_empty;
  wire fifo_instrumentation_full;
  wire fifo_kernel_to_monitor_almost_empty;
  wire fifo_kernel_to_monitor_almost_full;
  wire fifo_kernel_to_monitor_empty;
  wire fifo_kernel_to_monitor_full;
  wire fifo_monitor_to_kernel_almost_empty;
  wire fifo_monitor_to_kernel_almost_full;
  wire fifo_monitor_to_kernel_empty;
  wire fifo_monitor_to_kernel_full;
  wire fifo_ptm_almost_empty;
  wire fifo_ptm_almost_full;
  wire fifo_ptm_empty;
  wire fifo_ptm_full;
  wire [6:0]from_Dispatcher_address;
  wire [3:0]from_Dispatcher_bytes_selection;
  wire from_Dispatcher_clk;
  wire [31:0]from_Dispatcher_data_in;
  wire from_Dispatcher_enable;
  wire from_Dispatcher_reset;
  wire irq;
  wire p_0_in;
  wire s00_axi_aclk;
  wire [7:0]s00_axi_araddr;
  wire [1:0]s00_axi_arburst;
  wire s00_axi_aresetn;
  wire [7:0]s00_axi_arlen;
  wire s00_axi_arready;
  wire s00_axi_arvalid;
  wire [7:0]s00_axi_awaddr;
  wire [1:0]s00_axi_awburst;
  wire [7:0]s00_axi_awlen;
  wire s00_axi_awready;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rlast;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire s00_axi_wlast;
  wire s00_axi_wready;
  wire s00_axi_wvalid;
  wire s_axi_intr_aclk;
  wire [4:0]s_axi_intr_araddr;
  wire s_axi_intr_aresetn;
  wire s_axi_intr_arready;
  wire s_axi_intr_arvalid;
  wire [2:0]s_axi_intr_awaddr;
  wire s_axi_intr_awready;
  wire s_axi_intr_awvalid;
  wire s_axi_intr_bready;
  wire s_axi_intr_bvalid;
  wire [5:0]s_axi_intr_rdata;
  wire s_axi_intr_rready;
  wire s_axi_intr_rvalid;
  wire [5:0]s_axi_intr_wdata;
  wire s_axi_intr_wready;
  wire s_axi_intr_wvalid;
  wire [31:0]to_Dispatcher_data_out;
  wire \to_Dispatcher_data_out[0]_INST_0_i_1 ;
  wire \to_Dispatcher_data_out[10]_INST_0_i_1 ;
  wire \to_Dispatcher_data_out[11]_INST_0_i_1 ;
  wire \to_Dispatcher_data_out[12]_INST_0_i_1 ;
  wire \to_Dispatcher_data_out[13]_INST_0_i_1 ;
  wire \to_Dispatcher_data_out[14]_INST_0_i_1 ;
  wire [7:0]\to_Dispatcher_data_out[15]_INST_0_i_1 ;
  wire \to_Dispatcher_data_out[15]_INST_0_i_1_0 ;
  wire \to_Dispatcher_data_out[16]_INST_0_i_1 ;
  wire \to_Dispatcher_data_out[17]_INST_0_i_1 ;
  wire \to_Dispatcher_data_out[18]_INST_0_i_1 ;
  wire \to_Dispatcher_data_out[19]_INST_0_i_1 ;
  wire \to_Dispatcher_data_out[1]_INST_0_i_1 ;
  wire \to_Dispatcher_data_out[20]_INST_0_i_1 ;
  wire \to_Dispatcher_data_out[21]_INST_0_i_1 ;
  wire \to_Dispatcher_data_out[22]_INST_0_i_1 ;
  wire [7:0]\to_Dispatcher_data_out[23]_INST_0_i_1 ;
  wire \to_Dispatcher_data_out[23]_INST_0_i_1_0 ;
  wire \to_Dispatcher_data_out[24]_INST_0_i_1 ;
  wire \to_Dispatcher_data_out[25]_INST_0_i_1 ;
  wire \to_Dispatcher_data_out[26]_INST_0_i_1 ;
  wire \to_Dispatcher_data_out[27]_INST_0_i_1 ;
  wire \to_Dispatcher_data_out[28]_INST_0_i_1 ;
  wire \to_Dispatcher_data_out[29]_INST_0_i_1 ;
  wire \to_Dispatcher_data_out[2]_INST_0_i_1 ;
  wire \to_Dispatcher_data_out[30]_INST_0_i_1 ;
  wire \to_Dispatcher_data_out[31]_INST_0_i_1 ;
  wire [7:0]\to_Dispatcher_data_out[31]_INST_0_i_2 ;
  wire \to_Dispatcher_data_out[31]_INST_0_i_2_0 ;
  wire \to_Dispatcher_data_out[3]_INST_0_i_1 ;
  wire \to_Dispatcher_data_out[4]_INST_0_i_1 ;
  wire \to_Dispatcher_data_out[5]_INST_0_i_1 ;
  wire \to_Dispatcher_data_out[6]_INST_0_i_1 ;
  wire \to_Dispatcher_data_out[7]_INST_0_i_1 ;
  wire \to_Dispatcher_data_out[8]_INST_0_i_1 ;
  wire \to_Dispatcher_data_out[9]_INST_0_i_1 ;

  design_1_PL_to_PS_0_0_PL_to_PS_S00_AXI PL_to_PS_S00_AXI_inst
       (.DOADO(DOADO),
        .WEA(WEA),
        .from_Dispatcher_address(from_Dispatcher_address),
        .from_Dispatcher_bytes_selection(from_Dispatcher_bytes_selection),
        .from_Dispatcher_clk(from_Dispatcher_clk),
        .from_Dispatcher_data_in(from_Dispatcher_data_in),
        .from_Dispatcher_enable(from_Dispatcher_enable),
        .p_0_in(p_0_in),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr),
        .s00_axi_arburst(s00_axi_arburst),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_arlen(s00_axi_arlen),
        .s00_axi_arready(s00_axi_arready),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr),
        .s00_axi_awburst(s00_axi_awburst),
        .s00_axi_awlen(s00_axi_awlen),
        .s00_axi_awready(s00_axi_awready),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bready(s00_axi_bready),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rlast(s00_axi_rlast),
        .s00_axi_rready(s00_axi_rready),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wlast(s00_axi_wlast),
        .s00_axi_wready(s00_axi_wready),
        .s00_axi_wvalid(s00_axi_wvalid),
        .to_Dispatcher_data_out(to_Dispatcher_data_out),
        .\to_Dispatcher_data_out[0]_INST_0_i_1 (\to_Dispatcher_data_out[0]_INST_0_i_1 ),
        .\to_Dispatcher_data_out[10]_INST_0_i_1 (\to_Dispatcher_data_out[10]_INST_0_i_1 ),
        .\to_Dispatcher_data_out[11]_INST_0_i_1 (\to_Dispatcher_data_out[11]_INST_0_i_1 ),
        .\to_Dispatcher_data_out[12]_INST_0_i_1 (\to_Dispatcher_data_out[12]_INST_0_i_1 ),
        .\to_Dispatcher_data_out[13]_INST_0_i_1 (\to_Dispatcher_data_out[13]_INST_0_i_1 ),
        .\to_Dispatcher_data_out[14]_INST_0_i_1 (\to_Dispatcher_data_out[14]_INST_0_i_1 ),
        .\to_Dispatcher_data_out[15]_INST_0_i_1 (\to_Dispatcher_data_out[15]_INST_0_i_1 ),
        .\to_Dispatcher_data_out[15]_INST_0_i_1_0 (\to_Dispatcher_data_out[15]_INST_0_i_1_0 ),
        .\to_Dispatcher_data_out[16]_INST_0_i_1 (\to_Dispatcher_data_out[16]_INST_0_i_1 ),
        .\to_Dispatcher_data_out[17]_INST_0_i_1 (\to_Dispatcher_data_out[17]_INST_0_i_1 ),
        .\to_Dispatcher_data_out[18]_INST_0_i_1 (\to_Dispatcher_data_out[18]_INST_0_i_1 ),
        .\to_Dispatcher_data_out[19]_INST_0_i_1 (\to_Dispatcher_data_out[19]_INST_0_i_1 ),
        .\to_Dispatcher_data_out[1]_INST_0_i_1 (\to_Dispatcher_data_out[1]_INST_0_i_1 ),
        .\to_Dispatcher_data_out[20]_INST_0_i_1 (\to_Dispatcher_data_out[20]_INST_0_i_1 ),
        .\to_Dispatcher_data_out[21]_INST_0_i_1 (\to_Dispatcher_data_out[21]_INST_0_i_1 ),
        .\to_Dispatcher_data_out[22]_INST_0_i_1 (\to_Dispatcher_data_out[22]_INST_0_i_1 ),
        .\to_Dispatcher_data_out[23]_INST_0_i_1 (\to_Dispatcher_data_out[23]_INST_0_i_1 ),
        .\to_Dispatcher_data_out[23]_INST_0_i_1_0 (\to_Dispatcher_data_out[23]_INST_0_i_1_0 ),
        .\to_Dispatcher_data_out[24]_INST_0_i_1 (\to_Dispatcher_data_out[24]_INST_0_i_1 ),
        .\to_Dispatcher_data_out[25]_INST_0_i_1 (\to_Dispatcher_data_out[25]_INST_0_i_1 ),
        .\to_Dispatcher_data_out[26]_INST_0_i_1 (\to_Dispatcher_data_out[26]_INST_0_i_1 ),
        .\to_Dispatcher_data_out[27]_INST_0_i_1 (\to_Dispatcher_data_out[27]_INST_0_i_1 ),
        .\to_Dispatcher_data_out[28]_INST_0_i_1 (\to_Dispatcher_data_out[28]_INST_0_i_1 ),
        .\to_Dispatcher_data_out[29]_INST_0_i_1 (\to_Dispatcher_data_out[29]_INST_0_i_1 ),
        .\to_Dispatcher_data_out[2]_INST_0_i_1 (\to_Dispatcher_data_out[2]_INST_0_i_1 ),
        .\to_Dispatcher_data_out[30]_INST_0_i_1 (\to_Dispatcher_data_out[30]_INST_0_i_1 ),
        .\to_Dispatcher_data_out[31]_INST_0_i_1 (\to_Dispatcher_data_out[31]_INST_0_i_1 ),
        .\to_Dispatcher_data_out[31]_INST_0_i_2 (\to_Dispatcher_data_out[31]_INST_0_i_2 ),
        .\to_Dispatcher_data_out[31]_INST_0_i_2_0 (\to_Dispatcher_data_out[31]_INST_0_i_2_0 ),
        .\to_Dispatcher_data_out[3]_INST_0_i_1 (\to_Dispatcher_data_out[3]_INST_0_i_1 ),
        .\to_Dispatcher_data_out[4]_INST_0_i_1 (\to_Dispatcher_data_out[4]_INST_0_i_1 ),
        .\to_Dispatcher_data_out[5]_INST_0_i_1 (\to_Dispatcher_data_out[5]_INST_0_i_1 ),
        .\to_Dispatcher_data_out[6]_INST_0_i_1 (\to_Dispatcher_data_out[6]_INST_0_i_1 ),
        .\to_Dispatcher_data_out[7]_INST_0_i_1 (\to_Dispatcher_data_out[7]_INST_0_i_1 ),
        .\to_Dispatcher_data_out[8]_INST_0_i_1 (\to_Dispatcher_data_out[8]_INST_0_i_1 ),
        .\to_Dispatcher_data_out[9]_INST_0_i_1 (\to_Dispatcher_data_out[9]_INST_0_i_1 ));
  design_1_PL_to_PS_0_0_PL_to_PS_S_AXI_INTR PL_to_PS_S_AXI_INTR_inst
       (.dispatcher_generate_interrupt(dispatcher_generate_interrupt),
        .fifo_instrumentation_almost_empty(fifo_instrumentation_almost_empty),
        .fifo_instrumentation_almost_full(fifo_instrumentation_almost_full),
        .fifo_instrumentation_empty(fifo_instrumentation_empty),
        .fifo_instrumentation_full(fifo_instrumentation_full),
        .fifo_kernel_to_monitor_almost_empty(fifo_kernel_to_monitor_almost_empty),
        .fifo_kernel_to_monitor_almost_full(fifo_kernel_to_monitor_almost_full),
        .fifo_kernel_to_monitor_empty(fifo_kernel_to_monitor_empty),
        .fifo_kernel_to_monitor_full(fifo_kernel_to_monitor_full),
        .fifo_monitor_to_kernel_almost_empty(fifo_monitor_to_kernel_almost_empty),
        .fifo_monitor_to_kernel_almost_full(fifo_monitor_to_kernel_almost_full),
        .fifo_monitor_to_kernel_empty(fifo_monitor_to_kernel_empty),
        .fifo_monitor_to_kernel_full(fifo_monitor_to_kernel_full),
        .fifo_ptm_almost_empty(fifo_ptm_almost_empty),
        .fifo_ptm_almost_full(fifo_ptm_almost_full),
        .fifo_ptm_empty(fifo_ptm_empty),
        .fifo_ptm_full(fifo_ptm_full),
        .from_Dispatcher_clk(from_Dispatcher_clk),
        .from_Dispatcher_reset(from_Dispatcher_reset),
        .irq(irq),
        .s_axi_intr_aclk(s_axi_intr_aclk),
        .s_axi_intr_araddr(s_axi_intr_araddr),
        .s_axi_intr_aresetn(s_axi_intr_aresetn),
        .s_axi_intr_arready(s_axi_intr_arready),
        .s_axi_intr_arvalid(s_axi_intr_arvalid),
        .s_axi_intr_awaddr(s_axi_intr_awaddr),
        .s_axi_intr_awready(s_axi_intr_awready),
        .s_axi_intr_awvalid(s_axi_intr_awvalid),
        .s_axi_intr_bready(s_axi_intr_bready),
        .s_axi_intr_bvalid(s_axi_intr_bvalid),
        .s_axi_intr_rdata(s_axi_intr_rdata),
        .s_axi_intr_rready(s_axi_intr_rready),
        .s_axi_intr_rvalid(s_axi_intr_rvalid),
        .s_axi_intr_wdata(s_axi_intr_wdata),
        .s_axi_intr_wready(s_axi_intr_wready),
        .s_axi_intr_wvalid(s_axi_intr_wvalid));
endmodule

(* ORIG_REF_NAME = "PL_to_PS_S00_AXI" *) 
module design_1_PL_to_PS_0_0_PL_to_PS_S00_AXI
   (DOADO,
    WEA,
    \to_Dispatcher_data_out[15]_INST_0_i_1 ,
    \to_Dispatcher_data_out[23]_INST_0_i_1 ,
    \to_Dispatcher_data_out[31]_INST_0_i_2 ,
    s00_axi_wready,
    s00_axi_awready,
    s00_axi_rvalid,
    s00_axi_arready,
    s00_axi_bvalid,
    s00_axi_rlast,
    to_Dispatcher_data_out,
    p_0_in,
    s00_axi_rdata,
    from_Dispatcher_clk,
    s00_axi_aclk,
    from_Dispatcher_address,
    from_Dispatcher_data_in,
    \to_Dispatcher_data_out[31]_INST_0_i_1 ,
    \to_Dispatcher_data_out[0]_INST_0_i_1 ,
    \to_Dispatcher_data_out[1]_INST_0_i_1 ,
    \to_Dispatcher_data_out[2]_INST_0_i_1 ,
    \to_Dispatcher_data_out[3]_INST_0_i_1 ,
    \to_Dispatcher_data_out[4]_INST_0_i_1 ,
    \to_Dispatcher_data_out[5]_INST_0_i_1 ,
    \to_Dispatcher_data_out[6]_INST_0_i_1 ,
    \to_Dispatcher_data_out[7]_INST_0_i_1 ,
    \to_Dispatcher_data_out[8]_INST_0_i_1 ,
    \to_Dispatcher_data_out[9]_INST_0_i_1 ,
    \to_Dispatcher_data_out[10]_INST_0_i_1 ,
    \to_Dispatcher_data_out[11]_INST_0_i_1 ,
    \to_Dispatcher_data_out[12]_INST_0_i_1 ,
    \to_Dispatcher_data_out[13]_INST_0_i_1 ,
    \to_Dispatcher_data_out[14]_INST_0_i_1 ,
    \to_Dispatcher_data_out[15]_INST_0_i_1_0 ,
    \to_Dispatcher_data_out[16]_INST_0_i_1 ,
    \to_Dispatcher_data_out[17]_INST_0_i_1 ,
    \to_Dispatcher_data_out[18]_INST_0_i_1 ,
    \to_Dispatcher_data_out[19]_INST_0_i_1 ,
    \to_Dispatcher_data_out[20]_INST_0_i_1 ,
    \to_Dispatcher_data_out[21]_INST_0_i_1 ,
    \to_Dispatcher_data_out[22]_INST_0_i_1 ,
    \to_Dispatcher_data_out[23]_INST_0_i_1_0 ,
    \to_Dispatcher_data_out[24]_INST_0_i_1 ,
    \to_Dispatcher_data_out[25]_INST_0_i_1 ,
    \to_Dispatcher_data_out[26]_INST_0_i_1 ,
    \to_Dispatcher_data_out[27]_INST_0_i_1 ,
    \to_Dispatcher_data_out[28]_INST_0_i_1 ,
    \to_Dispatcher_data_out[29]_INST_0_i_1 ,
    \to_Dispatcher_data_out[30]_INST_0_i_1 ,
    \to_Dispatcher_data_out[31]_INST_0_i_2_0 ,
    s00_axi_arvalid,
    s00_axi_wvalid,
    s00_axi_wlast,
    s00_axi_awvalid,
    s00_axi_rready,
    s00_axi_bready,
    s00_axi_arlen,
    s00_axi_awburst,
    s00_axi_awlen,
    s00_axi_arburst,
    from_Dispatcher_bytes_selection,
    from_Dispatcher_enable,
    s00_axi_awaddr,
    s00_axi_aresetn,
    s00_axi_araddr);
  output [7:0]DOADO;
  output [0:0]WEA;
  output [7:0]\to_Dispatcher_data_out[15]_INST_0_i_1 ;
  output [7:0]\to_Dispatcher_data_out[23]_INST_0_i_1 ;
  output [7:0]\to_Dispatcher_data_out[31]_INST_0_i_2 ;
  output s00_axi_wready;
  output s00_axi_awready;
  output s00_axi_rvalid;
  output s00_axi_arready;
  output s00_axi_bvalid;
  output s00_axi_rlast;
  output [31:0]to_Dispatcher_data_out;
  output p_0_in;
  output [31:0]s00_axi_rdata;
  input from_Dispatcher_clk;
  input s00_axi_aclk;
  input [6:0]from_Dispatcher_address;
  input [31:0]from_Dispatcher_data_in;
  input \to_Dispatcher_data_out[31]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[0]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[1]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[2]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[3]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[4]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[5]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[6]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[7]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[8]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[9]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[10]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[11]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[12]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[13]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[14]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[15]_INST_0_i_1_0 ;
  input \to_Dispatcher_data_out[16]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[17]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[18]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[19]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[20]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[21]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[22]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[23]_INST_0_i_1_0 ;
  input \to_Dispatcher_data_out[24]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[25]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[26]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[27]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[28]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[29]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[30]_INST_0_i_1 ;
  input \to_Dispatcher_data_out[31]_INST_0_i_2_0 ;
  input s00_axi_arvalid;
  input s00_axi_wvalid;
  input s00_axi_wlast;
  input s00_axi_awvalid;
  input s00_axi_rready;
  input s00_axi_bready;
  input [7:0]s00_axi_arlen;
  input [1:0]s00_axi_awburst;
  input [7:0]s00_axi_awlen;
  input [1:0]s00_axi_arburst;
  input [3:0]from_Dispatcher_bytes_selection;
  input from_Dispatcher_enable;
  input [7:0]s00_axi_awaddr;
  input s00_axi_aresetn;
  input [7:0]s00_axi_araddr;

  wire [7:0]\BRAM_GEN[0].BYTE_BRAM_GEN[0].mem_data_out_reg[0] ;
  wire [7:0]\BRAM_GEN[0].BYTE_BRAM_GEN[1].mem_data_out_reg[0] ;
  wire [7:0]\BRAM_GEN[0].BYTE_BRAM_GEN[2].mem_data_out_reg[0] ;
  wire [7:0]\BRAM_GEN[0].BYTE_BRAM_GEN[3].mem_data_out_reg[0] ;
  wire [7:0]DOADO;
  wire [9:2]L;
  wire [0:0]WEA;
  wire [9:2]ar_wrap_size__0;
  wire [9:2]aw_wrap_size__0;
  wire [9:2]axi_araddr0;
  wire axi_araddr0_carry__0_i_1_n_0;
  wire axi_araddr0_carry__0_i_2_n_0;
  wire axi_araddr0_carry__0_i_3_n_0;
  wire axi_araddr0_carry__0_i_4_n_0;
  wire axi_araddr0_carry__0_i_5_n_0;
  wire axi_araddr0_carry__0_i_6_n_0;
  wire axi_araddr0_carry__0_i_7_n_0;
  wire axi_araddr0_carry__0_n_1;
  wire axi_araddr0_carry__0_n_2;
  wire axi_araddr0_carry__0_n_3;
  wire axi_araddr0_carry_i_1_n_0;
  wire axi_araddr0_carry_i_2_n_0;
  wire axi_araddr0_carry_i_3_n_0;
  wire axi_araddr0_carry_i_4_n_0;
  wire axi_araddr0_carry_i_5_n_0;
  wire axi_araddr0_carry_i_6_n_0;
  wire axi_araddr0_carry_i_7_n_0;
  wire axi_araddr0_carry_n_0;
  wire axi_araddr0_carry_n_1;
  wire axi_araddr0_carry_n_2;
  wire axi_araddr0_carry_n_3;
  wire axi_araddr1;
  wire axi_araddr3;
  wire axi_araddr3_carry_i_1_n_0;
  wire axi_araddr3_carry_i_2_n_0;
  wire axi_araddr3_carry_i_3_n_0;
  wire axi_araddr3_carry_i_4_n_0;
  wire axi_araddr3_carry_i_5_n_0;
  wire axi_araddr3_carry_i_6_n_0;
  wire axi_araddr3_carry_i_7_n_0;
  wire axi_araddr3_carry_i_8_n_0;
  wire axi_araddr3_carry_n_1;
  wire axi_araddr3_carry_n_2;
  wire axi_araddr3_carry_n_3;
  wire \axi_araddr[2]_i_1_n_0 ;
  wire \axi_araddr[3]_i_1_n_0 ;
  wire \axi_araddr[4]_i_1_n_0 ;
  wire \axi_araddr[5]_i_1_n_0 ;
  wire \axi_araddr[6]_i_1_n_0 ;
  wire \axi_araddr[7]_i_1_n_0 ;
  wire \axi_araddr[8]_i_1_n_0 ;
  wire \axi_araddr[9]_i_1_n_0 ;
  wire \axi_araddr[9]_i_2_n_0 ;
  wire \axi_araddr[9]_i_3_n_0 ;
  wire \axi_araddr[9]_i_4_n_0 ;
  wire \axi_araddr[9]_i_5_n_0 ;
  wire \axi_araddr[9]_i_6_n_0 ;
  wire \axi_araddr_reg_n_0_[9] ;
  wire [1:0]axi_arburst;
  wire \axi_arlen[7]_i_1_n_0 ;
  wire \axi_arlen_cntr[7]_i_1_n_0 ;
  wire \axi_arlen_cntr[7]_i_4_n_0 ;
  wire [7:0]axi_arlen_cntr_reg__0;
  wire axi_arready_i_1__0_n_0;
  wire axi_arready_i_2_n_0;
  wire axi_arready_i_3_n_0;
  wire axi_arready_i_4_n_0;
  wire axi_arready_i_5_n_0;
  wire axi_arready_i_6_n_0;
  wire axi_arv_arr_flag;
  wire axi_arv_arr_flag_i_1_n_0;
  wire [9:2]axi_awaddr0;
  wire axi_awaddr0_carry__0_i_1_n_0;
  wire axi_awaddr0_carry__0_i_2_n_0;
  wire axi_awaddr0_carry__0_i_3_n_0;
  wire axi_awaddr0_carry__0_i_4_n_0;
  wire axi_awaddr0_carry__0_i_5_n_0;
  wire axi_awaddr0_carry__0_i_6_n_0;
  wire axi_awaddr0_carry__0_i_7_n_0;
  wire axi_awaddr0_carry__0_n_1;
  wire axi_awaddr0_carry__0_n_2;
  wire axi_awaddr0_carry__0_n_3;
  wire axi_awaddr0_carry_i_1_n_0;
  wire axi_awaddr0_carry_i_2_n_0;
  wire axi_awaddr0_carry_i_3_n_0;
  wire axi_awaddr0_carry_i_4_n_0;
  wire axi_awaddr0_carry_i_5_n_0;
  wire axi_awaddr0_carry_i_6_n_0;
  wire axi_awaddr0_carry_i_7_n_0;
  wire axi_awaddr0_carry_n_0;
  wire axi_awaddr0_carry_n_1;
  wire axi_awaddr0_carry_n_2;
  wire axi_awaddr0_carry_n_3;
  wire axi_awaddr1;
  wire axi_awaddr3;
  wire axi_awaddr3_carry_i_1_n_0;
  wire axi_awaddr3_carry_i_2_n_0;
  wire axi_awaddr3_carry_i_3_n_0;
  wire axi_awaddr3_carry_i_4_n_0;
  wire axi_awaddr3_carry_i_5_n_0;
  wire axi_awaddr3_carry_i_6_n_0;
  wire axi_awaddr3_carry_i_7_n_0;
  wire axi_awaddr3_carry_i_8_n_0;
  wire axi_awaddr3_carry_n_1;
  wire axi_awaddr3_carry_n_2;
  wire axi_awaddr3_carry_n_3;
  wire \axi_awaddr[9]_i_1_n_0 ;
  wire \axi_awaddr[9]_i_3_n_0 ;
  wire \axi_awaddr[9]_i_4_n_0 ;
  wire \axi_awaddr[9]_i_5_n_0 ;
  wire \axi_awaddr[9]_i_6_n_0 ;
  wire \axi_awaddr[9]_i_7_n_0 ;
  wire [1:0]axi_awburst;
  wire \axi_awlen_cntr[0]_i_1_n_0 ;
  wire \axi_awlen_cntr[7]_i_1_n_0 ;
  wire \axi_awlen_cntr[7]_i_4_n_0 ;
  wire [7:0]axi_awlen_cntr_reg__0;
  wire axi_awready_i_1_n_0;
  wire axi_awready_i_2__0_n_0;
  wire axi_awv_awr_flag;
  wire axi_awv_awr_flag_i_1_n_0;
  wire axi_bvalid_i_1_n_0;
  wire axi_rlast0;
  wire axi_rlast_i_1_n_0;
  wire axi_rlast_i_2_n_0;
  wire axi_rvalid_i_1_n_0;
  wire axi_wready_i_1__0_n_0;
  wire [6:0]from_Dispatcher_address;
  wire [3:0]from_Dispatcher_bytes_selection;
  wire from_Dispatcher_clk;
  wire [31:0]from_Dispatcher_data_in;
  wire from_Dispatcher_enable;
  wire [6:0]mem_address;
  wire p_0_in;
  wire [9:3]p_0_in__0;
  wire [6:0]p_1_in;
  wire [9:2]p_2_in;
  wire p_9_in;
  wire [7:0]plusOp;
  wire [7:1]plusOp__0;
  wire \plusOp_inferred__1/i__carry__0_n_2 ;
  wire \plusOp_inferred__1/i__carry__0_n_3 ;
  wire \plusOp_inferred__1/i__carry_n_0 ;
  wire \plusOp_inferred__1/i__carry_n_1 ;
  wire \plusOp_inferred__1/i__carry_n_2 ;
  wire \plusOp_inferred__1/i__carry_n_3 ;
  wire \plusOp_inferred__2/i__carry__0_n_2 ;
  wire \plusOp_inferred__2/i__carry__0_n_3 ;
  wire \plusOp_inferred__2/i__carry__0_n_5 ;
  wire \plusOp_inferred__2/i__carry__0_n_6 ;
  wire \plusOp_inferred__2/i__carry__0_n_7 ;
  wire \plusOp_inferred__2/i__carry_n_0 ;
  wire \plusOp_inferred__2/i__carry_n_1 ;
  wire \plusOp_inferred__2/i__carry_n_2 ;
  wire \plusOp_inferred__2/i__carry_n_3 ;
  wire \plusOp_inferred__2/i__carry_n_4 ;
  wire \plusOp_inferred__2/i__carry_n_5 ;
  wire \plusOp_inferred__2/i__carry_n_6 ;
  wire \plusOp_inferred__2/i__carry_n_7 ;
  wire s00_axi_aclk;
  wire [7:0]s00_axi_araddr;
  wire [1:0]s00_axi_arburst;
  wire s00_axi_aresetn;
  wire [7:0]s00_axi_arlen;
  wire s00_axi_arready;
  wire s00_axi_arvalid;
  wire [7:0]s00_axi_awaddr;
  wire [1:0]s00_axi_awburst;
  wire [7:0]s00_axi_awlen;
  wire s00_axi_awready;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rlast;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire s00_axi_wlast;
  wire s00_axi_wready;
  wire s00_axi_wvalid;
  wire [31:0]to_Dispatcher_data_out;
  wire \to_Dispatcher_data_out[0]_INST_0_i_1 ;
  wire \to_Dispatcher_data_out[10]_INST_0_i_1 ;
  wire \to_Dispatcher_data_out[11]_INST_0_i_1 ;
  wire \to_Dispatcher_data_out[12]_INST_0_i_1 ;
  wire \to_Dispatcher_data_out[13]_INST_0_i_1 ;
  wire \to_Dispatcher_data_out[14]_INST_0_i_1 ;
  wire [7:0]\to_Dispatcher_data_out[15]_INST_0_i_1 ;
  wire \to_Dispatcher_data_out[15]_INST_0_i_1_0 ;
  wire \to_Dispatcher_data_out[16]_INST_0_i_1 ;
  wire \to_Dispatcher_data_out[17]_INST_0_i_1 ;
  wire \to_Dispatcher_data_out[18]_INST_0_i_1 ;
  wire \to_Dispatcher_data_out[19]_INST_0_i_1 ;
  wire \to_Dispatcher_data_out[1]_INST_0_i_1 ;
  wire \to_Dispatcher_data_out[20]_INST_0_i_1 ;
  wire \to_Dispatcher_data_out[21]_INST_0_i_1 ;
  wire \to_Dispatcher_data_out[22]_INST_0_i_1 ;
  wire [7:0]\to_Dispatcher_data_out[23]_INST_0_i_1 ;
  wire \to_Dispatcher_data_out[23]_INST_0_i_1_0 ;
  wire \to_Dispatcher_data_out[24]_INST_0_i_1 ;
  wire \to_Dispatcher_data_out[25]_INST_0_i_1 ;
  wire \to_Dispatcher_data_out[26]_INST_0_i_1 ;
  wire \to_Dispatcher_data_out[27]_INST_0_i_1 ;
  wire \to_Dispatcher_data_out[28]_INST_0_i_1 ;
  wire \to_Dispatcher_data_out[29]_INST_0_i_1 ;
  wire \to_Dispatcher_data_out[2]_INST_0_i_1 ;
  wire \to_Dispatcher_data_out[30]_INST_0_i_1 ;
  wire \to_Dispatcher_data_out[31]_INST_0_i_1 ;
  wire [7:0]\to_Dispatcher_data_out[31]_INST_0_i_2 ;
  wire \to_Dispatcher_data_out[31]_INST_0_i_2_0 ;
  wire \to_Dispatcher_data_out[3]_INST_0_i_1 ;
  wire \to_Dispatcher_data_out[4]_INST_0_i_1 ;
  wire \to_Dispatcher_data_out[5]_INST_0_i_1 ;
  wire \to_Dispatcher_data_out[6]_INST_0_i_1 ;
  wire \to_Dispatcher_data_out[7]_INST_0_i_1 ;
  wire \to_Dispatcher_data_out[8]_INST_0_i_1 ;
  wire \to_Dispatcher_data_out[9]_INST_0_i_1 ;
  wire [15:8]\NLW_BRAM_GEN[0].BYTE_BRAM_GEN[0].byte_ram_reg_DOADO_UNCONNECTED ;
  wire [15:8]\NLW_BRAM_GEN[0].BYTE_BRAM_GEN[0].byte_ram_reg_DOBDO_UNCONNECTED ;
  wire [1:0]\NLW_BRAM_GEN[0].BYTE_BRAM_GEN[0].byte_ram_reg_DOPADOP_UNCONNECTED ;
  wire [1:0]\NLW_BRAM_GEN[0].BYTE_BRAM_GEN[0].byte_ram_reg_DOPBDOP_UNCONNECTED ;
  wire [15:8]\NLW_BRAM_GEN[0].BYTE_BRAM_GEN[1].byte_ram_reg_DOADO_UNCONNECTED ;
  wire [15:8]\NLW_BRAM_GEN[0].BYTE_BRAM_GEN[1].byte_ram_reg_DOBDO_UNCONNECTED ;
  wire [1:0]\NLW_BRAM_GEN[0].BYTE_BRAM_GEN[1].byte_ram_reg_DOPADOP_UNCONNECTED ;
  wire [1:0]\NLW_BRAM_GEN[0].BYTE_BRAM_GEN[1].byte_ram_reg_DOPBDOP_UNCONNECTED ;
  wire [15:8]\NLW_BRAM_GEN[0].BYTE_BRAM_GEN[2].byte_ram_reg_DOADO_UNCONNECTED ;
  wire [15:8]\NLW_BRAM_GEN[0].BYTE_BRAM_GEN[2].byte_ram_reg_DOBDO_UNCONNECTED ;
  wire [1:0]\NLW_BRAM_GEN[0].BYTE_BRAM_GEN[2].byte_ram_reg_DOPADOP_UNCONNECTED ;
  wire [1:0]\NLW_BRAM_GEN[0].BYTE_BRAM_GEN[2].byte_ram_reg_DOPBDOP_UNCONNECTED ;
  wire [15:8]\NLW_BRAM_GEN[0].BYTE_BRAM_GEN[3].byte_ram_reg_DOADO_UNCONNECTED ;
  wire [15:8]\NLW_BRAM_GEN[0].BYTE_BRAM_GEN[3].byte_ram_reg_DOBDO_UNCONNECTED ;
  wire [1:0]\NLW_BRAM_GEN[0].BYTE_BRAM_GEN[3].byte_ram_reg_DOPADOP_UNCONNECTED ;
  wire [1:0]\NLW_BRAM_GEN[0].BYTE_BRAM_GEN[3].byte_ram_reg_DOPBDOP_UNCONNECTED ;
  wire [3:3]NLW_axi_araddr0_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_axi_araddr3_carry_O_UNCONNECTED;
  wire [3:3]NLW_axi_awaddr0_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_axi_awaddr3_carry_O_UNCONNECTED;
  wire [3:2]\NLW_plusOp_inferred__1/i__carry__0_CO_UNCONNECTED ;
  wire [3:3]\NLW_plusOp_inferred__1/i__carry__0_O_UNCONNECTED ;
  wire [3:2]\NLW_plusOp_inferred__2/i__carry__0_CO_UNCONNECTED ;
  wire [3:3]\NLW_plusOp_inferred__2/i__carry__0_O_UNCONNECTED ;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "1024" *) 
  (* RTL_RAM_NAME = "U0/PL_to_PS_S00_AXI_inst/BRAM_GEN[0].BYTE_BRAM_GEN[0].byte_ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "1023" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "7" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(18),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(18)) 
    \BRAM_GEN[0].BYTE_BRAM_GEN[0].byte_ram_reg 
       (.ADDRARDADDR({1'b1,1'b1,1'b1,from_Dispatcher_address,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,mem_address,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(from_Dispatcher_clk),
        .CLKBWRCLK(s00_axi_aclk),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,from_Dispatcher_data_in[7:0]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0}),
        .DOADO({\NLW_BRAM_GEN[0].BYTE_BRAM_GEN[0].byte_ram_reg_DOADO_UNCONNECTED [15:8],DOADO}),
        .DOBDO({\NLW_BRAM_GEN[0].BYTE_BRAM_GEN[0].byte_ram_reg_DOBDO_UNCONNECTED [15:8],\BRAM_GEN[0].BYTE_BRAM_GEN[0].mem_data_out_reg[0] }),
        .DOPADOP(\NLW_BRAM_GEN[0].BYTE_BRAM_GEN[0].byte_ram_reg_DOPADOP_UNCONNECTED [1:0]),
        .DOPBDOP(\NLW_BRAM_GEN[0].BYTE_BRAM_GEN[0].byte_ram_reg_DOPBDOP_UNCONNECTED [1:0]),
        .ENARDEN(1'b1),
        .ENBWREN(axi_arv_arr_flag),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({WEA,WEA}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0}));
  LUT4 #(
    .INIT(16'hB888)) 
    \BRAM_GEN[0].BYTE_BRAM_GEN[0].byte_ram_reg_i_1 
       (.I0(p_1_in[6]),
        .I1(axi_arv_arr_flag),
        .I2(axi_awv_awr_flag),
        .I3(L[8]),
        .O(mem_address[6]));
  LUT4 #(
    .INIT(16'hB888)) 
    \BRAM_GEN[0].BYTE_BRAM_GEN[0].byte_ram_reg_i_2 
       (.I0(p_1_in[5]),
        .I1(axi_arv_arr_flag),
        .I2(axi_awv_awr_flag),
        .I3(L[7]),
        .O(mem_address[5]));
  LUT4 #(
    .INIT(16'hB888)) 
    \BRAM_GEN[0].BYTE_BRAM_GEN[0].byte_ram_reg_i_3 
       (.I0(p_1_in[4]),
        .I1(axi_arv_arr_flag),
        .I2(axi_awv_awr_flag),
        .I3(L[6]),
        .O(mem_address[4]));
  LUT4 #(
    .INIT(16'hB888)) 
    \BRAM_GEN[0].BYTE_BRAM_GEN[0].byte_ram_reg_i_4 
       (.I0(p_1_in[3]),
        .I1(axi_arv_arr_flag),
        .I2(axi_awv_awr_flag),
        .I3(L[5]),
        .O(mem_address[3]));
  LUT4 #(
    .INIT(16'hB888)) 
    \BRAM_GEN[0].BYTE_BRAM_GEN[0].byte_ram_reg_i_5 
       (.I0(p_1_in[2]),
        .I1(axi_arv_arr_flag),
        .I2(axi_awv_awr_flag),
        .I3(L[4]),
        .O(mem_address[2]));
  LUT4 #(
    .INIT(16'hB888)) 
    \BRAM_GEN[0].BYTE_BRAM_GEN[0].byte_ram_reg_i_6 
       (.I0(p_1_in[1]),
        .I1(axi_arv_arr_flag),
        .I2(axi_awv_awr_flag),
        .I3(L[3]),
        .O(mem_address[1]));
  LUT4 #(
    .INIT(16'hB888)) 
    \BRAM_GEN[0].BYTE_BRAM_GEN[0].byte_ram_reg_i_7 
       (.I0(p_1_in[0]),
        .I1(axi_arv_arr_flag),
        .I2(axi_awv_awr_flag),
        .I3(L[2]),
        .O(mem_address[0]));
  LUT5 #(
    .INIT(32'h80000000)) 
    \BRAM_GEN[0].BYTE_BRAM_GEN[0].byte_ram_reg_i_8 
       (.I0(from_Dispatcher_bytes_selection[3]),
        .I1(from_Dispatcher_bytes_selection[1]),
        .I2(from_Dispatcher_bytes_selection[0]),
        .I3(from_Dispatcher_enable),
        .I4(from_Dispatcher_bytes_selection[2]),
        .O(WEA));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "1024" *) 
  (* RTL_RAM_NAME = "U0/PL_to_PS_S00_AXI_inst/BRAM_GEN[0].BYTE_BRAM_GEN[1].byte_ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "1023" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "7" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(18),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(18)) 
    \BRAM_GEN[0].BYTE_BRAM_GEN[1].byte_ram_reg 
       (.ADDRARDADDR({1'b1,1'b1,1'b1,from_Dispatcher_address,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,mem_address,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(from_Dispatcher_clk),
        .CLKBWRCLK(s00_axi_aclk),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,from_Dispatcher_data_in[15:8]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0}),
        .DOADO({\NLW_BRAM_GEN[0].BYTE_BRAM_GEN[1].byte_ram_reg_DOADO_UNCONNECTED [15:8],\to_Dispatcher_data_out[15]_INST_0_i_1 }),
        .DOBDO({\NLW_BRAM_GEN[0].BYTE_BRAM_GEN[1].byte_ram_reg_DOBDO_UNCONNECTED [15:8],\BRAM_GEN[0].BYTE_BRAM_GEN[1].mem_data_out_reg[0] }),
        .DOPADOP(\NLW_BRAM_GEN[0].BYTE_BRAM_GEN[1].byte_ram_reg_DOPADOP_UNCONNECTED [1:0]),
        .DOPBDOP(\NLW_BRAM_GEN[0].BYTE_BRAM_GEN[1].byte_ram_reg_DOPBDOP_UNCONNECTED [1:0]),
        .ENARDEN(1'b1),
        .ENBWREN(axi_arv_arr_flag),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({WEA,WEA}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0}));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "1024" *) 
  (* RTL_RAM_NAME = "U0/PL_to_PS_S00_AXI_inst/BRAM_GEN[0].BYTE_BRAM_GEN[2].byte_ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "1023" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "7" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(18),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(18)) 
    \BRAM_GEN[0].BYTE_BRAM_GEN[2].byte_ram_reg 
       (.ADDRARDADDR({1'b1,1'b1,1'b1,from_Dispatcher_address,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,mem_address,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(from_Dispatcher_clk),
        .CLKBWRCLK(s00_axi_aclk),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,from_Dispatcher_data_in[23:16]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0}),
        .DOADO({\NLW_BRAM_GEN[0].BYTE_BRAM_GEN[2].byte_ram_reg_DOADO_UNCONNECTED [15:8],\to_Dispatcher_data_out[23]_INST_0_i_1 }),
        .DOBDO({\NLW_BRAM_GEN[0].BYTE_BRAM_GEN[2].byte_ram_reg_DOBDO_UNCONNECTED [15:8],\BRAM_GEN[0].BYTE_BRAM_GEN[2].mem_data_out_reg[0] }),
        .DOPADOP(\NLW_BRAM_GEN[0].BYTE_BRAM_GEN[2].byte_ram_reg_DOPADOP_UNCONNECTED [1:0]),
        .DOPBDOP(\NLW_BRAM_GEN[0].BYTE_BRAM_GEN[2].byte_ram_reg_DOPBDOP_UNCONNECTED [1:0]),
        .ENARDEN(1'b1),
        .ENBWREN(axi_arv_arr_flag),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({WEA,WEA}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0}));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "1024" *) 
  (* RTL_RAM_NAME = "U0/PL_to_PS_S00_AXI_inst/BRAM_GEN[0].BYTE_BRAM_GEN[3].byte_ram" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "1023" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "7" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(18),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(18)) 
    \BRAM_GEN[0].BYTE_BRAM_GEN[3].byte_ram_reg 
       (.ADDRARDADDR({1'b1,1'b1,1'b1,from_Dispatcher_address,1'b1,1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,1'b1,1'b1,mem_address,1'b1,1'b1,1'b1,1'b1}),
        .CLKARDCLK(from_Dispatcher_clk),
        .CLKBWRCLK(s00_axi_aclk),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,from_Dispatcher_data_in[31:24]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0}),
        .DOADO({\NLW_BRAM_GEN[0].BYTE_BRAM_GEN[3].byte_ram_reg_DOADO_UNCONNECTED [15:8],\to_Dispatcher_data_out[31]_INST_0_i_2 }),
        .DOBDO({\NLW_BRAM_GEN[0].BYTE_BRAM_GEN[3].byte_ram_reg_DOBDO_UNCONNECTED [15:8],\BRAM_GEN[0].BYTE_BRAM_GEN[3].mem_data_out_reg[0] }),
        .DOPADOP(\NLW_BRAM_GEN[0].BYTE_BRAM_GEN[3].byte_ram_reg_DOPADOP_UNCONNECTED [1:0]),
        .DOPBDOP(\NLW_BRAM_GEN[0].BYTE_BRAM_GEN[3].byte_ram_reg_DOPBDOP_UNCONNECTED [1:0]),
        .ENARDEN(1'b1),
        .ENBWREN(axi_arv_arr_flag),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({WEA,WEA}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0}));
  CARRY4 axi_araddr0_carry
       (.CI(1'b0),
        .CO({axi_araddr0_carry_n_0,axi_araddr0_carry_n_1,axi_araddr0_carry_n_2,axi_araddr0_carry_n_3}),
        .CYINIT(1'b0),
        .DI({axi_araddr0_carry_i_1_n_0,axi_araddr0_carry_i_2_n_0,axi_araddr0_carry_i_3_n_0,1'b0}),
        .O(axi_araddr0[5:2]),
        .S({axi_araddr0_carry_i_4_n_0,axi_araddr0_carry_i_5_n_0,axi_araddr0_carry_i_6_n_0,axi_araddr0_carry_i_7_n_0}));
  CARRY4 axi_araddr0_carry__0
       (.CI(axi_araddr0_carry_n_0),
        .CO({NLW_axi_araddr0_carry__0_CO_UNCONNECTED[3],axi_araddr0_carry__0_n_1,axi_araddr0_carry__0_n_2,axi_araddr0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,axi_araddr0_carry__0_i_1_n_0,axi_araddr0_carry__0_i_2_n_0,axi_araddr0_carry__0_i_3_n_0}),
        .O(axi_araddr0[9:6]),
        .S({axi_araddr0_carry__0_i_4_n_0,axi_araddr0_carry__0_i_5_n_0,axi_araddr0_carry__0_i_6_n_0,axi_araddr0_carry__0_i_7_n_0}));
  LUT2 #(
    .INIT(4'h2)) 
    axi_araddr0_carry__0_i_1
       (.I0(p_1_in[5]),
        .I1(ar_wrap_size__0[7]),
        .O(axi_araddr0_carry__0_i_1_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    axi_araddr0_carry__0_i_2
       (.I0(p_1_in[4]),
        .I1(ar_wrap_size__0[6]),
        .O(axi_araddr0_carry__0_i_2_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    axi_araddr0_carry__0_i_3
       (.I0(p_1_in[3]),
        .I1(ar_wrap_size__0[5]),
        .O(axi_araddr0_carry__0_i_3_n_0));
  LUT4 #(
    .INIT(16'hD22D)) 
    axi_araddr0_carry__0_i_4
       (.I0(p_1_in[6]),
        .I1(ar_wrap_size__0[8]),
        .I2(\axi_araddr_reg_n_0_[9] ),
        .I3(ar_wrap_size__0[9]),
        .O(axi_araddr0_carry__0_i_4_n_0));
  LUT4 #(
    .INIT(16'hB44B)) 
    axi_araddr0_carry__0_i_5
       (.I0(ar_wrap_size__0[7]),
        .I1(p_1_in[5]),
        .I2(ar_wrap_size__0[8]),
        .I3(p_1_in[6]),
        .O(axi_araddr0_carry__0_i_5_n_0));
  LUT4 #(
    .INIT(16'hB44B)) 
    axi_araddr0_carry__0_i_6
       (.I0(ar_wrap_size__0[6]),
        .I1(p_1_in[4]),
        .I2(ar_wrap_size__0[7]),
        .I3(p_1_in[5]),
        .O(axi_araddr0_carry__0_i_6_n_0));
  LUT4 #(
    .INIT(16'hB44B)) 
    axi_araddr0_carry__0_i_7
       (.I0(ar_wrap_size__0[5]),
        .I1(p_1_in[3]),
        .I2(ar_wrap_size__0[6]),
        .I3(p_1_in[4]),
        .O(axi_araddr0_carry__0_i_7_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    axi_araddr0_carry_i_1
       (.I0(p_1_in[2]),
        .I1(ar_wrap_size__0[4]),
        .O(axi_araddr0_carry_i_1_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    axi_araddr0_carry_i_2
       (.I0(p_1_in[1]),
        .I1(ar_wrap_size__0[3]),
        .O(axi_araddr0_carry_i_2_n_0));
  LUT2 #(
    .INIT(4'hB)) 
    axi_araddr0_carry_i_3
       (.I0(p_1_in[0]),
        .I1(ar_wrap_size__0[2]),
        .O(axi_araddr0_carry_i_3_n_0));
  LUT4 #(
    .INIT(16'hB44B)) 
    axi_araddr0_carry_i_4
       (.I0(ar_wrap_size__0[4]),
        .I1(p_1_in[2]),
        .I2(ar_wrap_size__0[5]),
        .I3(p_1_in[3]),
        .O(axi_araddr0_carry_i_4_n_0));
  LUT4 #(
    .INIT(16'hB44B)) 
    axi_araddr0_carry_i_5
       (.I0(ar_wrap_size__0[3]),
        .I1(p_1_in[1]),
        .I2(ar_wrap_size__0[4]),
        .I3(p_1_in[2]),
        .O(axi_araddr0_carry_i_5_n_0));
  LUT4 #(
    .INIT(16'h4BB4)) 
    axi_araddr0_carry_i_6
       (.I0(p_1_in[0]),
        .I1(ar_wrap_size__0[2]),
        .I2(ar_wrap_size__0[3]),
        .I3(p_1_in[1]),
        .O(axi_araddr0_carry_i_6_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    axi_araddr0_carry_i_7
       (.I0(ar_wrap_size__0[2]),
        .I1(p_1_in[0]),
        .O(axi_araddr0_carry_i_7_n_0));
  CARRY4 axi_araddr3_carry
       (.CI(1'b0),
        .CO({axi_araddr3,axi_araddr3_carry_n_1,axi_araddr3_carry_n_2,axi_araddr3_carry_n_3}),
        .CYINIT(1'b1),
        .DI({axi_araddr3_carry_i_1_n_0,axi_araddr3_carry_i_2_n_0,axi_araddr3_carry_i_3_n_0,axi_araddr3_carry_i_4_n_0}),
        .O(NLW_axi_araddr3_carry_O_UNCONNECTED[3:0]),
        .S({axi_araddr3_carry_i_5_n_0,axi_araddr3_carry_i_6_n_0,axi_araddr3_carry_i_7_n_0,axi_araddr3_carry_i_8_n_0}));
  LUT4 #(
    .INIT(16'h44D4)) 
    axi_araddr3_carry_i_1
       (.I0(axi_arlen_cntr_reg__0[7]),
        .I1(ar_wrap_size__0[9]),
        .I2(ar_wrap_size__0[8]),
        .I3(axi_arlen_cntr_reg__0[6]),
        .O(axi_araddr3_carry_i_1_n_0));
  LUT4 #(
    .INIT(16'h44D4)) 
    axi_araddr3_carry_i_2
       (.I0(axi_arlen_cntr_reg__0[5]),
        .I1(ar_wrap_size__0[7]),
        .I2(ar_wrap_size__0[6]),
        .I3(axi_arlen_cntr_reg__0[4]),
        .O(axi_araddr3_carry_i_2_n_0));
  LUT4 #(
    .INIT(16'h44D4)) 
    axi_araddr3_carry_i_3
       (.I0(axi_arlen_cntr_reg__0[3]),
        .I1(ar_wrap_size__0[5]),
        .I2(ar_wrap_size__0[4]),
        .I3(axi_arlen_cntr_reg__0[2]),
        .O(axi_araddr3_carry_i_3_n_0));
  LUT4 #(
    .INIT(16'h44D4)) 
    axi_araddr3_carry_i_4
       (.I0(axi_arlen_cntr_reg__0[1]),
        .I1(ar_wrap_size__0[3]),
        .I2(ar_wrap_size__0[2]),
        .I3(axi_arlen_cntr_reg__0[0]),
        .O(axi_araddr3_carry_i_4_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    axi_araddr3_carry_i_5
       (.I0(axi_arlen_cntr_reg__0[6]),
        .I1(ar_wrap_size__0[8]),
        .I2(axi_arlen_cntr_reg__0[7]),
        .I3(ar_wrap_size__0[9]),
        .O(axi_araddr3_carry_i_5_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    axi_araddr3_carry_i_6
       (.I0(axi_arlen_cntr_reg__0[4]),
        .I1(ar_wrap_size__0[6]),
        .I2(axi_arlen_cntr_reg__0[5]),
        .I3(ar_wrap_size__0[7]),
        .O(axi_araddr3_carry_i_6_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    axi_araddr3_carry_i_7
       (.I0(axi_arlen_cntr_reg__0[2]),
        .I1(ar_wrap_size__0[4]),
        .I2(axi_arlen_cntr_reg__0[3]),
        .I3(ar_wrap_size__0[5]),
        .O(axi_araddr3_carry_i_7_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    axi_araddr3_carry_i_8
       (.I0(ar_wrap_size__0[3]),
        .I1(axi_arlen_cntr_reg__0[1]),
        .I2(ar_wrap_size__0[2]),
        .I3(axi_arlen_cntr_reg__0[0]),
        .O(axi_araddr3_carry_i_8_n_0));
  LUT5 #(
    .INIT(32'h8BBB8B88)) 
    \axi_araddr[2]_i_1 
       (.I0(s00_axi_araddr[0]),
        .I1(\axi_arlen[7]_i_1_n_0 ),
        .I2(p_1_in[0]),
        .I3(\axi_araddr[9]_i_3_n_0 ),
        .I4(axi_araddr0[2]),
        .O(\axi_araddr[2]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_araddr[3]_i_1 
       (.I0(s00_axi_araddr[1]),
        .I1(\axi_arlen[7]_i_1_n_0 ),
        .I2(\plusOp_inferred__2/i__carry_n_7 ),
        .I3(\axi_araddr[9]_i_3_n_0 ),
        .I4(axi_araddr0[3]),
        .O(\axi_araddr[3]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_araddr[4]_i_1 
       (.I0(s00_axi_araddr[2]),
        .I1(\axi_arlen[7]_i_1_n_0 ),
        .I2(\plusOp_inferred__2/i__carry_n_6 ),
        .I3(\axi_araddr[9]_i_3_n_0 ),
        .I4(axi_araddr0[4]),
        .O(\axi_araddr[4]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_araddr[5]_i_1 
       (.I0(s00_axi_araddr[3]),
        .I1(\axi_arlen[7]_i_1_n_0 ),
        .I2(\plusOp_inferred__2/i__carry_n_5 ),
        .I3(\axi_araddr[9]_i_3_n_0 ),
        .I4(axi_araddr0[5]),
        .O(\axi_araddr[5]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_araddr[6]_i_1 
       (.I0(s00_axi_araddr[4]),
        .I1(\axi_arlen[7]_i_1_n_0 ),
        .I2(\plusOp_inferred__2/i__carry_n_4 ),
        .I3(\axi_araddr[9]_i_3_n_0 ),
        .I4(axi_araddr0[6]),
        .O(\axi_araddr[6]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_araddr[7]_i_1 
       (.I0(s00_axi_araddr[5]),
        .I1(\axi_arlen[7]_i_1_n_0 ),
        .I2(\plusOp_inferred__2/i__carry__0_n_7 ),
        .I3(\axi_araddr[9]_i_3_n_0 ),
        .I4(axi_araddr0[7]),
        .O(\axi_araddr[7]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_araddr[8]_i_1 
       (.I0(s00_axi_araddr[6]),
        .I1(\axi_arlen[7]_i_1_n_0 ),
        .I2(\plusOp_inferred__2/i__carry__0_n_6 ),
        .I3(\axi_araddr[9]_i_3_n_0 ),
        .I4(axi_araddr0[8]),
        .O(\axi_araddr[8]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFEAAAAAAAAAAAAAA)) 
    \axi_araddr[9]_i_1 
       (.I0(\axi_arlen[7]_i_1_n_0 ),
        .I1(axi_arburst[1]),
        .I2(axi_arburst[0]),
        .I3(axi_araddr3),
        .I4(s00_axi_rready),
        .I5(s00_axi_rvalid),
        .O(\axi_araddr[9]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_araddr[9]_i_2 
       (.I0(s00_axi_araddr[7]),
        .I1(\axi_arlen[7]_i_1_n_0 ),
        .I2(\plusOp_inferred__2/i__carry__0_n_5 ),
        .I3(\axi_araddr[9]_i_3_n_0 ),
        .I4(axi_araddr0[9]),
        .O(\axi_araddr[9]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFEFFFFFFFEFFFE)) 
    \axi_araddr[9]_i_3 
       (.I0(\axi_araddr[9]_i_4_n_0 ),
        .I1(\axi_araddr[9]_i_5_n_0 ),
        .I2(\axi_araddr[9]_i_6_n_0 ),
        .I3(axi_arburst[0]),
        .I4(p_1_in[4]),
        .I5(ar_wrap_size__0[6]),
        .O(\axi_araddr[9]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h44F444F4FFFF44F4)) 
    \axi_araddr[9]_i_4 
       (.I0(p_1_in[0]),
        .I1(ar_wrap_size__0[2]),
        .I2(ar_wrap_size__0[8]),
        .I3(p_1_in[6]),
        .I4(ar_wrap_size__0[7]),
        .I5(p_1_in[5]),
        .O(\axi_araddr[9]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h4F44)) 
    \axi_araddr[9]_i_5 
       (.I0(p_1_in[1]),
        .I1(ar_wrap_size__0[3]),
        .I2(p_1_in[2]),
        .I3(ar_wrap_size__0[4]),
        .O(\axi_araddr[9]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'h4F44)) 
    \axi_araddr[9]_i_6 
       (.I0(p_1_in[3]),
        .I1(ar_wrap_size__0[5]),
        .I2(\axi_araddr_reg_n_0_[9] ),
        .I3(ar_wrap_size__0[9]),
        .O(\axi_araddr[9]_i_6_n_0 ));
  FDRE \axi_araddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\axi_araddr[9]_i_1_n_0 ),
        .D(\axi_araddr[2]_i_1_n_0 ),
        .Q(p_1_in[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_araddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\axi_araddr[9]_i_1_n_0 ),
        .D(\axi_araddr[3]_i_1_n_0 ),
        .Q(p_1_in[1]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_araddr_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\axi_araddr[9]_i_1_n_0 ),
        .D(\axi_araddr[4]_i_1_n_0 ),
        .Q(p_1_in[2]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_araddr_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\axi_araddr[9]_i_1_n_0 ),
        .D(\axi_araddr[5]_i_1_n_0 ),
        .Q(p_1_in[3]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_araddr_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\axi_araddr[9]_i_1_n_0 ),
        .D(\axi_araddr[6]_i_1_n_0 ),
        .Q(p_1_in[4]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_araddr_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\axi_araddr[9]_i_1_n_0 ),
        .D(\axi_araddr[7]_i_1_n_0 ),
        .Q(p_1_in[5]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_araddr_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\axi_araddr[9]_i_1_n_0 ),
        .D(\axi_araddr[8]_i_1_n_0 ),
        .Q(p_1_in[6]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_araddr_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\axi_araddr[9]_i_1_n_0 ),
        .D(\axi_araddr[9]_i_2_n_0 ),
        .Q(\axi_araddr_reg_n_0_[9] ),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_arburst_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\axi_arlen[7]_i_1_n_0 ),
        .D(s00_axi_arburst[0]),
        .Q(axi_arburst[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_arburst_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\axi_arlen[7]_i_1_n_0 ),
        .D(s00_axi_arburst[1]),
        .Q(axi_arburst[1]),
        .R(axi_awready_i_1_n_0));
  LUT3 #(
    .INIT(8'h04)) 
    \axi_arlen[7]_i_1 
       (.I0(axi_arv_arr_flag),
        .I1(s00_axi_arvalid),
        .I2(s00_axi_arready),
        .O(\axi_arlen[7]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \axi_arlen_cntr[0]_i_1 
       (.I0(axi_arlen_cntr_reg__0[0]),
        .O(plusOp[0]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \axi_arlen_cntr[1]_i_1 
       (.I0(axi_arlen_cntr_reg__0[1]),
        .I1(axi_arlen_cntr_reg__0[0]),
        .O(plusOp[1]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \axi_arlen_cntr[2]_i_1 
       (.I0(axi_arlen_cntr_reg__0[2]),
        .I1(axi_arlen_cntr_reg__0[1]),
        .I2(axi_arlen_cntr_reg__0[0]),
        .O(plusOp[2]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \axi_arlen_cntr[3]_i_1 
       (.I0(axi_arlen_cntr_reg__0[0]),
        .I1(axi_arlen_cntr_reg__0[1]),
        .I2(axi_arlen_cntr_reg__0[2]),
        .I3(axi_arlen_cntr_reg__0[3]),
        .O(plusOp[3]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \axi_arlen_cntr[4]_i_1 
       (.I0(axi_arlen_cntr_reg__0[4]),
        .I1(axi_arlen_cntr_reg__0[0]),
        .I2(axi_arlen_cntr_reg__0[1]),
        .I3(axi_arlen_cntr_reg__0[2]),
        .I4(axi_arlen_cntr_reg__0[3]),
        .O(plusOp[4]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \axi_arlen_cntr[5]_i_1 
       (.I0(axi_arlen_cntr_reg__0[5]),
        .I1(axi_arlen_cntr_reg__0[3]),
        .I2(axi_arlen_cntr_reg__0[2]),
        .I3(axi_arlen_cntr_reg__0[1]),
        .I4(axi_arlen_cntr_reg__0[0]),
        .I5(axi_arlen_cntr_reg__0[4]),
        .O(plusOp[5]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \axi_arlen_cntr[6]_i_1 
       (.I0(axi_arlen_cntr_reg__0[6]),
        .I1(axi_arlen_cntr_reg__0[4]),
        .I2(\axi_arlen_cntr[7]_i_4_n_0 ),
        .I3(axi_arlen_cntr_reg__0[5]),
        .O(plusOp[6]));
  LUT4 #(
    .INIT(16'h04FF)) 
    \axi_arlen_cntr[7]_i_1 
       (.I0(axi_arv_arr_flag),
        .I1(s00_axi_arvalid),
        .I2(s00_axi_arready),
        .I3(s00_axi_aresetn),
        .O(\axi_arlen_cntr[7]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h80)) 
    \axi_arlen_cntr[7]_i_2 
       (.I0(axi_araddr3),
        .I1(s00_axi_rready),
        .I2(s00_axi_rvalid),
        .O(axi_araddr1));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \axi_arlen_cntr[7]_i_3 
       (.I0(axi_arlen_cntr_reg__0[7]),
        .I1(axi_arlen_cntr_reg__0[5]),
        .I2(\axi_arlen_cntr[7]_i_4_n_0 ),
        .I3(axi_arlen_cntr_reg__0[4]),
        .I4(axi_arlen_cntr_reg__0[6]),
        .O(plusOp[7]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \axi_arlen_cntr[7]_i_4 
       (.I0(axi_arlen_cntr_reg__0[3]),
        .I1(axi_arlen_cntr_reg__0[2]),
        .I2(axi_arlen_cntr_reg__0[1]),
        .I3(axi_arlen_cntr_reg__0[0]),
        .O(\axi_arlen_cntr[7]_i_4_n_0 ));
  FDRE \axi_arlen_cntr_reg[0] 
       (.C(s00_axi_aclk),
        .CE(axi_araddr1),
        .D(plusOp[0]),
        .Q(axi_arlen_cntr_reg__0[0]),
        .R(\axi_arlen_cntr[7]_i_1_n_0 ));
  FDRE \axi_arlen_cntr_reg[1] 
       (.C(s00_axi_aclk),
        .CE(axi_araddr1),
        .D(plusOp[1]),
        .Q(axi_arlen_cntr_reg__0[1]),
        .R(\axi_arlen_cntr[7]_i_1_n_0 ));
  FDRE \axi_arlen_cntr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(axi_araddr1),
        .D(plusOp[2]),
        .Q(axi_arlen_cntr_reg__0[2]),
        .R(\axi_arlen_cntr[7]_i_1_n_0 ));
  FDRE \axi_arlen_cntr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(axi_araddr1),
        .D(plusOp[3]),
        .Q(axi_arlen_cntr_reg__0[3]),
        .R(\axi_arlen_cntr[7]_i_1_n_0 ));
  FDRE \axi_arlen_cntr_reg[4] 
       (.C(s00_axi_aclk),
        .CE(axi_araddr1),
        .D(plusOp[4]),
        .Q(axi_arlen_cntr_reg__0[4]),
        .R(\axi_arlen_cntr[7]_i_1_n_0 ));
  FDRE \axi_arlen_cntr_reg[5] 
       (.C(s00_axi_aclk),
        .CE(axi_araddr1),
        .D(plusOp[5]),
        .Q(axi_arlen_cntr_reg__0[5]),
        .R(\axi_arlen_cntr[7]_i_1_n_0 ));
  FDRE \axi_arlen_cntr_reg[6] 
       (.C(s00_axi_aclk),
        .CE(axi_araddr1),
        .D(plusOp[6]),
        .Q(axi_arlen_cntr_reg__0[6]),
        .R(\axi_arlen_cntr[7]_i_1_n_0 ));
  FDRE \axi_arlen_cntr_reg[7] 
       (.C(s00_axi_aclk),
        .CE(axi_araddr1),
        .D(plusOp[7]),
        .Q(axi_arlen_cntr_reg__0[7]),
        .R(\axi_arlen_cntr[7]_i_1_n_0 ));
  FDRE \axi_arlen_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\axi_arlen[7]_i_1_n_0 ),
        .D(s00_axi_arlen[0]),
        .Q(ar_wrap_size__0[2]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_arlen_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\axi_arlen[7]_i_1_n_0 ),
        .D(s00_axi_arlen[1]),
        .Q(ar_wrap_size__0[3]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_arlen_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\axi_arlen[7]_i_1_n_0 ),
        .D(s00_axi_arlen[2]),
        .Q(ar_wrap_size__0[4]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_arlen_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\axi_arlen[7]_i_1_n_0 ),
        .D(s00_axi_arlen[3]),
        .Q(ar_wrap_size__0[5]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_arlen_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\axi_arlen[7]_i_1_n_0 ),
        .D(s00_axi_arlen[4]),
        .Q(ar_wrap_size__0[6]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_arlen_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\axi_arlen[7]_i_1_n_0 ),
        .D(s00_axi_arlen[5]),
        .Q(ar_wrap_size__0[7]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_arlen_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\axi_arlen[7]_i_1_n_0 ),
        .D(s00_axi_arlen[6]),
        .Q(ar_wrap_size__0[8]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_arlen_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\axi_arlen[7]_i_1_n_0 ),
        .D(s00_axi_arlen[7]),
        .Q(ar_wrap_size__0[9]),
        .R(axi_awready_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT5 #(
    .INIT(32'hFF100010)) 
    axi_arready_i_1__0
       (.I0(axi_awv_awr_flag),
        .I1(axi_arv_arr_flag),
        .I2(s00_axi_arvalid),
        .I3(s00_axi_arready),
        .I4(axi_arready_i_2_n_0),
        .O(axi_arready_i_1__0_n_0));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    axi_arready_i_2
       (.I0(s00_axi_rready),
        .I1(s00_axi_rvalid),
        .I2(axi_arready_i_3_n_0),
        .I3(axi_arready_i_4_n_0),
        .I4(axi_arready_i_5_n_0),
        .I5(axi_arready_i_6_n_0),
        .O(axi_arready_i_2_n_0));
  LUT4 #(
    .INIT(16'h6FF6)) 
    axi_arready_i_3
       (.I0(ar_wrap_size__0[5]),
        .I1(axi_arlen_cntr_reg__0[3]),
        .I2(ar_wrap_size__0[4]),
        .I3(axi_arlen_cntr_reg__0[2]),
        .O(axi_arready_i_3_n_0));
  LUT4 #(
    .INIT(16'h6FF6)) 
    axi_arready_i_4
       (.I0(ar_wrap_size__0[9]),
        .I1(axi_arlen_cntr_reg__0[7]),
        .I2(ar_wrap_size__0[8]),
        .I3(axi_arlen_cntr_reg__0[6]),
        .O(axi_arready_i_4_n_0));
  LUT4 #(
    .INIT(16'h6FF6)) 
    axi_arready_i_5
       (.I0(ar_wrap_size__0[7]),
        .I1(axi_arlen_cntr_reg__0[5]),
        .I2(ar_wrap_size__0[6]),
        .I3(axi_arlen_cntr_reg__0[4]),
        .O(axi_arready_i_5_n_0));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT4 #(
    .INIT(16'h9009)) 
    axi_arready_i_6
       (.I0(ar_wrap_size__0[3]),
        .I1(axi_arlen_cntr_reg__0[1]),
        .I2(ar_wrap_size__0[2]),
        .I3(axi_arlen_cntr_reg__0[0]),
        .O(axi_arready_i_6_n_0));
  FDRE axi_arready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_arready_i_1__0_n_0),
        .Q(s00_axi_arready),
        .R(axi_awready_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT5 #(
    .INIT(32'h50505350)) 
    axi_arv_arr_flag_i_1
       (.I0(axi_arready_i_2_n_0),
        .I1(axi_awv_awr_flag),
        .I2(axi_arv_arr_flag),
        .I3(s00_axi_arvalid),
        .I4(s00_axi_arready),
        .O(axi_arv_arr_flag_i_1_n_0));
  FDRE axi_arv_arr_flag_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_arv_arr_flag_i_1_n_0),
        .Q(axi_arv_arr_flag),
        .R(axi_awready_i_1_n_0));
  CARRY4 axi_awaddr0_carry
       (.CI(1'b0),
        .CO({axi_awaddr0_carry_n_0,axi_awaddr0_carry_n_1,axi_awaddr0_carry_n_2,axi_awaddr0_carry_n_3}),
        .CYINIT(1'b0),
        .DI({axi_awaddr0_carry_i_1_n_0,axi_awaddr0_carry_i_2_n_0,axi_awaddr0_carry_i_3_n_0,1'b0}),
        .O(axi_awaddr0[5:2]),
        .S({axi_awaddr0_carry_i_4_n_0,axi_awaddr0_carry_i_5_n_0,axi_awaddr0_carry_i_6_n_0,axi_awaddr0_carry_i_7_n_0}));
  CARRY4 axi_awaddr0_carry__0
       (.CI(axi_awaddr0_carry_n_0),
        .CO({NLW_axi_awaddr0_carry__0_CO_UNCONNECTED[3],axi_awaddr0_carry__0_n_1,axi_awaddr0_carry__0_n_2,axi_awaddr0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,axi_awaddr0_carry__0_i_1_n_0,axi_awaddr0_carry__0_i_2_n_0,axi_awaddr0_carry__0_i_3_n_0}),
        .O(axi_awaddr0[9:6]),
        .S({axi_awaddr0_carry__0_i_4_n_0,axi_awaddr0_carry__0_i_5_n_0,axi_awaddr0_carry__0_i_6_n_0,axi_awaddr0_carry__0_i_7_n_0}));
  LUT2 #(
    .INIT(4'h2)) 
    axi_awaddr0_carry__0_i_1
       (.I0(L[7]),
        .I1(aw_wrap_size__0[7]),
        .O(axi_awaddr0_carry__0_i_1_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    axi_awaddr0_carry__0_i_2
       (.I0(L[6]),
        .I1(aw_wrap_size__0[6]),
        .O(axi_awaddr0_carry__0_i_2_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    axi_awaddr0_carry__0_i_3
       (.I0(L[5]),
        .I1(aw_wrap_size__0[5]),
        .O(axi_awaddr0_carry__0_i_3_n_0));
  LUT4 #(
    .INIT(16'hD22D)) 
    axi_awaddr0_carry__0_i_4
       (.I0(L[8]),
        .I1(aw_wrap_size__0[8]),
        .I2(L[9]),
        .I3(aw_wrap_size__0[9]),
        .O(axi_awaddr0_carry__0_i_4_n_0));
  LUT4 #(
    .INIT(16'hB44B)) 
    axi_awaddr0_carry__0_i_5
       (.I0(aw_wrap_size__0[7]),
        .I1(L[7]),
        .I2(aw_wrap_size__0[8]),
        .I3(L[8]),
        .O(axi_awaddr0_carry__0_i_5_n_0));
  LUT4 #(
    .INIT(16'hB44B)) 
    axi_awaddr0_carry__0_i_6
       (.I0(aw_wrap_size__0[6]),
        .I1(L[6]),
        .I2(aw_wrap_size__0[7]),
        .I3(L[7]),
        .O(axi_awaddr0_carry__0_i_6_n_0));
  LUT4 #(
    .INIT(16'hB44B)) 
    axi_awaddr0_carry__0_i_7
       (.I0(aw_wrap_size__0[5]),
        .I1(L[5]),
        .I2(aw_wrap_size__0[6]),
        .I3(L[6]),
        .O(axi_awaddr0_carry__0_i_7_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    axi_awaddr0_carry_i_1
       (.I0(L[4]),
        .I1(aw_wrap_size__0[4]),
        .O(axi_awaddr0_carry_i_1_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    axi_awaddr0_carry_i_2
       (.I0(L[3]),
        .I1(aw_wrap_size__0[3]),
        .O(axi_awaddr0_carry_i_2_n_0));
  LUT2 #(
    .INIT(4'hB)) 
    axi_awaddr0_carry_i_3
       (.I0(L[2]),
        .I1(aw_wrap_size__0[2]),
        .O(axi_awaddr0_carry_i_3_n_0));
  LUT4 #(
    .INIT(16'hB44B)) 
    axi_awaddr0_carry_i_4
       (.I0(aw_wrap_size__0[4]),
        .I1(L[4]),
        .I2(aw_wrap_size__0[5]),
        .I3(L[5]),
        .O(axi_awaddr0_carry_i_4_n_0));
  LUT4 #(
    .INIT(16'hB44B)) 
    axi_awaddr0_carry_i_5
       (.I0(aw_wrap_size__0[3]),
        .I1(L[3]),
        .I2(aw_wrap_size__0[4]),
        .I3(L[4]),
        .O(axi_awaddr0_carry_i_5_n_0));
  LUT4 #(
    .INIT(16'h4BB4)) 
    axi_awaddr0_carry_i_6
       (.I0(L[2]),
        .I1(aw_wrap_size__0[2]),
        .I2(aw_wrap_size__0[3]),
        .I3(L[3]),
        .O(axi_awaddr0_carry_i_6_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    axi_awaddr0_carry_i_7
       (.I0(aw_wrap_size__0[2]),
        .I1(L[2]),
        .O(axi_awaddr0_carry_i_7_n_0));
  CARRY4 axi_awaddr3_carry
       (.CI(1'b0),
        .CO({axi_awaddr3,axi_awaddr3_carry_n_1,axi_awaddr3_carry_n_2,axi_awaddr3_carry_n_3}),
        .CYINIT(1'b1),
        .DI({axi_awaddr3_carry_i_1_n_0,axi_awaddr3_carry_i_2_n_0,axi_awaddr3_carry_i_3_n_0,axi_awaddr3_carry_i_4_n_0}),
        .O(NLW_axi_awaddr3_carry_O_UNCONNECTED[3:0]),
        .S({axi_awaddr3_carry_i_5_n_0,axi_awaddr3_carry_i_6_n_0,axi_awaddr3_carry_i_7_n_0,axi_awaddr3_carry_i_8_n_0}));
  LUT4 #(
    .INIT(16'h44D4)) 
    axi_awaddr3_carry_i_1
       (.I0(axi_awlen_cntr_reg__0[7]),
        .I1(aw_wrap_size__0[9]),
        .I2(aw_wrap_size__0[8]),
        .I3(axi_awlen_cntr_reg__0[6]),
        .O(axi_awaddr3_carry_i_1_n_0));
  LUT4 #(
    .INIT(16'h44D4)) 
    axi_awaddr3_carry_i_2
       (.I0(axi_awlen_cntr_reg__0[5]),
        .I1(aw_wrap_size__0[7]),
        .I2(aw_wrap_size__0[6]),
        .I3(axi_awlen_cntr_reg__0[4]),
        .O(axi_awaddr3_carry_i_2_n_0));
  LUT4 #(
    .INIT(16'h44D4)) 
    axi_awaddr3_carry_i_3
       (.I0(axi_awlen_cntr_reg__0[3]),
        .I1(aw_wrap_size__0[5]),
        .I2(aw_wrap_size__0[4]),
        .I3(axi_awlen_cntr_reg__0[2]),
        .O(axi_awaddr3_carry_i_3_n_0));
  LUT4 #(
    .INIT(16'h44D4)) 
    axi_awaddr3_carry_i_4
       (.I0(axi_awlen_cntr_reg__0[1]),
        .I1(aw_wrap_size__0[3]),
        .I2(aw_wrap_size__0[2]),
        .I3(axi_awlen_cntr_reg__0[0]),
        .O(axi_awaddr3_carry_i_4_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    axi_awaddr3_carry_i_5
       (.I0(aw_wrap_size__0[9]),
        .I1(axi_awlen_cntr_reg__0[7]),
        .I2(aw_wrap_size__0[8]),
        .I3(axi_awlen_cntr_reg__0[6]),
        .O(axi_awaddr3_carry_i_5_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    axi_awaddr3_carry_i_6
       (.I0(aw_wrap_size__0[7]),
        .I1(axi_awlen_cntr_reg__0[5]),
        .I2(aw_wrap_size__0[6]),
        .I3(axi_awlen_cntr_reg__0[4]),
        .O(axi_awaddr3_carry_i_6_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    axi_awaddr3_carry_i_7
       (.I0(aw_wrap_size__0[5]),
        .I1(axi_awlen_cntr_reg__0[3]),
        .I2(aw_wrap_size__0[4]),
        .I3(axi_awlen_cntr_reg__0[2]),
        .O(axi_awaddr3_carry_i_7_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    axi_awaddr3_carry_i_8
       (.I0(aw_wrap_size__0[3]),
        .I1(axi_awlen_cntr_reg__0[1]),
        .I2(aw_wrap_size__0[2]),
        .I3(axi_awlen_cntr_reg__0[0]),
        .O(axi_awaddr3_carry_i_8_n_0));
  LUT5 #(
    .INIT(32'h74FF7400)) 
    \axi_awaddr[2]_i_1 
       (.I0(L[2]),
        .I1(\axi_awaddr[9]_i_4_n_0 ),
        .I2(axi_awaddr0[2]),
        .I3(\axi_awaddr[9]_i_3_n_0 ),
        .I4(s00_axi_awaddr[0]),
        .O(p_2_in[2]));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_awaddr[3]_i_1 
       (.I0(p_0_in__0[3]),
        .I1(\axi_awaddr[9]_i_4_n_0 ),
        .I2(axi_awaddr0[3]),
        .I3(\axi_awaddr[9]_i_3_n_0 ),
        .I4(s00_axi_awaddr[1]),
        .O(p_2_in[3]));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_awaddr[4]_i_1 
       (.I0(p_0_in__0[4]),
        .I1(\axi_awaddr[9]_i_4_n_0 ),
        .I2(axi_awaddr0[4]),
        .I3(\axi_awaddr[9]_i_3_n_0 ),
        .I4(s00_axi_awaddr[2]),
        .O(p_2_in[4]));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_awaddr[5]_i_1 
       (.I0(p_0_in__0[5]),
        .I1(\axi_awaddr[9]_i_4_n_0 ),
        .I2(axi_awaddr0[5]),
        .I3(\axi_awaddr[9]_i_3_n_0 ),
        .I4(s00_axi_awaddr[3]),
        .O(p_2_in[5]));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_awaddr[6]_i_1 
       (.I0(p_0_in__0[6]),
        .I1(\axi_awaddr[9]_i_4_n_0 ),
        .I2(axi_awaddr0[6]),
        .I3(\axi_awaddr[9]_i_3_n_0 ),
        .I4(s00_axi_awaddr[4]),
        .O(p_2_in[6]));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_awaddr[7]_i_1 
       (.I0(p_0_in__0[7]),
        .I1(\axi_awaddr[9]_i_4_n_0 ),
        .I2(axi_awaddr0[7]),
        .I3(\axi_awaddr[9]_i_3_n_0 ),
        .I4(s00_axi_awaddr[5]),
        .O(p_2_in[7]));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_awaddr[8]_i_1 
       (.I0(p_0_in__0[8]),
        .I1(\axi_awaddr[9]_i_4_n_0 ),
        .I2(axi_awaddr0[8]),
        .I3(\axi_awaddr[9]_i_3_n_0 ),
        .I4(s00_axi_awaddr[6]),
        .O(p_2_in[8]));
  LUT6 #(
    .INIT(64'hE0000000FFFFFFFF)) 
    \axi_awaddr[9]_i_1 
       (.I0(axi_awburst[1]),
        .I1(axi_awburst[0]),
        .I2(axi_awaddr3),
        .I3(s00_axi_wready),
        .I4(s00_axi_wvalid),
        .I5(\axi_awaddr[9]_i_3_n_0 ),
        .O(\axi_awaddr[9]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \axi_awaddr[9]_i_2 
       (.I0(p_0_in__0[9]),
        .I1(\axi_awaddr[9]_i_4_n_0 ),
        .I2(axi_awaddr0[9]),
        .I3(\axi_awaddr[9]_i_3_n_0 ),
        .I4(s00_axi_awaddr[7]),
        .O(p_2_in[9]));
  LUT3 #(
    .INIT(8'hFB)) 
    \axi_awaddr[9]_i_3 
       (.I0(axi_awv_awr_flag),
        .I1(s00_axi_awvalid),
        .I2(s00_axi_awready),
        .O(\axi_awaddr[9]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFEFFFFFFFEFFFE)) 
    \axi_awaddr[9]_i_4 
       (.I0(\axi_awaddr[9]_i_5_n_0 ),
        .I1(\axi_awaddr[9]_i_6_n_0 ),
        .I2(\axi_awaddr[9]_i_7_n_0 ),
        .I3(axi_awburst[0]),
        .I4(L[6]),
        .I5(aw_wrap_size__0[6]),
        .O(\axi_awaddr[9]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h44F444F4FFFF44F4)) 
    \axi_awaddr[9]_i_5 
       (.I0(L[2]),
        .I1(aw_wrap_size__0[2]),
        .I2(aw_wrap_size__0[8]),
        .I3(L[8]),
        .I4(aw_wrap_size__0[7]),
        .I5(L[7]),
        .O(\axi_awaddr[9]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'h4F44)) 
    \axi_awaddr[9]_i_6 
       (.I0(L[3]),
        .I1(aw_wrap_size__0[3]),
        .I2(L[4]),
        .I3(aw_wrap_size__0[4]),
        .O(\axi_awaddr[9]_i_6_n_0 ));
  LUT4 #(
    .INIT(16'h4F44)) 
    \axi_awaddr[9]_i_7 
       (.I0(L[5]),
        .I1(aw_wrap_size__0[5]),
        .I2(L[9]),
        .I3(aw_wrap_size__0[9]),
        .O(\axi_awaddr[9]_i_7_n_0 ));
  FDRE \axi_awaddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\axi_awaddr[9]_i_1_n_0 ),
        .D(p_2_in[2]),
        .Q(L[2]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_awaddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\axi_awaddr[9]_i_1_n_0 ),
        .D(p_2_in[3]),
        .Q(L[3]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_awaddr_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\axi_awaddr[9]_i_1_n_0 ),
        .D(p_2_in[4]),
        .Q(L[4]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_awaddr_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\axi_awaddr[9]_i_1_n_0 ),
        .D(p_2_in[5]),
        .Q(L[5]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_awaddr_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\axi_awaddr[9]_i_1_n_0 ),
        .D(p_2_in[6]),
        .Q(L[6]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_awaddr_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\axi_awaddr[9]_i_1_n_0 ),
        .D(p_2_in[7]),
        .Q(L[7]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_awaddr_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\axi_awaddr[9]_i_1_n_0 ),
        .D(p_2_in[8]),
        .Q(L[8]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_awaddr_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\axi_awaddr[9]_i_1_n_0 ),
        .D(p_2_in[9]),
        .Q(L[9]),
        .R(axi_awready_i_1_n_0));
  LUT3 #(
    .INIT(8'h04)) 
    \axi_awburst[1]_i_1 
       (.I0(s00_axi_awready),
        .I1(s00_axi_awvalid),
        .I2(axi_awv_awr_flag),
        .O(p_9_in));
  FDRE \axi_awburst_reg[0] 
       (.C(s00_axi_aclk),
        .CE(p_9_in),
        .D(s00_axi_awburst[0]),
        .Q(axi_awburst[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_awburst_reg[1] 
       (.C(s00_axi_aclk),
        .CE(p_9_in),
        .D(s00_axi_awburst[1]),
        .Q(axi_awburst[1]),
        .R(axi_awready_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \axi_awlen_cntr[0]_i_1 
       (.I0(axi_awlen_cntr_reg__0[0]),
        .O(\axi_awlen_cntr[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \axi_awlen_cntr[1]_i_1 
       (.I0(axi_awlen_cntr_reg__0[1]),
        .I1(axi_awlen_cntr_reg__0[0]),
        .O(plusOp__0[1]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \axi_awlen_cntr[2]_i_1 
       (.I0(axi_awlen_cntr_reg__0[2]),
        .I1(axi_awlen_cntr_reg__0[1]),
        .I2(axi_awlen_cntr_reg__0[0]),
        .O(plusOp__0[2]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \axi_awlen_cntr[3]_i_1 
       (.I0(axi_awlen_cntr_reg__0[0]),
        .I1(axi_awlen_cntr_reg__0[1]),
        .I2(axi_awlen_cntr_reg__0[2]),
        .I3(axi_awlen_cntr_reg__0[3]),
        .O(plusOp__0[3]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \axi_awlen_cntr[4]_i_1 
       (.I0(axi_awlen_cntr_reg__0[4]),
        .I1(axi_awlen_cntr_reg__0[0]),
        .I2(axi_awlen_cntr_reg__0[1]),
        .I3(axi_awlen_cntr_reg__0[2]),
        .I4(axi_awlen_cntr_reg__0[3]),
        .O(plusOp__0[4]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \axi_awlen_cntr[5]_i_1 
       (.I0(axi_awlen_cntr_reg__0[5]),
        .I1(axi_awlen_cntr_reg__0[3]),
        .I2(axi_awlen_cntr_reg__0[2]),
        .I3(axi_awlen_cntr_reg__0[1]),
        .I4(axi_awlen_cntr_reg__0[0]),
        .I5(axi_awlen_cntr_reg__0[4]),
        .O(plusOp__0[5]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \axi_awlen_cntr[6]_i_1 
       (.I0(axi_awlen_cntr_reg__0[6]),
        .I1(axi_awlen_cntr_reg__0[4]),
        .I2(\axi_awlen_cntr[7]_i_4_n_0 ),
        .I3(axi_awlen_cntr_reg__0[5]),
        .O(plusOp__0[6]));
  LUT4 #(
    .INIT(16'h5575)) 
    \axi_awlen_cntr[7]_i_1 
       (.I0(s00_axi_aresetn),
        .I1(s00_axi_awready),
        .I2(s00_axi_awvalid),
        .I3(axi_awv_awr_flag),
        .O(\axi_awlen_cntr[7]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h80)) 
    \axi_awlen_cntr[7]_i_2 
       (.I0(axi_awaddr3),
        .I1(s00_axi_wready),
        .I2(s00_axi_wvalid),
        .O(axi_awaddr1));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \axi_awlen_cntr[7]_i_3 
       (.I0(axi_awlen_cntr_reg__0[7]),
        .I1(axi_awlen_cntr_reg__0[5]),
        .I2(\axi_awlen_cntr[7]_i_4_n_0 ),
        .I3(axi_awlen_cntr_reg__0[4]),
        .I4(axi_awlen_cntr_reg__0[6]),
        .O(plusOp__0[7]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \axi_awlen_cntr[7]_i_4 
       (.I0(axi_awlen_cntr_reg__0[3]),
        .I1(axi_awlen_cntr_reg__0[2]),
        .I2(axi_awlen_cntr_reg__0[1]),
        .I3(axi_awlen_cntr_reg__0[0]),
        .O(\axi_awlen_cntr[7]_i_4_n_0 ));
  FDRE \axi_awlen_cntr_reg[0] 
       (.C(s00_axi_aclk),
        .CE(axi_awaddr1),
        .D(\axi_awlen_cntr[0]_i_1_n_0 ),
        .Q(axi_awlen_cntr_reg__0[0]),
        .R(\axi_awlen_cntr[7]_i_1_n_0 ));
  FDRE \axi_awlen_cntr_reg[1] 
       (.C(s00_axi_aclk),
        .CE(axi_awaddr1),
        .D(plusOp__0[1]),
        .Q(axi_awlen_cntr_reg__0[1]),
        .R(\axi_awlen_cntr[7]_i_1_n_0 ));
  FDRE \axi_awlen_cntr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(axi_awaddr1),
        .D(plusOp__0[2]),
        .Q(axi_awlen_cntr_reg__0[2]),
        .R(\axi_awlen_cntr[7]_i_1_n_0 ));
  FDRE \axi_awlen_cntr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(axi_awaddr1),
        .D(plusOp__0[3]),
        .Q(axi_awlen_cntr_reg__0[3]),
        .R(\axi_awlen_cntr[7]_i_1_n_0 ));
  FDRE \axi_awlen_cntr_reg[4] 
       (.C(s00_axi_aclk),
        .CE(axi_awaddr1),
        .D(plusOp__0[4]),
        .Q(axi_awlen_cntr_reg__0[4]),
        .R(\axi_awlen_cntr[7]_i_1_n_0 ));
  FDRE \axi_awlen_cntr_reg[5] 
       (.C(s00_axi_aclk),
        .CE(axi_awaddr1),
        .D(plusOp__0[5]),
        .Q(axi_awlen_cntr_reg__0[5]),
        .R(\axi_awlen_cntr[7]_i_1_n_0 ));
  FDRE \axi_awlen_cntr_reg[6] 
       (.C(s00_axi_aclk),
        .CE(axi_awaddr1),
        .D(plusOp__0[6]),
        .Q(axi_awlen_cntr_reg__0[6]),
        .R(\axi_awlen_cntr[7]_i_1_n_0 ));
  FDRE \axi_awlen_cntr_reg[7] 
       (.C(s00_axi_aclk),
        .CE(axi_awaddr1),
        .D(plusOp__0[7]),
        .Q(axi_awlen_cntr_reg__0[7]),
        .R(\axi_awlen_cntr[7]_i_1_n_0 ));
  FDRE \axi_awlen_reg[0] 
       (.C(s00_axi_aclk),
        .CE(p_9_in),
        .D(s00_axi_awlen[0]),
        .Q(aw_wrap_size__0[2]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_awlen_reg[1] 
       (.C(s00_axi_aclk),
        .CE(p_9_in),
        .D(s00_axi_awlen[1]),
        .Q(aw_wrap_size__0[3]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_awlen_reg[2] 
       (.C(s00_axi_aclk),
        .CE(p_9_in),
        .D(s00_axi_awlen[2]),
        .Q(aw_wrap_size__0[4]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_awlen_reg[3] 
       (.C(s00_axi_aclk),
        .CE(p_9_in),
        .D(s00_axi_awlen[3]),
        .Q(aw_wrap_size__0[5]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_awlen_reg[4] 
       (.C(s00_axi_aclk),
        .CE(p_9_in),
        .D(s00_axi_awlen[4]),
        .Q(aw_wrap_size__0[6]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_awlen_reg[5] 
       (.C(s00_axi_aclk),
        .CE(p_9_in),
        .D(s00_axi_awlen[5]),
        .Q(aw_wrap_size__0[7]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_awlen_reg[6] 
       (.C(s00_axi_aclk),
        .CE(p_9_in),
        .D(s00_axi_awlen[6]),
        .Q(aw_wrap_size__0[8]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_awlen_reg[7] 
       (.C(s00_axi_aclk),
        .CE(p_9_in),
        .D(s00_axi_awlen[7]),
        .Q(aw_wrap_size__0[9]),
        .R(axi_awready_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    axi_awready_i_1
       (.I0(s00_axi_aresetn),
        .O(axi_awready_i_1_n_0));
  LUT6 #(
    .INIT(64'hF0F4000400040004)) 
    axi_awready_i_2__0
       (.I0(axi_awv_awr_flag),
        .I1(s00_axi_awvalid),
        .I2(s00_axi_awready),
        .I3(axi_arv_arr_flag),
        .I4(s00_axi_wlast),
        .I5(s00_axi_wready),
        .O(axi_awready_i_2__0_n_0));
  FDRE axi_awready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_awready_i_2__0_n_0),
        .Q(s00_axi_awready),
        .R(axi_awready_i_1_n_0));
  LUT6 #(
    .INIT(64'h7070707070707F70)) 
    axi_awv_awr_flag_i_1
       (.I0(s00_axi_wlast),
        .I1(s00_axi_wready),
        .I2(axi_awv_awr_flag),
        .I3(s00_axi_awvalid),
        .I4(s00_axi_awready),
        .I5(axi_arv_arr_flag),
        .O(axi_awv_awr_flag_i_1_n_0));
  FDRE axi_awv_awr_flag_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_awv_awr_flag_i_1_n_0),
        .Q(axi_awv_awr_flag),
        .R(axi_awready_i_1_n_0));
  LUT6 #(
    .INIT(64'h7444444444444444)) 
    axi_bvalid_i_1
       (.I0(s00_axi_bready),
        .I1(s00_axi_bvalid),
        .I2(axi_awv_awr_flag),
        .I3(s00_axi_wvalid),
        .I4(s00_axi_wlast),
        .I5(s00_axi_wready),
        .O(axi_bvalid_i_1_n_0));
  FDRE axi_bvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_bvalid_i_1_n_0),
        .Q(s00_axi_bvalid),
        .R(axi_awready_i_1_n_0));
  LUT6 #(
    .INIT(64'h2220A2A0A2A0A2A0)) 
    axi_rlast_i_1
       (.I0(axi_rlast_i_2_n_0),
        .I1(s00_axi_rready),
        .I2(axi_rlast0),
        .I3(s00_axi_rlast),
        .I4(axi_araddr3),
        .I5(s00_axi_rvalid),
        .O(axi_rlast_i_1_n_0));
  LUT4 #(
    .INIT(16'hAA8A)) 
    axi_rlast_i_2
       (.I0(s00_axi_aresetn),
        .I1(s00_axi_arready),
        .I2(s00_axi_arvalid),
        .I3(axi_arv_arr_flag),
        .O(axi_rlast_i_2_n_0));
  LUT6 #(
    .INIT(64'h0000000001000000)) 
    axi_rlast_i_3
       (.I0(axi_arready_i_3_n_0),
        .I1(axi_arready_i_4_n_0),
        .I2(axi_arready_i_5_n_0),
        .I3(axi_arready_i_6_n_0),
        .I4(axi_arv_arr_flag),
        .I5(s00_axi_rlast),
        .O(axi_rlast0));
  FDRE axi_rlast_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_rlast_i_1_n_0),
        .Q(s00_axi_rlast),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'h3A)) 
    axi_rvalid_i_1
       (.I0(axi_arv_arr_flag),
        .I1(s00_axi_rready),
        .I2(s00_axi_rvalid),
        .O(axi_rvalid_i_1_n_0));
  FDRE axi_rvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_rvalid_i_1_n_0),
        .Q(s00_axi_rvalid),
        .R(axi_awready_i_1_n_0));
  LUT4 #(
    .INIT(16'h0F88)) 
    axi_wready_i_1__0
       (.I0(s00_axi_wvalid),
        .I1(axi_awv_awr_flag),
        .I2(s00_axi_wlast),
        .I3(s00_axi_wready),
        .O(axi_wready_i_1__0_n_0));
  FDRE axi_wready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_wready_i_1__0_n_0),
        .Q(s00_axi_wready),
        .R(axi_awready_i_1_n_0));
  CARRY4 \plusOp_inferred__1/i__carry 
       (.CI(1'b0),
        .CO({\plusOp_inferred__1/i__carry_n_0 ,\plusOp_inferred__1/i__carry_n_1 ,\plusOp_inferred__1/i__carry_n_2 ,\plusOp_inferred__1/i__carry_n_3 }),
        .CYINIT(L[2]),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(p_0_in__0[6:3]),
        .S(L[6:3]));
  CARRY4 \plusOp_inferred__1/i__carry__0 
       (.CI(\plusOp_inferred__1/i__carry_n_0 ),
        .CO({\NLW_plusOp_inferred__1/i__carry__0_CO_UNCONNECTED [3:2],\plusOp_inferred__1/i__carry__0_n_2 ,\plusOp_inferred__1/i__carry__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_plusOp_inferred__1/i__carry__0_O_UNCONNECTED [3],p_0_in__0[9:7]}),
        .S({1'b0,L[9:7]}));
  CARRY4 \plusOp_inferred__2/i__carry 
       (.CI(1'b0),
        .CO({\plusOp_inferred__2/i__carry_n_0 ,\plusOp_inferred__2/i__carry_n_1 ,\plusOp_inferred__2/i__carry_n_2 ,\plusOp_inferred__2/i__carry_n_3 }),
        .CYINIT(p_1_in[0]),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\plusOp_inferred__2/i__carry_n_4 ,\plusOp_inferred__2/i__carry_n_5 ,\plusOp_inferred__2/i__carry_n_6 ,\plusOp_inferred__2/i__carry_n_7 }),
        .S(p_1_in[4:1]));
  CARRY4 \plusOp_inferred__2/i__carry__0 
       (.CI(\plusOp_inferred__2/i__carry_n_0 ),
        .CO({\NLW_plusOp_inferred__2/i__carry__0_CO_UNCONNECTED [3:2],\plusOp_inferred__2/i__carry__0_n_2 ,\plusOp_inferred__2/i__carry__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_plusOp_inferred__2/i__carry__0_O_UNCONNECTED [3],\plusOp_inferred__2/i__carry__0_n_5 ,\plusOp_inferred__2/i__carry__0_n_6 ,\plusOp_inferred__2/i__carry__0_n_7 }),
        .S({1'b0,\axi_araddr_reg_n_0_[9] ,p_1_in[6:5]}));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \s00_axi_rdata[0]_INST_0 
       (.I0(s00_axi_rvalid),
        .I1(\BRAM_GEN[0].BYTE_BRAM_GEN[0].mem_data_out_reg[0] [0]),
        .O(s00_axi_rdata[0]));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \s00_axi_rdata[10]_INST_0 
       (.I0(s00_axi_rvalid),
        .I1(\BRAM_GEN[0].BYTE_BRAM_GEN[1].mem_data_out_reg[0] [2]),
        .O(s00_axi_rdata[10]));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \s00_axi_rdata[11]_INST_0 
       (.I0(s00_axi_rvalid),
        .I1(\BRAM_GEN[0].BYTE_BRAM_GEN[1].mem_data_out_reg[0] [3]),
        .O(s00_axi_rdata[11]));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \s00_axi_rdata[12]_INST_0 
       (.I0(s00_axi_rvalid),
        .I1(\BRAM_GEN[0].BYTE_BRAM_GEN[1].mem_data_out_reg[0] [4]),
        .O(s00_axi_rdata[12]));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \s00_axi_rdata[13]_INST_0 
       (.I0(s00_axi_rvalid),
        .I1(\BRAM_GEN[0].BYTE_BRAM_GEN[1].mem_data_out_reg[0] [5]),
        .O(s00_axi_rdata[13]));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \s00_axi_rdata[14]_INST_0 
       (.I0(s00_axi_rvalid),
        .I1(\BRAM_GEN[0].BYTE_BRAM_GEN[1].mem_data_out_reg[0] [6]),
        .O(s00_axi_rdata[14]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \s00_axi_rdata[15]_INST_0 
       (.I0(s00_axi_rvalid),
        .I1(\BRAM_GEN[0].BYTE_BRAM_GEN[1].mem_data_out_reg[0] [7]),
        .O(s00_axi_rdata[15]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \s00_axi_rdata[16]_INST_0 
       (.I0(s00_axi_rvalid),
        .I1(\BRAM_GEN[0].BYTE_BRAM_GEN[2].mem_data_out_reg[0] [0]),
        .O(s00_axi_rdata[16]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \s00_axi_rdata[17]_INST_0 
       (.I0(s00_axi_rvalid),
        .I1(\BRAM_GEN[0].BYTE_BRAM_GEN[2].mem_data_out_reg[0] [1]),
        .O(s00_axi_rdata[17]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \s00_axi_rdata[18]_INST_0 
       (.I0(s00_axi_rvalid),
        .I1(\BRAM_GEN[0].BYTE_BRAM_GEN[2].mem_data_out_reg[0] [2]),
        .O(s00_axi_rdata[18]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \s00_axi_rdata[19]_INST_0 
       (.I0(s00_axi_rvalid),
        .I1(\BRAM_GEN[0].BYTE_BRAM_GEN[2].mem_data_out_reg[0] [3]),
        .O(s00_axi_rdata[19]));
  LUT2 #(
    .INIT(4'h8)) 
    \s00_axi_rdata[1]_INST_0 
       (.I0(s00_axi_rvalid),
        .I1(\BRAM_GEN[0].BYTE_BRAM_GEN[0].mem_data_out_reg[0] [1]),
        .O(s00_axi_rdata[1]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \s00_axi_rdata[20]_INST_0 
       (.I0(s00_axi_rvalid),
        .I1(\BRAM_GEN[0].BYTE_BRAM_GEN[2].mem_data_out_reg[0] [4]),
        .O(s00_axi_rdata[20]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \s00_axi_rdata[21]_INST_0 
       (.I0(s00_axi_rvalid),
        .I1(\BRAM_GEN[0].BYTE_BRAM_GEN[2].mem_data_out_reg[0] [5]),
        .O(s00_axi_rdata[21]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \s00_axi_rdata[22]_INST_0 
       (.I0(s00_axi_rvalid),
        .I1(\BRAM_GEN[0].BYTE_BRAM_GEN[2].mem_data_out_reg[0] [6]),
        .O(s00_axi_rdata[22]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \s00_axi_rdata[23]_INST_0 
       (.I0(s00_axi_rvalid),
        .I1(\BRAM_GEN[0].BYTE_BRAM_GEN[2].mem_data_out_reg[0] [7]),
        .O(s00_axi_rdata[23]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \s00_axi_rdata[24]_INST_0 
       (.I0(s00_axi_rvalid),
        .I1(\BRAM_GEN[0].BYTE_BRAM_GEN[3].mem_data_out_reg[0] [0]),
        .O(s00_axi_rdata[24]));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \s00_axi_rdata[25]_INST_0 
       (.I0(s00_axi_rvalid),
        .I1(\BRAM_GEN[0].BYTE_BRAM_GEN[3].mem_data_out_reg[0] [1]),
        .O(s00_axi_rdata[25]));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \s00_axi_rdata[26]_INST_0 
       (.I0(s00_axi_rvalid),
        .I1(\BRAM_GEN[0].BYTE_BRAM_GEN[3].mem_data_out_reg[0] [2]),
        .O(s00_axi_rdata[26]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \s00_axi_rdata[27]_INST_0 
       (.I0(s00_axi_rvalid),
        .I1(\BRAM_GEN[0].BYTE_BRAM_GEN[3].mem_data_out_reg[0] [3]),
        .O(s00_axi_rdata[27]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \s00_axi_rdata[28]_INST_0 
       (.I0(s00_axi_rvalid),
        .I1(\BRAM_GEN[0].BYTE_BRAM_GEN[3].mem_data_out_reg[0] [4]),
        .O(s00_axi_rdata[28]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \s00_axi_rdata[29]_INST_0 
       (.I0(s00_axi_rvalid),
        .I1(\BRAM_GEN[0].BYTE_BRAM_GEN[3].mem_data_out_reg[0] [5]),
        .O(s00_axi_rdata[29]));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \s00_axi_rdata[2]_INST_0 
       (.I0(s00_axi_rvalid),
        .I1(\BRAM_GEN[0].BYTE_BRAM_GEN[0].mem_data_out_reg[0] [2]),
        .O(s00_axi_rdata[2]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \s00_axi_rdata[30]_INST_0 
       (.I0(s00_axi_rvalid),
        .I1(\BRAM_GEN[0].BYTE_BRAM_GEN[3].mem_data_out_reg[0] [6]),
        .O(s00_axi_rdata[30]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \s00_axi_rdata[31]_INST_0 
       (.I0(s00_axi_rvalid),
        .I1(\BRAM_GEN[0].BYTE_BRAM_GEN[3].mem_data_out_reg[0] [7]),
        .O(s00_axi_rdata[31]));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \s00_axi_rdata[3]_INST_0 
       (.I0(s00_axi_rvalid),
        .I1(\BRAM_GEN[0].BYTE_BRAM_GEN[0].mem_data_out_reg[0] [3]),
        .O(s00_axi_rdata[3]));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \s00_axi_rdata[4]_INST_0 
       (.I0(s00_axi_rvalid),
        .I1(\BRAM_GEN[0].BYTE_BRAM_GEN[0].mem_data_out_reg[0] [4]),
        .O(s00_axi_rdata[4]));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \s00_axi_rdata[5]_INST_0 
       (.I0(s00_axi_rvalid),
        .I1(\BRAM_GEN[0].BYTE_BRAM_GEN[0].mem_data_out_reg[0] [5]),
        .O(s00_axi_rdata[5]));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \s00_axi_rdata[6]_INST_0 
       (.I0(s00_axi_rvalid),
        .I1(\BRAM_GEN[0].BYTE_BRAM_GEN[0].mem_data_out_reg[0] [6]),
        .O(s00_axi_rdata[6]));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \s00_axi_rdata[7]_INST_0 
       (.I0(s00_axi_rvalid),
        .I1(\BRAM_GEN[0].BYTE_BRAM_GEN[0].mem_data_out_reg[0] [7]),
        .O(s00_axi_rdata[7]));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \s00_axi_rdata[8]_INST_0 
       (.I0(s00_axi_rvalid),
        .I1(\BRAM_GEN[0].BYTE_BRAM_GEN[1].mem_data_out_reg[0] [0]),
        .O(s00_axi_rdata[8]));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \s00_axi_rdata[9]_INST_0 
       (.I0(s00_axi_rvalid),
        .I1(\BRAM_GEN[0].BYTE_BRAM_GEN[1].mem_data_out_reg[0] [1]),
        .O(s00_axi_rdata[9]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \to_Dispatcher_data_out[0]_INST_0 
       (.I0(DOADO[0]),
        .I1(\to_Dispatcher_data_out[31]_INST_0_i_1 ),
        .I2(\to_Dispatcher_data_out[0]_INST_0_i_1 ),
        .O(to_Dispatcher_data_out[0]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \to_Dispatcher_data_out[10]_INST_0 
       (.I0(\to_Dispatcher_data_out[15]_INST_0_i_1 [2]),
        .I1(\to_Dispatcher_data_out[31]_INST_0_i_1 ),
        .I2(\to_Dispatcher_data_out[10]_INST_0_i_1 ),
        .O(to_Dispatcher_data_out[10]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \to_Dispatcher_data_out[11]_INST_0 
       (.I0(\to_Dispatcher_data_out[15]_INST_0_i_1 [3]),
        .I1(\to_Dispatcher_data_out[31]_INST_0_i_1 ),
        .I2(\to_Dispatcher_data_out[11]_INST_0_i_1 ),
        .O(to_Dispatcher_data_out[11]));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \to_Dispatcher_data_out[12]_INST_0 
       (.I0(\to_Dispatcher_data_out[15]_INST_0_i_1 [4]),
        .I1(\to_Dispatcher_data_out[31]_INST_0_i_1 ),
        .I2(\to_Dispatcher_data_out[12]_INST_0_i_1 ),
        .O(to_Dispatcher_data_out[12]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \to_Dispatcher_data_out[13]_INST_0 
       (.I0(\to_Dispatcher_data_out[15]_INST_0_i_1 [5]),
        .I1(\to_Dispatcher_data_out[31]_INST_0_i_1 ),
        .I2(\to_Dispatcher_data_out[13]_INST_0_i_1 ),
        .O(to_Dispatcher_data_out[13]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \to_Dispatcher_data_out[14]_INST_0 
       (.I0(\to_Dispatcher_data_out[15]_INST_0_i_1 [6]),
        .I1(\to_Dispatcher_data_out[31]_INST_0_i_1 ),
        .I2(\to_Dispatcher_data_out[14]_INST_0_i_1 ),
        .O(to_Dispatcher_data_out[14]));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \to_Dispatcher_data_out[15]_INST_0 
       (.I0(\to_Dispatcher_data_out[15]_INST_0_i_1 [7]),
        .I1(\to_Dispatcher_data_out[31]_INST_0_i_1 ),
        .I2(\to_Dispatcher_data_out[15]_INST_0_i_1_0 ),
        .O(to_Dispatcher_data_out[15]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \to_Dispatcher_data_out[16]_INST_0 
       (.I0(\to_Dispatcher_data_out[23]_INST_0_i_1 [0]),
        .I1(\to_Dispatcher_data_out[31]_INST_0_i_1 ),
        .I2(\to_Dispatcher_data_out[16]_INST_0_i_1 ),
        .O(to_Dispatcher_data_out[16]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \to_Dispatcher_data_out[17]_INST_0 
       (.I0(\to_Dispatcher_data_out[23]_INST_0_i_1 [1]),
        .I1(\to_Dispatcher_data_out[31]_INST_0_i_1 ),
        .I2(\to_Dispatcher_data_out[17]_INST_0_i_1 ),
        .O(to_Dispatcher_data_out[17]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \to_Dispatcher_data_out[18]_INST_0 
       (.I0(\to_Dispatcher_data_out[23]_INST_0_i_1 [2]),
        .I1(\to_Dispatcher_data_out[31]_INST_0_i_1 ),
        .I2(\to_Dispatcher_data_out[18]_INST_0_i_1 ),
        .O(to_Dispatcher_data_out[18]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \to_Dispatcher_data_out[19]_INST_0 
       (.I0(\to_Dispatcher_data_out[23]_INST_0_i_1 [3]),
        .I1(\to_Dispatcher_data_out[31]_INST_0_i_1 ),
        .I2(\to_Dispatcher_data_out[19]_INST_0_i_1 ),
        .O(to_Dispatcher_data_out[19]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \to_Dispatcher_data_out[1]_INST_0 
       (.I0(DOADO[1]),
        .I1(\to_Dispatcher_data_out[31]_INST_0_i_1 ),
        .I2(\to_Dispatcher_data_out[1]_INST_0_i_1 ),
        .O(to_Dispatcher_data_out[1]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \to_Dispatcher_data_out[20]_INST_0 
       (.I0(\to_Dispatcher_data_out[23]_INST_0_i_1 [4]),
        .I1(\to_Dispatcher_data_out[31]_INST_0_i_1 ),
        .I2(\to_Dispatcher_data_out[20]_INST_0_i_1 ),
        .O(to_Dispatcher_data_out[20]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \to_Dispatcher_data_out[21]_INST_0 
       (.I0(\to_Dispatcher_data_out[23]_INST_0_i_1 [5]),
        .I1(\to_Dispatcher_data_out[31]_INST_0_i_1 ),
        .I2(\to_Dispatcher_data_out[21]_INST_0_i_1 ),
        .O(to_Dispatcher_data_out[21]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \to_Dispatcher_data_out[22]_INST_0 
       (.I0(\to_Dispatcher_data_out[23]_INST_0_i_1 [6]),
        .I1(\to_Dispatcher_data_out[31]_INST_0_i_1 ),
        .I2(\to_Dispatcher_data_out[22]_INST_0_i_1 ),
        .O(to_Dispatcher_data_out[22]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \to_Dispatcher_data_out[23]_INST_0 
       (.I0(\to_Dispatcher_data_out[23]_INST_0_i_1 [7]),
        .I1(\to_Dispatcher_data_out[31]_INST_0_i_1 ),
        .I2(\to_Dispatcher_data_out[23]_INST_0_i_1_0 ),
        .O(to_Dispatcher_data_out[23]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \to_Dispatcher_data_out[24]_INST_0 
       (.I0(\to_Dispatcher_data_out[31]_INST_0_i_2 [0]),
        .I1(\to_Dispatcher_data_out[31]_INST_0_i_1 ),
        .I2(\to_Dispatcher_data_out[24]_INST_0_i_1 ),
        .O(to_Dispatcher_data_out[24]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \to_Dispatcher_data_out[25]_INST_0 
       (.I0(\to_Dispatcher_data_out[31]_INST_0_i_2 [1]),
        .I1(\to_Dispatcher_data_out[31]_INST_0_i_1 ),
        .I2(\to_Dispatcher_data_out[25]_INST_0_i_1 ),
        .O(to_Dispatcher_data_out[25]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \to_Dispatcher_data_out[26]_INST_0 
       (.I0(\to_Dispatcher_data_out[31]_INST_0_i_2 [2]),
        .I1(\to_Dispatcher_data_out[31]_INST_0_i_1 ),
        .I2(\to_Dispatcher_data_out[26]_INST_0_i_1 ),
        .O(to_Dispatcher_data_out[26]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \to_Dispatcher_data_out[27]_INST_0 
       (.I0(\to_Dispatcher_data_out[31]_INST_0_i_2 [3]),
        .I1(\to_Dispatcher_data_out[31]_INST_0_i_1 ),
        .I2(\to_Dispatcher_data_out[27]_INST_0_i_1 ),
        .O(to_Dispatcher_data_out[27]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \to_Dispatcher_data_out[28]_INST_0 
       (.I0(\to_Dispatcher_data_out[31]_INST_0_i_2 [4]),
        .I1(\to_Dispatcher_data_out[31]_INST_0_i_1 ),
        .I2(\to_Dispatcher_data_out[28]_INST_0_i_1 ),
        .O(to_Dispatcher_data_out[28]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \to_Dispatcher_data_out[29]_INST_0 
       (.I0(\to_Dispatcher_data_out[31]_INST_0_i_2 [5]),
        .I1(\to_Dispatcher_data_out[31]_INST_0_i_1 ),
        .I2(\to_Dispatcher_data_out[29]_INST_0_i_1 ),
        .O(to_Dispatcher_data_out[29]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \to_Dispatcher_data_out[2]_INST_0 
       (.I0(DOADO[2]),
        .I1(\to_Dispatcher_data_out[31]_INST_0_i_1 ),
        .I2(\to_Dispatcher_data_out[2]_INST_0_i_1 ),
        .O(to_Dispatcher_data_out[2]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \to_Dispatcher_data_out[30]_INST_0 
       (.I0(\to_Dispatcher_data_out[31]_INST_0_i_2 [6]),
        .I1(\to_Dispatcher_data_out[31]_INST_0_i_1 ),
        .I2(\to_Dispatcher_data_out[30]_INST_0_i_1 ),
        .O(to_Dispatcher_data_out[30]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \to_Dispatcher_data_out[31]_INST_0 
       (.I0(\to_Dispatcher_data_out[31]_INST_0_i_2 [7]),
        .I1(\to_Dispatcher_data_out[31]_INST_0_i_1 ),
        .I2(\to_Dispatcher_data_out[31]_INST_0_i_2_0 ),
        .O(to_Dispatcher_data_out[31]));
  LUT5 #(
    .INIT(32'h00000002)) 
    \to_Dispatcher_data_out[31]_INST_0_i_3 
       (.I0(from_Dispatcher_enable),
        .I1(from_Dispatcher_bytes_selection[2]),
        .I2(from_Dispatcher_bytes_selection[1]),
        .I3(from_Dispatcher_bytes_selection[3]),
        .I4(from_Dispatcher_bytes_selection[0]),
        .O(p_0_in));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \to_Dispatcher_data_out[3]_INST_0 
       (.I0(DOADO[3]),
        .I1(\to_Dispatcher_data_out[31]_INST_0_i_1 ),
        .I2(\to_Dispatcher_data_out[3]_INST_0_i_1 ),
        .O(to_Dispatcher_data_out[3]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \to_Dispatcher_data_out[4]_INST_0 
       (.I0(DOADO[4]),
        .I1(\to_Dispatcher_data_out[31]_INST_0_i_1 ),
        .I2(\to_Dispatcher_data_out[4]_INST_0_i_1 ),
        .O(to_Dispatcher_data_out[4]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \to_Dispatcher_data_out[5]_INST_0 
       (.I0(DOADO[5]),
        .I1(\to_Dispatcher_data_out[31]_INST_0_i_1 ),
        .I2(\to_Dispatcher_data_out[5]_INST_0_i_1 ),
        .O(to_Dispatcher_data_out[5]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \to_Dispatcher_data_out[6]_INST_0 
       (.I0(DOADO[6]),
        .I1(\to_Dispatcher_data_out[31]_INST_0_i_1 ),
        .I2(\to_Dispatcher_data_out[6]_INST_0_i_1 ),
        .O(to_Dispatcher_data_out[6]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \to_Dispatcher_data_out[7]_INST_0 
       (.I0(DOADO[7]),
        .I1(\to_Dispatcher_data_out[31]_INST_0_i_1 ),
        .I2(\to_Dispatcher_data_out[7]_INST_0_i_1 ),
        .O(to_Dispatcher_data_out[7]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \to_Dispatcher_data_out[8]_INST_0 
       (.I0(\to_Dispatcher_data_out[15]_INST_0_i_1 [0]),
        .I1(\to_Dispatcher_data_out[31]_INST_0_i_1 ),
        .I2(\to_Dispatcher_data_out[8]_INST_0_i_1 ),
        .O(to_Dispatcher_data_out[8]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \to_Dispatcher_data_out[9]_INST_0 
       (.I0(\to_Dispatcher_data_out[15]_INST_0_i_1 [1]),
        .I1(\to_Dispatcher_data_out[31]_INST_0_i_1 ),
        .I2(\to_Dispatcher_data_out[9]_INST_0_i_1 ),
        .O(to_Dispatcher_data_out[9]));
endmodule

(* ORIG_REF_NAME = "PL_to_PS_S_AXI_INTR" *) 
module design_1_PL_to_PS_0_0_PL_to_PS_S_AXI_INTR
   (s_axi_intr_wready,
    s_axi_intr_awready,
    s_axi_intr_arready,
    irq,
    s_axi_intr_bvalid,
    s_axi_intr_rvalid,
    s_axi_intr_rdata,
    s_axi_intr_aclk,
    s_axi_intr_wdata,
    from_Dispatcher_clk,
    s_axi_intr_awvalid,
    s_axi_intr_wvalid,
    s_axi_intr_bready,
    s_axi_intr_arvalid,
    s_axi_intr_rready,
    dispatcher_generate_interrupt,
    from_Dispatcher_reset,
    s_axi_intr_aresetn,
    s_axi_intr_awaddr,
    s_axi_intr_araddr,
    fifo_ptm_full,
    fifo_ptm_almost_full,
    fifo_instrumentation_full,
    fifo_instrumentation_almost_full,
    fifo_kernel_to_monitor_full,
    fifo_kernel_to_monitor_almost_full,
    fifo_monitor_to_kernel_full,
    fifo_monitor_to_kernel_almost_full,
    fifo_kernel_to_monitor_empty,
    fifo_kernel_to_monitor_almost_empty,
    fifo_instrumentation_empty,
    fifo_instrumentation_almost_empty,
    fifo_monitor_to_kernel_almost_empty,
    fifo_monitor_to_kernel_empty,
    fifo_ptm_empty,
    fifo_ptm_almost_empty);
  output s_axi_intr_wready;
  output s_axi_intr_awready;
  output s_axi_intr_arready;
  output irq;
  output s_axi_intr_bvalid;
  output s_axi_intr_rvalid;
  output [5:0]s_axi_intr_rdata;
  input s_axi_intr_aclk;
  input [5:0]s_axi_intr_wdata;
  input from_Dispatcher_clk;
  input s_axi_intr_awvalid;
  input s_axi_intr_wvalid;
  input s_axi_intr_bready;
  input s_axi_intr_arvalid;
  input s_axi_intr_rready;
  input dispatcher_generate_interrupt;
  input from_Dispatcher_reset;
  input s_axi_intr_aresetn;
  input [2:0]s_axi_intr_awaddr;
  input [4:0]s_axi_intr_araddr;
  input fifo_ptm_full;
  input fifo_ptm_almost_full;
  input fifo_instrumentation_full;
  input fifo_instrumentation_almost_full;
  input fifo_kernel_to_monitor_full;
  input fifo_kernel_to_monitor_almost_full;
  input fifo_monitor_to_kernel_full;
  input fifo_monitor_to_kernel_almost_full;
  input fifo_kernel_to_monitor_empty;
  input fifo_kernel_to_monitor_almost_empty;
  input fifo_instrumentation_empty;
  input fifo_instrumentation_almost_empty;
  input fifo_monitor_to_kernel_almost_empty;
  input fifo_monitor_to_kernel_empty;
  input fifo_ptm_empty;
  input fifo_ptm_almost_empty;

  wire aw_en_i_1_n_0;
  wire aw_en_reg_n_0;
  wire axi_arready0;
  wire \axi_awaddr[2]_i_1_n_0 ;
  wire \axi_awaddr[3]_i_1_n_0 ;
  wire \axi_awaddr[4]_i_1_n_0 ;
  wire axi_awready0;
  wire axi_awready_i_1__0_n_0;
  wire axi_bvalid_i_1__0_n_0;
  wire \axi_rdata[0]_i_2_n_0 ;
  wire \axi_rdata[0]_i_3_n_0 ;
  wire \axi_rdata[0]_i_4_n_0 ;
  wire \axi_rdata[0]_i_5_n_0 ;
  wire \axi_rdata[0]_i_6_n_0 ;
  wire \axi_rdata[0]_i_7_n_0 ;
  wire \axi_rdata[0]_i_8_n_0 ;
  wire \axi_rdata[0]_i_9_n_0 ;
  wire \axi_rdata[1]_i_2_n_0 ;
  wire \axi_rdata[2]_i_2_n_0 ;
  wire \axi_rdata[3]_i_2_n_0 ;
  wire \axi_rdata[4]_i_2_n_0 ;
  wire \axi_rdata[5]_i_3_n_0 ;
  wire \axi_rdata[5]_i_4_n_0 ;
  wire axi_rvalid_i_1__0_n_0;
  wire axi_wready0;
  wire det_intr;
  wire dispatcher_generate_interrupt;
  wire dispatcher_interrupt_pending;
  wire dispatcher_interrupt_pending_i_1_n_0;
  wire fifo_instrumentation_almost_empty;
  wire fifo_instrumentation_almost_full;
  wire fifo_instrumentation_empty;
  wire fifo_instrumentation_full;
  wire fifo_instrumentation_interrupt_pending;
  wire fifo_instrumentation_interrupt_pending0;
  wire fifo_kernel_to_monitor_almost_empty;
  wire fifo_kernel_to_monitor_almost_full;
  wire fifo_kernel_to_monitor_empty;
  wire fifo_kernel_to_monitor_full;
  wire fifo_kernel_to_monitor_interrupt_pending;
  wire fifo_kernel_to_monitor_interrupt_pending0;
  wire fifo_monitor_to_kernel_almost_empty;
  wire fifo_monitor_to_kernel_almost_full;
  wire fifo_monitor_to_kernel_empty;
  wire fifo_monitor_to_kernel_full;
  wire fifo_monitor_to_kernel_interrupt_pending;
  wire fifo_monitor_to_kernel_interrupt_pending0;
  wire fifo_ptm_almost_empty;
  wire fifo_ptm_almost_full;
  wire fifo_ptm_empty;
  wire fifo_ptm_full;
  wire fifo_ptm_interrupt_pending;
  wire fifo_ptm_interrupt_pending0;
  wire from_Dispatcher_clk;
  wire from_Dispatcher_reset;
  wire \gen_intr_detection[0].gen_intr_level_detect.gen_intr_active_high_detect.det_intr[0]_i_1_n_0 ;
  wire \gen_intr_detection[1].gen_intr_level_detect.gen_intr_active_high_detect.det_intr[1]_i_1_n_0 ;
  wire \gen_intr_detection[1].gen_intr_level_detect.gen_intr_active_high_detect.det_intr_reg ;
  wire \gen_intr_detection[2].gen_intr_level_detect.gen_intr_active_high_detect.det_intr[2]_i_1_n_0 ;
  wire \gen_intr_detection[2].gen_intr_level_detect.gen_intr_active_high_detect.det_intr_reg ;
  wire \gen_intr_detection[4].gen_intr_level_detect.gen_intr_active_high_detect.det_intr[4]_i_1_n_0 ;
  wire \gen_intr_detection[4].gen_intr_level_detect.gen_intr_active_high_detect.det_intr_reg ;
  wire \gen_intr_detection[5].gen_intr_level_detect.gen_intr_active_high_detect.det_intr[5]_i_1_n_0 ;
  wire \gen_intr_detection[5].gen_intr_level_detect.gen_intr_active_high_detect.det_intr_reg ;
  wire \gen_intr_detection[5].gen_irq_level.irq_level_high.s_irq_lvl_i_1_n_0 ;
  wire \gen_intr_reg[0].reg_intr_ack[0]_i_1_n_0 ;
  wire \gen_intr_reg[0].reg_intr_ack_reg_n_0_[0] ;
  wire \gen_intr_reg[0].reg_intr_en_reg_n_0_[0] ;
  wire \gen_intr_reg[0].reg_intr_pending_reg_n_0_[0] ;
  wire \gen_intr_reg[0].reg_intr_sts_reg_n_0_[0] ;
  wire \gen_intr_reg[1].reg_intr_ack[1]_i_1_n_0 ;
  wire \gen_intr_reg[2].reg_intr_ack[2]_i_1_n_0 ;
  wire \gen_intr_reg[2].reg_intr_pending[2]_i_1_n_0 ;
  wire \gen_intr_reg[3].reg_intr_ack[3]_i_1_n_0 ;
  wire \gen_intr_reg[4].reg_intr_ack[4]_i_1_n_0 ;
  wire \gen_intr_reg[4].reg_intr_ack_reg_n_0_[4] ;
  wire \gen_intr_reg[5].reg_intr_ack[5]_i_1_n_0 ;
  wire \gen_intr_reg[5].reg_intr_en_reg_n_0_[5] ;
  wire \gen_intr_reg[5].reg_intr_pending_reg_n_0_[5] ;
  wire intr_ack_all;
  wire intr_ack_all_ff;
  wire intr_ack_all_i_1_n_0;
  wire intr_all;
  wire intr_all0;
  wire intr_all_i_1_n_0;
  wire \intr_reg_n_0_[0] ;
  wire \intr_reg_n_0_[1] ;
  wire \intr_reg_n_0_[2] ;
  wire \intr_reg_n_0_[4] ;
  wire \intr_reg_n_0_[5] ;
  wire intr_reg_rden;
  wire intr_reg_wren__2;
  wire irq;
  wire or_reduction;
  wire [2:0]p_0_in;
  wire p_0_in0_in;
  wire p_0_in12_in;
  wire p_0_in15_in;
  wire p_0_in1_in;
  wire p_0_in6_in;
  wire p_0_in9_in;
  wire p_10_out;
  wire p_12_out;
  wire p_13_out;
  wire p_14_out;
  wire p_17_out;
  wire p_18_out;
  wire p_1_in;
  wire p_1_in13_in;
  wire p_1_in16_in;
  wire p_1_in4_in;
  wire p_1_in7_in;
  wire p_21_out;
  wire p_24_out;
  wire p_27_out__2;
  wire p_2_in;
  wire p_2_in3_in;
  wire p_3_in;
  wire p_3_in4_in;
  wire p_5_out;
  wire p_8_out;
  wire p_9_out;
  wire [5:0]reg_data_out;
  wire reg_global_intr_en;
  wire reg_global_intr_en0_out;
  wire s_axi_intr_aclk;
  wire [4:0]s_axi_intr_araddr;
  wire s_axi_intr_aresetn;
  wire s_axi_intr_arready;
  wire s_axi_intr_arvalid;
  wire [2:0]s_axi_intr_awaddr;
  wire s_axi_intr_awready;
  wire s_axi_intr_awvalid;
  wire s_axi_intr_bready;
  wire s_axi_intr_bvalid;
  wire [5:0]s_axi_intr_rdata;
  wire s_axi_intr_rready;
  wire s_axi_intr_rvalid;
  wire [5:0]s_axi_intr_wdata;
  wire s_axi_intr_wready;
  wire s_axi_intr_wvalid;
  wire [4:0]sel0;

  LUT6 #(
    .INIT(64'hBFFFBF00BF00BF00)) 
    aw_en_i_1
       (.I0(s_axi_intr_awready),
        .I1(s_axi_intr_awvalid),
        .I2(s_axi_intr_wvalid),
        .I3(aw_en_reg_n_0),
        .I4(s_axi_intr_bready),
        .I5(s_axi_intr_bvalid),
        .O(aw_en_i_1_n_0));
  FDSE aw_en_reg
       (.C(s_axi_intr_aclk),
        .CE(1'b1),
        .D(aw_en_i_1_n_0),
        .Q(aw_en_reg_n_0),
        .S(axi_awready_i_1__0_n_0));
  FDSE \axi_araddr_reg[2] 
       (.C(s_axi_intr_aclk),
        .CE(axi_arready0),
        .D(s_axi_intr_araddr[0]),
        .Q(sel0[0]),
        .S(axi_awready_i_1__0_n_0));
  FDSE \axi_araddr_reg[3] 
       (.C(s_axi_intr_aclk),
        .CE(axi_arready0),
        .D(s_axi_intr_araddr[1]),
        .Q(sel0[1]),
        .S(axi_awready_i_1__0_n_0));
  FDSE \axi_araddr_reg[4] 
       (.C(s_axi_intr_aclk),
        .CE(axi_arready0),
        .D(s_axi_intr_araddr[2]),
        .Q(sel0[2]),
        .S(axi_awready_i_1__0_n_0));
  FDSE \axi_araddr_reg[5] 
       (.C(s_axi_intr_aclk),
        .CE(axi_arready0),
        .D(s_axi_intr_araddr[3]),
        .Q(sel0[3]),
        .S(axi_awready_i_1__0_n_0));
  FDSE \axi_araddr_reg[6] 
       (.C(s_axi_intr_aclk),
        .CE(axi_arready0),
        .D(s_axi_intr_araddr[4]),
        .Q(sel0[4]),
        .S(axi_awready_i_1__0_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    axi_arready_i_1
       (.I0(s_axi_intr_arvalid),
        .I1(s_axi_intr_arready),
        .O(axi_arready0));
  FDRE axi_arready_reg
       (.C(s_axi_intr_aclk),
        .CE(1'b1),
        .D(axi_arready0),
        .Q(s_axi_intr_arready),
        .R(axi_awready_i_1__0_n_0));
  LUT6 #(
    .INIT(64'hFFFFBFFF00008000)) 
    \axi_awaddr[2]_i_1 
       (.I0(s_axi_intr_awaddr[0]),
        .I1(aw_en_reg_n_0),
        .I2(s_axi_intr_wvalid),
        .I3(s_axi_intr_awvalid),
        .I4(s_axi_intr_awready),
        .I5(p_0_in[0]),
        .O(\axi_awaddr[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFBFFF00008000)) 
    \axi_awaddr[3]_i_1 
       (.I0(s_axi_intr_awaddr[1]),
        .I1(aw_en_reg_n_0),
        .I2(s_axi_intr_wvalid),
        .I3(s_axi_intr_awvalid),
        .I4(s_axi_intr_awready),
        .I5(p_0_in[1]),
        .O(\axi_awaddr[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFBFFF00008000)) 
    \axi_awaddr[4]_i_1 
       (.I0(s_axi_intr_awaddr[2]),
        .I1(aw_en_reg_n_0),
        .I2(s_axi_intr_wvalid),
        .I3(s_axi_intr_awvalid),
        .I4(s_axi_intr_awready),
        .I5(p_0_in[2]),
        .O(\axi_awaddr[4]_i_1_n_0 ));
  FDRE \axi_awaddr_reg[2] 
       (.C(s_axi_intr_aclk),
        .CE(1'b1),
        .D(\axi_awaddr[2]_i_1_n_0 ),
        .Q(p_0_in[0]),
        .R(axi_awready_i_1__0_n_0));
  FDRE \axi_awaddr_reg[3] 
       (.C(s_axi_intr_aclk),
        .CE(1'b1),
        .D(\axi_awaddr[3]_i_1_n_0 ),
        .Q(p_0_in[1]),
        .R(axi_awready_i_1__0_n_0));
  FDRE \axi_awaddr_reg[4] 
       (.C(s_axi_intr_aclk),
        .CE(1'b1),
        .D(\axi_awaddr[4]_i_1_n_0 ),
        .Q(p_0_in[2]),
        .R(axi_awready_i_1__0_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    axi_awready_i_1__0
       (.I0(s_axi_intr_aresetn),
        .O(axi_awready_i_1__0_n_0));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT4 #(
    .INIT(16'h0080)) 
    axi_awready_i_2
       (.I0(aw_en_reg_n_0),
        .I1(s_axi_intr_wvalid),
        .I2(s_axi_intr_awvalid),
        .I3(s_axi_intr_awready),
        .O(axi_awready0));
  FDRE axi_awready_reg
       (.C(s_axi_intr_aclk),
        .CE(1'b1),
        .D(axi_awready0),
        .Q(s_axi_intr_awready),
        .R(axi_awready_i_1__0_n_0));
  LUT6 #(
    .INIT(64'h0000FFFF80008000)) 
    axi_bvalid_i_1__0
       (.I0(s_axi_intr_awvalid),
        .I1(s_axi_intr_wvalid),
        .I2(s_axi_intr_wready),
        .I3(s_axi_intr_awready),
        .I4(s_axi_intr_bready),
        .I5(s_axi_intr_bvalid),
        .O(axi_bvalid_i_1__0_n_0));
  FDRE axi_bvalid_reg
       (.C(s_axi_intr_aclk),
        .CE(1'b1),
        .D(axi_bvalid_i_1__0_n_0),
        .Q(s_axi_intr_bvalid),
        .R(axi_awready_i_1__0_n_0));
  LUT6 #(
    .INIT(64'hCCFCCCFCEEFCCCFC)) 
    \axi_rdata[0]_i_1 
       (.I0(\axi_rdata[0]_i_2_n_0 ),
        .I1(\axi_rdata[0]_i_3_n_0 ),
        .I2(\axi_rdata[0]_i_4_n_0 ),
        .I3(sel0[4]),
        .I4(sel0[2]),
        .I5(sel0[3]),
        .O(reg_data_out[0]));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[0]_i_2 
       (.I0(fifo_instrumentation_empty),
        .I1(fifo_instrumentation_full),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(fifo_kernel_to_monitor_almost_full),
        .I5(fifo_instrumentation_almost_empty),
        .O(\axi_rdata[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0088000000F00000)) 
    \axi_rdata[0]_i_3 
       (.I0(\axi_rdata[5]_i_3_n_0 ),
        .I1(fifo_instrumentation_almost_full),
        .I2(\axi_rdata[0]_i_5_n_0 ),
        .I3(sel0[2]),
        .I4(sel0[4]),
        .I5(sel0[3]),
        .O(\axi_rdata[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFCA0FCA00)) 
    \axi_rdata[0]_i_4 
       (.I0(\axi_rdata[0]_i_6_n_0 ),
        .I1(\axi_rdata[0]_i_7_n_0 ),
        .I2(sel0[3]),
        .I3(sel0[2]),
        .I4(\axi_rdata[0]_i_8_n_0 ),
        .I5(\axi_rdata[0]_i_9_n_0 ),
        .O(\axi_rdata[0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[0]_i_5 
       (.I0(fifo_kernel_to_monitor_empty),
        .I1(fifo_kernel_to_monitor_full),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(fifo_monitor_to_kernel_almost_full),
        .I5(fifo_kernel_to_monitor_almost_empty),
        .O(\axi_rdata[0]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[0]_i_6 
       (.I0(fifo_ptm_empty),
        .I1(fifo_ptm_full),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(\gen_intr_reg[0].reg_intr_pending_reg_n_0_[0] ),
        .I5(fifo_ptm_almost_empty),
        .O(\axi_rdata[0]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hF0CCAA00)) 
    \axi_rdata[0]_i_7 
       (.I0(fifo_monitor_to_kernel_almost_empty),
        .I1(fifo_monitor_to_kernel_empty),
        .I2(fifo_monitor_to_kernel_full),
        .I3(sel0[1]),
        .I4(sel0[0]),
        .O(\axi_rdata[0]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hCFAFCFA0C0AFC0A0)) 
    \axi_rdata[0]_i_8 
       (.I0(\gen_intr_reg[0].reg_intr_en_reg_n_0_[0] ),
        .I1(\gen_intr_reg[0].reg_intr_ack_reg_n_0_[0] ),
        .I2(sel0[0]),
        .I3(sel0[1]),
        .I4(reg_global_intr_en),
        .I5(\gen_intr_reg[0].reg_intr_sts_reg_n_0_[0] ),
        .O(\axi_rdata[0]_i_8_n_0 ));
  LUT5 #(
    .INIT(32'h00000040)) 
    \axi_rdata[0]_i_9 
       (.I0(sel0[2]),
        .I1(sel0[3]),
        .I2(fifo_ptm_almost_full),
        .I3(sel0[0]),
        .I4(sel0[1]),
        .O(\axi_rdata[0]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'h00000088000000F0)) 
    \axi_rdata[1]_i_1 
       (.I0(p_3_in4_in),
        .I1(\axi_rdata[5]_i_3_n_0 ),
        .I2(\axi_rdata[1]_i_2_n_0 ),
        .I3(sel0[3]),
        .I4(sel0[4]),
        .I5(sel0[2]),
        .O(reg_data_out[1]));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT5 #(
    .INIT(32'hF0CCAA00)) 
    \axi_rdata[1]_i_2 
       (.I0(p_1_in16_in),
        .I1(p_0_in15_in),
        .I2(p_3_in),
        .I3(sel0[1]),
        .I4(sel0[0]),
        .O(\axi_rdata[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h00000088000000F0)) 
    \axi_rdata[2]_i_1 
       (.I0(p_2_in3_in),
        .I1(\axi_rdata[5]_i_3_n_0 ),
        .I2(\axi_rdata[2]_i_2_n_0 ),
        .I3(sel0[3]),
        .I4(sel0[4]),
        .I5(sel0[2]),
        .O(reg_data_out[2]));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT5 #(
    .INIT(32'hF0CCAA00)) 
    \axi_rdata[2]_i_2 
       (.I0(p_1_in13_in),
        .I1(p_0_in12_in),
        .I2(p_2_in),
        .I3(sel0[1]),
        .I4(sel0[0]),
        .O(\axi_rdata[2]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT5 #(
    .INIT(32'h8C008000)) 
    \axi_rdata[3]_i_1 
       (.I0(p_1_in),
        .I1(\axi_rdata[3]_i_2_n_0 ),
        .I2(sel0[1]),
        .I3(sel0[0]),
        .I4(p_0_in9_in),
        .O(reg_data_out[3]));
  LUT3 #(
    .INIT(8'h01)) 
    \axi_rdata[3]_i_2 
       (.I0(sel0[3]),
        .I1(sel0[4]),
        .I2(sel0[2]),
        .O(\axi_rdata[3]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h00000088000000F0)) 
    \axi_rdata[4]_i_1 
       (.I0(p_0_in1_in),
        .I1(\axi_rdata[5]_i_3_n_0 ),
        .I2(\axi_rdata[4]_i_2_n_0 ),
        .I3(sel0[3]),
        .I4(sel0[4]),
        .I5(sel0[2]),
        .O(reg_data_out[4]));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT5 #(
    .INIT(32'hF0CCAA00)) 
    \axi_rdata[4]_i_2 
       (.I0(p_1_in7_in),
        .I1(p_0_in6_in),
        .I2(\gen_intr_reg[4].reg_intr_ack_reg_n_0_[4] ),
        .I3(sel0[1]),
        .I4(sel0[0]),
        .O(\axi_rdata[4]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    \axi_rdata[5]_i_1 
       (.I0(s_axi_intr_arready),
        .I1(s_axi_intr_arvalid),
        .I2(s_axi_intr_rvalid),
        .O(intr_reg_rden));
  LUT6 #(
    .INIT(64'h00000088000000F0)) 
    \axi_rdata[5]_i_2 
       (.I0(\gen_intr_reg[5].reg_intr_pending_reg_n_0_[5] ),
        .I1(\axi_rdata[5]_i_3_n_0 ),
        .I2(\axi_rdata[5]_i_4_n_0 ),
        .I3(sel0[3]),
        .I4(sel0[4]),
        .I5(sel0[2]),
        .O(reg_data_out[5]));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \axi_rdata[5]_i_3 
       (.I0(sel0[1]),
        .I1(sel0[0]),
        .O(\axi_rdata[5]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT5 #(
    .INIT(32'hF0CCAA00)) 
    \axi_rdata[5]_i_4 
       (.I0(p_1_in4_in),
        .I1(\gen_intr_reg[5].reg_intr_en_reg_n_0_[5] ),
        .I2(p_0_in0_in),
        .I3(sel0[1]),
        .I4(sel0[0]),
        .O(\axi_rdata[5]_i_4_n_0 ));
  FDRE \axi_rdata_reg[0] 
       (.C(s_axi_intr_aclk),
        .CE(intr_reg_rden),
        .D(reg_data_out[0]),
        .Q(s_axi_intr_rdata[0]),
        .R(axi_awready_i_1__0_n_0));
  FDRE \axi_rdata_reg[1] 
       (.C(s_axi_intr_aclk),
        .CE(intr_reg_rden),
        .D(reg_data_out[1]),
        .Q(s_axi_intr_rdata[1]),
        .R(axi_awready_i_1__0_n_0));
  FDRE \axi_rdata_reg[2] 
       (.C(s_axi_intr_aclk),
        .CE(intr_reg_rden),
        .D(reg_data_out[2]),
        .Q(s_axi_intr_rdata[2]),
        .R(axi_awready_i_1__0_n_0));
  FDRE \axi_rdata_reg[3] 
       (.C(s_axi_intr_aclk),
        .CE(intr_reg_rden),
        .D(reg_data_out[3]),
        .Q(s_axi_intr_rdata[3]),
        .R(axi_awready_i_1__0_n_0));
  FDRE \axi_rdata_reg[4] 
       (.C(s_axi_intr_aclk),
        .CE(intr_reg_rden),
        .D(reg_data_out[4]),
        .Q(s_axi_intr_rdata[4]),
        .R(axi_awready_i_1__0_n_0));
  FDRE \axi_rdata_reg[5] 
       (.C(s_axi_intr_aclk),
        .CE(intr_reg_rden),
        .D(reg_data_out[5]),
        .Q(s_axi_intr_rdata[5]),
        .R(axi_awready_i_1__0_n_0));
  LUT4 #(
    .INIT(16'h08F8)) 
    axi_rvalid_i_1__0
       (.I0(s_axi_intr_arvalid),
        .I1(s_axi_intr_arready),
        .I2(s_axi_intr_rvalid),
        .I3(s_axi_intr_rready),
        .O(axi_rvalid_i_1__0_n_0));
  FDRE axi_rvalid_reg
       (.C(s_axi_intr_aclk),
        .CE(1'b1),
        .D(axi_rvalid_i_1__0_n_0),
        .Q(s_axi_intr_rvalid),
        .R(axi_awready_i_1__0_n_0));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT4 #(
    .INIT(16'h0080)) 
    axi_wready_i_1
       (.I0(aw_en_reg_n_0),
        .I1(s_axi_intr_wvalid),
        .I2(s_axi_intr_awvalid),
        .I3(s_axi_intr_wready),
        .O(axi_wready0));
  FDRE axi_wready_reg
       (.C(s_axi_intr_aclk),
        .CE(1'b1),
        .D(axi_wready0),
        .Q(s_axi_intr_wready),
        .R(axi_awready_i_1__0_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    dispatcher_interrupt_pending_i_1
       (.I0(dispatcher_generate_interrupt),
        .I1(from_Dispatcher_reset),
        .O(dispatcher_interrupt_pending_i_1_n_0));
  FDRE dispatcher_interrupt_pending_reg
       (.C(from_Dispatcher_clk),
        .CE(1'b1),
        .D(dispatcher_interrupt_pending_i_1_n_0),
        .Q(dispatcher_interrupt_pending),
        .R(1'b0));
  LUT2 #(
    .INIT(4'hE)) 
    fifo_instrumentation_interrupt_pending_i_1
       (.I0(fifo_instrumentation_full),
        .I1(fifo_instrumentation_almost_full),
        .O(fifo_instrumentation_interrupt_pending0));
  FDRE fifo_instrumentation_interrupt_pending_reg
       (.C(s_axi_intr_aclk),
        .CE(1'b1),
        .D(fifo_instrumentation_interrupt_pending0),
        .Q(fifo_instrumentation_interrupt_pending),
        .R(axi_awready_i_1__0_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    fifo_kernel_to_monitor_interrupt_pending_i_1
       (.I0(fifo_kernel_to_monitor_full),
        .I1(fifo_kernel_to_monitor_almost_full),
        .O(fifo_kernel_to_monitor_interrupt_pending0));
  FDRE fifo_kernel_to_monitor_interrupt_pending_reg
       (.C(s_axi_intr_aclk),
        .CE(1'b1),
        .D(fifo_kernel_to_monitor_interrupt_pending0),
        .Q(fifo_kernel_to_monitor_interrupt_pending),
        .R(axi_awready_i_1__0_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    fifo_monitor_to_kernel_interrupt_pending_i_1
       (.I0(fifo_monitor_to_kernel_full),
        .I1(fifo_monitor_to_kernel_almost_full),
        .O(fifo_monitor_to_kernel_interrupt_pending0));
  FDRE fifo_monitor_to_kernel_interrupt_pending_reg
       (.C(s_axi_intr_aclk),
        .CE(1'b1),
        .D(fifo_monitor_to_kernel_interrupt_pending0),
        .Q(fifo_monitor_to_kernel_interrupt_pending),
        .R(axi_awready_i_1__0_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    fifo_ptm_interrupt_pending_i_1
       (.I0(fifo_ptm_full),
        .I1(fifo_ptm_almost_full),
        .O(fifo_ptm_interrupt_pending0));
  FDRE fifo_ptm_interrupt_pending_reg
       (.C(s_axi_intr_aclk),
        .CE(1'b1),
        .D(fifo_ptm_interrupt_pending0),
        .Q(fifo_ptm_interrupt_pending),
        .R(axi_awready_i_1__0_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    \gen_intr_detection[0].gen_intr_level_detect.gen_intr_active_high_detect.det_intr[0]_i_1 
       (.I0(\intr_reg_n_0_[0] ),
        .I1(det_intr),
        .O(\gen_intr_detection[0].gen_intr_level_detect.gen_intr_active_high_detect.det_intr[0]_i_1_n_0 ));
  FDRE \gen_intr_detection[0].gen_intr_level_detect.gen_intr_active_high_detect.det_intr_reg[0] 
       (.C(s_axi_intr_aclk),
        .CE(1'b1),
        .D(\gen_intr_detection[0].gen_intr_level_detect.gen_intr_active_high_detect.det_intr[0]_i_1_n_0 ),
        .Q(det_intr),
        .R(p_14_out));
  LUT2 #(
    .INIT(4'hE)) 
    \gen_intr_detection[1].gen_intr_level_detect.gen_intr_active_high_detect.det_intr[1]_i_1 
       (.I0(\intr_reg_n_0_[1] ),
        .I1(\gen_intr_detection[1].gen_intr_level_detect.gen_intr_active_high_detect.det_intr_reg ),
        .O(\gen_intr_detection[1].gen_intr_level_detect.gen_intr_active_high_detect.det_intr[1]_i_1_n_0 ));
  FDRE \gen_intr_detection[1].gen_intr_level_detect.gen_intr_active_high_detect.det_intr_reg[1] 
       (.C(s_axi_intr_aclk),
        .CE(1'b1),
        .D(\gen_intr_detection[1].gen_intr_level_detect.gen_intr_active_high_detect.det_intr[1]_i_1_n_0 ),
        .Q(\gen_intr_detection[1].gen_intr_level_detect.gen_intr_active_high_detect.det_intr_reg ),
        .R(p_13_out));
  LUT2 #(
    .INIT(4'hE)) 
    \gen_intr_detection[2].gen_intr_level_detect.gen_intr_active_high_detect.det_intr[2]_i_1 
       (.I0(\intr_reg_n_0_[2] ),
        .I1(\gen_intr_detection[2].gen_intr_level_detect.gen_intr_active_high_detect.det_intr_reg ),
        .O(\gen_intr_detection[2].gen_intr_level_detect.gen_intr_active_high_detect.det_intr[2]_i_1_n_0 ));
  FDRE \gen_intr_detection[2].gen_intr_level_detect.gen_intr_active_high_detect.det_intr_reg[2] 
       (.C(s_axi_intr_aclk),
        .CE(1'b1),
        .D(\gen_intr_detection[2].gen_intr_level_detect.gen_intr_active_high_detect.det_intr[2]_i_1_n_0 ),
        .Q(\gen_intr_detection[2].gen_intr_level_detect.gen_intr_active_high_detect.det_intr_reg ),
        .R(p_12_out));
  LUT2 #(
    .INIT(4'hE)) 
    \gen_intr_detection[4].gen_intr_level_detect.gen_intr_active_high_detect.det_intr[4]_i_1 
       (.I0(\intr_reg_n_0_[4] ),
        .I1(\gen_intr_detection[4].gen_intr_level_detect.gen_intr_active_high_detect.det_intr_reg ),
        .O(\gen_intr_detection[4].gen_intr_level_detect.gen_intr_active_high_detect.det_intr[4]_i_1_n_0 ));
  FDRE \gen_intr_detection[4].gen_intr_level_detect.gen_intr_active_high_detect.det_intr_reg[4] 
       (.C(s_axi_intr_aclk),
        .CE(1'b1),
        .D(\gen_intr_detection[4].gen_intr_level_detect.gen_intr_active_high_detect.det_intr[4]_i_1_n_0 ),
        .Q(\gen_intr_detection[4].gen_intr_level_detect.gen_intr_active_high_detect.det_intr_reg ),
        .R(p_10_out));
  LUT2 #(
    .INIT(4'hE)) 
    \gen_intr_detection[5].gen_intr_level_detect.gen_intr_active_high_detect.det_intr[5]_i_1 
       (.I0(\intr_reg_n_0_[5] ),
        .I1(\gen_intr_detection[5].gen_intr_level_detect.gen_intr_active_high_detect.det_intr_reg ),
        .O(\gen_intr_detection[5].gen_intr_level_detect.gen_intr_active_high_detect.det_intr[5]_i_1_n_0 ));
  FDRE \gen_intr_detection[5].gen_intr_level_detect.gen_intr_active_high_detect.det_intr_reg[5] 
       (.C(s_axi_intr_aclk),
        .CE(1'b1),
        .D(\gen_intr_detection[5].gen_intr_level_detect.gen_intr_active_high_detect.det_intr[5]_i_1_n_0 ),
        .Q(\gen_intr_detection[5].gen_intr_level_detect.gen_intr_active_high_detect.det_intr_reg ),
        .R(p_9_out));
  LUT5 #(
    .INIT(32'h0000EA00)) 
    \gen_intr_detection[5].gen_irq_level.irq_level_high.s_irq_lvl_i_1 
       (.I0(irq),
        .I1(reg_global_intr_en),
        .I2(intr_all),
        .I3(s_axi_intr_aresetn),
        .I4(intr_ack_all),
        .O(\gen_intr_detection[5].gen_irq_level.irq_level_high.s_irq_lvl_i_1_n_0 ));
  FDRE \gen_intr_detection[5].gen_irq_level.irq_level_high.s_irq_lvl_reg 
       (.C(s_axi_intr_aclk),
        .CE(1'b1),
        .D(\gen_intr_detection[5].gen_irq_level.irq_level_high.s_irq_lvl_i_1_n_0 ),
        .Q(irq),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h8)) 
    \gen_intr_reg[0].reg_intr_ack[0]_i_1 
       (.I0(s_axi_intr_wdata[0]),
        .I1(p_21_out),
        .O(\gen_intr_reg[0].reg_intr_ack[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT4 #(
    .INIT(16'h0800)) 
    \gen_intr_reg[0].reg_intr_ack[0]_i_2 
       (.I0(p_0_in[0]),
        .I1(p_0_in[1]),
        .I2(p_0_in[2]),
        .I3(intr_reg_wren__2),
        .O(p_21_out));
  FDRE \gen_intr_reg[0].reg_intr_ack_reg[0] 
       (.C(s_axi_intr_aclk),
        .CE(1'b1),
        .D(\gen_intr_reg[0].reg_intr_ack[0]_i_1_n_0 ),
        .Q(\gen_intr_reg[0].reg_intr_ack_reg_n_0_[0] ),
        .R(p_14_out));
  LUT4 #(
    .INIT(16'h0200)) 
    \gen_intr_reg[0].reg_intr_en[0]_i_1 
       (.I0(p_0_in[0]),
        .I1(p_0_in[1]),
        .I2(p_0_in[2]),
        .I3(intr_reg_wren__2),
        .O(p_24_out));
  LUT4 #(
    .INIT(16'h8000)) 
    \gen_intr_reg[0].reg_intr_en[0]_i_2 
       (.I0(s_axi_intr_awvalid),
        .I1(s_axi_intr_wvalid),
        .I2(s_axi_intr_wready),
        .I3(s_axi_intr_awready),
        .O(intr_reg_wren__2));
  FDRE \gen_intr_reg[0].reg_intr_en_reg[0] 
       (.C(s_axi_intr_aclk),
        .CE(p_24_out),
        .D(s_axi_intr_wdata[0]),
        .Q(\gen_intr_reg[0].reg_intr_en_reg_n_0_[0] ),
        .R(axi_awready_i_1__0_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    \gen_intr_reg[0].reg_intr_pending[0]_i_1 
       (.I0(\gen_intr_reg[0].reg_intr_en_reg_n_0_[0] ),
        .I1(\gen_intr_reg[0].reg_intr_sts_reg_n_0_[0] ),
        .O(p_18_out));
  FDRE \gen_intr_reg[0].reg_intr_pending_reg[0] 
       (.C(s_axi_intr_aclk),
        .CE(1'b1),
        .D(p_18_out),
        .Q(\gen_intr_reg[0].reg_intr_pending_reg_n_0_[0] ),
        .R(p_14_out));
  LUT2 #(
    .INIT(4'hB)) 
    \gen_intr_reg[0].reg_intr_sts[0]_i_1 
       (.I0(\gen_intr_reg[0].reg_intr_ack_reg_n_0_[0] ),
        .I1(s_axi_intr_aresetn),
        .O(p_14_out));
  FDRE \gen_intr_reg[0].reg_intr_sts_reg[0] 
       (.C(s_axi_intr_aclk),
        .CE(1'b1),
        .D(det_intr),
        .Q(\gen_intr_reg[0].reg_intr_sts_reg_n_0_[0] ),
        .R(p_14_out));
  LUT2 #(
    .INIT(4'h8)) 
    \gen_intr_reg[1].reg_intr_ack[1]_i_1 
       (.I0(s_axi_intr_wdata[1]),
        .I1(p_21_out),
        .O(\gen_intr_reg[1].reg_intr_ack[1]_i_1_n_0 ));
  FDRE \gen_intr_reg[1].reg_intr_ack_reg[1] 
       (.C(s_axi_intr_aclk),
        .CE(1'b1),
        .D(\gen_intr_reg[1].reg_intr_ack[1]_i_1_n_0 ),
        .Q(p_3_in),
        .R(p_13_out));
  FDRE \gen_intr_reg[1].reg_intr_en_reg[1] 
       (.C(s_axi_intr_aclk),
        .CE(p_24_out),
        .D(s_axi_intr_wdata[1]),
        .Q(p_0_in15_in),
        .R(axi_awready_i_1__0_n_0));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \gen_intr_reg[1].reg_intr_pending[1]_i_1 
       (.I0(p_0_in15_in),
        .I1(p_1_in16_in),
        .O(p_17_out));
  FDRE \gen_intr_reg[1].reg_intr_pending_reg[1] 
       (.C(s_axi_intr_aclk),
        .CE(1'b1),
        .D(p_17_out),
        .Q(p_3_in4_in),
        .R(p_13_out));
  LUT2 #(
    .INIT(4'hB)) 
    \gen_intr_reg[1].reg_intr_sts[1]_i_1 
       (.I0(p_3_in),
        .I1(s_axi_intr_aresetn),
        .O(p_13_out));
  FDRE \gen_intr_reg[1].reg_intr_sts_reg[1] 
       (.C(s_axi_intr_aclk),
        .CE(1'b1),
        .D(\gen_intr_detection[1].gen_intr_level_detect.gen_intr_active_high_detect.det_intr_reg ),
        .Q(p_1_in16_in),
        .R(p_13_out));
  LUT2 #(
    .INIT(4'h8)) 
    \gen_intr_reg[2].reg_intr_ack[2]_i_1 
       (.I0(s_axi_intr_wdata[2]),
        .I1(p_21_out),
        .O(\gen_intr_reg[2].reg_intr_ack[2]_i_1_n_0 ));
  FDRE \gen_intr_reg[2].reg_intr_ack_reg[2] 
       (.C(s_axi_intr_aclk),
        .CE(1'b1),
        .D(\gen_intr_reg[2].reg_intr_ack[2]_i_1_n_0 ),
        .Q(p_2_in),
        .R(p_12_out));
  FDRE \gen_intr_reg[2].reg_intr_en_reg[2] 
       (.C(s_axi_intr_aclk),
        .CE(p_24_out),
        .D(s_axi_intr_wdata[2]),
        .Q(p_0_in12_in),
        .R(axi_awready_i_1__0_n_0));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \gen_intr_reg[2].reg_intr_pending[2]_i_1 
       (.I0(p_0_in12_in),
        .I1(p_1_in13_in),
        .O(\gen_intr_reg[2].reg_intr_pending[2]_i_1_n_0 ));
  FDRE \gen_intr_reg[2].reg_intr_pending_reg[2] 
       (.C(s_axi_intr_aclk),
        .CE(1'b1),
        .D(\gen_intr_reg[2].reg_intr_pending[2]_i_1_n_0 ),
        .Q(p_2_in3_in),
        .R(p_12_out));
  LUT2 #(
    .INIT(4'hB)) 
    \gen_intr_reg[2].reg_intr_sts[2]_i_1 
       (.I0(p_2_in),
        .I1(s_axi_intr_aresetn),
        .O(p_12_out));
  FDRE \gen_intr_reg[2].reg_intr_sts_reg[2] 
       (.C(s_axi_intr_aclk),
        .CE(1'b1),
        .D(\gen_intr_detection[2].gen_intr_level_detect.gen_intr_active_high_detect.det_intr_reg ),
        .Q(p_1_in13_in),
        .R(p_12_out));
  LUT4 #(
    .INIT(16'h2000)) 
    \gen_intr_reg[3].reg_intr_ack[3]_i_1 
       (.I0(s_axi_intr_aresetn),
        .I1(p_1_in),
        .I2(p_21_out),
        .I3(s_axi_intr_wdata[3]),
        .O(\gen_intr_reg[3].reg_intr_ack[3]_i_1_n_0 ));
  FDRE \gen_intr_reg[3].reg_intr_ack_reg[3] 
       (.C(s_axi_intr_aclk),
        .CE(1'b1),
        .D(\gen_intr_reg[3].reg_intr_ack[3]_i_1_n_0 ),
        .Q(p_1_in),
        .R(1'b0));
  FDRE \gen_intr_reg[3].reg_intr_en_reg[3] 
       (.C(s_axi_intr_aclk),
        .CE(p_24_out),
        .D(s_axi_intr_wdata[3]),
        .Q(p_0_in9_in),
        .R(axi_awready_i_1__0_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    \gen_intr_reg[4].reg_intr_ack[4]_i_1 
       (.I0(s_axi_intr_wdata[4]),
        .I1(p_21_out),
        .O(\gen_intr_reg[4].reg_intr_ack[4]_i_1_n_0 ));
  FDRE \gen_intr_reg[4].reg_intr_ack_reg[4] 
       (.C(s_axi_intr_aclk),
        .CE(1'b1),
        .D(\gen_intr_reg[4].reg_intr_ack[4]_i_1_n_0 ),
        .Q(\gen_intr_reg[4].reg_intr_ack_reg_n_0_[4] ),
        .R(p_10_out));
  FDRE \gen_intr_reg[4].reg_intr_en_reg[4] 
       (.C(s_axi_intr_aclk),
        .CE(p_24_out),
        .D(s_axi_intr_wdata[4]),
        .Q(p_0_in6_in),
        .R(axi_awready_i_1__0_n_0));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \gen_intr_reg[4].reg_intr_pending[4]_i_1 
       (.I0(p_0_in6_in),
        .I1(p_1_in7_in),
        .O(p_8_out));
  FDRE \gen_intr_reg[4].reg_intr_pending_reg[4] 
       (.C(s_axi_intr_aclk),
        .CE(1'b1),
        .D(p_8_out),
        .Q(p_0_in1_in),
        .R(p_10_out));
  LUT2 #(
    .INIT(4'hB)) 
    \gen_intr_reg[4].reg_intr_sts[4]_i_1 
       (.I0(\gen_intr_reg[4].reg_intr_ack_reg_n_0_[4] ),
        .I1(s_axi_intr_aresetn),
        .O(p_10_out));
  FDRE \gen_intr_reg[4].reg_intr_sts_reg[4] 
       (.C(s_axi_intr_aclk),
        .CE(1'b1),
        .D(\gen_intr_detection[4].gen_intr_level_detect.gen_intr_active_high_detect.det_intr_reg ),
        .Q(p_1_in7_in),
        .R(p_10_out));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT4 #(
    .INIT(16'hB080)) 
    \gen_intr_reg[5].reg_global_intr_en[0]_i_1 
       (.I0(s_axi_intr_wdata[0]),
        .I1(p_27_out__2),
        .I2(s_axi_intr_aresetn),
        .I3(reg_global_intr_en),
        .O(reg_global_intr_en0_out));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT4 #(
    .INIT(16'h0100)) 
    \gen_intr_reg[5].reg_global_intr_en[0]_i_2 
       (.I0(p_0_in[0]),
        .I1(p_0_in[1]),
        .I2(p_0_in[2]),
        .I3(intr_reg_wren__2),
        .O(p_27_out__2));
  FDRE \gen_intr_reg[5].reg_global_intr_en_reg[0] 
       (.C(s_axi_intr_aclk),
        .CE(1'b1),
        .D(reg_global_intr_en0_out),
        .Q(reg_global_intr_en),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h8)) 
    \gen_intr_reg[5].reg_intr_ack[5]_i_1 
       (.I0(s_axi_intr_wdata[5]),
        .I1(p_21_out),
        .O(\gen_intr_reg[5].reg_intr_ack[5]_i_1_n_0 ));
  FDRE \gen_intr_reg[5].reg_intr_ack_reg[5] 
       (.C(s_axi_intr_aclk),
        .CE(1'b1),
        .D(\gen_intr_reg[5].reg_intr_ack[5]_i_1_n_0 ),
        .Q(p_0_in0_in),
        .R(p_9_out));
  FDRE \gen_intr_reg[5].reg_intr_en_reg[5] 
       (.C(s_axi_intr_aclk),
        .CE(p_24_out),
        .D(s_axi_intr_wdata[5]),
        .Q(\gen_intr_reg[5].reg_intr_en_reg_n_0_[5] ),
        .R(axi_awready_i_1__0_n_0));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \gen_intr_reg[5].reg_intr_pending[5]_i_1 
       (.I0(\gen_intr_reg[5].reg_intr_en_reg_n_0_[5] ),
        .I1(p_1_in4_in),
        .O(p_5_out));
  FDRE \gen_intr_reg[5].reg_intr_pending_reg[5] 
       (.C(s_axi_intr_aclk),
        .CE(1'b1),
        .D(p_5_out),
        .Q(\gen_intr_reg[5].reg_intr_pending_reg_n_0_[5] ),
        .R(p_9_out));
  LUT2 #(
    .INIT(4'hB)) 
    \gen_intr_reg[5].reg_intr_sts[5]_i_1 
       (.I0(p_0_in0_in),
        .I1(s_axi_intr_aresetn),
        .O(p_9_out));
  FDRE \gen_intr_reg[5].reg_intr_sts_reg[5] 
       (.C(s_axi_intr_aclk),
        .CE(1'b1),
        .D(\gen_intr_detection[5].gen_intr_level_detect.gen_intr_active_high_detect.det_intr_reg ),
        .Q(p_1_in4_in),
        .R(p_9_out));
  FDRE intr_ack_all_ff_reg
       (.C(s_axi_intr_aclk),
        .CE(1'b1),
        .D(intr_ack_all),
        .Q(intr_ack_all_ff),
        .R(axi_awready_i_1__0_n_0));
  LUT3 #(
    .INIT(8'h08)) 
    intr_ack_all_i_1
       (.I0(or_reduction),
        .I1(s_axi_intr_aresetn),
        .I2(intr_ack_all_ff),
        .O(intr_ack_all_i_1_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    intr_ack_all_i_2
       (.I0(p_0_in0_in),
        .I1(\gen_intr_reg[0].reg_intr_ack_reg_n_0_[0] ),
        .I2(p_3_in),
        .I3(p_2_in),
        .I4(\gen_intr_reg[4].reg_intr_ack_reg_n_0_[4] ),
        .I5(p_1_in),
        .O(or_reduction));
  FDRE intr_ack_all_reg
       (.C(s_axi_intr_aclk),
        .CE(1'b1),
        .D(intr_ack_all_i_1_n_0),
        .Q(intr_ack_all),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h00000000FFFFFFFE)) 
    intr_all_i_1
       (.I0(p_3_in4_in),
        .I1(p_2_in3_in),
        .I2(\gen_intr_reg[0].reg_intr_pending_reg_n_0_[0] ),
        .I3(\gen_intr_reg[5].reg_intr_pending_reg_n_0_[5] ),
        .I4(p_0_in1_in),
        .I5(intr_all0),
        .O(intr_all_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT2 #(
    .INIT(4'hB)) 
    intr_all_i_2
       (.I0(intr_ack_all_ff),
        .I1(s_axi_intr_aresetn),
        .O(intr_all0));
  FDRE intr_all_reg
       (.C(s_axi_intr_aclk),
        .CE(1'b1),
        .D(intr_all_i_1_n_0),
        .Q(intr_all),
        .R(1'b0));
  FDRE \intr_reg[0] 
       (.C(s_axi_intr_aclk),
        .CE(1'b1),
        .D(fifo_ptm_interrupt_pending),
        .Q(\intr_reg_n_0_[0] ),
        .R(axi_awready_i_1__0_n_0));
  FDRE \intr_reg[1] 
       (.C(s_axi_intr_aclk),
        .CE(1'b1),
        .D(dispatcher_interrupt_pending),
        .Q(\intr_reg_n_0_[1] ),
        .R(axi_awready_i_1__0_n_0));
  FDRE \intr_reg[2] 
       (.C(s_axi_intr_aclk),
        .CE(1'b1),
        .D(fifo_instrumentation_interrupt_pending),
        .Q(\intr_reg_n_0_[2] ),
        .R(axi_awready_i_1__0_n_0));
  FDRE \intr_reg[4] 
       (.C(s_axi_intr_aclk),
        .CE(1'b1),
        .D(fifo_kernel_to_monitor_interrupt_pending),
        .Q(\intr_reg_n_0_[4] ),
        .R(axi_awready_i_1__0_n_0));
  FDRE \intr_reg[5] 
       (.C(s_axi_intr_aclk),
        .CE(1'b1),
        .D(fifo_monitor_to_kernel_interrupt_pending),
        .Q(\intr_reg_n_0_[5] ),
        .R(axi_awready_i_1__0_n_0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
