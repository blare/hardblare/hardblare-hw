#include "types.h"
#include "no_os.h"
#include "address.h"
#include "decode.h"

#ifdef DEBUG_SIMULATION

inline void init_simulation_memories(){ //}uint32_t *debug_memory,uint32_t *loaded_ptm_traces , uint32_t *loaded_bbt_annotations, uint32_t *instrumentation_memory){
  puts("\n INIT MEMORY FIRMWARE \n");
  memcpy( (uint32_t *)DEBUG_BRAM, debug_memory, sizeof(debug_memory) );
  puts("\n DEBUG_BRAM size : \n");
  print_hex( sizeof(debug_memory) );
  memcpy((uint32_t *)PTM_TRACES_ADDRESS, loaded_ptm_traces, sizeof(loaded_ptm_traces));
  memcpy((uint32_t *)LOADED_BBT_ANNOTATION_ADDRESS, loaded_bbt_annotations, sizeof(loaded_bbt_annotations));
  memcpy((uint32_t *)READ_INSTRUMENTATION, instrumentation_memory, sizeof(instrumentation_memory));
  memcpy((uint32_t *)SECURITY_POLICY_CONFIG, config_security_policy_memory, sizeof(config_security_policy_memory));
  memcpy((uint32_t *)KERNEL_TO_MONITOR, kernel_to_monitor_memory, sizeof(kernel_to_monitor_memory));
  MemoryWrite(FIFO_KERNEL_TO_MONITOR_EMPTY, 0);
  MemoryWrite(FIFO_PTM_EMPTY, 0);
  MemoryWrite(FIFO_PTM_FULL, 0);
  MemoryWrite(FIFO_TMC_EMPTY, 0);
  MemoryWrite(FIFO_TMC_FULL, 0);
  MemoryWrite(FIFO_INSTRUMENTATION_EMPTY, 0);
  MemoryWrite(PLTOPS_GENERATE_INTERRUPT, 0);
  puts("\n START FIRMWARE \n");
}


inline void debug_print_basic_block_table_size(uint32_t basic_block_table_size){
  puts("\n ------------- \n");
  puts("\n basic_block_table_size : \n");
  print_hex(basic_block_table_size);
  puts("\n ------------- \n");
}


inline void debug_print_address_of_basic_block(uint32_t basic_block_address){
  puts("\t Annotations offset : ");
  print_hex(basic_block_address);
  puts("\n");
}


inline void debug_print_trace_value(uint32_t trace_value){
  puts("\n=> Trace : ");
  print_hex(trace_value);
  puts("\n");
}


inline void debug_print_number_of_annotations(uint32_t number_of_annotations){
  puts("\t ");
  print_hex(number_of_annotations);
  puts(" annotations : \n");
}


inline void debug_print_annotation_value(uint32_t annotation_value){
  puts("\t\t * ");
  print_hex(annotation_value);
  puts("\n");
}


inline void debug_read_instrumentation(uint32_t value){
  puts("\t\t\tReaded instrumentation value = ");
  print_hex(value);
  puts("\n");
}


inline void debug_print_TRF_entry(uint32_t reg, uint32_t value){
  puts("\t\t\tTRF[ ");
  print_hex(reg);
  puts(" ] = ");
  print_hex(value);
  puts("\n");
}


inline void debug_decode_init_format(){
  puts("\t\t\tDecode init format \n");
}

inline void debug_decode_tr_format(){
  puts("\t\t\tDecode tr format \n");
}

inline void debug_case_INSTR(){
  puts("\t\t\tINSTR_VALUE_INIT \n");
}

inline void debug_case_LW(){
  puts("\t\t\tLW \n");
}


inline void debug_case_SW(){
  puts("\t\t\tSW \n");
}


inline void debug_case_LCR(){
  puts("\t\t\tLCR \n");
}


inline void debug_case_TLB(){
  puts("\t\t\tTLB \n");
}


inline void debug_case_TAG_RRI(){
  puts("\t\t\tTag RRI \n");
}

inline void debug_case_LUI(){
  puts("\t\t\tLUI \n");
}

inline void debug_case_TAG_reg_Imm(){
  puts("\t\t\tTag reg imm \n");
}

inline void debug_case_TAG_reg_reg(){
  puts("\t\t\tTag reg reg \n");
}

inline void debug_case_TAG_mem_reg(){
  puts("\t\t\tTag mem reg \n");
}

inline void debug_case_TAG_Reg_Imm(){
  puts("\t\t\tTag reg imm \n");
}

inline void debug_case_TAG_arith_log(){
  puts("\t\t\tTag_arith_log \n");
}

inline void debug_case_TAG_check_reg(){
  puts("\t\t\tTag_check_reg \n");
}

inline void debug_ti_format(){
  puts("\t\t\tDebug ti format \n");
}

inline void debug_ti_Tag_mem_tr(){
  puts("\t\t\tDebug Tag_mem_tr \n");
}

inline void debug_ti_Tag_tr_mem(){
  puts("\t\t\tDebug Tag_tr_mem \n");
}

inline void debug_ti_Tag_mem_tr_2(){
  puts("\t\t\tDebug Tag_mem_tr2 \n");
}

inline void debug_ti_Tag_instrumentation_tr(){
  puts("\t\t\tDebug Tag_instrumentation_tr \n");
}

inline void debug_ti_Tag_tr_instrumentation(){
  puts("\t\t\tDebug  \n");
}


inline void debug_ti_Tag_Kblare_tr(){
  puts("\t\t\tDebug Tag_Kblare_tr \n");
}


inline void debug_ti_Tag_tr_Kblare(){
  puts("\t\t\tDebug Tag_tr_Kblare \n");
}


inline void debug_ti_Tag_check_mem(){
  puts("\t\t\tDebug Tag_check_mem \n");
}


inline void debug_ti_Tag_tr_mem_2(){
  puts("\t\t\tDebug Tag_tr_mem_2 \n");
}

inline void debug_raise_IRQ(){
  puts("\t\t ************************* !!! RAISE IRQ !!! **************************** \t\t\n");
}


inline void debug_print_Tag_check_mem(uint32_t destReg, uint32_t valDestReg, uint32_t tag_val_addr, uint32_t srcReg, uint32_t valueOfTag, char *result){
  puts("\t\t\tMem[ TRF[ ");
  print_hex(destReg);
  puts(" ]] = Mem[ ");
  print_hex(valDestReg);
  puts(" ] = ");
  print_hex(tag_val_addr);
  puts(" ");
  puts(result); 
  puts(" TRF[ ");
  print_hex(srcReg);
  puts(" ] = ");
  print_hex(valueOfTag);
  puts("\n");
}




inline void debug_Tag_tr_mem_2(uint32_t destReg, uint32_t srcReg, uint32_t addr, uint32_t  imm, const char *op, uint32_t Tag_value){
  puts("\t\t\tMem[ TRF[ ");
  print_hex(destReg);
  puts(" ] ");
  puts(op);
  puts(" ");
  print_hex(imm);
  puts(" ] = Mem [ ");
  print_hex(addr);
  puts(" ] = TRF[ ");
  print_hex(srcReg);
  puts(" ] = ");
  print_hex(Tag_value);
  puts("\n");
}

inline void debug_Tag_mem_tr(uint32_t destReg, uint32_t srcReg, uint32_t addr, uint32_t  imm, const char *op, uint32_t Tag_value){
  puts("\t\t\tMem[ TRF[ ");
  print_hex(destReg);
  puts(" ] ");
  puts(op);
  puts(" ");
  print_hex(imm);
  puts(" ] = Mem [ ");
  print_hex(addr);
  puts(" ] = TRF[ ");
  print_hex(srcReg);
  puts(" ] = ");
  print_hex(Tag_value);
  puts("\n");
}

inline void debug_Tag_tr_mem(uint32_t destReg, uint32_t srcReg, uint32_t addr, uint32_t  imm, uint32_t valueTag, const char *op){
  puts("\t\t\tTRF[ ");
  print_hex(destReg);
  puts(" ] = Mem[ TRF[ ");
  print_hex(srcReg);
  puts(" ] ");
  puts(op);
  puts(" ");
  print_hex(imm);
  puts(" = Mem [");
  print_hex(addr);
  puts(" ] = ");
  print_hex(valueTag);
  puts("\n");
}

inline void debug_print_TRF_op_imm(uint32_t destReg, uint32_t srcReg,const char *op, uint32_t  imm, uint32_t tagValue){
  
  puts("\t\t\tTRF[ ");
  print_hex(destReg);
  puts(" ] = ");
  puts(" TRF[ ");
  print_hex(srcReg);
  puts(" ] ");
  puts(op);
  puts(" ");
  print_hex(imm);
  puts(" = ");
  print_hex(tagValue);
  puts("\n");
}


inline void debug_print_TRF_imm(uint32_t destReg, uint32_t  imm_raw, uint32_t  tagValue){

  puts("\t\t\tTRF[ ");
  print_hex(destReg);
  puts(" ] = ");
  print_hex(imm_raw);
  puts(" = ");
  print_hex(tagValue);
  puts("\n");

}

inline void debug_print_TRF_reg(uint32_t destReg, uint32_t  src_reg, uint32_t  tagValue){

  puts("\t\t\tTRF[ ");
  print_hex(destReg);
  puts(" ] = TRF[ ");
  print_hex(src_reg);  
  puts(" ]");
  puts(" = ");
  print_hex(tagValue);
  puts("\n");
}



inline void debug_print_mem_reg_offset_reg(uint32_t destReg, uint32_t valueDestReg,uint32_t valueSrcReg, char *op, uint32_t imm, uint32_t address, uint32_t  srcReg){

  puts("\t\t\tMem([ TRF[");
  print_hex(destReg);
  puts(" ] ");
  puts(op);
  puts(" ");
  print_hex(imm);
  puts(" = ");
  print_hex(valueDestReg);
  puts(" ");
  puts(op);
  puts(" ");
  print_hex(imm);
  puts(" == @");
  print_hex(address);
  puts(") = ");
  puts(" TRF[");
  print_hex(srcReg);
  puts(" ] = ");
  print_hex(valueSrcReg);
  puts("\n");

}


inline void debug_print_TRF_TRR(uint32_t  destReg,
                                uint32_t  srcReg1,
                                uint32_t  srcReg2,
                                uint32_t  valueSrcReg1,
                                uint32_t  valueSrcReg2,
                                uint32_t  value,
                                char * ARMopcode) {

  puts("\t\t\t\(");
  puts(ARMopcode);
  puts(") TRF[ ");
  print_hex(destReg);
  puts(" ] = TRF[ ");
  print_hex(srcReg1);
  puts(" ] ");
  puts(ARMopcode);
  puts("] TRF[ ");
  print_hex(srcReg2);
  puts(" ] = ");
  print_hex(valueSrcReg1);
  puts(" ");
  puts(ARMopcode);
  puts(" ");
  print_hex(valueSrcReg2);
  puts(" = ");
  print_hex(value);
  puts("\n");

}


inline void debug_print_TRF_lui(uint32_t destReg, uint32_t imm_raw, uint32_t  srcReg, uint32_t final_value ){

  puts("\t\t\tTRF[");
  print_hex(destReg);
  puts(" ] = ");
  print_hex(imm_raw);
  puts(" << 16 | ");
  puts(" TRF[");
  print_hex(srcReg);
  puts(" ] = ");
  print_hex(final_value);
  puts("\n");

}


inline void debug_waiting_instrumentation_value(){
  puts("\t\t\tWaiting for instrumentation value... \n");
}



inline void debug_print_arm_opcode(uint32_t a, char* op , uint32_t b){
  puts("\n ");
  print_hex(a);
  puts(" ");
  puts(op);
  puts(" ");
  print_hex(b);
  puts(" \n");
}

inline void debug_print_get_kernel_to_monitor(uint32_t value){
  puts("\tKernel to monitor value == ");
  print_hex(value);
  puts("\n");
}

inline void debug_print_waiting_kernel_to_monitor(){
  puts("\tKernel to monitor FIFO waiting ... \n");
}

inline void debug_print_get_kernel_to_monitor_type(char* type){
  puts("\tType (");
  puts(type);
  puts(") \n");
}

inline void debug_print_get_monitor_to_kernel(uint32_t value){
  puts("\tMonitor to kernel value == ");
  print_hex(value);
  puts("\n");
}

inline void debug_print_waiting_monitor_to_kernel(){
  puts("\tMonitor to Kernel FIFO waiting ... \n");
}

inline void debug_print_get_monitor_to_kernel_type(char* type){
  puts("\tType (");
  puts(type);
  puts(") \n");
}

inline void debug_print_syscall_protocol_type(char* type){
  puts("\tType (");
  puts(type);
  puts(") \n");
}

inline void debug_print_syscall_protocol_newline() {
  puts("\n");
}


inline void debug_print_search_and_affect_tag_from_lsm_hook() {
  puts("search_and_affect_tag_from_lsm_hook\n");
}

inline void debug_print_search_tag_from_lsm_hook() {
  puts("search_tag_from_lsm_hook\n");
}


inline void debug_print_search_tag_from_lsm_hook_result_tag(uint32_t value){
  puts("\n\tResulting tag = ");
  print_hex(value);
  puts("\n");
}



inline void debug_print_search_and_affect_value_of_address(uint32_t address, uint32_t value){
  puts("\n\tSearch and affect value of address : FOUND and changed ! -  Mem_addr @[");
  print_hex( address );
  puts("] has NOW the value ");
  print_hex( value );
  puts("\n");
}

inline void debug_print_search_and_affect_value_of_address_not_found(uint32_t address){
  puts("\n\tSearch and affect value of address : NOT FOUND ! -  Mem_addr @[");
  print_hex( address );
  puts("] == ?????????? \n");
}

inline void debug_print_create_mem_entry_with_address(uint32_t address, uint32_t tag){
  puts("\n\tCreate mem entry with address @[");
  print_hex( address );
  puts("] with value ");
  print_hex( tag );
  puts("\n");
}


inline void debug_print_search_value_of_address(uint32_t address, uint32_t tag){
  puts("\n\tSearch value of address FOUND ! - @[");
  print_hex( address );
  puts("] has the value ");
  print_hex( tag );
  puts("\n");
}

inline void debug_print_search_value_of_address_not_found(uint32_t address){
  puts("\n\tSearch value of address NOT FOUND ! - @[");
  print_hex( address );
  puts("] == ?????????? \n");
}

  inline void debug_print_dump_debug_mem(){
  /*
  uint32_t *debug_address_i;
  puts("\n\n\n\n\n ------------------------------ \n");
        puts("\n END OF TRACES : \n\n\n\n\n\n");
        int tmpi = 0;
        for( tmpi = 0, debug_address_i =  (uint32_t *)DEBUG_BRAM; tmpi < 999 ; tmpi++, debug_address_i++)
          {
            puts("Data at ");
            print_hex(tmpi);
            puts(" has value = ");
            print_hex(MemoryRead(debug_address_i));
            puts("\n");
          }
        puts("\n\n\n\n\n\n ------------------------------ \n");
        puts("\n\n\n\n\n\n ------------------------------ \n");
  */
}


#endif
