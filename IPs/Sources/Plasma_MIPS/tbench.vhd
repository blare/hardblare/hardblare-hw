
library ieee;
use ieee.std_logic_1164.all;
use work.mlite_pack.all;
use ieee.std_logic_unsigned.all;

entity tbench is
   	 generic (
        -- Parameters of Axi Slave Bus Interface S00_AXI
        C_S00_AXI_DATA_WIDTH    : integer    := 32;
        C_S00_AXI_ADDR_WIDTH    : integer    := 8;
        
        -- Parameters of Axi Slave Bus Interface S_AXI_INTR
        C_S_AXI_INTR_DATA_WIDTH    : integer    := 32;
        C_S_AXI_INTR_ADDR_WIDTH    : integer    := 5;
        C_NUM_OF_INTR    : integer    := 1;
        C_INTR_SENSITIVITY    : std_logic_vector    := x"FFFFFFFF";
        C_INTR_ACTIVE_STATE    : std_logic_vector    := x"FFFFFFFF";
        C_IRQ_SENSITIVITY    : integer    := 1;
        C_IRQ_ACTIVE_STATE    : integer    := 1
);
end; --entity tbench

architecture logic of tbench is


   constant ARRAY_SIZE : integer := 15;
   constant NUMBER_WAIT_CYCLE : integer := 200;

   signal clk         : std_logic := '0';
   signal reset       : std_logic := '1';  
   signal s_external_irq : STD_LOGIC;
    
    
   signal s_mem_pause_in      : std_logic := '0';
   
   -- Valeurs du TMC
   signal s_external_fifo_read_tmc_en : STD_LOGIC;
   signal s_external_fifo_read_tmc_dout : STD_LOGIC_VECTOR ( 31 downto 0 );
   
   -- Simulation du PTM
   signal s_external_fifo_write_ptm_traces_data_write :  STD_LOGIC_VECTOR ( 31 downto 0 ) := x"00000000";
   signal s_external_fifo_write_ptm_traces_en :  STD_LOGIC := '0'; 

   signal s_external_FIFO_Kernel_to_Monitor_write_data_in : STD_LOGIC_VECTOR ( 31 downto 0 );
   signal s_external_FIFO_Kernel_to_Monitor_write_en : STD_LOGIC;
    
   signal s_i : integer := 0;
   signal s_init_done : std_logic := '0';

   -- Simulation de l'instrumentation
   signal s_fifo_instr_address :  STD_LOGIC_VECTOR ( 31 downto 0 );
   signal s_fifo_instr_bytes_selection :  STD_LOGIC_VECTOR ( 3 downto 0 );
   signal s_fifo_instr_data_write :  STD_LOGIC_VECTOR ( 31 downto 0 );
   signal s_fifo_instr_enable :  STD_LOGIC;

     
   -- PS to PL
   signal  s_from_PS_address : STD_LOGIC_VECTOR(31 downto 0);
   signal  s_from_PS_clk : STD_LOGIC;
   signal  s_from_PS_data_in : STD_LOGIC_VECTOR(31 downto 0);
   signal  s_from_PS_enable : STD_LOGIC;
   signal  s_from_PS_reset: STD_LOGIC;
   signal  s_from_PS_bytes_selection : STD_LOGIC_VECTOR(3 downto 0);

--   signal s_irq         : std_logic;
   
   -- S00_AXI
  signal s_s00_axi_0_awaddr : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal s_s00_axi_0_awlen :  STD_LOGIC_VECTOR ( 7 downto 0 );
  signal s_s00_axi_0_awsize :  STD_LOGIC_VECTOR ( 2 downto 0 );
  signal s_s00_axi_0_awburst :  STD_LOGIC_VECTOR ( 1 downto 0 );
  signal s_s00_axi_0_awlock :  STD_LOGIC_VECTOR ( 0 to 0 );
  signal s_s00_axi_0_awcache :  STD_LOGIC_VECTOR ( 3 downto 0 );
  signal s_s00_axi_0_awprot :  STD_LOGIC_VECTOR ( 2 downto 0 );
  signal s_s00_axi_0_awqos :  STD_LOGIC_VECTOR ( 3 downto 0 );
  signal s_s00_axi_0_awvalid :  STD_LOGIC_VECTOR ( 0 to 0 );
  signal s_s00_axi_0_awready :  STD_LOGIC_VECTOR ( 0 to 0 );
  signal s_s00_axi_0_wdata :  STD_LOGIC_VECTOR ( 31 downto 0 );
  signal s_s00_axi_0_wstrb :  STD_LOGIC_VECTOR ( 3 downto 0 );
  signal s_s00_axi_0_wlast :  STD_LOGIC_VECTOR ( 0 to 0 );
  signal s_s00_axi_0_wvalid :  STD_LOGIC_VECTOR ( 0 to 0 );
  signal s_s00_axi_0_wready :  STD_LOGIC_VECTOR ( 0 to 0 );
  signal s_s00_axi_0_bresp :  STD_LOGIC_VECTOR ( 1 downto 0 );
  signal s_s00_axi_0_bvalid :  STD_LOGIC_VECTOR ( 0 to 0 );
  signal s_s00_axi_0_bready :  STD_LOGIC_VECTOR ( 0 to 0 );
  signal s_s00_axi_0_araddr :  STD_LOGIC_VECTOR ( 15 downto 0 );
  signal s_s00_axi_0_arlen :  STD_LOGIC_VECTOR ( 7 downto 0 );
  signal s_s00_axi_0_arsize :  STD_LOGIC_VECTOR ( 2 downto 0 );
  signal s_s00_axi_0_arburst :  STD_LOGIC_VECTOR ( 1 downto 0 );
  signal s_s00_axi_0_arlock :  STD_LOGIC_VECTOR ( 0 to 0 );
  signal s_s00_axi_0_arcache :  STD_LOGIC_VECTOR ( 3 downto 0 );
  signal s_s00_axi_0_arprot :  STD_LOGIC_VECTOR ( 2 downto 0 );
  signal s_s00_axi_0_arqos :  STD_LOGIC_VECTOR ( 3 downto 0 );
  signal s_s00_axi_0_arvalid :  STD_LOGIC_VECTOR ( 0 to 0 );
  signal s_s00_axi_0_arready :  STD_LOGIC_VECTOR ( 0 to 0 );
  signal s_s00_axi_0_rdata :  STD_LOGIC_VECTOR ( 31 downto 0 );
  signal s_s00_axi_0_rresp :  STD_LOGIC_VECTOR ( 1 downto 0 );
  signal s_s00_axi_0_rlast :  STD_LOGIC_VECTOR ( 0 to 0 );
  signal s_s00_axi_0_rvalid :  STD_LOGIC_VECTOR ( 0 to 0 );
  signal s_s00_axi_0_rready :  STD_LOGIC_VECTOR ( 0 to 0 );
   
   -- S_AXI
--   signal s_s_axi_intr_0_araddr :  STD_LOGIC_VECTOR ( 4 downto 0 );
--   signal s_s_axi_intr_0_arprot :  STD_LOGIC_VECTOR ( 2 downto 0 );
--   signal s_s_axi_intr_0_arready :  STD_LOGIC;
--   signal s_s_axi_intr_0_arvalid :  STD_LOGIC;
--   signal s_s_axi_intr_0_awaddr :  STD_LOGIC_VECTOR ( 4 downto 0 );
--   signal s_s_axi_intr_0_awprot :  STD_LOGIC_VECTOR ( 2 downto 0 );
--   signal s_s_axi_intr_0_awready :  STD_LOGIC;
--   signal s_s_axi_intr_0_awvalid :  STD_LOGIC;
--   signal s_s_axi_intr_0_bready :  STD_LOGIC;
--   signal s_s_axi_intr_0_bresp :  STD_LOGIC_VECTOR ( 1 downto 0 );
--   signal s_s_axi_intr_0_bvalid :  STD_LOGIC;
--   signal s_s_axi_intr_0_rdata :  STD_LOGIC_VECTOR ( 31 downto 0 );
--   signal s_s_axi_intr_0_rready :  STD_LOGIC;
--   signal s_s_axi_intr_0_rresp :  STD_LOGIC_VECTOR ( 1 downto 0 );
--   signal s_s_axi_intr_0_rvalid :  STD_LOGIC;
--   signal s_s_axi_intr_0_wdata :  STD_LOGIC_VECTOR ( 31 downto 0 );
--   signal s_s_axi_intr_0_wready :  STD_LOGIC;
--   signal s_s_axi_intr_0_wstrb :  STD_LOGIC_VECTOR ( 3 downto 0 );
--   signal s_s_axi_intr_0_wvalid :  STD_LOGIC;

   
   -- external_fifo_read_tmc_dout
   
   type ARRAY_32_BITS is array (natural range <>) of std_logic_vector(31 downto 0);
   
   CONSTANT ptm_traces : ARRAY_32_BITS := (x"00000000", x"000102e4",x"00012648",x"0001031c",x"00010d38",x"0001032c",x"0001565c",x"00010348",x"00012648",x"00010368",x"00011998",x"00010384",x"00012648",x"00010398", x"00000000");
  
   CONSTANT instrumentation_values : ARRAY_32_BITS := (x"00000001", x"00000002",x"00000003");
  
   CONSTANT monitor_to_kernel_values : ARRAY_32_BITS := (x"00000001", x"00000002",x"00000003");
  
   CONSTANT kernel_to_monitor_values : ARRAY_32_BITS := (x"00000001", x"00000002",x"00000003");
    
  
   component design_1_wrapper is
     port (
     
        -- Clock IRQ and reset
        external_clock : in STD_LOGIC;
        
        external_reset : in STD_LOGIC;
        
        
        -- FIFO Kernel to Monitor       
        external_FIFO_Kernel_to_Monitor_write_data_in : in STD_LOGIC_VECTOR ( 31 downto 0 );
        external_FIFO_Kernel_to_Monitor_write_en : in STD_LOGIC;
        
        
        -- FIFO instr
        external_fifo_instr_address : in STD_LOGIC_VECTOR ( 31 downto 0 );
        external_fifo_instr_bytes_selection : in STD_LOGIC_VECTOR ( 3 downto 0 );
        external_fifo_instr_data_write : in STD_LOGIC_VECTOR ( 31 downto 0 );
        external_fifo_instr_enable : in STD_LOGIC;
        
        
        -- FIFO TMC
        external_fifo_read_tmc_dout : out STD_LOGIC_VECTOR ( 31 downto 0 );
        external_fifo_read_tmc_en : in STD_LOGIC;
        
        
        -- FIFO PTM
        external_fifo_write_ptm_traces_data_write : in STD_LOGIC_VECTOR ( 31 downto 0 );
        external_fifo_write_ptm_traces_en : in STD_LOGIC;
        
        s00_axi_0_awaddr : in STD_LOGIC_VECTOR ( 15 downto 0 );
        s00_axi_0_awlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
        s00_axi_0_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
        s00_axi_0_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
        s00_axi_0_awlock : in STD_LOGIC_VECTOR ( 0 to 0 );
        s00_axi_0_awcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
        s00_axi_0_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
        s00_axi_0_awqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
        s00_axi_0_awvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
        s00_axi_0_awready : out STD_LOGIC_VECTOR ( 0 to 0 );
        s00_axi_0_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
        s00_axi_0_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
        s00_axi_0_wlast : in STD_LOGIC_VECTOR ( 0 to 0 );
        s00_axi_0_wvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
        s00_axi_0_wready : out STD_LOGIC_VECTOR ( 0 to 0 );
        s00_axi_0_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
        s00_axi_0_bvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
        s00_axi_0_bready : in STD_LOGIC_VECTOR ( 0 to 0 );
        s00_axi_0_araddr : in STD_LOGIC_VECTOR ( 15 downto 0 );
        s00_axi_0_arlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
        s00_axi_0_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
        s00_axi_0_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
        s00_axi_0_arlock : in STD_LOGIC_VECTOR ( 0 to 0 );
        s00_axi_0_arcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
        s00_axi_0_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
        s00_axi_0_arqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
        s00_axi_0_arvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
        s00_axi_0_arready : out STD_LOGIC_VECTOR ( 0 to 0 );
        s00_axi_0_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
        s00_axi_0_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
        s00_axi_0_rlast : out STD_LOGIC_VECTOR ( 0 to 0 );
        s00_axi_0_rvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
        s00_axi_0_rready : in STD_LOGIC_VECTOR ( 0 to 0 )

        -- S_AXI
--        s_axi_intr_0_araddr : in STD_LOGIC_VECTOR ( 4 downto 0 );
--        s_axi_intr_0_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
--        s_axi_intr_0_arready : out STD_LOGIC;
--        s_axi_intr_0_arvalid : in STD_LOGIC;
--        s_axi_intr_0_awaddr : in STD_LOGIC_VECTOR ( 4 downto 0 );
--        s_axi_intr_0_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
--        s_axi_intr_0_awready : out STD_LOGIC;
--        s_axi_intr_0_awvalid : in STD_LOGIC;
--        s_axi_intr_0_bready : in STD_LOGIC;
--        s_axi_intr_0_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
--        s_axi_intr_0_bvalid : out STD_LOGIC;
--        s_axi_intr_0_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
--        s_axi_intr_0_rready : in STD_LOGIC;
--        s_axi_intr_0_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
--        s_axi_intr_0_rvalid : out STD_LOGIC;
--        s_axi_intr_0_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
--        s_axi_intr_0_wready : out STD_LOGIC;
--        s_axi_intr_0_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
--        s_axi_intr_0_wvalid : in STD_LOGIC

       
     );
   end component design_1_wrapper;

begin  --architecture

    clk <= not clk after 50 ns;
    reset <= '0' after 50 ns;     
  
    process
        begin
   
                wait for 450ns;
                              
                
                -- ************************
                --  Writes PTM
                -- ************************
                for s_i in 0 to (ptm_traces'LENGTH + instrumentation_values'LENGTH + monitor_to_kernel_values'LENGTH + 100000) loop
                    
                        if (s_i < ptm_traces'LENGTH) then                            
                            
                            if(s_init_done = '0') then
                            
                                s_external_fifo_write_ptm_traces_en <= '1';
                                s_external_fifo_write_ptm_traces_data_write <= ptm_traces(s_i);
                                
                                wait for 100ns;
                            end if;
                            
                        elsif ((s_i >= ptm_traces'LENGTH) and (s_i < (ptm_traces'LENGTH + instrumentation_values'LENGTH))) then
            
                            if(s_init_done = '0') then
            
                                s_fifo_instr_address <= x"00000000";
                                s_fifo_instr_bytes_selection <= "1111";
                                s_fifo_instr_data_write <= instrumentation_values(s_i - ptm_traces'LENGTH);
                                s_fifo_instr_enable <= '1';
                                
                                wait for 100ns;
                            end if;
                            
                        elsif ( (s_i >= (ptm_traces'LENGTH + instrumentation_values'LENGTH)) and (s_i < (ptm_traces'LENGTH + instrumentation_values'LENGTH + monitor_to_kernel_values'LENGTH)) ) then
                            
                           if(s_init_done = '0') then
                            
                               s_external_FIFO_Kernel_to_Monitor_write_en <= '1';
                               s_external_FIFO_Kernel_to_Monitor_write_data_in <= monitor_to_kernel_values(s_i - ptm_traces'LENGTH - instrumentation_values'LENGTH);
            
                               wait for 100ns;
                               
                            end if;
                            
                        else
                        
                            s_init_done <= '1';   
                            s_external_fifo_write_ptm_traces_en <= '0';
                            s_external_fifo_read_tmc_en <= '0';
                            s_fifo_instr_enable <= '0';
                            s_external_FIFO_Kernel_to_Monitor_write_en <= '0';
                        end if;
                        
                end loop;
       
              

                
  
    end process;
               
   
   integration: design_1_wrapper
      PORT MAP (

     -- Clock IRQ and reset
     external_clock => clk,
     external_reset => reset,
     
     -- Simulation du PTM
     external_fifo_write_ptm_traces_data_write => s_external_fifo_write_ptm_traces_data_write,
     external_fifo_write_ptm_traces_en => s_external_fifo_write_ptm_traces_en,
     
     -- Simulation de l'instrumentation
     external_fifo_instr_address => s_fifo_instr_address,
     external_fifo_instr_bytes_selection => s_fifo_instr_bytes_selection,
     external_fifo_instr_data_write => s_fifo_instr_data_write,
     external_fifo_instr_enable => s_fifo_instr_enable ,
     
     
     -- TMC
     external_fifo_read_tmc_en    => s_external_fifo_read_tmc_en,
     external_fifo_read_tmc_dout  => s_external_fifo_read_tmc_dout,
     
     external_FIFO_Kernel_to_Monitor_write_data_in => s_external_FIFO_Kernel_to_Monitor_write_data_in,
     external_FIFO_Kernel_to_Monitor_write_en => s_external_FIFO_Kernel_to_Monitor_write_en,
            
     
     -- S00_AXI
     s00_axi_0_awaddr      =>    s_s00_axi_0_awaddr ,
     s00_axi_0_awlen       =>   s_s00_axi_0_awlen , 
     s00_axi_0_awsize       =>   s_s00_axi_0_awsize,
     s00_axi_0_awburst     =>    s_s00_axi_0_awburst,
     s00_axi_0_awlock      =>    s_s00_axi_0_awlock,
     s00_axi_0_awcache     =>    s_s00_axi_0_awcache,
     s00_axi_0_awprot     =>     s_s00_axi_0_awprot,
     s00_axi_0_awqos      =>   s_s00_axi_0_awqos ,
     s00_axi_0_awvalid     =>    s_s00_axi_0_awvalid,
     s00_axi_0_awready     =>    s_s00_axi_0_awready,
     s00_axi_0_wdata       =>  s_s00_axi_0_wdata ,
     s00_axi_0_wstrb      =>    s_s00_axi_0_wstrb ,
     s00_axi_0_wlast       =>   s_s00_axi_0_wlast ,
     s00_axi_0_wvalid       =>   s_s00_axi_0_wvalid ,
     s00_axi_0_wready    =>      s_s00_axi_0_wready ,
     s00_axi_0_bresp     =>     s_s00_axi_0_bresp ,
     s00_axi_0_bvalid      =>    s_s00_axi_0_bvalid ,
     s00_axi_0_bready      =>    s_s00_axi_0_bready ,
     s00_axi_0_araddr     =>     s_s00_axi_0_araddr ,
     s00_axi_0_arlen       =>   s_s00_axi_0_arlen ,
     s00_axi_0_arsize        =>  s_s00_axi_0_arsize ,
     s00_axi_0_arburst     =>    s_s00_axi_0_arburst,
     s00_axi_0_arlock      =>    s_s00_axi_0_arlock ,
     s00_axi_0_arcache      =>   s_s00_axi_0_arcache,
     s00_axi_0_arprot   =>       s_s00_axi_0_arprot ,
     s00_axi_0_arqos      =>    s_s00_axi_0_arqos ,
     s00_axi_0_arvalid    =>     s_s00_axi_0_arvalid,
     s00_axi_0_arready     =>    s_s00_axi_0_arready,
     s00_axi_0_rdata      =>    s_s00_axi_0_rdata ,
     s00_axi_0_rresp      =>    s_s00_axi_0_rresp ,
     s00_axi_0_rlast   =>       s_s00_axi_0_rlast ,
     s00_axi_0_rvalid     =>     s_s00_axi_0_rvalid ,
     s00_axi_0_rready       =>   s_s00_axi_0_rready 
 
 
 
 
 
 
 
 
 
     
     -- S_AXI
--     s_axi_intr_0_araddr      =>   s_s_axi_intr_0_araddr    ,
--     s_axi_intr_0_arprot      =>   s_s_axi_intr_0_arprot    ,
--     s_axi_intr_0_arready     =>   s_s_axi_intr_0_arready   ,
--     s_axi_intr_0_arvalid     =>   s_s_axi_intr_0_arvalid   ,
--     s_axi_intr_0_awaddr      =>   s_s_axi_intr_0_awaddr    ,
--     s_axi_intr_0_awprot      =>   s_s_axi_intr_0_awprot    ,
--     s_axi_intr_0_awready     =>   s_s_axi_intr_0_awready   ,
--     s_axi_intr_0_awvalid     =>   s_s_axi_intr_0_awvalid   ,
--     s_axi_intr_0_bready      =>   s_s_axi_intr_0_bready    ,
--     s_axi_intr_0_bresp       =>   s_s_axi_intr_0_bresp     ,
--     s_axi_intr_0_bvalid      =>   s_s_axi_intr_0_bvalid    ,
--     s_axi_intr_0_rdata       =>   s_s_axi_intr_0_rdata     ,
--     s_axi_intr_0_rready      =>   s_s_axi_intr_0_rready    ,
--     s_axi_intr_0_rresp       =>   s_s_axi_intr_0_rresp     ,
--     s_axi_intr_0_rvalid      =>   s_s_axi_intr_0_rvalid    ,
--     s_axi_intr_0_wdata       =>   s_s_axi_intr_0_wdata     ,
--     s_axi_intr_0_wready      =>   s_s_axi_intr_0_wready    ,
--     s_axi_intr_0_wstrb       =>   s_s_axi_intr_0_wstrb     ,
--     s_axi_intr_0_wvalid      =>   s_s_axi_intr_0_wvalid  

);


end; --architecture logic
