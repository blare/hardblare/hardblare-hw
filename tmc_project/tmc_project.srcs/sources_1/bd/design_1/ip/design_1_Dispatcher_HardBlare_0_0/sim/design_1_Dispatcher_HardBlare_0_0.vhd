-- (c) Copyright 1995-2020 Xilinx, Inc. All rights reserved.
-- 
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
-- 
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
-- 
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
-- 
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES.
-- 
-- DO NOT MODIFY THIS FILE.

-- IP VLNV: user.org:user:Dispatcher_HardBlare:42.0
-- IP Revision: 14

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY design_1_Dispatcher_HardBlare_0_0 IS
  PORT (
    clk : IN STD_LOGIC;
    reset : IN STD_LOGIC;
    bram_firmware_code_en : OUT STD_LOGIC;
    bram_firmware_code_address : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    bram_firmware_code_data_write : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    bram_firmware_code_bytes_selection : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    bram_firmware_code_data_read : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    bram_firmware_code_clk : OUT STD_LOGIC;
    bram_firmware_code_reset : OUT STD_LOGIC;
    bram_bbt_annotations_en : OUT STD_LOGIC;
    bram_bbt_annotations_data_read : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    bram_bbt_annotations_address : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    bram_bbt_annotations_data_write : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    bram_bbt_annotations_bytes_selection : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    bram_bbt_annotations_clk : OUT STD_LOGIC;
    bram_bbt_annotations_reset : OUT STD_LOGIC;
    fifo_ptm_en : OUT STD_LOGIC;
    fifo_ptm_data_read : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    fifo_ptm_empty : IN STD_LOGIC;
    fifo_ptm_almost_empty : IN STD_LOGIC;
    fifo_ptm_full : IN STD_LOGIC;
    fifo_ptm_almost_full : IN STD_LOGIC;
    fifo_ptm_clk : OUT STD_LOGIC;
    fifo_ptm_reset : OUT STD_LOGIC;
    fifo_monitor_to_kernel_en : OUT STD_LOGIC;
    fifo_monitor_to_kernel_data_write : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    fifo_monitor_to_kernel_empty : IN STD_LOGIC;
    fifo_monitor_to_kernel_almost_empty : IN STD_LOGIC;
    fifo_monitor_to_kernel_full : IN STD_LOGIC;
    fifo_monitor_to_kernel_almost_full : IN STD_LOGIC;
    fifo_monitor_to_kernel_clk : OUT STD_LOGIC;
    fifo_monitor_to_kernel_reset : OUT STD_LOGIC;
    fifo_kernel_to_monitor_en : OUT STD_LOGIC;
    fifo_kernel_to_monitor_data_read : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    fifo_kernel_to_monitor_empty : IN STD_LOGIC;
    fifo_kernel_to_monitor_almost_empty : IN STD_LOGIC;
    fifo_kernel_to_monitor_full : IN STD_LOGIC;
    fifo_kernel_to_monitor_almost_full : IN STD_LOGIC;
    fifo_kernel_to_monitor_clk : OUT STD_LOGIC;
    fifo_kernel_to_monitor_reset : OUT STD_LOGIC;
    fifo_read_instrumentation_request_read_en : OUT STD_LOGIC;
    fifo_read_instrumentation_data_read : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    fifo_instrumentation_empty : IN STD_LOGIC;
    fifo_instrumentation_almost_empty : IN STD_LOGIC;
    fifo_instrumentation_full : IN STD_LOGIC;
    fifo_instrumentation_almost_full : IN STD_LOGIC;
    bram_debug_en : OUT STD_LOGIC;
    bram_debug_address : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    bram_debug_data_write : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    bram_debug_bytes_selection : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    bram_debug_data_read : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    bram_debug_clk : OUT STD_LOGIC;
    bram_debug_reset : OUT STD_LOGIC;
    PLtoPS_clk : OUT STD_LOGIC;
    PLtoPS_reset : OUT STD_LOGIC;
    PLtoPS_enable : OUT STD_LOGIC;
    PLtoPS_address : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    PLtoPS_write_data : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    PLtoPS_bytes_selection : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    PLtoPS_readed_data : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    PLtoPS_generate_interrupt : OUT STD_LOGIC;
    bram_sec_policy_config_en : OUT STD_LOGIC;
    bram_sec_policy_config_data_read : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    bram_sec_policy_config_address : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    bram_sec_policy_config_data_write : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    bram_sec_policy_config_bytes_selection : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    bram_sec_policy_config_clk : OUT STD_LOGIC;
    bram_sec_policy_config_reset : OUT STD_LOGIC
  );
END design_1_Dispatcher_HardBlare_0_0;

ARCHITECTURE design_1_Dispatcher_HardBlare_0_0_arch OF design_1_Dispatcher_HardBlare_0_0 IS
  ATTRIBUTE DowngradeIPIdentifiedWarnings : STRING;
  ATTRIBUTE DowngradeIPIdentifiedWarnings OF design_1_Dispatcher_HardBlare_0_0_arch: ARCHITECTURE IS "yes";
  COMPONENT Dispatcher_Plasma_MIPS_HardBlare IS
    GENERIC (
      memory_type : STRING;
      log_file : STRING;
      ethernet : STD_LOGIC;
      use_cache : STD_LOGIC
    );
    PORT (
      clk : IN STD_LOGIC;
      reset : IN STD_LOGIC;
      bram_firmware_code_en : OUT STD_LOGIC;
      bram_firmware_code_address : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      bram_firmware_code_data_write : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      bram_firmware_code_bytes_selection : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
      bram_firmware_code_data_read : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      bram_firmware_code_clk : OUT STD_LOGIC;
      bram_firmware_code_reset : OUT STD_LOGIC;
      bram_bbt_annotations_en : OUT STD_LOGIC;
      bram_bbt_annotations_data_read : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      bram_bbt_annotations_address : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      bram_bbt_annotations_data_write : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      bram_bbt_annotations_bytes_selection : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
      bram_bbt_annotations_clk : OUT STD_LOGIC;
      bram_bbt_annotations_reset : OUT STD_LOGIC;
      fifo_ptm_en : OUT STD_LOGIC;
      fifo_ptm_data_read : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      fifo_ptm_empty : IN STD_LOGIC;
      fifo_ptm_almost_empty : IN STD_LOGIC;
      fifo_ptm_full : IN STD_LOGIC;
      fifo_ptm_almost_full : IN STD_LOGIC;
      fifo_ptm_clk : OUT STD_LOGIC;
      fifo_ptm_reset : OUT STD_LOGIC;
      fifo_monitor_to_kernel_en : OUT STD_LOGIC;
      fifo_monitor_to_kernel_data_write : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      fifo_monitor_to_kernel_empty : IN STD_LOGIC;
      fifo_monitor_to_kernel_almost_empty : IN STD_LOGIC;
      fifo_monitor_to_kernel_full : IN STD_LOGIC;
      fifo_monitor_to_kernel_almost_full : IN STD_LOGIC;
      fifo_monitor_to_kernel_clk : OUT STD_LOGIC;
      fifo_monitor_to_kernel_reset : OUT STD_LOGIC;
      fifo_kernel_to_monitor_en : OUT STD_LOGIC;
      fifo_kernel_to_monitor_data_read : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      fifo_kernel_to_monitor_empty : IN STD_LOGIC;
      fifo_kernel_to_monitor_almost_empty : IN STD_LOGIC;
      fifo_kernel_to_monitor_full : IN STD_LOGIC;
      fifo_kernel_to_monitor_almost_full : IN STD_LOGIC;
      fifo_kernel_to_monitor_clk : OUT STD_LOGIC;
      fifo_kernel_to_monitor_reset : OUT STD_LOGIC;
      fifo_read_instrumentation_request_read_en : OUT STD_LOGIC;
      fifo_read_instrumentation_data_read : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      fifo_instrumentation_empty : IN STD_LOGIC;
      fifo_instrumentation_almost_empty : IN STD_LOGIC;
      fifo_instrumentation_full : IN STD_LOGIC;
      fifo_instrumentation_almost_full : IN STD_LOGIC;
      bram_debug_en : OUT STD_LOGIC;
      bram_debug_address : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      bram_debug_data_write : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      bram_debug_bytes_selection : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
      bram_debug_data_read : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      bram_debug_clk : OUT STD_LOGIC;
      bram_debug_reset : OUT STD_LOGIC;
      PLtoPS_clk : OUT STD_LOGIC;
      PLtoPS_reset : OUT STD_LOGIC;
      PLtoPS_enable : OUT STD_LOGIC;
      PLtoPS_address : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      PLtoPS_write_data : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      PLtoPS_bytes_selection : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
      PLtoPS_readed_data : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      PLtoPS_generate_interrupt : OUT STD_LOGIC;
      bram_sec_policy_config_en : OUT STD_LOGIC;
      bram_sec_policy_config_data_read : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      bram_sec_policy_config_address : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      bram_sec_policy_config_data_write : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      bram_sec_policy_config_bytes_selection : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
      bram_sec_policy_config_clk : OUT STD_LOGIC;
      bram_sec_policy_config_reset : OUT STD_LOGIC
    );
  END COMPONENT Dispatcher_Plasma_MIPS_HardBlare;
  ATTRIBUTE IP_DEFINITION_SOURCE : STRING;
  ATTRIBUTE IP_DEFINITION_SOURCE OF design_1_Dispatcher_HardBlare_0_0_arch: ARCHITECTURE IS "package_project";
BEGIN
  U0 : Dispatcher_Plasma_MIPS_HardBlare
    GENERIC MAP (
      memory_type => "XILINX_16X",
      log_file => "UNUSED",
      ethernet => '0',
      use_cache => '0'
    )
    PORT MAP (
      clk => clk,
      reset => reset,
      bram_firmware_code_en => bram_firmware_code_en,
      bram_firmware_code_address => bram_firmware_code_address,
      bram_firmware_code_data_write => bram_firmware_code_data_write,
      bram_firmware_code_bytes_selection => bram_firmware_code_bytes_selection,
      bram_firmware_code_data_read => bram_firmware_code_data_read,
      bram_firmware_code_clk => bram_firmware_code_clk,
      bram_firmware_code_reset => bram_firmware_code_reset,
      bram_bbt_annotations_en => bram_bbt_annotations_en,
      bram_bbt_annotations_data_read => bram_bbt_annotations_data_read,
      bram_bbt_annotations_address => bram_bbt_annotations_address,
      bram_bbt_annotations_data_write => bram_bbt_annotations_data_write,
      bram_bbt_annotations_bytes_selection => bram_bbt_annotations_bytes_selection,
      bram_bbt_annotations_clk => bram_bbt_annotations_clk,
      bram_bbt_annotations_reset => bram_bbt_annotations_reset,
      fifo_ptm_en => fifo_ptm_en,
      fifo_ptm_data_read => fifo_ptm_data_read,
      fifo_ptm_empty => fifo_ptm_empty,
      fifo_ptm_almost_empty => fifo_ptm_almost_empty,
      fifo_ptm_full => fifo_ptm_full,
      fifo_ptm_almost_full => fifo_ptm_almost_full,
      fifo_ptm_clk => fifo_ptm_clk,
      fifo_ptm_reset => fifo_ptm_reset,
      fifo_monitor_to_kernel_en => fifo_monitor_to_kernel_en,
      fifo_monitor_to_kernel_data_write => fifo_monitor_to_kernel_data_write,
      fifo_monitor_to_kernel_empty => fifo_monitor_to_kernel_empty,
      fifo_monitor_to_kernel_almost_empty => fifo_monitor_to_kernel_almost_empty,
      fifo_monitor_to_kernel_full => fifo_monitor_to_kernel_full,
      fifo_monitor_to_kernel_almost_full => fifo_monitor_to_kernel_almost_full,
      fifo_monitor_to_kernel_clk => fifo_monitor_to_kernel_clk,
      fifo_monitor_to_kernel_reset => fifo_monitor_to_kernel_reset,
      fifo_kernel_to_monitor_en => fifo_kernel_to_monitor_en,
      fifo_kernel_to_monitor_data_read => fifo_kernel_to_monitor_data_read,
      fifo_kernel_to_monitor_empty => fifo_kernel_to_monitor_empty,
      fifo_kernel_to_monitor_almost_empty => fifo_kernel_to_monitor_almost_empty,
      fifo_kernel_to_monitor_full => fifo_kernel_to_monitor_full,
      fifo_kernel_to_monitor_almost_full => fifo_kernel_to_monitor_almost_full,
      fifo_kernel_to_monitor_clk => fifo_kernel_to_monitor_clk,
      fifo_kernel_to_monitor_reset => fifo_kernel_to_monitor_reset,
      fifo_read_instrumentation_request_read_en => fifo_read_instrumentation_request_read_en,
      fifo_read_instrumentation_data_read => fifo_read_instrumentation_data_read,
      fifo_instrumentation_empty => fifo_instrumentation_empty,
      fifo_instrumentation_almost_empty => fifo_instrumentation_almost_empty,
      fifo_instrumentation_full => fifo_instrumentation_full,
      fifo_instrumentation_almost_full => fifo_instrumentation_almost_full,
      bram_debug_en => bram_debug_en,
      bram_debug_address => bram_debug_address,
      bram_debug_data_write => bram_debug_data_write,
      bram_debug_bytes_selection => bram_debug_bytes_selection,
      bram_debug_data_read => bram_debug_data_read,
      bram_debug_clk => bram_debug_clk,
      bram_debug_reset => bram_debug_reset,
      PLtoPS_clk => PLtoPS_clk,
      PLtoPS_reset => PLtoPS_reset,
      PLtoPS_enable => PLtoPS_enable,
      PLtoPS_address => PLtoPS_address,
      PLtoPS_write_data => PLtoPS_write_data,
      PLtoPS_bytes_selection => PLtoPS_bytes_selection,
      PLtoPS_readed_data => PLtoPS_readed_data,
      PLtoPS_generate_interrupt => PLtoPS_generate_interrupt,
      bram_sec_policy_config_en => bram_sec_policy_config_en,
      bram_sec_policy_config_data_read => bram_sec_policy_config_data_read,
      bram_sec_policy_config_address => bram_sec_policy_config_address,
      bram_sec_policy_config_data_write => bram_sec_policy_config_data_write,
      bram_sec_policy_config_bytes_selection => bram_sec_policy_config_bytes_selection,
      bram_sec_policy_config_clk => bram_sec_policy_config_clk,
      bram_sec_policy_config_reset => bram_sec_policy_config_reset
    );
END design_1_Dispatcher_HardBlare_0_0_arch;
