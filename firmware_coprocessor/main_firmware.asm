$main:
	lui a2,0x2000
	li a0,1
	sll v1,v0,0x4
	addu v1,v1,a2
	lw a1,0(v1)
	addiu v1,v0,1
	sltiu v0,v0,41
	movz v1,a0,v0
	beqz a1,8 $main+0x8
	move v0,v1
	lui t1,0x4000
	lui	t0,0x2000
	addiu	t2,t1,4
	addiu	t3,t1,20
	lw	a2,0(t1)
	sll	a2,a2,0x3
	beqz	a2,7c $main
	nop
	lw	v0,0(t2)
	beq	a1,v0,90 <main+0x90>
	addiu	v0,t1,36
	j	6c <main+0x6c>
	move	v1,zero
	lw	a0,0(v0)
	move	a3,v0
	beq	a1,a0,94 <main+0x94>
	addiu	v0,v0,32
	addiu	v1,v1,8
	sltu	a0,v1,a2
	bnez	a0,5c <main+0x5c>
	nop
	lw	a1,0(t0)
	bnez	a1,38 <main+0x38>
	addiu	t0,t0,16
	jr	ra
	move	v0,zero
	move	a3,t2
	lw	v1,16(a3)
	beqz	v1,7c <main+0x7c>
	nop
	addu	v0,v1,t2
	lw	a0,0(v0)
	beqz	a0,7c <main+0x7c>
	nop
	addu	v1,v1,t3
	move	v0,zero
	addiu	v0,v0,1
	lw	a1,0(v1)
	bne	v0,a0,b8 <main+0xb8>
	addiu	v1,v1,16
	lw	a1,0(t0)
	bnez	a1,38 <main+0x38>
	addiu	t0,t0,16
	j	88 <main+0x88>
	nop
