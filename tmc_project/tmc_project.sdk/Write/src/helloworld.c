/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* XILINX CONSORTIUM BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include "platform.h"
#include "ps7_init.h"
#include "xparameters.h"

void print(char *str);

int main()
{

	int volatile * const p_reg = (int *) 0x1c000234;

	int i = 0;
	int delay = 0;

    init_platform();

    ps7_post_config();

 //   *p_reg = 0xabcdabcd;

    xil_printf("Hello World\n\r");


if (i == 0)
{
    Xil_Out32(XPAR_AXI_BRAM_CTRL_0_BASEADDR, 0x82101234);
    Xil_Out32(XPAR_AXI_BRAM_CTRL_0_BASEADDR + 4, 0x00000000);
    Xil_Out32(XPAR_AXI_BRAM_CTRL_0_BASEADDR + 8, 0x00000000);
    Xil_Out32(XPAR_AXI_BRAM_CTRL_0_BASEADDR + 12, 0x00000000);
    Xil_Out32(XPAR_AXI_BRAM_CTRL_0_BASEADDR + 16, 0x00000000);
    Xil_Out32(XPAR_AXI_BRAM_CTRL_0_BASEADDR + 20, 0x00000000);
    Xil_Out32(XPAR_AXI_BRAM_CTRL_0_BASEADDR + 24, 0xae100bcd);
    Xil_Out32(XPAR_AXI_BRAM_CTRL_0_BASEADDR + 28, 0x00000000);
    Xil_Out32(XPAR_AXI_BRAM_CTRL_0_BASEADDR + 32, 0x00000000);
    Xil_Out32(XPAR_AXI_BRAM_CTRL_0_BASEADDR + 36, 0x00000000);
    Xil_Out32(XPAR_AXI_BRAM_CTRL_0_BASEADDR + 40, 0x00000000);
    Xil_Out32(XPAR_AXI_BRAM_CTRL_0_BASEADDR + 44, 0x00000000);
    Xil_Out32(XPAR_AXI_BRAM_CTRL_0_BASEADDR + 48, 0x10100000);
    Xil_Out32(XPAR_AXI_BRAM_CTRL_0_BASEADDR + 52, 0x00000000);
    Xil_Out32(XPAR_AXI_BRAM_CTRL_0_BASEADDR + 56, 0x00000000);
    Xil_Out32(XPAR_AXI_BRAM_CTRL_0_BASEADDR + 60, 0x00000000);
    Xil_Out32(XPAR_AXI_BRAM_CTRL_0_BASEADDR + 64, 0x00000000);
    Xil_Out32(XPAR_AXI_BRAM_CTRL_0_BASEADDR + 68, 0x00000000);
    Xil_Out32(XPAR_AXI_BRAM_CTRL_0_BASEADDR + 72, 0x8021beef);
    Xil_Out32(XPAR_AXI_BRAM_CTRL_0_BASEADDR + 76, 0x00000000);
    Xil_Out32(XPAR_AXI_BRAM_CTRL_0_BASEADDR + 80, 0x00000000);
    Xil_Out32(XPAR_AXI_BRAM_CTRL_0_BASEADDR + 84, 0x00000000);
    Xil_Out32(XPAR_AXI_BRAM_CTRL_0_BASEADDR + 88, 0x00000000);
    Xil_Out32(XPAR_AXI_BRAM_CTRL_0_BASEADDR + 92, 0x00000000);
    Xil_Out32(XPAR_AXI_BRAM_CTRL_0_BASEADDR + 96, 0xac21dead);
    Xil_Out32(XPAR_AXI_BRAM_CTRL_0_BASEADDR + 100, 0x00000000);
    Xil_Out32(XPAR_AXI_BRAM_CTRL_0_BASEADDR + 104, 0x00000000);
    Xil_Out32(XPAR_AXI_BRAM_CTRL_0_BASEADDR + 108, 0x00000000);
    Xil_Out32(XPAR_AXI_BRAM_CTRL_0_BASEADDR + 112, 0x00000000);
    Xil_Out32(XPAR_AXI_BRAM_CTRL_0_BASEADDR + 116, 0x00000000);
    Xil_Out32(XPAR_AXI_BRAM_CTRL_0_BASEADDR + 120, 0xb0201000);
    Xil_Out32(XPAR_AXI_BRAM_CTRL_0_BASEADDR + 124, 0x00000000);
    Xil_Out32(XPAR_AXI_BRAM_CTRL_0_BASEADDR + 128, 0x00000000);
    Xil_Out32(XPAR_AXI_BRAM_CTRL_0_BASEADDR + 132, 0x00000000);
    Xil_Out32(XPAR_AXI_BRAM_CTRL_0_BASEADDR + 136, 0x00000000);
    Xil_Out32(XPAR_AXI_BRAM_CTRL_0_BASEADDR + 140, 0x00000000);
    Xil_Out32(XPAR_AXI_BRAM_CTRL_0_BASEADDR + 144, 0xb40b0000);

    i = 1;
}

    if (i == 1)
    {
    	Xil_Out32(XPAR_AXI_BRAM_CTRL_1_BASEADDR, 0xffffffff);
    	i = 0;
    }

    while(delay < 10000)
    {
    	delay = delay + 1;
    }

    while (1)
    {
    //  getchar();

    //  Xil_DCacheFlushRange(0x1c000000, 4*1);
      xil_printf("The value at address 0x1c000234 is %x\n\r", Xil_In32(0x1c000234));
   //   xil_printf("The value at address %x is %x\n\r", p_reg, *p_reg);

    }
    cleanup_platform();
    return 0;
}
