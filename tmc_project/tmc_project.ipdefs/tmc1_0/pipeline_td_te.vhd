library IEEE; 
use ieee.std_logic_1164.All;
use work.dependencies_pack.all;

entity pipeline_TD_TE is 
port (
    clk, reset      : in  std_logic;
    pause           : in  std_logic;
    stall           : in  std_logic;
    a_in            : in  std_logic_vector(31 downto 0);
    a_out           : out std_logic_vector(31 downto 0);

    b_in            : in  std_logic_vector(31 downto 0);
    b_out           : out std_logic_vector(31 downto 0);

    en_read_instrumentation     : in  std_logic;
    en_read_instrumentation_o   : out std_logic;

    en_read_kblare_ps2pl        : in  std_logic;
    en_read_kblare_ps2pl_o      : out std_logic;

    en_write_kblare             : in  std_logic;
    en_write_kblare_o           :out std_logic;

    write_pl2ps_addr            : in std_logic_vector(31 downto 0);
    write_pl2ps_addr_o          : out std_logic_vector(31 downto 0);
    
    imm_in          : in  std_logic_vector(15 downto 0);
    imm_out         : out std_logic_vector(15 downto 0);

    reg_source_1    : in  std_logic_vector(31 downto 0);
    reg_source_1_out: out std_logic_vector(31 downto 0);

    reg_source_2    : in  std_logic_vector(31 downto 0);
    reg_source_2_out: out std_logic_vector(31 downto 0);

    reg_pc          : in  std_logic_vector(31 downto 0);
    reg_pc_out      : out std_logic_vector(31 downto 0);

    reg_instr       : in  std_logic_vector(31 downto 0);
    reg_instr_out   : out std_logic_vector(31 downto 0);    

    rs_index        : in  std_logic_vector(4 downto 0);
    rs_index_out    : out std_logic_vector(4 downto 0);

    rs2_index       : in  std_logic_vector(4 downto 0);
    rs2_index_out   : out std_logic_vector(4 downto 0);

    rd_index        : in  std_logic_vector(4 downto 0);
    rd_index_out    : out std_logic_vector(4 downto 0);

    c_mux           : in  reg_dst_source_type;
    c_mux_o         : out reg_dst_source_type;

    tag_check_src   : in  tag_check_type;
    tag_check_src_o : out tag_check_type;

    mem_source      : in  mem_source_type;
    mem_source_o    : out mem_source_type;

    tag_mem_source  : in  tag_mem_source_type;
    tag_mem_source_o: out tag_mem_source_type;

    virtual_mem_src : in  virtual_mem_source_type;
    virtual_mem_src_o: out virtual_mem_source_type;

    rf_en           : in  std_logic;
    rf_en_out       : out std_logic;

    rf_w_en         : in  std_logic;
    rf_w_en_out     : out std_logic;

    fp_rf_en        : in  std_logic;
    fp_rf_en_out    : out std_logic;

    fp_rf_w_en      : in  std_logic;
    fp_rf_w_en_out  : out std_logic;

    mmu_write_en    : in  std_logic;
    mmu_write_en_o  : out std_logic;

    conf_rf_wen     : in  std_logic;
    conf_rf_wen_o   : out std_logic;
    
    config_reg_index        : in  std_logic_vector(3 downto 0);
    config_reg_index_out    : out std_logic_vector(3 downto 0);

    tag_alu_func_i  : in  tag_alu_function_type;
    tag_alu_func    : out tag_alu_function_type );
end entity; 

architecture behavioral of pipeline_TD_TE is 
    
signal en_write_kblare_reg      : std_logic;
signal write_pl2ps_addr_reg     : std_logic_vector(31 downto 0);
signal a_out_reg                : std_logic_vector(31 downto 0);
signal b_out_reg                : std_logic_vector(31 downto 0);
signal reg_source_1_out_reg     : std_logic_vector(31 downto 0);
signal reg_source_2_out_reg     : std_logic_vector(31 downto 0);
signal imm_out_reg              : std_logic_vector(15 downto 0);
signal tag_alu_func_reg         : tag_alu_function_type;
signal rs_index_out_reg         : std_logic_vector(4 downto 0);
signal rs2_index_out_reg        : std_logic_vector(4 downto 0);
signal rd_index_out_reg         : std_logic_vector(4 downto 0);
signal config_reg_index_out_reg : std_logic_vector(3 downto 0);
signal c_mux_reg                : reg_dst_source_type;
signal reg_pc_reg               : std_logic_vector(31 downto 0);
signal reg_instr_reg            : std_logic_vector(31 downto 0);
signal tag_check_src_reg        : tag_check_type;
signal tag_mem_source_reg       : tag_mem_source_type;
signal mem_source_reg           : mem_source_type;
signal rf_en_reg                : std_logic;
signal rf_w_en_reg              : std_logic;
signal fp_rf_en_reg             : std_logic;
signal fp_rf_w_en_reg           : std_logic;
signal mmu_write_en_reg         : std_logic;
signal stall_r                  : std_logic;
signal virtual_mem_src_reg      : virtual_mem_source_type;
signal en_read_instrumentation_r: std_logic;
signal en_read_kblare_ps2pl_r   : std_logic;
signal conf_rf_wen_reg          : std_logic;
--architecture
begin

process(clk) 
begin
    if rising_edge(clk) then 
        if (reset = '1' or stall = '1') then 
            en_write_kblare_reg <= '0';
            write_pl2ps_addr_reg<= (others => '0');
            a_out_reg           <= (others => '0');
            b_out_reg           <= (others => '0');
            reg_source_1_out_reg<= (others => '0');
            reg_source_2_out_reg<= (others => '0');
            imm_out_reg         <= (others => '0');
            tag_alu_func_reg    <= (others => '0');
            rs_index_out_reg    <= (others => '0');
            rs2_index_out_reg   <= (others => '0');
            rd_index_out_reg    <= (others => '0');
            config_reg_index_out_reg <= (others => '0');
            c_mux_reg           <= (others => '0');
            reg_pc_reg          <= (others => '0');
            reg_instr_reg       <= (others => '0');
            tag_check_src_reg   <= (others => '0');
            tag_mem_source_reg  <= (others => '0');
            mem_source_reg      <= (others => '0');
            virtual_mem_src_reg <= (others => '0');
            conf_rf_wen_reg     <= '0';
            rf_en_reg           <= '0';
            rf_w_en_reg         <= '0';
            fp_rf_en_reg        <= '0';
            fp_rf_w_en_reg      <= '0';
            mmu_write_en_reg    <= '0';
            en_read_instrumentation_r <= '0';
            en_read_kblare_ps2pl_r <= '0';
            stall_r <= '0';
        else
            --   stall_r <= stall;
            --if pause = '0' then        
                en_write_kblare_reg <= en_write_kblare;
                write_pl2ps_addr_reg<= write_pl2ps_addr;
                a_out_reg           <= a_in;
                b_out_reg           <= b_in;
                reg_source_1_out_reg<= reg_source_1;
                reg_source_2_out_reg<= reg_source_2;
                imm_out_reg         <= imm_in;
                tag_alu_func_reg    <= tag_alu_func_i;
                rs_index_out_reg    <= rs_index;
                rs2_index_out_reg   <= rs2_index;
                rd_index_out_reg    <= rd_index;
                config_reg_index_out_reg <= config_reg_index;
                reg_pc_reg          <= reg_pc;
                reg_instr_reg       <= reg_instr;
                c_mux_reg           <= c_mux;
                tag_mem_source_reg  <= tag_mem_source;
                mem_source_reg      <= mem_source;
                virtual_mem_src_reg <= virtual_mem_src;
                tag_check_src_reg   <= tag_check_src;
                rf_en_reg           <= rf_en;
                rf_w_en_reg         <= rf_w_en;
                fp_rf_en_reg        <= fp_rf_en;
                fp_rf_w_en_reg      <= fp_rf_w_en;
                mmu_write_en_reg    <= mmu_write_en;
                conf_rf_wen_reg     <= conf_rf_wen;
                en_read_instrumentation_r <= en_read_instrumentation;
                en_read_kblare_ps2pl_r <= en_read_kblare_ps2pl;
            --elsif (stall = '1') then 
            --    en_write_kblare_reg <= '0';
            --    write_pl2ps_addr_reg<= (others => '0');
            --    a_out_reg           <= (others => '0');
            --    b_out_reg           <= (others => '0');
            --    reg_source_1_out_reg<= (others => '0');
            --    reg_source_2_out_reg<= (others => '0');
            --    imm_out_reg         <= (others => '0');
            --    tag_alu_func_reg    <= (others => '0');
            --    rs_index_out_reg    <= (others => '0');
            --    rs2_index_out_reg   <= (others => '0');
            --    rd_index_out_reg    <= (others => '0');
            --    config_reg_index_out_reg <= (others => '0');
            --    c_mux_reg           <= (others => '0');
            --    reg_pc_reg          <= (others => '0');
            --    reg_instr_reg       <= (others => '0');
            --    tag_check_src_reg   <= (others => '0');
            --    tag_mem_source_reg  <= (others => '0');
            --    mem_source_reg      <= (others => '0');
            --    virtual_mem_src_reg <= (others => '0');
            --    conf_rf_wen_reg     <= '0';
            --    rf_en_reg           <= '0';
            --    rf_w_en_reg         <= '0';
            --    fp_rf_en_reg        <= '0';
            --    fp_rf_w_en_reg      <= '0';
            --    mmu_write_en_reg    <= '0';
            --    en_read_instrumentation_r <= '0';
            --    en_read_kblare_ps2pl_r <= '0';
            --else 
            --    en_write_kblare_reg <= en_write_kblare_reg;
            --    write_pl2ps_addr_reg<= write_pl2ps_addr_reg;
            --    a_out_reg             <= a_out_reg        ;
            --    b_out_reg             <= b_out_reg        ;
            --    reg_source_1_out_reg<= reg_source_1_out_reg;
            --    reg_source_2_out_reg<= reg_source_2_out_reg;
            --    imm_out_reg       <= imm_out_reg      ;
            --    tag_alu_func_reg    <= tag_alu_func_reg ;
            --    rs_index_out_reg    <= rs_index_out_reg ;
            --    rs2_index_out_reg   <= rs2_index_out_reg;
            --    rd_index_out_reg    <= rd_index_out_reg ;
            --    config_reg_index_out_reg <= config_reg_index_out_reg;
            --    reg_pc_reg          <= reg_pc_reg;
            --    reg_instr_reg       <= reg_instr_reg;
            --    c_mux_reg           <= c_mux_reg;
            --    tag_mem_source_reg  <= tag_mem_source_reg;
            --    mem_source_reg      <= mem_source_reg;
            --    tag_check_src_reg   <= tag_check_src_reg;
            --    rf_en_reg           <= rf_en_reg;
            --    rf_w_en_reg         <= rf_w_en_reg;
            --    fp_rf_en_reg        <= fp_rf_en_reg;
            --    fp_rf_w_en_reg      <= fp_rf_w_en_reg;
            --    mmu_write_en_reg    <= mmu_write_en_reg;
            --    conf_rf_wen_reg     <= conf_rf_wen_reg;
            --    virtual_mem_src_reg <= virtual_mem_src_reg;
            --    en_read_instrumentation_r <= en_read_instrumentation_r;
            --    en_read_kblare_ps2pl_r <= en_read_kblare_ps2pl_r;
            --end if;
        end if;
    end if;
end process;

en_write_kblare_o<= en_write_kblare_reg;
write_pl2ps_addr_o<=write_pl2ps_addr_reg;
a_out           <=  a_out_reg ;
b_out           <=  b_out_reg ;
--a_out           <=  a_in ;
--b_out           <=  b_in ;
reg_source_1_out<= reg_source_1_out_reg;
reg_source_2_out<= reg_source_2_out_reg;
imm_out         <=  imm_out_reg ;
tag_alu_func    <=  tag_alu_func_reg ;
rs_index_out    <=  rs_index_out_reg ;
rs2_index_out   <=  rs2_index_out_reg;
rd_index_out    <=  rd_index_out_reg ;
config_reg_index_out <= config_reg_index_out_reg;
c_mux_o         <=  c_mux_reg;
reg_pc_out      <= reg_pc_reg;
reg_instr_out   <= reg_instr_reg;
tag_check_src_o <= tag_check_src_reg;
tag_mem_source_o<= tag_mem_source_reg;
mem_source_o    <= mem_source_reg;
rf_en_out       <= rf_en_reg;
rf_w_en_out     <= rf_w_en_reg;
fp_rf_en_out    <= fp_rf_en_reg;
fp_rf_w_en_out  <= fp_rf_w_en_reg;
mmu_write_en_o  <= mmu_write_en_reg;
virtual_mem_src_o <= virtual_mem_src_reg;
en_read_instrumentation_o <= en_read_instrumentation_r;
en_read_kblare_ps2pl_o <= en_read_kblare_ps2pl_r;
conf_rf_wen_o   <= conf_rf_wen_reg;
end architecture;