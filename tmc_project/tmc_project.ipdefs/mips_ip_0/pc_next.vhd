---------------------------------------------------------------------
-- TITLE: Program Counter Next
-- AUTHOR: Steve Rhoads (rhoadss@yahoo.com)
-- DATE CREATED: 2/8/01
-- FILENAME: pc_next.vhd
-- PROJECT: MIPS CPU core
-- COPYRIGHT: Software placed into the public domain by the author.
--    Software 'as is' without warranty.  Author liable for nothing.
-- DESCRIPTION:
--    Implements the Program Counter logic.
---------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use work.mips_pack.all;

entity pc_next is
   port(clk          : in  std_logic;
        reset_in     : in  std_logic;
        stall        : in  std_logic;
        --pc_reg       : in  std_logic_vector(31 downto 2);
        pc_new       : in  std_logic_vector(31 downto 2);
        take_branch  : in  std_logic;
        pause_in     : in  std_logic;
        opcode25_0   : in  std_logic_vector(25 downto 0);
        pc_source    : in  pc_source_type;
        i_mem_enable : out std_logic;
        pc_future    : out std_logic_vector(31 downto 0);
        pc_prev      : out std_logic_vector(31 downto 0);
        pc_out       : out std_logic_vector(31 downto 0));
end; --pc_next

architecture logic of pc_next is 
   --signal pc_next_s : std_logic_vector(31 downto 2);
  attribute keep_hierarchy : string;
  attribute keep_hierarchy of logic : architecture is "yes";
  signal i_mem_enable_s : std_logic;
  signal pc_inc_r : std_logic_vector(31 downto 2) := (others => '0');
  signal pc_reg : std_logic_vector(31 downto 2);
  signal pc_inc_s : std_logic_vector(31 downto 2) := (others => '0');
  signal pc_next_s : std_logic_vector(31 downto 2) := (others => '0');
--   attribute keep of pc_incr_
begin

--pc_next: process(clk, pc_new, take_branch, reset_in, i_mem_enable_s,
--                 opcode25_0, pc_source, pc_reg, stall, pc_inc_r )
pc_next: process(clk)
  
begin     
   
   --if stall = '1' then 
   --    i_mem_enable_s <= '0';
   --    pc_inc := pc_inc_r;
   --    pc_next := pc_reg;
   --else
   --    pc_inc := bv_increment(pc_reg);
   --    i_mem_enable_s <= '1';
   --end if;
       
   if rising_edge(clk) then
    if reset_in = '1' then
      pc_inc_r <= (others => '0');
      pc_reg <= (others => '0');
    --elsif stall = '1' then 
    --  pc_inc_r<= pc_inc_r;
    --  pc_reg <= pc_reg;
    else
      pc_inc_r <= pc_inc_s;
      pc_reg <= pc_next_s;
      --i_mem_enable <= i_mem_enable_s;
    end if;
  end if;
end process;

process(reset_in,stall)
begin
  if reset_in = '1' then 
    i_mem_enable <= '0';
  elsif stall = '1' then
    i_mem_enable <= '0';
  else
    i_mem_enable <= '1';   
  end if; 
end process;


process(reset_in,stall, pc_reg, pc_inc_r, opcode25_0, take_branch, pc_source, pc_new)
variable pc_inc, pc_next : std_logic_vector(31 downto 2) := ZERO(31 downto 2);
begin
  if reset_in = '1' then 
    pc_inc := (others => '0');
    pc_next := (others => '0');
  elsif stall = '1' then
    pc_inc := pc_inc_r;
    pc_next := pc_reg;
  else
    pc_inc := bv_increment(pc_reg);
    case (pc_source) is
      when (from_inc4) =>
        pc_next := pc_inc;
      when (from_opcode25_0) =>
        pc_next := pc_reg(31 downto 28) & opcode25_0;
      when (from_branch) | (from_lbranch) =>
        if take_branch = '1' then
          pc_next := pc_new;
        else
          pc_next := pc_inc;
        end if;
      when others =>
        pc_next := pc_reg;   
    end case;
  end if;
  pc_next_s <= pc_next;
  pc_inc_s <= pc_inc;
end process;
  
  pc_future <= pc_next_s & "00";
  pc_out <= pc_next_s & "00";
end; --logic
