----------
--
-- AUTHOR               : M.Abdul WAHAB
-- DATE                 : 2017
-- Description          : Register Bank 
----------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.mips_pack.all;

entity reg_bank is
   port(clk            : in  std_logic;
        reset          : in  std_logic;
        pause          : in  std_logic;
        rs_index       : in  std_logic_vector(5 downto 0);
        rt_index       : in  std_logic_vector(5 downto 0);
        rd_index       : in  std_logic_vector(5 downto 0);
        reg_source_out : out std_logic_vector(31 downto 0);
        reg_target_out : out std_logic_vector(31 downto 0);
        reg_dest_new   : in  std_logic_vector(31 downto 0);
        intr_enable    : out std_logic);
end; --entity reg_bank

-- AUTHOR: Steve Rhoads (rhoadss@yahoo.com)
architecture logic of reg_bank is
    signal reg31, reg01, reg02, reg03 : std_logic_vector(31 downto 0) := (31 downto 0 => '0') ;
    signal reg04, reg05, reg06, reg07 : std_logic_vector(31 downto 0) := (31 downto 0 => '0') ;
    signal reg08, reg09, reg10, reg11 : std_logic_vector(31 downto 0) := (31 downto 0 => '0') ;
    signal reg12, reg13, reg14, reg15 : std_logic_vector(31 downto 0) := (31 downto 0 => '0') ;
    signal reg16, reg17, reg18, reg19 : std_logic_vector(31 downto 0) := (31 downto 0 => '0') ;
    signal reg20, reg21, reg22, reg23 : std_logic_vector(31 downto 0) := (31 downto 0 => '0') ;
    signal reg24, reg25, reg26, reg27 : std_logic_vector(31 downto 0) := (others => '0');
    signal reg28, reg29, reg30        : std_logic_vector(31 downto 0) := (others => '0');
    signal reg_epc                    : std_logic_vector(31 downto 0) := (others => '0');
    signal reg_status                 : std_logic := '0';
    signal reg_source_out_s           : std_logic_vector(31 downto 0);
    signal reg_target_out_s           : std_logic_vector(31 downto 0);
begin
-- reg01 <= X"00000001";
-- reg02 <= X"00000002";
-- reg03 <= X"00000003";
-- reg04 <= X"00000004";
-- reg05 <= X"00000005";
-- reg06 <= X"00000006";
-- reg07 <= X"00000007";
-- reg08 <= X"00000008";
-- reg09 <= X"00000009";
-- reg10 <= X"0000000A";
-- reg11 <= X"0000000B";
-- reg12 <= X"0000000C";
-- reg13 <= X"0000000D";
-- reg14 <= X"0000000E";
-- reg15 <= X"0000000F";
-- reg16 <= X"00000010";
-- reg17 <= X"00000011";
-- reg18 <= X"00000012";
-- reg19 <= X"00000013";
-- reg20 <= X"00000014";

reg_proc: process(clk, rs_index, rt_index, rd_index, reg_dest_new,
   reg31, reg01, reg02, reg03, reg04, reg05, reg06, reg07,
   reg08, reg09, reg10, reg11, reg12, reg13, reg14, reg15,
   reg16, reg17, reg18, reg19, reg20, reg21, reg22, reg23,
   reg24, reg25, reg26, reg27, reg28, reg29, reg30,
   reg_epc, reg_status)
begin
   
   case rs_index is
   when "000000" => reg_source_out_s <= ZERO;
   when "000001" => reg_source_out_s <= reg01;
   when "000010" => reg_source_out_s <= reg02;
   when "000011" => reg_source_out_s <= reg03;
   when "000100" => reg_source_out_s <= reg04;
   when "000101" => reg_source_out_s <= reg05;
   when "000110" => reg_source_out_s <= reg06;
   when "000111" => reg_source_out_s <= reg07;
   when "001000" => reg_source_out_s <= reg08;
   when "001001" => reg_source_out_s <= reg09;
   when "001010" => reg_source_out_s <= reg10;
   when "001011" => reg_source_out_s <= reg11;
   when "001100" => reg_source_out_s <= reg12;
   when "001101" => reg_source_out_s <= reg13;
   when "001110" => reg_source_out_s <= reg14;
   when "001111" => reg_source_out_s <= reg15;
   when "010000" => reg_source_out_s <= reg16;
   when "010001" => reg_source_out_s <= reg17;
   when "010010" => reg_source_out_s <= reg18;
   when "010011" => reg_source_out_s <= reg19;
   when "010100" => reg_source_out_s <= reg20;
   when "010101" => reg_source_out_s <= reg21;
   when "010110" => reg_source_out_s <= reg22;
   when "010111" => reg_source_out_s <= reg23;
   when "011000" => reg_source_out_s <= reg24;
   when "011001" => reg_source_out_s <= reg25;
   when "011010" => reg_source_out_s <= reg26;
   when "011011" => reg_source_out_s <= reg27;
   when "011100" => reg_source_out_s <= reg28;
   when "011101" => reg_source_out_s <= reg29;
   when "011110" => reg_source_out_s <= reg30;
   when "011111" => reg_source_out_s <= reg31;
   when "101100" => reg_source_out_s <= ZERO(31 downto 1) & reg_status;
   when "101110" => reg_source_out_s <= reg_epc;     --CP0 14
   when "111111" => reg_source_out_s <= '1' & ZERO(30 downto 0); --intr vector
   when others =>   reg_source_out_s <= ZERO;
   end case;

   case rt_index is
   when "000000" => reg_target_out_s <= ZERO;
   when "000001" => reg_target_out_s <= reg01;
   when "000010" => reg_target_out_s <= reg02;
   when "000011" => reg_target_out_s <= reg03;
   when "000100" => reg_target_out_s <= reg04;
   when "000101" => reg_target_out_s <= reg05;
   when "000110" => reg_target_out_s <= reg06;
   when "000111" => reg_target_out_s <= reg07;
   when "001000" => reg_target_out_s <= reg08;
   when "001001" => reg_target_out_s <= reg09;
   when "001010" => reg_target_out_s <= reg10;
   when "001011" => reg_target_out_s <= reg11;
   when "001100" => reg_target_out_s <= reg12;
   when "001101" => reg_target_out_s <= reg13;
   when "001110" => reg_target_out_s <= reg14;
   when "001111" => reg_target_out_s <= reg15;
   when "010000" => reg_target_out_s <= reg16;
   when "010001" => reg_target_out_s <= reg17;
   when "010010" => reg_target_out_s <= reg18;
   when "010011" => reg_target_out_s <= reg19;
   when "010100" => reg_target_out_s <= reg20;
   when "010101" => reg_target_out_s <= reg21;
   when "010110" => reg_target_out_s <= reg22;
   when "010111" => reg_target_out_s <= reg23;
   when "011000" => reg_target_out_s <= reg24;
   when "011001" => reg_target_out_s <= reg25;
   when "011010" => reg_target_out_s <= reg26;
   when "011011" => reg_target_out_s <= reg27;
   when "011100" => reg_target_out_s <= reg28;
   when "011101" => reg_target_out_s <= reg29;
   when "011110" => reg_target_out_s <= reg30;
   when "011111" => reg_target_out_s <= reg31;
   when others =>   reg_target_out_s <= ZERO;
   end case;
   
--      assert reg_dest_new'last_event >= 10 ns
--         report "Reg_dest timing error";
  if rising_edge(clk) then
      case rd_index is
      when "000001" => reg01 <= reg_dest_new;
      when "000010" => reg02 <= reg_dest_new;
      when "000011" => reg03 <= reg_dest_new;
      when "000100" => reg04 <= reg_dest_new;
      when "000101" => reg05 <= reg_dest_new;
      when "000110" => reg06 <= reg_dest_new;
      when "000111" => reg07 <= reg_dest_new;
      when "001000" => reg08 <= reg_dest_new;
      when "001001" => reg09 <= reg_dest_new;
      when "001010" => reg10 <= reg_dest_new;
      when "001011" => reg11 <= reg_dest_new;
      when "001100" => reg12 <= reg_dest_new;
      when "001101" => reg13 <= reg_dest_new;
      when "001110" => reg14 <= reg_dest_new;
      when "001111" => reg15 <= reg_dest_new;
      when "010000" => reg16 <= reg_dest_new;
      when "010001" => reg17 <= reg_dest_new;
      when "010010" => reg18 <= reg_dest_new;
      when "010011" => reg19 <= reg_dest_new;
      when "010100" => reg20 <= reg_dest_new;
      when "010101" => reg21 <= reg_dest_new;
      when "010110" => reg22 <= reg_dest_new;
      when "010111" => reg23 <= reg_dest_new;
      when "011000" => reg24 <= reg_dest_new;
      when "011001" => reg25 <= reg_dest_new;
      when "011010" => reg26 <= reg_dest_new;
      when "011011" => reg27 <= reg_dest_new;
      when "011100" => reg28 <= reg_dest_new;
      when "011101" => reg29 <= reg_dest_new;
      when "011110" => reg30 <= reg_dest_new;
      when "011111" => reg31 <= reg_dest_new;
      when "101100" => reg_status <= reg_dest_new(0);
      when "101110" => reg_epc <= reg_dest_new;  --CP0 14
                       reg_status <= '0';        --disable interrupts
      when others =>
      end case;
   end if;   
   intr_enable <= reg_status;
end process;

  process(rs_index, rd_index, reg_dest_new, reg_source_out_s)
  begin
    if rs_index /= ZERO(5 downto 0) and rs_index = rd_index then
    --if rs_index = rd_index then
      reg_source_out <= reg_dest_new;
    else
      reg_source_out <= reg_source_out_s;
    end if;
  end process;

  process(rt_index, rd_index, reg_dest_new, reg_target_out_s)
  begin
    if rt_index /= ZERO(5 downto 0) and rt_index = rd_index then
    --if rt_index = rd_index then
      reg_target_out <= reg_dest_new;
    else
      reg_target_out <= reg_target_out_s;
    end if;
  end process;

end; --architecture logic

-- AUTHOR: MAW
--architecture structural of reg_bank is 
--type reg_array_t is array (0 to 31) of 
--                    std_logic_vector(31 downto 0);


--signal addr_read1, addr_read2 : std_logic_vector(4 downto 0);
--signal addr_write             : std_logic_vector(4 downto 0);
--signal data_out_1, data_out_2 : std_logic_vector(31 downto 0);
--signal write_enable           : std_logic;
--signal intr_enable_reg        : std_logic;

--begin 

--  reg_proc: process(clk, rs_index, rt_index, rd_index, reg_dest_new,
--                    intr_enable_reg, data_out_1, data_out_2, reset, 
--                    pause)
--    begin
--      if rs_index = "101110" then -- reg_epc CP0 14
--        addr_read1 <= (others => '0');
--      else 
--        addr_read1 <= rs_index(4 downto 0);
--      end if;

--      case rs_index is 
--      when "000000" => reg_source_out_s <= ZERO;
--      when "101100" => reg_source_out_s <= ZERO(31 downto 1) & intr_enable_reg;
--                    --interrupt vector address = 0x3c
--      when "111111" => reg_source_out_s <= ZERO(31 downto 8) & "00111100";
--      when others   => reg_source_out_s <= data_out_1;
--    end case;

--    --setup for second dual-port memory
--    addr_read2 <= rt_index(4 downto 0);
--    case rt_index is
--    when "000000" => reg_target_out_s <= ZERO;
--    when others   => reg_target_out_s <= data_out_2;
--    end case;

--    --setup write port for both dual-port memories
--    if rd_index /= "000000" and rd_index /= "101100" and pause = '0' then
--      write_enable <= '1';
--    else
--      write_enable <= '0';
--    end if;
--    if rd_index = "101110" then  --reg_epc CP0 14
--      addr_write <= "00000";
--    else
--      addr_write <= rd_index(4 downto 0);
--    end if;

--    if reset = '1' then
--      intr_enable_reg <= '0';
--    elsif rising_edge(clk) then
--      if rd_index = "101110" then     --reg_epc CP0 14
--         intr_enable_reg <= '0';      --disable interrupts
--      elsif rd_index = "101100" then
--         intr_enable_reg <= reg_dest_new(0);
--      end if;
--    end if;

--    intr_enable <= intr_enable_reg;

--  end process;

--ram_proc: process(clk, addr_read1, addr_read2)
--  --, addr_write, 
--  --                reg_dest_new, write_enable )
--  variable regf_s : reg_array_t := (others => (others=>'0'));
--  begin      
--    data_out_1 <= regf_s(to_integer(unsigned(addr_read1)));
--    data_out_2 <= regf_s(to_integer(unsigned(addr_read2)));
--    if rising_edge(clk) then    
--      if write_enable = '1' then
--        regf_s(to_integer(unsigned(addr_write))) := reg_dest_new;
--      end if; 
--    end if; -- clk
--end process;
--end architecture;

