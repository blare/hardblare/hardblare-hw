--Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
--Date        : Tue Dec 10 21:24:50 2019
--Host        : HardBlare-Workstation running 64-bit Ubuntu 16.04.6 LTS
--Command     : generate_target Dispatcher_Plasma_MIPS_HardBlare_wrapper.bd
--Design      : Dispatcher_Plasma_MIPS_HardBlare_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity Dispatcher_Plasma_MIPS_HardBlare_wrapper is
  port (
    bram_annotations_address : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_annotations_bytes_selection : out STD_LOGIC_VECTOR ( 3 downto 0 );
    bram_annotations_clk : out STD_LOGIC;
    bram_annotations_data_read : in STD_LOGIC;
    bram_annotations_data_write : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_annotations_en : out STD_LOGIC;
    bram_annotations_reset : out STD_LOGIC;
    bram_bbt_annotations_address : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_bbt_annotations_bytes_selection : out STD_LOGIC_VECTOR ( 3 downto 0 );
    bram_bbt_annotations_clk : out STD_LOGIC;
    bram_bbt_annotations_data_read : in STD_LOGIC;
    bram_bbt_annotations_data_write : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_bbt_annotations_en : out STD_LOGIC;
    bram_bbt_annotations_reset : out STD_LOGIC;
    bram_firmware_code_address : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_firmware_code_bytes_selection : out STD_LOGIC_VECTOR ( 3 downto 0 );
    bram_firmware_code_clk : out STD_LOGIC;
    bram_firmware_code_data_read : in STD_LOGIC;
    bram_firmware_code_data_write : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_firmware_code_en : out STD_LOGIC;
    bram_firmware_code_reset : out STD_LOGIC;
    bram_kernel_to_monitor_address : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_kernel_to_monitor_bytes_selection : out STD_LOGIC_VECTOR ( 3 downto 0 );
    bram_kernel_to_monitor_clk : out STD_LOGIC;
    bram_kernel_to_monitor_data_read : in STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_kernel_to_monitor_data_write : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_kernel_to_monitor_en : out STD_LOGIC;
    bram_kernel_to_monitor_reset : out STD_LOGIC;
    bram_monitor_to_kernel_address : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_monitor_to_kernel_bytes_selection : out STD_LOGIC_VECTOR ( 3 downto 0 );
    bram_monitor_to_kernel_clk : out STD_LOGIC;
    bram_monitor_to_kernel_data_read : in STD_LOGIC;
    bram_monitor_to_kernel_data_write : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_monitor_to_kernel_en : out STD_LOGIC;
    bram_monitor_to_kernel_reset : out STD_LOGIC;
    bram_ptm_traces_address : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_ptm_traces_bytes_selection : out STD_LOGIC_VECTOR ( 3 downto 0 );
    bram_ptm_traces_clk : out STD_LOGIC;
    bram_ptm_traces_data_read : in STD_LOGIC;
    bram_ptm_traces_data_write : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_ptm_traces_en : out STD_LOGIC;
    bram_ptm_traces_reset : out STD_LOGIC;
    clk : in STD_LOGIC;
    reset : in STD_LOGIC
  );
end Dispatcher_Plasma_MIPS_HardBlare_wrapper;

architecture STRUCTURE of Dispatcher_Plasma_MIPS_HardBlare_wrapper is
  component Dispatcher_Plasma_MIPS_HardBlare is
  port (
    clk : in STD_LOGIC;
    reset : in STD_LOGIC;
    bram_firmware_code_data_read : in STD_LOGIC;
    bram_ptm_traces_data_read : in STD_LOGIC;
    bram_bbt_annotations_data_read : in STD_LOGIC;
    bram_annotations_data_read : in STD_LOGIC;
    bram_monitor_to_kernel_data_read : in STD_LOGIC;
    bram_kernel_to_monitor_data_read : in STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_firmware_code_en : out STD_LOGIC;
    bram_firmware_code_reset : out STD_LOGIC;
    bram_firmware_code_clk : out STD_LOGIC;
    bram_firmware_code_address : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_firmware_code_data_write : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_ptm_traces_data_write : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_ptm_traces_address : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_ptm_traces_en : out STD_LOGIC;
    bram_ptm_traces_bytes_selection : out STD_LOGIC_VECTOR ( 3 downto 0 );
    bram_firmware_code_bytes_selection : out STD_LOGIC_VECTOR ( 3 downto 0 );
    bram_ptm_traces_reset : out STD_LOGIC;
    bram_ptm_traces_clk : out STD_LOGIC;
    bram_bbt_annotations_en : out STD_LOGIC;
    bram_bbt_annotations_clk : out STD_LOGIC;
    bram_bbt_annotations_address : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_bbt_annotations_reset : out STD_LOGIC;
    bram_bbt_annotations_data_write : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_bbt_annotations_bytes_selection : out STD_LOGIC_VECTOR ( 3 downto 0 );
    bram_annotations_en : out STD_LOGIC;
    bram_annotations_reset : out STD_LOGIC;
    bram_annotations_clk : out STD_LOGIC;
    bram_annotations_address : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_annotations_data_write : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_annotations_bytes_selection : out STD_LOGIC_VECTOR ( 3 downto 0 );
    bram_monitor_to_kernel_en : out STD_LOGIC;
    bram_monitor_to_kernel_reset : out STD_LOGIC;
    bram_monitor_to_kernel_clk : out STD_LOGIC;
    bram_monitor_to_kernel_address : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_monitor_to_kernel_data_write : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_monitor_to_kernel_bytes_selection : out STD_LOGIC_VECTOR ( 3 downto 0 );
    bram_kernel_to_monitor_en : out STD_LOGIC;
    bram_kernel_to_monitor_reset : out STD_LOGIC;
    bram_kernel_to_monitor_clk : out STD_LOGIC;
    bram_kernel_to_monitor_address : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_kernel_to_monitor_data_write : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_kernel_to_monitor_bytes_selection : out STD_LOGIC_VECTOR ( 3 downto 0 )
  );
  end component Dispatcher_Plasma_MIPS_HardBlare;
begin
Dispatcher_Plasma_MIPS_HardBlare_i: component Dispatcher_Plasma_MIPS_HardBlare
     port map (
      bram_annotations_address(31 downto 0) => bram_annotations_address(31 downto 0),
      bram_annotations_bytes_selection(3 downto 0) => bram_annotations_bytes_selection(3 downto 0),
      bram_annotations_clk => bram_annotations_clk,
      bram_annotations_data_read => bram_annotations_data_read,
      bram_annotations_data_write(31 downto 0) => bram_annotations_data_write(31 downto 0),
      bram_annotations_en => bram_annotations_en,
      bram_annotations_reset => bram_annotations_reset,
      bram_bbt_annotations_address(31 downto 0) => bram_bbt_annotations_address(31 downto 0),
      bram_bbt_annotations_bytes_selection(3 downto 0) => bram_bbt_annotations_bytes_selection(3 downto 0),
      bram_bbt_annotations_clk => bram_bbt_annotations_clk,
      bram_bbt_annotations_data_read => bram_bbt_annotations_data_read,
      bram_bbt_annotations_data_write(31 downto 0) => bram_bbt_annotations_data_write(31 downto 0),
      bram_bbt_annotations_en => bram_bbt_annotations_en,
      bram_bbt_annotations_reset => bram_bbt_annotations_reset,
      bram_firmware_code_address(31 downto 0) => bram_firmware_code_address(31 downto 0),
      bram_firmware_code_bytes_selection(3 downto 0) => bram_firmware_code_bytes_selection(3 downto 0),
      bram_firmware_code_clk => bram_firmware_code_clk,
      bram_firmware_code_data_read => bram_firmware_code_data_read,
      bram_firmware_code_data_write(31 downto 0) => bram_firmware_code_data_write(31 downto 0),
      bram_firmware_code_en => bram_firmware_code_en,
      bram_firmware_code_reset => bram_firmware_code_reset,
      bram_kernel_to_monitor_address(31 downto 0) => bram_kernel_to_monitor_address(31 downto 0),
      bram_kernel_to_monitor_bytes_selection(3 downto 0) => bram_kernel_to_monitor_bytes_selection(3 downto 0),
      bram_kernel_to_monitor_clk => bram_kernel_to_monitor_clk,
      bram_kernel_to_monitor_data_read(31 downto 0) => bram_kernel_to_monitor_data_read(31 downto 0),
      bram_kernel_to_monitor_data_write(31 downto 0) => bram_kernel_to_monitor_data_write(31 downto 0),
      bram_kernel_to_monitor_en => bram_kernel_to_monitor_en,
      bram_kernel_to_monitor_reset => bram_kernel_to_monitor_reset,
      bram_monitor_to_kernel_address(31 downto 0) => bram_monitor_to_kernel_address(31 downto 0),
      bram_monitor_to_kernel_bytes_selection(3 downto 0) => bram_monitor_to_kernel_bytes_selection(3 downto 0),
      bram_monitor_to_kernel_clk => bram_monitor_to_kernel_clk,
      bram_monitor_to_kernel_data_read => bram_monitor_to_kernel_data_read,
      bram_monitor_to_kernel_data_write(31 downto 0) => bram_monitor_to_kernel_data_write(31 downto 0),
      bram_monitor_to_kernel_en => bram_monitor_to_kernel_en,
      bram_monitor_to_kernel_reset => bram_monitor_to_kernel_reset,
      bram_ptm_traces_address(31 downto 0) => bram_ptm_traces_address(31 downto 0),
      bram_ptm_traces_bytes_selection(3 downto 0) => bram_ptm_traces_bytes_selection(3 downto 0),
      bram_ptm_traces_clk => bram_ptm_traces_clk,
      bram_ptm_traces_data_read => bram_ptm_traces_data_read,
      bram_ptm_traces_data_write(31 downto 0) => bram_ptm_traces_data_write(31 downto 0),
      bram_ptm_traces_en => bram_ptm_traces_en,
      bram_ptm_traces_reset => bram_ptm_traces_reset,
      clk => clk,
      reset => reset
    );
end STRUCTURE;
