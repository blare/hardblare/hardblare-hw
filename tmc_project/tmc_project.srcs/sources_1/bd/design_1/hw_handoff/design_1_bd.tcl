
################################################################
# This is a generated script based on design: design_1
#
# Though there are limitations about the generated script,
# the main purpose of this utility is to make learning
# IP Integrator Tcl commands easier.
################################################################

namespace eval _tcl {
proc get_script_folder {} {
   set script_path [file normalize [info script]]
   set script_folder [file dirname $script_path]
   return $script_folder
}
}
variable script_folder
set script_folder [_tcl::get_script_folder]

################################################################
# Check if script is running in correct Vivado version.
################################################################
set scripts_vivado_version 2018.2
set current_vivado_version [version -short]

if { [string first $scripts_vivado_version $current_vivado_version] == -1 } {
   puts ""
   catch {common::send_msg_id "BD_TCL-109" "ERROR" "This script was generated using Vivado <$scripts_vivado_version> and is being run in <$current_vivado_version> of Vivado. Please run the script in Vivado <$scripts_vivado_version> then open the design in Vivado <$current_vivado_version>. Upgrade the design by running \"Tools => Report => Report IP Status...\", then run write_bd_tcl to create an updated script."}

   return 1
}

################################################################
# START
################################################################

# To test this script, run the following commands from Vivado Tcl console:
# source design_1_script.tcl

# If there is no project opened, this script will create a
# project, but make sure you do not have an existing project
# <./myproj/project_1.xpr> in the current working folder.

set list_projs [get_projects -quiet]
if { $list_projs eq "" } {
   create_project project_1 myproj -part xc7z020clg484-1
   set_property BOARD_PART em.avnet.com:zed:part0:1.2 [current_project]
}


# CHANGE DESIGN NAME HERE
variable design_name
set design_name design_1

# If you do not already have an existing IP Integrator design open,
# you can create a design using the following command:
#    create_bd_design $design_name

# Creating design if needed
set errMsg ""
set nRet 0

set cur_design [current_bd_design -quiet]
set list_cells [get_bd_cells -quiet]

if { ${design_name} eq "" } {
   # USE CASES:
   #    1) Design_name not set

   set errMsg "Please set the variable <design_name> to a non-empty value."
   set nRet 1

} elseif { ${cur_design} ne "" && ${list_cells} eq "" } {
   # USE CASES:
   #    2): Current design opened AND is empty AND names same.
   #    3): Current design opened AND is empty AND names diff; design_name NOT in project.
   #    4): Current design opened AND is empty AND names diff; design_name exists in project.

   if { $cur_design ne $design_name } {
      common::send_msg_id "BD_TCL-001" "INFO" "Changing value of <design_name> from <$design_name> to <$cur_design> since current design is empty."
      set design_name [get_property NAME $cur_design]
   }
   common::send_msg_id "BD_TCL-002" "INFO" "Constructing design in IPI design <$cur_design>..."

} elseif { ${cur_design} ne "" && $list_cells ne "" && $cur_design eq $design_name } {
   # USE CASES:
   #    5) Current design opened AND has components AND same names.

   set errMsg "Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 1
} elseif { [get_files -quiet ${design_name}.bd] ne "" } {
   # USE CASES: 
   #    6) Current opened design, has components, but diff names, design_name exists in project.
   #    7) No opened design, design_name exists in project.

   set errMsg "Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 2

} else {
   # USE CASES:
   #    8) No opened design, design_name not in project.
   #    9) Current opened design, has components, but diff names, design_name not in project.

   common::send_msg_id "BD_TCL-003" "INFO" "Currently there is no design <$design_name> in project, so creating one..."

   create_bd_design $design_name

   common::send_msg_id "BD_TCL-004" "INFO" "Making design <$design_name> as current_bd_design."
   current_bd_design $design_name

}

common::send_msg_id "BD_TCL-005" "INFO" "Currently the variable <design_name> is equal to \"$design_name\"."

if { $nRet != 0 } {
   catch {common::send_msg_id "BD_TCL-114" "ERROR" $errMsg}
   return $nRet
}

##################################################################
# DESIGN PROCs
##################################################################



# Procedure to create entire design; Provide argument to make
# procedure reusable. If parentCell is "", will use root.
proc create_root_design { parentCell } {

  variable script_folder
  variable design_name

  if { $parentCell eq "" } {
     set parentCell [get_bd_cells /]
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj


  # Create interface ports
  set DDR [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:ddrx_rtl:1.0 DDR ]
  set FIXED_IO [ create_bd_intf_port -mode Master -vlnv xilinx.com:display_processing_system7:fixedio_rtl:1.0 FIXED_IO ]

  # Create ports

  # Create instance: AXI_BRAM_BBT_Annotations, and set properties
  set AXI_BRAM_BBT_Annotations [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_bram_ctrl:4.0 AXI_BRAM_BBT_Annotations ]
  set_property -dict [ list \
   CONFIG.ECC_TYPE {Hamming} \
   CONFIG.FAULT_INJECT {0} \
   CONFIG.SINGLE_PORT_BRAM {1} \
   CONFIG.USE_ECC {0} \
 ] $AXI_BRAM_BBT_Annotations

  # Create instance: AXI_BRAM_DEBUG, and set properties
  set AXI_BRAM_DEBUG [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_bram_ctrl:4.0 AXI_BRAM_DEBUG ]
  set_property -dict [ list \
   CONFIG.SINGLE_PORT_BRAM {1} \
 ] $AXI_BRAM_DEBUG

  # Create instance: AXI_BRAM_Kernel_to_Monitor, and set properties
  set AXI_BRAM_Kernel_to_Monitor [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_bram_ctrl:4.0 AXI_BRAM_Kernel_to_Monitor ]
  set_property -dict [ list \
   CONFIG.SINGLE_PORT_BRAM {1} \
   CONFIG.SUPPORTS_NARROW_BURST {0} \
 ] $AXI_BRAM_Kernel_to_Monitor

  # Create instance: AXI_BRAM_Monitor_to_Kernel, and set properties
  set AXI_BRAM_Monitor_to_Kernel [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_bram_ctrl:4.0 AXI_BRAM_Monitor_to_Kernel ]
  set_property -dict [ list \
   CONFIG.ECC_TYPE {Hamming} \
   CONFIG.PROTOCOL {AXI4} \
   CONFIG.SINGLE_PORT_BRAM {1} \
   CONFIG.USE_ECC {0} \
 ] $AXI_BRAM_Monitor_to_Kernel

  # Create instance: AXI_BRAM_Security_Policy_Config, and set properties
  set AXI_BRAM_Security_Policy_Config [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_bram_ctrl:4.0 AXI_BRAM_Security_Policy_Config ]
  set_property -dict [ list \
   CONFIG.SINGLE_PORT_BRAM {1} \
   CONFIG.SUPPORTS_NARROW_BURST {0} \
 ] $AXI_BRAM_Security_Policy_Config

  # Create instance: AXI_FIFO_Instrumentation, and set properties
  set AXI_FIFO_Instrumentation [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_bram_ctrl:4.0 AXI_FIFO_Instrumentation ]
  set_property -dict [ list \
   CONFIG.SINGLE_PORT_BRAM {1} \
 ] $AXI_FIFO_Instrumentation

  # Create instance: BRAM_BBT_Annotations, and set properties
  set BRAM_BBT_Annotations [ create_bd_cell -type ip -vlnv xilinx.com:ip:blk_mem_gen:8.4 BRAM_BBT_Annotations ]
  set_property -dict [ list \
   CONFIG.Additional_Inputs_for_Power_Estimation {true} \
   CONFIG.Assume_Synchronous_Clk {false} \
   CONFIG.Byte_Size {8} \
   CONFIG.EN_SAFETY_CKT {true} \
   CONFIG.Enable_32bit_Address {true} \
   CONFIG.Enable_B {Use_ENB_Pin} \
   CONFIG.Memory_Type {True_Dual_Port_RAM} \
   CONFIG.Operating_Mode_A {WRITE_FIRST} \
   CONFIG.Port_B_Clock {100} \
   CONFIG.Port_B_Enable_Rate {100} \
   CONFIG.Port_B_Write_Rate {50} \
   CONFIG.Read_Width_A {32} \
   CONFIG.Read_Width_B {32} \
   CONFIG.Register_PortA_Output_of_Memory_Primitives {false} \
   CONFIG.Register_PortB_Output_of_Memory_Primitives {false} \
   CONFIG.Use_Byte_Write_Enable {true} \
   CONFIG.Use_RSTA_Pin {true} \
   CONFIG.Use_RSTB_Pin {true} \
   CONFIG.Write_Width_A {32} \
   CONFIG.Write_Width_B {32} \
   CONFIG.use_bram_block {BRAM_Controller} \
 ] $BRAM_BBT_Annotations

  # Create instance: BRAM_DEBUG, and set properties
  set BRAM_DEBUG [ create_bd_cell -type ip -vlnv xilinx.com:ip:blk_mem_gen:8.4 BRAM_DEBUG ]
  set_property -dict [ list \
   CONFIG.Assume_Synchronous_Clk {false} \
   CONFIG.Byte_Size {8} \
   CONFIG.EN_SAFETY_CKT {true} \
   CONFIG.Enable_32bit_Address {true} \
   CONFIG.Enable_B {Use_ENB_Pin} \
   CONFIG.Memory_Type {True_Dual_Port_RAM} \
   CONFIG.Port_B_Clock {100} \
   CONFIG.Port_B_Enable_Rate {100} \
   CONFIG.Port_B_Write_Rate {50} \
   CONFIG.Register_PortA_Output_of_Memory_Primitives {false} \
   CONFIG.Register_PortB_Output_of_Memory_Primitives {false} \
   CONFIG.Use_Byte_Write_Enable {true} \
   CONFIG.Use_RSTA_Pin {true} \
   CONFIG.Use_RSTB_Pin {true} \
   CONFIG.use_bram_block {BRAM_Controller} \
 ] $BRAM_DEBUG

  # Create instance: BRAM_Firmware_code, and set properties
  set BRAM_Firmware_code [ create_bd_cell -type ip -vlnv xilinx.com:ip:blk_mem_gen:8.4 BRAM_Firmware_code ]
  set_property -dict [ list \
   CONFIG.Byte_Size {8} \
   CONFIG.Coe_File {../../../../../../firmware_code.coe} \
   CONFIG.EN_SAFETY_CKT {true} \
   CONFIG.Enable_32bit_Address {true} \
   CONFIG.Enable_B {Always_Enabled} \
   CONFIG.Fill_Remaining_Memory_Locations {false} \
   CONFIG.Load_Init_File {true} \
   CONFIG.Memory_Type {Single_Port_RAM} \
   CONFIG.Port_B_Clock {0} \
   CONFIG.Port_B_Enable_Rate {0} \
   CONFIG.Port_B_Write_Rate {0} \
   CONFIG.Register_PortA_Output_of_Memory_Primitives {false} \
   CONFIG.Register_PortB_Output_of_Memory_Primitives {false} \
   CONFIG.Use_Byte_Write_Enable {true} \
   CONFIG.Use_RSTA_Pin {true} \
   CONFIG.Use_RSTB_Pin {false} \
   CONFIG.Write_Depth_A {11000} \
   CONFIG.use_bram_block {Stand_Alone} \
 ] $BRAM_Firmware_code

  # Create instance: BRAM_Security_Policy_Config, and set properties
  set BRAM_Security_Policy_Config [ create_bd_cell -type ip -vlnv xilinx.com:ip:blk_mem_gen:8.4 BRAM_Security_Policy_Config ]
  set_property -dict [ list \
   CONFIG.Byte_Size {8} \
   CONFIG.EN_SAFETY_CKT {false} \
   CONFIG.Enable_32bit_Address {true} \
   CONFIG.Enable_B {Use_ENB_Pin} \
   CONFIG.Memory_Type {True_Dual_Port_RAM} \
   CONFIG.Port_B_Clock {100} \
   CONFIG.Port_B_Enable_Rate {100} \
   CONFIG.Port_B_Write_Rate {50} \
   CONFIG.Register_PortA_Output_of_Memory_Primitives {false} \
   CONFIG.Register_PortB_Output_of_Memory_Primitives {false} \
   CONFIG.Use_Byte_Write_Enable {true} \
   CONFIG.Use_RSTA_Pin {true} \
   CONFIG.Use_RSTB_Pin {true} \
   CONFIG.use_bram_block {BRAM_Controller} \
 ] $BRAM_Security_Policy_Config

  # Create instance: BRAM_to_FIFO_Instrumentation, and set properties
  set BRAM_to_FIFO_Instrumentation [ create_bd_cell -type ip -vlnv user.org:user:BRAM_to_FIFO:3.0 BRAM_to_FIFO_Instrumentation ]

  # Create instance: BRAM_to_FIFO_Kernel_to_Monitor, and set properties
  set BRAM_to_FIFO_Kernel_to_Monitor [ create_bd_cell -type ip -vlnv user.org:user:BRAM_to_FIFO:3.0 BRAM_to_FIFO_Kernel_to_Monitor ]

  # Create instance: BRAM_to_FIFO_Monitor_to_Kernel, and set properties
  set BRAM_to_FIFO_Monitor_to_Kernel [ create_bd_cell -type ip -vlnv user.org:user:BRAM_to_FIFO:3.0 BRAM_to_FIFO_Monitor_to_Kernel ]

  # Create instance: Dispatcher_HardBlare, and set properties
  set Dispatcher_HardBlare [ create_bd_cell -type ip -vlnv user.org:user:Dispatcher_HardBlare:42.0 Dispatcher_HardBlare ]

  # Create instance: FIFO_Instrumentation, and set properties
  set FIFO_Instrumentation [ create_bd_cell -type ip -vlnv xilinx.com:ip:fifo_generator:13.2 FIFO_Instrumentation ]
  set_property -dict [ list \
   CONFIG.Almost_Empty_Flag {true} \
   CONFIG.Almost_Full_Flag {false} \
   CONFIG.Clock_Type_AXI {Independent_Clock} \
   CONFIG.Data_Count_Width {13} \
   CONFIG.Empty_Threshold_Assert_Value {2} \
   CONFIG.Empty_Threshold_Assert_Value_axis {1022} \
   CONFIG.Empty_Threshold_Assert_Value_rach {1022} \
   CONFIG.Empty_Threshold_Assert_Value_rdch {1022} \
   CONFIG.Empty_Threshold_Assert_Value_wach {1022} \
   CONFIG.Empty_Threshold_Assert_Value_wdch {1022} \
   CONFIG.Empty_Threshold_Assert_Value_wrch {1022} \
   CONFIG.Empty_Threshold_Negate_Value {3} \
   CONFIG.Enable_ECC {true} \
   CONFIG.Enable_Safety_Circuit {false} \
   CONFIG.FIFO_Implementation_axis {Independent_Clocks_Block_RAM} \
   CONFIG.FIFO_Implementation_rach {Independent_Clocks_Distributed_RAM} \
   CONFIG.FIFO_Implementation_rdch {Independent_Clocks_Block_RAM} \
   CONFIG.FIFO_Implementation_wach {Independent_Clocks_Distributed_RAM} \
   CONFIG.FIFO_Implementation_wdch {Independent_Clocks_Block_RAM} \
   CONFIG.FIFO_Implementation_wrch {Independent_Clocks_Distributed_RAM} \
   CONFIG.Fifo_Implementation {Common_Clock_Block_RAM} \
   CONFIG.Full_Flags_Reset_Value {0} \
   CONFIG.Full_Threshold_Assert_Value {4094} \
   CONFIG.Full_Threshold_Assert_Value_rach {1023} \
   CONFIG.Full_Threshold_Assert_Value_wach {1023} \
   CONFIG.Full_Threshold_Assert_Value_wrch {1023} \
   CONFIG.Full_Threshold_Negate_Value {4093} \
   CONFIG.INTERFACE_TYPE {Native} \
   CONFIG.Input_Data_Width {32} \
   CONFIG.Input_Depth {8192} \
   CONFIG.Output_Data_Width {32} \
   CONFIG.Output_Depth {8192} \
   CONFIG.PROTOCOL {AXI4} \
   CONFIG.Performance_Options {Standard_FIFO} \
   CONFIG.Programmable_Full_Type {Multiple_Programmable_Full_Threshold_Constants} \
   CONFIG.READ_WRITE_MODE {READ_WRITE} \
   CONFIG.Read_Data_Count_Width {13} \
   CONFIG.Reset_Type {Synchronous_Reset} \
   CONFIG.Use_Dout_Reset {false} \
   CONFIG.Use_Extra_Logic {false} \
   CONFIG.Write_Data_Count_Width {13} \
 ] $FIFO_Instrumentation

  # Create instance: FIFO_Kernel_to_Monitor, and set properties
  set FIFO_Kernel_to_Monitor [ create_bd_cell -type ip -vlnv xilinx.com:ip:fifo_generator:13.2 FIFO_Kernel_to_Monitor ]
  set_property -dict [ list \
   CONFIG.Almost_Empty_Flag {true} \
   CONFIG.Almost_Full_Flag {false} \
   CONFIG.Data_Count_Width {12} \
   CONFIG.Enable_ECC {true} \
   CONFIG.Full_Threshold_Assert_Value {180} \
   CONFIG.Full_Threshold_Negate_Value {179} \
   CONFIG.Input_Data_Width {32} \
   CONFIG.Input_Depth {4096} \
   CONFIG.Output_Data_Width {32} \
   CONFIG.Output_Depth {4096} \
   CONFIG.Programmable_Full_Type {Single_Programmable_Full_Threshold_Constant} \
   CONFIG.Read_Data_Count_Width {12} \
   CONFIG.Use_Dout_Reset {false} \
   CONFIG.Write_Data_Count_Width {12} \
 ] $FIFO_Kernel_to_Monitor

  # Create instance: FIFO_Monitor_to_Kernel, and set properties
  set FIFO_Monitor_to_Kernel [ create_bd_cell -type ip -vlnv xilinx.com:ip:fifo_generator:13.2 FIFO_Monitor_to_Kernel ]
  set_property -dict [ list \
   CONFIG.Almost_Empty_Flag {true} \
   CONFIG.Almost_Full_Flag {true} \
   CONFIG.Data_Count_Width {4} \
   CONFIG.Enable_ECC {true} \
   CONFIG.Enable_Reset_Synchronization {true} \
   CONFIG.Enable_Safety_Circuit {false} \
   CONFIG.Fifo_Implementation {Independent_Clocks_Block_RAM} \
   CONFIG.Full_Flags_Reset_Value {0} \
   CONFIG.Full_Threshold_Assert_Value {13} \
   CONFIG.Full_Threshold_Negate_Value {12} \
   CONFIG.Input_Data_Width {32} \
   CONFIG.Input_Depth {16} \
   CONFIG.Output_Data_Width {32} \
   CONFIG.Output_Depth {16} \
   CONFIG.Programmable_Empty_Type {No_Programmable_Empty_Threshold} \
   CONFIG.Programmable_Full_Type {Single_Programmable_Full_Threshold_Constant} \
   CONFIG.Read_Data_Count_Width {4} \
   CONFIG.Reset_Pin {false} \
   CONFIG.Reset_Type {Asynchronous_Reset} \
   CONFIG.Use_Dout_Reset {false} \
   CONFIG.Write_Data_Count_Width {4} \
 ] $FIFO_Monitor_to_Kernel

  # Create instance: FIFO_PTM_TRACES, and set properties
  set FIFO_PTM_TRACES [ create_bd_cell -type ip -vlnv xilinx.com:ip:fifo_generator:13.2 FIFO_PTM_TRACES ]
  set_property -dict [ list \
   CONFIG.Almost_Empty_Flag {true} \
   CONFIG.Almost_Full_Flag {false} \
   CONFIG.Data_Count_Width {14} \
   CONFIG.Enable_ECC {true} \
   CONFIG.Enable_ECC_Type {Hard_ECC} \
   CONFIG.Enable_Reset_Synchronization {false} \
   CONFIG.Enable_Safety_Circuit {false} \
   CONFIG.Fifo_Implementation {Independent_Clocks_Block_RAM} \
   CONFIG.Full_Flags_Reset_Value {1} \
   CONFIG.Full_Threshold_Assert_Value {16381} \
   CONFIG.Full_Threshold_Negate_Value {16000} \
   CONFIG.Input_Data_Width {32} \
   CONFIG.Input_Depth {16384} \
   CONFIG.Output_Data_Width {32} \
   CONFIG.Output_Depth {16384} \
   CONFIG.Programmable_Full_Type {Multiple_Programmable_Full_Threshold_Constants} \
   CONFIG.Read_Data_Count_Width {14} \
   CONFIG.Reset_Pin {true} \
   CONFIG.Reset_Type {Asynchronous_Reset} \
   CONFIG.Use_Dout_Reset {false} \
   CONFIG.Write_Data_Count_Width {14} \
 ] $FIFO_PTM_TRACES

  # Create instance: PL_to_PS, and set properties
  set PL_to_PS [ create_bd_cell -type ip -vlnv user.org:user:PL_to_PS:96 PL_to_PS ]
  set_property -dict [ list \
   CONFIG.C_NUM_OF_INTR {6} \
   CONFIG.C_S00_AXI_ADDR_WIDTH {32} \
   CONFIG.C_S00_AXI_ARUSER_WIDTH {1} \
   CONFIG.C_S00_AXI_AWUSER_WIDTH {1} \
   CONFIG.C_S00_AXI_BUSER_WIDTH {1} \
   CONFIG.C_S00_AXI_ID_WIDTH {12} \
   CONFIG.C_S00_AXI_RUSER_WIDTH {1} \
   CONFIG.C_S00_AXI_WUSER_WIDTH {1} \
   CONFIG.C_S_AXI_INTR_ADDR_WIDTH {32} \
 ] $PL_to_PS

  # Create instance: axi_mem_intercon, and set properties
  set axi_mem_intercon [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 axi_mem_intercon ]
  set_property -dict [ list \
   CONFIG.NUM_MI {1} \
   CONFIG.SYNCHRONIZATION_STAGES {2} \
 ] $axi_mem_intercon

  # Create instance: axi_mem_intercon_1, and set properties
  set axi_mem_intercon_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 axi_mem_intercon_1 ]
  set_property -dict [ list \
   CONFIG.ENABLE_ADVANCED_OPTIONS {1} \
   CONFIG.ENABLE_PROTOCOL_CHECKERS {0} \
   CONFIG.M00_SECURE {0} \
   CONFIG.M01_SECURE {0} \
   CONFIG.M02_SECURE {0} \
   CONFIG.NUM_MI {8} \
   CONFIG.STRATEGY {0} \
   CONFIG.SYNCHRONIZATION_STAGES {2} \
   CONFIG.XBAR_DATA_WIDTH {32} \
 ] $axi_mem_intercon_1

  # Create instance: decodeur_traces, and set properties
  set decodeur_traces [ create_bd_cell -type ip -vlnv user.org:user:decodeur_traces:4.0 decodeur_traces ]

  set_property -dict [ list \
   CONFIG.POLARITY {ACTIVE_LOW} \
 ] [get_bd_pins /decodeur_traces/reset]

  # Create instance: myip_v2_0_0, and set properties
  set myip_v2_0_0 [ create_bd_cell -type ip -vlnv lab:user:myip_v2_0:1.0 myip_v2_0_0 ]

  # Create instance: processing_system7_0, and set properties
  set processing_system7_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:processing_system7:5.5 processing_system7_0 ]
  set_property -dict [ list \
   CONFIG.PCW_ACT_APU_PERIPHERAL_FREQMHZ {666.666687} \
   CONFIG.PCW_ACT_CAN0_PERIPHERAL_FREQMHZ {23.8095} \
   CONFIG.PCW_ACT_CAN1_PERIPHERAL_FREQMHZ {23.8095} \
   CONFIG.PCW_ACT_CAN_PERIPHERAL_FREQMHZ {10.000000} \
   CONFIG.PCW_ACT_DCI_PERIPHERAL_FREQMHZ {10.158730} \
   CONFIG.PCW_ACT_ENET0_PERIPHERAL_FREQMHZ {10.000000} \
   CONFIG.PCW_ACT_ENET1_PERIPHERAL_FREQMHZ {10.000000} \
   CONFIG.PCW_ACT_FPGA0_PERIPHERAL_FREQMHZ {102.564102} \
   CONFIG.PCW_ACT_FPGA1_PERIPHERAL_FREQMHZ {190.476196} \
   CONFIG.PCW_ACT_FPGA2_PERIPHERAL_FREQMHZ {10.000000} \
   CONFIG.PCW_ACT_FPGA3_PERIPHERAL_FREQMHZ {10.000000} \
   CONFIG.PCW_ACT_I2C_PERIPHERAL_FREQMHZ {50} \
   CONFIG.PCW_ACT_PCAP_PERIPHERAL_FREQMHZ {200.000000} \
   CONFIG.PCW_ACT_QSPI_PERIPHERAL_FREQMHZ {10.000000} \
   CONFIG.PCW_ACT_SDIO_PERIPHERAL_FREQMHZ {50.000000} \
   CONFIG.PCW_ACT_SMC_PERIPHERAL_FREQMHZ {10.000000} \
   CONFIG.PCW_ACT_SPI_PERIPHERAL_FREQMHZ {10.000000} \
   CONFIG.PCW_ACT_TPIU_PERIPHERAL_FREQMHZ {200.000000} \
   CONFIG.PCW_ACT_TTC0_CLK0_PERIPHERAL_FREQMHZ {111.111115} \
   CONFIG.PCW_ACT_TTC0_CLK1_PERIPHERAL_FREQMHZ {111.111115} \
   CONFIG.PCW_ACT_TTC0_CLK2_PERIPHERAL_FREQMHZ {111.111115} \
   CONFIG.PCW_ACT_TTC1_CLK0_PERIPHERAL_FREQMHZ {111.111115} \
   CONFIG.PCW_ACT_TTC1_CLK1_PERIPHERAL_FREQMHZ {111.111115} \
   CONFIG.PCW_ACT_TTC1_CLK2_PERIPHERAL_FREQMHZ {111.111115} \
   CONFIG.PCW_ACT_TTC_PERIPHERAL_FREQMHZ {50} \
   CONFIG.PCW_ACT_UART_PERIPHERAL_FREQMHZ {50.000000} \
   CONFIG.PCW_ACT_USB0_PERIPHERAL_FREQMHZ {60} \
   CONFIG.PCW_ACT_USB1_PERIPHERAL_FREQMHZ {60} \
   CONFIG.PCW_ACT_WDT_PERIPHERAL_FREQMHZ {111.111115} \
   CONFIG.PCW_APU_CLK_RATIO_ENABLE {6:2:1} \
   CONFIG.PCW_APU_PERIPHERAL_FREQMHZ {666.666667} \
   CONFIG.PCW_ARMPLL_CTRL_FBDIV {40} \
   CONFIG.PCW_CAN0_BASEADDR {0xE0008000} \
   CONFIG.PCW_CAN0_GRP_CLK_ENABLE {0} \
   CONFIG.PCW_CAN0_HIGHADDR {0xE0008FFF} \
   CONFIG.PCW_CAN0_PERIPHERAL_CLKSRC {External} \
   CONFIG.PCW_CAN0_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_CAN0_PERIPHERAL_FREQMHZ {-1} \
   CONFIG.PCW_CAN1_BASEADDR {0xE0009000} \
   CONFIG.PCW_CAN1_GRP_CLK_ENABLE {0} \
   CONFIG.PCW_CAN1_HIGHADDR {0xE0009FFF} \
   CONFIG.PCW_CAN1_PERIPHERAL_CLKSRC {External} \
   CONFIG.PCW_CAN1_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_CAN1_PERIPHERAL_FREQMHZ {-1} \
   CONFIG.PCW_CAN_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_CAN_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_CAN_PERIPHERAL_DIVISOR1 {1} \
   CONFIG.PCW_CAN_PERIPHERAL_FREQMHZ {100} \
   CONFIG.PCW_CAN_PERIPHERAL_VALID {0} \
   CONFIG.PCW_CLK0_FREQ {102564102} \
   CONFIG.PCW_CLK1_FREQ {190476196} \
   CONFIG.PCW_CLK2_FREQ {10000000} \
   CONFIG.PCW_CLK3_FREQ {10000000} \
   CONFIG.PCW_CORE0_FIQ_INTR {0} \
   CONFIG.PCW_CORE0_IRQ_INTR {0} \
   CONFIG.PCW_CORE1_FIQ_INTR {0} \
   CONFIG.PCW_CORE1_IRQ_INTR {0} \
   CONFIG.PCW_CPU_CPU_6X4X_MAX_RANGE {667} \
   CONFIG.PCW_CPU_CPU_PLL_FREQMHZ {1333.333} \
   CONFIG.PCW_CPU_PERIPHERAL_CLKSRC {ARM PLL} \
   CONFIG.PCW_CPU_PERIPHERAL_DIVISOR0 {2} \
   CONFIG.PCW_CRYSTAL_PERIPHERAL_FREQMHZ {33.333333} \
   CONFIG.PCW_DCI_PERIPHERAL_CLKSRC {DDR PLL} \
   CONFIG.PCW_DCI_PERIPHERAL_DIVISOR0 {15} \
   CONFIG.PCW_DCI_PERIPHERAL_DIVISOR1 {7} \
   CONFIG.PCW_DCI_PERIPHERAL_FREQMHZ {10.159} \
   CONFIG.PCW_DDRPLL_CTRL_FBDIV {32} \
   CONFIG.PCW_DDR_DDR_PLL_FREQMHZ {1066.667} \
   CONFIG.PCW_DDR_HPRLPR_QUEUE_PARTITION {HPR(0)/LPR(32)} \
   CONFIG.PCW_DDR_HPR_TO_CRITICAL_PRIORITY_LEVEL {15} \
   CONFIG.PCW_DDR_LPR_TO_CRITICAL_PRIORITY_LEVEL {2} \
   CONFIG.PCW_DDR_PERIPHERAL_CLKSRC {DDR PLL} \
   CONFIG.PCW_DDR_PERIPHERAL_DIVISOR0 {2} \
   CONFIG.PCW_DDR_PORT0_HPR_ENABLE {0} \
   CONFIG.PCW_DDR_PORT1_HPR_ENABLE {0} \
   CONFIG.PCW_DDR_PORT2_HPR_ENABLE {0} \
   CONFIG.PCW_DDR_PORT3_HPR_ENABLE {0} \
   CONFIG.PCW_DDR_RAM_BASEADDR {0x00100000} \
   CONFIG.PCW_DDR_RAM_HIGHADDR {0x1FFFFFFF} \
   CONFIG.PCW_DDR_WRITE_TO_CRITICAL_PRIORITY_LEVEL {2} \
   CONFIG.PCW_DM_WIDTH {4} \
   CONFIG.PCW_DQS_WIDTH {4} \
   CONFIG.PCW_DQ_WIDTH {32} \
   CONFIG.PCW_ENET0_BASEADDR {0xE000B000} \
   CONFIG.PCW_ENET0_ENET0_IO {<Select>} \
   CONFIG.PCW_ENET0_GRP_MDIO_ENABLE {0} \
   CONFIG.PCW_ENET0_GRP_MDIO_IO {<Select>} \
   CONFIG.PCW_ENET0_HIGHADDR {0xE000BFFF} \
   CONFIG.PCW_ENET0_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_ENET0_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_ENET0_PERIPHERAL_DIVISOR1 {1} \
   CONFIG.PCW_ENET0_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_ENET0_PERIPHERAL_FREQMHZ {1000 Mbps} \
   CONFIG.PCW_ENET0_RESET_ENABLE {0} \
   CONFIG.PCW_ENET1_BASEADDR {0xE000C000} \
   CONFIG.PCW_ENET1_GRP_MDIO_ENABLE {0} \
   CONFIG.PCW_ENET1_HIGHADDR {0xE000CFFF} \
   CONFIG.PCW_ENET1_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_ENET1_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_ENET1_PERIPHERAL_DIVISOR1 {1} \
   CONFIG.PCW_ENET1_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_ENET1_PERIPHERAL_FREQMHZ {1000 Mbps} \
   CONFIG.PCW_ENET1_RESET_ENABLE {0} \
   CONFIG.PCW_ENET_RESET_ENABLE {0} \
   CONFIG.PCW_ENET_RESET_POLARITY {Active Low} \
   CONFIG.PCW_ENET_RESET_SELECT {<Select>} \
   CONFIG.PCW_EN_4K_TIMER {0} \
   CONFIG.PCW_EN_CAN0 {0} \
   CONFIG.PCW_EN_CAN1 {0} \
   CONFIG.PCW_EN_CLK0_PORT {1} \
   CONFIG.PCW_EN_CLK1_PORT {1} \
   CONFIG.PCW_EN_CLK2_PORT {0} \
   CONFIG.PCW_EN_CLK3_PORT {0} \
   CONFIG.PCW_EN_CLKTRIG0_PORT {0} \
   CONFIG.PCW_EN_CLKTRIG1_PORT {0} \
   CONFIG.PCW_EN_CLKTRIG2_PORT {0} \
   CONFIG.PCW_EN_CLKTRIG3_PORT {0} \
   CONFIG.PCW_EN_DDR {1} \
   CONFIG.PCW_EN_EMIO_CAN0 {0} \
   CONFIG.PCW_EN_EMIO_CAN1 {0} \
   CONFIG.PCW_EN_EMIO_CD_SDIO0 {0} \
   CONFIG.PCW_EN_EMIO_CD_SDIO1 {0} \
   CONFIG.PCW_EN_EMIO_ENET0 {0} \
   CONFIG.PCW_EN_EMIO_ENET1 {0} \
   CONFIG.PCW_EN_EMIO_GPIO {0} \
   CONFIG.PCW_EN_EMIO_I2C0 {0} \
   CONFIG.PCW_EN_EMIO_I2C1 {0} \
   CONFIG.PCW_EN_EMIO_MODEM_UART0 {0} \
   CONFIG.PCW_EN_EMIO_MODEM_UART1 {0} \
   CONFIG.PCW_EN_EMIO_PJTAG {0} \
   CONFIG.PCW_EN_EMIO_SDIO0 {0} \
   CONFIG.PCW_EN_EMIO_SDIO1 {0} \
   CONFIG.PCW_EN_EMIO_SPI0 {0} \
   CONFIG.PCW_EN_EMIO_SPI1 {0} \
   CONFIG.PCW_EN_EMIO_SRAM_INT {0} \
   CONFIG.PCW_EN_EMIO_TRACE {1} \
   CONFIG.PCW_EN_EMIO_TTC0 {0} \
   CONFIG.PCW_EN_EMIO_TTC1 {0} \
   CONFIG.PCW_EN_EMIO_UART0 {0} \
   CONFIG.PCW_EN_EMIO_UART1 {0} \
   CONFIG.PCW_EN_EMIO_WDT {0} \
   CONFIG.PCW_EN_EMIO_WP_SDIO0 {0} \
   CONFIG.PCW_EN_EMIO_WP_SDIO1 {0} \
   CONFIG.PCW_EN_ENET0 {0} \
   CONFIG.PCW_EN_ENET1 {0} \
   CONFIG.PCW_EN_GPIO {0} \
   CONFIG.PCW_EN_I2C0 {0} \
   CONFIG.PCW_EN_I2C1 {0} \
   CONFIG.PCW_EN_MODEM_UART0 {0} \
   CONFIG.PCW_EN_MODEM_UART1 {0} \
   CONFIG.PCW_EN_PJTAG {0} \
   CONFIG.PCW_EN_PTP_ENET0 {1} \
   CONFIG.PCW_EN_QSPI {0} \
   CONFIG.PCW_EN_RST0_PORT {1} \
   CONFIG.PCW_EN_RST1_PORT {0} \
   CONFIG.PCW_EN_RST2_PORT {0} \
   CONFIG.PCW_EN_RST3_PORT {0} \
   CONFIG.PCW_EN_SDIO0 {1} \
   CONFIG.PCW_EN_SDIO1 {0} \
   CONFIG.PCW_EN_SMC {0} \
   CONFIG.PCW_EN_SPI0 {0} \
   CONFIG.PCW_EN_SPI1 {0} \
   CONFIG.PCW_EN_TRACE {1} \
   CONFIG.PCW_EN_TTC0 {0} \
   CONFIG.PCW_EN_TTC1 {0} \
   CONFIG.PCW_EN_UART0 {0} \
   CONFIG.PCW_EN_UART1 {1} \
   CONFIG.PCW_EN_USB0 {0} \
   CONFIG.PCW_EN_USB1 {0} \
   CONFIG.PCW_EN_WDT {0} \
   CONFIG.PCW_FCLK0_PERIPHERAL_CLKSRC {ARM PLL} \
   CONFIG.PCW_FCLK0_PERIPHERAL_DIVISOR0 {13} \
   CONFIG.PCW_FCLK0_PERIPHERAL_DIVISOR1 {1} \
   CONFIG.PCW_FCLK1_PERIPHERAL_CLKSRC {ARM PLL} \
   CONFIG.PCW_FCLK1_PERIPHERAL_DIVISOR0 {7} \
   CONFIG.PCW_FCLK1_PERIPHERAL_DIVISOR1 {1} \
   CONFIG.PCW_FCLK2_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_FCLK2_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_FCLK2_PERIPHERAL_DIVISOR1 {1} \
   CONFIG.PCW_FCLK3_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_FCLK3_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_FCLK3_PERIPHERAL_DIVISOR1 {1} \
   CONFIG.PCW_FCLK_CLK0_BUF {TRUE} \
   CONFIG.PCW_FCLK_CLK1_BUF {TRUE} \
   CONFIG.PCW_FCLK_CLK2_BUF {FALSE} \
   CONFIG.PCW_FCLK_CLK3_BUF {FALSE} \
   CONFIG.PCW_FPGA0_PERIPHERAL_FREQMHZ {100} \
   CONFIG.PCW_FPGA1_PERIPHERAL_FREQMHZ {200} \
   CONFIG.PCW_FPGA2_PERIPHERAL_FREQMHZ {50} \
   CONFIG.PCW_FPGA3_PERIPHERAL_FREQMHZ {50} \
   CONFIG.PCW_FPGA_FCLK0_ENABLE {1} \
   CONFIG.PCW_FPGA_FCLK1_ENABLE {1} \
   CONFIG.PCW_FPGA_FCLK2_ENABLE {0} \
   CONFIG.PCW_FPGA_FCLK3_ENABLE {0} \
   CONFIG.PCW_GP0_EN_MODIFIABLE_TXN {0} \
   CONFIG.PCW_GP0_NUM_READ_THREADS {4} \
   CONFIG.PCW_GP0_NUM_WRITE_THREADS {4} \
   CONFIG.PCW_GP1_EN_MODIFIABLE_TXN {0} \
   CONFIG.PCW_GP1_NUM_READ_THREADS {4} \
   CONFIG.PCW_GP1_NUM_WRITE_THREADS {4} \
   CONFIG.PCW_GPIO_BASEADDR {0xE000A000} \
   CONFIG.PCW_GPIO_EMIO_GPIO_ENABLE {0} \
   CONFIG.PCW_GPIO_EMIO_GPIO_WIDTH {64} \
   CONFIG.PCW_GPIO_HIGHADDR {0xE000AFFF} \
   CONFIG.PCW_GPIO_MIO_GPIO_ENABLE {0} \
   CONFIG.PCW_GPIO_MIO_GPIO_IO {<Select>} \
   CONFIG.PCW_GPIO_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_I2C0_BASEADDR {0xE0004000} \
   CONFIG.PCW_I2C0_GRP_INT_ENABLE {0} \
   CONFIG.PCW_I2C0_HIGHADDR {0xE0004FFF} \
   CONFIG.PCW_I2C0_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_I2C0_RESET_ENABLE {0} \
   CONFIG.PCW_I2C1_BASEADDR {0xE0005000} \
   CONFIG.PCW_I2C1_GRP_INT_ENABLE {0} \
   CONFIG.PCW_I2C1_HIGHADDR {0xE0005FFF} \
   CONFIG.PCW_I2C1_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_I2C1_RESET_ENABLE {0} \
   CONFIG.PCW_I2C_PERIPHERAL_FREQMHZ {25} \
   CONFIG.PCW_I2C_RESET_ENABLE {0} \
   CONFIG.PCW_I2C_RESET_POLARITY {Active Low} \
   CONFIG.PCW_IMPORT_BOARD_PRESET {None} \
   CONFIG.PCW_INCLUDE_ACP_TRANS_CHECK {0} \
   CONFIG.PCW_INCLUDE_TRACE_BUFFER {0} \
   CONFIG.PCW_IOPLL_CTRL_FBDIV {48} \
   CONFIG.PCW_IO_IO_PLL_FREQMHZ {1600.000} \
   CONFIG.PCW_IRQ_F2P_INTR {1} \
   CONFIG.PCW_IRQ_F2P_MODE {DIRECT} \
   CONFIG.PCW_MIO_0_DIRECTION {<Select>} \
   CONFIG.PCW_MIO_0_IOTYPE {<Select>} \
   CONFIG.PCW_MIO_0_PULLUP {<Select>} \
   CONFIG.PCW_MIO_0_SLEW {<Select>} \
   CONFIG.PCW_MIO_10_DIRECTION {<Select>} \
   CONFIG.PCW_MIO_10_IOTYPE {<Select>} \
   CONFIG.PCW_MIO_10_PULLUP {<Select>} \
   CONFIG.PCW_MIO_10_SLEW {<Select>} \
   CONFIG.PCW_MIO_11_DIRECTION {<Select>} \
   CONFIG.PCW_MIO_11_IOTYPE {<Select>} \
   CONFIG.PCW_MIO_11_PULLUP {<Select>} \
   CONFIG.PCW_MIO_11_SLEW {<Select>} \
   CONFIG.PCW_MIO_12_DIRECTION {<Select>} \
   CONFIG.PCW_MIO_12_IOTYPE {<Select>} \
   CONFIG.PCW_MIO_12_PULLUP {<Select>} \
   CONFIG.PCW_MIO_12_SLEW {<Select>} \
   CONFIG.PCW_MIO_13_DIRECTION {<Select>} \
   CONFIG.PCW_MIO_13_IOTYPE {<Select>} \
   CONFIG.PCW_MIO_13_PULLUP {<Select>} \
   CONFIG.PCW_MIO_13_SLEW {<Select>} \
   CONFIG.PCW_MIO_14_DIRECTION {<Select>} \
   CONFIG.PCW_MIO_14_IOTYPE {<Select>} \
   CONFIG.PCW_MIO_14_PULLUP {<Select>} \
   CONFIG.PCW_MIO_14_SLEW {<Select>} \
   CONFIG.PCW_MIO_15_DIRECTION {<Select>} \
   CONFIG.PCW_MIO_15_IOTYPE {<Select>} \
   CONFIG.PCW_MIO_15_PULLUP {<Select>} \
   CONFIG.PCW_MIO_15_SLEW {<Select>} \
   CONFIG.PCW_MIO_16_DIRECTION {<Select>} \
   CONFIG.PCW_MIO_16_IOTYPE {<Select>} \
   CONFIG.PCW_MIO_16_PULLUP {<Select>} \
   CONFIG.PCW_MIO_16_SLEW {<Select>} \
   CONFIG.PCW_MIO_17_DIRECTION {<Select>} \
   CONFIG.PCW_MIO_17_IOTYPE {<Select>} \
   CONFIG.PCW_MIO_17_PULLUP {<Select>} \
   CONFIG.PCW_MIO_17_SLEW {<Select>} \
   CONFIG.PCW_MIO_18_DIRECTION {<Select>} \
   CONFIG.PCW_MIO_18_IOTYPE {<Select>} \
   CONFIG.PCW_MIO_18_PULLUP {<Select>} \
   CONFIG.PCW_MIO_18_SLEW {<Select>} \
   CONFIG.PCW_MIO_19_DIRECTION {<Select>} \
   CONFIG.PCW_MIO_19_IOTYPE {<Select>} \
   CONFIG.PCW_MIO_19_PULLUP {<Select>} \
   CONFIG.PCW_MIO_19_SLEW {<Select>} \
   CONFIG.PCW_MIO_1_DIRECTION {<Select>} \
   CONFIG.PCW_MIO_1_IOTYPE {<Select>} \
   CONFIG.PCW_MIO_1_PULLUP {<Select>} \
   CONFIG.PCW_MIO_1_SLEW {<Select>} \
   CONFIG.PCW_MIO_20_DIRECTION {<Select>} \
   CONFIG.PCW_MIO_20_IOTYPE {<Select>} \
   CONFIG.PCW_MIO_20_PULLUP {<Select>} \
   CONFIG.PCW_MIO_20_SLEW {<Select>} \
   CONFIG.PCW_MIO_21_DIRECTION {<Select>} \
   CONFIG.PCW_MIO_21_IOTYPE {<Select>} \
   CONFIG.PCW_MIO_21_PULLUP {<Select>} \
   CONFIG.PCW_MIO_21_SLEW {<Select>} \
   CONFIG.PCW_MIO_22_DIRECTION {<Select>} \
   CONFIG.PCW_MIO_22_IOTYPE {<Select>} \
   CONFIG.PCW_MIO_22_PULLUP {<Select>} \
   CONFIG.PCW_MIO_22_SLEW {<Select>} \
   CONFIG.PCW_MIO_23_DIRECTION {<Select>} \
   CONFIG.PCW_MIO_23_IOTYPE {<Select>} \
   CONFIG.PCW_MIO_23_PULLUP {<Select>} \
   CONFIG.PCW_MIO_23_SLEW {<Select>} \
   CONFIG.PCW_MIO_24_DIRECTION {<Select>} \
   CONFIG.PCW_MIO_24_IOTYPE {<Select>} \
   CONFIG.PCW_MIO_24_PULLUP {<Select>} \
   CONFIG.PCW_MIO_24_SLEW {<Select>} \
   CONFIG.PCW_MIO_25_DIRECTION {<Select>} \
   CONFIG.PCW_MIO_25_IOTYPE {<Select>} \
   CONFIG.PCW_MIO_25_PULLUP {<Select>} \
   CONFIG.PCW_MIO_25_SLEW {<Select>} \
   CONFIG.PCW_MIO_26_DIRECTION {<Select>} \
   CONFIG.PCW_MIO_26_IOTYPE {<Select>} \
   CONFIG.PCW_MIO_26_PULLUP {<Select>} \
   CONFIG.PCW_MIO_26_SLEW {<Select>} \
   CONFIG.PCW_MIO_27_DIRECTION {<Select>} \
   CONFIG.PCW_MIO_27_IOTYPE {<Select>} \
   CONFIG.PCW_MIO_27_PULLUP {<Select>} \
   CONFIG.PCW_MIO_27_SLEW {<Select>} \
   CONFIG.PCW_MIO_28_DIRECTION {<Select>} \
   CONFIG.PCW_MIO_28_IOTYPE {<Select>} \
   CONFIG.PCW_MIO_28_PULLUP {<Select>} \
   CONFIG.PCW_MIO_28_SLEW {<Select>} \
   CONFIG.PCW_MIO_29_DIRECTION {<Select>} \
   CONFIG.PCW_MIO_29_IOTYPE {<Select>} \
   CONFIG.PCW_MIO_29_PULLUP {<Select>} \
   CONFIG.PCW_MIO_29_SLEW {<Select>} \
   CONFIG.PCW_MIO_2_DIRECTION {<Select>} \
   CONFIG.PCW_MIO_2_IOTYPE {<Select>} \
   CONFIG.PCW_MIO_2_PULLUP {<Select>} \
   CONFIG.PCW_MIO_2_SLEW {<Select>} \
   CONFIG.PCW_MIO_30_DIRECTION {<Select>} \
   CONFIG.PCW_MIO_30_IOTYPE {<Select>} \
   CONFIG.PCW_MIO_30_PULLUP {<Select>} \
   CONFIG.PCW_MIO_30_SLEW {<Select>} \
   CONFIG.PCW_MIO_31_DIRECTION {<Select>} \
   CONFIG.PCW_MIO_31_IOTYPE {<Select>} \
   CONFIG.PCW_MIO_31_PULLUP {<Select>} \
   CONFIG.PCW_MIO_31_SLEW {<Select>} \
   CONFIG.PCW_MIO_32_DIRECTION {<Select>} \
   CONFIG.PCW_MIO_32_IOTYPE {<Select>} \
   CONFIG.PCW_MIO_32_PULLUP {<Select>} \
   CONFIG.PCW_MIO_32_SLEW {<Select>} \
   CONFIG.PCW_MIO_33_DIRECTION {<Select>} \
   CONFIG.PCW_MIO_33_IOTYPE {<Select>} \
   CONFIG.PCW_MIO_33_PULLUP {<Select>} \
   CONFIG.PCW_MIO_33_SLEW {<Select>} \
   CONFIG.PCW_MIO_34_DIRECTION {<Select>} \
   CONFIG.PCW_MIO_34_IOTYPE {<Select>} \
   CONFIG.PCW_MIO_34_PULLUP {<Select>} \
   CONFIG.PCW_MIO_34_SLEW {<Select>} \
   CONFIG.PCW_MIO_35_DIRECTION {<Select>} \
   CONFIG.PCW_MIO_35_IOTYPE {<Select>} \
   CONFIG.PCW_MIO_35_PULLUP {<Select>} \
   CONFIG.PCW_MIO_35_SLEW {<Select>} \
   CONFIG.PCW_MIO_36_DIRECTION {<Select>} \
   CONFIG.PCW_MIO_36_IOTYPE {<Select>} \
   CONFIG.PCW_MIO_36_PULLUP {<Select>} \
   CONFIG.PCW_MIO_36_SLEW {<Select>} \
   CONFIG.PCW_MIO_37_DIRECTION {<Select>} \
   CONFIG.PCW_MIO_37_IOTYPE {<Select>} \
   CONFIG.PCW_MIO_37_PULLUP {<Select>} \
   CONFIG.PCW_MIO_37_SLEW {<Select>} \
   CONFIG.PCW_MIO_38_DIRECTION {<Select>} \
   CONFIG.PCW_MIO_38_IOTYPE {<Select>} \
   CONFIG.PCW_MIO_38_PULLUP {<Select>} \
   CONFIG.PCW_MIO_38_SLEW {<Select>} \
   CONFIG.PCW_MIO_39_DIRECTION {<Select>} \
   CONFIG.PCW_MIO_39_IOTYPE {<Select>} \
   CONFIG.PCW_MIO_39_PULLUP {<Select>} \
   CONFIG.PCW_MIO_39_SLEW {<Select>} \
   CONFIG.PCW_MIO_3_DIRECTION {<Select>} \
   CONFIG.PCW_MIO_3_IOTYPE {<Select>} \
   CONFIG.PCW_MIO_3_PULLUP {<Select>} \
   CONFIG.PCW_MIO_3_SLEW {<Select>} \
   CONFIG.PCW_MIO_40_DIRECTION {inout} \
   CONFIG.PCW_MIO_40_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_40_PULLUP {disabled} \
   CONFIG.PCW_MIO_40_SLEW {fast} \
   CONFIG.PCW_MIO_41_DIRECTION {inout} \
   CONFIG.PCW_MIO_41_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_41_PULLUP {disabled} \
   CONFIG.PCW_MIO_41_SLEW {fast} \
   CONFIG.PCW_MIO_42_DIRECTION {inout} \
   CONFIG.PCW_MIO_42_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_42_PULLUP {disabled} \
   CONFIG.PCW_MIO_42_SLEW {fast} \
   CONFIG.PCW_MIO_43_DIRECTION {inout} \
   CONFIG.PCW_MIO_43_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_43_PULLUP {disabled} \
   CONFIG.PCW_MIO_43_SLEW {fast} \
   CONFIG.PCW_MIO_44_DIRECTION {inout} \
   CONFIG.PCW_MIO_44_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_44_PULLUP {disabled} \
   CONFIG.PCW_MIO_44_SLEW {fast} \
   CONFIG.PCW_MIO_45_DIRECTION {inout} \
   CONFIG.PCW_MIO_45_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_45_PULLUP {disabled} \
   CONFIG.PCW_MIO_45_SLEW {fast} \
   CONFIG.PCW_MIO_46_DIRECTION {in} \
   CONFIG.PCW_MIO_46_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_46_PULLUP {disabled} \
   CONFIG.PCW_MIO_46_SLEW {slow} \
   CONFIG.PCW_MIO_47_DIRECTION {in} \
   CONFIG.PCW_MIO_47_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_47_PULLUP {disabled} \
   CONFIG.PCW_MIO_47_SLEW {slow} \
   CONFIG.PCW_MIO_48_DIRECTION {out} \
   CONFIG.PCW_MIO_48_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_48_PULLUP {disabled} \
   CONFIG.PCW_MIO_48_SLEW {slow} \
   CONFIG.PCW_MIO_49_DIRECTION {in} \
   CONFIG.PCW_MIO_49_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_49_PULLUP {disabled} \
   CONFIG.PCW_MIO_49_SLEW {slow} \
   CONFIG.PCW_MIO_4_DIRECTION {<Select>} \
   CONFIG.PCW_MIO_4_IOTYPE {<Select>} \
   CONFIG.PCW_MIO_4_PULLUP {<Select>} \
   CONFIG.PCW_MIO_4_SLEW {<Select>} \
   CONFIG.PCW_MIO_50_DIRECTION {<Select>} \
   CONFIG.PCW_MIO_50_IOTYPE {<Select>} \
   CONFIG.PCW_MIO_50_PULLUP {<Select>} \
   CONFIG.PCW_MIO_50_SLEW {<Select>} \
   CONFIG.PCW_MIO_51_DIRECTION {<Select>} \
   CONFIG.PCW_MIO_51_IOTYPE {<Select>} \
   CONFIG.PCW_MIO_51_PULLUP {<Select>} \
   CONFIG.PCW_MIO_51_SLEW {<Select>} \
   CONFIG.PCW_MIO_52_DIRECTION {<Select>} \
   CONFIG.PCW_MIO_52_IOTYPE {<Select>} \
   CONFIG.PCW_MIO_52_PULLUP {<Select>} \
   CONFIG.PCW_MIO_52_SLEW {<Select>} \
   CONFIG.PCW_MIO_53_DIRECTION {<Select>} \
   CONFIG.PCW_MIO_53_IOTYPE {<Select>} \
   CONFIG.PCW_MIO_53_PULLUP {<Select>} \
   CONFIG.PCW_MIO_53_SLEW {<Select>} \
   CONFIG.PCW_MIO_5_DIRECTION {<Select>} \
   CONFIG.PCW_MIO_5_IOTYPE {<Select>} \
   CONFIG.PCW_MIO_5_PULLUP {<Select>} \
   CONFIG.PCW_MIO_5_SLEW {<Select>} \
   CONFIG.PCW_MIO_6_DIRECTION {<Select>} \
   CONFIG.PCW_MIO_6_IOTYPE {<Select>} \
   CONFIG.PCW_MIO_6_PULLUP {<Select>} \
   CONFIG.PCW_MIO_6_SLEW {<Select>} \
   CONFIG.PCW_MIO_7_DIRECTION {<Select>} \
   CONFIG.PCW_MIO_7_IOTYPE {<Select>} \
   CONFIG.PCW_MIO_7_PULLUP {<Select>} \
   CONFIG.PCW_MIO_7_SLEW {<Select>} \
   CONFIG.PCW_MIO_8_DIRECTION {<Select>} \
   CONFIG.PCW_MIO_8_IOTYPE {<Select>} \
   CONFIG.PCW_MIO_8_PULLUP {<Select>} \
   CONFIG.PCW_MIO_8_SLEW {<Select>} \
   CONFIG.PCW_MIO_9_DIRECTION {<Select>} \
   CONFIG.PCW_MIO_9_IOTYPE {<Select>} \
   CONFIG.PCW_MIO_9_PULLUP {<Select>} \
   CONFIG.PCW_MIO_9_SLEW {<Select>} \
   CONFIG.PCW_MIO_PRIMITIVE {54} \
   CONFIG.PCW_MIO_TREE_PERIPHERALS {unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#SD 0#SD 0#SD 0#SD 0#SD 0#SD 0#SD 0#SD 0#UART 1#UART 1#unassigned#unassigned#unassigned#unassigned} \
   CONFIG.PCW_MIO_TREE_SIGNALS {unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#unassigned#clk#cmd#data[0]#data[1]#data[2]#data[3]#wp#cd#tx#rx#unassigned#unassigned#unassigned#unassigned} \
   CONFIG.PCW_M_AXI_GP0_ENABLE_STATIC_REMAP {0} \
   CONFIG.PCW_M_AXI_GP0_ID_WIDTH {12} \
   CONFIG.PCW_M_AXI_GP0_SUPPORT_NARROW_BURST {0} \
   CONFIG.PCW_M_AXI_GP0_THREAD_ID_WIDTH {12} \
   CONFIG.PCW_M_AXI_GP1_ENABLE_STATIC_REMAP {0} \
   CONFIG.PCW_M_AXI_GP1_ID_WIDTH {12} \
   CONFIG.PCW_M_AXI_GP1_SUPPORT_NARROW_BURST {0} \
   CONFIG.PCW_M_AXI_GP1_THREAD_ID_WIDTH {12} \
   CONFIG.PCW_NAND_CYCLES_T_AR {1} \
   CONFIG.PCW_NAND_CYCLES_T_CLR {1} \
   CONFIG.PCW_NAND_CYCLES_T_RC {2} \
   CONFIG.PCW_NAND_CYCLES_T_REA {1} \
   CONFIG.PCW_NAND_CYCLES_T_RR {1} \
   CONFIG.PCW_NAND_CYCLES_T_WC {2} \
   CONFIG.PCW_NAND_CYCLES_T_WP {1} \
   CONFIG.PCW_NAND_GRP_D8_ENABLE {0} \
   CONFIG.PCW_NAND_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_NOR_CS0_T_CEOE {1} \
   CONFIG.PCW_NOR_CS0_T_PC {1} \
   CONFIG.PCW_NOR_CS0_T_RC {2} \
   CONFIG.PCW_NOR_CS0_T_TR {1} \
   CONFIG.PCW_NOR_CS0_T_WC {2} \
   CONFIG.PCW_NOR_CS0_T_WP {1} \
   CONFIG.PCW_NOR_CS0_WE_TIME {0} \
   CONFIG.PCW_NOR_CS1_T_CEOE {1} \
   CONFIG.PCW_NOR_CS1_T_PC {1} \
   CONFIG.PCW_NOR_CS1_T_RC {2} \
   CONFIG.PCW_NOR_CS1_T_TR {1} \
   CONFIG.PCW_NOR_CS1_T_WC {2} \
   CONFIG.PCW_NOR_CS1_T_WP {1} \
   CONFIG.PCW_NOR_CS1_WE_TIME {0} \
   CONFIG.PCW_NOR_GRP_A25_ENABLE {0} \
   CONFIG.PCW_NOR_GRP_CS0_ENABLE {0} \
   CONFIG.PCW_NOR_GRP_CS1_ENABLE {0} \
   CONFIG.PCW_NOR_GRP_SRAM_CS0_ENABLE {0} \
   CONFIG.PCW_NOR_GRP_SRAM_CS1_ENABLE {0} \
   CONFIG.PCW_NOR_GRP_SRAM_INT_ENABLE {0} \
   CONFIG.PCW_NOR_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_NOR_SRAM_CS0_T_CEOE {1} \
   CONFIG.PCW_NOR_SRAM_CS0_T_PC {1} \
   CONFIG.PCW_NOR_SRAM_CS0_T_RC {2} \
   CONFIG.PCW_NOR_SRAM_CS0_T_TR {1} \
   CONFIG.PCW_NOR_SRAM_CS0_T_WC {2} \
   CONFIG.PCW_NOR_SRAM_CS0_T_WP {1} \
   CONFIG.PCW_NOR_SRAM_CS0_WE_TIME {0} \
   CONFIG.PCW_NOR_SRAM_CS1_T_CEOE {1} \
   CONFIG.PCW_NOR_SRAM_CS1_T_PC {1} \
   CONFIG.PCW_NOR_SRAM_CS1_T_RC {2} \
   CONFIG.PCW_NOR_SRAM_CS1_T_TR {1} \
   CONFIG.PCW_NOR_SRAM_CS1_T_WC {2} \
   CONFIG.PCW_NOR_SRAM_CS1_T_WP {1} \
   CONFIG.PCW_NOR_SRAM_CS1_WE_TIME {0} \
   CONFIG.PCW_OVERRIDE_BASIC_CLOCK {0} \
   CONFIG.PCW_P2F_CAN0_INTR {0} \
   CONFIG.PCW_P2F_CAN1_INTR {0} \
   CONFIG.PCW_P2F_CTI_INTR {0} \
   CONFIG.PCW_P2F_DMAC0_INTR {0} \
   CONFIG.PCW_P2F_DMAC1_INTR {0} \
   CONFIG.PCW_P2F_DMAC2_INTR {0} \
   CONFIG.PCW_P2F_DMAC3_INTR {0} \
   CONFIG.PCW_P2F_DMAC4_INTR {0} \
   CONFIG.PCW_P2F_DMAC5_INTR {0} \
   CONFIG.PCW_P2F_DMAC6_INTR {0} \
   CONFIG.PCW_P2F_DMAC7_INTR {0} \
   CONFIG.PCW_P2F_DMAC_ABORT_INTR {0} \
   CONFIG.PCW_P2F_ENET0_INTR {0} \
   CONFIG.PCW_P2F_ENET1_INTR {0} \
   CONFIG.PCW_P2F_GPIO_INTR {0} \
   CONFIG.PCW_P2F_I2C0_INTR {0} \
   CONFIG.PCW_P2F_I2C1_INTR {0} \
   CONFIG.PCW_P2F_QSPI_INTR {0} \
   CONFIG.PCW_P2F_SDIO0_INTR {0} \
   CONFIG.PCW_P2F_SDIO1_INTR {0} \
   CONFIG.PCW_P2F_SMC_INTR {0} \
   CONFIG.PCW_P2F_SPI0_INTR {0} \
   CONFIG.PCW_P2F_SPI1_INTR {0} \
   CONFIG.PCW_P2F_UART0_INTR {0} \
   CONFIG.PCW_P2F_UART1_INTR {0} \
   CONFIG.PCW_P2F_USB0_INTR {0} \
   CONFIG.PCW_P2F_USB1_INTR {0} \
   CONFIG.PCW_PACKAGE_DDR_BOARD_DELAY0 {0.063} \
   CONFIG.PCW_PACKAGE_DDR_BOARD_DELAY1 {0.062} \
   CONFIG.PCW_PACKAGE_DDR_BOARD_DELAY2 {0.065} \
   CONFIG.PCW_PACKAGE_DDR_BOARD_DELAY3 {0.083} \
   CONFIG.PCW_PACKAGE_DDR_DQS_TO_CLK_DELAY_0 {-0.007} \
   CONFIG.PCW_PACKAGE_DDR_DQS_TO_CLK_DELAY_1 {-0.010} \
   CONFIG.PCW_PACKAGE_DDR_DQS_TO_CLK_DELAY_2 {-0.006} \
   CONFIG.PCW_PACKAGE_DDR_DQS_TO_CLK_DELAY_3 {-0.048} \
   CONFIG.PCW_PACKAGE_NAME {clg484} \
   CONFIG.PCW_PCAP_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_PCAP_PERIPHERAL_DIVISOR0 {8} \
   CONFIG.PCW_PCAP_PERIPHERAL_FREQMHZ {200} \
   CONFIG.PCW_PERIPHERAL_BOARD_PRESET {part0} \
   CONFIG.PCW_PJTAG_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_PLL_BYPASSMODE_ENABLE {0} \
   CONFIG.PCW_PRESET_BANK0_VOLTAGE {LVCMOS 3.3V} \
   CONFIG.PCW_PRESET_BANK1_VOLTAGE {LVCMOS 1.8V} \
   CONFIG.PCW_PS7_SI_REV {PRODUCTION} \
   CONFIG.PCW_QSPI_GRP_FBCLK_ENABLE {0} \
   CONFIG.PCW_QSPI_GRP_IO1_ENABLE {0} \
   CONFIG.PCW_QSPI_GRP_SINGLE_SS_ENABLE {0} \
   CONFIG.PCW_QSPI_GRP_SINGLE_SS_IO {<Select>} \
   CONFIG.PCW_QSPI_GRP_SS1_ENABLE {0} \
   CONFIG.PCW_QSPI_INTERNAL_HIGHADDRESS {0xFCFFFFFF} \
   CONFIG.PCW_QSPI_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_QSPI_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_QSPI_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_QSPI_PERIPHERAL_FREQMHZ {200} \
   CONFIG.PCW_QSPI_QSPI_IO {<Select>} \
   CONFIG.PCW_SD0_GRP_CD_ENABLE {1} \
   CONFIG.PCW_SD0_GRP_CD_IO {MIO 47} \
   CONFIG.PCW_SD0_GRP_POW_ENABLE {0} \
   CONFIG.PCW_SD0_GRP_WP_ENABLE {1} \
   CONFIG.PCW_SD0_GRP_WP_IO {MIO 46} \
   CONFIG.PCW_SD0_PERIPHERAL_ENABLE {1} \
   CONFIG.PCW_SD0_SD0_IO {MIO 40 .. 45} \
   CONFIG.PCW_SD1_GRP_CD_ENABLE {0} \
   CONFIG.PCW_SD1_GRP_POW_ENABLE {0} \
   CONFIG.PCW_SD1_GRP_WP_ENABLE {0} \
   CONFIG.PCW_SD1_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_SDIO0_BASEADDR {0xE0100000} \
   CONFIG.PCW_SDIO0_HIGHADDR {0xE0100FFF} \
   CONFIG.PCW_SDIO1_BASEADDR {0xE0101000} \
   CONFIG.PCW_SDIO1_HIGHADDR {0xE0101FFF} \
   CONFIG.PCW_SDIO_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_SDIO_PERIPHERAL_DIVISOR0 {32} \
   CONFIG.PCW_SDIO_PERIPHERAL_FREQMHZ {50} \
   CONFIG.PCW_SDIO_PERIPHERAL_VALID {1} \
   CONFIG.PCW_SINGLE_QSPI_DATA_MODE {<Select>} \
   CONFIG.PCW_SMC_CYCLE_T0 {NA} \
   CONFIG.PCW_SMC_CYCLE_T1 {NA} \
   CONFIG.PCW_SMC_CYCLE_T2 {NA} \
   CONFIG.PCW_SMC_CYCLE_T3 {NA} \
   CONFIG.PCW_SMC_CYCLE_T4 {NA} \
   CONFIG.PCW_SMC_CYCLE_T5 {NA} \
   CONFIG.PCW_SMC_CYCLE_T6 {NA} \
   CONFIG.PCW_SMC_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_SMC_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_SMC_PERIPHERAL_FREQMHZ {100} \
   CONFIG.PCW_SMC_PERIPHERAL_VALID {0} \
   CONFIG.PCW_SPI0_BASEADDR {0xE0006000} \
   CONFIG.PCW_SPI0_GRP_SS0_ENABLE {0} \
   CONFIG.PCW_SPI0_GRP_SS1_ENABLE {0} \
   CONFIG.PCW_SPI0_GRP_SS2_ENABLE {0} \
   CONFIG.PCW_SPI0_HIGHADDR {0xE0006FFF} \
   CONFIG.PCW_SPI0_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_SPI1_BASEADDR {0xE0007000} \
   CONFIG.PCW_SPI1_GRP_SS0_ENABLE {0} \
   CONFIG.PCW_SPI1_GRP_SS1_ENABLE {0} \
   CONFIG.PCW_SPI1_GRP_SS2_ENABLE {0} \
   CONFIG.PCW_SPI1_HIGHADDR {0xE0007FFF} \
   CONFIG.PCW_SPI1_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_SPI_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_SPI_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_SPI_PERIPHERAL_FREQMHZ {166.666666} \
   CONFIG.PCW_SPI_PERIPHERAL_VALID {0} \
   CONFIG.PCW_S_AXI_ACP_ARUSER_VAL {31} \
   CONFIG.PCW_S_AXI_ACP_AWUSER_VAL {31} \
   CONFIG.PCW_S_AXI_ACP_ID_WIDTH {3} \
   CONFIG.PCW_S_AXI_GP0_ID_WIDTH {6} \
   CONFIG.PCW_S_AXI_GP1_ID_WIDTH {6} \
   CONFIG.PCW_S_AXI_HP0_DATA_WIDTH {64} \
   CONFIG.PCW_S_AXI_HP0_ID_WIDTH {6} \
   CONFIG.PCW_S_AXI_HP1_DATA_WIDTH {64} \
   CONFIG.PCW_S_AXI_HP1_ID_WIDTH {6} \
   CONFIG.PCW_S_AXI_HP2_DATA_WIDTH {64} \
   CONFIG.PCW_S_AXI_HP2_ID_WIDTH {6} \
   CONFIG.PCW_S_AXI_HP3_DATA_WIDTH {64} \
   CONFIG.PCW_S_AXI_HP3_ID_WIDTH {6} \
   CONFIG.PCW_TPIU_PERIPHERAL_CLKSRC {External} \
   CONFIG.PCW_TPIU_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_TPIU_PERIPHERAL_FREQMHZ {200} \
   CONFIG.PCW_TRACE_BUFFER_CLOCK_DELAY {12} \
   CONFIG.PCW_TRACE_BUFFER_FIFO_SIZE {128} \
   CONFIG.PCW_TRACE_GRP_16BIT_ENABLE {0} \
   CONFIG.PCW_TRACE_GRP_16BIT_IO {EMIO} \
   CONFIG.PCW_TRACE_GRP_2BIT_ENABLE {1} \
   CONFIG.PCW_TRACE_GRP_2BIT_IO {EMIO} \
   CONFIG.PCW_TRACE_GRP_32BIT_ENABLE {0} \
   CONFIG.PCW_TRACE_GRP_32BIT_IO {EMIO} \
   CONFIG.PCW_TRACE_GRP_4BIT_ENABLE {1} \
   CONFIG.PCW_TRACE_GRP_4BIT_IO {EMIO} \
   CONFIG.PCW_TRACE_GRP_8BIT_ENABLE {1} \
   CONFIG.PCW_TRACE_GRP_8BIT_IO {EMIO} \
   CONFIG.PCW_TRACE_INTERNAL_WIDTH {8} \
   CONFIG.PCW_TRACE_PERIPHERAL_ENABLE {1} \
   CONFIG.PCW_TRACE_PIPELINE_WIDTH {8} \
   CONFIG.PCW_TRACE_TRACE_IO {EMIO} \
   CONFIG.PCW_TTC0_BASEADDR {0xE0104000} \
   CONFIG.PCW_TTC0_CLK0_PERIPHERAL_CLKSRC {CPU_1X} \
   CONFIG.PCW_TTC0_CLK0_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_TTC0_CLK0_PERIPHERAL_FREQMHZ {133.333333} \
   CONFIG.PCW_TTC0_CLK1_PERIPHERAL_CLKSRC {CPU_1X} \
   CONFIG.PCW_TTC0_CLK1_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_TTC0_CLK1_PERIPHERAL_FREQMHZ {133.333333} \
   CONFIG.PCW_TTC0_CLK2_PERIPHERAL_CLKSRC {CPU_1X} \
   CONFIG.PCW_TTC0_CLK2_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_TTC0_CLK2_PERIPHERAL_FREQMHZ {133.333333} \
   CONFIG.PCW_TTC0_HIGHADDR {0xE0104fff} \
   CONFIG.PCW_TTC0_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_TTC0_TTC0_IO {<Select>} \
   CONFIG.PCW_TTC1_BASEADDR {0xE0105000} \
   CONFIG.PCW_TTC1_CLK0_PERIPHERAL_CLKSRC {CPU_1X} \
   CONFIG.PCW_TTC1_CLK0_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_TTC1_CLK0_PERIPHERAL_FREQMHZ {133.333333} \
   CONFIG.PCW_TTC1_CLK1_PERIPHERAL_CLKSRC {CPU_1X} \
   CONFIG.PCW_TTC1_CLK1_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_TTC1_CLK1_PERIPHERAL_FREQMHZ {133.333333} \
   CONFIG.PCW_TTC1_CLK2_PERIPHERAL_CLKSRC {CPU_1X} \
   CONFIG.PCW_TTC1_CLK2_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_TTC1_CLK2_PERIPHERAL_FREQMHZ {133.333333} \
   CONFIG.PCW_TTC1_HIGHADDR {0xE0105fff} \
   CONFIG.PCW_TTC1_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_TTC_PERIPHERAL_FREQMHZ {50} \
   CONFIG.PCW_UART0_BASEADDR {0xE0000000} \
   CONFIG.PCW_UART0_BAUD_RATE {115200} \
   CONFIG.PCW_UART0_GRP_FULL_ENABLE {0} \
   CONFIG.PCW_UART0_HIGHADDR {0xE0000FFF} \
   CONFIG.PCW_UART0_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_UART1_BASEADDR {0xE0001000} \
   CONFIG.PCW_UART1_BAUD_RATE {115200} \
   CONFIG.PCW_UART1_GRP_FULL_ENABLE {0} \
   CONFIG.PCW_UART1_HIGHADDR {0xE0001FFF} \
   CONFIG.PCW_UART1_PERIPHERAL_ENABLE {1} \
   CONFIG.PCW_UART1_UART1_IO {MIO 48 .. 49} \
   CONFIG.PCW_UART_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_UART_PERIPHERAL_DIVISOR0 {32} \
   CONFIG.PCW_UART_PERIPHERAL_FREQMHZ {50} \
   CONFIG.PCW_UART_PERIPHERAL_VALID {1} \
   CONFIG.PCW_UIPARAM_ACT_DDR_FREQ_MHZ {533.333374} \
   CONFIG.PCW_UIPARAM_DDR_ADV_ENABLE {0} \
   CONFIG.PCW_UIPARAM_DDR_AL {0} \
   CONFIG.PCW_UIPARAM_DDR_BANK_ADDR_COUNT {3} \
   CONFIG.PCW_UIPARAM_DDR_BL {8} \
   CONFIG.PCW_UIPARAM_DDR_BOARD_DELAY0 {0.41} \
   CONFIG.PCW_UIPARAM_DDR_BOARD_DELAY1 {0.411} \
   CONFIG.PCW_UIPARAM_DDR_BOARD_DELAY2 {0.341} \
   CONFIG.PCW_UIPARAM_DDR_BOARD_DELAY3 {0.358} \
   CONFIG.PCW_UIPARAM_DDR_BUS_WIDTH {32 Bit} \
   CONFIG.PCW_UIPARAM_DDR_CL {7} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_0_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_0_PACKAGE_LENGTH {61.0905} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_0_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_1_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_1_PACKAGE_LENGTH {61.0905} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_1_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_2_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_2_PACKAGE_LENGTH {61.0905} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_2_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_3_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_3_PACKAGE_LENGTH {61.0905} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_3_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_STOP_EN {0} \
   CONFIG.PCW_UIPARAM_DDR_COL_ADDR_COUNT {10} \
   CONFIG.PCW_UIPARAM_DDR_CWL {6} \
   CONFIG.PCW_UIPARAM_DDR_DEVICE_CAPACITY {2048 MBits} \
   CONFIG.PCW_UIPARAM_DDR_DQS_0_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_DQS_0_PACKAGE_LENGTH {68.4725} \
   CONFIG.PCW_UIPARAM_DDR_DQS_0_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_DQS_1_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_DQS_1_PACKAGE_LENGTH {71.086} \
   CONFIG.PCW_UIPARAM_DDR_DQS_1_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_DQS_2_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_DQS_2_PACKAGE_LENGTH {66.794} \
   CONFIG.PCW_UIPARAM_DDR_DQS_2_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_DQS_3_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_DQS_3_PACKAGE_LENGTH {108.7385} \
   CONFIG.PCW_UIPARAM_DDR_DQS_3_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_DQS_TO_CLK_DELAY_0 {0.025} \
   CONFIG.PCW_UIPARAM_DDR_DQS_TO_CLK_DELAY_1 {0.028} \
   CONFIG.PCW_UIPARAM_DDR_DQS_TO_CLK_DELAY_2 {-0.009} \
   CONFIG.PCW_UIPARAM_DDR_DQS_TO_CLK_DELAY_3 {-0.061} \
   CONFIG.PCW_UIPARAM_DDR_DQ_0_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_DQ_0_PACKAGE_LENGTH {64.1705} \
   CONFIG.PCW_UIPARAM_DDR_DQ_0_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_DQ_1_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_DQ_1_PACKAGE_LENGTH {63.686} \
   CONFIG.PCW_UIPARAM_DDR_DQ_1_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_DQ_2_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_DQ_2_PACKAGE_LENGTH {68.46} \
   CONFIG.PCW_UIPARAM_DDR_DQ_2_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_DQ_3_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_DQ_3_PACKAGE_LENGTH {105.4895} \
   CONFIG.PCW_UIPARAM_DDR_DQ_3_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_DRAM_WIDTH {16 Bits} \
   CONFIG.PCW_UIPARAM_DDR_ECC {Disabled} \
   CONFIG.PCW_UIPARAM_DDR_ENABLE {1} \
   CONFIG.PCW_UIPARAM_DDR_FREQ_MHZ {533.333313} \
   CONFIG.PCW_UIPARAM_DDR_HIGH_TEMP {Normal (0-85)} \
   CONFIG.PCW_UIPARAM_DDR_MEMORY_TYPE {DDR 3} \
   CONFIG.PCW_UIPARAM_DDR_PARTNO {MT41J128M16 HA-15E} \
   CONFIG.PCW_UIPARAM_DDR_ROW_ADDR_COUNT {14} \
   CONFIG.PCW_UIPARAM_DDR_SPEED_BIN {DDR3_1066F} \
   CONFIG.PCW_UIPARAM_DDR_TRAIN_DATA_EYE {1} \
   CONFIG.PCW_UIPARAM_DDR_TRAIN_READ_GATE {1} \
   CONFIG.PCW_UIPARAM_DDR_TRAIN_WRITE_LEVEL {1} \
   CONFIG.PCW_UIPARAM_DDR_T_FAW {45.0} \
   CONFIG.PCW_UIPARAM_DDR_T_RAS_MIN {36.0} \
   CONFIG.PCW_UIPARAM_DDR_T_RC {49.5} \
   CONFIG.PCW_UIPARAM_DDR_T_RCD {7} \
   CONFIG.PCW_UIPARAM_DDR_T_RP {7} \
   CONFIG.PCW_UIPARAM_DDR_USE_INTERNAL_VREF {1} \
   CONFIG.PCW_UIPARAM_GENERATE_SUMMARY {NA} \
   CONFIG.PCW_USB0_BASEADDR {0xE0102000} \
   CONFIG.PCW_USB0_HIGHADDR {0xE0102fff} \
   CONFIG.PCW_USB0_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_USB0_PERIPHERAL_FREQMHZ {60} \
   CONFIG.PCW_USB0_RESET_ENABLE {0} \
   CONFIG.PCW_USB0_USB0_IO {<Select>} \
   CONFIG.PCW_USB1_BASEADDR {0xE0103000} \
   CONFIG.PCW_USB1_HIGHADDR {0xE0103fff} \
   CONFIG.PCW_USB1_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_USB1_PERIPHERAL_FREQMHZ {60} \
   CONFIG.PCW_USB1_RESET_ENABLE {0} \
   CONFIG.PCW_USB_RESET_ENABLE {0} \
   CONFIG.PCW_USB_RESET_POLARITY {Active Low} \
   CONFIG.PCW_USB_RESET_SELECT {<Select>} \
   CONFIG.PCW_USE_AXI_FABRIC_IDLE {0} \
   CONFIG.PCW_USE_AXI_NONSECURE {0} \
   CONFIG.PCW_USE_CORESIGHT {0} \
   CONFIG.PCW_USE_CROSS_TRIGGER {0} \
   CONFIG.PCW_USE_CR_FABRIC {1} \
   CONFIG.PCW_USE_DDR_BYPASS {0} \
   CONFIG.PCW_USE_DEBUG {0} \
   CONFIG.PCW_USE_DEFAULT_ACP_USER_VAL {0} \
   CONFIG.PCW_USE_DMA0 {0} \
   CONFIG.PCW_USE_DMA1 {0} \
   CONFIG.PCW_USE_DMA2 {0} \
   CONFIG.PCW_USE_DMA3 {0} \
   CONFIG.PCW_USE_EXPANDED_IOP {0} \
   CONFIG.PCW_USE_EXPANDED_PS_SLCR_REGISTERS {0} \
   CONFIG.PCW_USE_FABRIC_INTERRUPT {1} \
   CONFIG.PCW_USE_HIGH_OCM {0} \
   CONFIG.PCW_USE_M_AXI_GP0 {1} \
   CONFIG.PCW_USE_M_AXI_GP1 {0} \
   CONFIG.PCW_USE_PROC_EVENT_BUS {0} \
   CONFIG.PCW_USE_PS_SLCR_REGISTERS {0} \
   CONFIG.PCW_USE_S_AXI_ACP {0} \
   CONFIG.PCW_USE_S_AXI_GP0 {0} \
   CONFIG.PCW_USE_S_AXI_GP1 {0} \
   CONFIG.PCW_USE_S_AXI_HP0 {1} \
   CONFIG.PCW_USE_S_AXI_HP1 {0} \
   CONFIG.PCW_USE_S_AXI_HP2 {0} \
   CONFIG.PCW_USE_S_AXI_HP3 {0} \
   CONFIG.PCW_USE_TRACE {0} \
   CONFIG.PCW_USE_TRACE_DATA_EDGE_DETECTOR {0} \
   CONFIG.PCW_VALUE_SILVERSION {3} \
   CONFIG.PCW_WDT_PERIPHERAL_CLKSRC {CPU_1X} \
   CONFIG.PCW_WDT_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_WDT_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_WDT_PERIPHERAL_FREQMHZ {133.333333} \
   CONFIG.preset {ZedBoard} \
 ] $processing_system7_0

  # Create instance: rst_processing_system7_0_100M, and set properties
  set rst_processing_system7_0_100M [ create_bd_cell -type ip -vlnv xilinx.com:ip:proc_sys_reset:5.0 rst_processing_system7_0_100M ]
  set_property -dict [ list \
   CONFIG.C_AUX_RESET_HIGH {0} \
 ] $rst_processing_system7_0_100M

  # Create instance: util_vector_logic_0, and set properties
  set util_vector_logic_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic:2.0 util_vector_logic_0 ]
  set_property -dict [ list \
   CONFIG.C_OPERATION {not} \
   CONFIG.C_SIZE {1} \
   CONFIG.LOGO_FILE {data/sym_notgate.png} \
 ] $util_vector_logic_0

  # Create instance: xlconstant_0, and set properties
  set xlconstant_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 xlconstant_0 ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {0} \
 ] $xlconstant_0

  # Create interface connections
  connect_bd_intf_net -intf_net AXI_BRAM_DEBUG_BRAM_PORTA [get_bd_intf_pins AXI_BRAM_DEBUG/BRAM_PORTA] [get_bd_intf_pins BRAM_DEBUG/BRAM_PORTA]
  connect_bd_intf_net -intf_net axi_bram_ctrl_2_BRAM_PORTA [get_bd_intf_pins AXI_BRAM_Security_Policy_Config/BRAM_PORTA] [get_bd_intf_pins BRAM_Security_Policy_Config/BRAM_PORTA]
  connect_bd_intf_net -intf_net axi_bram_ctrl_annot_bb_BRAM_PORTA [get_bd_intf_pins AXI_BRAM_BBT_Annotations/BRAM_PORTA] [get_bd_intf_pins BRAM_BBT_Annotations/BRAM_PORTA]
  connect_bd_intf_net -intf_net axi_mem_intercon_1_M00_AXI [get_bd_intf_pins AXI_BRAM_Kernel_to_Monitor/S_AXI] [get_bd_intf_pins axi_mem_intercon_1/M00_AXI]
  connect_bd_intf_net -intf_net axi_mem_intercon_1_M01_AXI [get_bd_intf_pins AXI_BRAM_Security_Policy_Config/S_AXI] [get_bd_intf_pins axi_mem_intercon_1/M01_AXI]
  connect_bd_intf_net -intf_net axi_mem_intercon_1_M02_AXI [get_bd_intf_pins AXI_FIFO_Instrumentation/S_AXI] [get_bd_intf_pins axi_mem_intercon_1/M02_AXI]
  connect_bd_intf_net -intf_net axi_mem_intercon_1_M03_AXI [get_bd_intf_pins AXI_BRAM_Monitor_to_Kernel/S_AXI] [get_bd_intf_pins axi_mem_intercon_1/M03_AXI]
  connect_bd_intf_net -intf_net axi_mem_intercon_1_M04_AXI [get_bd_intf_pins AXI_BRAM_BBT_Annotations/S_AXI] [get_bd_intf_pins axi_mem_intercon_1/M04_AXI]
  connect_bd_intf_net -intf_net axi_mem_intercon_1_M05_AXI [get_bd_intf_pins AXI_BRAM_DEBUG/S_AXI] [get_bd_intf_pins axi_mem_intercon_1/M05_AXI]
  connect_bd_intf_net -intf_net axi_mem_intercon_1_M06_AXI [get_bd_intf_pins PL_to_PS/s00_axi] [get_bd_intf_pins axi_mem_intercon_1/M06_AXI]
  connect_bd_intf_net -intf_net axi_mem_intercon_1_M07_AXI [get_bd_intf_pins PL_to_PS/s_axi_intr] [get_bd_intf_pins axi_mem_intercon_1/M07_AXI]
  connect_bd_intf_net -intf_net axi_mem_intercon_M00_AXI [get_bd_intf_pins axi_mem_intercon/M00_AXI] [get_bd_intf_pins processing_system7_0/S_AXI_HP0]
  connect_bd_intf_net -intf_net myip_v2_0_0_m00_axi [get_bd_intf_pins axi_mem_intercon/S00_AXI] [get_bd_intf_pins myip_v2_0_0/m00_axi]
  connect_bd_intf_net -intf_net processing_system7_0_DDR [get_bd_intf_ports DDR] [get_bd_intf_pins processing_system7_0/DDR]
  connect_bd_intf_net -intf_net processing_system7_0_FIXED_IO [get_bd_intf_ports FIXED_IO] [get_bd_intf_pins processing_system7_0/FIXED_IO]
  connect_bd_intf_net -intf_net processing_system7_0_M_AXI_GP0 [get_bd_intf_pins axi_mem_intercon_1/S00_AXI] [get_bd_intf_pins processing_system7_0/M_AXI_GP0]

  # Create port connections
  connect_bd_net -net AXI_BRAM_Kernel_to_Monitor_bram_addr_a [get_bd_pins AXI_BRAM_Kernel_to_Monitor/bram_addr_a] [get_bd_pins BRAM_to_FIFO_Kernel_to_Monitor/from_bram_address]
  connect_bd_net -net AXI_BRAM_Kernel_to_Monitor_bram_clk_a [get_bd_pins AXI_BRAM_Kernel_to_Monitor/bram_clk_a] [get_bd_pins BRAM_to_FIFO_Kernel_to_Monitor/from_bram_clk]
  connect_bd_net -net AXI_BRAM_Kernel_to_Monitor_bram_en_a [get_bd_pins AXI_BRAM_Kernel_to_Monitor/bram_en_a] [get_bd_pins BRAM_to_FIFO_Kernel_to_Monitor/from_bram_enable]
  connect_bd_net -net AXI_BRAM_Kernel_to_Monitor_bram_rst_a [get_bd_pins AXI_BRAM_Kernel_to_Monitor/bram_rst_a] [get_bd_pins BRAM_to_FIFO_Kernel_to_Monitor/from_bram_reset]
  connect_bd_net -net AXI_BRAM_Kernel_to_Monitor_bram_we_a [get_bd_pins AXI_BRAM_Kernel_to_Monitor/bram_we_a] [get_bd_pins BRAM_to_FIFO_Kernel_to_Monitor/from_bram_bytes_selection]
  connect_bd_net -net AXI_BRAM_Kernel_to_Monitor_bram_wrdata_a [get_bd_pins AXI_BRAM_Kernel_to_Monitor/bram_wrdata_a] [get_bd_pins BRAM_to_FIFO_Kernel_to_Monitor/from_bram_data_write]
  connect_bd_net -net AXI_BRAM_Monitor_to_Kernel_bram_addr_a [get_bd_pins AXI_BRAM_Monitor_to_Kernel/bram_addr_a] [get_bd_pins BRAM_to_FIFO_Monitor_to_Kernel/from_bram_address]
  connect_bd_net -net AXI_BRAM_Monitor_to_Kernel_bram_clk_a [get_bd_pins AXI_BRAM_Monitor_to_Kernel/bram_clk_a] [get_bd_pins BRAM_to_FIFO_Monitor_to_Kernel/from_bram_clk]
  connect_bd_net -net AXI_BRAM_Monitor_to_Kernel_bram_en_a [get_bd_pins AXI_BRAM_Monitor_to_Kernel/bram_en_a] [get_bd_pins BRAM_to_FIFO_Monitor_to_Kernel/from_bram_enable]
  connect_bd_net -net AXI_BRAM_Monitor_to_Kernel_bram_rst_a [get_bd_pins AXI_BRAM_Monitor_to_Kernel/bram_rst_a] [get_bd_pins BRAM_to_FIFO_Monitor_to_Kernel/from_bram_reset]
  connect_bd_net -net AXI_BRAM_Monitor_to_Kernel_bram_we_a [get_bd_pins AXI_BRAM_Monitor_to_Kernel/bram_we_a] [get_bd_pins BRAM_to_FIFO_Monitor_to_Kernel/from_bram_bytes_selection]
  connect_bd_net -net AXI_FIFO_Instrumentation_bram_addr_a [get_bd_pins AXI_FIFO_Instrumentation/bram_addr_a] [get_bd_pins BRAM_to_FIFO_Instrumentation/from_bram_address]
  connect_bd_net -net AXI_FIFO_Instrumentation_bram_clk_a [get_bd_pins AXI_FIFO_Instrumentation/bram_clk_a] [get_bd_pins BRAM_to_FIFO_Instrumentation/from_bram_clk]
  connect_bd_net -net AXI_FIFO_Instrumentation_bram_en_a [get_bd_pins AXI_FIFO_Instrumentation/bram_en_a] [get_bd_pins BRAM_to_FIFO_Instrumentation/from_bram_enable]
  connect_bd_net -net AXI_FIFO_Instrumentation_bram_rst_a [get_bd_pins AXI_FIFO_Instrumentation/bram_rst_a] [get_bd_pins BRAM_to_FIFO_Instrumentation/from_bram_reset]
  connect_bd_net -net AXI_FIFO_Instrumentation_bram_we_a [get_bd_pins AXI_FIFO_Instrumentation/bram_we_a] [get_bd_pins BRAM_to_FIFO_Instrumentation/from_bram_bytes_selection]
  connect_bd_net -net AXI_FIFO_Instrumentation_bram_wrdata_a [get_bd_pins AXI_FIFO_Instrumentation/bram_wrdata_a] [get_bd_pins BRAM_to_FIFO_Instrumentation/from_bram_data_write]
  connect_bd_net -net BRAM_BBT_Annotations_doutb [get_bd_pins BRAM_BBT_Annotations/doutb] [get_bd_pins Dispatcher_HardBlare/bram_bbt_annotations_data_read]
  connect_bd_net -net BRAM_DEBUG_doutb [get_bd_pins BRAM_DEBUG/doutb] [get_bd_pins Dispatcher_HardBlare/bram_debug_data_read]
  connect_bd_net -net BRAM_Firmware_code_douta [get_bd_pins BRAM_Firmware_code/douta] [get_bd_pins Dispatcher_HardBlare/bram_firmware_code_data_read]
  connect_bd_net -net BRAM_Security_Policy_Config_doutb [get_bd_pins BRAM_Security_Policy_Config/doutb] [get_bd_pins Dispatcher_HardBlare/bram_sec_policy_config_data_read]
  connect_bd_net -net BRAM_to_FIFO_0_to_bram_data_read [get_bd_pins AXI_BRAM_Monitor_to_Kernel/bram_rddata_a] [get_bd_pins BRAM_to_FIFO_Monitor_to_Kernel/to_bram_data_read]
  connect_bd_net -net BRAM_to_FIFO_0_to_fifo_clk [get_bd_pins BRAM_to_FIFO_Instrumentation/to_fifo_clk] [get_bd_pins FIFO_Instrumentation/clk]
  connect_bd_net -net BRAM_to_FIFO_0_to_fifo_clk1 [get_bd_pins BRAM_to_FIFO_Monitor_to_Kernel/to_fifo_clk] [get_bd_pins FIFO_Monitor_to_Kernel/rd_clk]
  connect_bd_net -net BRAM_to_FIFO_0_to_fifo_data_write [get_bd_pins BRAM_to_FIFO_Instrumentation/to_fifo_data_write] [get_bd_pins FIFO_Instrumentation/din]
  connect_bd_net -net BRAM_to_FIFO_0_to_fifo_data_write1 [get_bd_pins BRAM_to_FIFO_Kernel_to_Monitor/to_fifo_data_write] [get_bd_pins FIFO_Kernel_to_Monitor/din]
  connect_bd_net -net BRAM_to_FIFO_0_to_fifo_read_enable [get_bd_pins BRAM_to_FIFO_Monitor_to_Kernel/to_fifo_read_enable] [get_bd_pins FIFO_Monitor_to_Kernel/rd_en]
  connect_bd_net -net BRAM_to_FIFO_0_to_fifo_reset [get_bd_pins BRAM_to_FIFO_Instrumentation/to_fifo_reset] [get_bd_pins FIFO_Instrumentation/srst]
  connect_bd_net -net BRAM_to_FIFO_0_to_fifo_write_enable [get_bd_pins BRAM_to_FIFO_Instrumentation/to_fifo_write_enable] [get_bd_pins FIFO_Instrumentation/wr_en]
  connect_bd_net -net BRAM_to_FIFO_0_to_fifo_write_enable1 [get_bd_pins BRAM_to_FIFO_Kernel_to_Monitor/to_fifo_write_enable] [get_bd_pins FIFO_Kernel_to_Monitor/wr_en]
  connect_bd_net -net Dispatcher_HardBlare_0_bram_bbt_annotations_address [get_bd_pins BRAM_BBT_Annotations/addrb] [get_bd_pins Dispatcher_HardBlare/bram_bbt_annotations_address]
  connect_bd_net -net Dispatcher_HardBlare_0_bram_bbt_annotations_bytes_selection [get_bd_pins BRAM_BBT_Annotations/web] [get_bd_pins Dispatcher_HardBlare/bram_bbt_annotations_bytes_selection]
  connect_bd_net -net Dispatcher_HardBlare_0_bram_bbt_annotations_data_write [get_bd_pins BRAM_BBT_Annotations/dinb] [get_bd_pins Dispatcher_HardBlare/bram_bbt_annotations_data_write]
  connect_bd_net -net Dispatcher_HardBlare_0_bram_debug_address [get_bd_pins BRAM_DEBUG/addrb] [get_bd_pins Dispatcher_HardBlare/bram_debug_address]
  connect_bd_net -net Dispatcher_HardBlare_0_bram_debug_bytes_selection [get_bd_pins BRAM_DEBUG/web] [get_bd_pins Dispatcher_HardBlare/bram_debug_bytes_selection]
  connect_bd_net -net Dispatcher_HardBlare_0_bram_debug_data_write [get_bd_pins BRAM_DEBUG/dinb] [get_bd_pins Dispatcher_HardBlare/bram_debug_data_write]
  connect_bd_net -net Dispatcher_HardBlare_0_bram_firmware_code_address [get_bd_pins BRAM_Firmware_code/addra] [get_bd_pins Dispatcher_HardBlare/bram_firmware_code_address]
  connect_bd_net -net Dispatcher_HardBlare_0_bram_firmware_code_bytes_selection [get_bd_pins BRAM_Firmware_code/wea] [get_bd_pins Dispatcher_HardBlare/bram_firmware_code_bytes_selection]
  connect_bd_net -net Dispatcher_HardBlare_0_bram_firmware_code_data_write [get_bd_pins BRAM_Firmware_code/dina] [get_bd_pins Dispatcher_HardBlare/bram_firmware_code_data_write]
  connect_bd_net -net Dispatcher_HardBlare_0_bram_firmware_code_en [get_bd_pins BRAM_Firmware_code/ena] [get_bd_pins Dispatcher_HardBlare/bram_firmware_code_en]
  connect_bd_net -net Dispatcher_HardBlare_0_fifo_kernel_to_monitor_en [get_bd_pins Dispatcher_HardBlare/fifo_kernel_to_monitor_en] [get_bd_pins FIFO_Kernel_to_Monitor/rd_en]
  connect_bd_net -net Dispatcher_HardBlare_0_fifo_monitor_to_kernel_data_write [get_bd_pins Dispatcher_HardBlare/fifo_monitor_to_kernel_data_write] [get_bd_pins FIFO_Monitor_to_Kernel/din]
  connect_bd_net -net Dispatcher_HardBlare_0_fifo_monitor_to_kernel_en [get_bd_pins Dispatcher_HardBlare/fifo_monitor_to_kernel_en] [get_bd_pins FIFO_Monitor_to_Kernel/wr_en]
  connect_bd_net -net Dispatcher_HardBlare_0_fifo_ptm_en [get_bd_pins Dispatcher_HardBlare/fifo_ptm_en] [get_bd_pins FIFO_PTM_TRACES/rd_en]
  connect_bd_net -net Dispatcher_HardBlare_0_instrumentation_request_read_en [get_bd_pins Dispatcher_HardBlare/fifo_read_instrumentation_request_read_en] [get_bd_pins FIFO_Instrumentation/rd_en]
  connect_bd_net -net Dispatcher_HardBlare_PLtoPS_address [get_bd_pins Dispatcher_HardBlare/PLtoPS_address] [get_bd_pins PL_to_PS/from_Dispatcher_address]
  connect_bd_net -net Dispatcher_HardBlare_PLtoPS_bytes_selection [get_bd_pins Dispatcher_HardBlare/PLtoPS_bytes_selection] [get_bd_pins PL_to_PS/from_Dispatcher_bytes_selection]
  connect_bd_net -net Dispatcher_HardBlare_PLtoPS_clk [get_bd_pins Dispatcher_HardBlare/PLtoPS_clk] [get_bd_pins PL_to_PS/from_Dispatcher_clk]
  connect_bd_net -net Dispatcher_HardBlare_PLtoPS_enable [get_bd_pins Dispatcher_HardBlare/PLtoPS_enable] [get_bd_pins PL_to_PS/from_Dispatcher_enable]
  connect_bd_net -net Dispatcher_HardBlare_PLtoPS_generate_interrupt [get_bd_pins Dispatcher_HardBlare/PLtoPS_generate_interrupt] [get_bd_pins PL_to_PS/dispatcher_generate_interrupt]
  connect_bd_net -net Dispatcher_HardBlare_PLtoPS_reset [get_bd_pins Dispatcher_HardBlare/PLtoPS_reset] [get_bd_pins PL_to_PS/from_Dispatcher_reset]
  connect_bd_net -net Dispatcher_HardBlare_PLtoPS_write_data [get_bd_pins Dispatcher_HardBlare/PLtoPS_write_data] [get_bd_pins PL_to_PS/from_Dispatcher_data_in]
  connect_bd_net -net Dispatcher_HardBlare_bram_bbt_annotations_clk [get_bd_pins BRAM_BBT_Annotations/clkb] [get_bd_pins Dispatcher_HardBlare/bram_bbt_annotations_clk]
  connect_bd_net -net Dispatcher_HardBlare_bram_bbt_annotations_en [get_bd_pins BRAM_BBT_Annotations/enb] [get_bd_pins Dispatcher_HardBlare/bram_bbt_annotations_en]
  connect_bd_net -net Dispatcher_HardBlare_bram_bbt_annotations_reset [get_bd_pins BRAM_BBT_Annotations/rstb] [get_bd_pins Dispatcher_HardBlare/bram_bbt_annotations_reset]
  connect_bd_net -net Dispatcher_HardBlare_bram_debug_clk [get_bd_pins BRAM_DEBUG/clkb] [get_bd_pins Dispatcher_HardBlare/bram_debug_clk]
  connect_bd_net -net Dispatcher_HardBlare_bram_debug_en [get_bd_pins BRAM_DEBUG/enb] [get_bd_pins Dispatcher_HardBlare/bram_debug_en]
  connect_bd_net -net Dispatcher_HardBlare_bram_debug_reset [get_bd_pins BRAM_DEBUG/rstb] [get_bd_pins Dispatcher_HardBlare/bram_debug_reset]
  connect_bd_net -net Dispatcher_HardBlare_bram_firmware_code_clk [get_bd_pins BRAM_Firmware_code/clka] [get_bd_pins Dispatcher_HardBlare/bram_firmware_code_clk]
  connect_bd_net -net Dispatcher_HardBlare_bram_firmware_code_reset [get_bd_pins BRAM_Firmware_code/rsta] [get_bd_pins Dispatcher_HardBlare/bram_firmware_code_reset]
  connect_bd_net -net Dispatcher_HardBlare_bram_sec_policy_config_address [get_bd_pins BRAM_Security_Policy_Config/addrb] [get_bd_pins Dispatcher_HardBlare/bram_sec_policy_config_address]
  connect_bd_net -net Dispatcher_HardBlare_bram_sec_policy_config_bytes_selection [get_bd_pins BRAM_Security_Policy_Config/web] [get_bd_pins Dispatcher_HardBlare/bram_sec_policy_config_bytes_selection]
  connect_bd_net -net Dispatcher_HardBlare_bram_sec_policy_config_clk [get_bd_pins BRAM_Security_Policy_Config/clkb] [get_bd_pins Dispatcher_HardBlare/bram_sec_policy_config_clk]
  connect_bd_net -net Dispatcher_HardBlare_bram_sec_policy_config_data_write [get_bd_pins BRAM_Security_Policy_Config/dinb] [get_bd_pins Dispatcher_HardBlare/bram_sec_policy_config_data_write]
  connect_bd_net -net Dispatcher_HardBlare_bram_sec_policy_config_en [get_bd_pins BRAM_Security_Policy_Config/enb] [get_bd_pins Dispatcher_HardBlare/bram_sec_policy_config_en]
  connect_bd_net -net Dispatcher_HardBlare_bram_sec_policy_config_reset [get_bd_pins BRAM_Security_Policy_Config/rstb] [get_bd_pins Dispatcher_HardBlare/bram_sec_policy_config_reset]
  connect_bd_net -net Dispatcher_HardBlare_fifo_kernel_to_monitor_clk [get_bd_pins Dispatcher_HardBlare/fifo_kernel_to_monitor_clk] [get_bd_pins FIFO_Kernel_to_Monitor/clk]
  connect_bd_net -net Dispatcher_HardBlare_fifo_kernel_to_monitor_reset [get_bd_pins Dispatcher_HardBlare/fifo_kernel_to_monitor_reset] [get_bd_pins FIFO_Kernel_to_Monitor/srst]
  connect_bd_net -net Dispatcher_HardBlare_fifo_monitor_to_kernel_clk [get_bd_pins Dispatcher_HardBlare/fifo_monitor_to_kernel_clk] [get_bd_pins FIFO_Monitor_to_Kernel/wr_clk]
  connect_bd_net -net Dispatcher_HardBlare_fifo_ptm_clk [get_bd_pins Dispatcher_HardBlare/fifo_ptm_clk] [get_bd_pins FIFO_PTM_TRACES/rd_clk]
  connect_bd_net -net Dispatcher_HardBlare_fifo_ptm_reset [get_bd_pins Dispatcher_HardBlare/fifo_ptm_reset] [get_bd_pins FIFO_PTM_TRACES/rd_rst]
  connect_bd_net -net FIFO_Instrumentation_almost_empty [get_bd_pins Dispatcher_HardBlare/fifo_instrumentation_almost_empty] [get_bd_pins FIFO_Instrumentation/almost_empty] [get_bd_pins PL_to_PS/fifo_instrumentation_almost_empty]
  connect_bd_net -net FIFO_Instrumentation_dout [get_bd_pins AXI_FIFO_Instrumentation/bram_rddata_a] [get_bd_pins Dispatcher_HardBlare/fifo_read_instrumentation_data_read] [get_bd_pins FIFO_Instrumentation/dout]
  connect_bd_net -net FIFO_Instrumentation_empty [get_bd_pins Dispatcher_HardBlare/fifo_instrumentation_empty] [get_bd_pins FIFO_Instrumentation/empty] [get_bd_pins PL_to_PS/fifo_instrumentation_empty]
  connect_bd_net -net FIFO_Instrumentation_full [get_bd_pins Dispatcher_HardBlare/fifo_instrumentation_full] [get_bd_pins FIFO_Instrumentation/full] [get_bd_pins PL_to_PS/fifo_instrumentation_full]
  connect_bd_net -net FIFO_Instrumentation_prog_full [get_bd_pins Dispatcher_HardBlare/fifo_instrumentation_almost_full] [get_bd_pins FIFO_Instrumentation/prog_full] [get_bd_pins PL_to_PS/fifo_instrumentation_almost_full]
  connect_bd_net -net FIFO_Kernel_to_Monitor_almost_empty [get_bd_pins Dispatcher_HardBlare/fifo_kernel_to_monitor_almost_empty] [get_bd_pins FIFO_Kernel_to_Monitor/almost_empty] [get_bd_pins PL_to_PS/fifo_kernel_to_monitor_almost_empty]
  connect_bd_net -net FIFO_Kernel_to_Monitor_dout [get_bd_pins Dispatcher_HardBlare/fifo_kernel_to_monitor_data_read] [get_bd_pins FIFO_Kernel_to_Monitor/dout]
  connect_bd_net -net FIFO_Kernel_to_Monitor_empty [get_bd_pins Dispatcher_HardBlare/fifo_kernel_to_monitor_empty] [get_bd_pins FIFO_Kernel_to_Monitor/empty] [get_bd_pins PL_to_PS/fifo_kernel_to_monitor_empty]
  connect_bd_net -net FIFO_Kernel_to_Monitor_full [get_bd_pins Dispatcher_HardBlare/fifo_kernel_to_monitor_full] [get_bd_pins FIFO_Kernel_to_Monitor/full] [get_bd_pins PL_to_PS/fifo_kernel_to_monitor_full]
  connect_bd_net -net FIFO_Kernel_to_Monitor_prog_full [get_bd_pins Dispatcher_HardBlare/fifo_kernel_to_monitor_almost_full] [get_bd_pins FIFO_Kernel_to_Monitor/prog_full] [get_bd_pins PL_to_PS/fifo_kernel_to_monitor_almost_full]
  connect_bd_net -net FIFO_Monito_to_Kernel_almost_empty [get_bd_pins Dispatcher_HardBlare/fifo_monitor_to_kernel_almost_empty] [get_bd_pins FIFO_Monitor_to_Kernel/almost_empty] [get_bd_pins PL_to_PS/fifo_monitor_to_kernel_almost_empty]
  connect_bd_net -net FIFO_Monito_to_Kernel_empty [get_bd_pins Dispatcher_HardBlare/fifo_monitor_to_kernel_empty] [get_bd_pins FIFO_Monitor_to_Kernel/empty] [get_bd_pins PL_to_PS/fifo_monitor_to_kernel_empty]
  connect_bd_net -net FIFO_Monito_to_Kernel_full [get_bd_pins Dispatcher_HardBlare/fifo_monitor_to_kernel_full] [get_bd_pins FIFO_Monitor_to_Kernel/full] [get_bd_pins PL_to_PS/fifo_monitor_to_kernel_full]
  connect_bd_net -net FIFO_Monitor_to_Kernel_dout [get_bd_pins BRAM_to_FIFO_Monitor_to_Kernel/from_fifo_data_read] [get_bd_pins FIFO_Monitor_to_Kernel/dout]
  connect_bd_net -net FIFO_Monitor_to_Kernel_prog_full [get_bd_pins Dispatcher_HardBlare/fifo_monitor_to_kernel_almost_full] [get_bd_pins FIFO_Monitor_to_Kernel/almost_full] [get_bd_pins PL_to_PS/fifo_monitor_to_kernel_almost_full]
  connect_bd_net -net FIFO_PTM_TRACES_almost_empty [get_bd_pins Dispatcher_HardBlare/fifo_ptm_almost_empty] [get_bd_pins FIFO_PTM_TRACES/almost_empty] [get_bd_pins PL_to_PS/fifo_ptm_almost_empty]
  connect_bd_net -net FIFO_PTM_TRACES_dout [get_bd_pins Dispatcher_HardBlare/fifo_ptm_data_read] [get_bd_pins FIFO_PTM_TRACES/dout]
  connect_bd_net -net FIFO_PTM_TRACES_empty [get_bd_pins Dispatcher_HardBlare/fifo_ptm_empty] [get_bd_pins FIFO_PTM_TRACES/empty] [get_bd_pins PL_to_PS/fifo_ptm_empty]
  connect_bd_net -net FIFO_PTM_TRACES_full [get_bd_pins Dispatcher_HardBlare/fifo_ptm_full] [get_bd_pins FIFO_PTM_TRACES/full] [get_bd_pins PL_to_PS/fifo_ptm_full]
  connect_bd_net -net FIFO_PTM_TRACES_prog_full [get_bd_pins Dispatcher_HardBlare/fifo_ptm_almost_full] [get_bd_pins FIFO_PTM_TRACES/prog_full] [get_bd_pins PL_to_PS/fifo_ptm_almost_full]
  connect_bd_net -net PL_to_PS_0_irq [get_bd_pins PL_to_PS/irq] [get_bd_pins processing_system7_0/IRQ_F2P]
  connect_bd_net -net PL_to_PS_0_to_Dispatcher_data_out [get_bd_pins Dispatcher_HardBlare/PLtoPS_readed_data] [get_bd_pins PL_to_PS/to_Dispatcher_data_out]
  connect_bd_net -net decodeur_traces_1_pc [get_bd_pins FIFO_PTM_TRACES/din] [get_bd_pins decodeur_traces/pc]
  connect_bd_net -net decodeur_traces_1_w_en [get_bd_pins FIFO_PTM_TRACES/wr_en] [get_bd_pins decodeur_traces/w_en]
  connect_bd_net -net processing_system7_0_FCLK_CLK0 [get_bd_pins AXI_BRAM_BBT_Annotations/s_axi_aclk] [get_bd_pins AXI_BRAM_DEBUG/s_axi_aclk] [get_bd_pins AXI_BRAM_Kernel_to_Monitor/s_axi_aclk] [get_bd_pins AXI_BRAM_Monitor_to_Kernel/s_axi_aclk] [get_bd_pins AXI_BRAM_Security_Policy_Config/s_axi_aclk] [get_bd_pins AXI_FIFO_Instrumentation/s_axi_aclk] [get_bd_pins Dispatcher_HardBlare/clk] [get_bd_pins PL_to_PS/s00_axi_aclk] [get_bd_pins PL_to_PS/s_axi_intr_aclk] [get_bd_pins axi_mem_intercon/ACLK] [get_bd_pins axi_mem_intercon/M00_ACLK] [get_bd_pins axi_mem_intercon/S00_ACLK] [get_bd_pins axi_mem_intercon_1/ACLK] [get_bd_pins axi_mem_intercon_1/M00_ACLK] [get_bd_pins axi_mem_intercon_1/M01_ACLK] [get_bd_pins axi_mem_intercon_1/M02_ACLK] [get_bd_pins axi_mem_intercon_1/M03_ACLK] [get_bd_pins axi_mem_intercon_1/M04_ACLK] [get_bd_pins axi_mem_intercon_1/M05_ACLK] [get_bd_pins axi_mem_intercon_1/M06_ACLK] [get_bd_pins axi_mem_intercon_1/M07_ACLK] [get_bd_pins axi_mem_intercon_1/S00_ACLK] [get_bd_pins myip_v2_0_0/m00_axi_aclk] [get_bd_pins processing_system7_0/FCLK_CLK0] [get_bd_pins processing_system7_0/M_AXI_GP0_ACLK] [get_bd_pins processing_system7_0/S_AXI_HP0_ACLK] [get_bd_pins rst_processing_system7_0_100M/slowest_sync_clk]
  connect_bd_net -net processing_system7_0_FCLK_CLK1 [get_bd_pins FIFO_PTM_TRACES/wr_clk] [get_bd_pins decodeur_traces/clk] [get_bd_pins processing_system7_0/FCLK_CLK1] [get_bd_pins processing_system7_0/TRACE_CLK]
  connect_bd_net -net processing_system7_0_FCLK_RESET0_N [get_bd_pins processing_system7_0/FCLK_RESET0_N] [get_bd_pins rst_processing_system7_0_100M/ext_reset_in]
  connect_bd_net -net processing_system7_0_TRACE_CTL [get_bd_pins processing_system7_0/TRACE_CTL] [get_bd_pins util_vector_logic_0/Op1]
  connect_bd_net -net processing_system7_0_TRACE_DATA [get_bd_pins decodeur_traces/trace_data] [get_bd_pins processing_system7_0/TRACE_DATA]
  connect_bd_net -net rst_processing_system7_0_100M_interconnect_aresetn [get_bd_pins axi_mem_intercon/ARESETN] [get_bd_pins axi_mem_intercon_1/ARESETN] [get_bd_pins rst_processing_system7_0_100M/interconnect_aresetn]
  connect_bd_net -net rst_processing_system7_0_100M_peripheral_aresetn [get_bd_pins AXI_BRAM_BBT_Annotations/s_axi_aresetn] [get_bd_pins AXI_BRAM_DEBUG/s_axi_aresetn] [get_bd_pins AXI_BRAM_Kernel_to_Monitor/s_axi_aresetn] [get_bd_pins AXI_BRAM_Monitor_to_Kernel/s_axi_aresetn] [get_bd_pins AXI_BRAM_Security_Policy_Config/s_axi_aresetn] [get_bd_pins AXI_FIFO_Instrumentation/s_axi_aresetn] [get_bd_pins PL_to_PS/s00_axi_aresetn] [get_bd_pins PL_to_PS/s_axi_intr_aresetn] [get_bd_pins axi_mem_intercon/M00_ARESETN] [get_bd_pins axi_mem_intercon/S00_ARESETN] [get_bd_pins axi_mem_intercon_1/M00_ARESETN] [get_bd_pins axi_mem_intercon_1/M01_ARESETN] [get_bd_pins axi_mem_intercon_1/M02_ARESETN] [get_bd_pins axi_mem_intercon_1/M03_ARESETN] [get_bd_pins axi_mem_intercon_1/M04_ARESETN] [get_bd_pins axi_mem_intercon_1/M05_ARESETN] [get_bd_pins axi_mem_intercon_1/M06_ARESETN] [get_bd_pins axi_mem_intercon_1/M07_ARESETN] [get_bd_pins axi_mem_intercon_1/S00_ARESETN] [get_bd_pins myip_v2_0_0/m00_axi_aresetn] [get_bd_pins rst_processing_system7_0_100M/peripheral_aresetn]
  connect_bd_net -net util_vector_logic_0_Res [get_bd_pins decodeur_traces/enable] [get_bd_pins util_vector_logic_0/Res]
  connect_bd_net -net xlconstant_0_dout [get_bd_pins Dispatcher_HardBlare/reset] [get_bd_pins FIFO_PTM_TRACES/wr_rst] [get_bd_pins decodeur_traces/reset] [get_bd_pins xlconstant_0/dout]

  # Create address segments
  create_bd_addr_seg -range 0x04000000 -offset 0x1C000000 [get_bd_addr_spaces myip_v2_0_0/m00_axi] [get_bd_addr_segs processing_system7_0/S_AXI_HP0/HP0_DDR_LOWOCM] SEG_processing_system7_0_HP0_DDR_LOWOCM
  create_bd_addr_seg -range 0x00020000 -offset 0x48000000 [get_bd_addr_spaces processing_system7_0/Data] [get_bd_addr_segs AXI_BRAM_BBT_Annotations/S_AXI/Mem0] SEG_AXI_BRAM_BBT_Annotations_Mem0
  create_bd_addr_seg -range 0x00010000 -offset 0x44000000 [get_bd_addr_spaces processing_system7_0/Data] [get_bd_addr_segs AXI_BRAM_DEBUG/S_AXI/Mem0] SEG_AXI_BRAM_DEBUG_Mem0
  create_bd_addr_seg -range 0x00001000 -offset 0x43C10000 [get_bd_addr_spaces processing_system7_0/Data] [get_bd_addr_segs AXI_FIFO_Instrumentation/S_AXI/Mem0] SEG_AXI_FIFO_Instrumentation_Mem0
  create_bd_addr_seg -range 0x00020000 -offset 0x41000000 [get_bd_addr_spaces processing_system7_0/Data] [get_bd_addr_segs PL_to_PS/s00_axi/reg0] SEG_PL_to_PS_reg0
  create_bd_addr_seg -range 0x00020000 -offset 0x40000000 [get_bd_addr_spaces processing_system7_0/Data] [get_bd_addr_segs PL_to_PS/s_axi_intr/reg0] SEG_PL_to_PS_reg01
  create_bd_addr_seg -range 0x00001000 -offset 0x43C00000 [get_bd_addr_spaces processing_system7_0/Data] [get_bd_addr_segs AXI_BRAM_Kernel_to_Monitor/S_AXI/Mem0] SEG_axi_bram_ctrl_0_Mem0
  create_bd_addr_seg -range 0x00001000 -offset 0x43C20000 [get_bd_addr_spaces processing_system7_0/Data] [get_bd_addr_segs AXI_BRAM_Monitor_to_Kernel/S_AXI/Mem0] SEG_axi_bram_ctrl_1_Mem0
  create_bd_addr_seg -range 0x00001000 -offset 0x42000000 [get_bd_addr_spaces processing_system7_0/Data] [get_bd_addr_segs AXI_BRAM_Security_Policy_Config/S_AXI/Mem0] SEG_axi_bram_ctrl_2_Mem0


  # Restore current instance
  current_bd_instance $oldCurInst

  save_bd_design
}
# End of create_root_design()


##################################################################
# MAIN FLOW
##################################################################

create_root_design ""


