library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

entity cache is
generic(n: integer := 32);
Port(clock, reset:in std_logic;

    address_in:in std_logic_vector(25 downto 0); --26 bit physical address from MMU
    data_out:out std_logic_vector(n-1 downto 0); -- Data to processor
    r_enable:in std_logic; -- Processor wants to read
    r_enable_out:out std_logic:= '0'; -- Read enable to memory
    r_valid_out:out std_logic:= '0'; -- Read data is available for processor
    mdata_valid: in std_logic;

    address_out:out std_logic_vector(25 downto 0); -- 26 bit address to memory
    mdata_in:in std_logic_vector(n-1 downto 0);  -- Data from memory
    mdata_out:out std_logic_vector(n-1 downto 0); -- Data to memory when processor wants to write
    w_enable_out:out std_logic:= '0'; -- Write enable to memory

--    write_enable_out : out std_logic;

    data_in:in std_logic_vector(n-1 downto 0); -- Data from processor for write through cache
    w_enable:in std_logic); --Processor wants to write to memory
    
end cache;

architecture Behavioral of cache is

type tag_way_type is array (0 to 3) of std_logic_vector(20 downto 0); -- 4 way set associative, Tag size is 21 bits
type tag_set_type is array (0 to 7) of tag_way_type; -- Total 8 sets
signal cache: tag_set_type;
signal index: integer range 0 to 7;
signal offset: integer range 0 to 3;

type valid_way_type is array (0 to 3) of std_logic; 
type valid_set_type is array (0 to 7) of valid_way_type; -- One valid bit for each cache line 4*8
signal valid: valid_set_type;

type cache_storage is array (0 to 127) of std_logic_vector(n-1 downto 0); -- 4 bytes in a cache line and 4*8 cache lines
signal cache_store: cache_storage;
signal storage_address: integer range 0 to 127;
signal read_storage_address: integer range 0 to 127;

type replacement_type is array (0 to 7) of integer range 0 to 3;
signal replacement: replacement_type := (others => 0);

type state_type is (START, ALMOST_DONE, DONE);
signal memory_read: state_type:= DONE;

type cache_state_type is (WRITE, IDLE);
signal cache_write: cache_state_type:= IDLE;

signal w_enable_from_cache: std_logic := '0';

signal cache_write_data: std_logic_vector(n-1 downto 0);

signal cache_store_address : integer range 0 to 127;
signal count: integer range 0 to 3 := 0;

--signal temp_address_out: std_logic_vector(25 downto 0);

begin

--write_enable_out <= '1' when (w_enable = '1') or (cache_write = WRITE) else
--		    '0';

index <= to_integer(unsigned(address_in(4 downto 2)));  -- Tag (25 to 5)-- Index (4 to 2) -- Offset (1 to 0)-- Total 26 bit address
offset <= to_integer(unsigned(address_in(1 downto 0)));
w_enable_out <= w_enable_from_cache;

address_out <= address_in(25 downto 2) & std_logic_vector(to_unsigned(count,2)) when (memory_read = START) or (memory_read = ALMOST_DONE) 		   else address_in when (w_enable = '1') or (cache_write = WRITE) else
		(others => '0');

read_storage_address <= index * 4 * 4 + 0 + offset when (cache(index)(0) = address_in (25 downto 5)) and (valid(index)(0) = '1') else 
                        index * 4 * 4 + 4*1 + offset when (cache(index)(1) = address_in (25 downto 5)) and (valid(index)(1) = '1') else
			index * 4 * 4 + 4*2 + offset when (cache(index)(2) = address_in (25 downto 5)) and (valid(index)(2) = '1') else
			index * 4 * 4 + 4*3 + offset when (cache(index)(3) = address_in (25 downto 5)) and (valid(index)(3) = '1') else
			0;

process (clock)
begin
if rising_edge(clock) then

if(reset = '1') then
	cache <= (others =>(others =>(others => '0')));
	valid <= (others =>(others => '0'));
	cache_store <= (others =>(others => '0'));

else

if(r_enable = '1') and (memory_read = DONE) then
	if (cache(index)(0) = address_in (25 downto 5)) and (valid(index)(0) = '1') then
		
		replacement(index) <= 0;                                          --Most recently used replacement policy
		data_out <= cache_store(read_storage_address);
   		r_valid_out <= '1';
	elsif (cache(index)(1) = address_in (25 downto 5)) and (valid(index)(1) = '1') then
		
		replacement(index) <= 1;
		data_out <= cache_store(read_storage_address);
		r_valid_out <= '1';
	elsif (cache(index)(2) = address_in (25 downto 5)) and (valid(index)(2) = '1') then
		
		replacement(index) <= 2;
		data_out <= cache_store(read_storage_address);
		r_valid_out <= '1';
	elsif (cache(index)(3) = address_in (25 downto 5)) and (valid(index)(3) = '1') then
		
		replacement(index) <= 3;
		data_out <= cache_store(read_storage_address);
		r_valid_out <= '1';
	else
		memory_read <= START;
		r_valid_out <= '0';
	end if;

end if;

if(w_enable = '1') or (cache_write = WRITE) then -- Check if the tag is already present in the cache set 

if(cache_write = IDLE) then
	if cache(index)(0) = address_in (25 downto 5) then
		storage_address <= 4 * 4 * index + 0 + offset;
		valid(index)(0) <= '1';
--		cache(index)(0) <= address_in (25 downto 5);
	elsif(cache(index)(1) = address_in (25 downto 5)) then
		storage_address <= 4 * 4 * index + 4*1 + offset;
		valid(index)(1) <= '1';
--		cache(index)(1) <= address_in (25 downto 5);
	elsif(cache(index)(2) = address_in (25 downto 5)) then
		storage_address <= 4 * 4 * index + 4*2 + offset;
		valid(index)(2) <= '1';
--		cache(index)(2) <= address_in (25 downto 5);
	elsif(cache(index)(3) = address_in (25 downto 5)) then
		storage_address <= 4 * 4 * index + 4*3 + offset;
		valid(index)(3) <= '1';
--		cache(index)(3) <= address_in (25 downto 5);
	else -- or check if any entry is empty
		if(valid(index)(0) = '0') then
			storage_address <= 4 * 4 * index + 0 + offset;
			valid(index)(0) <= '1';
			cache(index)(0) <= address_in (25 downto 5);
		elsif((valid(index)(0) = '1') and (valid(index)(1) = '0')) then
			storage_address <= 4 * 4 * index + 4*1 + offset;
			valid(index)(1) <= '1';
			cache(index)(1) <= address_in (25 downto 5);
		elsif((valid(index)(0) = '1') and (valid(index)(1) = '1') and (valid(index)(2) = '0')) then
			storage_address <= 4 * 4 * index + 4*2 + offset;
			valid(index)(2) <= '1';
			cache(index)(2) <= address_in (25 downto 5);
		elsif((valid(index)(0) = '1') and (valid(index)(1) = '1') and (valid(index)(2) = '1') and (valid(index)(3) = '0')) then
			storage_address <= 4 * 4 * index + 4*3 + offset;
			valid(index)(3) <= '1';
			cache(index)(3) <= address_in (25 downto 5);
		else -- Or replace one entry using MRU policy
			storage_address <= 4 * 4 * index + 4 * replacement(index) + offset;
			valid(index)(replacement(index)) <= '1';
			cache(index)(replacement(index)) <= address_in (25 downto 5);
		end if;	
--		cache_write <= WRITE;
--		storage_address <= 4 * 4 * address_in(4 downto 2) + 4 * replacement(index) + address_in(1 downto 0);
--		valid(index)(replacement(index)) <= '1';
--		cache(index)(replacement(index)) <= address_in (25 downto 5);
	end if;
cache_write <= WRITE;
cache_write_data <= data_in;
w_enable_from_cache <= w_enable;
end if;

if(cache_write = WRITE) then
	cache_store(storage_address) <= cache_write_data;
	cache_write <= IDLE;
	w_enable_from_cache <= '0';
end if;

--address_out <= address_in;
mdata_out <= data_in;

end if;

if(memory_read = START) or (memory_read = ALMOST_DONE) then
	
		
	r_enable_out <= '1';
	
	if(mdata_valid = '1') then
		cache_store_address <= 4 * 4 * index + 4 * replacement(index) + count;
		cache_store(cache_store_address) <= mdata_in;
		count <= count + 1;
		if(count = 3) then
			memory_read <= ALMOST_DONE;
			count <= 0;
		end if;

		if(memory_read = ALMOST_DONE) then
			valid(index)(replacement(index)) <= '1';
			cache(index)(replacement(index)) <= address_in (25 downto 5);
			r_enable_out <= '0';
			memory_read <= DONE;
		end if;
	end if;
end if;

end if;

end if;

end process;

end Behavioral;
