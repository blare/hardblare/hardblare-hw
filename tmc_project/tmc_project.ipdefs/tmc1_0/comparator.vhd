library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity comparator is
generic ( n: integer := 32;
	  m: integer := 26;
	  OFFSET: integer := 12);
port(
    IN1:in std_logic_vector(31-OFFSET downto 0);
    IN2:in std_logic_vector(31-OFFSET downto 0);
    r:in std_logic;
    equal:out std_logic);
end comparator;

architecture Behavioral of comparator is
begin
--    process(IN1, IN2)
--    begin
        
--        if  then  

	equal <= '1' when (IN1 = IN2) and r='1' else
            	 '0';
--        end if;
--    end process;  
end Behavioral;


