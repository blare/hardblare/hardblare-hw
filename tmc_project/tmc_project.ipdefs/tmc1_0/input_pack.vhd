library ieee; 
use ieee.std_logic_1164.all; 
  
package byte_pkg is 
    constant width : integer := 13; -- Frame number width =(m-OFFSET-1), m = 26, OFFSET = 12
    subtype byte is std_logic_vector(width downto 0); 
    type byte_array_t is array(natural range <>) of byte; 

    
end; 


