---------------------------------------------------------------------
-- TITLE: Bus Multiplexer / Signal Router
-- AUTHOR: MAW
-- DATE CREATED: 25/10/2017
-- FILENAME: dep_bus_mux.vhd
-- PROJECT: DIFT Coprocessor
-- DESCRIPTION:
--    This entity is the main signal router.  
--    It multiplexes signals from multiple sources to the correct location.
--    The outputs are as follows:
--       a_bus        : goes to the ALU
--       b_bus        : goes to the ALU
--       reg_dest_out : goes to the tag register bank
---------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.dependencies_pack.all;

entity dep_bus_mux is
   port(
        reset                 : in  std_logic;
        imm_in                : in  std_logic_vector(15 downto 0);
        en_rf                 : in  std_logic;
        en_rf_fp              : in  std_logic;
        en_rf_2               : in  std_logic;
        en_rf_2_fp            : in  std_logic;
        reg_source_gp         : in  std_logic_vector(31 downto 0);
        reg_source_fp         : in  std_logic_vector(31 downto 0);
        reg_source            : out std_logic_vector(31 downto 0);

        a_mux                 : in  reg_src1_source_type;
        a_out                 : out std_logic_vector(31 downto 0);

        reg_source_gp_2       : in  std_logic_vector(31 downto 0);
        reg_source_fp_2       : in  std_logic_vector(31 downto 0);
        reg_source_2          : out std_logic_vector(31 downto 0);
        b_mux                 : in  reg_src2_source_type;
        b_out                 : out std_logic_vector(31 downto 0);

        tag_prop_sources      : in tag_prop_type;
        tag_check_sources     : in tag_check_type;

        enable_sec_policies   : in  std_logic_vector(nb_sec_policy-1 downto 0);
        tag_alu_function_src  : in  tag_alu_function_source_type;
        tag_alu_function_op   : in  tag_alu_function_type;
        tag_alu_function_sec  : in  tag_alu_function_type;
        tag_alu_function_o    : out tag_alu_function_type;

        c_bus                 : in  std_logic_vector(31 downto 0);
        c_memory              : in  std_logic_vector(31 downto 0);
        c_tag                 : in  std_logic_vector(31 downto 0);
        --c_instrumentation     : in  std_logic_vector(31 downto 0);
        --c_kblare              : in  std_logic_vector(31 downto 0);
        c_mux                 : in  reg_dst_source_type;
        reg_dest_out          : out std_logic_vector(31 downto 0) );
end; --entity dep_bus_mux

architecture logic of dep_bus_mux is
  signal enable_reg_file : std_logic_vector(1 downto 0);
  signal enable_reg_file_2: std_logic_vector(1 downto 0);
  --signal reg_source_s    : std_logic_vector(31 downto 0);
  --signal reg_source_2_s  : std_logic_vector(31 downto 0);
begin

  enable_reg_file <= en_rf & en_rf_fp;
  enable_reg_file_2 <= en_rf_2 & en_rf_2_fp;
  --reg_source_and_source_2_process: process(enable_reg_file, reg_source_gp, reg_source_gp_2, reg_source_fp, reg_source_fp_2)
  --begin
  --  case enable_reg_file is 
  --  when "01" => 
  --    reg_source_s <= reg_source_fp;
  --    reg_source_2_s <= reg_source_fp_2;
  --  when "10" | "00" => 
  --    reg_source_s <= reg_source_gp; 
  --    reg_source_2_s <= reg_source_gp_2; 
  --  when "11" => -- Can happen 
  --    reg_source_s <= reg_source_fp; 
  --    reg_source_2_s <= reg_source_fp_2; 
  --  when others => 
  --    reg_source_s <= reg_source_gp; 
  --    reg_source_2_s <= reg_source_gp_2; 
  --  end case;
  --end process;

bmux_bmux: process(reg_source_gp, imm_in, c_bus, c_memory, reg_source_fp, reg_source_fp_2,
                   enable_sec_policies, tag_alu_function_sec, tag_prop_sources, 
                   tag_alu_function_op, enable_reg_file, reg_source_gp_2,
                   c_mux, imm_in, a_mux, b_mux, tag_alu_function_src) 
variable bb, sum     : std_logic_vector(32 downto 0);
variable a_o, b_o    : std_logic_vector(31 downto 0);
variable temp        : std_logic_vector(31 downto 0);
variable en_i        : integer;
variable reg_source_s: std_logic_vector(31 downto 0);
variable reg_source_2_s: std_logic_vector(31 downto 0);
--variable source_prop_en : std_logic := '0';
--variable source_address_prop_en : std_logic := '0';
--variable dest_address_prop_en : std_logic := '0';
begin

    case enable_reg_file is
      when "01" => 
        reg_source_s := reg_source_fp;
      when "10" | "00" => 
        reg_source_s := reg_source_gp;
      when "11" => -- Can happen 
        reg_source_s := reg_source_fp;
      when others => 
        reg_source_s := reg_source_gp;
    end case;

    case enable_reg_file_2 is
      when "01" => 
        reg_source_2_s := reg_source_fp_2;
      when "10" | "00" =>
        reg_source_2_s := reg_source_gp_2;
      when "11" => -- Can happen 
        reg_source_2_s := reg_source_fp_2;
      when others => 
        reg_source_2_s := reg_source_gp_2;
    end case;

    en_i := to_integer(unsigned(enable_sec_policies));
    case tag_alu_function_src is 
    when tag_alu_function_from_sec_pol => 
      case en_i is 
        --case? enable_sec_policies is 
        -- if case? don't work, try this. 
        --when X"1" to X"F" =>[link](https://forums.xilinx.com/t5/Synthesis/don-t-care-synthesis/td-p/43694)
        --when "---1"=> 
      when 1 to 15 => 
    -- works with integers so try this: To_integer(unsigned(enable_sec_policies));
        tag_alu_function_o <= tag_alu_function_sec;
      when others =>
        tag_alu_function_o <= tag_alu_nothing;
      end case; 
    --end case?;
    when tag_alu_function_from_opcode => 
      tag_alu_function_o <= tag_alu_function_op;
    when others => 
      tag_alu_function_o <= tag_alu_nothing;
    end case;


    case a_mux is
      when reg_src1_from_reg =>
        --case enable_reg_file is
        --  when "11" => -- @TODOs
        --    a_o := reg_source_gp;
        --  when others => 
        --    a_o := reg_source_s;
        --end case;
        a_o := reg_source_s;
      --when reg_src1_from_mem =>
      --  a_o := c_memory;
      --when reg_src1_from_instr =>        
      --  a_o := c_instrumentation;     
      when others => --
        a_o := (others => '0');
    end case;

    case b_mux is
      when reg_src2_from_reg =>
        --case enable_reg_file is
        --  when "11" => 
        --    b_o := reg_source_gp_2;
        --  when others => 
        --    b_o := reg_source_2_s;
        --end case;
        b_o := reg_source_2_s;
      when reg_src2_from_imm =>
        if imm_in(15) = '0' then
           b_o(31 downto 16) := ZERO(31 downto 16);
        else
           b_o(31 downto 16) := "1111111111111111";
        end if;
        b_o(15 downto 0) := imm_in;
      when reg_src2_from_unsigned_imm =>
        b_o(31 downto 16) := ZERO(31 downto 16);
        b_o(15 downto 0) := imm_in;
      when reg_src2_from_upper_imm =>        
        b_o := imm_in & (15 downto 0 => '0');
      when others =>             --b_from_immX4
        b_o := (others => '0');
    end case;    

  case tag_prop_sources is 
    -- MOV operation
    when tag_source_prop_none =>
      a_out <= ZERO;
      b_out <= ZERO;
    when tag_source_prop_en => -- load operation
      a_out <= a_o;
      b_out <= b_o;
      tag_alu_function_o <= tag_alu_copy_src1; -- don't add the immediate
    when tag_source_address_prop_en => -- load operation
      a_out <= a_o;
      b_out <= b_o;
    when tag_dest_address_prop_en => -- store operation
      a_out <= a_o;
      b_out <= b_o;
    -- All other operations
    when others => 
      a_out <= a_o;
      b_out <= b_o;
  end case;
    --a_out <= a_o;
    --b_out <= b_o;
    reg_source <= reg_source_s;
    reg_source_2 <= reg_source_2_s;
end process;

-- The input signals come from writeback stage 
cmux: process(c_bus, c_memory, c_tag, c_mux)
variable write_en_rf_s : std_logic;
begin
   --write_en_rf_s := '0';
   case c_mux is
   when reg_dst_not_used => 
      reg_dest_out <= ZERO;
   when reg_dst_from_alu => 
      reg_dest_out <= c_bus;
   when reg_dst_from_mem =>
      reg_dest_out <= c_tag;
   when reg_dst_from_data_mem =>
      reg_dest_out <= c_memory;
   --when reg_dst_from_instr =>
   --   write_en_rf_s := '1';
   --   reg_dest_out <= c_instrumentation;  
   --when reg_dst_from_kblare =>
   --   write_en_rf_s := '1';
   --   reg_dest_out <= c_kblare;
   when others =>
      reg_dest_out <= c_bus;
   end case;

end process;

end; --architecture logic
