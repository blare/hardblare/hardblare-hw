// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
// Date        : Sun Jun  7 12:02:51 2020
// Host        : HardBlare-Workstation running 64-bit Ubuntu 16.04.6 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_1_BRAM_to_FIFO_0_0 -prefix
//               design_1_BRAM_to_FIFO_0_0_ design_1_BRAM_to_FIFO_0_2_sim_netlist.v
// Design      : design_1_BRAM_to_FIFO_0_2
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module design_1_BRAM_to_FIFO_0_0_BRAM_to_FIFO
   (to_fifo_write_enable,
    to_fifo_read_enable,
    from_bram_bytes_selection,
    from_bram_enable);
  output to_fifo_write_enable;
  output to_fifo_read_enable;
  input [3:0]from_bram_bytes_selection;
  input from_bram_enable;

  wire [3:0]from_bram_bytes_selection;
  wire from_bram_enable;
  wire to_fifo_read_enable;
  wire to_fifo_write_enable;

  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h00010000)) 
    to_fifo_read_enable0
       (.I0(from_bram_bytes_selection[1]),
        .I1(from_bram_bytes_selection[0]),
        .I2(from_bram_bytes_selection[2]),
        .I3(from_bram_bytes_selection[3]),
        .I4(from_bram_enable),
        .O(to_fifo_read_enable));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    to_fifo_write_enable0
       (.I0(from_bram_bytes_selection[1]),
        .I1(from_bram_bytes_selection[0]),
        .I2(from_bram_bytes_selection[2]),
        .I3(from_bram_bytes_selection[3]),
        .I4(from_bram_enable),
        .O(to_fifo_write_enable));
endmodule

(* CHECK_LICENSE_TYPE = "design_1_BRAM_to_FIFO_0_2,BRAM_to_FIFO,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* ip_definition_source = "package_project" *) 
(* x_core_info = "BRAM_to_FIFO,Vivado 2018.2" *) 
(* NotValidForBitStream *)
module design_1_BRAM_to_FIFO_0_0
   (from_bram_address,
    from_bram_clk,
    from_bram_data_write,
    from_bram_enable,
    from_bram_reset,
    from_bram_bytes_selection,
    to_bram_data_read,
    to_fifo_data_write,
    to_fifo_write_enable,
    to_fifo_read_enable,
    to_fifo_clk,
    to_fifo_reset,
    from_fifo_data_read);
  input [31:0]from_bram_address;
  input from_bram_clk;
  input [31:0]from_bram_data_write;
  input from_bram_enable;
  input from_bram_reset;
  input [3:0]from_bram_bytes_selection;
  output [31:0]to_bram_data_read;
  output [31:0]to_fifo_data_write;
  output to_fifo_write_enable;
  output to_fifo_read_enable;
  output to_fifo_clk;
  output to_fifo_reset;
  input [31:0]from_fifo_data_read;

  wire [3:0]from_bram_bytes_selection;
  wire from_bram_clk;
  wire [31:0]from_bram_data_write;
  wire from_bram_enable;
  wire from_bram_reset;
  wire [31:0]from_fifo_data_read;
  wire to_fifo_read_enable;
  wire to_fifo_write_enable;

  assign to_bram_data_read[31:0] = from_fifo_data_read;
  assign to_fifo_clk = from_bram_clk;
  assign to_fifo_data_write[31:0] = from_bram_data_write;
  assign to_fifo_reset = from_bram_reset;
  design_1_BRAM_to_FIFO_0_0_BRAM_to_FIFO U0
       (.from_bram_bytes_selection(from_bram_bytes_selection),
        .from_bram_enable(from_bram_enable),
        .to_fifo_read_enable(to_fifo_read_enable),
        .to_fifo_write_enable(to_fifo_write_enable));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
