// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
// Date        : Sat Apr 18 03:53:32 2020
// Host        : HardBlare-Workstation running 64-bit Ubuntu 16.04.6 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_1_myip_v2_0_0_0 -prefix
//               design_1_myip_v2_0_0_0_ design_1_myip_v2_0_0_0_sim_netlist.v
// Design      : design_1_myip_v2_0_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_myip_v2_0_0_0,myip_v2_0,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "myip_v2_0,Vivado 2018.2" *) 
(* NotValidForBitStream *)
module design_1_myip_v2_0_0_0
   (address_in,
    data_in,
    data_out,
    read_en,
    write_en,
    m00_axi_init_axi_txn,
    m00_axi_error,
    m00_axi_txn_done,
    m00_axi_aclk,
    m00_axi_aresetn,
    m00_axi_awaddr,
    m00_axi_awprot,
    m00_axi_awvalid,
    m00_axi_awready,
    m00_axi_wdata,
    m00_axi_wstrb,
    m00_axi_wvalid,
    m00_axi_wready,
    m00_axi_bresp,
    m00_axi_bvalid,
    m00_axi_bready,
    m00_axi_araddr,
    m00_axi_arprot,
    m00_axi_arvalid,
    m00_axi_arready,
    m00_axi_rdata,
    m00_axi_rresp,
    m00_axi_rvalid,
    m00_axi_rready);
  input [31:0]address_in;
  input [31:0]data_in;
  output [31:0]data_out;
  input read_en;
  input write_en;
  input m00_axi_init_axi_txn;
  output m00_axi_error;
  output m00_axi_txn_done;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 m00_axi_signal_clock CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME m00_axi_signal_clock, ASSOCIATED_BUSIF m00_axi, ASSOCIATED_RESET m00_axi_aresetn, FREQ_HZ 1.02564e+08, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0" *) input m00_axi_aclk;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 m00_axi_signal_reset RST" *) (* x_interface_parameter = "XIL_INTERFACENAME m00_axi_signal_reset, POLARITY ACTIVE_LOW" *) input m00_axi_aresetn;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m00_axi AWADDR" *) (* x_interface_parameter = "XIL_INTERFACENAME m00_axi, ADDR_WIDTH 32, ARUSER_WIDTH 0, AWUSER_WIDTH 0, BUSER_WIDTH 0, DATA_WIDTH 32, HAS_BRESP 1, HAS_BURST 0, HAS_CACHE 0, HAS_LOCK 0, HAS_PROT 1, HAS_QOS 0, HAS_REGION 0, HAS_RRESP 1, HAS_WSTRB 1, ID_WIDTH 0, PROTOCOL AXI4LITE, READ_WRITE_MODE READ_WRITE, RUSER_WIDTH 0, WUSER_WIDTH 0, NUM_WRITE_OUTSTANDING 8, NUM_READ_OUTSTANDING 8, SUPPORTS_NARROW_BURST 0, MAX_BURST_LENGTH 1, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, FREQ_HZ 1.02564e+08, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0" *) output [31:0]m00_axi_awaddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m00_axi AWPROT" *) output [2:0]m00_axi_awprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m00_axi AWVALID" *) output m00_axi_awvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m00_axi AWREADY" *) input m00_axi_awready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m00_axi WDATA" *) output [31:0]m00_axi_wdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m00_axi WSTRB" *) output [3:0]m00_axi_wstrb;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m00_axi WVALID" *) output m00_axi_wvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m00_axi WREADY" *) input m00_axi_wready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m00_axi BRESP" *) input [1:0]m00_axi_bresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m00_axi BVALID" *) input m00_axi_bvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m00_axi BREADY" *) output m00_axi_bready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m00_axi ARADDR" *) output [31:0]m00_axi_araddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m00_axi ARPROT" *) output [2:0]m00_axi_arprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m00_axi ARVALID" *) output m00_axi_arvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m00_axi ARREADY" *) input m00_axi_arready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m00_axi RDATA" *) input [31:0]m00_axi_rdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m00_axi RRESP" *) input [1:0]m00_axi_rresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m00_axi RVALID" *) input m00_axi_rvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 m00_axi RREADY" *) output m00_axi_rready;

  wire \<const0> ;
  wire \<const1> ;
  wire [31:0]address_in;
  wire [31:0]data_in;
  wire m00_axi_aclk;
  wire [31:0]m00_axi_araddr;
  wire m00_axi_aresetn;
  wire m00_axi_arready;
  wire m00_axi_arvalid;
  wire [31:0]m00_axi_awaddr;
  wire m00_axi_awready;
  wire m00_axi_awvalid;
  wire m00_axi_bready;
  wire [1:0]m00_axi_bresp;
  wire m00_axi_bvalid;
  wire m00_axi_error;
  wire m00_axi_init_axi_txn;
  wire [31:0]m00_axi_rdata;
  wire m00_axi_rready;
  wire [1:0]m00_axi_rresp;
  wire m00_axi_rvalid;
  wire m00_axi_txn_done;
  wire [31:0]m00_axi_wdata;
  wire m00_axi_wready;
  wire m00_axi_wvalid;
  wire read_en;
  wire write_en;

  assign data_out[31:0] = m00_axi_rdata;
  assign m00_axi_arprot[2] = \<const0> ;
  assign m00_axi_arprot[1] = \<const0> ;
  assign m00_axi_arprot[0] = \<const1> ;
  assign m00_axi_awprot[2] = \<const0> ;
  assign m00_axi_awprot[1] = \<const0> ;
  assign m00_axi_awprot[0] = \<const0> ;
  assign m00_axi_wstrb[3] = \<const1> ;
  assign m00_axi_wstrb[2] = \<const1> ;
  assign m00_axi_wstrb[1] = \<const1> ;
  assign m00_axi_wstrb[0] = \<const1> ;
  GND GND
       (.G(\<const0> ));
  design_1_myip_v2_0_0_0_myip_v2_0 U0
       (.address_in(address_in),
        .data_in(data_in),
        .m00_axi_aclk(m00_axi_aclk),
        .m00_axi_araddr(m00_axi_araddr),
        .m00_axi_aresetn(m00_axi_aresetn),
        .m00_axi_arready(m00_axi_arready),
        .m00_axi_arvalid(m00_axi_arvalid),
        .m00_axi_awaddr(m00_axi_awaddr),
        .m00_axi_awready(m00_axi_awready),
        .m00_axi_awvalid(m00_axi_awvalid),
        .m00_axi_bready(m00_axi_bready),
        .m00_axi_bresp(m00_axi_bresp[1]),
        .m00_axi_bvalid(m00_axi_bvalid),
        .m00_axi_error(m00_axi_error),
        .m00_axi_init_axi_txn(m00_axi_init_axi_txn),
        .m00_axi_rready(m00_axi_rready),
        .m00_axi_rresp(m00_axi_rresp[1]),
        .m00_axi_rvalid(m00_axi_rvalid),
        .m00_axi_txn_done(m00_axi_txn_done),
        .m00_axi_wdata(m00_axi_wdata),
        .m00_axi_wready(m00_axi_wready),
        .m00_axi_wvalid(m00_axi_wvalid),
        .read_en(read_en),
        .write_en(write_en));
  VCC VCC
       (.P(\<const1> ));
endmodule

module design_1_myip_v2_0_0_0_myip_v2_0
   (m00_axi_bready,
    m00_axi_awaddr,
    m00_axi_wdata,
    m00_axi_araddr,
    m00_axi_arvalid,
    m00_axi_awvalid,
    m00_axi_rready,
    m00_axi_wvalid,
    m00_axi_txn_done,
    m00_axi_error,
    m00_axi_aresetn,
    m00_axi_aclk,
    m00_axi_init_axi_txn,
    m00_axi_bvalid,
    address_in,
    data_in,
    m00_axi_arready,
    m00_axi_awready,
    m00_axi_rvalid,
    m00_axi_wready,
    write_en,
    read_en,
    m00_axi_bresp,
    m00_axi_rresp);
  output m00_axi_bready;
  output [31:0]m00_axi_awaddr;
  output [31:0]m00_axi_wdata;
  output [31:0]m00_axi_araddr;
  output m00_axi_arvalid;
  output m00_axi_awvalid;
  output m00_axi_rready;
  output m00_axi_wvalid;
  output m00_axi_txn_done;
  output m00_axi_error;
  input m00_axi_aresetn;
  input m00_axi_aclk;
  input m00_axi_init_axi_txn;
  input m00_axi_bvalid;
  input [31:0]address_in;
  input [31:0]data_in;
  input m00_axi_arready;
  input m00_axi_awready;
  input m00_axi_rvalid;
  input m00_axi_wready;
  input write_en;
  input read_en;
  input [0:0]m00_axi_bresp;
  input [0:0]m00_axi_rresp;

  wire [31:0]address_in;
  wire [31:0]data_in;
  wire m00_axi_aclk;
  wire [31:0]m00_axi_araddr;
  wire m00_axi_aresetn;
  wire m00_axi_arready;
  wire m00_axi_arvalid;
  wire [31:0]m00_axi_awaddr;
  wire m00_axi_awready;
  wire m00_axi_awvalid;
  wire m00_axi_bready;
  wire [0:0]m00_axi_bresp;
  wire m00_axi_bvalid;
  wire m00_axi_error;
  wire m00_axi_init_axi_txn;
  wire m00_axi_rready;
  wire [0:0]m00_axi_rresp;
  wire m00_axi_rvalid;
  wire m00_axi_txn_done;
  wire [31:0]m00_axi_wdata;
  wire m00_axi_wready;
  wire m00_axi_wvalid;
  wire read_en;
  wire write_en;

  design_1_myip_v2_0_0_0_myip_v2_0_M00_AXI myip_v2_0_M00_AXI_inst
       (.address_in(address_in),
        .data_in(data_in),
        .m00_axi_aclk(m00_axi_aclk),
        .m00_axi_araddr(m00_axi_araddr),
        .m00_axi_aresetn(m00_axi_aresetn),
        .m00_axi_arready(m00_axi_arready),
        .m00_axi_arvalid(m00_axi_arvalid),
        .m00_axi_awaddr(m00_axi_awaddr),
        .m00_axi_awready(m00_axi_awready),
        .m00_axi_awvalid(m00_axi_awvalid),
        .m00_axi_bready(m00_axi_bready),
        .m00_axi_bresp(m00_axi_bresp),
        .m00_axi_bvalid(m00_axi_bvalid),
        .m00_axi_error(m00_axi_error),
        .m00_axi_init_axi_txn(m00_axi_init_axi_txn),
        .m00_axi_rready(m00_axi_rready),
        .m00_axi_rresp(m00_axi_rresp),
        .m00_axi_rvalid(m00_axi_rvalid),
        .m00_axi_txn_done(m00_axi_txn_done),
        .m00_axi_wdata(m00_axi_wdata),
        .m00_axi_wready(m00_axi_wready),
        .m00_axi_wvalid(m00_axi_wvalid),
        .read_en(read_en),
        .write_en(write_en));
endmodule

module design_1_myip_v2_0_0_0_myip_v2_0_M00_AXI
   (m00_axi_bready,
    m00_axi_awaddr,
    m00_axi_wdata,
    m00_axi_araddr,
    m00_axi_arvalid,
    m00_axi_awvalid,
    m00_axi_rready,
    m00_axi_wvalid,
    m00_axi_txn_done,
    m00_axi_error,
    m00_axi_aresetn,
    m00_axi_aclk,
    m00_axi_init_axi_txn,
    m00_axi_bvalid,
    address_in,
    data_in,
    m00_axi_arready,
    m00_axi_awready,
    m00_axi_rvalid,
    m00_axi_wready,
    write_en,
    read_en,
    m00_axi_bresp,
    m00_axi_rresp);
  output m00_axi_bready;
  output [31:0]m00_axi_awaddr;
  output [31:0]m00_axi_wdata;
  output [31:0]m00_axi_araddr;
  output m00_axi_arvalid;
  output m00_axi_awvalid;
  output m00_axi_rready;
  output m00_axi_wvalid;
  output m00_axi_txn_done;
  output m00_axi_error;
  input m00_axi_aresetn;
  input m00_axi_aclk;
  input m00_axi_init_axi_txn;
  input m00_axi_bvalid;
  input [31:0]address_in;
  input [31:0]data_in;
  input m00_axi_arready;
  input m00_axi_awready;
  input m00_axi_rvalid;
  input m00_axi_wready;
  input write_en;
  input read_en;
  input [0:0]m00_axi_bresp;
  input [0:0]m00_axi_rresp;

  wire ERROR_i_1_n_0;
  wire ERROR_i_2_n_0;
  wire ERROR_i_3_n_0;
  wire \FSM_sequential_mst_exec_state[0]_i_1_n_0 ;
  wire \FSM_sequential_mst_exec_state[0]_i_2_n_0 ;
  wire \FSM_sequential_mst_exec_state[1]_i_1_n_0 ;
  wire \FSM_sequential_mst_exec_state[1]_i_2_n_0 ;
  wire \FSM_sequential_mst_exec_state[1]_i_3_n_0 ;
  wire [31:0]address_in;
  wire [31:0]address_in_reg;
  wire address_in_reg0;
  wire \axi_araddr[31]_i_1_n_0 ;
  wire axi_arvalid_i_1_n_0;
  wire \axi_awaddr[31]_i_1_n_0 ;
  wire axi_awvalid_i_1_n_0;
  wire axi_awvalid_i_2_n_0;
  wire axi_bready_i_1_n_0;
  wire axi_rready_i_1_n_0;
  wire \axi_wdata[31]_i_1_n_0 ;
  wire axi_wvalid_i_1_n_0;
  wire compare_done_i_1_n_0;
  wire [31:0]data_in;
  wire [31:0]data_in_reg;
  wire error_reg;
  wire error_reg0;
  wire error_reg_i_1_n_0;
  wire init_txn_ff;
  wire init_txn_ff2;
  wire init_txn_pulse__0;
  wire last_read;
  wire last_read_i_1_n_0;
  wire last_write;
  wire last_write_i_1_n_0;
  wire m00_axi_aclk;
  wire [31:0]m00_axi_araddr;
  wire m00_axi_aresetn;
  wire m00_axi_arready;
  wire m00_axi_arvalid;
  wire [31:0]m00_axi_awaddr;
  wire m00_axi_awready;
  wire m00_axi_awvalid;
  wire m00_axi_bready;
  wire [0:0]m00_axi_bresp;
  wire m00_axi_bvalid;
  wire m00_axi_error;
  wire m00_axi_init_axi_txn;
  wire m00_axi_rready;
  wire [0:0]m00_axi_rresp;
  wire m00_axi_rvalid;
  wire m00_axi_txn_done;
  wire [31:0]m00_axi_wdata;
  wire m00_axi_wready;
  wire m00_axi_wvalid;
  (* RTL_KEEP = "yes" *) wire [1:0]mst_exec_state;
  wire read_en;
  wire [1:0]read_index;
  wire \read_index[0]_i_1_n_0 ;
  wire \read_index[1]_i_1_n_0 ;
  wire read_issued_i_1_n_0;
  wire read_issued_reg_n_0;
  wire reads_done;
  wire reads_done_i_1_n_0;
  wire start_single_read0;
  wire start_single_read_i_1_n_0;
  wire start_single_read_reg_n_0;
  wire start_single_write0;
  wire start_single_write_i_1_n_0;
  wire start_single_write_reg_n_0;
  wire write_en;
  wire [1:0]write_index;
  wire \write_index[0]_i_1_n_0 ;
  wire \write_index[1]_i_1_n_0 ;
  wire write_issued_i_1_n_0;
  wire write_issued_reg_n_0;
  wire writes_done;
  wire writes_done_i_1_n_0;

  LUT1 #(
    .INIT(2'h1)) 
    ERROR_i_1
       (.I0(m00_axi_aresetn),
        .O(ERROR_i_1_n_0));
  LUT5 #(
    .INIT(32'h28FF2800)) 
    ERROR_i_2
       (.I0(error_reg),
        .I1(mst_exec_state[1]),
        .I2(mst_exec_state[0]),
        .I3(ERROR_i_3_n_0),
        .I4(m00_axi_error),
        .O(ERROR_i_2_n_0));
  LUT6 #(
    .INIT(64'h00F0FF2200F00022)) 
    ERROR_i_3
       (.I0(init_txn_ff),
        .I1(init_txn_ff2),
        .I2(reads_done),
        .I3(mst_exec_state[0]),
        .I4(mst_exec_state[1]),
        .I5(writes_done),
        .O(ERROR_i_3_n_0));
  FDRE ERROR_reg
       (.C(m00_axi_aclk),
        .CE(1'b1),
        .D(ERROR_i_2_n_0),
        .Q(m00_axi_error),
        .R(ERROR_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \FSM_sequential_mst_exec_state[0]_i_1 
       (.I0(\FSM_sequential_mst_exec_state[0]_i_2_n_0 ),
        .I1(\FSM_sequential_mst_exec_state[1]_i_3_n_0 ),
        .I2(mst_exec_state[0]),
        .O(\FSM_sequential_mst_exec_state[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h000000000008FF08)) 
    \FSM_sequential_mst_exec_state[0]_i_2 
       (.I0(write_en),
        .I1(init_txn_ff),
        .I2(init_txn_ff2),
        .I3(mst_exec_state[0]),
        .I4(writes_done),
        .I5(mst_exec_state[1]),
        .O(\FSM_sequential_mst_exec_state[0]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \FSM_sequential_mst_exec_state[1]_i_1 
       (.I0(\FSM_sequential_mst_exec_state[1]_i_2_n_0 ),
        .I1(\FSM_sequential_mst_exec_state[1]_i_3_n_0 ),
        .I2(mst_exec_state[1]),
        .O(\FSM_sequential_mst_exec_state[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h000000000004FF04)) 
    \FSM_sequential_mst_exec_state[1]_i_2 
       (.I0(write_en),
        .I1(init_txn_ff),
        .I2(init_txn_ff2),
        .I3(mst_exec_state[1]),
        .I4(reads_done),
        .I5(mst_exec_state[0]),
        .O(\FSM_sequential_mst_exec_state[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000FFFFFFFFFEFF)) 
    \FSM_sequential_mst_exec_state[1]_i_3 
       (.I0(read_en),
        .I1(write_en),
        .I2(init_txn_ff2),
        .I3(init_txn_ff),
        .I4(mst_exec_state[1]),
        .I5(mst_exec_state[0]),
        .O(\FSM_sequential_mst_exec_state[1]_i_3_n_0 ));
  (* FSM_ENCODED_STATES = "init_write:01,init_read:10,idle:00" *) 
  (* KEEP = "yes" *) 
  FDRE \FSM_sequential_mst_exec_state_reg[0] 
       (.C(m00_axi_aclk),
        .CE(1'b1),
        .D(\FSM_sequential_mst_exec_state[0]_i_1_n_0 ),
        .Q(mst_exec_state[0]),
        .R(ERROR_i_1_n_0));
  (* FSM_ENCODED_STATES = "init_write:01,init_read:10,idle:00" *) 
  (* KEEP = "yes" *) 
  FDRE \FSM_sequential_mst_exec_state_reg[1] 
       (.C(m00_axi_aclk),
        .CE(1'b1),
        .D(\FSM_sequential_mst_exec_state[1]_i_1_n_0 ),
        .Q(mst_exec_state[1]),
        .R(ERROR_i_1_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    \address_in_reg[31]_i_1 
       (.I0(read_en),
        .I1(write_en),
        .O(address_in_reg0));
  FDRE \address_in_reg_reg[0] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(address_in[0]),
        .Q(address_in_reg[0]),
        .R(ERROR_i_1_n_0));
  FDRE \address_in_reg_reg[10] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(address_in[10]),
        .Q(address_in_reg[10]),
        .R(ERROR_i_1_n_0));
  FDRE \address_in_reg_reg[11] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(address_in[11]),
        .Q(address_in_reg[11]),
        .R(ERROR_i_1_n_0));
  FDRE \address_in_reg_reg[12] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(address_in[12]),
        .Q(address_in_reg[12]),
        .R(ERROR_i_1_n_0));
  FDRE \address_in_reg_reg[13] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(address_in[13]),
        .Q(address_in_reg[13]),
        .R(ERROR_i_1_n_0));
  FDRE \address_in_reg_reg[14] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(address_in[14]),
        .Q(address_in_reg[14]),
        .R(ERROR_i_1_n_0));
  FDRE \address_in_reg_reg[15] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(address_in[15]),
        .Q(address_in_reg[15]),
        .R(ERROR_i_1_n_0));
  FDRE \address_in_reg_reg[16] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(address_in[16]),
        .Q(address_in_reg[16]),
        .R(ERROR_i_1_n_0));
  FDRE \address_in_reg_reg[17] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(address_in[17]),
        .Q(address_in_reg[17]),
        .R(ERROR_i_1_n_0));
  FDRE \address_in_reg_reg[18] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(address_in[18]),
        .Q(address_in_reg[18]),
        .R(ERROR_i_1_n_0));
  FDRE \address_in_reg_reg[19] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(address_in[19]),
        .Q(address_in_reg[19]),
        .R(ERROR_i_1_n_0));
  FDRE \address_in_reg_reg[1] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(address_in[1]),
        .Q(address_in_reg[1]),
        .R(ERROR_i_1_n_0));
  FDRE \address_in_reg_reg[20] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(address_in[20]),
        .Q(address_in_reg[20]),
        .R(ERROR_i_1_n_0));
  FDRE \address_in_reg_reg[21] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(address_in[21]),
        .Q(address_in_reg[21]),
        .R(ERROR_i_1_n_0));
  FDRE \address_in_reg_reg[22] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(address_in[22]),
        .Q(address_in_reg[22]),
        .R(ERROR_i_1_n_0));
  FDRE \address_in_reg_reg[23] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(address_in[23]),
        .Q(address_in_reg[23]),
        .R(ERROR_i_1_n_0));
  FDRE \address_in_reg_reg[24] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(address_in[24]),
        .Q(address_in_reg[24]),
        .R(ERROR_i_1_n_0));
  FDRE \address_in_reg_reg[25] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(address_in[25]),
        .Q(address_in_reg[25]),
        .R(ERROR_i_1_n_0));
  FDRE \address_in_reg_reg[26] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(address_in[26]),
        .Q(address_in_reg[26]),
        .R(ERROR_i_1_n_0));
  FDRE \address_in_reg_reg[27] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(address_in[27]),
        .Q(address_in_reg[27]),
        .R(ERROR_i_1_n_0));
  FDRE \address_in_reg_reg[28] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(address_in[28]),
        .Q(address_in_reg[28]),
        .R(ERROR_i_1_n_0));
  FDRE \address_in_reg_reg[29] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(address_in[29]),
        .Q(address_in_reg[29]),
        .R(ERROR_i_1_n_0));
  FDRE \address_in_reg_reg[2] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(address_in[2]),
        .Q(address_in_reg[2]),
        .R(ERROR_i_1_n_0));
  FDRE \address_in_reg_reg[30] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(address_in[30]),
        .Q(address_in_reg[30]),
        .R(ERROR_i_1_n_0));
  FDRE \address_in_reg_reg[31] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(address_in[31]),
        .Q(address_in_reg[31]),
        .R(ERROR_i_1_n_0));
  FDRE \address_in_reg_reg[3] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(address_in[3]),
        .Q(address_in_reg[3]),
        .R(ERROR_i_1_n_0));
  FDRE \address_in_reg_reg[4] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(address_in[4]),
        .Q(address_in_reg[4]),
        .R(ERROR_i_1_n_0));
  FDRE \address_in_reg_reg[5] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(address_in[5]),
        .Q(address_in_reg[5]),
        .R(ERROR_i_1_n_0));
  FDRE \address_in_reg_reg[6] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(address_in[6]),
        .Q(address_in_reg[6]),
        .R(ERROR_i_1_n_0));
  FDRE \address_in_reg_reg[7] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(address_in[7]),
        .Q(address_in_reg[7]),
        .R(ERROR_i_1_n_0));
  FDRE \address_in_reg_reg[8] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(address_in[8]),
        .Q(address_in_reg[8]),
        .R(ERROR_i_1_n_0));
  FDRE \address_in_reg_reg[9] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(address_in[9]),
        .Q(address_in_reg[9]),
        .R(ERROR_i_1_n_0));
  LUT5 #(
    .INIT(32'hFF5D5D5D)) 
    \axi_araddr[31]_i_1 
       (.I0(m00_axi_aresetn),
        .I1(init_txn_ff),
        .I2(init_txn_ff2),
        .I3(m00_axi_arvalid),
        .I4(m00_axi_arready),
        .O(\axi_araddr[31]_i_1_n_0 ));
  FDRE \axi_araddr_reg[0] 
       (.C(m00_axi_aclk),
        .CE(\axi_araddr[31]_i_1_n_0 ),
        .D(address_in_reg[0]),
        .Q(m00_axi_araddr[0]),
        .R(1'b0));
  FDRE \axi_araddr_reg[10] 
       (.C(m00_axi_aclk),
        .CE(\axi_araddr[31]_i_1_n_0 ),
        .D(address_in_reg[10]),
        .Q(m00_axi_araddr[10]),
        .R(1'b0));
  FDRE \axi_araddr_reg[11] 
       (.C(m00_axi_aclk),
        .CE(\axi_araddr[31]_i_1_n_0 ),
        .D(address_in_reg[11]),
        .Q(m00_axi_araddr[11]),
        .R(1'b0));
  FDRE \axi_araddr_reg[12] 
       (.C(m00_axi_aclk),
        .CE(\axi_araddr[31]_i_1_n_0 ),
        .D(address_in_reg[12]),
        .Q(m00_axi_araddr[12]),
        .R(1'b0));
  FDRE \axi_araddr_reg[13] 
       (.C(m00_axi_aclk),
        .CE(\axi_araddr[31]_i_1_n_0 ),
        .D(address_in_reg[13]),
        .Q(m00_axi_araddr[13]),
        .R(1'b0));
  FDRE \axi_araddr_reg[14] 
       (.C(m00_axi_aclk),
        .CE(\axi_araddr[31]_i_1_n_0 ),
        .D(address_in_reg[14]),
        .Q(m00_axi_araddr[14]),
        .R(1'b0));
  FDRE \axi_araddr_reg[15] 
       (.C(m00_axi_aclk),
        .CE(\axi_araddr[31]_i_1_n_0 ),
        .D(address_in_reg[15]),
        .Q(m00_axi_araddr[15]),
        .R(1'b0));
  FDRE \axi_araddr_reg[16] 
       (.C(m00_axi_aclk),
        .CE(\axi_araddr[31]_i_1_n_0 ),
        .D(address_in_reg[16]),
        .Q(m00_axi_araddr[16]),
        .R(1'b0));
  FDRE \axi_araddr_reg[17] 
       (.C(m00_axi_aclk),
        .CE(\axi_araddr[31]_i_1_n_0 ),
        .D(address_in_reg[17]),
        .Q(m00_axi_araddr[17]),
        .R(1'b0));
  FDRE \axi_araddr_reg[18] 
       (.C(m00_axi_aclk),
        .CE(\axi_araddr[31]_i_1_n_0 ),
        .D(address_in_reg[18]),
        .Q(m00_axi_araddr[18]),
        .R(1'b0));
  FDRE \axi_araddr_reg[19] 
       (.C(m00_axi_aclk),
        .CE(\axi_araddr[31]_i_1_n_0 ),
        .D(address_in_reg[19]),
        .Q(m00_axi_araddr[19]),
        .R(1'b0));
  FDRE \axi_araddr_reg[1] 
       (.C(m00_axi_aclk),
        .CE(\axi_araddr[31]_i_1_n_0 ),
        .D(address_in_reg[1]),
        .Q(m00_axi_araddr[1]),
        .R(1'b0));
  FDRE \axi_araddr_reg[20] 
       (.C(m00_axi_aclk),
        .CE(\axi_araddr[31]_i_1_n_0 ),
        .D(address_in_reg[20]),
        .Q(m00_axi_araddr[20]),
        .R(1'b0));
  FDRE \axi_araddr_reg[21] 
       (.C(m00_axi_aclk),
        .CE(\axi_araddr[31]_i_1_n_0 ),
        .D(address_in_reg[21]),
        .Q(m00_axi_araddr[21]),
        .R(1'b0));
  FDRE \axi_araddr_reg[22] 
       (.C(m00_axi_aclk),
        .CE(\axi_araddr[31]_i_1_n_0 ),
        .D(address_in_reg[22]),
        .Q(m00_axi_araddr[22]),
        .R(1'b0));
  FDRE \axi_araddr_reg[23] 
       (.C(m00_axi_aclk),
        .CE(\axi_araddr[31]_i_1_n_0 ),
        .D(address_in_reg[23]),
        .Q(m00_axi_araddr[23]),
        .R(1'b0));
  FDRE \axi_araddr_reg[24] 
       (.C(m00_axi_aclk),
        .CE(\axi_araddr[31]_i_1_n_0 ),
        .D(address_in_reg[24]),
        .Q(m00_axi_araddr[24]),
        .R(1'b0));
  FDRE \axi_araddr_reg[25] 
       (.C(m00_axi_aclk),
        .CE(\axi_araddr[31]_i_1_n_0 ),
        .D(address_in_reg[25]),
        .Q(m00_axi_araddr[25]),
        .R(1'b0));
  FDRE \axi_araddr_reg[26] 
       (.C(m00_axi_aclk),
        .CE(\axi_araddr[31]_i_1_n_0 ),
        .D(address_in_reg[26]),
        .Q(m00_axi_araddr[26]),
        .R(1'b0));
  FDRE \axi_araddr_reg[27] 
       (.C(m00_axi_aclk),
        .CE(\axi_araddr[31]_i_1_n_0 ),
        .D(address_in_reg[27]),
        .Q(m00_axi_araddr[27]),
        .R(1'b0));
  FDRE \axi_araddr_reg[28] 
       (.C(m00_axi_aclk),
        .CE(\axi_araddr[31]_i_1_n_0 ),
        .D(address_in_reg[28]),
        .Q(m00_axi_araddr[28]),
        .R(1'b0));
  FDRE \axi_araddr_reg[29] 
       (.C(m00_axi_aclk),
        .CE(\axi_araddr[31]_i_1_n_0 ),
        .D(address_in_reg[29]),
        .Q(m00_axi_araddr[29]),
        .R(1'b0));
  FDRE \axi_araddr_reg[2] 
       (.C(m00_axi_aclk),
        .CE(\axi_araddr[31]_i_1_n_0 ),
        .D(address_in_reg[2]),
        .Q(m00_axi_araddr[2]),
        .R(1'b0));
  FDRE \axi_araddr_reg[30] 
       (.C(m00_axi_aclk),
        .CE(\axi_araddr[31]_i_1_n_0 ),
        .D(address_in_reg[30]),
        .Q(m00_axi_araddr[30]),
        .R(1'b0));
  FDRE \axi_araddr_reg[31] 
       (.C(m00_axi_aclk),
        .CE(\axi_araddr[31]_i_1_n_0 ),
        .D(address_in_reg[31]),
        .Q(m00_axi_araddr[31]),
        .R(1'b0));
  FDRE \axi_araddr_reg[3] 
       (.C(m00_axi_aclk),
        .CE(\axi_araddr[31]_i_1_n_0 ),
        .D(address_in_reg[3]),
        .Q(m00_axi_araddr[3]),
        .R(1'b0));
  FDRE \axi_araddr_reg[4] 
       (.C(m00_axi_aclk),
        .CE(\axi_araddr[31]_i_1_n_0 ),
        .D(address_in_reg[4]),
        .Q(m00_axi_araddr[4]),
        .R(1'b0));
  FDRE \axi_araddr_reg[5] 
       (.C(m00_axi_aclk),
        .CE(\axi_araddr[31]_i_1_n_0 ),
        .D(address_in_reg[5]),
        .Q(m00_axi_araddr[5]),
        .R(1'b0));
  FDRE \axi_araddr_reg[6] 
       (.C(m00_axi_aclk),
        .CE(\axi_araddr[31]_i_1_n_0 ),
        .D(address_in_reg[6]),
        .Q(m00_axi_araddr[6]),
        .R(1'b0));
  FDRE \axi_araddr_reg[7] 
       (.C(m00_axi_aclk),
        .CE(\axi_araddr[31]_i_1_n_0 ),
        .D(address_in_reg[7]),
        .Q(m00_axi_araddr[7]),
        .R(1'b0));
  FDRE \axi_araddr_reg[8] 
       (.C(m00_axi_aclk),
        .CE(\axi_araddr[31]_i_1_n_0 ),
        .D(address_in_reg[8]),
        .Q(m00_axi_araddr[8]),
        .R(1'b0));
  FDRE \axi_araddr_reg[9] 
       (.C(m00_axi_aclk),
        .CE(\axi_araddr[31]_i_1_n_0 ),
        .D(address_in_reg[9]),
        .Q(m00_axi_araddr[9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'hAE)) 
    axi_arvalid_i_1
       (.I0(start_single_read_reg_n_0),
        .I1(m00_axi_arvalid),
        .I2(m00_axi_arready),
        .O(axi_arvalid_i_1_n_0));
  FDRE axi_arvalid_reg
       (.C(m00_axi_aclk),
        .CE(1'b1),
        .D(axi_arvalid_i_1_n_0),
        .Q(m00_axi_arvalid),
        .R(axi_awvalid_i_1_n_0));
  LUT5 #(
    .INIT(32'hFF5D5D5D)) 
    \axi_awaddr[31]_i_1 
       (.I0(m00_axi_aresetn),
        .I1(init_txn_ff),
        .I2(init_txn_ff2),
        .I3(m00_axi_awvalid),
        .I4(m00_axi_awready),
        .O(\axi_awaddr[31]_i_1_n_0 ));
  FDRE \axi_awaddr_reg[0] 
       (.C(m00_axi_aclk),
        .CE(\axi_awaddr[31]_i_1_n_0 ),
        .D(address_in_reg[0]),
        .Q(m00_axi_awaddr[0]),
        .R(1'b0));
  FDRE \axi_awaddr_reg[10] 
       (.C(m00_axi_aclk),
        .CE(\axi_awaddr[31]_i_1_n_0 ),
        .D(address_in_reg[10]),
        .Q(m00_axi_awaddr[10]),
        .R(1'b0));
  FDRE \axi_awaddr_reg[11] 
       (.C(m00_axi_aclk),
        .CE(\axi_awaddr[31]_i_1_n_0 ),
        .D(address_in_reg[11]),
        .Q(m00_axi_awaddr[11]),
        .R(1'b0));
  FDRE \axi_awaddr_reg[12] 
       (.C(m00_axi_aclk),
        .CE(\axi_awaddr[31]_i_1_n_0 ),
        .D(address_in_reg[12]),
        .Q(m00_axi_awaddr[12]),
        .R(1'b0));
  FDRE \axi_awaddr_reg[13] 
       (.C(m00_axi_aclk),
        .CE(\axi_awaddr[31]_i_1_n_0 ),
        .D(address_in_reg[13]),
        .Q(m00_axi_awaddr[13]),
        .R(1'b0));
  FDRE \axi_awaddr_reg[14] 
       (.C(m00_axi_aclk),
        .CE(\axi_awaddr[31]_i_1_n_0 ),
        .D(address_in_reg[14]),
        .Q(m00_axi_awaddr[14]),
        .R(1'b0));
  FDRE \axi_awaddr_reg[15] 
       (.C(m00_axi_aclk),
        .CE(\axi_awaddr[31]_i_1_n_0 ),
        .D(address_in_reg[15]),
        .Q(m00_axi_awaddr[15]),
        .R(1'b0));
  FDRE \axi_awaddr_reg[16] 
       (.C(m00_axi_aclk),
        .CE(\axi_awaddr[31]_i_1_n_0 ),
        .D(address_in_reg[16]),
        .Q(m00_axi_awaddr[16]),
        .R(1'b0));
  FDRE \axi_awaddr_reg[17] 
       (.C(m00_axi_aclk),
        .CE(\axi_awaddr[31]_i_1_n_0 ),
        .D(address_in_reg[17]),
        .Q(m00_axi_awaddr[17]),
        .R(1'b0));
  FDRE \axi_awaddr_reg[18] 
       (.C(m00_axi_aclk),
        .CE(\axi_awaddr[31]_i_1_n_0 ),
        .D(address_in_reg[18]),
        .Q(m00_axi_awaddr[18]),
        .R(1'b0));
  FDRE \axi_awaddr_reg[19] 
       (.C(m00_axi_aclk),
        .CE(\axi_awaddr[31]_i_1_n_0 ),
        .D(address_in_reg[19]),
        .Q(m00_axi_awaddr[19]),
        .R(1'b0));
  FDRE \axi_awaddr_reg[1] 
       (.C(m00_axi_aclk),
        .CE(\axi_awaddr[31]_i_1_n_0 ),
        .D(address_in_reg[1]),
        .Q(m00_axi_awaddr[1]),
        .R(1'b0));
  FDRE \axi_awaddr_reg[20] 
       (.C(m00_axi_aclk),
        .CE(\axi_awaddr[31]_i_1_n_0 ),
        .D(address_in_reg[20]),
        .Q(m00_axi_awaddr[20]),
        .R(1'b0));
  FDRE \axi_awaddr_reg[21] 
       (.C(m00_axi_aclk),
        .CE(\axi_awaddr[31]_i_1_n_0 ),
        .D(address_in_reg[21]),
        .Q(m00_axi_awaddr[21]),
        .R(1'b0));
  FDRE \axi_awaddr_reg[22] 
       (.C(m00_axi_aclk),
        .CE(\axi_awaddr[31]_i_1_n_0 ),
        .D(address_in_reg[22]),
        .Q(m00_axi_awaddr[22]),
        .R(1'b0));
  FDRE \axi_awaddr_reg[23] 
       (.C(m00_axi_aclk),
        .CE(\axi_awaddr[31]_i_1_n_0 ),
        .D(address_in_reg[23]),
        .Q(m00_axi_awaddr[23]),
        .R(1'b0));
  FDRE \axi_awaddr_reg[24] 
       (.C(m00_axi_aclk),
        .CE(\axi_awaddr[31]_i_1_n_0 ),
        .D(address_in_reg[24]),
        .Q(m00_axi_awaddr[24]),
        .R(1'b0));
  FDRE \axi_awaddr_reg[25] 
       (.C(m00_axi_aclk),
        .CE(\axi_awaddr[31]_i_1_n_0 ),
        .D(address_in_reg[25]),
        .Q(m00_axi_awaddr[25]),
        .R(1'b0));
  FDRE \axi_awaddr_reg[26] 
       (.C(m00_axi_aclk),
        .CE(\axi_awaddr[31]_i_1_n_0 ),
        .D(address_in_reg[26]),
        .Q(m00_axi_awaddr[26]),
        .R(1'b0));
  FDRE \axi_awaddr_reg[27] 
       (.C(m00_axi_aclk),
        .CE(\axi_awaddr[31]_i_1_n_0 ),
        .D(address_in_reg[27]),
        .Q(m00_axi_awaddr[27]),
        .R(1'b0));
  FDRE \axi_awaddr_reg[28] 
       (.C(m00_axi_aclk),
        .CE(\axi_awaddr[31]_i_1_n_0 ),
        .D(address_in_reg[28]),
        .Q(m00_axi_awaddr[28]),
        .R(1'b0));
  FDRE \axi_awaddr_reg[29] 
       (.C(m00_axi_aclk),
        .CE(\axi_awaddr[31]_i_1_n_0 ),
        .D(address_in_reg[29]),
        .Q(m00_axi_awaddr[29]),
        .R(1'b0));
  FDRE \axi_awaddr_reg[2] 
       (.C(m00_axi_aclk),
        .CE(\axi_awaddr[31]_i_1_n_0 ),
        .D(address_in_reg[2]),
        .Q(m00_axi_awaddr[2]),
        .R(1'b0));
  FDRE \axi_awaddr_reg[30] 
       (.C(m00_axi_aclk),
        .CE(\axi_awaddr[31]_i_1_n_0 ),
        .D(address_in_reg[30]),
        .Q(m00_axi_awaddr[30]),
        .R(1'b0));
  FDRE \axi_awaddr_reg[31] 
       (.C(m00_axi_aclk),
        .CE(\axi_awaddr[31]_i_1_n_0 ),
        .D(address_in_reg[31]),
        .Q(m00_axi_awaddr[31]),
        .R(1'b0));
  FDRE \axi_awaddr_reg[3] 
       (.C(m00_axi_aclk),
        .CE(\axi_awaddr[31]_i_1_n_0 ),
        .D(address_in_reg[3]),
        .Q(m00_axi_awaddr[3]),
        .R(1'b0));
  FDRE \axi_awaddr_reg[4] 
       (.C(m00_axi_aclk),
        .CE(\axi_awaddr[31]_i_1_n_0 ),
        .D(address_in_reg[4]),
        .Q(m00_axi_awaddr[4]),
        .R(1'b0));
  FDRE \axi_awaddr_reg[5] 
       (.C(m00_axi_aclk),
        .CE(\axi_awaddr[31]_i_1_n_0 ),
        .D(address_in_reg[5]),
        .Q(m00_axi_awaddr[5]),
        .R(1'b0));
  FDRE \axi_awaddr_reg[6] 
       (.C(m00_axi_aclk),
        .CE(\axi_awaddr[31]_i_1_n_0 ),
        .D(address_in_reg[6]),
        .Q(m00_axi_awaddr[6]),
        .R(1'b0));
  FDRE \axi_awaddr_reg[7] 
       (.C(m00_axi_aclk),
        .CE(\axi_awaddr[31]_i_1_n_0 ),
        .D(address_in_reg[7]),
        .Q(m00_axi_awaddr[7]),
        .R(1'b0));
  FDRE \axi_awaddr_reg[8] 
       (.C(m00_axi_aclk),
        .CE(\axi_awaddr[31]_i_1_n_0 ),
        .D(address_in_reg[8]),
        .Q(m00_axi_awaddr[8]),
        .R(1'b0));
  FDRE \axi_awaddr_reg[9] 
       (.C(m00_axi_aclk),
        .CE(\axi_awaddr[31]_i_1_n_0 ),
        .D(address_in_reg[9]),
        .Q(m00_axi_awaddr[9]),
        .R(1'b0));
  LUT3 #(
    .INIT(8'h4F)) 
    axi_awvalid_i_1
       (.I0(init_txn_ff2),
        .I1(init_txn_ff),
        .I2(m00_axi_aresetn),
        .O(axi_awvalid_i_1_n_0));
  LUT3 #(
    .INIT(8'hAE)) 
    axi_awvalid_i_2
       (.I0(start_single_write_reg_n_0),
        .I1(m00_axi_awvalid),
        .I2(m00_axi_awready),
        .O(axi_awvalid_i_2_n_0));
  FDRE axi_awvalid_reg
       (.C(m00_axi_aclk),
        .CE(1'b1),
        .D(axi_awvalid_i_2_n_0),
        .Q(m00_axi_awvalid),
        .R(axi_awvalid_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT2 #(
    .INIT(4'h4)) 
    axi_bready_i_1
       (.I0(m00_axi_bready),
        .I1(m00_axi_bvalid),
        .O(axi_bready_i_1_n_0));
  FDRE axi_bready_reg
       (.C(m00_axi_aclk),
        .CE(1'b1),
        .D(axi_bready_i_1_n_0),
        .Q(m00_axi_bready),
        .R(axi_awvalid_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h5DFF5D5D)) 
    axi_rready_i_1
       (.I0(m00_axi_aresetn),
        .I1(init_txn_ff),
        .I2(init_txn_ff2),
        .I3(m00_axi_rready),
        .I4(m00_axi_rvalid),
        .O(axi_rready_i_1_n_0));
  FDRE axi_rready_reg
       (.C(m00_axi_aclk),
        .CE(1'b1),
        .D(axi_rready_i_1_n_0),
        .Q(m00_axi_rready),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hFF5D5D5D)) 
    \axi_wdata[31]_i_1 
       (.I0(m00_axi_aresetn),
        .I1(init_txn_ff),
        .I2(init_txn_ff2),
        .I3(m00_axi_wvalid),
        .I4(m00_axi_wready),
        .O(\axi_wdata[31]_i_1_n_0 ));
  FDRE \axi_wdata_reg[0] 
       (.C(m00_axi_aclk),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(data_in_reg[0]),
        .Q(m00_axi_wdata[0]),
        .R(1'b0));
  FDRE \axi_wdata_reg[10] 
       (.C(m00_axi_aclk),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(data_in_reg[10]),
        .Q(m00_axi_wdata[10]),
        .R(1'b0));
  FDRE \axi_wdata_reg[11] 
       (.C(m00_axi_aclk),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(data_in_reg[11]),
        .Q(m00_axi_wdata[11]),
        .R(1'b0));
  FDRE \axi_wdata_reg[12] 
       (.C(m00_axi_aclk),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(data_in_reg[12]),
        .Q(m00_axi_wdata[12]),
        .R(1'b0));
  FDRE \axi_wdata_reg[13] 
       (.C(m00_axi_aclk),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(data_in_reg[13]),
        .Q(m00_axi_wdata[13]),
        .R(1'b0));
  FDRE \axi_wdata_reg[14] 
       (.C(m00_axi_aclk),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(data_in_reg[14]),
        .Q(m00_axi_wdata[14]),
        .R(1'b0));
  FDRE \axi_wdata_reg[15] 
       (.C(m00_axi_aclk),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(data_in_reg[15]),
        .Q(m00_axi_wdata[15]),
        .R(1'b0));
  FDRE \axi_wdata_reg[16] 
       (.C(m00_axi_aclk),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(data_in_reg[16]),
        .Q(m00_axi_wdata[16]),
        .R(1'b0));
  FDRE \axi_wdata_reg[17] 
       (.C(m00_axi_aclk),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(data_in_reg[17]),
        .Q(m00_axi_wdata[17]),
        .R(1'b0));
  FDRE \axi_wdata_reg[18] 
       (.C(m00_axi_aclk),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(data_in_reg[18]),
        .Q(m00_axi_wdata[18]),
        .R(1'b0));
  FDRE \axi_wdata_reg[19] 
       (.C(m00_axi_aclk),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(data_in_reg[19]),
        .Q(m00_axi_wdata[19]),
        .R(1'b0));
  FDRE \axi_wdata_reg[1] 
       (.C(m00_axi_aclk),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(data_in_reg[1]),
        .Q(m00_axi_wdata[1]),
        .R(1'b0));
  FDRE \axi_wdata_reg[20] 
       (.C(m00_axi_aclk),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(data_in_reg[20]),
        .Q(m00_axi_wdata[20]),
        .R(1'b0));
  FDRE \axi_wdata_reg[21] 
       (.C(m00_axi_aclk),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(data_in_reg[21]),
        .Q(m00_axi_wdata[21]),
        .R(1'b0));
  FDRE \axi_wdata_reg[22] 
       (.C(m00_axi_aclk),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(data_in_reg[22]),
        .Q(m00_axi_wdata[22]),
        .R(1'b0));
  FDRE \axi_wdata_reg[23] 
       (.C(m00_axi_aclk),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(data_in_reg[23]),
        .Q(m00_axi_wdata[23]),
        .R(1'b0));
  FDRE \axi_wdata_reg[24] 
       (.C(m00_axi_aclk),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(data_in_reg[24]),
        .Q(m00_axi_wdata[24]),
        .R(1'b0));
  FDRE \axi_wdata_reg[25] 
       (.C(m00_axi_aclk),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(data_in_reg[25]),
        .Q(m00_axi_wdata[25]),
        .R(1'b0));
  FDRE \axi_wdata_reg[26] 
       (.C(m00_axi_aclk),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(data_in_reg[26]),
        .Q(m00_axi_wdata[26]),
        .R(1'b0));
  FDRE \axi_wdata_reg[27] 
       (.C(m00_axi_aclk),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(data_in_reg[27]),
        .Q(m00_axi_wdata[27]),
        .R(1'b0));
  FDRE \axi_wdata_reg[28] 
       (.C(m00_axi_aclk),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(data_in_reg[28]),
        .Q(m00_axi_wdata[28]),
        .R(1'b0));
  FDRE \axi_wdata_reg[29] 
       (.C(m00_axi_aclk),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(data_in_reg[29]),
        .Q(m00_axi_wdata[29]),
        .R(1'b0));
  FDRE \axi_wdata_reg[2] 
       (.C(m00_axi_aclk),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(data_in_reg[2]),
        .Q(m00_axi_wdata[2]),
        .R(1'b0));
  FDRE \axi_wdata_reg[30] 
       (.C(m00_axi_aclk),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(data_in_reg[30]),
        .Q(m00_axi_wdata[30]),
        .R(1'b0));
  FDRE \axi_wdata_reg[31] 
       (.C(m00_axi_aclk),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(data_in_reg[31]),
        .Q(m00_axi_wdata[31]),
        .R(1'b0));
  FDRE \axi_wdata_reg[3] 
       (.C(m00_axi_aclk),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(data_in_reg[3]),
        .Q(m00_axi_wdata[3]),
        .R(1'b0));
  FDRE \axi_wdata_reg[4] 
       (.C(m00_axi_aclk),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(data_in_reg[4]),
        .Q(m00_axi_wdata[4]),
        .R(1'b0));
  FDRE \axi_wdata_reg[5] 
       (.C(m00_axi_aclk),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(data_in_reg[5]),
        .Q(m00_axi_wdata[5]),
        .R(1'b0));
  FDRE \axi_wdata_reg[6] 
       (.C(m00_axi_aclk),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(data_in_reg[6]),
        .Q(m00_axi_wdata[6]),
        .R(1'b0));
  FDRE \axi_wdata_reg[7] 
       (.C(m00_axi_aclk),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(data_in_reg[7]),
        .Q(m00_axi_wdata[7]),
        .R(1'b0));
  FDRE \axi_wdata_reg[8] 
       (.C(m00_axi_aclk),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(data_in_reg[8]),
        .Q(m00_axi_wdata[8]),
        .R(1'b0));
  FDRE \axi_wdata_reg[9] 
       (.C(m00_axi_aclk),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(data_in_reg[9]),
        .Q(m00_axi_wdata[9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'hAE)) 
    axi_wvalid_i_1
       (.I0(start_single_write_reg_n_0),
        .I1(m00_axi_wvalid),
        .I2(m00_axi_wready),
        .O(axi_wvalid_i_1_n_0));
  FDRE axi_wvalid_reg
       (.C(m00_axi_aclk),
        .CE(1'b1),
        .D(axi_wvalid_i_1_n_0),
        .Q(m00_axi_wvalid),
        .R(axi_awvalid_i_1_n_0));
  LUT6 #(
    .INIT(64'hFFF5FFF50CF00C00)) 
    compare_done_i_1
       (.I0(init_txn_pulse__0),
        .I1(reads_done),
        .I2(mst_exec_state[0]),
        .I3(mst_exec_state[1]),
        .I4(writes_done),
        .I5(m00_axi_txn_done),
        .O(compare_done_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT2 #(
    .INIT(4'h2)) 
    compare_done_i_2
       (.I0(init_txn_ff),
        .I1(init_txn_ff2),
        .O(init_txn_pulse__0));
  FDRE compare_done_reg
       (.C(m00_axi_aclk),
        .CE(1'b1),
        .D(compare_done_i_1_n_0),
        .Q(m00_axi_txn_done),
        .R(ERROR_i_1_n_0));
  FDRE \data_in_reg_reg[0] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(data_in[0]),
        .Q(data_in_reg[0]),
        .R(ERROR_i_1_n_0));
  FDRE \data_in_reg_reg[10] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(data_in[10]),
        .Q(data_in_reg[10]),
        .R(ERROR_i_1_n_0));
  FDRE \data_in_reg_reg[11] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(data_in[11]),
        .Q(data_in_reg[11]),
        .R(ERROR_i_1_n_0));
  FDRE \data_in_reg_reg[12] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(data_in[12]),
        .Q(data_in_reg[12]),
        .R(ERROR_i_1_n_0));
  FDRE \data_in_reg_reg[13] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(data_in[13]),
        .Q(data_in_reg[13]),
        .R(ERROR_i_1_n_0));
  FDRE \data_in_reg_reg[14] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(data_in[14]),
        .Q(data_in_reg[14]),
        .R(ERROR_i_1_n_0));
  FDRE \data_in_reg_reg[15] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(data_in[15]),
        .Q(data_in_reg[15]),
        .R(ERROR_i_1_n_0));
  FDRE \data_in_reg_reg[16] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(data_in[16]),
        .Q(data_in_reg[16]),
        .R(ERROR_i_1_n_0));
  FDRE \data_in_reg_reg[17] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(data_in[17]),
        .Q(data_in_reg[17]),
        .R(ERROR_i_1_n_0));
  FDRE \data_in_reg_reg[18] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(data_in[18]),
        .Q(data_in_reg[18]),
        .R(ERROR_i_1_n_0));
  FDRE \data_in_reg_reg[19] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(data_in[19]),
        .Q(data_in_reg[19]),
        .R(ERROR_i_1_n_0));
  FDRE \data_in_reg_reg[1] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(data_in[1]),
        .Q(data_in_reg[1]),
        .R(ERROR_i_1_n_0));
  FDRE \data_in_reg_reg[20] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(data_in[20]),
        .Q(data_in_reg[20]),
        .R(ERROR_i_1_n_0));
  FDRE \data_in_reg_reg[21] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(data_in[21]),
        .Q(data_in_reg[21]),
        .R(ERROR_i_1_n_0));
  FDRE \data_in_reg_reg[22] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(data_in[22]),
        .Q(data_in_reg[22]),
        .R(ERROR_i_1_n_0));
  FDRE \data_in_reg_reg[23] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(data_in[23]),
        .Q(data_in_reg[23]),
        .R(ERROR_i_1_n_0));
  FDRE \data_in_reg_reg[24] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(data_in[24]),
        .Q(data_in_reg[24]),
        .R(ERROR_i_1_n_0));
  FDRE \data_in_reg_reg[25] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(data_in[25]),
        .Q(data_in_reg[25]),
        .R(ERROR_i_1_n_0));
  FDRE \data_in_reg_reg[26] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(data_in[26]),
        .Q(data_in_reg[26]),
        .R(ERROR_i_1_n_0));
  FDRE \data_in_reg_reg[27] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(data_in[27]),
        .Q(data_in_reg[27]),
        .R(ERROR_i_1_n_0));
  FDRE \data_in_reg_reg[28] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(data_in[28]),
        .Q(data_in_reg[28]),
        .R(ERROR_i_1_n_0));
  FDRE \data_in_reg_reg[29] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(data_in[29]),
        .Q(data_in_reg[29]),
        .R(ERROR_i_1_n_0));
  FDRE \data_in_reg_reg[2] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(data_in[2]),
        .Q(data_in_reg[2]),
        .R(ERROR_i_1_n_0));
  FDRE \data_in_reg_reg[30] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(data_in[30]),
        .Q(data_in_reg[30]),
        .R(ERROR_i_1_n_0));
  FDRE \data_in_reg_reg[31] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(data_in[31]),
        .Q(data_in_reg[31]),
        .R(ERROR_i_1_n_0));
  FDRE \data_in_reg_reg[3] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(data_in[3]),
        .Q(data_in_reg[3]),
        .R(ERROR_i_1_n_0));
  FDRE \data_in_reg_reg[4] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(data_in[4]),
        .Q(data_in_reg[4]),
        .R(ERROR_i_1_n_0));
  FDRE \data_in_reg_reg[5] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(data_in[5]),
        .Q(data_in_reg[5]),
        .R(ERROR_i_1_n_0));
  FDRE \data_in_reg_reg[6] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(data_in[6]),
        .Q(data_in_reg[6]),
        .R(ERROR_i_1_n_0));
  FDRE \data_in_reg_reg[7] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(data_in[7]),
        .Q(data_in_reg[7]),
        .R(ERROR_i_1_n_0));
  FDRE \data_in_reg_reg[8] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(data_in[8]),
        .Q(data_in_reg[8]),
        .R(ERROR_i_1_n_0));
  FDRE \data_in_reg_reg[9] 
       (.C(m00_axi_aclk),
        .CE(address_in_reg0),
        .D(data_in[9]),
        .Q(data_in_reg[9]),
        .R(ERROR_i_1_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    error_reg_i_1
       (.I0(error_reg0),
        .I1(error_reg),
        .O(error_reg_i_1_n_0));
  LUT6 #(
    .INIT(64'hFF80808080808080)) 
    error_reg_i_2
       (.I0(m00_axi_bresp),
        .I1(m00_axi_bvalid),
        .I2(m00_axi_bready),
        .I3(m00_axi_rresp),
        .I4(m00_axi_rvalid),
        .I5(m00_axi_rready),
        .O(error_reg0));
  FDRE error_reg_reg
       (.C(m00_axi_aclk),
        .CE(1'b1),
        .D(error_reg_i_1_n_0),
        .Q(error_reg),
        .R(axi_awvalid_i_1_n_0));
  FDRE init_txn_ff2_reg
       (.C(m00_axi_aclk),
        .CE(1'b1),
        .D(init_txn_ff),
        .Q(init_txn_ff2),
        .R(ERROR_i_1_n_0));
  FDRE init_txn_ff_reg
       (.C(m00_axi_aclk),
        .CE(1'b1),
        .D(m00_axi_init_axi_txn),
        .Q(init_txn_ff),
        .R(ERROR_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'hFF08)) 
    last_read_i_1
       (.I0(m00_axi_arready),
        .I1(read_index[0]),
        .I2(read_index[1]),
        .I3(last_read),
        .O(last_read_i_1_n_0));
  FDRE last_read_reg
       (.C(m00_axi_aclk),
        .CE(1'b1),
        .D(last_read_i_1_n_0),
        .Q(last_read),
        .R(axi_awvalid_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'hFF08)) 
    last_write_i_1
       (.I0(m00_axi_awready),
        .I1(write_index[0]),
        .I2(write_index[1]),
        .I3(last_write),
        .O(last_write_i_1_n_0));
  FDRE last_write_reg
       (.C(m00_axi_aclk),
        .CE(1'b1),
        .D(last_write_i_1_n_0),
        .Q(last_write),
        .R(axi_awvalid_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \read_index[0]_i_1 
       (.I0(start_single_read_reg_n_0),
        .I1(read_index[0]),
        .O(\read_index[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \read_index[1]_i_1 
       (.I0(read_index[0]),
        .I1(start_single_read_reg_n_0),
        .I2(read_index[1]),
        .O(\read_index[1]_i_1_n_0 ));
  FDRE \read_index_reg[0] 
       (.C(m00_axi_aclk),
        .CE(1'b1),
        .D(\read_index[0]_i_1_n_0 ),
        .Q(read_index[0]),
        .R(axi_awvalid_i_1_n_0));
  FDRE \read_index_reg[1] 
       (.C(m00_axi_aclk),
        .CE(1'b1),
        .D(\read_index[1]_i_1_n_0 ),
        .Q(read_index[1]),
        .R(axi_awvalid_i_1_n_0));
  LUT6 #(
    .INIT(64'hFFFBFFFF00500000)) 
    read_issued_i_1
       (.I0(mst_exec_state[0]),
        .I1(m00_axi_rready),
        .I2(start_single_read0),
        .I3(reads_done),
        .I4(mst_exec_state[1]),
        .I5(read_issued_reg_n_0),
        .O(read_issued_i_1_n_0));
  FDRE read_issued_reg
       (.C(m00_axi_aclk),
        .CE(1'b1),
        .D(read_issued_i_1_n_0),
        .Q(read_issued_reg_n_0),
        .R(ERROR_i_1_n_0));
  LUT4 #(
    .INIT(16'hFF80)) 
    reads_done_i_1
       (.I0(m00_axi_rready),
        .I1(m00_axi_rvalid),
        .I2(last_read),
        .I3(reads_done),
        .O(reads_done_i_1_n_0));
  FDRE reads_done_reg
       (.C(m00_axi_aclk),
        .CE(1'b1),
        .D(reads_done_i_1_n_0),
        .Q(reads_done),
        .R(axi_awvalid_i_1_n_0));
  LUT6 #(
    .INIT(64'hFFFEFFFF00500000)) 
    start_single_read_i_1
       (.I0(mst_exec_state[0]),
        .I1(m00_axi_rready),
        .I2(start_single_read0),
        .I3(reads_done),
        .I4(mst_exec_state[1]),
        .I5(start_single_read_reg_n_0),
        .O(start_single_read_i_1_n_0));
  LUT5 #(
    .INIT(32'h00000001)) 
    start_single_read_i_2
       (.I0(last_read),
        .I1(m00_axi_rvalid),
        .I2(read_issued_reg_n_0),
        .I3(start_single_read_reg_n_0),
        .I4(m00_axi_arvalid),
        .O(start_single_read0));
  FDRE start_single_read_reg
       (.C(m00_axi_aclk),
        .CE(1'b1),
        .D(start_single_read_i_1_n_0),
        .Q(start_single_read_reg_n_0),
        .R(ERROR_i_1_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFB00004400)) 
    start_single_write_i_1
       (.I0(mst_exec_state[1]),
        .I1(mst_exec_state[0]),
        .I2(m00_axi_bready),
        .I3(start_single_write0),
        .I4(writes_done),
        .I5(start_single_write_reg_n_0),
        .O(start_single_write_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    start_single_write_i_2
       (.I0(write_issued_reg_n_0),
        .I1(m00_axi_awvalid),
        .I2(m00_axi_bvalid),
        .I3(m00_axi_wvalid),
        .I4(start_single_write_reg_n_0),
        .I5(last_write),
        .O(start_single_write0));
  FDRE start_single_write_reg
       (.C(m00_axi_aclk),
        .CE(1'b1),
        .D(start_single_write_i_1_n_0),
        .Q(start_single_write_reg_n_0),
        .R(ERROR_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \write_index[0]_i_1 
       (.I0(start_single_write_reg_n_0),
        .I1(write_index[0]),
        .O(\write_index[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \write_index[1]_i_1 
       (.I0(write_index[0]),
        .I1(start_single_write_reg_n_0),
        .I2(write_index[1]),
        .O(\write_index[1]_i_1_n_0 ));
  FDRE \write_index_reg[0] 
       (.C(m00_axi_aclk),
        .CE(1'b1),
        .D(\write_index[0]_i_1_n_0 ),
        .Q(write_index[0]),
        .R(axi_awvalid_i_1_n_0));
  FDRE \write_index_reg[1] 
       (.C(m00_axi_aclk),
        .CE(1'b1),
        .D(\write_index[1]_i_1_n_0 ),
        .Q(write_index[1]),
        .R(axi_awvalid_i_1_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFBF00004400)) 
    write_issued_i_1
       (.I0(mst_exec_state[1]),
        .I1(mst_exec_state[0]),
        .I2(m00_axi_bready),
        .I3(start_single_write0),
        .I4(writes_done),
        .I5(write_issued_reg_n_0),
        .O(write_issued_i_1_n_0));
  FDRE write_issued_reg
       (.C(m00_axi_aclk),
        .CE(1'b1),
        .D(write_issued_i_1_n_0),
        .Q(write_issued_reg_n_0),
        .R(ERROR_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'hFF80)) 
    writes_done_i_1
       (.I0(m00_axi_bready),
        .I1(m00_axi_bvalid),
        .I2(last_write),
        .I3(writes_done),
        .O(writes_done_i_1_n_0));
  FDRE writes_done_reg
       (.C(m00_axi_aclk),
        .CE(1'b1),
        .D(writes_done_i_1_n_0),
        .Q(writes_done),
        .R(axi_awvalid_i_1_n_0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
