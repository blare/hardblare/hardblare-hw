-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
-- Date        : Fri Jun  4 10:30:45 2021
-- Host        : HardBlare-Workstation running 64-bit Ubuntu 16.04.6 LTS
-- Command     : write_vhdl -force -mode synth_stub
--               /home/mounir/HardBlare/hardblare-hw/tmc_project/tmc_project.srcs/sources_1/bd/design_1/ip/design_1_decodeur_traces_1_1/design_1_decodeur_traces_1_1_stub.vhdl
-- Design      : design_1_decodeur_traces_1_1
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z020clg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity design_1_decodeur_traces_1_1 is
  Port ( 
    clk : in STD_LOGIC;
    reset : in STD_LOGIC;
    enable : in STD_LOGIC;
    trace_data : in STD_LOGIC_VECTOR ( 7 downto 0 );
    pc : out STD_LOGIC_VECTOR ( 31 downto 0 );
    w_en : out STD_LOGIC;
    w_ctxt_en : out STD_LOGIC;
    fifo_overflow : out STD_LOGIC;
    waypoint_address : out STD_LOGIC_VECTOR ( 31 downto 0 );
    context_id : out STD_LOGIC_VECTOR ( 31 downto 0 );
    waypoint_address_en : out STD_LOGIC
  );

end design_1_decodeur_traces_1_1;

architecture stub of design_1_decodeur_traces_1_1 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "clk,reset,enable,trace_data[7:0],pc[31:0],w_en,w_ctxt_en,fifo_overflow,waypoint_address[31:0],context_id[31:0],waypoint_address_en";
attribute x_core_info : string;
attribute x_core_info of stub : architecture is "decodeur_traces,Vivado 2018.2";
begin
end;
