library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.All;

entity decode_i_sync is
    port(
    clk, reset, start_i, enable : in std_logic;
    data_in : in std_logic_vector(7 downto 0);
    ctxtid_bits : in std_logic_vector(1 downto 0);
    data_out : out std_logic_vector(31 downto 0);
    context_id_out : out std_logic_vector(31 downto 0);    
    stop_i, out_en, out_ctxt_en, fifo_overflow : out std_logic
    );
end entity;

architecture archi_comportementale of decode_i_sync is
    type state_type is (wait_state, i_sync_count_01, i_sync_count_02, i_sync_count_03, i_sync_count_04,i_sync_ib, ctxtid_1, ctxtid_2, ctxtid_3, wait_enable);
    signal state_reg, state_next, temp_state   : state_type := wait_state;
    signal data_reg_s : std_logic_vector(7 downto 0);

    signal output_data_0 : std_logic_vector(7 downto 0);
    signal output_data_1 : std_logic_vector(7 downto 0);
    signal output_data_2 : std_logic_vector(7 downto 0);
    signal output_data_3 : std_logic_vector(7 downto 0);
    signal output_data_4 : std_logic_vector(7 downto 0); -- context_id
    signal output_data_5 : std_logic_vector(7 downto 0); -- context_id
    signal output_data_6 : std_logic_vector(7 downto 0); -- context_id
    signal output_data_7 : std_logic_vector(7 downto 0); -- context_id
    signal en_0, en_1, en_2, en_3, en_4, en_5, en_6, en_7 : std_logic;
    signal enable_counter : std_logic := '0';
    signal count_ctxt : std_logic_vector(1 downto 0);

    begin

        -----------------------------------------
        -----------------------------------------
        ----- N E X T  S T A T E  L O G I C -----
        -----------------------------------------
        -----------------------------------------
		process(clk)
        begin
            if (clk'event and clk='1') then -- condition better than rising_edge(clk)
                if reset = '1' then
                    state_reg <= wait_state;          
                elsif (enable = '1') then
                    state_reg <= state_next;
                else
                    state_reg <= wait_enable;
                end if;
            end if;
        end process;
    
--        data_reg_s <= data_in;
        process(clk)
        begin
            if (clk'event and clk = '1') then
                if reset = '1' then 
                    data_reg_s <= (others => '0');
                 else
                    data_reg_s <= data_in;
                end if;
            end if;
        end process;
    
        
    process(clk)
    begin
        if (clk'event and clk = '1') then 
            if reset = '1' then 
                temp_state <= wait_state; 
            elsif enable = '0' then 
                temp_state <= state_next; 
            end if; 
        end if; 
    end process;

    process(state_reg, data_reg_s, start_i, temp_state, ctxtid_bits)
    begin
        fifo_overflow <= '0';
        out_en <= '0';
        out_ctxt_en <= '0';
        stop_i <= '0';
        en_0 <= '0';
        en_1 <= '0';
        en_2 <= '0';
        en_3 <= '0';
        en_4 <= '0';
        en_5 <= '0';
        en_6 <= '0';
        en_7 <= '0';
        enable_counter <= '0';
        case state_reg is            
            when i_sync_count_01 =>
            en_0 <= '1';
            en_1 <= '0';
            en_2 <= '0';
            en_3 <= '0';
            out_en <= '0';
            stop_i <= '0';
            state_next <= i_sync_count_02;

            when i_sync_count_02 =>
            en_0 <= '0';
            en_1 <= '1';
            en_2 <= '0';
            en_3 <= '0';
            out_en <= '0';
            stop_i <= '0';
            state_next <= i_sync_count_03;

            when i_sync_count_03 =>
            en_0 <= '0';
            en_1 <= '0';
            en_2 <= '1';
            en_3 <= '0';
            out_en <= '0';
            stop_i <= '0';
            state_next <= i_sync_count_04;

            when i_sync_count_04 =>
            en_0 <= '0';
            en_1 <= '0';
            en_2 <= '0';
            en_3 <= '1';
            out_en <= '1';
            stop_i <= '0';
            state_next <= i_sync_ib;

            when i_sync_ib =>
            en_0 <= '0';
            en_1 <= '0';
            en_2 <= '0';
            en_3 <= '0';
            out_en <= '0';
            stop_i <= '0';
            case ctxtid_bits is 
            when "00" =>  
                state_next <= wait_state; 
            when "01" =>
                state_next <= ctxtid_1;
            when "10" => 
                state_next <= ctxtid_2;
            when "11" =>
                state_next <= ctxtid_3;
            when others => 
                state_next <= wait_state;
            end case;

            when wait_state =>
            en_0 <= '0';
            en_1 <= '0';
            en_2 <= '0';
            en_3 <= '0';
            stop_i <= '1';
            if start_i = '1' then --
                state_next <= i_sync_count_01;                
            else
                state_next <= wait_state;
            end if;

            when wait_enable =>
            en_0 <= '0';
            en_1 <= '0';
            en_2 <= '0';
            en_3 <= '0';
            stop_i <= '0';
            fifo_overflow <= '0';
            out_en <= '0';
            state_next <= temp_state;
            
            when ctxtid_1  =>           
            en_4 <= '1';
            state_next <= wait_state;
            
            when ctxtid_2  =>           
            en_5 <= '1';
            enable_counter <= '1';  
            if count_ctxt = "01" then  
                state_next <= wait_state;
            else  
                state_next <= ctxtid_2;
            end if;
            
            when ctxtid_3  =>                 
            enable_counter <= '1';  
            case count_ctxt is
            when "00" =>
                en_4 <= '1';
                state_next <= ctxtid_3;
            when "01" => 
                en_5 <= '1';
                state_next <= ctxtid_3;
            when "10" =>             
                en_6 <= '1';
                state_next <= ctxtid_3;
            when "11" => 
                out_ctxt_en <= '1';
                en_7 <= '1';           
                state_next <= wait_state;     
            when others =>
                --output_data_4 <= output_data_4;     
            end case;
            
        end case;
    end process;      

    process(clk)
    begin
        if rising_edge(clk) then 
            if reset = '1' then 
                count_ctxt <= "00"; 
            elsif enable_counter = '1' then 
                count_ctxt <= std_logic_vector(unsigned(count_ctxt) + 1);
            end if;
        end if;
    end process;


    process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then 
                output_data_0 <= (others => '0'); 
            elsif (en_0 = '1') then
                output_data_0 <= data_reg_s;
            end if;
        end if;
    end process;

    process(clk)
    begin    
        if rising_edge(clk) then
            if reset = '1' then 
                output_data_1 <= (others => '0'); 
            elsif (en_1 = '1') then
                output_data_1 <= data_reg_s;
            end if;
        end if;
    end process;

    process(clk)
    begin            
        if rising_edge(clk) then
            if reset = '1' then 
                output_data_2 <= (others => '0'); 
            elsif (en_2 = '1') then
                output_data_2 <= data_reg_s;
            end if;
        end if;
    end process;

    process(clk)
    begin            
        if rising_edge(clk) then
            if reset = '1' then 
                output_data_3 <= (others => '0'); 
            elsif (en_3 = '1') then
                output_data_3 <= data_reg_s;
            end if;
        end if;
    end process;

    
    process(clk)
    begin            
        if rising_edge(clk) then
            if reset = '1' then 
                output_data_4 <= (others => '0'); 
            elsif (en_4 = '1') then
                output_data_4 <= data_reg_s;
            end if;
        end if;
    end process;
    
    process(clk)
    begin            
        if rising_edge(clk) then
            if reset = '1' then 
                output_data_5 <= (others => '0'); 
            elsif (en_5 = '1') then
                output_data_5 <= data_reg_s;                
            end if;
        end if;
    end process;
    
    process(clk)
    begin            
        if rising_edge(clk) then
            if reset = '1' then 
                output_data_6 <= (others => '0');                 
            elsif (en_6 = '1') then
                output_data_6 <= data_reg_s;
            end if;
        end if;
    end process;

    process(clk)
    begin            
        if rising_edge(clk) then
            if reset = '1' then 
                output_data_7 <= (others => '0');                 
            elsif (en_7 = '1') then
                output_data_7 <= data_reg_s;
            end if;
        end if;
    end process;

    data_out <= output_data_3 & output_data_2 & output_data_1 & output_data_0;
    context_id_out <= output_data_7 & output_data_6 & output_data_5 & output_data_4;
end architecture;
