proc start_step { step } {
  set stopFile ".stop.rst"
  if {[file isfile .stop.rst]} {
    puts ""
    puts "*** Halting run - EA reset detected ***"
    puts ""
    puts ""
    return -code error
  }
  set beginFile ".$step.begin.rst"
  set platform "$::tcl_platform(platform)"
  set user "$::tcl_platform(user)"
  set pid [pid]
  set host ""
  if { [string equal $platform unix] } {
    if { [info exist ::env(HOSTNAME)] } {
      set host $::env(HOSTNAME)
    }
  } else {
    if { [info exist ::env(COMPUTERNAME)] } {
      set host $::env(COMPUTERNAME)
    }
  }
  set ch [open $beginFile w]
  puts $ch "<?xml version=\"1.0\"?>"
  puts $ch "<ProcessHandle Version=\"1\" Minor=\"0\">"
  puts $ch "    <Process Command=\".planAhead.\" Owner=\"$user\" Host=\"$host\" Pid=\"$pid\">"
  puts $ch "    </Process>"
  puts $ch "</ProcessHandle>"
  close $ch
}

proc end_step { step } {
  set endFile ".$step.end.rst"
  set ch [open $endFile w]
  close $ch
}

proc step_failed { step } {
  set endFile ".$step.error.rst"
  set ch [open $endFile w]
  close $ch
}

set_msg_config -id {HDL 9-1061} -limit 100000
set_msg_config -id {HDL 9-1654} -limit 100000

start_step init_design
set rc [catch {
  create_msg_db init_design.pb
  set_param gui.test TreeTableDev
  debug::add_scope template.lib 1
  set_property design_mode GateLvl [current_fileset]
  set_property webtalk.parent_dir e:/vivado/test/mem_ctl/mem_ctl_v1_9_2_project/mem_ctl_v1_9_2_project.cache/wt [current_project]
  set_property parent.project_path e:/vivado/test/mem_ctl/mem_ctl_v1_9_2_project/mem_ctl_v1_9_2_project.xpr [current_project]
  set_property ip_repo_paths {
  e:/vivado/test/mem_ctl/mem_ctl_v1_9_2_project/mem_ctl_v1_9_2_project.cache/ip
  c:/Users/wahab_muh.RPHPZB1401/Documents/these/these_05_06_2016/decodeur_traces
  E:/Vivado/test/mem_ctl
  E:/Vivado/composants/ip_repo/axi_config_1.0
} [current_project]
  set_property ip_output_repo e:/vivado/test/mem_ctl/mem_ctl_v1_9_2_project/mem_ctl_v1_9_2_project.cache/ip [current_project]
  add_files -quiet e:/vivado/test/mem_ctl/mem_ctl_v1_9_2_project/mem_ctl_v1_9_2_project.runs/synth_1/mem_ctl.dcp
  link_design -top mem_ctl -part xc7z020clg484-1
  close_msg_db -file init_design.pb
} RESULT]
if {$rc} {
  step_failed init_design
  return -code error $RESULT
} else {
  end_step init_design
}

start_step opt_design
set rc [catch {
  create_msg_db opt_design.pb
  catch {write_debug_probes -quiet -force debug_nets}
  opt_design 
  write_checkpoint -force mem_ctl_opt.dcp
  catch {report_drc -file mem_ctl_drc_opted.rpt}
  close_msg_db -file opt_design.pb
} RESULT]
if {$rc} {
  step_failed opt_design
  return -code error $RESULT
} else {
  end_step opt_design
}

start_step place_design
set rc [catch {
  create_msg_db place_design.pb
  place_design -directive LateBlockPlacement
  write_checkpoint -force mem_ctl_placed.dcp
  catch { report_io -file mem_ctl_io_placed.rpt }
  catch { report_clock_utilization -file mem_ctl_clock_utilization_placed.rpt }
  catch { report_utilization -file mem_ctl_utilization_placed.rpt -pb mem_ctl_utilization_placed.pb }
  catch { report_control_sets -verbose -file mem_ctl_control_sets_placed.rpt }
  close_msg_db -file place_design.pb
} RESULT]
if {$rc} {
  step_failed place_design
  return -code error $RESULT
} else {
  end_step place_design
}

start_step phys_opt_design
set rc [catch {
  create_msg_db phys_opt_design.pb
  phys_opt_design -directive AlternateFlowWithRetiming
  write_checkpoint -force mem_ctl_physopt.dcp
  close_msg_db -file phys_opt_design.pb
} RESULT]
if {$rc} {
  step_failed phys_opt_design
  return -code error $RESULT
} else {
  end_step phys_opt_design
}

  set_msg_config -id {Route 35-39} -severity "critical warning" -new_severity warning
start_step route_design
set rc [catch {
  create_msg_db route_design.pb
  route_design -directive MoreGlobalIterations
  write_checkpoint -force mem_ctl_routed.dcp
  catch { report_drc -file mem_ctl_drc_routed.rpt -pb mem_ctl_drc_routed.pb }
  catch { report_timing_summary -max_paths 10 -file mem_ctl_timing_summary_routed.rpt -rpx mem_ctl_timing_summary_routed.rpx }
  catch { report_power -file mem_ctl_power_routed.rpt -pb mem_ctl_power_summary_routed.pb }
  catch { report_route_status -file mem_ctl_route_status.rpt -pb mem_ctl_route_status.pb }
  close_msg_db -file route_design.pb
} RESULT]
if {$rc} {
  step_failed route_design
  return -code error $RESULT
} else {
  end_step route_design
}

start_step post_route_phys_opt_design
set rc [catch {
  create_msg_db post_route_phys_opt_design.pb
  phys_opt_design -directive AggressiveExplore
  write_checkpoint -force mem_ctl_postroute_physopt.dcp
  catch { report_timing_summary -warn_on_violation -max_paths 10 -file mem_ctl_timing_summary_postroute_physopted.rpt -rpx mem_ctl_timing_summary_postroute_physopted.rpx }
  close_msg_db -file post_route_phys_opt_design.pb
} RESULT]
if {$rc} {
  step_failed post_route_phys_opt_design
  return -code error $RESULT
} else {
  end_step post_route_phys_opt_design
}

