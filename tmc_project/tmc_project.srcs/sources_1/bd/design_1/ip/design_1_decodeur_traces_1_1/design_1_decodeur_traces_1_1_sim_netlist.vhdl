-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
-- Date        : Fri Jun  4 10:30:45 2021
-- Host        : HardBlare-Workstation running 64-bit Ubuntu 16.04.6 LTS
-- Command     : write_vhdl -force -mode funcsim
--               /home/mounir/HardBlare/hardblare-hw/tmc_project/tmc_project.srcs/sources_1/bd/design_1/ip/design_1_decodeur_traces_1_1/design_1_decodeur_traces_1_1_sim_netlist.vhdl
-- Design      : design_1_decodeur_traces_1_1
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_decodeur_traces_1_1_address_compute is
  port (
    D : out STD_LOGIC_VECTOR ( 0 to 0 );
    \output_data_0_reg[5]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    pc : out STD_LOGIC_VECTOR ( 31 downto 0 );
    \pc_s_reg_reg[22]_0\ : out STD_LOGIC;
    \output_data_3_reg[6]\ : out STD_LOGIC;
    \pc_s_reg_reg[21]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    enable_reg_reg_reg : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \pc_reg[31]\ : in STD_LOGIC_VECTOR ( 31 downto 0 );
    out_en_reg : in STD_LOGIC;
    out_en_reg_0 : in STD_LOGIC;
    out_en_b_i : in STD_LOGIC;
    reset : in STD_LOGIC;
    waypoint_address : in STD_LOGIC_VECTOR ( 22 downto 0 );
    clk : in STD_LOGIC;
    en_0_reg_reg : in STD_LOGIC_VECTOR ( 4 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_decodeur_traces_1_1_address_compute : entity is "address_compute";
end design_1_decodeur_traces_1_1_address_compute;

architecture STRUCTURE of design_1_decodeur_traces_1_1_address_compute is
  signal \output_data_0[5]_i_3_n_0\ : STD_LOGIC;
  signal \output_data_1[6]_i_3__0_n_0\ : STD_LOGIC;
  signal \^pc\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal pc_s : STD_LOGIC_VECTOR ( 28 downto 7 );
  signal pc_s_0 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \pc_s_reg[14]_i_2__0_n_0\ : STD_LOGIC;
  signal \pc_s_reg[14]_i_3__0_n_0\ : STD_LOGIC;
  signal \pc_s_reg[21]_i_2__0_n_0\ : STD_LOGIC;
  signal \pc_s_reg[21]_i_3__0_n_0\ : STD_LOGIC;
  signal \pc_s_reg[28]_i_3__0_n_0\ : STD_LOGIC;
  signal \pc_s_reg[28]_i_4__0_n_0\ : STD_LOGIC;
  signal \pc_s_reg[28]_i_5_n_0\ : STD_LOGIC;
  signal \pc_s_reg[31]_i_10_n_0\ : STD_LOGIC;
  signal \pc_s_reg[31]_i_2__0_n_0\ : STD_LOGIC;
  signal \pc_s_reg[31]_i_3_n_0\ : STD_LOGIC;
  signal \pc_s_reg[31]_i_4_n_0\ : STD_LOGIC;
  signal \pc_s_reg[31]_i_5_n_0\ : STD_LOGIC;
  signal \pc_s_reg[31]_i_6_n_0\ : STD_LOGIC;
  signal \pc_s_reg[31]_i_7_n_0\ : STD_LOGIC;
  signal \pc_s_reg[31]_i_8_n_0\ : STD_LOGIC;
  signal \pc_s_reg[31]_i_9_n_0\ : STD_LOGIC;
  signal \pc_s_reg[7]_i_2__0_n_0\ : STD_LOGIC;
  signal \pc_s_reg[7]_i_3_n_0\ : STD_LOGIC;
  signal \^pc_s_reg_reg[21]_0\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^pc_s_reg_reg[22]_0\ : STD_LOGIC;
  signal waypoint_address_cs_reg : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal waypoint_address_reg : STD_LOGIC_VECTOR ( 31 downto 2 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \output_data_1[6]_i_3__0\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \pc_s_reg[21]_i_3__0\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \pc_s_reg[28]_i_2__0\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \pc_s_reg[28]_i_3__0\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \pc_s_reg[28]_i_4__0\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \pc_s_reg[28]_i_5\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \pc_s_reg[31]_i_10\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \pc_s_reg[31]_i_2__0\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \pc_s_reg[31]_i_4\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \pc_s_reg[31]_i_5\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \pc_s_reg[31]_i_6\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \pc_s_reg[31]_i_8\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \pc_s_reg[7]_i_2__0\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \pc_s_reg[7]_i_3\ : label is "soft_lutpair6";
begin
  pc(31 downto 0) <= \^pc\(31 downto 0);
  \pc_s_reg_reg[21]_0\(0) <= \^pc_s_reg_reg[21]_0\(0);
  \pc_s_reg_reg[22]_0\ <= \^pc_s_reg_reg[22]_0\;
\output_data_0[5]_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F2F2F2F2F2D0D0D0"
    )
        port map (
      I0 => enable_reg_reg_reg,
      I1 => Q(1),
      I2 => Q(0),
      I3 => waypoint_address_reg(7),
      I4 => \pc_s_reg[7]_i_2__0_n_0\,
      I5 => \output_data_0[5]_i_3_n_0\,
      O => \output_data_0_reg[5]\(0)
    );
\output_data_0[5]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => \^pc\(7),
      I1 => \pc_s_reg[31]_i_3_n_0\,
      I2 => \pc_reg[31]\(7),
      I3 => \pc_s_reg[7]_i_3_n_0\,
      O => \output_data_0[5]_i_3_n_0\
    );
\output_data_1[6]_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F2F2F2F2F2D0D0D0"
    )
        port map (
      I0 => enable_reg_reg_reg,
      I1 => Q(1),
      I2 => Q(0),
      I3 => waypoint_address_reg(14),
      I4 => \pc_s_reg[14]_i_2__0_n_0\,
      I5 => \output_data_1[6]_i_3__0_n_0\,
      O => D(0)
    );
\output_data_1[6]_i_3__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => \^pc\(14),
      I1 => \pc_s_reg[31]_i_3_n_0\,
      I2 => \pc_reg[31]\(14),
      I3 => \pc_s_reg[14]_i_3__0_n_0\,
      O => \output_data_1[6]_i_3__0_n_0\
    );
\output_data_3[6]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => waypoint_address_reg(28),
      I1 => \pc_s_reg[28]_i_3__0_n_0\,
      I2 => \^pc\(28),
      I3 => \pc_s_reg[31]_i_3_n_0\,
      O => \output_data_3_reg[6]\
    );
\pc_s_reg[0]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => \^pc\(0),
      I1 => \pc_s_reg[31]_i_3_n_0\,
      I2 => \pc_reg[31]\(0),
      I3 => \pc_s_reg[7]_i_3_n_0\,
      O => pc_s_0(0)
    );
\pc_s_reg[10]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \pc_s_reg[14]_i_2__0_n_0\,
      I1 => waypoint_address_reg(10),
      I2 => \pc_s_reg[14]_i_3__0_n_0\,
      I3 => \pc_reg[31]\(10),
      I4 => \^pc\(10),
      I5 => \pc_s_reg[31]_i_3_n_0\,
      O => pc_s_0(10)
    );
\pc_s_reg[11]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \pc_s_reg[14]_i_2__0_n_0\,
      I1 => waypoint_address_reg(11),
      I2 => \pc_s_reg[14]_i_3__0_n_0\,
      I3 => \pc_reg[31]\(11),
      I4 => \^pc\(11),
      I5 => \pc_s_reg[31]_i_3_n_0\,
      O => pc_s_0(11)
    );
\pc_s_reg[12]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \pc_s_reg[14]_i_2__0_n_0\,
      I1 => waypoint_address_reg(12),
      I2 => \pc_s_reg[14]_i_3__0_n_0\,
      I3 => \pc_reg[31]\(12),
      I4 => \^pc\(12),
      I5 => \pc_s_reg[31]_i_3_n_0\,
      O => pc_s_0(12)
    );
\pc_s_reg[13]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \pc_s_reg[14]_i_2__0_n_0\,
      I1 => waypoint_address_reg(13),
      I2 => \pc_s_reg[14]_i_3__0_n_0\,
      I3 => \pc_reg[31]\(13),
      I4 => \^pc\(13),
      I5 => \pc_s_reg[31]_i_3_n_0\,
      O => pc_s_0(13)
    );
\pc_s_reg[14]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => waypoint_address_reg(14),
      I1 => \pc_s_reg[14]_i_2__0_n_0\,
      I2 => \pc_s_reg[14]_i_3__0_n_0\,
      I3 => \pc_reg[31]\(14),
      I4 => \pc_s_reg[31]_i_3_n_0\,
      I5 => \^pc\(14),
      O => pc_s(14)
    );
\pc_s_reg[14]_i_2__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \pc_s_reg[31]_i_2__0_n_0\,
      I1 => \pc_s_reg[31]_i_9_n_0\,
      O => \pc_s_reg[14]_i_2__0_n_0\
    );
\pc_s_reg[14]_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000200000100"
    )
        port map (
      I0 => waypoint_address_cs_reg(4),
      I1 => waypoint_address_cs_reg(0),
      I2 => \pc_s_reg[21]_i_3__0_n_0\,
      I3 => out_en_b_i,
      I4 => \^pc_s_reg_reg[21]_0\(0),
      I5 => out_en_reg_0,
      O => \pc_s_reg[14]_i_3__0_n_0\
    );
\pc_s_reg[15]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => \^pc\(15),
      I1 => \pc_s_reg[31]_i_3_n_0\,
      I2 => \pc_reg[31]\(15),
      I3 => \pc_s_reg[21]_i_2__0_n_0\,
      O => pc_s_0(15)
    );
\pc_s_reg[16]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => \^pc\(16),
      I1 => \pc_s_reg[31]_i_3_n_0\,
      I2 => \pc_reg[31]\(16),
      I3 => \pc_s_reg[21]_i_2__0_n_0\,
      O => pc_s_0(16)
    );
\pc_s_reg[17]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => \^pc\(17),
      I1 => \pc_s_reg[31]_i_3_n_0\,
      I2 => \pc_reg[31]\(17),
      I3 => \pc_s_reg[21]_i_2__0_n_0\,
      O => pc_s_0(17)
    );
\pc_s_reg[18]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => \^pc\(18),
      I1 => \pc_s_reg[31]_i_3_n_0\,
      I2 => \pc_reg[31]\(18),
      I3 => \pc_s_reg[21]_i_2__0_n_0\,
      O => pc_s_0(18)
    );
\pc_s_reg[19]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => \^pc\(19),
      I1 => \pc_s_reg[31]_i_3_n_0\,
      I2 => \pc_reg[31]\(19),
      I3 => \pc_s_reg[21]_i_2__0_n_0\,
      O => pc_s_0(19)
    );
\pc_s_reg[1]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => \^pc\(1),
      I1 => \pc_s_reg[31]_i_3_n_0\,
      I2 => \pc_reg[31]\(1),
      I3 => \pc_s_reg[7]_i_3_n_0\,
      O => pc_s_0(1)
    );
\pc_s_reg[20]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => \^pc\(20),
      I1 => \pc_s_reg[31]_i_3_n_0\,
      I2 => \pc_reg[31]\(20),
      I3 => \pc_s_reg[21]_i_2__0_n_0\,
      O => pc_s_0(20)
    );
\pc_s_reg[21]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => \^pc\(21),
      I1 => \pc_s_reg[31]_i_3_n_0\,
      I2 => \pc_reg[31]\(21),
      I3 => \pc_s_reg[21]_i_2__0_n_0\,
      O => pc_s(21)
    );
\pc_s_reg[21]_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000200000018"
    )
        port map (
      I0 => out_en_reg_0,
      I1 => \^pc_s_reg_reg[21]_0\(0),
      I2 => out_en_b_i,
      I3 => \pc_s_reg[21]_i_3__0_n_0\,
      I4 => waypoint_address_cs_reg(0),
      I5 => waypoint_address_cs_reg(4),
      O => \pc_s_reg[21]_i_2__0_n_0\
    );
\pc_s_reg[21]_i_3__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => waypoint_address_cs_reg(1),
      I1 => waypoint_address_cs_reg(2),
      O => \pc_s_reg[21]_i_3__0_n_0\
    );
\pc_s_reg[22]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \pc_s_reg[28]_i_3__0_n_0\,
      I1 => waypoint_address_reg(22),
      I2 => \pc_s_reg[31]_i_3_n_0\,
      I3 => \^pc\(22),
      I4 => \pc_reg[31]\(22),
      I5 => \^pc_s_reg_reg[22]_0\,
      O => pc_s_0(22)
    );
\pc_s_reg[23]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \pc_s_reg[28]_i_3__0_n_0\,
      I1 => waypoint_address_reg(23),
      I2 => \pc_s_reg[31]_i_3_n_0\,
      I3 => \^pc\(23),
      I4 => \pc_reg[31]\(23),
      I5 => \^pc_s_reg_reg[22]_0\,
      O => pc_s_0(23)
    );
\pc_s_reg[24]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \pc_s_reg[28]_i_3__0_n_0\,
      I1 => waypoint_address_reg(24),
      I2 => \pc_s_reg[31]_i_3_n_0\,
      I3 => \^pc\(24),
      I4 => \pc_reg[31]\(24),
      I5 => \^pc_s_reg_reg[22]_0\,
      O => pc_s_0(24)
    );
\pc_s_reg[25]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \pc_s_reg[28]_i_3__0_n_0\,
      I1 => waypoint_address_reg(25),
      I2 => \pc_s_reg[31]_i_3_n_0\,
      I3 => \^pc\(25),
      I4 => \pc_reg[31]\(25),
      I5 => \^pc_s_reg_reg[22]_0\,
      O => pc_s_0(25)
    );
\pc_s_reg[26]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \pc_s_reg[28]_i_3__0_n_0\,
      I1 => waypoint_address_reg(26),
      I2 => \pc_s_reg[31]_i_3_n_0\,
      I3 => \^pc\(26),
      I4 => \pc_reg[31]\(26),
      I5 => \^pc_s_reg_reg[22]_0\,
      O => pc_s_0(26)
    );
\pc_s_reg[27]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \pc_s_reg[28]_i_3__0_n_0\,
      I1 => waypoint_address_reg(27),
      I2 => \pc_s_reg[31]_i_3_n_0\,
      I3 => \^pc\(27),
      I4 => \pc_reg[31]\(27),
      I5 => \^pc_s_reg_reg[22]_0\,
      O => pc_s_0(27)
    );
\pc_s_reg[28]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \pc_reg[31]\(28),
      I1 => \^pc_s_reg_reg[22]_0\,
      I2 => \pc_s_reg[31]_i_3_n_0\,
      I3 => \^pc\(28),
      I4 => \pc_s_reg[28]_i_3__0_n_0\,
      I5 => waypoint_address_reg(28),
      O => pc_s(28)
    );
\pc_s_reg[28]_i_2__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFBAAA"
    )
        port map (
      I0 => \pc_s_reg[28]_i_4__0_n_0\,
      I1 => waypoint_address_cs_reg(1),
      I2 => waypoint_address_cs_reg(2),
      I3 => \pc_s_reg[28]_i_5_n_0\,
      I4 => \pc_s_reg[14]_i_3__0_n_0\,
      O => \^pc_s_reg_reg[22]_0\
    );
\pc_s_reg[28]_i_3__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000220"
    )
        port map (
      I0 => out_en_reg,
      I1 => waypoint_address_cs_reg(2),
      I2 => waypoint_address_cs_reg(1),
      I3 => waypoint_address_cs_reg(0),
      I4 => waypoint_address_cs_reg(4),
      O => \pc_s_reg[28]_i_3__0_n_0\
    );
\pc_s_reg[28]_i_4__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1000"
    )
        port map (
      I0 => out_en_b_i,
      I1 => \pc_s_reg[31]_i_8_n_0\,
      I2 => \^pc_s_reg_reg[21]_0\(0),
      I3 => out_en_reg_0,
      O => \pc_s_reg[28]_i_4__0_n_0\
    );
\pc_s_reg[28]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000010"
    )
        port map (
      I0 => \^pc_s_reg_reg[21]_0\(0),
      I1 => out_en_b_i,
      I2 => out_en_reg_0,
      I3 => waypoint_address_cs_reg(4),
      I4 => waypoint_address_cs_reg(0),
      O => \pc_s_reg[28]_i_5_n_0\
    );
\pc_s_reg[29]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \pc_s_reg[31]_i_2__0_n_0\,
      I1 => waypoint_address_reg(29),
      I2 => \pc_s_reg[31]_i_3_n_0\,
      I3 => \^pc\(29),
      I4 => \pc_reg[31]\(29),
      I5 => \pc_s_reg[31]_i_4_n_0\,
      O => pc_s_0(29)
    );
\pc_s_reg[2]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \pc_s_reg[7]_i_2__0_n_0\,
      I1 => waypoint_address_reg(2),
      I2 => \pc_s_reg[7]_i_3_n_0\,
      I3 => \pc_reg[31]\(2),
      I4 => \^pc\(2),
      I5 => \pc_s_reg[31]_i_3_n_0\,
      O => pc_s_0(2)
    );
\pc_s_reg[30]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \pc_s_reg[31]_i_2__0_n_0\,
      I1 => waypoint_address_reg(30),
      I2 => \pc_s_reg[31]_i_3_n_0\,
      I3 => \^pc\(30),
      I4 => \pc_reg[31]\(30),
      I5 => \pc_s_reg[31]_i_4_n_0\,
      O => pc_s_0(30)
    );
\pc_s_reg[31]_i_10\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => waypoint_address_cs_reg(0),
      I1 => waypoint_address_cs_reg(4),
      O => \pc_s_reg[31]_i_10_n_0\
    );
\pc_s_reg[31]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \pc_s_reg[31]_i_2__0_n_0\,
      I1 => waypoint_address_reg(31),
      I2 => \pc_s_reg[31]_i_3_n_0\,
      I3 => \^pc\(31),
      I4 => \pc_reg[31]\(31),
      I5 => \pc_s_reg[31]_i_4_n_0\,
      O => pc_s_0(31)
    );
\pc_s_reg[31]_i_2__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => waypoint_address_cs_reg(4),
      I1 => waypoint_address_cs_reg(0),
      I2 => \pc_s_reg[31]_i_5_n_0\,
      O => \pc_s_reg[31]_i_2__0_n_0\
    );
\pc_s_reg[31]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFEEEEF"
    )
        port map (
      I0 => \pc_s_reg[31]_i_6_n_0\,
      I1 => \pc_s_reg[31]_i_7_n_0\,
      I2 => \^pc_s_reg_reg[21]_0\(0),
      I3 => out_en_b_i,
      I4 => \pc_s_reg[31]_i_8_n_0\,
      O => \pc_s_reg[31]_i_3_n_0\
    );
\pc_s_reg[31]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \pc_s_reg[31]_i_9_n_0\,
      I1 => \pc_s_reg[14]_i_3__0_n_0\,
      O => \pc_s_reg[31]_i_4_n_0\
    );
\pc_s_reg[31]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000010"
    )
        port map (
      I0 => \^pc_s_reg_reg[21]_0\(0),
      I1 => out_en_b_i,
      I2 => out_en_reg_0,
      I3 => waypoint_address_cs_reg(2),
      I4 => waypoint_address_cs_reg(1),
      O => \pc_s_reg[31]_i_5_n_0\
    );
\pc_s_reg[31]_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F8F8F888"
    )
        port map (
      I0 => waypoint_address_cs_reg(2),
      I1 => waypoint_address_cs_reg(1),
      I2 => out_en_b_i,
      I3 => out_en_reg_0,
      I4 => \^pc_s_reg_reg[21]_0\(0),
      O => \pc_s_reg[31]_i_6_n_0\
    );
\pc_s_reg[31]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFEEEFEEEF000F"
    )
        port map (
      I0 => waypoint_address_cs_reg(1),
      I1 => waypoint_address_cs_reg(2),
      I2 => out_en_reg_0,
      I3 => out_en_b_i,
      I4 => waypoint_address_cs_reg(0),
      I5 => waypoint_address_cs_reg(4),
      O => \pc_s_reg[31]_i_7_n_0\
    );
\pc_s_reg[31]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => waypoint_address_cs_reg(4),
      I1 => waypoint_address_cs_reg(0),
      I2 => waypoint_address_cs_reg(2),
      I3 => waypoint_address_cs_reg(1),
      O => \pc_s_reg[31]_i_8_n_0\
    );
\pc_s_reg[31]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000228"
    )
        port map (
      I0 => out_en_reg_0,
      I1 => \^pc_s_reg_reg[21]_0\(0),
      I2 => waypoint_address_cs_reg(1),
      I3 => waypoint_address_cs_reg(2),
      I4 => \pc_s_reg[31]_i_10_n_0\,
      I5 => out_en_b_i,
      O => \pc_s_reg[31]_i_9_n_0\
    );
\pc_s_reg[3]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \pc_s_reg[7]_i_2__0_n_0\,
      I1 => waypoint_address_reg(3),
      I2 => \pc_s_reg[7]_i_3_n_0\,
      I3 => \pc_reg[31]\(3),
      I4 => \^pc\(3),
      I5 => \pc_s_reg[31]_i_3_n_0\,
      O => pc_s_0(3)
    );
\pc_s_reg[4]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \pc_s_reg[7]_i_2__0_n_0\,
      I1 => waypoint_address_reg(4),
      I2 => \pc_s_reg[7]_i_3_n_0\,
      I3 => \pc_reg[31]\(4),
      I4 => \^pc\(4),
      I5 => \pc_s_reg[31]_i_3_n_0\,
      O => pc_s_0(4)
    );
\pc_s_reg[5]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \pc_s_reg[7]_i_2__0_n_0\,
      I1 => waypoint_address_reg(5),
      I2 => \pc_s_reg[7]_i_3_n_0\,
      I3 => \pc_reg[31]\(5),
      I4 => \^pc\(5),
      I5 => \pc_s_reg[31]_i_3_n_0\,
      O => pc_s_0(5)
    );
\pc_s_reg[6]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \pc_s_reg[7]_i_2__0_n_0\,
      I1 => waypoint_address_reg(6),
      I2 => \pc_s_reg[7]_i_3_n_0\,
      I3 => \pc_reg[31]\(6),
      I4 => \^pc\(6),
      I5 => \pc_s_reg[31]_i_3_n_0\,
      O => pc_s_0(6)
    );
\pc_s_reg[7]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => waypoint_address_reg(7),
      I1 => \pc_s_reg[7]_i_2__0_n_0\,
      I2 => \pc_s_reg[7]_i_3_n_0\,
      I3 => \pc_reg[31]\(7),
      I4 => \pc_s_reg[31]_i_3_n_0\,
      I5 => \^pc\(7),
      O => pc_s(7)
    );
\pc_s_reg[7]_i_2__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF28"
    )
        port map (
      I0 => \pc_s_reg[31]_i_5_n_0\,
      I1 => waypoint_address_cs_reg(0),
      I2 => waypoint_address_cs_reg(4),
      I3 => \pc_s_reg[31]_i_9_n_0\,
      O => \pc_s_reg[7]_i_2__0_n_0\
    );
\pc_s_reg[7]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0010"
    )
        port map (
      I0 => out_en_reg_0,
      I1 => \^pc_s_reg_reg[21]_0\(0),
      I2 => out_en_b_i,
      I3 => \pc_s_reg[31]_i_8_n_0\,
      O => \pc_s_reg[7]_i_3_n_0\
    );
\pc_s_reg[8]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \pc_s_reg[14]_i_2__0_n_0\,
      I1 => waypoint_address_reg(8),
      I2 => \pc_s_reg[14]_i_3__0_n_0\,
      I3 => \pc_reg[31]\(8),
      I4 => \^pc\(8),
      I5 => \pc_s_reg[31]_i_3_n_0\,
      O => pc_s_0(8)
    );
\pc_s_reg[9]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \pc_s_reg[14]_i_2__0_n_0\,
      I1 => waypoint_address_reg(9),
      I2 => \pc_s_reg[14]_i_3__0_n_0\,
      I3 => \pc_reg[31]\(9),
      I4 => \^pc\(9),
      I5 => \pc_s_reg[31]_i_3_n_0\,
      O => pc_s_0(9)
    );
\pc_s_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => pc_s_0(0),
      Q => \^pc\(0),
      R => reset
    );
\pc_s_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => pc_s_0(10),
      Q => \^pc\(10),
      R => reset
    );
\pc_s_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => pc_s_0(11),
      Q => \^pc\(11),
      R => reset
    );
\pc_s_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => pc_s_0(12),
      Q => \^pc\(12),
      R => reset
    );
\pc_s_reg_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => pc_s_0(13),
      Q => \^pc\(13),
      R => reset
    );
\pc_s_reg_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => pc_s(14),
      Q => \^pc\(14),
      R => reset
    );
\pc_s_reg_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => pc_s_0(15),
      Q => \^pc\(15),
      R => reset
    );
\pc_s_reg_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => pc_s_0(16),
      Q => \^pc\(16),
      R => reset
    );
\pc_s_reg_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => pc_s_0(17),
      Q => \^pc\(17),
      R => reset
    );
\pc_s_reg_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => pc_s_0(18),
      Q => \^pc\(18),
      R => reset
    );
\pc_s_reg_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => pc_s_0(19),
      Q => \^pc\(19),
      R => reset
    );
\pc_s_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => pc_s_0(1),
      Q => \^pc\(1),
      R => reset
    );
\pc_s_reg_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => pc_s_0(20),
      Q => \^pc\(20),
      R => reset
    );
\pc_s_reg_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => pc_s(21),
      Q => \^pc\(21),
      R => reset
    );
\pc_s_reg_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => pc_s_0(22),
      Q => \^pc\(22),
      R => reset
    );
\pc_s_reg_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => pc_s_0(23),
      Q => \^pc\(23),
      R => reset
    );
\pc_s_reg_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => pc_s_0(24),
      Q => \^pc\(24),
      R => reset
    );
\pc_s_reg_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => pc_s_0(25),
      Q => \^pc\(25),
      R => reset
    );
\pc_s_reg_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => pc_s_0(26),
      Q => \^pc\(26),
      R => reset
    );
\pc_s_reg_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => pc_s_0(27),
      Q => \^pc\(27),
      R => reset
    );
\pc_s_reg_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => pc_s(28),
      Q => \^pc\(28),
      R => reset
    );
\pc_s_reg_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => pc_s_0(29),
      Q => \^pc\(29),
      R => reset
    );
\pc_s_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => pc_s_0(2),
      Q => \^pc\(2),
      R => reset
    );
\pc_s_reg_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => pc_s_0(30),
      Q => \^pc\(30),
      R => reset
    );
\pc_s_reg_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => pc_s_0(31),
      Q => \^pc\(31),
      R => reset
    );
\pc_s_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => pc_s_0(3),
      Q => \^pc\(3),
      R => reset
    );
\pc_s_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => pc_s_0(4),
      Q => \^pc\(4),
      R => reset
    );
\pc_s_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => pc_s_0(5),
      Q => \^pc\(5),
      R => reset
    );
\pc_s_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => pc_s_0(6),
      Q => \^pc\(6),
      R => reset
    );
\pc_s_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => pc_s(7),
      Q => \^pc\(7),
      R => reset
    );
\pc_s_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => pc_s_0(8),
      Q => \^pc\(8),
      R => reset
    );
\pc_s_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => pc_s_0(9),
      Q => \^pc\(9),
      R => reset
    );
\waypoint_address_cs_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => en_0_reg_reg(0),
      Q => waypoint_address_cs_reg(0),
      R => reset
    );
\waypoint_address_cs_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => en_0_reg_reg(1),
      Q => waypoint_address_cs_reg(1),
      R => reset
    );
\waypoint_address_cs_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => en_0_reg_reg(2),
      Q => waypoint_address_cs_reg(2),
      R => reset
    );
\waypoint_address_cs_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => en_0_reg_reg(3),
      Q => \^pc_s_reg_reg[21]_0\(0),
      R => reset
    );
\waypoint_address_cs_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => en_0_reg_reg(4),
      Q => waypoint_address_cs_reg(4),
      R => reset
    );
\waypoint_address_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => waypoint_address(8),
      Q => waypoint_address_reg(10),
      R => reset
    );
\waypoint_address_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => waypoint_address(9),
      Q => waypoint_address_reg(11),
      R => reset
    );
\waypoint_address_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => waypoint_address(10),
      Q => waypoint_address_reg(12),
      R => reset
    );
\waypoint_address_reg_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => waypoint_address(11),
      Q => waypoint_address_reg(13),
      R => reset
    );
\waypoint_address_reg_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => waypoint_address(12),
      Q => waypoint_address_reg(14),
      R => reset
    );
\waypoint_address_reg_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => waypoint_address(13),
      Q => waypoint_address_reg(22),
      R => reset
    );
\waypoint_address_reg_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => waypoint_address(14),
      Q => waypoint_address_reg(23),
      R => reset
    );
\waypoint_address_reg_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => waypoint_address(15),
      Q => waypoint_address_reg(24),
      R => reset
    );
\waypoint_address_reg_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => waypoint_address(16),
      Q => waypoint_address_reg(25),
      R => reset
    );
\waypoint_address_reg_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => waypoint_address(17),
      Q => waypoint_address_reg(26),
      R => reset
    );
\waypoint_address_reg_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => waypoint_address(18),
      Q => waypoint_address_reg(27),
      R => reset
    );
\waypoint_address_reg_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => waypoint_address(19),
      Q => waypoint_address_reg(28),
      R => reset
    );
\waypoint_address_reg_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => waypoint_address(20),
      Q => waypoint_address_reg(29),
      R => reset
    );
\waypoint_address_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => waypoint_address(0),
      Q => waypoint_address_reg(2),
      R => reset
    );
\waypoint_address_reg_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => waypoint_address(21),
      Q => waypoint_address_reg(30),
      R => reset
    );
\waypoint_address_reg_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => waypoint_address(22),
      Q => waypoint_address_reg(31),
      R => reset
    );
\waypoint_address_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => waypoint_address(1),
      Q => waypoint_address_reg(3),
      R => reset
    );
\waypoint_address_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => waypoint_address(2),
      Q => waypoint_address_reg(4),
      R => reset
    );
\waypoint_address_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => waypoint_address(3),
      Q => waypoint_address_reg(5),
      R => reset
    );
\waypoint_address_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => waypoint_address(4),
      Q => waypoint_address_reg(6),
      R => reset
    );
\waypoint_address_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => waypoint_address(5),
      Q => waypoint_address_reg(7),
      R => reset
    );
\waypoint_address_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => waypoint_address(6),
      Q => waypoint_address_reg(8),
      R => reset
    );
\waypoint_address_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => waypoint_address(7),
      Q => waypoint_address_reg(9),
      R => reset
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_decodeur_traces_1_1_decode_bap is
  port (
    \out\ : out STD_LOGIC_VECTOR ( 4 downto 0 );
    out_en_b_i : out STD_LOGIC;
    \pc_reg[0]_0\ : out STD_LOGIC;
    \FSM_onehot_state_reg_reg[1]_0\ : out STD_LOGIC;
    output_data_0_s_reg_reg_0 : out STD_LOGIC;
    \state_reg_reg[0]\ : out STD_LOGIC;
    \FSM_onehot_state_reg_reg[0]_0\ : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 1 downto 0 );
    out_en_s : out STD_LOGIC;
    \FSM_onehot_state_reg_reg[2]_0\ : out STD_LOGIC;
    \pc_s_reg_reg[31]_0\ : out STD_LOGIC_VECTOR ( 31 downto 0 );
    reset : in STD_LOGIC;
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    clk : in STD_LOGIC;
    enable_reg_reg_reg : in STD_LOGIC_VECTOR ( 0 to 0 );
    enable_reg_reg_reg_0 : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 7 downto 0 );
    \state_reg_reg[1]\ : in STD_LOGIC;
    \state_reg_reg[1]_0\ : in STD_LOGIC;
    \FSM_onehot_state_reg_reg[7]_0\ : in STD_LOGIC;
    \state_reg_reg[2]\ : in STD_LOGIC;
    \data_reg_s_reg[7]\ : in STD_LOGIC;
    out_en_reg : in STD_LOGIC;
    \data_reg_s_reg[7]_0\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \data_reg_s_reg[6]\ : in STD_LOGIC;
    \output_data_3_reg[7]\ : in STD_LOGIC_VECTOR ( 31 downto 0 );
    \data_reg_s_reg[6]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \data_reg_s_reg[6]_1\ : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_decodeur_traces_1_1_decode_bap : entity is "decode_bap";
end design_1_decodeur_traces_1_1_decode_bap;

architecture STRUCTURE of design_1_decodeur_traces_1_1_decode_bap is
  signal \^d\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \FSM_onehot_state_reg[0]_i_1__0_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg[0]_i_2__0_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg[0]_i_6__0_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg[10]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg[11]_i_1__0_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg[12]_i_1__0_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg[13]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg[3]_i_1__0_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg[4]_i_1__0_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg[5]_i_1__0_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg[7]_i_1__0_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg[8]_i_1__1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg_reg_n_0_[10]\ : STD_LOGIC;
  attribute RTL_KEEP : string;
  attribute RTL_KEEP of \FSM_onehot_state_reg_reg_n_0_[10]\ : signal is "yes";
  signal \FSM_onehot_state_reg_reg_n_0_[11]\ : STD_LOGIC;
  attribute RTL_KEEP of \FSM_onehot_state_reg_reg_n_0_[11]\ : signal is "yes";
  signal \FSM_onehot_state_reg_reg_n_0_[12]\ : STD_LOGIC;
  attribute RTL_KEEP of \FSM_onehot_state_reg_reg_n_0_[12]\ : signal is "yes";
  signal \FSM_onehot_state_reg_reg_n_0_[13]\ : STD_LOGIC;
  attribute RTL_KEEP of \FSM_onehot_state_reg_reg_n_0_[13]\ : signal is "yes";
  signal \FSM_onehot_state_reg_reg_n_0_[1]\ : STD_LOGIC;
  attribute RTL_KEEP of \FSM_onehot_state_reg_reg_n_0_[1]\ : signal is "yes";
  signal \FSM_onehot_state_reg_reg_n_0_[3]\ : STD_LOGIC;
  attribute RTL_KEEP of \FSM_onehot_state_reg_reg_n_0_[3]\ : signal is "yes";
  signal \FSM_onehot_state_reg_reg_n_0_[4]\ : STD_LOGIC;
  attribute RTL_KEEP of \FSM_onehot_state_reg_reg_n_0_[4]\ : signal is "yes";
  signal \FSM_onehot_state_reg_reg_n_0_[8]\ : STD_LOGIC;
  attribute RTL_KEEP of \FSM_onehot_state_reg_reg_n_0_[8]\ : signal is "yes";
  signal \FSM_onehot_state_reg_reg_n_0_[9]\ : STD_LOGIC;
  attribute RTL_KEEP of \FSM_onehot_state_reg_reg_n_0_[9]\ : signal is "yes";
  signal data1 : STD_LOGIC_VECTOR ( 31 downto 2 );
  signal instruction_address_reg : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \^out\ : STD_LOGIC_VECTOR ( 4 downto 0 );
  attribute RTL_KEEP of \^out\ : signal is "yes";
  signal \^out_en_b_i\ : STD_LOGIC;
  signal output_data_0_s_reg : STD_LOGIC;
  signal \output_data_1[6]_i_1__0_n_0\ : STD_LOGIC;
  signal \output_data_1[6]_i_2_n_0\ : STD_LOGIC;
  signal \output_data_1[6]_i_3_n_0\ : STD_LOGIC;
  signal \output_data_1[6]_i_4_n_0\ : STD_LOGIC;
  signal output_data_1_s_reg : STD_LOGIC;
  signal \output_data_2[6]_i_1_n_0\ : STD_LOGIC;
  signal output_data_2_s_reg : STD_LOGIC;
  signal \output_data_3[6]_i_1__0_n_0\ : STD_LOGIC;
  signal output_data_3_s_reg : STD_LOGIC;
  signal \output_data_4[0]_i_1_n_0\ : STD_LOGIC;
  signal \output_data_4[1]_i_1_n_0\ : STD_LOGIC;
  signal \output_data_4[2]_i_1_n_0\ : STD_LOGIC;
  signal output_data_4_s : STD_LOGIC;
  signal output_data_4_s_reg : STD_LOGIC;
  signal \^pc_reg[0]_0\ : STD_LOGIC;
  signal \pc_s__0\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \pc_s__0__0\ : STD_LOGIC_VECTOR ( 14 to 14 );
  signal pc_s_n_0 : STD_LOGIC;
  signal pc_s_reg : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \pc_s_reg[14]_i_2_n_0\ : STD_LOGIC;
  signal \pc_s_reg[14]_i_3_n_0\ : STD_LOGIC;
  signal \pc_s_reg[21]_i_2_n_0\ : STD_LOGIC;
  signal \pc_s_reg[21]_i_3_n_0\ : STD_LOGIC;
  signal \pc_s_reg[28]_i_2_n_0\ : STD_LOGIC;
  signal \pc_s_reg[28]_i_3_n_0\ : STD_LOGIC;
  signal \pc_s_reg[28]_i_4_n_0\ : STD_LOGIC;
  signal \pc_s_reg[29]_i_2_n_0\ : STD_LOGIC;
  signal \pc_s_reg[30]_i_2_n_0\ : STD_LOGIC;
  signal \pc_s_reg[31]_i_2_n_0\ : STD_LOGIC;
  signal \pc_s_reg[7]_i_2_n_0\ : STD_LOGIC;
  signal temp_signal : STD_LOGIC_VECTOR ( 5 to 5 );
  signal w_en_i_1_n_0 : STD_LOGIC;
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg_reg[0]\ : label is "bap_20:10000000000000,bap_21:00000000001000,bap_11:00000000000100,bap_exception_2:00000100000000,bap_exception_11:00000010000000,bap_exception_10:00001000000000,bap_51:00000001000000,bap_10:00000000000010,wait_state:00000000000001,bap_50:00010000000000,bap_40:00100000000000,bap_41:00000000100000,bap_31:00000000010000,bap_30:01000000000000";
  attribute KEEP : string;
  attribute KEEP of \FSM_onehot_state_reg_reg[0]\ : label is "yes";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg_reg[10]\ : label is "bap_20:10000000000000,bap_21:00000000001000,bap_11:00000000000100,bap_exception_2:00000100000000,bap_exception_11:00000010000000,bap_exception_10:00001000000000,bap_51:00000001000000,bap_10:00000000000010,wait_state:00000000000001,bap_50:00010000000000,bap_40:00100000000000,bap_41:00000000100000,bap_31:00000000010000,bap_30:01000000000000";
  attribute KEEP of \FSM_onehot_state_reg_reg[10]\ : label is "yes";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg_reg[11]\ : label is "bap_20:10000000000000,bap_21:00000000001000,bap_11:00000000000100,bap_exception_2:00000100000000,bap_exception_11:00000010000000,bap_exception_10:00001000000000,bap_51:00000001000000,bap_10:00000000000010,wait_state:00000000000001,bap_50:00010000000000,bap_40:00100000000000,bap_41:00000000100000,bap_31:00000000010000,bap_30:01000000000000";
  attribute KEEP of \FSM_onehot_state_reg_reg[11]\ : label is "yes";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg_reg[12]\ : label is "bap_20:10000000000000,bap_21:00000000001000,bap_11:00000000000100,bap_exception_2:00000100000000,bap_exception_11:00000010000000,bap_exception_10:00001000000000,bap_51:00000001000000,bap_10:00000000000010,wait_state:00000000000001,bap_50:00010000000000,bap_40:00100000000000,bap_41:00000000100000,bap_31:00000000010000,bap_30:01000000000000";
  attribute KEEP of \FSM_onehot_state_reg_reg[12]\ : label is "yes";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg_reg[13]\ : label is "bap_20:10000000000000,bap_21:00000000001000,bap_11:00000000000100,bap_exception_2:00000100000000,bap_exception_11:00000010000000,bap_exception_10:00001000000000,bap_51:00000001000000,bap_10:00000000000010,wait_state:00000000000001,bap_50:00010000000000,bap_40:00100000000000,bap_41:00000000100000,bap_31:00000000010000,bap_30:01000000000000";
  attribute KEEP of \FSM_onehot_state_reg_reg[13]\ : label is "yes";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg_reg[1]\ : label is "bap_20:10000000000000,bap_21:00000000001000,bap_11:00000000000100,bap_exception_2:00000100000000,bap_exception_11:00000010000000,bap_exception_10:00001000000000,bap_51:00000001000000,bap_10:00000000000010,wait_state:00000000000001,bap_50:00010000000000,bap_40:00100000000000,bap_41:00000000100000,bap_31:00000000010000,bap_30:01000000000000";
  attribute KEEP of \FSM_onehot_state_reg_reg[1]\ : label is "yes";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg_reg[2]\ : label is "bap_20:10000000000000,bap_21:00000000001000,bap_11:00000000000100,bap_exception_2:00000100000000,bap_exception_11:00000010000000,bap_exception_10:00001000000000,bap_51:00000001000000,bap_10:00000000000010,wait_state:00000000000001,bap_50:00010000000000,bap_40:00100000000000,bap_41:00000000100000,bap_31:00000000010000,bap_30:01000000000000";
  attribute KEEP of \FSM_onehot_state_reg_reg[2]\ : label is "yes";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg_reg[3]\ : label is "bap_20:10000000000000,bap_21:00000000001000,bap_11:00000000000100,bap_exception_2:00000100000000,bap_exception_11:00000010000000,bap_exception_10:00001000000000,bap_51:00000001000000,bap_10:00000000000010,wait_state:00000000000001,bap_50:00010000000000,bap_40:00100000000000,bap_41:00000000100000,bap_31:00000000010000,bap_30:01000000000000";
  attribute KEEP of \FSM_onehot_state_reg_reg[3]\ : label is "yes";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg_reg[4]\ : label is "bap_20:10000000000000,bap_21:00000000001000,bap_11:00000000000100,bap_exception_2:00000100000000,bap_exception_11:00000010000000,bap_exception_10:00001000000000,bap_51:00000001000000,bap_10:00000000000010,wait_state:00000000000001,bap_50:00010000000000,bap_40:00100000000000,bap_41:00000000100000,bap_31:00000000010000,bap_30:01000000000000";
  attribute KEEP of \FSM_onehot_state_reg_reg[4]\ : label is "yes";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg_reg[5]\ : label is "bap_20:10000000000000,bap_21:00000000001000,bap_11:00000000000100,bap_exception_2:00000100000000,bap_exception_11:00000010000000,bap_exception_10:00001000000000,bap_51:00000001000000,bap_10:00000000000010,wait_state:00000000000001,bap_50:00010000000000,bap_40:00100000000000,bap_41:00000000100000,bap_31:00000000010000,bap_30:01000000000000";
  attribute KEEP of \FSM_onehot_state_reg_reg[5]\ : label is "yes";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg_reg[6]\ : label is "bap_20:10000000000000,bap_21:00000000001000,bap_11:00000000000100,bap_exception_2:00000100000000,bap_exception_11:00000010000000,bap_exception_10:00001000000000,bap_51:00000001000000,bap_10:00000000000010,wait_state:00000000000001,bap_50:00010000000000,bap_40:00100000000000,bap_41:00000000100000,bap_31:00000000010000,bap_30:01000000000000";
  attribute KEEP of \FSM_onehot_state_reg_reg[6]\ : label is "yes";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg_reg[7]\ : label is "bap_20:10000000000000,bap_21:00000000001000,bap_11:00000000000100,bap_exception_2:00000100000000,bap_exception_11:00000010000000,bap_exception_10:00001000000000,bap_51:00000001000000,bap_10:00000000000010,wait_state:00000000000001,bap_50:00010000000000,bap_40:00100000000000,bap_41:00000000100000,bap_31:00000000010000,bap_30:01000000000000";
  attribute KEEP of \FSM_onehot_state_reg_reg[7]\ : label is "yes";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg_reg[8]\ : label is "bap_20:10000000000000,bap_21:00000000001000,bap_11:00000000000100,bap_exception_2:00000100000000,bap_exception_11:00000010000000,bap_exception_10:00001000000000,bap_51:00000001000000,bap_10:00000000000010,wait_state:00000000000001,bap_50:00010000000000,bap_40:00100000000000,bap_41:00000000100000,bap_31:00000000010000,bap_30:01000000000000";
  attribute KEEP of \FSM_onehot_state_reg_reg[8]\ : label is "yes";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg_reg[9]\ : label is "bap_20:10000000000000,bap_21:00000000001000,bap_11:00000000000100,bap_exception_2:00000100000000,bap_exception_11:00000010000000,bap_exception_10:00001000000000,bap_51:00000001000000,bap_10:00000000000010,wait_state:00000000000001,bap_50:00010000000000,bap_40:00100000000000,bap_41:00000000100000,bap_31:00000000010000,bap_30:01000000000000";
  attribute KEEP of \FSM_onehot_state_reg_reg[9]\ : label is "yes";
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \output_data_1[6]_i_3\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \output_data_1[6]_i_4\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \pc_s_reg[0]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \pc_s_reg[14]_i_2\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \pc_s_reg[14]_i_3\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \pc_s_reg[21]_i_2\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \pc_s_reg[21]_i_3\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \pc_s_reg[28]_i_2\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \pc_s_reg[28]_i_3\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \pc_s_reg[28]_i_4\ : label is "soft_lutpair10";
begin
  D(1 downto 0) <= \^d\(1 downto 0);
  \out\(4 downto 0) <= \^out\(4 downto 0);
  out_en_b_i <= \^out_en_b_i\;
  \pc_reg[0]_0\ <= \^pc_reg[0]_0\;
\FSM_onehot_state_reg[0]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEAAAAA"
    )
        port map (
      I0 => \FSM_onehot_state_reg[0]_i_2__0_n_0\,
      I1 => enable_reg_reg_reg_0,
      I2 => \^pc_reg[0]_0\,
      I3 => \^out\(0),
      I4 => \state_reg_reg[1]\,
      O => \FSM_onehot_state_reg[0]_i_1__0_n_0\
    );
\FSM_onehot_state_reg[0]_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5C5C4C4CFCFCCC4C"
    )
        port map (
      I0 => \state_reg_reg[1]_0\,
      I1 => \^out\(0),
      I2 => enable_reg_reg_reg_0,
      I3 => \FSM_onehot_state_reg_reg[7]_0\,
      I4 => \^pc_reg[0]_0\,
      I5 => Q(0),
      O => \FSM_onehot_state_reg[0]_i_2__0_n_0\
    );
\FSM_onehot_state_reg[0]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \FSM_onehot_state_reg[0]_i_6__0_n_0\,
      I1 => \FSM_onehot_state_reg_reg_n_0_[1]\,
      I2 => \FSM_onehot_state_reg_reg_n_0_[12]\,
      I3 => \FSM_onehot_state_reg_reg_n_0_[13]\,
      O => \^pc_reg[0]_0\
    );
\FSM_onehot_state_reg[0]_i_6__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \FSM_onehot_state_reg_reg_n_0_[9]\,
      I1 => \FSM_onehot_state_reg_reg_n_0_[8]\,
      I2 => \FSM_onehot_state_reg_reg_n_0_[11]\,
      I3 => \FSM_onehot_state_reg_reg_n_0_[10]\,
      O => \FSM_onehot_state_reg[0]_i_6__0_n_0\
    );
\FSM_onehot_state_reg[10]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => enable_reg_reg_reg_0,
      I1 => \^out\(2),
      I2 => Q(6),
      O => \FSM_onehot_state_reg[10]_i_1_n_0\
    );
\FSM_onehot_state_reg[11]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \FSM_onehot_state_reg_reg_n_0_[4]\,
      I1 => enable_reg_reg_reg_0,
      I2 => Q(7),
      O => \FSM_onehot_state_reg[11]_i_1__0_n_0\
    );
\FSM_onehot_state_reg[12]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \FSM_onehot_state_reg_reg_n_0_[3]\,
      I1 => enable_reg_reg_reg_0,
      I2 => Q(7),
      O => \FSM_onehot_state_reg[12]_i_1__0_n_0\
    );
\FSM_onehot_state_reg[13]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \^out\(1),
      I1 => enable_reg_reg_reg_0,
      I2 => Q(7),
      O => \FSM_onehot_state_reg[13]_i_1_n_0\
    );
\FSM_onehot_state_reg[1]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^pc_reg[0]_0\,
      I1 => enable_reg_reg_reg_0,
      O => \FSM_onehot_state_reg_reg[1]_0\
    );
\FSM_onehot_state_reg[1]_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => \FSM_onehot_state_reg_reg_n_0_[13]\,
      I1 => \FSM_onehot_state_reg_reg_n_0_[12]\,
      I2 => \FSM_onehot_state_reg_reg_n_0_[1]\,
      I3 => \FSM_onehot_state_reg[0]_i_6__0_n_0\,
      I4 => \^out\(4),
      O => \FSM_onehot_state_reg_reg[0]_0\
    );
\FSM_onehot_state_reg[1]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFEFFFFFFFF"
    )
        port map (
      I0 => \^out\(4),
      I1 => \FSM_onehot_state_reg[0]_i_6__0_n_0\,
      I2 => \FSM_onehot_state_reg_reg_n_0_[1]\,
      I3 => \FSM_onehot_state_reg_reg_n_0_[12]\,
      I4 => \FSM_onehot_state_reg_reg_n_0_[13]\,
      I5 => \state_reg_reg[2]\,
      O => \state_reg_reg[0]\
    );
\FSM_onehot_state_reg[2]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^out\(1),
      I1 => enable_reg_reg_reg_0,
      O => \FSM_onehot_state_reg_reg[2]_0\
    );
\FSM_onehot_state_reg[3]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8F80"
    )
        port map (
      I0 => \^out\(1),
      I1 => Q(7),
      I2 => enable_reg_reg_reg_0,
      I3 => \FSM_onehot_state_reg_reg_n_0_[3]\,
      O => \FSM_onehot_state_reg[3]_i_1__0_n_0\
    );
\FSM_onehot_state_reg[4]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8F80"
    )
        port map (
      I0 => \FSM_onehot_state_reg_reg_n_0_[3]\,
      I1 => Q(7),
      I2 => enable_reg_reg_reg_0,
      I3 => \FSM_onehot_state_reg_reg_n_0_[4]\,
      O => \FSM_onehot_state_reg[4]_i_1__0_n_0\
    );
\FSM_onehot_state_reg[5]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8F80"
    )
        port map (
      I0 => \FSM_onehot_state_reg_reg_n_0_[4]\,
      I1 => Q(7),
      I2 => enable_reg_reg_reg_0,
      I3 => \^out\(2),
      O => \FSM_onehot_state_reg[5]_i_1__0_n_0\
    );
\FSM_onehot_state_reg[7]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8F80"
    )
        port map (
      I0 => \^out\(3),
      I1 => Q(7),
      I2 => enable_reg_reg_reg_0,
      I3 => \^out\(4),
      O => \FSM_onehot_state_reg[7]_i_1__0_n_0\
    );
\FSM_onehot_state_reg[8]_i_1__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^out\(4),
      I1 => enable_reg_reg_reg_0,
      O => \FSM_onehot_state_reg[8]_i_1__1_n_0\
    );
\FSM_onehot_state_reg_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk,
      CE => '1',
      D => \FSM_onehot_state_reg[0]_i_1__0_n_0\,
      Q => \^out\(0),
      S => reset
    );
\FSM_onehot_state_reg_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \FSM_onehot_state_reg[10]_i_1_n_0\,
      Q => \FSM_onehot_state_reg_reg_n_0_[10]\,
      R => reset
    );
\FSM_onehot_state_reg_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \FSM_onehot_state_reg[11]_i_1__0_n_0\,
      Q => \FSM_onehot_state_reg_reg_n_0_[11]\,
      R => reset
    );
\FSM_onehot_state_reg_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \FSM_onehot_state_reg[12]_i_1__0_n_0\,
      Q => \FSM_onehot_state_reg_reg_n_0_[12]\,
      R => reset
    );
\FSM_onehot_state_reg_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \FSM_onehot_state_reg[13]_i_1_n_0\,
      Q => \FSM_onehot_state_reg_reg_n_0_[13]\,
      R => reset
    );
\FSM_onehot_state_reg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \data_reg_s_reg[7]_0\(0),
      Q => \FSM_onehot_state_reg_reg_n_0_[1]\,
      R => reset
    );
\FSM_onehot_state_reg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \data_reg_s_reg[7]_0\(1),
      Q => \^out\(1),
      R => reset
    );
\FSM_onehot_state_reg_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \FSM_onehot_state_reg[3]_i_1__0_n_0\,
      Q => \FSM_onehot_state_reg_reg_n_0_[3]\,
      R => reset
    );
\FSM_onehot_state_reg_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \FSM_onehot_state_reg[4]_i_1__0_n_0\,
      Q => \FSM_onehot_state_reg_reg_n_0_[4]\,
      R => reset
    );
\FSM_onehot_state_reg_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \FSM_onehot_state_reg[5]_i_1__0_n_0\,
      Q => \^out\(2),
      R => reset
    );
\FSM_onehot_state_reg_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => enable_reg_reg_reg_0,
      D => \data_reg_s_reg[6]\,
      Q => \^out\(3),
      R => reset
    );
\FSM_onehot_state_reg_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \FSM_onehot_state_reg[7]_i_1__0_n_0\,
      Q => \^out\(4),
      R => reset
    );
\FSM_onehot_state_reg_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \FSM_onehot_state_reg[8]_i_1__1_n_0\,
      Q => \FSM_onehot_state_reg_reg_n_0_[8]\,
      R => reset
    );
\FSM_onehot_state_reg_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \data_reg_s_reg[7]_0\(2),
      Q => \FSM_onehot_state_reg_reg_n_0_[9]\,
      R => reset
    );
enable_i_reg_reg: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => E(0),
      Q => temp_signal(5),
      R => reset
    );
\instruction_address_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \output_data_3_reg[7]\(0),
      Q => instruction_address_reg(0),
      R => reset
    );
\instruction_address_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \output_data_3_reg[7]\(10),
      Q => instruction_address_reg(10),
      R => reset
    );
\instruction_address_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \output_data_3_reg[7]\(11),
      Q => instruction_address_reg(11),
      R => reset
    );
\instruction_address_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \output_data_3_reg[7]\(12),
      Q => instruction_address_reg(12),
      R => reset
    );
\instruction_address_reg_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \output_data_3_reg[7]\(13),
      Q => instruction_address_reg(13),
      R => reset
    );
\instruction_address_reg_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \output_data_3_reg[7]\(14),
      Q => instruction_address_reg(14),
      R => reset
    );
\instruction_address_reg_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \output_data_3_reg[7]\(15),
      Q => instruction_address_reg(15),
      R => reset
    );
\instruction_address_reg_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \output_data_3_reg[7]\(16),
      Q => instruction_address_reg(16),
      R => reset
    );
\instruction_address_reg_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \output_data_3_reg[7]\(17),
      Q => instruction_address_reg(17),
      R => reset
    );
\instruction_address_reg_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \output_data_3_reg[7]\(18),
      Q => instruction_address_reg(18),
      R => reset
    );
\instruction_address_reg_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \output_data_3_reg[7]\(19),
      Q => instruction_address_reg(19),
      R => reset
    );
\instruction_address_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \output_data_3_reg[7]\(1),
      Q => instruction_address_reg(1),
      R => reset
    );
\instruction_address_reg_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \output_data_3_reg[7]\(20),
      Q => instruction_address_reg(20),
      R => reset
    );
\instruction_address_reg_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \output_data_3_reg[7]\(21),
      Q => instruction_address_reg(21),
      R => reset
    );
\instruction_address_reg_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \output_data_3_reg[7]\(22),
      Q => instruction_address_reg(22),
      R => reset
    );
\instruction_address_reg_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \output_data_3_reg[7]\(23),
      Q => instruction_address_reg(23),
      R => reset
    );
\instruction_address_reg_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \output_data_3_reg[7]\(24),
      Q => instruction_address_reg(24),
      R => reset
    );
\instruction_address_reg_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \output_data_3_reg[7]\(25),
      Q => instruction_address_reg(25),
      R => reset
    );
\instruction_address_reg_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \output_data_3_reg[7]\(26),
      Q => instruction_address_reg(26),
      R => reset
    );
\instruction_address_reg_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \output_data_3_reg[7]\(27),
      Q => instruction_address_reg(27),
      R => reset
    );
\instruction_address_reg_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \output_data_3_reg[7]\(28),
      Q => instruction_address_reg(28),
      R => reset
    );
\instruction_address_reg_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \output_data_3_reg[7]\(29),
      Q => instruction_address_reg(29),
      R => reset
    );
\instruction_address_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \output_data_3_reg[7]\(2),
      Q => instruction_address_reg(2),
      R => reset
    );
\instruction_address_reg_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \output_data_3_reg[7]\(30),
      Q => instruction_address_reg(30),
      R => reset
    );
\instruction_address_reg_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \output_data_3_reg[7]\(31),
      Q => instruction_address_reg(31),
      R => reset
    );
\instruction_address_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \output_data_3_reg[7]\(3),
      Q => instruction_address_reg(3),
      R => reset
    );
\instruction_address_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \output_data_3_reg[7]\(4),
      Q => instruction_address_reg(4),
      R => reset
    );
\instruction_address_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \output_data_3_reg[7]\(5),
      Q => instruction_address_reg(5),
      R => reset
    );
\instruction_address_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \output_data_3_reg[7]\(6),
      Q => instruction_address_reg(6),
      R => reset
    );
\instruction_address_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \output_data_3_reg[7]\(7),
      Q => instruction_address_reg(7),
      R => reset
    );
\instruction_address_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \output_data_3_reg[7]\(8),
      Q => instruction_address_reg(8),
      R => reset
    );
\instruction_address_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \output_data_3_reg[7]\(9),
      Q => instruction_address_reg(9),
      R => reset
    );
\output_data_0[5]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8880"
    )
        port map (
      I0 => Q(0),
      I1 => enable_reg_reg_reg_0,
      I2 => \^out\(0),
      I3 => \^pc_reg[0]_0\,
      O => output_data_0_s_reg_reg_0
    );
\output_data_0_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => enable_reg_reg_reg(0),
      D => Q(1),
      Q => data1(2),
      R => reset
    );
\output_data_0_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => enable_reg_reg_reg(0),
      D => Q(2),
      Q => data1(3),
      R => reset
    );
\output_data_0_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => enable_reg_reg_reg(0),
      D => Q(3),
      Q => data1(4),
      R => reset
    );
\output_data_0_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => enable_reg_reg_reg(0),
      D => Q(4),
      Q => data1(5),
      R => reset
    );
\output_data_0_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => enable_reg_reg_reg(0),
      D => Q(5),
      Q => data1(6),
      R => reset
    );
\output_data_0_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => enable_reg_reg_reg(0),
      D => Q(6),
      Q => data1(7),
      R => reset
    );
output_data_0_s_reg_reg: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => enable_reg_reg_reg(0),
      Q => output_data_0_s_reg,
      R => reset
    );
\output_data_1[6]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => enable_reg_reg_reg_0,
      I1 => \^out\(1),
      O => \output_data_1[6]_i_1__0_n_0\
    );
\output_data_1[6]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF0000FF80FF80"
    )
        port map (
      I0 => pc_s_n_0,
      I1 => data1(14),
      I2 => \output_data_1[6]_i_3_n_0\,
      I3 => \output_data_1[6]_i_4_n_0\,
      I4 => Q(6),
      I5 => \data_reg_s_reg[7]\,
      O => \output_data_1[6]_i_2_n_0\
    );
\output_data_1[6]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => output_data_2_s_reg,
      I1 => output_data_3_s_reg,
      I2 => output_data_4_s_reg,
      I3 => output_data_1_s_reg,
      O => \output_data_1[6]_i_3_n_0\
    );
\output_data_1[6]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F8AA88AA"
    )
        port map (
      I0 => pc_s_reg(14),
      I1 => output_data_0_s_reg,
      I2 => instruction_address_reg(14),
      I3 => pc_s_n_0,
      I4 => temp_signal(5),
      O => \output_data_1[6]_i_4_n_0\
    );
\output_data_1_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \output_data_1[6]_i_1__0_n_0\,
      D => Q(0),
      Q => data1(8),
      R => reset
    );
\output_data_1_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \output_data_1[6]_i_1__0_n_0\,
      D => Q(1),
      Q => data1(9),
      R => reset
    );
\output_data_1_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \output_data_1[6]_i_1__0_n_0\,
      D => Q(2),
      Q => data1(10),
      R => reset
    );
\output_data_1_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \output_data_1[6]_i_1__0_n_0\,
      D => Q(3),
      Q => data1(11),
      R => reset
    );
\output_data_1_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \output_data_1[6]_i_1__0_n_0\,
      D => Q(4),
      Q => data1(12),
      R => reset
    );
\output_data_1_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \output_data_1[6]_i_1__0_n_0\,
      D => Q(5),
      Q => data1(13),
      R => reset
    );
\output_data_1_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \output_data_1[6]_i_1__0_n_0\,
      D => \output_data_1[6]_i_2_n_0\,
      Q => data1(14),
      R => reset
    );
output_data_1_s_reg_reg: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \FSM_onehot_state_reg[13]_i_1_n_0\,
      Q => output_data_1_s_reg,
      R => reset
    );
\output_data_2[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => enable_reg_reg_reg_0,
      I1 => \FSM_onehot_state_reg_reg_n_0_[3]\,
      O => \output_data_2[6]_i_1_n_0\
    );
\output_data_2_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \output_data_2[6]_i_1_n_0\,
      D => Q(0),
      Q => data1(15),
      R => reset
    );
\output_data_2_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \output_data_2[6]_i_1_n_0\,
      D => Q(1),
      Q => data1(16),
      R => reset
    );
\output_data_2_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \output_data_2[6]_i_1_n_0\,
      D => Q(2),
      Q => data1(17),
      R => reset
    );
\output_data_2_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \output_data_2[6]_i_1_n_0\,
      D => Q(3),
      Q => data1(18),
      R => reset
    );
\output_data_2_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \output_data_2[6]_i_1_n_0\,
      D => Q(4),
      Q => data1(19),
      R => reset
    );
\output_data_2_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \output_data_2[6]_i_1_n_0\,
      D => Q(5),
      Q => data1(20),
      R => reset
    );
\output_data_2_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \output_data_2[6]_i_1_n_0\,
      D => \data_reg_s_reg[6]_0\(0),
      Q => data1(21),
      R => reset
    );
output_data_2_s_reg_reg: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \FSM_onehot_state_reg[12]_i_1__0_n_0\,
      Q => output_data_2_s_reg,
      R => reset
    );
\output_data_3[6]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => enable_reg_reg_reg_0,
      I1 => \FSM_onehot_state_reg_reg_n_0_[4]\,
      O => \output_data_3[6]_i_1__0_n_0\
    );
\output_data_3_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \output_data_3[6]_i_1__0_n_0\,
      D => Q(0),
      Q => data1(22),
      R => reset
    );
\output_data_3_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \output_data_3[6]_i_1__0_n_0\,
      D => Q(1),
      Q => data1(23),
      R => reset
    );
\output_data_3_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \output_data_3[6]_i_1__0_n_0\,
      D => Q(2),
      Q => data1(24),
      R => reset
    );
\output_data_3_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \output_data_3[6]_i_1__0_n_0\,
      D => Q(3),
      Q => data1(25),
      R => reset
    );
\output_data_3_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \output_data_3[6]_i_1__0_n_0\,
      D => Q(4),
      Q => data1(26),
      R => reset
    );
\output_data_3_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \output_data_3[6]_i_1__0_n_0\,
      D => Q(5),
      Q => data1(27),
      R => reset
    );
\output_data_3_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \output_data_3[6]_i_1__0_n_0\,
      D => \data_reg_s_reg[6]_1\(0),
      Q => data1(28),
      R => reset
    );
output_data_3_s_reg_reg: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \FSM_onehot_state_reg[11]_i_1__0_n_0\,
      Q => output_data_3_s_reg,
      R => reset
    );
\output_data_4[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => Q(0),
      I1 => \^out\(2),
      I2 => enable_reg_reg_reg_0,
      I3 => data1(29),
      O => \output_data_4[0]_i_1_n_0\
    );
\output_data_4[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => Q(1),
      I1 => \^out\(2),
      I2 => enable_reg_reg_reg_0,
      I3 => data1(30),
      O => \output_data_4[1]_i_1_n_0\
    );
\output_data_4[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => Q(2),
      I1 => \^out\(2),
      I2 => enable_reg_reg_reg_0,
      I3 => data1(31),
      O => \output_data_4[2]_i_1_n_0\
    );
\output_data_4_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \output_data_4[0]_i_1_n_0\,
      Q => data1(29),
      R => reset
    );
\output_data_4_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \output_data_4[1]_i_1_n_0\,
      Q => data1(30),
      R => reset
    );
\output_data_4_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \output_data_4[2]_i_1_n_0\,
      Q => data1(31),
      R => reset
    );
output_data_4_s_reg_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^out\(2),
      I1 => enable_reg_reg_reg_0,
      O => output_data_4_s
    );
output_data_4_s_reg_reg: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => output_data_4_s,
      Q => output_data_4_s_reg,
      R => reset
    );
\pc_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => w_en_i_1_n_0,
      D => \pc_s__0\(0),
      Q => \pc_s_reg_reg[31]_0\(0),
      R => reset
    );
\pc_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => w_en_i_1_n_0,
      D => \pc_s__0\(10),
      Q => \pc_s_reg_reg[31]_0\(10),
      R => reset
    );
\pc_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => w_en_i_1_n_0,
      D => \pc_s__0\(11),
      Q => \pc_s_reg_reg[31]_0\(11),
      R => reset
    );
\pc_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => w_en_i_1_n_0,
      D => \pc_s__0\(12),
      Q => \pc_s_reg_reg[31]_0\(12),
      R => reset
    );
\pc_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => w_en_i_1_n_0,
      D => \pc_s__0\(13),
      Q => \pc_s_reg_reg[31]_0\(13),
      R => reset
    );
\pc_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => w_en_i_1_n_0,
      D => \pc_s__0__0\(14),
      Q => \pc_s_reg_reg[31]_0\(14),
      R => reset
    );
\pc_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => w_en_i_1_n_0,
      D => \pc_s__0\(15),
      Q => \pc_s_reg_reg[31]_0\(15),
      R => reset
    );
\pc_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => w_en_i_1_n_0,
      D => \pc_s__0\(16),
      Q => \pc_s_reg_reg[31]_0\(16),
      R => reset
    );
\pc_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => w_en_i_1_n_0,
      D => \pc_s__0\(17),
      Q => \pc_s_reg_reg[31]_0\(17),
      R => reset
    );
\pc_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => w_en_i_1_n_0,
      D => \pc_s__0\(18),
      Q => \pc_s_reg_reg[31]_0\(18),
      R => reset
    );
\pc_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => w_en_i_1_n_0,
      D => \pc_s__0\(19),
      Q => \pc_s_reg_reg[31]_0\(19),
      R => reset
    );
\pc_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => w_en_i_1_n_0,
      D => \pc_s__0\(1),
      Q => \pc_s_reg_reg[31]_0\(1),
      R => reset
    );
\pc_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => w_en_i_1_n_0,
      D => \pc_s__0\(20),
      Q => \pc_s_reg_reg[31]_0\(20),
      R => reset
    );
\pc_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => w_en_i_1_n_0,
      D => \^d\(0),
      Q => \pc_s_reg_reg[31]_0\(21),
      R => reset
    );
\pc_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => w_en_i_1_n_0,
      D => \pc_s__0\(22),
      Q => \pc_s_reg_reg[31]_0\(22),
      R => reset
    );
\pc_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => w_en_i_1_n_0,
      D => \pc_s__0\(23),
      Q => \pc_s_reg_reg[31]_0\(23),
      R => reset
    );
\pc_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => w_en_i_1_n_0,
      D => \pc_s__0\(24),
      Q => \pc_s_reg_reg[31]_0\(24),
      R => reset
    );
\pc_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => w_en_i_1_n_0,
      D => \pc_s__0\(25),
      Q => \pc_s_reg_reg[31]_0\(25),
      R => reset
    );
\pc_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => w_en_i_1_n_0,
      D => \pc_s__0\(26),
      Q => \pc_s_reg_reg[31]_0\(26),
      R => reset
    );
\pc_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => w_en_i_1_n_0,
      D => \pc_s__0\(27),
      Q => \pc_s_reg_reg[31]_0\(27),
      R => reset
    );
\pc_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => w_en_i_1_n_0,
      D => \^d\(1),
      Q => \pc_s_reg_reg[31]_0\(28),
      R => reset
    );
\pc_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => w_en_i_1_n_0,
      D => \pc_s__0\(29),
      Q => \pc_s_reg_reg[31]_0\(29),
      R => reset
    );
\pc_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => w_en_i_1_n_0,
      D => \pc_s__0\(2),
      Q => \pc_s_reg_reg[31]_0\(2),
      R => reset
    );
\pc_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => w_en_i_1_n_0,
      D => \pc_s__0\(30),
      Q => \pc_s_reg_reg[31]_0\(30),
      R => reset
    );
\pc_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => w_en_i_1_n_0,
      D => \pc_s__0\(31),
      Q => \pc_s_reg_reg[31]_0\(31),
      R => reset
    );
\pc_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => w_en_i_1_n_0,
      D => \pc_s__0\(3),
      Q => \pc_s_reg_reg[31]_0\(3),
      R => reset
    );
\pc_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => w_en_i_1_n_0,
      D => \pc_s__0\(4),
      Q => \pc_s_reg_reg[31]_0\(4),
      R => reset
    );
\pc_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => w_en_i_1_n_0,
      D => \pc_s__0\(5),
      Q => \pc_s_reg_reg[31]_0\(5),
      R => reset
    );
\pc_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => w_en_i_1_n_0,
      D => \pc_s__0\(6),
      Q => \pc_s_reg_reg[31]_0\(6),
      R => reset
    );
\pc_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => w_en_i_1_n_0,
      D => \pc_s__0\(7),
      Q => \pc_s_reg_reg[31]_0\(7),
      R => reset
    );
\pc_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => w_en_i_1_n_0,
      D => \pc_s__0\(8),
      Q => \pc_s_reg_reg[31]_0\(8),
      R => reset
    );
\pc_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => w_en_i_1_n_0,
      D => \pc_s__0\(9),
      Q => \pc_s_reg_reg[31]_0\(9),
      R => reset
    );
pc_s: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000100010116"
    )
        port map (
      I0 => output_data_0_s_reg,
      I1 => output_data_1_s_reg,
      I2 => output_data_2_s_reg,
      I3 => output_data_3_s_reg,
      I4 => output_data_4_s_reg,
      I5 => temp_signal(5),
      O => pc_s_n_0
    );
\pc_s_reg[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CA0A"
    )
        port map (
      I0 => pc_s_reg(0),
      I1 => instruction_address_reg(0),
      I2 => pc_s_n_0,
      I3 => temp_signal(5),
      O => \pc_s__0\(0)
    );
\pc_s_reg[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \pc_s_reg[28]_i_3_n_0\,
      I1 => instruction_address_reg(10),
      I2 => \pc_s_reg[14]_i_3_n_0\,
      I3 => pc_s_reg(10),
      I4 => data1(10),
      I5 => \pc_s_reg[14]_i_2_n_0\,
      O => \pc_s__0\(10)
    );
\pc_s_reg[11]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \pc_s_reg[28]_i_3_n_0\,
      I1 => instruction_address_reg(11),
      I2 => \pc_s_reg[14]_i_3_n_0\,
      I3 => pc_s_reg(11),
      I4 => data1(11),
      I5 => \pc_s_reg[14]_i_2_n_0\,
      O => \pc_s__0\(11)
    );
\pc_s_reg[12]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \pc_s_reg[28]_i_3_n_0\,
      I1 => instruction_address_reg(12),
      I2 => \pc_s_reg[14]_i_3_n_0\,
      I3 => pc_s_reg(12),
      I4 => data1(12),
      I5 => \pc_s_reg[14]_i_2_n_0\,
      O => \pc_s__0\(12)
    );
\pc_s_reg[13]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \pc_s_reg[28]_i_3_n_0\,
      I1 => instruction_address_reg(13),
      I2 => \pc_s_reg[14]_i_3_n_0\,
      I3 => pc_s_reg(13),
      I4 => data1(13),
      I5 => \pc_s_reg[14]_i_2_n_0\,
      O => \pc_s__0\(13)
    );
\pc_s_reg[14]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => data1(14),
      I1 => \pc_s_reg[14]_i_2_n_0\,
      I2 => \pc_s_reg[28]_i_3_n_0\,
      I3 => instruction_address_reg(14),
      I4 => \pc_s_reg[14]_i_3_n_0\,
      I5 => pc_s_reg(14),
      O => \pc_s__0__0\(14)
    );
\pc_s_reg[14]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => output_data_1_s_reg,
      I1 => output_data_4_s_reg,
      I2 => output_data_3_s_reg,
      I3 => output_data_2_s_reg,
      I4 => pc_s_n_0,
      O => \pc_s_reg[14]_i_2_n_0\
    );
\pc_s_reg[14]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => output_data_0_s_reg,
      I1 => pc_s_n_0,
      O => \pc_s_reg[14]_i_3_n_0\
    );
\pc_s_reg[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \pc_s_reg[28]_i_3_n_0\,
      I1 => instruction_address_reg(15),
      I2 => \pc_s_reg[21]_i_3_n_0\,
      I3 => pc_s_reg(15),
      I4 => data1(15),
      I5 => \pc_s_reg[21]_i_2_n_0\,
      O => \pc_s__0\(15)
    );
\pc_s_reg[16]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \pc_s_reg[28]_i_3_n_0\,
      I1 => instruction_address_reg(16),
      I2 => \pc_s_reg[21]_i_3_n_0\,
      I3 => pc_s_reg(16),
      I4 => data1(16),
      I5 => \pc_s_reg[21]_i_2_n_0\,
      O => \pc_s__0\(16)
    );
\pc_s_reg[17]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \pc_s_reg[28]_i_3_n_0\,
      I1 => instruction_address_reg(17),
      I2 => \pc_s_reg[21]_i_3_n_0\,
      I3 => pc_s_reg(17),
      I4 => data1(17),
      I5 => \pc_s_reg[21]_i_2_n_0\,
      O => \pc_s__0\(17)
    );
\pc_s_reg[18]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \pc_s_reg[28]_i_3_n_0\,
      I1 => instruction_address_reg(18),
      I2 => \pc_s_reg[21]_i_3_n_0\,
      I3 => pc_s_reg(18),
      I4 => data1(18),
      I5 => \pc_s_reg[21]_i_2_n_0\,
      O => \pc_s__0\(18)
    );
\pc_s_reg[19]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \pc_s_reg[28]_i_3_n_0\,
      I1 => instruction_address_reg(19),
      I2 => \pc_s_reg[21]_i_3_n_0\,
      I3 => pc_s_reg(19),
      I4 => data1(19),
      I5 => \pc_s_reg[21]_i_2_n_0\,
      O => \pc_s__0\(19)
    );
\pc_s_reg[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CA0A"
    )
        port map (
      I0 => pc_s_reg(1),
      I1 => instruction_address_reg(1),
      I2 => pc_s_n_0,
      I3 => temp_signal(5),
      O => \pc_s__0\(1)
    );
\pc_s_reg[20]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \pc_s_reg[28]_i_3_n_0\,
      I1 => instruction_address_reg(20),
      I2 => \pc_s_reg[21]_i_3_n_0\,
      I3 => pc_s_reg(20),
      I4 => data1(20),
      I5 => \pc_s_reg[21]_i_2_n_0\,
      O => \pc_s__0\(20)
    );
\pc_s_reg[21]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => data1(21),
      I1 => \pc_s_reg[21]_i_2_n_0\,
      I2 => \pc_s_reg[28]_i_3_n_0\,
      I3 => instruction_address_reg(21),
      I4 => \pc_s_reg[21]_i_3_n_0\,
      I5 => pc_s_reg(21),
      O => \^d\(0)
    );
\pc_s_reg[21]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FE00"
    )
        port map (
      I0 => output_data_2_s_reg,
      I1 => output_data_3_s_reg,
      I2 => output_data_4_s_reg,
      I3 => pc_s_n_0,
      O => \pc_s_reg[21]_i_2_n_0\
    );
\pc_s_reg[21]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EF"
    )
        port map (
      I0 => output_data_1_s_reg,
      I1 => output_data_0_s_reg,
      I2 => pc_s_n_0,
      O => \pc_s_reg[21]_i_3_n_0\
    );
\pc_s_reg[22]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \pc_s_reg[28]_i_3_n_0\,
      I1 => instruction_address_reg(22),
      I2 => \pc_s_reg[28]_i_4_n_0\,
      I3 => data1(22),
      I4 => pc_s_reg(22),
      I5 => \pc_s_reg[28]_i_2_n_0\,
      O => \pc_s__0\(22)
    );
\pc_s_reg[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \pc_s_reg[28]_i_3_n_0\,
      I1 => instruction_address_reg(23),
      I2 => \pc_s_reg[28]_i_4_n_0\,
      I3 => data1(23),
      I4 => pc_s_reg(23),
      I5 => \pc_s_reg[28]_i_2_n_0\,
      O => \pc_s__0\(23)
    );
\pc_s_reg[24]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \pc_s_reg[28]_i_3_n_0\,
      I1 => instruction_address_reg(24),
      I2 => \pc_s_reg[28]_i_4_n_0\,
      I3 => data1(24),
      I4 => pc_s_reg(24),
      I5 => \pc_s_reg[28]_i_2_n_0\,
      O => \pc_s__0\(24)
    );
\pc_s_reg[25]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \pc_s_reg[28]_i_3_n_0\,
      I1 => instruction_address_reg(25),
      I2 => \pc_s_reg[28]_i_4_n_0\,
      I3 => data1(25),
      I4 => pc_s_reg(25),
      I5 => \pc_s_reg[28]_i_2_n_0\,
      O => \pc_s__0\(25)
    );
\pc_s_reg[26]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \pc_s_reg[28]_i_3_n_0\,
      I1 => instruction_address_reg(26),
      I2 => \pc_s_reg[28]_i_4_n_0\,
      I3 => data1(26),
      I4 => pc_s_reg(26),
      I5 => \pc_s_reg[28]_i_2_n_0\,
      O => \pc_s__0\(26)
    );
\pc_s_reg[27]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \pc_s_reg[28]_i_3_n_0\,
      I1 => instruction_address_reg(27),
      I2 => \pc_s_reg[28]_i_4_n_0\,
      I3 => data1(27),
      I4 => pc_s_reg(27),
      I5 => \pc_s_reg[28]_i_2_n_0\,
      O => \pc_s__0\(27)
    );
\pc_s_reg[28]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => pc_s_reg(28),
      I1 => \pc_s_reg[28]_i_2_n_0\,
      I2 => \pc_s_reg[28]_i_3_n_0\,
      I3 => instruction_address_reg(28),
      I4 => \pc_s_reg[28]_i_4_n_0\,
      I5 => data1(28),
      O => \^d\(1)
    );
\pc_s_reg[28]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => pc_s_n_0,
      I1 => output_data_0_s_reg,
      I2 => output_data_1_s_reg,
      I3 => output_data_2_s_reg,
      O => \pc_s_reg[28]_i_2_n_0\
    );
\pc_s_reg[28]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => pc_s_n_0,
      I1 => temp_signal(5),
      O => \pc_s_reg[28]_i_3_n_0\
    );
\pc_s_reg[28]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => output_data_4_s_reg,
      I1 => output_data_3_s_reg,
      I2 => pc_s_n_0,
      O => \pc_s_reg[28]_i_4_n_0\
    );
\pc_s_reg[29]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFAAEAAAEAAAEAAA"
    )
        port map (
      I0 => \pc_s_reg[29]_i_2_n_0\,
      I1 => output_data_4_s_reg,
      I2 => data1(29),
      I3 => pc_s_n_0,
      I4 => temp_signal(5),
      I5 => instruction_address_reg(29),
      O => \pc_s__0\(29)
    );
\pc_s_reg[29]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFB00000000"
    )
        port map (
      I0 => output_data_3_s_reg,
      I1 => pc_s_n_0,
      I2 => output_data_0_s_reg,
      I3 => output_data_1_s_reg,
      I4 => output_data_2_s_reg,
      I5 => pc_s_reg(29),
      O => \pc_s_reg[29]_i_2_n_0\
    );
\pc_s_reg[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFB380B380B380"
    )
        port map (
      I0 => temp_signal(5),
      I1 => pc_s_n_0,
      I2 => instruction_address_reg(2),
      I3 => pc_s_reg(2),
      I4 => data1(2),
      I5 => \pc_s_reg[7]_i_2_n_0\,
      O => \pc_s__0\(2)
    );
\pc_s_reg[30]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFAAEAAAEAAAEAAA"
    )
        port map (
      I0 => \pc_s_reg[30]_i_2_n_0\,
      I1 => output_data_4_s_reg,
      I2 => data1(30),
      I3 => pc_s_n_0,
      I4 => temp_signal(5),
      I5 => instruction_address_reg(30),
      O => \pc_s__0\(30)
    );
\pc_s_reg[30]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFB00000000"
    )
        port map (
      I0 => output_data_3_s_reg,
      I1 => pc_s_n_0,
      I2 => output_data_0_s_reg,
      I3 => output_data_1_s_reg,
      I4 => output_data_2_s_reg,
      I5 => pc_s_reg(30),
      O => \pc_s_reg[30]_i_2_n_0\
    );
\pc_s_reg[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFAAEAAAEAAAEAAA"
    )
        port map (
      I0 => \pc_s_reg[31]_i_2_n_0\,
      I1 => output_data_4_s_reg,
      I2 => data1(31),
      I3 => pc_s_n_0,
      I4 => temp_signal(5),
      I5 => instruction_address_reg(31),
      O => \pc_s__0\(31)
    );
\pc_s_reg[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFB00000000"
    )
        port map (
      I0 => output_data_3_s_reg,
      I1 => pc_s_n_0,
      I2 => output_data_0_s_reg,
      I3 => output_data_1_s_reg,
      I4 => output_data_2_s_reg,
      I5 => pc_s_reg(31),
      O => \pc_s_reg[31]_i_2_n_0\
    );
\pc_s_reg[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFB380B380B380"
    )
        port map (
      I0 => temp_signal(5),
      I1 => pc_s_n_0,
      I2 => instruction_address_reg(3),
      I3 => pc_s_reg(3),
      I4 => data1(3),
      I5 => \pc_s_reg[7]_i_2_n_0\,
      O => \pc_s__0\(3)
    );
\pc_s_reg[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFB380B380B380"
    )
        port map (
      I0 => temp_signal(5),
      I1 => pc_s_n_0,
      I2 => instruction_address_reg(4),
      I3 => pc_s_reg(4),
      I4 => data1(4),
      I5 => \pc_s_reg[7]_i_2_n_0\,
      O => \pc_s__0\(4)
    );
\pc_s_reg[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFB380B380B380"
    )
        port map (
      I0 => temp_signal(5),
      I1 => pc_s_n_0,
      I2 => instruction_address_reg(5),
      I3 => pc_s_reg(5),
      I4 => data1(5),
      I5 => \pc_s_reg[7]_i_2_n_0\,
      O => \pc_s__0\(5)
    );
\pc_s_reg[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFB380B380B380"
    )
        port map (
      I0 => temp_signal(5),
      I1 => pc_s_n_0,
      I2 => instruction_address_reg(6),
      I3 => pc_s_reg(6),
      I4 => data1(6),
      I5 => \pc_s_reg[7]_i_2_n_0\,
      O => \pc_s__0\(6)
    );
\pc_s_reg[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFB380B380B380"
    )
        port map (
      I0 => temp_signal(5),
      I1 => pc_s_n_0,
      I2 => instruction_address_reg(7),
      I3 => pc_s_reg(7),
      I4 => data1(7),
      I5 => \pc_s_reg[7]_i_2_n_0\,
      O => \pc_s__0\(7)
    );
\pc_s_reg[7]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => output_data_4_s_reg,
      I1 => output_data_3_s_reg,
      I2 => output_data_2_s_reg,
      I3 => output_data_0_s_reg,
      I4 => output_data_1_s_reg,
      I5 => pc_s_n_0,
      O => \pc_s_reg[7]_i_2_n_0\
    );
\pc_s_reg[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \pc_s_reg[28]_i_3_n_0\,
      I1 => instruction_address_reg(8),
      I2 => \pc_s_reg[14]_i_3_n_0\,
      I3 => pc_s_reg(8),
      I4 => data1(8),
      I5 => \pc_s_reg[14]_i_2_n_0\,
      O => \pc_s__0\(8)
    );
\pc_s_reg[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF888F888F888"
    )
        port map (
      I0 => \pc_s_reg[28]_i_3_n_0\,
      I1 => instruction_address_reg(9),
      I2 => \pc_s_reg[14]_i_3_n_0\,
      I3 => pc_s_reg(9),
      I4 => data1(9),
      I5 => \pc_s_reg[14]_i_2_n_0\,
      O => \pc_s__0\(9)
    );
\pc_s_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \pc_s__0\(0),
      Q => pc_s_reg(0),
      R => reset
    );
\pc_s_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \pc_s__0\(10),
      Q => pc_s_reg(10),
      R => reset
    );
\pc_s_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \pc_s__0\(11),
      Q => pc_s_reg(11),
      R => reset
    );
\pc_s_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \pc_s__0\(12),
      Q => pc_s_reg(12),
      R => reset
    );
\pc_s_reg_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \pc_s__0\(13),
      Q => pc_s_reg(13),
      R => reset
    );
\pc_s_reg_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \pc_s__0__0\(14),
      Q => pc_s_reg(14),
      R => reset
    );
\pc_s_reg_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \pc_s__0\(15),
      Q => pc_s_reg(15),
      R => reset
    );
\pc_s_reg_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \pc_s__0\(16),
      Q => pc_s_reg(16),
      R => reset
    );
\pc_s_reg_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \pc_s__0\(17),
      Q => pc_s_reg(17),
      R => reset
    );
\pc_s_reg_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \pc_s__0\(18),
      Q => pc_s_reg(18),
      R => reset
    );
\pc_s_reg_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \pc_s__0\(19),
      Q => pc_s_reg(19),
      R => reset
    );
\pc_s_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \pc_s__0\(1),
      Q => pc_s_reg(1),
      R => reset
    );
\pc_s_reg_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \pc_s__0\(20),
      Q => pc_s_reg(20),
      R => reset
    );
\pc_s_reg_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \^d\(0),
      Q => pc_s_reg(21),
      R => reset
    );
\pc_s_reg_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \pc_s__0\(22),
      Q => pc_s_reg(22),
      R => reset
    );
\pc_s_reg_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \pc_s__0\(23),
      Q => pc_s_reg(23),
      R => reset
    );
\pc_s_reg_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \pc_s__0\(24),
      Q => pc_s_reg(24),
      R => reset
    );
\pc_s_reg_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \pc_s__0\(25),
      Q => pc_s_reg(25),
      R => reset
    );
\pc_s_reg_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \pc_s__0\(26),
      Q => pc_s_reg(26),
      R => reset
    );
\pc_s_reg_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \pc_s__0\(27),
      Q => pc_s_reg(27),
      R => reset
    );
\pc_s_reg_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \^d\(1),
      Q => pc_s_reg(28),
      R => reset
    );
\pc_s_reg_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \pc_s__0\(29),
      Q => pc_s_reg(29),
      R => reset
    );
\pc_s_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \pc_s__0\(2),
      Q => pc_s_reg(2),
      R => reset
    );
\pc_s_reg_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \pc_s__0\(30),
      Q => pc_s_reg(30),
      R => reset
    );
\pc_s_reg_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \pc_s__0\(31),
      Q => pc_s_reg(31),
      R => reset
    );
\pc_s_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \pc_s__0\(3),
      Q => pc_s_reg(3),
      R => reset
    );
\pc_s_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \pc_s__0\(4),
      Q => pc_s_reg(4),
      R => reset
    );
\pc_s_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \pc_s__0\(5),
      Q => pc_s_reg(5),
      R => reset
    );
\pc_s_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \pc_s__0\(6),
      Q => pc_s_reg(6),
      R => reset
    );
\pc_s_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \pc_s__0\(7),
      Q => pc_s_reg(7),
      R => reset
    );
\pc_s_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \pc_s__0\(8),
      Q => pc_s_reg(8),
      R => reset
    );
\pc_s_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \pc_s__0\(9),
      Q => pc_s_reg(9),
      R => reset
    );
w_en_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F8"
    )
        port map (
      I0 => enable_reg_reg_reg_0,
      I1 => \^pc_reg[0]_0\,
      I2 => temp_signal(5),
      O => w_en_i_1_n_0
    );
\w_en_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \^out_en_b_i\,
      I1 => out_en_reg,
      O => out_en_s
    );
w_en_reg: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => w_en_i_1_n_0,
      Q => \^out_en_b_i\,
      R => reset
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_decodeur_traces_1_1_decode_i_sync is
  port (
    \out\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    Q : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \FSM_onehot_state_reg_reg[9]\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    D : out STD_LOGIC_VECTOR ( 0 to 0 );
    \output_data_1_reg[6]_0\ : out STD_LOGIC;
    \output_data_3_reg[6]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    \output_data_3_reg[7]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \state_reg_reg[0]\ : out STD_LOGIC;
    \state_reg_reg[2]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \FSM_onehot_state_reg_reg[1]_0\ : out STD_LOGIC;
    \FSM_onehot_state_reg_reg[1]_1\ : out STD_LOGIC;
    \state_reg_reg[0]_0\ : out STD_LOGIC;
    \state_reg_reg[0]_1\ : out STD_LOGIC;
    \state_reg_reg[1]\ : out STD_LOGIC;
    \FSM_onehot_state_reg_reg[0]_0\ : out STD_LOGIC;
    \state_reg_reg[0]_2\ : out STD_LOGIC;
    \state_reg_reg[1]_0\ : out STD_LOGIC;
    \FSM_onehot_state_reg_reg[6]_0\ : out STD_LOGIC;
    \output_data_2_reg[6]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \output_data_3_reg[6]_1\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \instruction_address_reg_reg[31]\ : out STD_LOGIC_VECTOR ( 31 downto 0 );
    \context_id_reg[31]\ : out STD_LOGIC_VECTOR ( 31 downto 0 );
    enable_reg_reg_reg : in STD_LOGIC;
    \FSM_onehot_state_reg_reg[6]_1\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \FSM_onehot_state_reg_reg[3]_0\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \pc_reg[28]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \waypoint_address_cs_reg_reg[1]\ : in STD_LOGIC;
    \waypoint_address_reg_reg[28]\ : in STD_LOGIC;
    \FSM_onehot_state_reg_reg[0]_1\ : in STD_LOGIC;
    \state_reg_reg[1]_1\ : in STD_LOGIC;
    \state_reg_reg[1]_2\ : in STD_LOGIC;
    \FSM_onehot_state_reg_reg[13]\ : in STD_LOGIC;
    enable_reg_reg_reg_0 : in STD_LOGIC;
    \state_reg_reg[1]_3\ : in STD_LOGIC;
    \state_reg_reg[0]_3\ : in STD_LOGIC;
    \state_reg_reg[2]_0\ : in STD_LOGIC;
    \FSM_onehot_state_reg_reg[1]_2\ : in STD_LOGIC;
    \FSM_onehot_state_reg_reg[2]_0\ : in STD_LOGIC;
    \FSM_onehot_state_reg_reg[7]_0\ : in STD_LOGIC;
    \state_reg_reg[3]\ : in STD_LOGIC;
    \state_reg_reg[1]_4\ : in STD_LOGIC;
    \state_reg_reg[2]_1\ : in STD_LOGIC;
    \state_reg_reg[1]_5\ : in STD_LOGIC;
    enable_reg_reg_reg_1 : in STD_LOGIC;
    \state_reg_reg[0]_4\ : in STD_LOGIC;
    \FSM_onehot_state_reg_reg[9]_0\ : in STD_LOGIC;
    \state_reg_reg[2]_2\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \pc_s_reg_reg[28]\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    reset : in STD_LOGIC;
    clk : in STD_LOGIC;
    \data_in_reg_reg[7]\ : in STD_LOGIC_VECTOR ( 7 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_decodeur_traces_1_1_decode_i_sync : entity is "decode_i_sync";
end design_1_decodeur_traces_1_1_decode_i_sync;

architecture STRUCTURE of design_1_decodeur_traces_1_1_decode_i_sync is
  signal \^e\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \FSM_onehot_state_reg[0]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg[0]_i_4__1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg[0]_i_5__1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg[0]_i_7_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg[1]_i_1__1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg[1]_i_2__1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg[1]_i_3__1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg[1]_i_4_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg[1]_i_5__0_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg[1]_i_9__0_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg[2]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg[2]_i_6_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg[6]_i_1__1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg[7]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg_reg_n_0_[1]\ : STD_LOGIC;
  attribute RTL_KEEP : string;
  attribute RTL_KEEP of \FSM_onehot_state_reg_reg_n_0_[1]\ : signal is "yes";
  signal \FSM_onehot_state_reg_reg_n_0_[2]\ : STD_LOGIC;
  attribute RTL_KEEP of \FSM_onehot_state_reg_reg_n_0_[2]\ : signal is "yes";
  signal \FSM_onehot_state_reg_reg_n_0_[3]\ : STD_LOGIC;
  attribute RTL_KEEP of \FSM_onehot_state_reg_reg_n_0_[3]\ : signal is "yes";
  signal \FSM_onehot_state_reg_reg_n_0_[4]\ : STD_LOGIC;
  attribute RTL_KEEP of \FSM_onehot_state_reg_reg_n_0_[4]\ : signal is "yes";
  signal \FSM_onehot_state_reg_reg_n_0_[5]\ : STD_LOGIC;
  attribute RTL_KEEP of \FSM_onehot_state_reg_reg_n_0_[5]\ : signal is "yes";
  signal \FSM_onehot_state_reg_reg_n_0_[6]\ : STD_LOGIC;
  attribute RTL_KEEP of \FSM_onehot_state_reg_reg_n_0_[6]\ : signal is "yes";
  signal \FSM_onehot_state_reg_reg_n_0_[7]\ : STD_LOGIC;
  attribute RTL_KEEP of \FSM_onehot_state_reg_reg_n_0_[7]\ : signal is "yes";
  signal \FSM_onehot_state_reg_reg_n_0_[8]\ : STD_LOGIC;
  attribute RTL_KEEP of \FSM_onehot_state_reg_reg_n_0_[8]\ : signal is "yes";
  signal \^q\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal count_ctxt : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \count_ctxt[0]_i_1_n_0\ : STD_LOGIC;
  signal \count_ctxt[1]_i_1_n_0\ : STD_LOGIC;
  signal en_0 : STD_LOGIC;
  signal en_1 : STD_LOGIC;
  signal en_2 : STD_LOGIC;
  signal en_4 : STD_LOGIC;
  signal en_5 : STD_LOGIC;
  signal en_6 : STD_LOGIC;
  signal \^out\ : STD_LOGIC_VECTOR ( 0 to 0 );
  attribute RTL_KEEP of \^out\ : signal is "yes";
  signal \^output_data_3_reg[7]_0\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \state_reg[0]_i_8_n_0\ : STD_LOGIC;
  signal \state_reg[0]_i_9_n_0\ : STD_LOGIC;
  signal \state_reg[2]_i_2_n_0\ : STD_LOGIC;
  signal \state_reg[2]_i_3_n_0\ : STD_LOGIC;
  signal \state_reg[2]_i_5_n_0\ : STD_LOGIC;
  signal \state_reg[2]_i_7_n_0\ : STD_LOGIC;
  signal \^state_reg_reg[0]_0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_onehot_state_reg[0]_i_7\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \FSM_onehot_state_reg[10]_i_1__0\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \FSM_onehot_state_reg[1]_i_2__0\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \FSM_onehot_state_reg[1]_i_3__1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \FSM_onehot_state_reg[1]_i_4\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \FSM_onehot_state_reg[1]_i_5__0\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \FSM_onehot_state_reg[1]_i_9__0\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \FSM_onehot_state_reg[2]_i_3\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \FSM_onehot_state_reg[2]_i_6\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \FSM_onehot_state_reg[6]_i_1__0\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \FSM_onehot_state_reg[9]_i_1__0\ : label is "soft_lutpair17";
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg_reg[0]\ : label is "i_sync_count_03:000001000,i_sync_count_04:000010000,i_sync_count_02:000000100,i_sync_count_01:000000010,wait_state:000000001,ctxtid_2:010000000,ctxtid_1:001000000,ctxtid_3:100000000,i_sync_ib:000100000";
  attribute KEEP : string;
  attribute KEEP of \FSM_onehot_state_reg_reg[0]\ : label is "yes";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg_reg[1]\ : label is "i_sync_count_03:000001000,i_sync_count_04:000010000,i_sync_count_02:000000100,i_sync_count_01:000000010,wait_state:000000001,ctxtid_2:010000000,ctxtid_1:001000000,ctxtid_3:100000000,i_sync_ib:000100000";
  attribute KEEP of \FSM_onehot_state_reg_reg[1]\ : label is "yes";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg_reg[2]\ : label is "i_sync_count_03:000001000,i_sync_count_04:000010000,i_sync_count_02:000000100,i_sync_count_01:000000010,wait_state:000000001,ctxtid_2:010000000,ctxtid_1:001000000,ctxtid_3:100000000,i_sync_ib:000100000";
  attribute KEEP of \FSM_onehot_state_reg_reg[2]\ : label is "yes";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg_reg[3]\ : label is "i_sync_count_03:000001000,i_sync_count_04:000010000,i_sync_count_02:000000100,i_sync_count_01:000000010,wait_state:000000001,ctxtid_2:010000000,ctxtid_1:001000000,ctxtid_3:100000000,i_sync_ib:000100000";
  attribute KEEP of \FSM_onehot_state_reg_reg[3]\ : label is "yes";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg_reg[4]\ : label is "i_sync_count_03:000001000,i_sync_count_04:000010000,i_sync_count_02:000000100,i_sync_count_01:000000010,wait_state:000000001,ctxtid_2:010000000,ctxtid_1:001000000,ctxtid_3:100000000,i_sync_ib:000100000";
  attribute KEEP of \FSM_onehot_state_reg_reg[4]\ : label is "yes";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg_reg[5]\ : label is "i_sync_count_03:000001000,i_sync_count_04:000010000,i_sync_count_02:000000100,i_sync_count_01:000000010,wait_state:000000001,ctxtid_2:010000000,ctxtid_1:001000000,ctxtid_3:100000000,i_sync_ib:000100000";
  attribute KEEP of \FSM_onehot_state_reg_reg[5]\ : label is "yes";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg_reg[6]\ : label is "i_sync_count_03:000001000,i_sync_count_04:000010000,i_sync_count_02:000000100,i_sync_count_01:000000010,wait_state:000000001,ctxtid_2:010000000,ctxtid_1:001000000,ctxtid_3:100000000,i_sync_ib:000100000";
  attribute KEEP of \FSM_onehot_state_reg_reg[6]\ : label is "yes";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg_reg[7]\ : label is "i_sync_count_03:000001000,i_sync_count_04:000010000,i_sync_count_02:000000100,i_sync_count_01:000000010,wait_state:000000001,ctxtid_2:010000000,ctxtid_1:001000000,ctxtid_3:100000000,i_sync_ib:000100000";
  attribute KEEP of \FSM_onehot_state_reg_reg[7]\ : label is "yes";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg_reg[8]\ : label is "i_sync_count_03:000001000,i_sync_count_04:000010000,i_sync_count_02:000000100,i_sync_count_01:000000010,wait_state:000000001,ctxtid_2:010000000,ctxtid_1:001000000,ctxtid_3:100000000,i_sync_ib:000100000";
  attribute KEEP of \FSM_onehot_state_reg_reg[8]\ : label is "yes";
  attribute SOFT_HLUTNM of \output_data_1[6]_i_5\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \output_data_2[6]_i_2\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \output_data_3[6]_i_2\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \state_reg[0]_i_7\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \state_reg[0]_i_8\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \state_reg[0]_i_9\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \state_reg[1]_i_3\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \state_reg[2]_i_2\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \state_reg[2]_i_7\ : label is "soft_lutpair19";
begin
  E(0) <= \^e\(0);
  Q(7 downto 0) <= \^q\(7 downto 0);
  \out\(0) <= \^out\(0);
  \output_data_3_reg[7]_0\(0) <= \^output_data_3_reg[7]_0\(0);
  \state_reg_reg[0]_0\ <= \^state_reg_reg[0]_0\;
\FSM_onehot_state_reg[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \FSM_onehot_state_reg[0]_i_2_n_0\,
      I1 => \FSM_onehot_state_reg_reg[0]_1\,
      I2 => \FSM_onehot_state_reg[0]_i_4__1_n_0\,
      I3 => \FSM_onehot_state_reg[0]_i_5__1_n_0\,
      I4 => \state_reg_reg[1]_1\,
      I5 => \^e\(0),
      O => \FSM_onehot_state_reg[0]_i_1_n_0\
    );
\FSM_onehot_state_reg[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"44F444F4FFFF44F4"
    )
        port map (
      I0 => \FSM_onehot_state_reg[0]_i_7_n_0\,
      I1 => \FSM_onehot_state_reg_reg_n_0_[7]\,
      I2 => \state_reg_reg[1]_2\,
      I3 => \FSM_onehot_state_reg_reg[13]\,
      I4 => \^out\(0),
      I5 => enable_reg_reg_reg,
      O => \FSM_onehot_state_reg[0]_i_2_n_0\
    );
\FSM_onehot_state_reg[0]_i_4__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \FSM_onehot_state_reg_reg_n_0_[6]\,
      I1 => enable_reg_reg_reg,
      O => \FSM_onehot_state_reg[0]_i_4__1_n_0\
    );
\FSM_onehot_state_reg[0]_i_5__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8888888888888088"
    )
        port map (
      I0 => \^out\(0),
      I1 => \state_reg_reg[1]_5\,
      I2 => \^q\(0),
      I3 => \^q\(3),
      I4 => \FSM_onehot_state_reg[1]_i_5__0_n_0\,
      I5 => \^q\(7),
      O => \FSM_onehot_state_reg[0]_i_5__1_n_0\
    );
\FSM_onehot_state_reg[0]_i_6__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFEF"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(3),
      I2 => \^q\(4),
      I3 => \^q\(2),
      I4 => \state_reg[2]_i_7_n_0\,
      I5 => \^q\(7),
      O => \FSM_onehot_state_reg_reg[0]_0\
    );
\FSM_onehot_state_reg[0]_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F7"
    )
        port map (
      I0 => enable_reg_reg_reg,
      I1 => count_ctxt(0),
      I2 => count_ctxt(1),
      O => \FSM_onehot_state_reg[0]_i_7_n_0\
    );
\FSM_onehot_state_reg[0]_i_8\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \^out\(0),
      I1 => \state_reg_reg[2]_2\(0),
      O => \state_reg_reg[0]_2\
    );
\FSM_onehot_state_reg[10]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \^q\(7),
      I1 => enable_reg_reg_reg,
      I2 => \FSM_onehot_state_reg_reg[3]_0\(1),
      O => D(0)
    );
\FSM_onehot_state_reg[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEAFFEAFFEAEAEA"
    )
        port map (
      I0 => enable_reg_reg_reg_0,
      I1 => \FSM_onehot_state_reg[1]_i_3__1_n_0\,
      I2 => \state_reg_reg[1]_3\,
      I3 => \FSM_onehot_state_reg[1]_i_4_n_0\,
      I4 => \state_reg_reg[0]_3\,
      I5 => \state_reg_reg[2]_0\,
      O => \FSM_onehot_state_reg_reg[9]\(0)
    );
\FSM_onehot_state_reg[1]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"88F8FFFF88F88888"
    )
        port map (
      I0 => \FSM_onehot_state_reg[1]_i_2__1_n_0\,
      I1 => \state_reg_reg[2]_1\,
      I2 => \^out\(0),
      I3 => \state_reg_reg[1]_5\,
      I4 => enable_reg_reg_reg,
      I5 => \FSM_onehot_state_reg_reg_n_0_[1]\,
      O => \FSM_onehot_state_reg[1]_i_1__1_n_0\
    );
\FSM_onehot_state_reg[1]_i_2__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0400"
    )
        port map (
      I0 => \^state_reg_reg[0]_0\,
      I1 => enable_reg_reg_reg,
      I2 => \^q\(7),
      I3 => \FSM_onehot_state_reg_reg[9]_0\,
      O => \FSM_onehot_state_reg_reg[1]_1\
    );
\FSM_onehot_state_reg[1]_i_2__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000004000000"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(3),
      I2 => \^q\(7),
      I3 => enable_reg_reg_reg,
      I4 => \^out\(0),
      I5 => \FSM_onehot_state_reg[1]_i_5__0_n_0\,
      O => \FSM_onehot_state_reg[1]_i_2__1_n_0\
    );
\FSM_onehot_state_reg[1]_i_3__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \^q\(7),
      I1 => enable_reg_reg_reg,
      I2 => \FSM_onehot_state_reg_reg[6]_1\(0),
      O => \FSM_onehot_state_reg[1]_i_3__1_n_0\
    );
\FSM_onehot_state_reg[1]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"55400000"
    )
        port map (
      I0 => \^q\(7),
      I1 => enable_reg_reg_reg,
      I2 => \FSM_onehot_state_reg_reg[6]_1\(0),
      I3 => \FSM_onehot_state_reg_reg[1]_2\,
      I4 => \^q\(0),
      O => \FSM_onehot_state_reg[1]_i_4_n_0\
    );
\FSM_onehot_state_reg[1]_i_5__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(5),
      I2 => \^q\(6),
      I3 => \^q\(4),
      I4 => \^q\(2),
      O => \FSM_onehot_state_reg[1]_i_5__0_n_0\
    );
\FSM_onehot_state_reg[1]_i_6__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000010000000000"
    )
        port map (
      I0 => \FSM_onehot_state_reg[1]_i_9__0_n_0\,
      I1 => \^q\(3),
      I2 => \^q\(0),
      I3 => enable_reg_reg_reg,
      I4 => \^q\(7),
      I5 => \FSM_onehot_state_reg_reg[3]_0\(0),
      O => \FSM_onehot_state_reg_reg[1]_0\
    );
\FSM_onehot_state_reg[1]_i_9__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DFFFFFFF"
    )
        port map (
      I0 => \^q\(4),
      I1 => \^q\(2),
      I2 => \^q\(1),
      I3 => \^q\(5),
      I4 => \^q\(6),
      O => \FSM_onehot_state_reg[1]_i_9__0_n_0\
    );
\FSM_onehot_state_reg[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFEEEAEEEAEEEA"
    )
        port map (
      I0 => \FSM_onehot_state_reg_reg[2]_0\,
      I1 => \FSM_onehot_state_reg[2]_i_3_n_0\,
      I2 => \state_reg_reg[0]_3\,
      I3 => \state_reg_reg[2]_0\,
      I4 => \FSM_onehot_state_reg[2]_i_6_n_0\,
      I5 => \state_reg_reg[1]_3\,
      O => \FSM_onehot_state_reg_reg[9]\(1)
    );
\FSM_onehot_state_reg[2]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80808000"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(7),
      I2 => enable_reg_reg_reg,
      I3 => \FSM_onehot_state_reg_reg[6]_1\(0),
      I4 => \FSM_onehot_state_reg_reg[1]_2\,
      O => \FSM_onehot_state_reg[2]_i_3_n_0\
    );
\FSM_onehot_state_reg[2]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => enable_reg_reg_reg,
      I1 => \^q\(7),
      I2 => \FSM_onehot_state_reg_reg[6]_1\(0),
      O => \FSM_onehot_state_reg[2]_i_6_n_0\
    );
\FSM_onehot_state_reg[6]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^q\(6),
      I1 => \FSM_onehot_state_reg_reg[6]_1\(2),
      O => \FSM_onehot_state_reg_reg[6]_0\
    );
\FSM_onehot_state_reg[6]_i_1__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \FSM_onehot_state_reg_reg_n_0_[6]\,
      I1 => enable_reg_reg_reg,
      O => \FSM_onehot_state_reg[6]_i_1__1_n_0\
    );
\FSM_onehot_state_reg[7]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF00"
    )
        port map (
      I0 => count_ctxt(1),
      I1 => count_ctxt(0),
      I2 => enable_reg_reg_reg,
      I3 => \FSM_onehot_state_reg_reg_n_0_[7]\,
      O => \FSM_onehot_state_reg[7]_i_1_n_0\
    );
\FSM_onehot_state_reg[8]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFAA2AAA"
    )
        port map (
      I0 => \FSM_onehot_state_reg_reg_n_0_[8]\,
      I1 => count_ctxt(0),
      I2 => count_ctxt(1),
      I3 => enable_reg_reg_reg,
      I4 => \FSM_onehot_state_reg_reg_n_0_[5]\,
      O => \FSM_onehot_state_reg[8]_i_1_n_0\
    );
\FSM_onehot_state_reg[9]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \^q\(7),
      I1 => enable_reg_reg_reg,
      I2 => \FSM_onehot_state_reg_reg[6]_1\(3),
      O => \FSM_onehot_state_reg_reg[9]\(2)
    );
\FSM_onehot_state_reg_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk,
      CE => '1',
      D => \FSM_onehot_state_reg[0]_i_1_n_0\,
      Q => \^out\(0),
      S => reset
    );
\FSM_onehot_state_reg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \FSM_onehot_state_reg[1]_i_1__1_n_0\,
      Q => \FSM_onehot_state_reg_reg_n_0_[1]\,
      R => reset
    );
\FSM_onehot_state_reg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => enable_reg_reg_reg,
      D => \FSM_onehot_state_reg_reg_n_0_[1]\,
      Q => \FSM_onehot_state_reg_reg_n_0_[2]\,
      R => reset
    );
\FSM_onehot_state_reg_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => enable_reg_reg_reg,
      D => \FSM_onehot_state_reg_reg_n_0_[2]\,
      Q => \FSM_onehot_state_reg_reg_n_0_[3]\,
      R => reset
    );
\FSM_onehot_state_reg_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => enable_reg_reg_reg,
      D => \FSM_onehot_state_reg_reg_n_0_[3]\,
      Q => \FSM_onehot_state_reg_reg_n_0_[4]\,
      R => reset
    );
\FSM_onehot_state_reg_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => enable_reg_reg_reg,
      D => \FSM_onehot_state_reg_reg_n_0_[4]\,
      Q => \FSM_onehot_state_reg_reg_n_0_[5]\,
      R => reset
    );
\FSM_onehot_state_reg_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \FSM_onehot_state_reg[6]_i_1__1_n_0\,
      Q => \FSM_onehot_state_reg_reg_n_0_[6]\,
      R => reset
    );
\FSM_onehot_state_reg_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \FSM_onehot_state_reg[7]_i_1_n_0\,
      Q => \FSM_onehot_state_reg_reg_n_0_[7]\,
      R => reset
    );
\FSM_onehot_state_reg_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \FSM_onehot_state_reg[8]_i_1_n_0\,
      Q => \FSM_onehot_state_reg_reg_n_0_[8]\,
      R => reset
    );
\count_ctxt[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"37C8"
    )
        port map (
      I0 => \FSM_onehot_state_reg_reg_n_0_[8]\,
      I1 => enable_reg_reg_reg,
      I2 => \FSM_onehot_state_reg_reg_n_0_[7]\,
      I3 => count_ctxt(0),
      O => \count_ctxt[0]_i_1_n_0\
    );
\count_ctxt[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"5F7FA080"
    )
        port map (
      I0 => count_ctxt(0),
      I1 => \FSM_onehot_state_reg_reg_n_0_[7]\,
      I2 => enable_reg_reg_reg,
      I3 => \FSM_onehot_state_reg_reg_n_0_[8]\,
      I4 => count_ctxt(1),
      O => \count_ctxt[1]_i_1_n_0\
    );
\count_ctxt_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \count_ctxt[0]_i_1_n_0\,
      Q => count_ctxt(0),
      R => reset
    );
\count_ctxt_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \count_ctxt[1]_i_1_n_0\,
      Q => count_ctxt(1),
      R => reset
    );
\data_reg_s_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \data_in_reg_reg[7]\(0),
      Q => \^q\(0),
      R => reset
    );
\data_reg_s_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \data_in_reg_reg[7]\(1),
      Q => \^q\(1),
      R => reset
    );
\data_reg_s_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \data_in_reg_reg[7]\(2),
      Q => \^q\(2),
      R => reset
    );
\data_reg_s_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \data_in_reg_reg[7]\(3),
      Q => \^q\(3),
      R => reset
    );
\data_reg_s_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \data_in_reg_reg[7]\(4),
      Q => \^q\(4),
      R => reset
    );
\data_reg_s_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \data_in_reg_reg[7]\(5),
      Q => \^q\(5),
      R => reset
    );
\data_reg_s_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \data_in_reg_reg[7]\(6),
      Q => \^q\(6),
      R => reset
    );
\data_reg_s_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \data_in_reg_reg[7]\(7),
      Q => \^q\(7),
      R => reset
    );
enable_i_reg_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \FSM_onehot_state_reg_reg_n_0_[4]\,
      I1 => enable_reg_reg_reg,
      O => \^output_data_3_reg[7]_0\(0)
    );
\output_data_0[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \FSM_onehot_state_reg_reg_n_0_[1]\,
      I1 => enable_reg_reg_reg,
      O => en_0
    );
\output_data_0_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => en_0,
      D => \^q\(0),
      Q => \instruction_address_reg_reg[31]\(0),
      R => reset
    );
\output_data_0_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => en_0,
      D => \^q\(1),
      Q => \instruction_address_reg_reg[31]\(1),
      R => reset
    );
\output_data_0_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => en_0,
      D => \^q\(2),
      Q => \instruction_address_reg_reg[31]\(2),
      R => reset
    );
\output_data_0_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => en_0,
      D => \^q\(3),
      Q => \instruction_address_reg_reg[31]\(3),
      R => reset
    );
\output_data_0_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => en_0,
      D => \^q\(4),
      Q => \instruction_address_reg_reg[31]\(4),
      R => reset
    );
\output_data_0_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => en_0,
      D => \^q\(5),
      Q => \instruction_address_reg_reg[31]\(5),
      R => reset
    );
\output_data_0_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => en_0,
      D => \^q\(6),
      Q => \instruction_address_reg_reg[31]\(6),
      R => reset
    );
\output_data_0_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => en_0,
      D => \^q\(7),
      Q => \instruction_address_reg_reg[31]\(7),
      R => reset
    );
\output_data_1[6]_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BF"
    )
        port map (
      I0 => \^q\(7),
      I1 => enable_reg_reg_reg,
      I2 => \FSM_onehot_state_reg_reg[6]_1\(1),
      O => \output_data_1_reg[6]_0\
    );
\output_data_1[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \FSM_onehot_state_reg_reg_n_0_[2]\,
      I1 => enable_reg_reg_reg,
      O => en_1
    );
\output_data_1_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => en_1,
      D => \^q\(0),
      Q => \instruction_address_reg_reg[31]\(8),
      R => reset
    );
\output_data_1_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => en_1,
      D => \^q\(1),
      Q => \instruction_address_reg_reg[31]\(9),
      R => reset
    );
\output_data_1_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => en_1,
      D => \^q\(2),
      Q => \instruction_address_reg_reg[31]\(10),
      R => reset
    );
\output_data_1_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => en_1,
      D => \^q\(3),
      Q => \instruction_address_reg_reg[31]\(11),
      R => reset
    );
\output_data_1_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => en_1,
      D => \^q\(4),
      Q => \instruction_address_reg_reg[31]\(12),
      R => reset
    );
\output_data_1_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => en_1,
      D => \^q\(5),
      Q => \instruction_address_reg_reg[31]\(13),
      R => reset
    );
\output_data_1_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => en_1,
      D => \^q\(6),
      Q => \instruction_address_reg_reg[31]\(14),
      R => reset
    );
\output_data_1_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => en_1,
      D => \^q\(7),
      Q => \instruction_address_reg_reg[31]\(15),
      R => reset
    );
\output_data_2[6]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^q\(6),
      I1 => \^q\(7),
      I2 => \pc_s_reg_reg[28]\(0),
      O => \output_data_2_reg[6]_0\(0)
    );
\output_data_2[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \FSM_onehot_state_reg_reg_n_0_[3]\,
      I1 => enable_reg_reg_reg,
      O => en_2
    );
\output_data_2_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => en_2,
      D => \^q\(0),
      Q => \instruction_address_reg_reg[31]\(16),
      R => reset
    );
\output_data_2_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => en_2,
      D => \^q\(1),
      Q => \instruction_address_reg_reg[31]\(17),
      R => reset
    );
\output_data_2_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => en_2,
      D => \^q\(2),
      Q => \instruction_address_reg_reg[31]\(18),
      R => reset
    );
\output_data_2_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => en_2,
      D => \^q\(3),
      Q => \instruction_address_reg_reg[31]\(19),
      R => reset
    );
\output_data_2_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => en_2,
      D => \^q\(4),
      Q => \instruction_address_reg_reg[31]\(20),
      R => reset
    );
\output_data_2_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => en_2,
      D => \^q\(5),
      Q => \instruction_address_reg_reg[31]\(21),
      R => reset
    );
\output_data_2_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => en_2,
      D => \^q\(6),
      Q => \instruction_address_reg_reg[31]\(22),
      R => reset
    );
\output_data_2_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => en_2,
      D => \^q\(7),
      Q => \instruction_address_reg_reg[31]\(23),
      R => reset
    );
\output_data_3[6]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^q\(6),
      I1 => \^q\(7),
      I2 => \pc_s_reg_reg[28]\(1),
      O => \output_data_3_reg[6]_1\(0)
    );
\output_data_3[6]_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F2F2F2F2F2D0D0D0"
    )
        port map (
      I0 => enable_reg_reg_reg,
      I1 => \^q\(7),
      I2 => \^q\(6),
      I3 => \pc_reg[28]\(0),
      I4 => \waypoint_address_cs_reg_reg[1]\,
      I5 => \waypoint_address_reg_reg[28]\,
      O => \output_data_3_reg[6]_0\(0)
    );
\output_data_3_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^output_data_3_reg[7]_0\(0),
      D => \^q\(0),
      Q => \instruction_address_reg_reg[31]\(24),
      R => reset
    );
\output_data_3_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^output_data_3_reg[7]_0\(0),
      D => \^q\(1),
      Q => \instruction_address_reg_reg[31]\(25),
      R => reset
    );
\output_data_3_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^output_data_3_reg[7]_0\(0),
      D => \^q\(2),
      Q => \instruction_address_reg_reg[31]\(26),
      R => reset
    );
\output_data_3_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^output_data_3_reg[7]_0\(0),
      D => \^q\(3),
      Q => \instruction_address_reg_reg[31]\(27),
      R => reset
    );
\output_data_3_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^output_data_3_reg[7]_0\(0),
      D => \^q\(4),
      Q => \instruction_address_reg_reg[31]\(28),
      R => reset
    );
\output_data_3_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^output_data_3_reg[7]_0\(0),
      D => \^q\(5),
      Q => \instruction_address_reg_reg[31]\(29),
      R => reset
    );
\output_data_3_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^output_data_3_reg[7]_0\(0),
      D => \^q\(6),
      Q => \instruction_address_reg_reg[31]\(30),
      R => reset
    );
\output_data_3_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^output_data_3_reg[7]_0\(0),
      D => \^q\(7),
      Q => \instruction_address_reg_reg[31]\(31),
      R => reset
    );
\output_data_4[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AB00AA00"
    )
        port map (
      I0 => \FSM_onehot_state_reg_reg_n_0_[6]\,
      I1 => count_ctxt(0),
      I2 => count_ctxt(1),
      I3 => enable_reg_reg_reg,
      I4 => \FSM_onehot_state_reg_reg_n_0_[8]\,
      O => en_4
    );
\output_data_4_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => en_4,
      D => \^q\(0),
      Q => \context_id_reg[31]\(0),
      R => reset
    );
\output_data_4_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => en_4,
      D => \^q\(1),
      Q => \context_id_reg[31]\(1),
      R => reset
    );
\output_data_4_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => en_4,
      D => \^q\(2),
      Q => \context_id_reg[31]\(2),
      R => reset
    );
\output_data_4_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => en_4,
      D => \^q\(3),
      Q => \context_id_reg[31]\(3),
      R => reset
    );
\output_data_4_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => en_4,
      D => \^q\(4),
      Q => \context_id_reg[31]\(4),
      R => reset
    );
\output_data_4_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => en_4,
      D => \^q\(5),
      Q => \context_id_reg[31]\(5),
      R => reset
    );
\output_data_4_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => en_4,
      D => \^q\(6),
      Q => \context_id_reg[31]\(6),
      R => reset
    );
\output_data_4_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => en_4,
      D => \^q\(7),
      Q => \context_id_reg[31]\(7),
      R => reset
    );
\output_data_5[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"88C88888"
    )
        port map (
      I0 => \FSM_onehot_state_reg_reg_n_0_[7]\,
      I1 => enable_reg_reg_reg,
      I2 => count_ctxt(0),
      I3 => count_ctxt(1),
      I4 => \FSM_onehot_state_reg_reg_n_0_[8]\,
      O => en_5
    );
\output_data_5_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => en_5,
      D => \^q\(0),
      Q => \context_id_reg[31]\(8),
      R => reset
    );
\output_data_5_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => en_5,
      D => \^q\(1),
      Q => \context_id_reg[31]\(9),
      R => reset
    );
\output_data_5_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => en_5,
      D => \^q\(2),
      Q => \context_id_reg[31]\(10),
      R => reset
    );
\output_data_5_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => en_5,
      D => \^q\(3),
      Q => \context_id_reg[31]\(11),
      R => reset
    );
\output_data_5_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => en_5,
      D => \^q\(4),
      Q => \context_id_reg[31]\(12),
      R => reset
    );
\output_data_5_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => en_5,
      D => \^q\(5),
      Q => \context_id_reg[31]\(13),
      R => reset
    );
\output_data_5_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => en_5,
      D => \^q\(6),
      Q => \context_id_reg[31]\(14),
      R => reset
    );
\output_data_5_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => en_5,
      D => \^q\(7),
      Q => \context_id_reg[31]\(15),
      R => reset
    );
\output_data_6[7]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4000"
    )
        port map (
      I0 => count_ctxt(0),
      I1 => \FSM_onehot_state_reg_reg_n_0_[8]\,
      I2 => enable_reg_reg_reg,
      I3 => count_ctxt(1),
      O => en_6
    );
\output_data_6_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => en_6,
      D => \^q\(0),
      Q => \context_id_reg[31]\(16),
      R => reset
    );
\output_data_6_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => en_6,
      D => \^q\(1),
      Q => \context_id_reg[31]\(17),
      R => reset
    );
\output_data_6_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => en_6,
      D => \^q\(2),
      Q => \context_id_reg[31]\(18),
      R => reset
    );
\output_data_6_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => en_6,
      D => \^q\(3),
      Q => \context_id_reg[31]\(19),
      R => reset
    );
\output_data_6_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => en_6,
      D => \^q\(4),
      Q => \context_id_reg[31]\(20),
      R => reset
    );
\output_data_6_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => en_6,
      D => \^q\(5),
      Q => \context_id_reg[31]\(21),
      R => reset
    );
\output_data_6_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => en_6,
      D => \^q\(6),
      Q => \context_id_reg[31]\(22),
      R => reset
    );
\output_data_6_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => en_6,
      D => \^q\(7),
      Q => \context_id_reg[31]\(23),
      R => reset
    );
\output_data_7_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^e\(0),
      D => \^q\(0),
      Q => \context_id_reg[31]\(24),
      R => reset
    );
\output_data_7_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^e\(0),
      D => \^q\(1),
      Q => \context_id_reg[31]\(25),
      R => reset
    );
\output_data_7_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^e\(0),
      D => \^q\(2),
      Q => \context_id_reg[31]\(26),
      R => reset
    );
\output_data_7_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^e\(0),
      D => \^q\(3),
      Q => \context_id_reg[31]\(27),
      R => reset
    );
\output_data_7_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^e\(0),
      D => \^q\(4),
      Q => \context_id_reg[31]\(28),
      R => reset
    );
\output_data_7_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^e\(0),
      D => \^q\(5),
      Q => \context_id_reg[31]\(29),
      R => reset
    );
\output_data_7_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^e\(0),
      D => \^q\(6),
      Q => \context_id_reg[31]\(30),
      R => reset
    );
\output_data_7_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^e\(0),
      D => \^q\(7),
      Q => \context_id_reg[31]\(31),
      R => reset
    );
\state_reg[0]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFEF"
    )
        port map (
      I0 => \state_reg[2]_i_7_n_0\,
      I1 => \^q\(2),
      I2 => \^q\(4),
      I3 => \^q\(3),
      I4 => \^q\(0),
      O => \^state_reg_reg[0]_0\
    );
\state_reg[0]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF00A800A800"
    )
        port map (
      I0 => enable_reg_reg_reg,
      I1 => \FSM_onehot_state_reg_reg[7]_0\,
      I2 => \state_reg_reg[3]\,
      I3 => \state_reg[0]_i_8_n_0\,
      I4 => \state_reg[0]_i_9_n_0\,
      I5 => \state_reg_reg[1]_4\,
      O => \state_reg_reg[0]\
    );
\state_reg[0]_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000010"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(3),
      I2 => enable_reg_reg_reg,
      I3 => \^q\(7),
      I4 => \FSM_onehot_state_reg[1]_i_5__0_n_0\,
      O => \state_reg_reg[0]_1\
    );
\state_reg[0]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"5400"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(3),
      I2 => \FSM_onehot_state_reg[1]_i_5__0_n_0\,
      I3 => \^q\(7),
      O => \state_reg[0]_i_8_n_0\
    );
\state_reg[0]_i_9\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0001FFFF"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(3),
      I2 => \FSM_onehot_state_reg[1]_i_5__0_n_0\,
      I3 => \^q\(7),
      I4 => enable_reg_reg_reg,
      O => \state_reg[0]_i_9_n_0\
    );
\state_reg[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000004000000"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(3),
      I2 => \^q\(4),
      I3 => enable_reg_reg_reg,
      I4 => \^q\(2),
      I5 => \state_reg[2]_i_7_n_0\,
      O => \state_reg_reg[1]\
    );
\state_reg[1]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"44400040"
    )
        port map (
      I0 => \^q\(0),
      I1 => enable_reg_reg_reg,
      I2 => \^q\(3),
      I3 => \FSM_onehot_state_reg[1]_i_5__0_n_0\,
      I4 => \^q\(7),
      O => \state_reg_reg[1]_0\
    );
\state_reg[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFEF0F0"
    )
        port map (
      I0 => \state_reg[2]_i_2_n_0\,
      I1 => \state_reg[2]_i_3_n_0\,
      I2 => enable_reg_reg_reg_1,
      I3 => \state_reg[2]_i_5_n_0\,
      I4 => \state_reg_reg[2]_1\,
      I5 => \state_reg_reg[0]_4\,
      O => \state_reg_reg[2]\(0)
    );
\state_reg[2]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^q\(0),
      I1 => enable_reg_reg_reg,
      O => \state_reg[2]_i_2_n_0\
    );
\state_reg[2]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000400000000"
    )
        port map (
      I0 => \^q\(3),
      I1 => enable_reg_reg_reg,
      I2 => \^q\(7),
      I3 => \state_reg[2]_i_7_n_0\,
      I4 => \^q\(2),
      I5 => \^q\(4),
      O => \state_reg[2]_i_3_n_0\
    );
\state_reg[2]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000002000000"
    )
        port map (
      I0 => enable_reg_reg_reg,
      I1 => \^q\(7),
      I2 => \^q\(4),
      I3 => \^q\(3),
      I4 => \^q\(2),
      I5 => \state_reg[2]_i_7_n_0\,
      O => \state_reg[2]_i_5_n_0\
    );
\state_reg[2]_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"7F"
    )
        port map (
      I0 => \^q\(6),
      I1 => \^q\(5),
      I2 => \^q\(1),
      O => \state_reg[2]_i_7_n_0\
    );
w_ctxt_en_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => \FSM_onehot_state_reg_reg_n_0_[8]\,
      I1 => count_ctxt(1),
      I2 => count_ctxt(0),
      I3 => enable_reg_reg_reg,
      O => \^e\(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_decodeur_traces_1_1_decode_waypoint is
  port (
    \out\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    waypoint_address_en : out STD_LOGIC;
    \waypoint_address_cs_reg_reg[4]\ : out STD_LOGIC_VECTOR ( 4 downto 0 );
    \pc_s_reg_reg[28]\ : out STD_LOGIC;
    \FSM_onehot_state_reg_reg[0]_0\ : out STD_LOGIC;
    out_en_reg_0 : out STD_LOGIC;
    \FSM_onehot_state_reg_reg[1]_0\ : out STD_LOGIC;
    waypoint_address : out STD_LOGIC_VECTOR ( 22 downto 0 );
    clk : in STD_LOGIC;
    D : in STD_LOGIC_VECTOR ( 2 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 7 downto 0 );
    enable_reg_reg_reg : in STD_LOGIC;
    out_en_b_i : in STD_LOGIC;
    \waypoint_address_cs_reg_reg[3]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    reset : in STD_LOGIC;
    \data_reg_s_reg[2]\ : in STD_LOGIC;
    \data_in_reg_reg[7]\ : in STD_LOGIC_VECTOR ( 7 downto 0 );
    enable_reg_reg_reg_0 : in STD_LOGIC_VECTOR ( 0 to 0 );
    enable_reg_reg_reg_1 : in STD_LOGIC_VECTOR ( 0 to 0 );
    enable_reg_reg_reg_2 : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_decodeur_traces_1_1_decode_waypoint : entity is "decode_waypoint";
end design_1_decodeur_traces_1_1_decode_waypoint;

architecture STRUCTURE of design_1_decodeur_traces_1_1_decode_waypoint is
  signal \FSM_onehot_state_reg[11]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg[12]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg[1]_i_9_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg[2]_i_1__0_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg[3]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg[5]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg[6]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg[8]_i_1__0_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg[9]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg_reg_n_0_[10]\ : STD_LOGIC;
  attribute RTL_KEEP : string;
  attribute RTL_KEEP of \FSM_onehot_state_reg_reg_n_0_[10]\ : signal is "yes";
  signal \FSM_onehot_state_reg_reg_n_0_[11]\ : STD_LOGIC;
  attribute RTL_KEEP of \FSM_onehot_state_reg_reg_n_0_[11]\ : signal is "yes";
  signal \FSM_onehot_state_reg_reg_n_0_[12]\ : STD_LOGIC;
  attribute RTL_KEEP of \FSM_onehot_state_reg_reg_n_0_[12]\ : signal is "yes";
  signal \FSM_onehot_state_reg_reg_n_0_[2]\ : STD_LOGIC;
  attribute RTL_KEEP of \FSM_onehot_state_reg_reg_n_0_[2]\ : signal is "yes";
  signal \FSM_onehot_state_reg_reg_n_0_[4]\ : STD_LOGIC;
  attribute RTL_KEEP of \FSM_onehot_state_reg_reg_n_0_[4]\ : signal is "yes";
  signal \FSM_onehot_state_reg_reg_n_0_[5]\ : STD_LOGIC;
  attribute RTL_KEEP of \FSM_onehot_state_reg_reg_n_0_[5]\ : signal is "yes";
  signal \FSM_onehot_state_reg_reg_n_0_[6]\ : STD_LOGIC;
  attribute RTL_KEEP of \FSM_onehot_state_reg_reg_n_0_[6]\ : signal is "yes";
  signal \FSM_onehot_state_reg_reg_n_0_[7]\ : STD_LOGIC;
  attribute RTL_KEEP of \FSM_onehot_state_reg_reg_n_0_[7]\ : signal is "yes";
  signal \FSM_onehot_state_reg_reg_n_0_[8]\ : STD_LOGIC;
  attribute RTL_KEEP of \FSM_onehot_state_reg_reg_n_0_[8]\ : signal is "yes";
  signal \FSM_onehot_state_reg_reg_n_0_[9]\ : STD_LOGIC;
  attribute RTL_KEEP of \FSM_onehot_state_reg_reg_n_0_[9]\ : signal is "yes";
  signal en_0_reg_i_1_n_0 : STD_LOGIC;
  signal en_4 : STD_LOGIC;
  signal \^out\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  attribute RTL_KEEP of \^out\ : signal is "yes";
  signal out_en_i_1_n_0 : STD_LOGIC;
  signal \^out_en_reg_0\ : STD_LOGIC;
  signal \output_data_0[5]_i_1__0_n_0\ : STD_LOGIC;
  signal \output_data_1[6]_i_1_n_0\ : STD_LOGIC;
  signal \output_data_3[6]_i_1_n_0\ : STD_LOGIC;
  signal \output_data_4[0]_i_1_n_0\ : STD_LOGIC;
  signal \output_data_4[1]_i_1_n_0\ : STD_LOGIC;
  signal \output_data_4[2]_i_1_n_0\ : STD_LOGIC;
  signal stop_waypoint : STD_LOGIC;
  signal \^waypoint_address\ : STD_LOGIC_VECTOR ( 22 downto 0 );
  signal \^waypoint_address_en\ : STD_LOGIC;
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg_reg[0]\ : label is "waypoint_01:0000000000100,waypoint_10:0100000000000,waypoint_00:1000000000000,waypoint_ib:0000010000000,waypoint_4_ib:0000001000000,waypoint_4:0000100000000,waypoint:0000000000010,wait_state:0000000000001,waypoint_21:0000000010000,waypoint_31:0000000100000,waypoint_20:0010000000000,waypoint_30:0001000000000,waypoint_11:0000000001000";
  attribute KEEP : string;
  attribute KEEP of \FSM_onehot_state_reg_reg[0]\ : label is "yes";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg_reg[10]\ : label is "waypoint_01:0000000000100,waypoint_10:0100000000000,waypoint_00:1000000000000,waypoint_ib:0000010000000,waypoint_4_ib:0000001000000,waypoint_4:0000100000000,waypoint:0000000000010,wait_state:0000000000001,waypoint_21:0000000010000,waypoint_31:0000000100000,waypoint_20:0010000000000,waypoint_30:0001000000000,waypoint_11:0000000001000";
  attribute KEEP of \FSM_onehot_state_reg_reg[10]\ : label is "yes";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg_reg[11]\ : label is "waypoint_01:0000000000100,waypoint_10:0100000000000,waypoint_00:1000000000000,waypoint_ib:0000010000000,waypoint_4_ib:0000001000000,waypoint_4:0000100000000,waypoint:0000000000010,wait_state:0000000000001,waypoint_21:0000000010000,waypoint_31:0000000100000,waypoint_20:0010000000000,waypoint_30:0001000000000,waypoint_11:0000000001000";
  attribute KEEP of \FSM_onehot_state_reg_reg[11]\ : label is "yes";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg_reg[12]\ : label is "waypoint_01:0000000000100,waypoint_10:0100000000000,waypoint_00:1000000000000,waypoint_ib:0000010000000,waypoint_4_ib:0000001000000,waypoint_4:0000100000000,waypoint:0000000000010,wait_state:0000000000001,waypoint_21:0000000010000,waypoint_31:0000000100000,waypoint_20:0010000000000,waypoint_30:0001000000000,waypoint_11:0000000001000";
  attribute KEEP of \FSM_onehot_state_reg_reg[12]\ : label is "yes";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg_reg[1]\ : label is "waypoint_01:0000000000100,waypoint_10:0100000000000,waypoint_00:1000000000000,waypoint_ib:0000010000000,waypoint_4_ib:0000001000000,waypoint_4:0000100000000,waypoint:0000000000010,wait_state:0000000000001,waypoint_21:0000000010000,waypoint_31:0000000100000,waypoint_20:0010000000000,waypoint_30:0001000000000,waypoint_11:0000000001000";
  attribute KEEP of \FSM_onehot_state_reg_reg[1]\ : label is "yes";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg_reg[2]\ : label is "waypoint_01:0000000000100,waypoint_10:0100000000000,waypoint_00:1000000000000,waypoint_ib:0000010000000,waypoint_4_ib:0000001000000,waypoint_4:0000100000000,waypoint:0000000000010,wait_state:0000000000001,waypoint_21:0000000010000,waypoint_31:0000000100000,waypoint_20:0010000000000,waypoint_30:0001000000000,waypoint_11:0000000001000";
  attribute KEEP of \FSM_onehot_state_reg_reg[2]\ : label is "yes";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg_reg[3]\ : label is "waypoint_01:0000000000100,waypoint_10:0100000000000,waypoint_00:1000000000000,waypoint_ib:0000010000000,waypoint_4_ib:0000001000000,waypoint_4:0000100000000,waypoint:0000000000010,wait_state:0000000000001,waypoint_21:0000000010000,waypoint_31:0000000100000,waypoint_20:0010000000000,waypoint_30:0001000000000,waypoint_11:0000000001000";
  attribute KEEP of \FSM_onehot_state_reg_reg[3]\ : label is "yes";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg_reg[4]\ : label is "waypoint_01:0000000000100,waypoint_10:0100000000000,waypoint_00:1000000000000,waypoint_ib:0000010000000,waypoint_4_ib:0000001000000,waypoint_4:0000100000000,waypoint:0000000000010,wait_state:0000000000001,waypoint_21:0000000010000,waypoint_31:0000000100000,waypoint_20:0010000000000,waypoint_30:0001000000000,waypoint_11:0000000001000";
  attribute KEEP of \FSM_onehot_state_reg_reg[4]\ : label is "yes";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg_reg[5]\ : label is "waypoint_01:0000000000100,waypoint_10:0100000000000,waypoint_00:1000000000000,waypoint_ib:0000010000000,waypoint_4_ib:0000001000000,waypoint_4:0000100000000,waypoint:0000000000010,wait_state:0000000000001,waypoint_21:0000000010000,waypoint_31:0000000100000,waypoint_20:0010000000000,waypoint_30:0001000000000,waypoint_11:0000000001000";
  attribute KEEP of \FSM_onehot_state_reg_reg[5]\ : label is "yes";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg_reg[6]\ : label is "waypoint_01:0000000000100,waypoint_10:0100000000000,waypoint_00:1000000000000,waypoint_ib:0000010000000,waypoint_4_ib:0000001000000,waypoint_4:0000100000000,waypoint:0000000000010,wait_state:0000000000001,waypoint_21:0000000010000,waypoint_31:0000000100000,waypoint_20:0010000000000,waypoint_30:0001000000000,waypoint_11:0000000001000";
  attribute KEEP of \FSM_onehot_state_reg_reg[6]\ : label is "yes";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg_reg[7]\ : label is "waypoint_01:0000000000100,waypoint_10:0100000000000,waypoint_00:1000000000000,waypoint_ib:0000010000000,waypoint_4_ib:0000001000000,waypoint_4:0000100000000,waypoint:0000000000010,wait_state:0000000000001,waypoint_21:0000000010000,waypoint_31:0000000100000,waypoint_20:0010000000000,waypoint_30:0001000000000,waypoint_11:0000000001000";
  attribute KEEP of \FSM_onehot_state_reg_reg[7]\ : label is "yes";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg_reg[8]\ : label is "waypoint_01:0000000000100,waypoint_10:0100000000000,waypoint_00:1000000000000,waypoint_ib:0000010000000,waypoint_4_ib:0000001000000,waypoint_4:0000100000000,waypoint:0000000000010,wait_state:0000000000001,waypoint_21:0000000010000,waypoint_31:0000000100000,waypoint_20:0010000000000,waypoint_30:0001000000000,waypoint_11:0000000001000";
  attribute KEEP of \FSM_onehot_state_reg_reg[8]\ : label is "yes";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg_reg[9]\ : label is "waypoint_01:0000000000100,waypoint_10:0100000000000,waypoint_00:1000000000000,waypoint_ib:0000010000000,waypoint_4_ib:0000001000000,waypoint_4:0000100000000,waypoint:0000000000010,wait_state:0000000000001,waypoint_21:0000000010000,waypoint_31:0000000100000,waypoint_20:0010000000000,waypoint_30:0001000000000,waypoint_11:0000000001000";
  attribute KEEP of \FSM_onehot_state_reg_reg[9]\ : label is "yes";
begin
  \out\(2 downto 0) <= \^out\(2 downto 0);
  out_en_reg_0 <= \^out_en_reg_0\;
  waypoint_address(22 downto 0) <= \^waypoint_address\(22 downto 0);
  waypoint_address_en <= \^waypoint_address_en\;
\FSM_onehot_state_reg[0]_i_2__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FC00AAAA"
    )
        port map (
      I0 => \^out\(0),
      I1 => \data_reg_s_reg[2]\,
      I2 => Q(7),
      I3 => \^out_en_reg_0\,
      I4 => enable_reg_reg_reg,
      O => \FSM_onehot_state_reg_reg[0]_0\
    );
\FSM_onehot_state_reg[11]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => \FSM_onehot_state_reg_reg_n_0_[2]\,
      I1 => Q(7),
      I2 => enable_reg_reg_reg,
      O => \FSM_onehot_state_reg[11]_i_1_n_0\
    );
\FSM_onehot_state_reg[12]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0CAA"
    )
        port map (
      I0 => \FSM_onehot_state_reg[12]_i_2_n_0\,
      I1 => \^out\(1),
      I2 => Q(7),
      I3 => enable_reg_reg_reg,
      O => \FSM_onehot_state_reg[12]_i_1_n_0\
    );
\FSM_onehot_state_reg[12]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => \FSM_onehot_state_reg_reg_n_0_[8]\,
      I1 => \FSM_onehot_state_reg_reg_n_0_[11]\,
      I2 => \FSM_onehot_state_reg_reg_n_0_[12]\,
      I3 => \FSM_onehot_state_reg_reg_n_0_[10]\,
      I4 => \FSM_onehot_state_reg_reg_n_0_[9]\,
      O => \FSM_onehot_state_reg[12]_i_2_n_0\
    );
\FSM_onehot_state_reg[1]_i_7__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => \data_in_reg_reg[7]\(2),
      I1 => \data_in_reg_reg[7]\(3),
      I2 => \data_in_reg_reg[7]\(0),
      I3 => \data_in_reg_reg[7]\(1),
      I4 => \FSM_onehot_state_reg[1]_i_9_n_0\,
      O => \FSM_onehot_state_reg_reg[1]_0\
    );
\FSM_onehot_state_reg[1]_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \data_in_reg_reg[7]\(5),
      I1 => \data_in_reg_reg[7]\(4),
      I2 => \data_in_reg_reg[7]\(7),
      I3 => \data_in_reg_reg[7]\(6),
      O => \FSM_onehot_state_reg[1]_i_9_n_0\
    );
\FSM_onehot_state_reg[2]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8F80"
    )
        port map (
      I0 => \^out\(1),
      I1 => Q(7),
      I2 => enable_reg_reg_reg,
      I3 => \FSM_onehot_state_reg_reg_n_0_[2]\,
      O => \FSM_onehot_state_reg[2]_i_1__0_n_0\
    );
\FSM_onehot_state_reg[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8F80"
    )
        port map (
      I0 => \FSM_onehot_state_reg_reg_n_0_[2]\,
      I1 => Q(7),
      I2 => enable_reg_reg_reg,
      I3 => \^out\(2),
      O => \FSM_onehot_state_reg[3]_i_1_n_0\
    );
\FSM_onehot_state_reg[4]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8F80"
    )
        port map (
      I0 => \^out\(2),
      I1 => Q(7),
      I2 => enable_reg_reg_reg,
      I3 => \FSM_onehot_state_reg_reg_n_0_[4]\,
      O => \FSM_onehot_state_reg[4]_i_1_n_0\
    );
\FSM_onehot_state_reg[5]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8F80"
    )
        port map (
      I0 => \FSM_onehot_state_reg_reg_n_0_[4]\,
      I1 => Q(7),
      I2 => enable_reg_reg_reg,
      I3 => \FSM_onehot_state_reg_reg_n_0_[5]\,
      O => \FSM_onehot_state_reg[5]_i_1_n_0\
    );
\FSM_onehot_state_reg[6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8F80"
    )
        port map (
      I0 => Q(6),
      I1 => \FSM_onehot_state_reg_reg_n_0_[5]\,
      I2 => enable_reg_reg_reg,
      I3 => \FSM_onehot_state_reg_reg_n_0_[6]\,
      O => \FSM_onehot_state_reg[6]_i_1_n_0\
    );
\FSM_onehot_state_reg[8]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => enable_reg_reg_reg,
      I1 => \FSM_onehot_state_reg_reg_n_0_[5]\,
      I2 => Q(6),
      O => \FSM_onehot_state_reg[8]_i_1__0_n_0\
    );
\FSM_onehot_state_reg[9]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => \FSM_onehot_state_reg_reg_n_0_[4]\,
      I1 => Q(7),
      I2 => enable_reg_reg_reg,
      O => \FSM_onehot_state_reg[9]_i_1_n_0\
    );
\FSM_onehot_state_reg_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk,
      CE => '1',
      D => D(0),
      Q => \^out\(0),
      S => reset
    );
\FSM_onehot_state_reg_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => D(2),
      Q => \FSM_onehot_state_reg_reg_n_0_[10]\,
      R => reset
    );
\FSM_onehot_state_reg_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \FSM_onehot_state_reg[11]_i_1_n_0\,
      Q => \FSM_onehot_state_reg_reg_n_0_[11]\,
      R => reset
    );
\FSM_onehot_state_reg_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \FSM_onehot_state_reg[12]_i_1_n_0\,
      Q => \FSM_onehot_state_reg_reg_n_0_[12]\,
      R => reset
    );
\FSM_onehot_state_reg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => D(1),
      Q => \^out\(1),
      R => reset
    );
\FSM_onehot_state_reg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \FSM_onehot_state_reg[2]_i_1__0_n_0\,
      Q => \FSM_onehot_state_reg_reg_n_0_[2]\,
      R => reset
    );
\FSM_onehot_state_reg_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \FSM_onehot_state_reg[3]_i_1_n_0\,
      Q => \^out\(2),
      R => reset
    );
\FSM_onehot_state_reg_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \FSM_onehot_state_reg[4]_i_1_n_0\,
      Q => \FSM_onehot_state_reg_reg_n_0_[4]\,
      R => reset
    );
\FSM_onehot_state_reg_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \FSM_onehot_state_reg[5]_i_1_n_0\,
      Q => \FSM_onehot_state_reg_reg_n_0_[5]\,
      R => reset
    );
\FSM_onehot_state_reg_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \FSM_onehot_state_reg[6]_i_1_n_0\,
      Q => \FSM_onehot_state_reg_reg_n_0_[6]\,
      R => reset
    );
\FSM_onehot_state_reg_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => enable_reg_reg_reg,
      D => \FSM_onehot_state_reg_reg_n_0_[6]\,
      Q => \FSM_onehot_state_reg_reg_n_0_[7]\,
      R => reset
    );
\FSM_onehot_state_reg_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \FSM_onehot_state_reg[8]_i_1__0_n_0\,
      Q => \FSM_onehot_state_reg_reg_n_0_[8]\,
      R => reset
    );
\FSM_onehot_state_reg_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \FSM_onehot_state_reg[9]_i_1_n_0\,
      Q => \FSM_onehot_state_reg_reg_n_0_[9]\,
      R => reset
    );
en_0_reg_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => \^out\(1),
      I1 => Q(7),
      I2 => enable_reg_reg_reg,
      O => en_0_reg_i_1_n_0
    );
en_0_reg_reg: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => out_en_i_1_n_0,
      D => en_0_reg_i_1_n_0,
      Q => \waypoint_address_cs_reg_reg[4]\(4),
      R => '0'
    );
en_1_reg_reg: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => out_en_i_1_n_0,
      D => \FSM_onehot_state_reg[11]_i_1_n_0\,
      Q => \waypoint_address_cs_reg_reg[4]\(3),
      R => '0'
    );
en_2_reg_reg: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => out_en_i_1_n_0,
      D => D(2),
      Q => \waypoint_address_cs_reg_reg[4]\(2),
      R => '0'
    );
en_3_reg_reg: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => out_en_i_1_n_0,
      D => \FSM_onehot_state_reg[9]_i_1_n_0\,
      Q => \waypoint_address_cs_reg_reg[4]\(1),
      R => '0'
    );
en_4_reg_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \FSM_onehot_state_reg_reg_n_0_[5]\,
      I1 => enable_reg_reg_reg,
      O => en_4
    );
en_4_reg_reg: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => out_en_i_1_n_0,
      D => en_4,
      Q => \waypoint_address_cs_reg_reg[4]\(0),
      R => '0'
    );
out_en_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => reset,
      O => out_en_i_1_n_0
    );
out_en_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^out_en_reg_0\,
      I1 => enable_reg_reg_reg,
      O => stop_waypoint
    );
out_en_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \FSM_onehot_state_reg_reg_n_0_[9]\,
      I1 => \FSM_onehot_state_reg_reg_n_0_[10]\,
      I2 => \FSM_onehot_state_reg_reg_n_0_[12]\,
      I3 => \FSM_onehot_state_reg_reg_n_0_[11]\,
      I4 => \FSM_onehot_state_reg_reg_n_0_[8]\,
      I5 => \FSM_onehot_state_reg_reg_n_0_[7]\,
      O => \^out_en_reg_0\
    );
out_en_reg: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => out_en_i_1_n_0,
      D => stop_waypoint,
      Q => \^waypoint_address_en\,
      R => '0'
    );
\output_data_0[5]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => enable_reg_reg_reg,
      I1 => \^out\(1),
      O => \output_data_0[5]_i_1__0_n_0\
    );
\output_data_0_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \output_data_0[5]_i_1__0_n_0\,
      D => Q(1),
      Q => \^waypoint_address\(0),
      R => reset
    );
\output_data_0_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \output_data_0[5]_i_1__0_n_0\,
      D => Q(2),
      Q => \^waypoint_address\(1),
      R => reset
    );
\output_data_0_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \output_data_0[5]_i_1__0_n_0\,
      D => Q(3),
      Q => \^waypoint_address\(2),
      R => reset
    );
\output_data_0_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \output_data_0[5]_i_1__0_n_0\,
      D => Q(4),
      Q => \^waypoint_address\(3),
      R => reset
    );
\output_data_0_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \output_data_0[5]_i_1__0_n_0\,
      D => Q(5),
      Q => \^waypoint_address\(4),
      R => reset
    );
\output_data_0_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \output_data_0[5]_i_1__0_n_0\,
      D => enable_reg_reg_reg_0(0),
      Q => \^waypoint_address\(5),
      R => reset
    );
\output_data_1[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => enable_reg_reg_reg,
      I1 => \FSM_onehot_state_reg_reg_n_0_[2]\,
      O => \output_data_1[6]_i_1_n_0\
    );
\output_data_1_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \output_data_1[6]_i_1_n_0\,
      D => Q(0),
      Q => \^waypoint_address\(6),
      R => reset
    );
\output_data_1_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \output_data_1[6]_i_1_n_0\,
      D => Q(1),
      Q => \^waypoint_address\(7),
      R => reset
    );
\output_data_1_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \output_data_1[6]_i_1_n_0\,
      D => Q(2),
      Q => \^waypoint_address\(8),
      R => reset
    );
\output_data_1_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \output_data_1[6]_i_1_n_0\,
      D => Q(3),
      Q => \^waypoint_address\(9),
      R => reset
    );
\output_data_1_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \output_data_1[6]_i_1_n_0\,
      D => Q(4),
      Q => \^waypoint_address\(10),
      R => reset
    );
\output_data_1_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \output_data_1[6]_i_1_n_0\,
      D => Q(5),
      Q => \^waypoint_address\(11),
      R => reset
    );
\output_data_1_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \output_data_1[6]_i_1_n_0\,
      D => enable_reg_reg_reg_1(0),
      Q => \^waypoint_address\(12),
      R => reset
    );
\output_data_3[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => enable_reg_reg_reg,
      I1 => \FSM_onehot_state_reg_reg_n_0_[4]\,
      O => \output_data_3[6]_i_1_n_0\
    );
\output_data_3_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \output_data_3[6]_i_1_n_0\,
      D => Q(0),
      Q => \^waypoint_address\(13),
      R => reset
    );
\output_data_3_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \output_data_3[6]_i_1_n_0\,
      D => Q(1),
      Q => \^waypoint_address\(14),
      R => reset
    );
\output_data_3_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \output_data_3[6]_i_1_n_0\,
      D => Q(2),
      Q => \^waypoint_address\(15),
      R => reset
    );
\output_data_3_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \output_data_3[6]_i_1_n_0\,
      D => Q(3),
      Q => \^waypoint_address\(16),
      R => reset
    );
\output_data_3_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \output_data_3[6]_i_1_n_0\,
      D => Q(4),
      Q => \^waypoint_address\(17),
      R => reset
    );
\output_data_3_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \output_data_3[6]_i_1_n_0\,
      D => Q(5),
      Q => \^waypoint_address\(18),
      R => reset
    );
\output_data_3_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \output_data_3[6]_i_1_n_0\,
      D => enable_reg_reg_reg_2(0),
      Q => \^waypoint_address\(19),
      R => reset
    );
\output_data_4[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFBF0080"
    )
        port map (
      I0 => Q(0),
      I1 => enable_reg_reg_reg,
      I2 => \FSM_onehot_state_reg_reg_n_0_[5]\,
      I3 => reset,
      I4 => \^waypoint_address\(20),
      O => \output_data_4[0]_i_1_n_0\
    );
\output_data_4[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFBF0080"
    )
        port map (
      I0 => Q(1),
      I1 => enable_reg_reg_reg,
      I2 => \FSM_onehot_state_reg_reg_n_0_[5]\,
      I3 => reset,
      I4 => \^waypoint_address\(21),
      O => \output_data_4[1]_i_1_n_0\
    );
\output_data_4[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFBF0080"
    )
        port map (
      I0 => Q(2),
      I1 => enable_reg_reg_reg,
      I2 => \FSM_onehot_state_reg_reg_n_0_[5]\,
      I3 => reset,
      I4 => \^waypoint_address\(22),
      O => \output_data_4[2]_i_1_n_0\
    );
\output_data_4_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \output_data_4[0]_i_1_n_0\,
      Q => \^waypoint_address\(20),
      R => '0'
    );
\output_data_4_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \output_data_4[1]_i_1_n_0\,
      Q => \^waypoint_address\(21),
      R => '0'
    );
\output_data_4_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => \output_data_4[2]_i_1_n_0\,
      Q => \^waypoint_address\(22),
      R => '0'
    );
\pc_s_reg[28]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \^waypoint_address_en\,
      I1 => out_en_b_i,
      I2 => \waypoint_address_cs_reg_reg[3]\(0),
      O => \pc_s_reg_reg[28]\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_decodeur_traces_1_1_pft_decoder_v2 is
  port (
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    output_data_0_s_reg_reg : out STD_LOGIC;
    output_data_0_s_reg_reg_0 : out STD_LOGIC;
    output_data_0_s_reg_reg_1 : out STD_LOGIC;
    \FSM_onehot_state_reg_reg[1]\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    Q : out STD_LOGIC_VECTOR ( 0 to 0 );
    \state_reg_reg[0]_0\ : out STD_LOGIC;
    \state_reg_reg[0]_1\ : out STD_LOGIC;
    \state_reg_reg[0]_2\ : out STD_LOGIC;
    \state_reg_reg[1]_0\ : out STD_LOGIC;
    \FSM_onehot_state_reg_reg[0]\ : out STD_LOGIC;
    \FSM_onehot_state_reg_reg[0]_0\ : out STD_LOGIC;
    \state_reg_reg[0]_3\ : out STD_LOGIC;
    \FSM_onehot_state_reg_reg[0]_1\ : out STD_LOGIC;
    \FSM_onehot_state_reg_reg[1]_0\ : out STD_LOGIC;
    \state_reg_reg[0]_4\ : out STD_LOGIC;
    \state_reg_reg[2]_0\ : out STD_LOGIC;
    \FSM_onehot_state_reg_reg[0]_2\ : out STD_LOGIC;
    \data_reg_s_reg[0]\ : in STD_LOGIC;
    enable_reg_reg_reg : in STD_LOGIC;
    \FSM_onehot_state_reg_reg[7]\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    enable_reg_reg_reg_0 : in STD_LOGIC;
    \FSM_onehot_state_reg_reg[1]_1\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \FSM_onehot_state_reg_reg[7]_0\ : in STD_LOGIC;
    \data_reg_s_reg[3]\ : in STD_LOGIC;
    \data_in_reg_reg[2]\ : in STD_LOGIC;
    D : in STD_LOGIC_VECTOR ( 0 to 0 );
    \data_reg_s_reg[2]\ : in STD_LOGIC;
    enable_reg_reg_reg_1 : in STD_LOGIC;
    \data_reg_s_reg[0]_0\ : in STD_LOGIC;
    \FSM_onehot_state_reg_reg[13]\ : in STD_LOGIC;
    \data_reg_s_reg[0]_1\ : in STD_LOGIC;
    \data_reg_s_reg[0]_2\ : in STD_LOGIC;
    \out\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \FSM_onehot_state_reg_reg[0]_3\ : in STD_LOGIC;
    \FSM_onehot_state_reg_reg[1]_2\ : in STD_LOGIC;
    \FSM_onehot_state_reg_reg[9]\ : in STD_LOGIC;
    \FSM_onehot_state_reg_reg[0]_4\ : in STD_LOGIC;
    \data_reg_s_reg[0]_3\ : in STD_LOGIC;
    reset : in STD_LOGIC;
    clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_decodeur_traces_1_1_pft_decoder_v2 : entity is "pft_decoder_v2";
end design_1_decodeur_traces_1_1_pft_decoder_v2;

architecture STRUCTURE of design_1_decodeur_traces_1_1_pft_decoder_v2 is
  signal \FSM_onehot_state_reg[0]_i_3__1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg[0]_i_4_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg[0]_i_5_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg[0]_i_7__0_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg[0]_i_9_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg[1]_i_3__0_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg[1]_i_4__0_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg[1]_i_5_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg[1]_i_7_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg[1]_i_8__0_n_0\ : STD_LOGIC;
  signal \^q\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^output_data_0_s_reg_reg\ : STD_LOGIC;
  signal \^output_data_0_s_reg_reg_0\ : STD_LOGIC;
  signal \^output_data_0_s_reg_reg_1\ : STD_LOGIC;
  signal state_next : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal state_reg : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \state_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \state_reg[0]_i_4_n_0\ : STD_LOGIC;
  signal \state_reg[1]_i_4_n_0\ : STD_LOGIC;
  signal \state_reg[3]_i_2_n_0\ : STD_LOGIC;
  signal \^state_reg_reg[0]_0\ : STD_LOGIC;
  signal \^state_reg_reg[0]_1\ : STD_LOGIC;
  signal \^state_reg_reg[0]_2\ : STD_LOGIC;
  signal \^state_reg_reg[1]_0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_onehot_state_reg[0]_i_3__1\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \FSM_onehot_state_reg[0]_i_5__0\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \FSM_onehot_state_reg[0]_i_7__0\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \FSM_onehot_state_reg[0]_i_8__0\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \FSM_onehot_state_reg[1]_i_10\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \FSM_onehot_state_reg[1]_i_3__0\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \FSM_onehot_state_reg[1]_i_4__0\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \FSM_onehot_state_reg[1]_i_4__1\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \FSM_onehot_state_reg[1]_i_8__0\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \FSM_onehot_state_reg[2]_i_4\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \state_reg[0]_i_10\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \state_reg[0]_i_6\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \state_reg[1]_i_4\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \state_reg[2]_i_4\ : label is "soft_lutpair24";
begin
  Q(0) <= \^q\(0);
  output_data_0_s_reg_reg <= \^output_data_0_s_reg_reg\;
  output_data_0_s_reg_reg_0 <= \^output_data_0_s_reg_reg_0\;
  output_data_0_s_reg_reg_1 <= \^output_data_0_s_reg_reg_1\;
  \state_reg_reg[0]_0\ <= \^state_reg_reg[0]_0\;
  \state_reg_reg[0]_1\ <= \^state_reg_reg[0]_1\;
  \state_reg_reg[0]_2\ <= \^state_reg_reg[0]_2\;
  \state_reg_reg[1]_0\ <= \^state_reg_reg[1]_0\;
\FSM_onehot_state_reg[0]_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFAEA"
    )
        port map (
      I0 => \FSM_onehot_state_reg_reg[0]_3\,
      I1 => \FSM_onehot_state_reg[0]_i_3__1_n_0\,
      I2 => \FSM_onehot_state_reg_reg[1]_1\(0),
      I3 => \^output_data_0_s_reg_reg_1\,
      I4 => \FSM_onehot_state_reg[0]_i_4_n_0\,
      I5 => \FSM_onehot_state_reg[0]_i_5_n_0\,
      O => \FSM_onehot_state_reg_reg[1]\(0)
    );
\FSM_onehot_state_reg[0]_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0088080000880888"
    )
        port map (
      I0 => \out\(0),
      I1 => \FSM_onehot_state_reg[0]_i_9_n_0\,
      I2 => \FSM_onehot_state_reg_reg[9]\,
      I3 => state_reg(3),
      I4 => \^q\(0),
      I5 => \data_in_reg_reg[2]\,
      O => \FSM_onehot_state_reg_reg[0]_0\
    );
\FSM_onehot_state_reg[0]_i_3__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"04030000"
    )
        port map (
      I0 => \out\(0),
      I1 => state_reg(0),
      I2 => state_reg(3),
      I3 => \^q\(0),
      I4 => state_reg(1),
      O => \FSM_onehot_state_reg[0]_i_3__1_n_0\
    );
\FSM_onehot_state_reg[0]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000001000000"
    )
        port map (
      I0 => \^q\(0),
      I1 => state_reg(3),
      I2 => state_reg(1),
      I3 => state_reg(0),
      I4 => \FSM_onehot_state_reg_reg[1]_1\(0),
      I5 => \data_in_reg_reg[2]\,
      O => \FSM_onehot_state_reg[0]_i_4_n_0\
    );
\FSM_onehot_state_reg[0]_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5050000051FF0000"
    )
        port map (
      I0 => state_reg(1),
      I1 => \data_in_reg_reg[2]\,
      I2 => \FSM_onehot_state_reg[0]_i_7__0_n_0\,
      I3 => \FSM_onehot_state_reg_reg[0]_4\,
      I4 => state_reg(0),
      I5 => state_reg(3),
      O => \^state_reg_reg[0]_0\
    );
\FSM_onehot_state_reg[0]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AA82AAAA00000000"
    )
        port map (
      I0 => \FSM_onehot_state_reg_reg[1]_1\(0),
      I1 => \^q\(0),
      I2 => state_reg(3),
      I3 => state_reg(1),
      I4 => state_reg(0),
      I5 => \data_reg_s_reg[0]_3\,
      O => \FSM_onehot_state_reg[0]_i_5_n_0\
    );
\FSM_onehot_state_reg[0]_i_5__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFEEF"
    )
        port map (
      I0 => \FSM_onehot_state_reg_reg[7]\(1),
      I1 => state_reg(0),
      I2 => \^q\(0),
      I3 => state_reg(3),
      I4 => state_reg(1),
      O => \FSM_onehot_state_reg_reg[0]_1\
    );
\FSM_onehot_state_reg[0]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000001000"
    )
        port map (
      I0 => state_reg(1),
      I1 => \^q\(0),
      I2 => \out\(0),
      I3 => state_reg(3),
      I4 => state_reg(0),
      I5 => \FSM_onehot_state_reg_reg[13]\,
      O => \FSM_onehot_state_reg_reg[0]\
    );
\FSM_onehot_state_reg[0]_i_7__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"34"
    )
        port map (
      I0 => \FSM_onehot_state_reg_reg[9]\,
      I1 => state_reg(3),
      I2 => \^q\(0),
      O => \FSM_onehot_state_reg[0]_i_7__0_n_0\
    );
\FSM_onehot_state_reg[0]_i_8__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0400"
    )
        port map (
      I0 => state_reg(1),
      I1 => \out\(0),
      I2 => state_reg(3),
      I3 => \^q\(0),
      O => \FSM_onehot_state_reg_reg[0]_2\
    );
\FSM_onehot_state_reg[0]_i_9\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => state_reg(0),
      I1 => state_reg(1),
      O => \FSM_onehot_state_reg[0]_i_9_n_0\
    );
\FSM_onehot_state_reg[1]_i_10\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \^q\(0),
      I1 => state_reg(3),
      O => \state_reg_reg[0]_4\
    );
\FSM_onehot_state_reg[1]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFEFEFFFE"
    )
        port map (
      I0 => enable_reg_reg_reg_0,
      I1 => \FSM_onehot_state_reg[1]_i_3__0_n_0\,
      I2 => \FSM_onehot_state_reg[1]_i_4__0_n_0\,
      I3 => \FSM_onehot_state_reg_reg[1]_1\(1),
      I4 => enable_reg_reg_reg,
      I5 => \FSM_onehot_state_reg[1]_i_5_n_0\,
      O => \FSM_onehot_state_reg_reg[1]\(1)
    );
\FSM_onehot_state_reg[1]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF11F100F1"
    )
        port map (
      I0 => \^q\(0),
      I1 => state_reg(3),
      I2 => \FSM_onehot_state_reg_reg[13]\,
      I3 => state_reg(0),
      I4 => \data_in_reg_reg[2]\,
      I5 => \FSM_onehot_state_reg[1]_i_8__0_n_0\,
      O => \^state_reg_reg[1]_0\
    );
\FSM_onehot_state_reg[1]_i_3__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8AA00000"
    )
        port map (
      I0 => state_reg(1),
      I1 => \out\(0),
      I2 => state_reg(0),
      I3 => \^q\(0),
      I4 => \data_reg_s_reg[3]\,
      O => \FSM_onehot_state_reg[1]_i_3__0_n_0\
    );
\FSM_onehot_state_reg[1]_i_4__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A800"
    )
        port map (
      I0 => state_reg(3),
      I1 => state_reg(1),
      I2 => \^q\(0),
      I3 => \data_reg_s_reg[3]\,
      O => \FSM_onehot_state_reg[1]_i_4__0_n_0\
    );
\FSM_onehot_state_reg[1]_i_4__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => state_reg(1),
      I1 => \^q\(0),
      I2 => state_reg(3),
      I3 => state_reg(0),
      O => \FSM_onehot_state_reg_reg[1]_0\
    );
\FSM_onehot_state_reg[1]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAFEAAAAAAAEAAAA"
    )
        port map (
      I0 => \FSM_onehot_state_reg[1]_i_7_n_0\,
      I1 => \FSM_onehot_state_reg_reg[7]_0\,
      I2 => state_reg(0),
      I3 => state_reg(1),
      I4 => \data_reg_s_reg[3]\,
      I5 => \data_in_reg_reg[2]\,
      O => \FSM_onehot_state_reg[1]_i_5_n_0\
    );
\FSM_onehot_state_reg[1]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000800000808000"
    )
        port map (
      I0 => \FSM_onehot_state_reg[0]_i_9_n_0\,
      I1 => enable_reg_reg_reg,
      I2 => \FSM_onehot_state_reg_reg[1]_1\(0),
      I3 => \^q\(0),
      I4 => state_reg(3),
      I5 => \FSM_onehot_state_reg_reg[9]\,
      O => \FSM_onehot_state_reg[1]_i_7_n_0\
    );
\FSM_onehot_state_reg[1]_i_8__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF8F0F0"
    )
        port map (
      I0 => \FSM_onehot_state_reg_reg[9]\,
      I1 => state_reg(0),
      I2 => state_reg(1),
      I3 => \^q\(0),
      I4 => state_reg(3),
      O => \FSM_onehot_state_reg[1]_i_8__0_n_0\
    );
\FSM_onehot_state_reg[2]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F5F5F151"
    )
        port map (
      I0 => state_reg(0),
      I1 => state_reg(1),
      I2 => state_reg(3),
      I3 => \FSM_onehot_state_reg_reg[9]\,
      I4 => \^q\(0),
      O => \^output_data_0_s_reg_reg_0\
    );
\FSM_onehot_state_reg[2]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"D0D0D5D000000000"
    )
        port map (
      I0 => \^q\(0),
      I1 => \out\(0),
      I2 => state_reg(1),
      I3 => \data_in_reg_reg[2]\,
      I4 => state_reg(3),
      I5 => state_reg(0),
      O => \^output_data_0_s_reg_reg\
    );
\FSM_onehot_state_reg[2]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000014"
    )
        port map (
      I0 => state_reg(1),
      I1 => \^q\(0),
      I2 => state_reg(3),
      I3 => state_reg(0),
      I4 => \FSM_onehot_state_reg_reg[1]_2\,
      I5 => \FSM_onehot_state_reg_reg[7]\(1),
      O => \^output_data_0_s_reg_reg_1\
    );
\output_data_0[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFE0E0E0E0E0E0E0"
    )
        port map (
      I0 => \^output_data_0_s_reg_reg\,
      I1 => \^output_data_0_s_reg_reg_0\,
      I2 => \data_reg_s_reg[0]\,
      I3 => \^output_data_0_s_reg_reg_1\,
      I4 => enable_reg_reg_reg,
      I5 => \FSM_onehot_state_reg_reg[7]\(0),
      O => E(0)
    );
\state_reg[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFEFFFEFE"
    )
        port map (
      I0 => \state_reg[0]_i_2_n_0\,
      I1 => \^state_reg_reg[0]_0\,
      I2 => \^state_reg_reg[0]_1\,
      I3 => \data_reg_s_reg[2]\,
      I4 => \state_reg[0]_i_4_n_0\,
      I5 => enable_reg_reg_reg_1,
      O => state_next(0)
    );
\state_reg[0]_i_10\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => state_reg(1),
      I1 => state_reg(0),
      I2 => \^q\(0),
      O => \state_reg_reg[0]_3\
    );
\state_reg[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF0404040404"
    )
        port map (
      I0 => enable_reg_reg_reg,
      I1 => state_reg(0),
      I2 => state_reg(3),
      I3 => \FSM_onehot_state_reg_reg[7]_0\,
      I4 => \^state_reg_reg[0]_2\,
      I5 => \data_reg_s_reg[0]_0\,
      O => \state_reg[0]_i_2_n_0\
    );
\state_reg[0]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCCCCCEACCC0CCEE"
    )
        port map (
      I0 => state_reg(0),
      I1 => enable_reg_reg_reg,
      I2 => \FSM_onehot_state_reg_reg[13]\,
      I3 => state_reg(1),
      I4 => \^q\(0),
      I5 => state_reg(3),
      O => \state_reg[0]_i_4_n_0\
    );
\state_reg[0]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F8"
    )
        port map (
      I0 => state_reg(3),
      I1 => \^q\(0),
      I2 => state_reg(1),
      O => \^state_reg_reg[0]_2\
    );
\state_reg[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFEAA"
    )
        port map (
      I0 => \FSM_onehot_state_reg[0]_i_3__1_n_0\,
      I1 => \data_reg_s_reg[0]_1\,
      I2 => \data_reg_s_reg[0]_2\,
      I3 => \^state_reg_reg[1]_0\,
      I4 => \state_reg[1]_i_4_n_0\,
      O => state_next(1)
    );
\state_reg[1]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => state_reg(3),
      I1 => state_reg(1),
      I2 => enable_reg_reg_reg,
      O => \state_reg[1]_i_4_n_0\
    );
\state_reg[2]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00020000"
    )
        port map (
      I0 => enable_reg_reg_reg,
      I1 => state_reg(0),
      I2 => state_reg(3),
      I3 => \^q\(0),
      I4 => state_reg(1),
      O => \^state_reg_reg[0]_1\
    );
\state_reg[2]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000008000000FF00"
    )
        port map (
      I0 => state_reg(0),
      I1 => state_reg(1),
      I2 => \out\(0),
      I3 => \^q\(0),
      I4 => state_reg(3),
      I5 => enable_reg_reg_reg,
      O => \state_reg_reg[2]_0\
    );
\state_reg[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF01000000"
    )
        port map (
      I0 => \FSM_onehot_state_reg_reg[9]\,
      I1 => \^q\(0),
      I2 => state_reg(1),
      I3 => state_reg(0),
      I4 => state_reg(3),
      I5 => \state_reg[3]_i_2_n_0\,
      O => state_next(3)
    );
\state_reg[3]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000013331000000"
    )
        port map (
      I0 => \FSM_onehot_state_reg_reg[13]\,
      I1 => state_reg(1),
      I2 => state_reg(0),
      I3 => enable_reg_reg_reg,
      I4 => \^q\(0),
      I5 => state_reg(3),
      O => \state_reg[3]_i_2_n_0\
    );
\state_reg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => state_next(0),
      Q => state_reg(0),
      R => reset
    );
\state_reg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => state_next(1),
      Q => state_reg(1),
      R => reset
    );
\state_reg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => D(0),
      Q => \^q\(0),
      R => reset
    );
\state_reg_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => state_next(3),
      Q => state_reg(3),
      R => reset
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_decodeur_traces_1_1_datapath is
  port (
    \out\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \FSM_onehot_state_reg_reg[7]\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \output_data_0_reg[0]\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    waypoint_address_en : out STD_LOGIC;
    \output_data_7_reg[0]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \FSM_onehot_state_reg_reg[0]\ : out STD_LOGIC;
    \pc_reg[0]\ : out STD_LOGIC;
    output_data_0_s_reg_reg : out STD_LOGIC;
    \state_reg_reg[0]\ : out STD_LOGIC;
    \state_reg_reg[0]_0\ : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 0 to 0 );
    \FSM_onehot_state_reg_reg[1]\ : out STD_LOGIC;
    \FSM_onehot_state_reg_reg[1]_0\ : out STD_LOGIC;
    \state_reg_reg[0]_1\ : out STD_LOGIC;
    out_en_reg : out STD_LOGIC;
    \state_reg_reg[0]_2\ : out STD_LOGIC;
    pc : out STD_LOGIC_VECTOR ( 31 downto 0 );
    out_en_s : out STD_LOGIC;
    \FSM_onehot_state_reg_reg[0]_0\ : out STD_LOGIC;
    \FSM_onehot_state_reg_reg[1]_1\ : out STD_LOGIC;
    \state_reg_reg[1]\ : out STD_LOGIC;
    \FSM_onehot_state_reg_reg[0]_1\ : out STD_LOGIC;
    \state_reg_reg[0]_3\ : out STD_LOGIC;
    \state_reg_reg[1]_0\ : out STD_LOGIC;
    waypoint_address : out STD_LOGIC_VECTOR ( 22 downto 0 );
    \context_id_reg[31]\ : out STD_LOGIC_VECTOR ( 31 downto 0 );
    reset : in STD_LOGIC;
    clk : in STD_LOGIC;
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    enable_reg_reg_reg : in STD_LOGIC;
    \FSM_onehot_state_reg_reg[0]_2\ : in STD_LOGIC;
    \state_reg_reg[1]_1\ : in STD_LOGIC;
    \state_reg_reg[1]_2\ : in STD_LOGIC;
    \state_reg_reg[1]_3\ : in STD_LOGIC;
    \state_reg_reg[1]_4\ : in STD_LOGIC;
    \FSM_onehot_state_reg_reg[7]_0\ : in STD_LOGIC;
    \state_reg_reg[1]_5\ : in STD_LOGIC;
    \state_reg_reg[0]_4\ : in STD_LOGIC;
    \state_reg_reg[2]\ : in STD_LOGIC;
    \state_reg_reg[3]\ : in STD_LOGIC;
    \state_reg_reg[1]_6\ : in STD_LOGIC;
    \state_reg_reg[2]_0\ : in STD_LOGIC;
    enable_reg_reg_reg_0 : in STD_LOGIC;
    \state_reg_reg[0]_5\ : in STD_LOGIC;
    \state_reg_reg[2]_1\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 7 downto 0 );
    \state_reg_reg[2]_2\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \FSM_onehot_state_reg_reg[1]_2\ : in STD_LOGIC_VECTOR ( 1 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_decodeur_traces_1_1_datapath : entity is "datapath";
end design_1_decodeur_traces_1_1_datapath;

architecture STRUCTURE of design_1_decodeur_traces_1_1_datapath is
  signal \^fsm_onehot_state_reg_reg[0]\ : STD_LOGIC;
  signal \^fsm_onehot_state_reg_reg[7]\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal en_0_reg : STD_LOGIC;
  signal en_1_reg : STD_LOGIC;
  signal en_2_reg : STD_LOGIC;
  signal en_3_reg : STD_LOGIC;
  signal en_4_reg : STD_LOGIC;
  signal en_s_instruction : STD_LOGIC;
  signal instruction_address : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal out_en_b_i : STD_LOGIC;
  signal \^out_en_reg\ : STD_LOGIC;
  signal \^output_data_0_reg[0]\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal p_0_in : STD_LOGIC;
  signal p_1_in : STD_LOGIC;
  signal \^pc_reg[0]\ : STD_LOGIC;
  signal \pc_s__0\ : STD_LOGIC_VECTOR ( 28 downto 21 );
  signal \^state_reg_reg[0]_0\ : STD_LOGIC;
  signal \^state_reg_reg[0]_1\ : STD_LOGIC;
  signal u_address_compue_n_0 : STD_LOGIC;
  signal u_address_compue_n_1 : STD_LOGIC;
  signal u_address_compue_n_34 : STD_LOGIC;
  signal u_address_compue_n_35 : STD_LOGIC;
  signal u_decode_bap_n_1 : STD_LOGIC;
  signal u_decode_bap_n_14 : STD_LOGIC;
  signal u_decode_bap_n_15 : STD_LOGIC;
  signal u_decode_bap_n_16 : STD_LOGIC;
  signal u_decode_bap_n_17 : STD_LOGIC;
  signal u_decode_bap_n_18 : STD_LOGIC;
  signal u_decode_bap_n_19 : STD_LOGIC;
  signal u_decode_bap_n_2 : STD_LOGIC;
  signal u_decode_bap_n_20 : STD_LOGIC;
  signal u_decode_bap_n_21 : STD_LOGIC;
  signal u_decode_bap_n_22 : STD_LOGIC;
  signal u_decode_bap_n_23 : STD_LOGIC;
  signal u_decode_bap_n_24 : STD_LOGIC;
  signal u_decode_bap_n_25 : STD_LOGIC;
  signal u_decode_bap_n_26 : STD_LOGIC;
  signal u_decode_bap_n_27 : STD_LOGIC;
  signal u_decode_bap_n_28 : STD_LOGIC;
  signal u_decode_bap_n_29 : STD_LOGIC;
  signal u_decode_bap_n_3 : STD_LOGIC;
  signal u_decode_bap_n_30 : STD_LOGIC;
  signal u_decode_bap_n_31 : STD_LOGIC;
  signal u_decode_bap_n_32 : STD_LOGIC;
  signal u_decode_bap_n_33 : STD_LOGIC;
  signal u_decode_bap_n_34 : STD_LOGIC;
  signal u_decode_bap_n_35 : STD_LOGIC;
  signal u_decode_bap_n_36 : STD_LOGIC;
  signal u_decode_bap_n_37 : STD_LOGIC;
  signal u_decode_bap_n_38 : STD_LOGIC;
  signal u_decode_bap_n_39 : STD_LOGIC;
  signal u_decode_bap_n_40 : STD_LOGIC;
  signal u_decode_bap_n_41 : STD_LOGIC;
  signal u_decode_bap_n_42 : STD_LOGIC;
  signal u_decode_bap_n_43 : STD_LOGIC;
  signal u_decode_bap_n_44 : STD_LOGIC;
  signal u_decode_bap_n_45 : STD_LOGIC;
  signal u_decode_bap_n_46 : STD_LOGIC;
  signal u_decode_bap_n_7 : STD_LOGIC;
  signal u_decode_i_sync_n_10 : STD_LOGIC;
  signal u_decode_i_sync_n_11 : STD_LOGIC;
  signal u_decode_i_sync_n_12 : STD_LOGIC;
  signal u_decode_i_sync_n_13 : STD_LOGIC;
  signal u_decode_i_sync_n_14 : STD_LOGIC;
  signal u_decode_i_sync_n_27 : STD_LOGIC;
  signal u_decode_i_sync_n_28 : STD_LOGIC;
  signal u_decode_i_sync_n_29 : STD_LOGIC;
  signal u_decode_i_sync_n_3 : STD_LOGIC;
  signal u_decode_i_sync_n_4 : STD_LOGIC;
  signal u_decode_i_sync_n_5 : STD_LOGIC;
  signal u_decode_i_sync_n_6 : STD_LOGIC;
  signal u_decode_i_sync_n_7 : STD_LOGIC;
  signal u_decode_i_sync_n_8 : STD_LOGIC;
  signal u_decode_i_sync_n_9 : STD_LOGIC;
  signal u_decode_waypoint_n_0 : STD_LOGIC;
  signal u_decode_waypoint_n_9 : STD_LOGIC;
  signal \^waypoint_address\ : STD_LOGIC_VECTOR ( 22 downto 0 );
  signal waypoint_address_cs_reg : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \^waypoint_address_en\ : STD_LOGIC;
begin
  \FSM_onehot_state_reg_reg[0]\ <= \^fsm_onehot_state_reg_reg[0]\;
  \FSM_onehot_state_reg_reg[7]\(1 downto 0) <= \^fsm_onehot_state_reg_reg[7]\(1 downto 0);
  out_en_reg <= \^out_en_reg\;
  \output_data_0_reg[0]\(1 downto 0) <= \^output_data_0_reg[0]\(1 downto 0);
  \pc_reg[0]\ <= \^pc_reg[0]\;
  \state_reg_reg[0]_0\ <= \^state_reg_reg[0]_0\;
  \state_reg_reg[0]_1\ <= \^state_reg_reg[0]_1\;
  waypoint_address(22 downto 0) <= \^waypoint_address\(22 downto 0);
  waypoint_address_en <= \^waypoint_address_en\;
u_address_compue: entity work.design_1_decodeur_traces_1_1_address_compute
     port map (
      D(0) => u_address_compue_n_0,
      Q(1) => p_0_in,
      Q(0) => p_1_in,
      clk => clk,
      en_0_reg_reg(4) => en_0_reg,
      en_0_reg_reg(3) => en_1_reg,
      en_0_reg_reg(2) => en_2_reg,
      en_0_reg_reg(1) => en_3_reg,
      en_0_reg_reg(0) => en_4_reg,
      enable_reg_reg_reg => enable_reg_reg_reg,
      out_en_b_i => out_en_b_i,
      out_en_reg => u_decode_waypoint_n_9,
      out_en_reg_0 => \^waypoint_address_en\,
      \output_data_0_reg[5]\(0) => u_address_compue_n_1,
      \output_data_3_reg[6]\ => u_address_compue_n_35,
      pc(31 downto 0) => pc(31 downto 0),
      \pc_reg[31]\(31) => u_decode_bap_n_15,
      \pc_reg[31]\(30) => u_decode_bap_n_16,
      \pc_reg[31]\(29) => u_decode_bap_n_17,
      \pc_reg[31]\(28) => u_decode_bap_n_18,
      \pc_reg[31]\(27) => u_decode_bap_n_19,
      \pc_reg[31]\(26) => u_decode_bap_n_20,
      \pc_reg[31]\(25) => u_decode_bap_n_21,
      \pc_reg[31]\(24) => u_decode_bap_n_22,
      \pc_reg[31]\(23) => u_decode_bap_n_23,
      \pc_reg[31]\(22) => u_decode_bap_n_24,
      \pc_reg[31]\(21) => u_decode_bap_n_25,
      \pc_reg[31]\(20) => u_decode_bap_n_26,
      \pc_reg[31]\(19) => u_decode_bap_n_27,
      \pc_reg[31]\(18) => u_decode_bap_n_28,
      \pc_reg[31]\(17) => u_decode_bap_n_29,
      \pc_reg[31]\(16) => u_decode_bap_n_30,
      \pc_reg[31]\(15) => u_decode_bap_n_31,
      \pc_reg[31]\(14) => u_decode_bap_n_32,
      \pc_reg[31]\(13) => u_decode_bap_n_33,
      \pc_reg[31]\(12) => u_decode_bap_n_34,
      \pc_reg[31]\(11) => u_decode_bap_n_35,
      \pc_reg[31]\(10) => u_decode_bap_n_36,
      \pc_reg[31]\(9) => u_decode_bap_n_37,
      \pc_reg[31]\(8) => u_decode_bap_n_38,
      \pc_reg[31]\(7) => u_decode_bap_n_39,
      \pc_reg[31]\(6) => u_decode_bap_n_40,
      \pc_reg[31]\(5) => u_decode_bap_n_41,
      \pc_reg[31]\(4) => u_decode_bap_n_42,
      \pc_reg[31]\(3) => u_decode_bap_n_43,
      \pc_reg[31]\(2) => u_decode_bap_n_44,
      \pc_reg[31]\(1) => u_decode_bap_n_45,
      \pc_reg[31]\(0) => u_decode_bap_n_46,
      \pc_s_reg_reg[21]_0\(0) => waypoint_address_cs_reg(3),
      \pc_s_reg_reg[22]_0\ => u_address_compue_n_34,
      reset => reset,
      waypoint_address(22 downto 0) => \^waypoint_address\(22 downto 0)
    );
u_decode_bap: entity work.design_1_decodeur_traces_1_1_decode_bap
     port map (
      D(1) => \pc_s__0\(28),
      D(0) => \pc_s__0\(21),
      E(0) => en_s_instruction,
      \FSM_onehot_state_reg_reg[0]_0\ => \^fsm_onehot_state_reg_reg[0]\,
      \FSM_onehot_state_reg_reg[1]_0\ => u_decode_bap_n_7,
      \FSM_onehot_state_reg_reg[2]_0\ => u_decode_bap_n_14,
      \FSM_onehot_state_reg_reg[7]_0\ => \FSM_onehot_state_reg_reg[7]_0\,
      Q(7) => p_0_in,
      Q(6) => p_1_in,
      Q(5) => u_decode_i_sync_n_3,
      Q(4) => u_decode_i_sync_n_4,
      Q(3) => u_decode_i_sync_n_5,
      Q(2) => u_decode_i_sync_n_6,
      Q(1) => u_decode_i_sync_n_7,
      Q(0) => u_decode_i_sync_n_8,
      clk => clk,
      \data_reg_s_reg[6]\ => u_decode_i_sync_n_27,
      \data_reg_s_reg[6]_0\(0) => u_decode_i_sync_n_28,
      \data_reg_s_reg[6]_1\(0) => u_decode_i_sync_n_29,
      \data_reg_s_reg[7]\ => u_decode_i_sync_n_13,
      \data_reg_s_reg[7]_0\(2) => u_decode_i_sync_n_9,
      \data_reg_s_reg[7]_0\(1) => u_decode_i_sync_n_10,
      \data_reg_s_reg[7]_0\(0) => u_decode_i_sync_n_11,
      enable_reg_reg_reg(0) => E(0),
      enable_reg_reg_reg_0 => enable_reg_reg_reg,
      \out\(4) => \^fsm_onehot_state_reg_reg[7]\(1),
      \out\(3) => u_decode_bap_n_1,
      \out\(2) => u_decode_bap_n_2,
      \out\(1) => u_decode_bap_n_3,
      \out\(0) => \^fsm_onehot_state_reg_reg[7]\(0),
      out_en_b_i => out_en_b_i,
      out_en_reg => \^waypoint_address_en\,
      out_en_s => out_en_s,
      output_data_0_s_reg_reg_0 => output_data_0_s_reg_reg,
      \output_data_3_reg[7]\(31 downto 0) => instruction_address(31 downto 0),
      \pc_reg[0]_0\ => \^pc_reg[0]\,
      \pc_s_reg_reg[31]_0\(31) => u_decode_bap_n_15,
      \pc_s_reg_reg[31]_0\(30) => u_decode_bap_n_16,
      \pc_s_reg_reg[31]_0\(29) => u_decode_bap_n_17,
      \pc_s_reg_reg[31]_0\(28) => u_decode_bap_n_18,
      \pc_s_reg_reg[31]_0\(27) => u_decode_bap_n_19,
      \pc_s_reg_reg[31]_0\(26) => u_decode_bap_n_20,
      \pc_s_reg_reg[31]_0\(25) => u_decode_bap_n_21,
      \pc_s_reg_reg[31]_0\(24) => u_decode_bap_n_22,
      \pc_s_reg_reg[31]_0\(23) => u_decode_bap_n_23,
      \pc_s_reg_reg[31]_0\(22) => u_decode_bap_n_24,
      \pc_s_reg_reg[31]_0\(21) => u_decode_bap_n_25,
      \pc_s_reg_reg[31]_0\(20) => u_decode_bap_n_26,
      \pc_s_reg_reg[31]_0\(19) => u_decode_bap_n_27,
      \pc_s_reg_reg[31]_0\(18) => u_decode_bap_n_28,
      \pc_s_reg_reg[31]_0\(17) => u_decode_bap_n_29,
      \pc_s_reg_reg[31]_0\(16) => u_decode_bap_n_30,
      \pc_s_reg_reg[31]_0\(15) => u_decode_bap_n_31,
      \pc_s_reg_reg[31]_0\(14) => u_decode_bap_n_32,
      \pc_s_reg_reg[31]_0\(13) => u_decode_bap_n_33,
      \pc_s_reg_reg[31]_0\(12) => u_decode_bap_n_34,
      \pc_s_reg_reg[31]_0\(11) => u_decode_bap_n_35,
      \pc_s_reg_reg[31]_0\(10) => u_decode_bap_n_36,
      \pc_s_reg_reg[31]_0\(9) => u_decode_bap_n_37,
      \pc_s_reg_reg[31]_0\(8) => u_decode_bap_n_38,
      \pc_s_reg_reg[31]_0\(7) => u_decode_bap_n_39,
      \pc_s_reg_reg[31]_0\(6) => u_decode_bap_n_40,
      \pc_s_reg_reg[31]_0\(5) => u_decode_bap_n_41,
      \pc_s_reg_reg[31]_0\(4) => u_decode_bap_n_42,
      \pc_s_reg_reg[31]_0\(3) => u_decode_bap_n_43,
      \pc_s_reg_reg[31]_0\(2) => u_decode_bap_n_44,
      \pc_s_reg_reg[31]_0\(1) => u_decode_bap_n_45,
      \pc_s_reg_reg[31]_0\(0) => u_decode_bap_n_46,
      reset => reset,
      \state_reg_reg[0]\ => \^state_reg_reg[0]_0\,
      \state_reg_reg[1]\ => \state_reg_reg[1]_3\,
      \state_reg_reg[1]_0\ => \state_reg_reg[1]_4\,
      \state_reg_reg[2]\ => \state_reg_reg[2]_1\
    );
u_decode_i_sync: entity work.design_1_decodeur_traces_1_1_decode_i_sync
     port map (
      D(0) => u_decode_i_sync_n_12,
      E(0) => \output_data_7_reg[0]\(0),
      \FSM_onehot_state_reg_reg[0]_0\ => \FSM_onehot_state_reg_reg[0]_1\,
      \FSM_onehot_state_reg_reg[0]_1\ => \FSM_onehot_state_reg_reg[0]_2\,
      \FSM_onehot_state_reg_reg[13]\ => \^fsm_onehot_state_reg_reg[0]\,
      \FSM_onehot_state_reg_reg[1]_0\ => \FSM_onehot_state_reg_reg[1]\,
      \FSM_onehot_state_reg_reg[1]_1\ => \FSM_onehot_state_reg_reg[1]_0\,
      \FSM_onehot_state_reg_reg[1]_2\ => \^pc_reg[0]\,
      \FSM_onehot_state_reg_reg[2]_0\ => u_decode_bap_n_14,
      \FSM_onehot_state_reg_reg[3]_0\(1) => u_decode_waypoint_n_0,
      \FSM_onehot_state_reg_reg[3]_0\(0) => \^output_data_0_reg[0]\(0),
      \FSM_onehot_state_reg_reg[6]_0\ => u_decode_i_sync_n_27,
      \FSM_onehot_state_reg_reg[6]_1\(3) => u_decode_bap_n_1,
      \FSM_onehot_state_reg_reg[6]_1\(2) => u_decode_bap_n_2,
      \FSM_onehot_state_reg_reg[6]_1\(1) => u_decode_bap_n_3,
      \FSM_onehot_state_reg_reg[6]_1\(0) => \^fsm_onehot_state_reg_reg[7]\(0),
      \FSM_onehot_state_reg_reg[7]_0\ => \^state_reg_reg[0]_0\,
      \FSM_onehot_state_reg_reg[9]\(2) => u_decode_i_sync_n_9,
      \FSM_onehot_state_reg_reg[9]\(1) => u_decode_i_sync_n_10,
      \FSM_onehot_state_reg_reg[9]\(0) => u_decode_i_sync_n_11,
      \FSM_onehot_state_reg_reg[9]_0\ => \^out_en_reg\,
      Q(7) => p_0_in,
      Q(6) => p_1_in,
      Q(5) => u_decode_i_sync_n_3,
      Q(4) => u_decode_i_sync_n_4,
      Q(3) => u_decode_i_sync_n_5,
      Q(2) => u_decode_i_sync_n_6,
      Q(1) => u_decode_i_sync_n_7,
      Q(0) => u_decode_i_sync_n_8,
      clk => clk,
      \context_id_reg[31]\(31 downto 0) => \context_id_reg[31]\(31 downto 0),
      \data_in_reg_reg[7]\(7 downto 0) => Q(7 downto 0),
      enable_reg_reg_reg => enable_reg_reg_reg,
      enable_reg_reg_reg_0 => u_decode_bap_n_7,
      enable_reg_reg_reg_1 => enable_reg_reg_reg_0,
      \instruction_address_reg_reg[31]\(31 downto 0) => instruction_address(31 downto 0),
      \out\(0) => \out\(0),
      \output_data_1_reg[6]_0\ => u_decode_i_sync_n_13,
      \output_data_2_reg[6]_0\(0) => u_decode_i_sync_n_28,
      \output_data_3_reg[6]_0\(0) => u_decode_i_sync_n_14,
      \output_data_3_reg[6]_1\(0) => u_decode_i_sync_n_29,
      \output_data_3_reg[7]_0\(0) => en_s_instruction,
      \pc_reg[28]\(0) => u_decode_bap_n_18,
      \pc_s_reg_reg[28]\(1) => \pc_s__0\(28),
      \pc_s_reg_reg[28]\(0) => \pc_s__0\(21),
      reset => reset,
      \state_reg_reg[0]\ => \state_reg_reg[0]\,
      \state_reg_reg[0]_0\ => \^state_reg_reg[0]_1\,
      \state_reg_reg[0]_1\ => \state_reg_reg[0]_2\,
      \state_reg_reg[0]_2\ => \state_reg_reg[0]_3\,
      \state_reg_reg[0]_3\ => \state_reg_reg[0]_4\,
      \state_reg_reg[0]_4\ => \state_reg_reg[0]_5\,
      \state_reg_reg[1]\ => \state_reg_reg[1]\,
      \state_reg_reg[1]_0\ => \state_reg_reg[1]_0\,
      \state_reg_reg[1]_1\ => \state_reg_reg[1]_1\,
      \state_reg_reg[1]_2\ => \state_reg_reg[1]_2\,
      \state_reg_reg[1]_3\ => \state_reg_reg[1]_5\,
      \state_reg_reg[1]_4\ => \state_reg_reg[1]_6\,
      \state_reg_reg[1]_5\ => \state_reg_reg[1]_4\,
      \state_reg_reg[2]\(0) => D(0),
      \state_reg_reg[2]_0\ => \state_reg_reg[2]\,
      \state_reg_reg[2]_1\ => \state_reg_reg[2]_0\,
      \state_reg_reg[2]_2\(0) => \state_reg_reg[2]_2\(0),
      \state_reg_reg[3]\ => \state_reg_reg[3]\,
      \waypoint_address_cs_reg_reg[1]\ => u_address_compue_n_34,
      \waypoint_address_reg_reg[28]\ => u_address_compue_n_35
    );
u_decode_waypoint: entity work.design_1_decodeur_traces_1_1_decode_waypoint
     port map (
      D(2) => u_decode_i_sync_n_12,
      D(1 downto 0) => \FSM_onehot_state_reg_reg[1]_2\(1 downto 0),
      \FSM_onehot_state_reg_reg[0]_0\ => \FSM_onehot_state_reg_reg[0]_0\,
      \FSM_onehot_state_reg_reg[1]_0\ => \FSM_onehot_state_reg_reg[1]_1\,
      Q(7) => p_0_in,
      Q(6) => p_1_in,
      Q(5) => u_decode_i_sync_n_3,
      Q(4) => u_decode_i_sync_n_4,
      Q(3) => u_decode_i_sync_n_5,
      Q(2) => u_decode_i_sync_n_6,
      Q(1) => u_decode_i_sync_n_7,
      Q(0) => u_decode_i_sync_n_8,
      clk => clk,
      \data_in_reg_reg[7]\(7 downto 0) => Q(7 downto 0),
      \data_reg_s_reg[2]\ => \^state_reg_reg[0]_1\,
      enable_reg_reg_reg => enable_reg_reg_reg,
      enable_reg_reg_reg_0(0) => u_address_compue_n_1,
      enable_reg_reg_reg_1(0) => u_address_compue_n_0,
      enable_reg_reg_reg_2(0) => u_decode_i_sync_n_14,
      \out\(2) => u_decode_waypoint_n_0,
      \out\(1 downto 0) => \^output_data_0_reg[0]\(1 downto 0),
      out_en_b_i => out_en_b_i,
      out_en_reg_0 => \^out_en_reg\,
      \pc_s_reg_reg[28]\ => u_decode_waypoint_n_9,
      reset => reset,
      waypoint_address(22 downto 0) => \^waypoint_address\(22 downto 0),
      \waypoint_address_cs_reg_reg[3]\(0) => waypoint_address_cs_reg(3),
      \waypoint_address_cs_reg_reg[4]\(4) => en_0_reg,
      \waypoint_address_cs_reg_reg[4]\(3) => en_1_reg,
      \waypoint_address_cs_reg_reg[4]\(2) => en_2_reg,
      \waypoint_address_cs_reg_reg[4]\(1) => en_3_reg,
      \waypoint_address_cs_reg_reg[4]\(0) => en_4_reg,
      waypoint_address_en => \^waypoint_address_en\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_decodeur_traces_1_1_decodeur_traces is
  port (
    waypoint_address_en : out STD_LOGIC;
    waypoint_address : out STD_LOGIC_VECTOR ( 22 downto 0 );
    pc : out STD_LOGIC_VECTOR ( 31 downto 0 );
    w_en : out STD_LOGIC;
    w_ctxt_en : out STD_LOGIC;
    context_id : out STD_LOGIC_VECTOR ( 31 downto 0 );
    reset : in STD_LOGIC;
    clk : in STD_LOGIC;
    trace_data : in STD_LOGIC_VECTOR ( 7 downto 0 );
    enable : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_decodeur_traces_1_1_decodeur_traces : entity is "decodeur_traces";
end design_1_decodeur_traces_1_1_decodeur_traces;

architecture STRUCTURE of design_1_decodeur_traces_1_1_decodeur_traces is
  signal chemin_n_0 : STD_LOGIC;
  signal chemin_n_1 : STD_LOGIC;
  signal chemin_n_10 : STD_LOGIC;
  signal chemin_n_11 : STD_LOGIC;
  signal chemin_n_13 : STD_LOGIC;
  signal chemin_n_14 : STD_LOGIC;
  signal chemin_n_15 : STD_LOGIC;
  signal chemin_n_16 : STD_LOGIC;
  signal chemin_n_17 : STD_LOGIC;
  signal chemin_n_2 : STD_LOGIC;
  signal chemin_n_3 : STD_LOGIC;
  signal chemin_n_4 : STD_LOGIC;
  signal chemin_n_51 : STD_LOGIC;
  signal chemin_n_52 : STD_LOGIC;
  signal chemin_n_53 : STD_LOGIC;
  signal chemin_n_54 : STD_LOGIC;
  signal chemin_n_55 : STD_LOGIC;
  signal chemin_n_56 : STD_LOGIC;
  signal chemin_n_7 : STD_LOGIC;
  signal chemin_n_8 : STD_LOGIC;
  signal chemin_n_9 : STD_LOGIC;
  signal count : STD_LOGIC;
  signal \count[0]_i_1_n_0\ : STD_LOGIC;
  signal \count[0]_i_2_n_0\ : STD_LOGIC;
  signal data_in : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal enable_reg : STD_LOGIC;
  signal enable_reg_i_1_n_0 : STD_LOGIC;
  signal enable_reg_reg_reg_n_0 : STD_LOGIC;
  signal fsm_n_1 : STD_LOGIC;
  signal fsm_n_10 : STD_LOGIC;
  signal fsm_n_11 : STD_LOGIC;
  signal fsm_n_12 : STD_LOGIC;
  signal fsm_n_13 : STD_LOGIC;
  signal fsm_n_14 : STD_LOGIC;
  signal fsm_n_15 : STD_LOGIC;
  signal fsm_n_16 : STD_LOGIC;
  signal fsm_n_17 : STD_LOGIC;
  signal fsm_n_18 : STD_LOGIC;
  signal fsm_n_2 : STD_LOGIC;
  signal fsm_n_3 : STD_LOGIC;
  signal fsm_n_4 : STD_LOGIC;
  signal fsm_n_5 : STD_LOGIC;
  signal fsm_n_7 : STD_LOGIC;
  signal fsm_n_8 : STD_LOGIC;
  signal fsm_n_9 : STD_LOGIC;
  signal out_en_s : STD_LOGIC;
  signal output_data_4 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal output_data_5 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal output_data_6 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal output_data_7 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal state_next : STD_LOGIC_VECTOR ( 2 to 2 );
  signal state_reg : STD_LOGIC_VECTOR ( 2 to 2 );
  signal \u_decode_bap/output_data_0_s\ : STD_LOGIC;
  signal w_ctxt_en_s : STD_LOGIC;
begin
chemin: entity work.design_1_decodeur_traces_1_1_datapath
     port map (
      D(0) => state_next(2),
      E(0) => \u_decode_bap/output_data_0_s\,
      \FSM_onehot_state_reg_reg[0]\ => chemin_n_7,
      \FSM_onehot_state_reg_reg[0]_0\ => chemin_n_51,
      \FSM_onehot_state_reg_reg[0]_1\ => chemin_n_54,
      \FSM_onehot_state_reg_reg[0]_2\ => fsm_n_12,
      \FSM_onehot_state_reg_reg[1]\ => chemin_n_13,
      \FSM_onehot_state_reg_reg[1]_0\ => chemin_n_14,
      \FSM_onehot_state_reg_reg[1]_1\ => chemin_n_52,
      \FSM_onehot_state_reg_reg[1]_2\(1) => fsm_n_4,
      \FSM_onehot_state_reg_reg[1]_2\(0) => fsm_n_5,
      \FSM_onehot_state_reg_reg[7]\(1) => chemin_n_1,
      \FSM_onehot_state_reg_reg[7]\(0) => chemin_n_2,
      \FSM_onehot_state_reg_reg[7]_0\ => fsm_n_14,
      Q(7 downto 0) => data_in(7 downto 0),
      clk => clk,
      \context_id_reg[31]\(31 downto 24) => output_data_7(7 downto 0),
      \context_id_reg[31]\(23 downto 16) => output_data_6(7 downto 0),
      \context_id_reg[31]\(15 downto 8) => output_data_5(7 downto 0),
      \context_id_reg[31]\(7 downto 0) => output_data_4(7 downto 0),
      enable_reg_reg_reg => enable_reg_reg_reg_n_0,
      enable_reg_reg_reg_0 => fsm_n_8,
      \out\(0) => chemin_n_0,
      out_en_reg => chemin_n_16,
      out_en_s => out_en_s,
      \output_data_0_reg[0]\(1) => chemin_n_3,
      \output_data_0_reg[0]\(0) => chemin_n_4,
      output_data_0_s_reg_reg => chemin_n_9,
      \output_data_7_reg[0]\(0) => w_ctxt_en_s,
      pc(31 downto 0) => pc(31 downto 0),
      \pc_reg[0]\ => chemin_n_8,
      reset => reset,
      \state_reg_reg[0]\ => chemin_n_10,
      \state_reg_reg[0]_0\ => chemin_n_11,
      \state_reg_reg[0]_1\ => chemin_n_15,
      \state_reg_reg[0]_2\ => chemin_n_17,
      \state_reg_reg[0]_3\ => chemin_n_55,
      \state_reg_reg[0]_4\ => fsm_n_2,
      \state_reg_reg[0]_5\ => fsm_n_17,
      \state_reg_reg[1]\ => chemin_n_53,
      \state_reg_reg[1]_0\ => chemin_n_56,
      \state_reg_reg[1]_1\ => fsm_n_11,
      \state_reg_reg[1]_2\ => fsm_n_18,
      \state_reg_reg[1]_3\ => fsm_n_7,
      \state_reg_reg[1]_4\ => fsm_n_15,
      \state_reg_reg[1]_5\ => fsm_n_3,
      \state_reg_reg[1]_6\ => fsm_n_13,
      \state_reg_reg[2]\ => fsm_n_1,
      \state_reg_reg[2]_0\ => fsm_n_10,
      \state_reg_reg[2]_1\ => fsm_n_16,
      \state_reg_reg[2]_2\(0) => state_reg(2),
      \state_reg_reg[3]\ => fsm_n_9,
      waypoint_address(22 downto 0) => waypoint_address(22 downto 0),
      waypoint_address_en => waypoint_address_en
    );
\context_id_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => output_data_4(0),
      Q => context_id(0),
      R => reset
    );
\context_id_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => output_data_5(2),
      Q => context_id(10),
      R => reset
    );
\context_id_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => output_data_5(3),
      Q => context_id(11),
      R => reset
    );
\context_id_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => output_data_5(4),
      Q => context_id(12),
      R => reset
    );
\context_id_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => output_data_5(5),
      Q => context_id(13),
      R => reset
    );
\context_id_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => output_data_5(6),
      Q => context_id(14),
      R => reset
    );
\context_id_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => output_data_5(7),
      Q => context_id(15),
      R => reset
    );
\context_id_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => output_data_6(0),
      Q => context_id(16),
      R => reset
    );
\context_id_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => output_data_6(1),
      Q => context_id(17),
      R => reset
    );
\context_id_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => output_data_6(2),
      Q => context_id(18),
      R => reset
    );
\context_id_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => output_data_6(3),
      Q => context_id(19),
      R => reset
    );
\context_id_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => output_data_4(1),
      Q => context_id(1),
      R => reset
    );
\context_id_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => output_data_6(4),
      Q => context_id(20),
      R => reset
    );
\context_id_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => output_data_6(5),
      Q => context_id(21),
      R => reset
    );
\context_id_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => output_data_6(6),
      Q => context_id(22),
      R => reset
    );
\context_id_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => output_data_6(7),
      Q => context_id(23),
      R => reset
    );
\context_id_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => output_data_7(0),
      Q => context_id(24),
      R => reset
    );
\context_id_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => output_data_7(1),
      Q => context_id(25),
      R => reset
    );
\context_id_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => output_data_7(2),
      Q => context_id(26),
      R => reset
    );
\context_id_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => output_data_7(3),
      Q => context_id(27),
      R => reset
    );
\context_id_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => output_data_7(4),
      Q => context_id(28),
      R => reset
    );
\context_id_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => output_data_7(5),
      Q => context_id(29),
      R => reset
    );
\context_id_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => output_data_4(2),
      Q => context_id(2),
      R => reset
    );
\context_id_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => output_data_7(6),
      Q => context_id(30),
      R => reset
    );
\context_id_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => output_data_7(7),
      Q => context_id(31),
      R => reset
    );
\context_id_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => output_data_4(3),
      Q => context_id(3),
      R => reset
    );
\context_id_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => output_data_4(4),
      Q => context_id(4),
      R => reset
    );
\context_id_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => output_data_4(5),
      Q => context_id(5),
      R => reset
    );
\context_id_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => output_data_4(6),
      Q => context_id(6),
      R => reset
    );
\context_id_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => output_data_4(7),
      Q => context_id(7),
      R => reset
    );
\context_id_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => output_data_5(0),
      Q => context_id(8),
      R => reset
    );
\context_id_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => output_data_5(1),
      Q => context_id(9),
      R => reset
    );
\count[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF00000001"
    )
        port map (
      I0 => trace_data(5),
      I1 => trace_data(4),
      I2 => trace_data(6),
      I3 => trace_data(0),
      I4 => \count[0]_i_2_n_0\,
      I5 => count,
      O => \count[0]_i_1_n_0\
    );
\count[0]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFEF"
    )
        port map (
      I0 => trace_data(2),
      I1 => trace_data(3),
      I2 => trace_data(7),
      I3 => trace_data(1),
      O => \count[0]_i_2_n_0\
    );
\count_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \count[0]_i_1_n_0\,
      Q => count,
      R => '0'
    );
\data_in_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => trace_data(0),
      Q => data_in(0),
      R => reset
    );
\data_in_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => trace_data(1),
      Q => data_in(1),
      R => reset
    );
\data_in_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => trace_data(2),
      Q => data_in(2),
      R => reset
    );
\data_in_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => trace_data(3),
      Q => data_in(3),
      R => reset
    );
\data_in_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => trace_data(4),
      Q => data_in(4),
      R => reset
    );
\data_in_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => trace_data(5),
      Q => data_in(5),
      R => reset
    );
\data_in_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => trace_data(6),
      Q => data_in(6),
      R => reset
    );
\data_in_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => trace_data(7),
      Q => data_in(7),
      R => reset
    );
enable_reg_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => enable,
      I1 => count,
      I2 => enable_reg,
      O => enable_reg_i_1_n_0
    );
enable_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => enable_reg_i_1_n_0,
      Q => enable_reg,
      R => reset
    );
enable_reg_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => enable_reg,
      Q => enable_reg_reg_reg_n_0,
      R => reset
    );
fsm: entity work.design_1_decodeur_traces_1_1_pft_decoder_v2
     port map (
      D(0) => state_next(2),
      E(0) => \u_decode_bap/output_data_0_s\,
      \FSM_onehot_state_reg_reg[0]\ => fsm_n_11,
      \FSM_onehot_state_reg_reg[0]_0\ => fsm_n_12,
      \FSM_onehot_state_reg_reg[0]_1\ => fsm_n_14,
      \FSM_onehot_state_reg_reg[0]_2\ => fsm_n_18,
      \FSM_onehot_state_reg_reg[0]_3\ => chemin_n_51,
      \FSM_onehot_state_reg_reg[0]_4\ => chemin_n_55,
      \FSM_onehot_state_reg_reg[13]\ => chemin_n_7,
      \FSM_onehot_state_reg_reg[1]\(1) => fsm_n_4,
      \FSM_onehot_state_reg_reg[1]\(0) => fsm_n_5,
      \FSM_onehot_state_reg_reg[1]_0\ => fsm_n_15,
      \FSM_onehot_state_reg_reg[1]_1\(1) => chemin_n_3,
      \FSM_onehot_state_reg_reg[1]_1\(0) => chemin_n_4,
      \FSM_onehot_state_reg_reg[1]_2\ => chemin_n_8,
      \FSM_onehot_state_reg_reg[7]\(1) => chemin_n_1,
      \FSM_onehot_state_reg_reg[7]\(0) => chemin_n_2,
      \FSM_onehot_state_reg_reg[7]_0\ => chemin_n_11,
      \FSM_onehot_state_reg_reg[9]\ => chemin_n_16,
      Q(0) => state_reg(2),
      clk => clk,
      \data_in_reg_reg[2]\ => chemin_n_52,
      \data_reg_s_reg[0]\ => chemin_n_9,
      \data_reg_s_reg[0]_0\ => chemin_n_17,
      \data_reg_s_reg[0]_1\ => chemin_n_53,
      \data_reg_s_reg[0]_2\ => chemin_n_56,
      \data_reg_s_reg[0]_3\ => chemin_n_54,
      \data_reg_s_reg[2]\ => chemin_n_15,
      \data_reg_s_reg[3]\ => chemin_n_13,
      enable_reg_reg_reg => enable_reg_reg_reg_n_0,
      enable_reg_reg_reg_0 => chemin_n_14,
      enable_reg_reg_reg_1 => chemin_n_10,
      \out\(0) => chemin_n_0,
      output_data_0_s_reg_reg => fsm_n_1,
      output_data_0_s_reg_reg_0 => fsm_n_2,
      output_data_0_s_reg_reg_1 => fsm_n_3,
      reset => reset,
      \state_reg_reg[0]_0\ => fsm_n_7,
      \state_reg_reg[0]_1\ => fsm_n_8,
      \state_reg_reg[0]_2\ => fsm_n_9,
      \state_reg_reg[0]_3\ => fsm_n_13,
      \state_reg_reg[0]_4\ => fsm_n_16,
      \state_reg_reg[1]_0\ => fsm_n_10,
      \state_reg_reg[2]_0\ => fsm_n_17
    );
w_ctxt_en_reg: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => w_ctxt_en_s,
      Q => w_ctxt_en,
      R => reset
    );
w_en_reg: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => out_en_s,
      Q => w_en,
      R => reset
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_decodeur_traces_1_1 is
  port (
    clk : in STD_LOGIC;
    reset : in STD_LOGIC;
    enable : in STD_LOGIC;
    trace_data : in STD_LOGIC_VECTOR ( 7 downto 0 );
    pc : out STD_LOGIC_VECTOR ( 31 downto 0 );
    w_en : out STD_LOGIC;
    w_ctxt_en : out STD_LOGIC;
    fifo_overflow : out STD_LOGIC;
    waypoint_address : out STD_LOGIC_VECTOR ( 31 downto 0 );
    context_id : out STD_LOGIC_VECTOR ( 31 downto 0 );
    waypoint_address_en : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_1_decodeur_traces_1_1 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_1_decodeur_traces_1_1 : entity is "design_1_decodeur_traces_1_1,decodeur_traces,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_1_decodeur_traces_1_1 : entity is "yes";
  attribute ip_definition_source : string;
  attribute ip_definition_source of design_1_decodeur_traces_1_1 : entity is "package_project";
  attribute x_core_info : string;
  attribute x_core_info of design_1_decodeur_traces_1_1 : entity is "decodeur_traces,Vivado 2018.2";
end design_1_decodeur_traces_1_1;

architecture STRUCTURE of design_1_decodeur_traces_1_1 is
  signal \<const0>\ : STD_LOGIC;
  signal \^waypoint_address\ : STD_LOGIC_VECTOR ( 31 downto 2 );
  attribute x_interface_info : string;
  attribute x_interface_info of clk : signal is "xilinx.com:signal:clock:1.0 clk CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of clk : signal is "XIL_INTERFACENAME clk, ASSOCIATED_RESET reset, FREQ_HZ 190476196, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK1";
  attribute x_interface_info of reset : signal is "xilinx.com:signal:reset:1.0 reset RST";
  attribute x_interface_parameter of reset : signal is "XIL_INTERFACENAME reset, POLARITY ACTIVE_LOW";
begin
  fifo_overflow <= \<const0>\;
  waypoint_address(31 downto 22) <= \^waypoint_address\(31 downto 22);
  waypoint_address(21) <= \<const0>\;
  waypoint_address(20) <= \<const0>\;
  waypoint_address(19) <= \<const0>\;
  waypoint_address(18) <= \<const0>\;
  waypoint_address(17) <= \<const0>\;
  waypoint_address(16) <= \<const0>\;
  waypoint_address(15) <= \<const0>\;
  waypoint_address(14 downto 2) <= \^waypoint_address\(14 downto 2);
  waypoint_address(1) <= \<const0>\;
  waypoint_address(0) <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
U0: entity work.design_1_decodeur_traces_1_1_decodeur_traces
     port map (
      clk => clk,
      context_id(31 downto 0) => context_id(31 downto 0),
      enable => enable,
      pc(31 downto 0) => pc(31 downto 0),
      reset => reset,
      trace_data(7 downto 0) => trace_data(7 downto 0),
      w_ctxt_en => w_ctxt_en,
      w_en => w_en,
      waypoint_address(22 downto 13) => \^waypoint_address\(31 downto 22),
      waypoint_address(12 downto 0) => \^waypoint_address\(14 downto 2),
      waypoint_address_en => waypoint_address_en
    );
end STRUCTURE;
