----------
--
-- AUTHOR               : M.Abdul WAHAB
-- DATE                 : 2017
-- Description          : Pipeline registers for stage IM/IWB
----------
library IEEE; 
use ieee.std_logic_1164.All;
use work.mips_pack.all;

entity pipeline_IM_IWB is
	port(
		clk, reset 	 	: in std_logic ;
		stall		 	: in std_logic ;
		hazard 			: in std_logic ;
		c_alu_i      	: in std_logic_vector(31 downto 0);
		c_shift_i    	: in std_logic_vector(31 downto 0);
		c_mult_i     	: in std_logic_vector(31 downto 0);
		c_source		: in c_source_type;
		pc_source       : in pc_source_type;
		c_pc            : in std_logic_vector(31 downto 0);
		imm_in          : in std_logic_vector(15 downto 0);        
		mem_data		: in std_logic_vector(31 downto 0);
		rt_index		: in std_logic_vector(5 downto 0);
		rd_index		: in std_logic_vector(5 downto 0);
		mem_source_out_i: in mem_source_type;

        ldr_instr       : in  std_logic; 
        ldr_instr_out   : out std_logic;

		c_alu      	 	: out std_logic_vector(31 downto 0);
		c_shift		 	: out std_logic_vector(31 downto 0);
		c_mult       	: out std_logic_vector(31 downto 0);
		c_source_out	: out c_source_type;
		pc_source_out   : out pc_source_type;
		c_pc_out        : out std_logic_vector(31 downto 0);
		imm_out         : out std_logic_vector(15 downto 0);
		rt_index_out	: out std_logic_vector(5 downto 0);
		rd_index_out	: out std_logic_vector(5 downto 0);
		mem_source_out  : out mem_source_type;
		mem_data_out_p  : out std_logic_vector(31 downto 0);
		mem_data_out  : out std_logic_vector(31 downto 0) );
end entity;

architecture behavioral of pipeline_IM_IWB is 

signal c_alu_reg 			: std_logic_vector(31 downto 0);
signal c_shift_reg 			: std_logic_vector(31 downto 0);
signal c_mult_reg 			: std_logic_vector(31 downto 0);
signal c_source_out_reg 	: c_source_type;
signal pc_source_out_reg 	: pc_source_type;
signal mem_data_out_reg 	: std_logic_vector(31 downto 0);
signal c_pc_out_reg 		: std_logic_vector(31 downto 0);
signal imm_out_reg 			: std_logic_vector(15 downto 0);
signal rd_index_out_reg 	: std_logic_vector(5 downto 0);
signal mem_source_out_reg 	: mem_source_type;
signal rt_index_out_reg 	: std_logic_vector(5 downto 0);
signal stall_r : std_logic;
signal ldr_instr_reg        : std_logic;

begin 

process(clk)
begin
        if rising_edge(clk) then 
                if (reset = '1' or stall = '1') then
                	c_alu_reg 				<= (others => '0');
					c_shift_reg 			<= (others => '0');
					c_mult_reg 				<= (others => '0');
					c_source_out_reg 		<= (others => '0');
					pc_source_out_reg 		<= (others => '0');
					mem_data_out_reg 		<= (others => '0');
					c_pc_out_reg 			<= (others => '0');
					imm_out_reg 			<= (others => '0');
					rt_index_out_reg 		<= (others => '0');
					rd_index_out_reg 		<= (others => '0');					
					mem_source_out_reg 		<= (others => '0');
					ldr_instr_reg       	<= '0';
                elsif stall = '0' then
                    c_alu_reg 			<= c_alu_i;
					c_shift_reg 		<= c_shift_i;
					c_mult_reg 			<= c_mult_i;
					c_source_out_reg 	<= c_source;
					pc_source_out_reg 	<= pc_source;
					mem_data_out_reg 	<= mem_data;
					c_pc_out_reg 		<= c_pc;
					imm_out_reg 		<= imm_in;
					rt_index_out_reg 	<= rt_index;
					rd_index_out_reg 	<= rd_index;
					mem_source_out_reg 	<= mem_source_out_i;
                    ldr_instr_reg       <= ldr_instr;
                end if;
        end if;
end process;

-- pipelined
c_alu 			<=    c_alu_reg;
c_shift 		<=    c_shift_reg;
c_mult 			<=    c_mult_reg;
c_source_out 	<=    c_source_out_reg;
pc_source_out 	<=    pc_source_out_reg;

-- not pipelined because available one cycle after initiating memory access 
mem_data_out    <=    mem_data;
-- pipelined
mem_data_out_p  <=    mem_data_out_reg; 
c_pc_out 		<=    c_pc_out_reg;
imm_out			<=    imm_out_reg;
rt_index_out	<=    rt_index_out_reg;
rd_index_out	<=    rd_index_out_reg;
mem_source_out  <=    mem_source_out_reg;
ldr_instr_out   <= ldr_instr_reg;

end architecture;