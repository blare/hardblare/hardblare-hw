# 
# Synthesis run script generated by Vivado
# 

set_param gui.test TreeTableDev
debug::add_scope template.lib 1
set_msg_config -id {HDL 9-1061} -limit 100000
set_msg_config -id {HDL 9-1654} -limit 100000

create_project -in_memory -part xc7z020clg484-1
set_param project.compositeFile.enableAutoGeneration 0
set_param synth.vivado.isSynthRun true
set_property webtalk.parent_dir /lab/home/labsticc/biswas/tmc/AXI_master2/AXI_IP2/AXI_IP2.cache/wt [current_project]
set_property parent.project_path /lab/home/labsticc/biswas/tmc/AXI_master2/AXI_IP2/AXI_IP2.xpr [current_project]
set_property default_lib xil_defaultlib [current_project]
set_property target_language VHDL [current_project]
set_property board_part em.avnet.com:zed:part0:1.2 [current_project]
read_vhdl -library xil_defaultlib {
  /lab/home/labsticc/biswas/tmc/AXI_master2/myip_v2_0_M00_AXI.vhd
  /lab/home/labsticc/biswas/tmc/AXI_master2/myip_v2_0.vhd
}
catch { write_hwdef -file myip_v2_0.hwdef }
synth_design -top myip_v2_0 -part xc7z020clg484-1
write_checkpoint -noxdef myip_v2_0.dcp
catch { report_utilization -file myip_v2_0_utilization_synth.rpt -pb myip_v2_0_utilization_synth.pb }
