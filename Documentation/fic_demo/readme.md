# demo-fic

Démo ayant tourné pendant le FIC avec quelques slides en support.

Il faut installer asciinema :

```bash
sudo pip3 install asciinema
asciinema play demo-fic
```

La version en ligne de la démo est ici : https://asciinema.org/a/yUrSMFEL22YZjl5whN9HFQdTL

## Déroulement de la démo

- Un cas OK de la CWE 415
- Un cas KO de la CWE 415
- Un cas OK de la CWE 125
- Un cas KO de la CWE 125

## CWE-415

https://cwe.mitre.org/data/definitions/415.html

```c
#include <stdio.h>
#include <unistd.h>
#define BUFSIZE 512
int main(int argc, char **argv) 
{
  char *buf;
  printf("Malloc\n");
  buf = (char *) malloc(BUFSIZE);
  int val = atoi(argv[1]);
  printf("Arg: %d\n", val);   
  free(buf);
  printf("Free buf\n");
  if(val == 2)
    {
      free(buf);
      printf("Free buf\n");
    }
}
```

## CWE-125

https://cwe.mitre.org/data/definitions/125.html

```c
#include <stdio.h>
int getValueFromArray(int *array, int len, int index)
{
  int value;
  if (index < len){
      value = array[index];
    }
  else{
      value = -1;
    }
  return value;
}
int main(int argc, const char* argv[])
{
  int array[42] = { 42 };
  int x = atoi(argv[1]);
  int result = getValueFromArray(&array, 42, x);
  printf("Value is: %x\n", result);
}
```

