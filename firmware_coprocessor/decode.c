#include "types.h"
#include "no_os.h"
#include "decode.h"
#include "memory_tag_trf.h"
#include "address.h"
#include "debug.h"
#include "irq.h"



uint32_t first_byte_policy(uint32_t a, uint32_t b) {
  return ( (a & 0xf0000000) | (b & 0xf0000000) );
}


uint32_t present_in_first_byte_policy(uint32_t a, uint32_t b) {
  return ( (a & 0xf0000000) & (b & 0xf0000000) );
}

uint32_t others_bytes_policy(uint32_t a) {
  return (a & 0x0fffffff);
}


uint32_t or(uint32_t a, uint32_t b)
{
  IF_SIMULATION(debug_print_arm_opcode(a, "OR", b););
  return first_byte_policy(a,b) | (others_bytes_policy(a) | others_bytes_policy(b));
}


uint32_t nor(uint32_t a, uint32_t b)
{
  IF_SIMULATION(debug_print_arm_opcode(a, "NOR", b));
  return first_byte_policy(a,b) | ~(others_bytes_policy(a) | others_bytes_policy(b));
}


uint32_t and(uint32_t a, uint32_t b)
{
  IF_SIMULATION(debug_print_arm_opcode(a, "AND", b));
  return first_byte_policy(a,b) | (others_bytes_policy(a) & others_bytes_policy(b));
}


uint32_t nand(uint32_t a, uint32_t b)
{
  IF_SIMULATION(debug_print_arm_opcode(a, "NAND", b));
  return first_byte_policy(a,b) | ~(others_bytes_policy(a) & others_bytes_policy(b));
}


uint32_t xor(uint32_t a, uint32_t b)
{
  IF_SIMULATION(debug_print_arm_opcode(a, "XOR", b));
  return first_byte_policy(a,b) | (others_bytes_policy(a) ^ others_bytes_policy(b));
}


uint32_t xnor(uint32_t a, uint32_t b)
{
  IF_SIMULATION(debug_print_arm_opcode(a, "XNOR", b));
  return first_byte_policy(a,b) | ~(others_bytes_policy(a) ^ others_bytes_policy(b));
}


uint32_t sup(uint32_t a, uint32_t b)
{
  IF_SIMULATION(debug_print_arm_opcode(a, "SUP", b));
  return (others_bytes_policy(a) > others_bytes_policy(b)) ? (first_byte_policy(a,b) | others_bytes_policy(a)) : (first_byte_policy(a,b) | others_bytes_policy(b));
}

uint32_t inf(uint32_t a, uint32_t b)
{
  IF_SIMULATION(debug_print_arm_opcode(a, "INF", b));
  return (a < b)? (first_byte_policy(a,b) | others_bytes_policy(a)) : (first_byte_policy(a,b) | others_bytes_policy(b));
}

uint32_t sum(uint32_t a, uint32_t b)
{
  IF_SIMULATION(debug_print_arm_opcode(a, "SUM", b));
  return first_byte_policy(a,b) | (others_bytes_policy(a) + others_bytes_policy(b));
}

uint32_t sub(uint32_t a, uint32_t b)
{
  IF_SIMULATION(debug_print_arm_opcode(a, "SUB", b));
  return first_byte_policy(a,b) | (others_bytes_policy(a) - others_bytes_policy(b));
}


uint32_t (*ARM_OPCODE_FUNC_OPERATOR[SECURITY_POLICY_ARM_OPCODE_FUNC_OPERATOR_NUMBER]) (uint32_t, uint32_t) = {
  or,    // OR
  nor,   // NOR
  and,   // AND
  nand,  // NAND
  xor,   // XOR
  xnor,  // XNOR
  sup,   // SUP
  inf,   // INF
  sum,   // SUM
  sub    // SUB
};


void monitor_to_kernel_send_value(uint32_t tag) {

  uint32_t monitor_to_kernel_full;

  // DEBUG
  IF_SIMULATION( debug_print_waiting_monitor_to_kernel(); );
  IF_HARDWARE_DEBUG( MemoryWrite(getDebugAddress(), 0xffffffff); );

  // MONITOR TO KERNEL WAITING VALUE
  do {
    monitor_to_kernel_full = MemoryRead(FIFO_MONITOR_TO_KERNEL_FULL);
  } while ( monitor_to_kernel_full != 0 );

  // Sending the value to the FIFO
  MemoryWrite(MONITOR_TO_KERNEL, tag);

  // DEBUG
  IF_SIMULATION( debug_print_get_monitor_to_kernel(tag); );
  IF_HARDWARE_DEBUG( MemoryWrite(getDebugAddress(), tag);  MemoryWrite(getDebugAddress(), 0xffffffff ); );

  return;

}

uint32_t kernel_to_monitor_get_value(void) {

  uint32_t kernel_to_monitor_empty;
  uint32_t kernel_to_monitor_value;

  // DEBUG
  IF_SIMULATION( debug_print_waiting_kernel_to_monitor(); );
  IF_HARDWARE_DEBUG( MemoryWrite(getDebugAddress(), 0xffffffff); );

  // KERNEL TO MONITOR WAITING VALUE
  do {
    kernel_to_monitor_empty = MemoryRead(FIFO_KERNEL_TO_MONITOR_EMPTY);
  } while ( kernel_to_monitor_empty != 0 );

  // Reading the value from the FIFO
  kernel_to_monitor_value = MemoryRead(KERNEL_TO_MONITOR);

  // DEBUG
  IF_SIMULATION( debug_print_get_kernel_to_monitor(kernel_to_monitor_value); SimulationGetNextKernelToMonitorAddress(); );
  IF_HARDWARE_DEBUG( MemoryWrite(getDebugAddress(), kernel_to_monitor_value);  MemoryWrite(getDebugAddress(), 0xffffffff); );

  return kernel_to_monitor_value;

}


// Syscall handling
void handling_syscall(void) {

  uint32_t kernel_to_monitor_value;
  uint32_t address, size, tag, tag_of_memory;

  while(1) {

    // DEBUG
    IF_SIMULATION(debug_print_get_kernel_to_monitor_type("Type"));

    // Reading the value from the FIFO
    kernel_to_monitor_value = kernel_to_monitor_get_value();

    switch(kernel_to_monitor_value) {

    case (SYSCALL_PROTOCOL_SYSCALL_EXIT) :
      // DEBUG
      IF_SIMULATION(debug_print_get_kernel_to_monitor_type("PROTOCOL_SYSCALL_EXIT"); debug_print_syscall_protocol_newline(););
      return;


    case (SYSCALL_PROTOCOL_REQUEST_SEND_TAG):

      // DEBUG
      IF_SIMULATION(debug_print_get_kernel_to_monitor_type("PROTOCOL_SEND_TAG"));

      // DEBUG
      IF_SIMULATION(debug_print_get_kernel_to_monitor_type("Address"));
      address = kernel_to_monitor_get_value();

      // DEBUG
      IF_SIMULATION(debug_print_get_kernel_to_monitor_type("Size"));
      size = kernel_to_monitor_get_value();

      // DEBUG
      IF_SIMULATION(debug_print_get_kernel_to_monitor_type("Tag"));
      tag = kernel_to_monitor_get_value();

      search_and_affect_tag_from_lsm_hook(address, size, tag);

      break;


    case (SYSCALL_PROTOCOL_REQUEST_GET_TAG) :

      // DEBUG
      IF_SIMULATION(debug_print_get_kernel_to_monitor_type("PROTOCOL_REQUEST_TAG"));

      // DEBUG
      IF_SIMULATION(debug_print_get_kernel_to_monitor_type("Address"));
      address = kernel_to_monitor_get_value();

      // DEBUG
      IF_SIMULATION(debug_print_get_kernel_to_monitor_type("Size"));
      size = kernel_to_monitor_get_value();

      tag = search_tag_from_lsm_hook(address, size);

      // DEBUG
      IF_SIMULATION(debug_print_get_monitor_to_kernel_type("Tag of address"));

      // Sending the value to the LSM HOOKS
      monitor_to_kernel_send_value(tag);

      break;


    case (SYSCALL_PROTOCOL_REQUEST_ALLOC_MEMORY) :

      // DEBUG
      IF_SIMULATION(debug_print_get_kernel_to_monitor_type("PROTOCOL_REQUEST_ALLOC_MEMORY"));

      // DEBUG
      IF_SIMULATION(debug_print_get_kernel_to_monitor_type("Address"));
      address = kernel_to_monitor_get_value();

      // DEBUG
      IF_SIMULATION(debug_print_get_kernel_to_monitor_type("Size"));
      size = kernel_to_monitor_get_value();

      // First thing to do : checking if the memory is not already allocated.
      tag_of_memory = search_tag_from_lsm_hook(address, size);

      if(// Already allocated (double alloc)
         present_in_first_byte_policy(tag_of_memory, SECURITY_POLICY_DEFAULT_TAG[SECURITY_POLICY_DEFAULT_TAG_NEW_FRAME_HEAP])
         )
        {
          // RAISE EXCEPTION
          raise_irq_when_alloc_memory_failed(address, size, tag_of_memory,SECURITY_POLICY_DEFAULT_TAG[SECURITY_POLICY_DEFAULT_TAG_NEW_FRAME_HEAP]);
        }
      else
        {
          tag = SECURITY_POLICY_DEFAULT_TAG[SECURITY_POLICY_DEFAULT_TAG_NEW_FRAME_HEAP];
          search_and_affect_tag_from_lsm_hook(address, size, tag);
        }

      // DEBUG
      IF_SIMULATION(debug_print_get_monitor_to_kernel_type("Tag of address"));

      break;


    case (SYSCALL_PROTOCOL_REQUEST_DEALLOC_MEMORY) :

      // DEBUG
      IF_SIMULATION(debug_print_get_kernel_to_monitor_type("PROTOCOL_REQUEST_ALLOC_MEMORY"));

      // DEBUG
      IF_SIMULATION(debug_print_get_kernel_to_monitor_type("Address"));
      address = kernel_to_monitor_get_value();

      // DEBUG
      IF_SIMULATION(debug_print_get_kernel_to_monitor_type("Size"));
      size = kernel_to_monitor_get_value();

      // First thing to do : checking if the memory is not already deallocated.
      tag_of_memory = search_tag_from_lsm_hook(address, size);

      if(// Already deallocated (double free)
         present_in_first_byte_policy(tag_of_memory, SECURITY_POLICY_DEFAULT_TAG[SECURITY_POLICY_DEFAULT_TAG_DELETE_FRAME_HEAP])
         )
        {
          // RAISE EXCEPTION
          raise_irq_when_dealloc_memory_failed(address, size, tag_of_memory, SECURITY_POLICY_DEFAULT_TAG[SECURITY_POLICY_DEFAULT_TAG_DELETE_FRAME_HEAP]);

        }
      else
        {
          tag = SECURITY_POLICY_DEFAULT_TAG[SECURITY_POLICY_DEFAULT_TAG_DELETE_FRAME_HEAP];
          search_and_affect_tag_from_lsm_hook(address, size, tag);
        }

      // DEBUG
      IF_SIMULATION(debug_print_get_monitor_to_kernel_type("Tag of address"));

      break;


    default:
      // DEBUG
      IF_SIMULATION(debug_print_get_kernel_to_monitor_type("PROTOCOL_UNDEFINED"));
      IF_SIMULATION(debug_print_syscall_protocol_newline());
      return;
      break;

    }
  }

  return;
}


// Wait and read instrumentation value
uint32_t wait_read_instrumentation_value()
{

  uint32_t instrumentation_fifo_is_empty;
  uint32_t instrumentation_value;

  // DEBUG
  IF_SIMULATION( debug_waiting_instrumentation_value(); );
  IF_HARDWARE_DEBUG( MemoryWrite(getDebugAddress(), 0x22222222); );

  do{
    instrumentation_fifo_is_empty = MemoryRead(FIFO_INSTRUMENTATION_EMPTY);
  } while(instrumentation_fifo_is_empty == 1);

  instrumentation_value =  MemoryRead(READ_INSTRUMENTATION);

  // DEBUG
  IF_SIMULATION( debug_read_instrumentation(instrumentation_value); );
  IF_HARDWARE_DEBUG( MemoryWrite(getDebugAddress(), instrumentation_value); );
  IF_HARDWARE_DEBUG( MemoryWrite(getDebugAddress(), 0x22222222); );

  return instrumentation_value;

}



void decode_init_format(const uint32_t trace_value, uint32_t annot)
{
  uint32_t opcode = (annot >> 26) & SIX_BITS;
  uint32_t destReg = (annot >> 21) & FIVE_BITS;
  uint32_t srcReg = (annot >> 16) & FIVE_BITS;
  uint32_t imm_raw = (annot >> 0 ) & SIXTEEN_BITS;
  uint32_t imm = imm_raw;
  char is_neg = imm && NEG_IMM16;

  // DEBUG
  IF_SIMULATION(debug_decode_init_format());

  if(is_neg)
    {
      imm = imm & NEG_IMM16_MASK;
    }

  switch(opcode)
    {

      // MOUNIR - SVC
    case SVC_ANNOT :
      {
        IF_SIMULATION( debug_print_trace_value( trace_value ); );
        IF_SIMULATION( puts("\t============== SYSCALL !!! ============== \n"); );
        handling_syscall();
        break;
      }

      // MOUNIR - INSTR_VALUE_INIT : destReg = Instr_value
    case INSTR_VALUE_INIT :
      {
        // DEBUG
        IF_SIMULATION(debug_case_INSTR());

        uint32_t instr_value = wait_read_instrumentation_value();
        TRF[destReg] = instr_value;

        // DEBUG
        IF_SIMULATION(debug_print_TRF_entry(destReg, instr_value););
        break;
      }

      // MOUNIR - INIT_SYSTEM_TAG_REG_POLICY
    case INIT_SYSTEM_TAG_REG_POLICY:
      {
        TRF[destReg] = TRF[destReg] | SECURITY_POLICY_DEFAULT_TAG[imm];
        break;
      }

      // MOUNIR - INIT_USER_TAG_REG_POLICY
    case INIT_USER_TAG_REG_POLICY:
      {
        TRF[destReg] = TRF[destReg] | SECURITY_POLICY_DEFAULT_TAG[imm];
        break;
      }

    case LW :
      {
        // TODO: Connexion a quelle memoire ? RAM ? Valeur ou Tag ?
        // DEBUG
        IF_SIMULATION(debug_case_LW(););
        break;
      }

    case SW :
      {
        // TODO: Connexion a quelle memoire ? RAM ? Valeur ou Tag ? 
        // DEBUG
        IF_SIMULATION(debug_case_SW(););
        break;
      }

    case LCR :
      {
        // TODO: Connexion a quelle memoire ? RAM ? Valeur ou Tag ? 
        // DEBUG
        IF_SIMULATION(debug_case_LCR(););
        break;
      }

    case TLB :
      {
        // TODO: ??
        // DEBUG
        IF_SIMULATION(debug_case_TLB(););
        break;
      }

      // Tag_rri: reg_dst = reg_src (+/-) imm_16.
    case Tag_rri :
      {

        // DEBUG
        IF_SIMULATION(debug_case_TAG_RRI(););

        if(is_neg)
          {
            TRF[destReg] = TRF[srcReg] - imm;
            // DEBUG
            IF_SIMULATION(debug_print_TRF_op_imm(destReg, srcReg, "-", imm, TRF[destReg]););
          }
        else
          {
            TRF[destReg] = TRF[srcReg] + imm;
            // DEBUG
            IF_SIMULATION(debug_print_TRF_op_imm(destReg, srcReg, "+", imm, TRF[destReg]););
          }

        break;
      }

      // Tag_reg_imm : TRF[reg_dst] = imm
    case Tag_reg_imm :
      {
        TRF[destReg] = imm_raw;

        // DEBUG
        IF_SIMULATION(debug_case_TAG_Reg_Imm();); 
        IF_SIMULATION(debug_print_TRF_imm(destReg, imm_raw););

        break;
      }

      // Tag_reg_reg : reg_dst <= reg_src
    case Tag_reg_reg :
      {

        TRF[destReg] = TRF[srcReg];

        // DEBUG
        IF_SIMULATION(debug_case_TAG_reg_reg(););
        IF_SIMULATION(debug_print_TRF_reg(destReg, srcReg););

        break;
      }

      // Tag_mem_reg: Mem([reg1_dst + offset] ) = reg_src
    case Tag_mem_reg :
      {

        uint32_t value = TRF[srcReg];
        uint32_t address = TRF[destReg];

        // DEBUG
        IF_SIMULATION(debug_case_TAG_mem_reg(););

        if(is_neg)
          {
            address = address - imm;
            // DEBUG
            IF_SIMULATION(debug_print_mem_reg_offset_reg(destReg, TRF[destReg], TRF[srcReg], "-", imm, address, srcReg));
          }
        else
          {
            address = address + imm;
            // DEBUG
            IF_SIMULATION(debug_print_mem_reg_offset_reg(destReg, TRF[destReg], TRF[srcReg], "+", imm, address, srcReg));
          }

        if(!search_and_affect_value_of_address(address, value)){
          create_mem_entry_with_address(address);
          search_and_affect_value_of_address(address, value);
        }
        break;
      }

      // Lui : reg_dst <= (imm_1616) | reg_src
    case Lui :
      {
        uint32_t imm_value = imm_raw;
        uint32_t srcReg_value = TRF[srcReg];
        uint32_t final_value = (imm_value << 16) | srcReg_value;
        TRF[destReg] = final_value;

        // DEBUG
        IF_SIMULATION(debug_case_LUI(););
        IF_SIMULATION(debug_print_TRF_lui(destReg, imm_raw, srcReg, final_value););

        break;
      }

    default:
      {
        break;
      }
    }

}


void decode_tr_format(const uint32_t trace_value, uint32_t annot)
{

  uint32_t opcode = (annot >> 26) & SIX_BITS;
  uint32_t armOpcodeType = (annot >> 22) & FOUR_BITS;
  uint32_t destReg = (annot >> 17) & FIVE_BITS;
  uint32_t srcReg1 = (annot >> 12) & FIVE_BITS;
  uint32_t srcReg2 = (annot >> 7) & FIVE_BITS;
  // uint32_t func = (annot >> 0) & FIVE_BITS; //TODO

  uint32_t valueSrcReg1 = TRF[srcReg1];
  uint32_t valueSrcReg2 = TRF[srcReg2];

  // DEBUG
  IF_SIMULATION(debug_decode_tr_format(););

  switch(opcode) {

  case TRR :
    {
      switch(armOpcodeType) {
      case SECURITY_POLICY_ARM_OPCODE_NULL :
	{
	  uint32_t value = (*ARM_OPCODE_FUNC[SECURITY_POLICY_ARM_OPCODE_NULL]) (valueSrcReg1, valueSrcReg2);
	  TRF[destReg] = value;
    // DEBUG
    IF_SIMULATION(debug_print_TRF_TRR(destReg, srcReg1, srcReg2, valueSrcReg1, valueSrcReg2, value, "ARM_OPCODE_NULL"););
	  break;
	}

      case SECURITY_POLICY_ARM_OPCODE_MOV :
	{
	  uint32_t value = (*ARM_OPCODE_FUNC[SECURITY_POLICY_ARM_OPCODE_MOV]) (valueSrcReg1, valueSrcReg2);
	  TRF[destReg] = value;
    // DEBUG
    IF_SIMULATION(debug_print_TRF_TRR(destReg, srcReg1, srcReg2, valueSrcReg1, valueSrcReg2, value, "ARM_OPCODE_MOV"););
	  break;
	}

      case SECURITY_POLICY_ARM_OPCODE_Arith_Logic :
	{
	  uint32_t value = (*ARM_OPCODE_FUNC[SECURITY_POLICY_ARM_OPCODE_Arith_Logic]) (valueSrcReg1, valueSrcReg2);
	  TRF[destReg] = value;
    // DEBUG
    IF_SIMULATION(debug_print_TRF_TRR(destReg, srcReg1, srcReg2, valueSrcReg1, valueSrcReg2, value, "ARM_OPCODE_Arith_Logic"););
	  break;
	}

      case SECURITY_POLICY_ARM_OPCODE_COMP :
	{
	  uint32_t value = (*ARM_OPCODE_FUNC[SECURITY_POLICY_ARM_OPCODE_COMP]) (valueSrcReg1, valueSrcReg2);
	  TRF[destReg] = value;
    // DEBUG
    IF_SIMULATION(debug_print_TRF_TRR(destReg, srcReg1, srcReg2, valueSrcReg1, valueSrcReg2, value, "ARM_OPCODE_COMP"););
	  break;
	}

      case SECURITY_POLICY_ARM_OPCODE_Arith_logic_and_COMP :
	{
	  uint32_t value = (*ARM_OPCODE_FUNC[SECURITY_POLICY_ARM_OPCODE_Arith_logic_and_COMP]) (valueSrcReg1, valueSrcReg2);
	  TRF[destReg] = value;
    // DEBUG
    IF_SIMULATION(debug_print_TRF_TRR(destReg, srcReg1, srcReg2, valueSrcReg1, valueSrcReg2, value, "ARM_OPCODE_Arith_logic_and_COMP"););
	  break;
	}

      case SECURITY_POLICY_ARM_OPCODE_FP_MOV :
	{
	  uint32_t value = (*ARM_OPCODE_FUNC[SECURITY_POLICY_ARM_OPCODE_FP_MOV]) (valueSrcReg1, valueSrcReg2);
	  TRF[destReg] = value;
    // DEBUG
    IF_SIMULATION(debug_print_TRF_TRR(destReg, srcReg1, srcReg2, valueSrcReg1, valueSrcReg2, value, "ARM_OPCODE_FP_MOV"););
	  break;
	}

      case SECURITY_POLICY_ARM_OPCODE_FP_Arith_Logic :
	{
	  uint32_t value = (*ARM_OPCODE_FUNC[SECURITY_POLICY_ARM_OPCODE_FP_Arith_Logic]) (valueSrcReg1, valueSrcReg2);
	  TRF[destReg] = value;
    // DEBUG
    IF_SIMULATION(debug_print_TRF_TRR(destReg, srcReg1, srcReg2, valueSrcReg1, valueSrcReg2, value, "ARM_OPCODE_FP_Arith_Logic"););
	  break;
	}

      case SECURITY_POLICY_ARM_OPCODE_FP_CMP :
	{
	  uint32_t value = (*ARM_OPCODE_FUNC[SECURITY_POLICY_ARM_OPCODE_FP_CMP]) (valueSrcReg1, valueSrcReg2);
	  TRF[destReg] = value;
    // DEBUG
    IF_SIMULATION(debug_print_TRF_TRR(destReg, srcReg1, srcReg2, valueSrcReg1, valueSrcReg2, value, "ARM_OPCODE_FP_CMP"););
	  break;
	}

      case SECURITY_POLICY_ARM_OPCODE_CUSTOM1 :
	{
	  uint32_t value = (*ARM_OPCODE_FUNC[SECURITY_POLICY_ARM_OPCODE_CUSTOM1]) (valueSrcReg1, valueSrcReg2);
	  TRF[destReg] = value;
    // DEBUG
    IF_SIMULATION(debug_print_TRF_TRR(destReg, srcReg1, srcReg2, valueSrcReg1, valueSrcReg2, value, "ARM_OPCODE_CUSTOM1" ););
	  break;
	}

      case SECURITY_POLICY_ARM_OPCODE_CUSTOM2 :
	{
	  uint32_t value = (*ARM_OPCODE_FUNC[SECURITY_POLICY_ARM_OPCODE_CUSTOM2]) (valueSrcReg1, valueSrcReg2);
	  TRF[destReg] = value;
    // DEBUG
    IF_SIMULATION(debug_print_TRF_TRR(destReg, srcReg1, srcReg2, valueSrcReg1, valueSrcReg2, value, "ARM_OPCODE_CUSTOM2" ););
	  break;
	}

      case SECURITY_POLICY_ARM_OPCODE_CUSTOM3 :
	{
	  uint32_t value = (*ARM_OPCODE_FUNC[SECURITY_POLICY_ARM_OPCODE_CUSTOM3]) (valueSrcReg1, valueSrcReg2);
	  TRF[destReg] = value;
    // DEBUG
    IF_SIMULATION(debug_print_TRF_TRR(destReg, srcReg1, srcReg2, valueSrcReg1, valueSrcReg2, value, "ARM_OPCODE_CUSTOM3" ););
	  break;
	}

      case SECURITY_POLICY_ARM_OPCODE_CUSTOM4 :
	{
	  uint32_t value = (*ARM_OPCODE_FUNC[SECURITY_POLICY_ARM_OPCODE_CUSTOM4]) (valueSrcReg1, valueSrcReg2);
	  TRF[destReg] = value;
    // DEBUG
    IF_SIMULATION(debug_print_TRF_TRR(destReg, srcReg1, srcReg2, valueSrcReg1, valueSrcReg2, value, "ARM_OPCODE_CUSTOM4" ););
	  break;
	}
	
      case SECURITY_POLICY_ARM_OPCODE_Not_implemented1 :
	{
	  uint32_t value = (*ARM_OPCODE_FUNC[SECURITY_POLICY_ARM_OPCODE_Not_implemented1]) (valueSrcReg1, valueSrcReg2);
	  TRF[destReg] = value;
    // DEBUG
    IF_SIMULATION(debug_print_TRF_TRR(destReg, srcReg1, srcReg2, valueSrcReg1, valueSrcReg2, value, "ARM_OPCODE_Not_implemented1" ););
	  break;
	}
	
      case SECURITY_POLICY_ARM_OPCODE_Not_implemented2 :
	{
	  uint32_t value = (*ARM_OPCODE_FUNC[SECURITY_POLICY_ARM_OPCODE_Not_implemented2]) (valueSrcReg1, valueSrcReg2);
	  TRF[destReg] = value;
    // DEBUG
    IF_SIMULATION(debug_print_TRF_TRR(destReg, srcReg1, srcReg2, valueSrcReg1, valueSrcReg2, value, "ARM_OPCODE_Not_implemented2" ););
	  break;
	}
	
      case SECURITY_POLICY_ARM_OPCODE_Not_implemented3 :
	{
	  uint32_t value = (*ARM_OPCODE_FUNC[SECURITY_POLICY_ARM_OPCODE_Not_implemented3]) (valueSrcReg1, valueSrcReg2);
	  TRF[destReg] = value;
    // DEBUG
    IF_SIMULATION(debug_print_TRF_TRR(destReg, srcReg1, srcReg2, valueSrcReg1, valueSrcReg2, value, "ARM_OPCODE_Not_implemented3" ););
	  break;
	}

      case SECURITY_POLICY_ARM_OPCODE_Not_implemented4 :
	{
	  uint32_t value = (*ARM_OPCODE_FUNC[SECURITY_POLICY_ARM_OPCODE_Not_implemented4]) (valueSrcReg1, valueSrcReg2);
	  TRF[destReg] = value;
    // DEBUG
    IF_SIMULATION(debug_print_TRF_TRR(destReg, srcReg1, srcReg2, valueSrcReg1, valueSrcReg2, value, "ARM_OPCODE_Not_implemented4" ););
	  break;
	}

      default:
	{
    // TODO UNDEFINED CASE
	  break;
	}
      }

      break;
    }

    case Tag_arith_log :
      {
        // DEBUG
        IF_SIMULATION(debug_case_TAG_arith_log(););
        break;
      }

    case Tag_check_reg :
      {
        // DEBUG
        IF_SIMULATION(debug_case_TAG_check_reg(););

        if(TRF[destReg] == TRF[srcReg1])
        {
          // OK TODO
        }
        else
        {
          // Exception
        }
        break;
      }

    default:
      {
	break;
      }

    }

}


void decode_ti_format(const uint32_t trace_value, uint32_t annot)
{

  uint32_t opcode = (annot >> 26) & SIX_BITS;
   uint32_t armOpcodeType = (annot >> 22) & FOUR_BITS;
  uint32_t destReg = (annot >> 17) & FIVE_BITS;
  uint32_t srcReg = (annot >> 12) & FIVE_BITS;
  uint32_t Immediate = (annot >> 0) & TWELVE_BITS;

  char is_neg = Immediate && NEG_IMM12;

  // DEBUG
  IF_SIMULATION(debug_ti_format(););

  if(is_neg)
    {
      Immediate = Immediate & NEG_IMM12_MASK;
    }

 switch(opcode)
   {

     // tag (Mem(reg_dst + offset)) = tag(reg_src1)
   case Tag_mem_tr :
     {

       uint32_t addr;

       // DEBUG
       IF_SIMULATION(debug_ti_Tag_mem_tr(););

       if(is_neg)
         {
           addr = TRF[destReg] - Immediate;
           // DEBUG
           IF_SIMULATION(debug_Tag_mem_tr(destReg, srcReg, addr, Immediate, "-", TRF[srcReg]););
         }
       else
         {
           addr =  TRF[destReg] + Immediate;
           // DEBUG
           IF_SIMULATION(debug_Tag_mem_tr(destReg, srcReg, addr, Immediate, "+", TRF[srcReg]););
         }

       if(!search_and_affect_value_of_address(addr, TRF[srcReg])){
         create_mem_entry_with_address(addr);
         search_and_affect_value_of_address(addr, TRF[srcReg]);
       }

       break;
     }

     // tag(reg_dst) = tag (Mem(reg_src + offset))
   case Tag_tr_mem :
     {
       uint32_t addr;

       // DEBUG
       IF_SIMULATION(debug_ti_Tag_tr_mem(););

       if(is_neg)
         {
           addr = TRF[srcReg] - Immediate;
         }
       else
         {
           addr = TRF[srcReg] + Immediate;
         }

       uint32_t value_of_addr = search_value_of_address(addr);

       TRF[destReg] = value_of_addr;

       // DEBUG
       IF_SIMULATION(debug_Tag_tr_mem(destReg, srcReg, addr, Immediate, value_of_addr, (is_neg ? "-" : "+" )););
       break;

     }

     // tag (Mem(reg_dst + offset)) = tag(reg_src1) duplicate mem_tr_2
   case Tag_mem_tr_2 :
     {  uint32_t addr;

       // DEBUG
       IF_SIMULATION(debug_ti_Tag_mem_tr_2(););

       if(is_neg)
         {
           addr = TRF[destReg] - Immediate;
           // DEBUG
           IF_SIMULATION(debug_Tag_mem_tr(destReg, srcReg, addr, Immediate, "-", TRF[srcReg]););
         }
       else
         {
           addr =  TRF[destReg] + Immediate;
           // DEBUG
           IF_SIMULATION(debug_Tag_mem_tr(destReg, srcReg, addr, Immediate, "+", TRF[srcReg]););
         }

       if(!search_and_affect_value_of_address(addr, TRF[srcReg])){
         create_mem_entry_with_address(addr);
         search_and_affect_value_of_address(addr, TRF[srcReg]);
       }

       break;

     }

     // tag(reg_dst) = tag (Mem(reg_src + offset))
   case Tag_tr_mem_2 :
     {

       // DEBUG
       IF_SIMULATION(debug_ti_Tag_tr_mem_2(););

       uint32_t addr;
       if(is_neg)
         {
           addr = TRF[srcReg] - Immediate;
         }
       else
         {
           addr = TRF[srcReg] + Immediate;
         }

       uint32_t value_of_addr = search_value_of_address(addr);
       TRF[destReg] = value_of_addr;

       // DEBUG
       IF_SIMULATION(debug_Tag_tr_mem(destReg, srcReg, addr, Immediate, value_of_addr, (is_neg ? "-" : "+" )););

       break;
     }

   case Tag_instrumentation_tr :
     {
       /*
       uint32_t instr_value = wait_read_instrumentation_value();
       TRF[destReg] = instr_value;
       */

       // DEBUG
       IF_SIMULATION( debug_ti_Tag_instrumentation_tr(); );
       break;
     }
     // Tag_tr_instrumentation - reg_dst <= tag(Mem(instrumentation)) 
   case Tag_tr_instrumentation :
     {
       /*
       uint32_t instr_value_addr = wait_read_instrumentation_value();
       uint32_t value_of_addr = search_value_of_address(instr_value_addr);
       TRF[destReg] = value_of_addr;
       */

       // DEBUG
       IF_SIMULATION(debug_ti_Tag_tr_instrumentation(); );
       break;
     }
   case Tag_Kblare_tr :
     {
       // DEBUG
       IF_SIMULATION( debug_ti_Tag_Kblare_tr(); );
       break;
     }
   case Tag_tr_Kblare :
     {
       // DEBUG
       IF_SIMULATION( debug_ti_Tag_tr_Kblare(); );
       break;
     }
     // Tag_check_mem -- Check tag value of mem address specified in register (reg_src1 + offset)
   case Tag_check_mem :
     {
       uint32_t tag_of_address_reg = search_value_of_address(TRF[destReg]);
       uint32_t value_of_tag  = TRF[srcReg];

       // DEBUG
       IF_SIMULATION( debug_ti_Tag_check_mem(); );

       if(tag_of_address_reg != value_of_tag)
         {
           // Raising exception
           IF_NOT_SIMULATION(
                             uint32_t* PLtoPSaddress = PLTOPS;
                             MemoryWrite(PLtoPSaddress++, TAG_CHECK_MEM_FAILED);
                             MemoryWrite(PLtoPSaddress++, 4);
                             MemoryWrite(PLtoPSaddress++, trace_value);
                             MemoryWrite(PLtoPSaddress++, annot);
                             MemoryWrite(PLtoPSaddress++, tag_of_address_reg);
                             MemoryWrite(PLtoPSaddress++, value_of_tag);
                             );

           // DEBUG
           IF_SIMULATION(
                         debug_raise_IRQ();
                         print_hex(TAG_CHECK_MEM_FAILED);
                         puts("\n");
                         print_hex(4);
                         puts("\n");
                         print_hex(trace_value);
                         puts("\n");
                         print_hex(annot);
                         puts("\n");
                         print_hex(tag_of_address_reg);
                         puts("\n");
                         print_hex(value_of_tag);
                         puts("\n");
                         );
         }
       // DEBUG
       IF_SIMULATION( debug_print_Tag_check_mem(destReg, TRF[destReg], tag_of_address_reg, srcReg, value_of_tag, (tag_of_address_reg != value_of_tag) ? "!=" : "=="); );
       break;
     }

   // CHECK_SYSTEM_TAG_MEM_POLICY -- Check tag value of mem address specified in register (reg_src1 + offset)
   case CHECK_SYSTEM_TAG_MEM_POLICY:
     {
       uint32_t address_to_check;
       uint32_t tag_of_address_to_check;
       uint32_t tag_should_have = SECURITY_POLICY_DEFAULT_TAG[destReg];
       // uint32_t operation  = armOpcodeType;

       if(is_neg){
         address_to_check = TRF[srcReg] - Immediate;
       }
       else {
         address_to_check = TRF[srcReg] + Immediate;
       }

       tag_of_address_to_check = search_value_of_address(address_to_check);
       // DEBUG
       // IF_SIMULATION( debug_ti_Tag_check_mem(); );

       if(!(tag_of_address_to_check & tag_should_have))
         {
           // Raising exception
           IF_NOT_SIMULATION(
                             uint32_t* PLtoPSaddress = PLTOPS;
                             MemoryWrite(PLtoPSaddress++, TAG_CHECK_MEM_FAILED);
                             MemoryWrite(PLtoPSaddress++, 5);
                             MemoryWrite(PLtoPSaddress++, trace_value);
                             MemoryWrite(PLtoPSaddress++, annot);
                             MemoryWrite(PLtoPSaddress++, tag_of_address_to_check);
                             MemoryWrite(PLtoPSaddress++, tag_should_have);
                             );

           // DEBUG
           IF_SIMULATION(
                         debug_raise_IRQ();
                         print_hex(TAG_CHECK_MEM_FAILED);
                         puts("\n");
                         print_hex(5);
                         puts("\n");
                         print_hex(trace_value);
                         puts("\n");
                         print_hex(annot);
                         puts("\n");
                         print_hex(tag_of_address_to_check);
                         puts("\n");
                         print_hex(tag_should_have);
                         puts("\n");
                         );
         }
       // DEBUG
       IF_SIMULATION( debug_print_Tag_check_mem(destReg, TRF[destReg], tag_of_address_reg, srcReg, value_of_tag, (tag_of_address_reg != value_of_tag) ? "!=" : "=="); );
       break;
     }

    default:
      {
        break;
      }
   }

}


// Decode annotation
uint32_t decode_annotation(const uint32_t trace_value, const uint32_t annotation) {

  uint32_t opcode = (annotation >> 26) & SIX_BITS;

  #ifdef DEBUG
    puts("\n decode_annotation \n");
  #endif

  switch(opcode)
    {
    case SVC_ANNOT :
    case INSTR_VALUE_INIT :
    case INIT_SYSTEM_TAG_REG_POLICY :
    case INIT_USER_TAG_REG_POLICY :
    case LW :
    case SW :
    case LCR :
    case TLB :
    case Tag_rri :
    case Tag_reg_imm :
    case Tag_reg_reg :
    case Tag_mem_reg :
    case Lui :
      {
        decode_init_format(trace_value, annotation);
        break;
      }

    case TRR :
    case Tag_arith_log :
    case Tag_check_reg :
      {
        decode_tr_format(trace_value, annotation);
        break;
      }

    case Tag_mem_tr :
    case Tag_tr_mem :
    case Tag_mem_tr_2 :
    case Tag_tr_mem_2 :
    case Tag_instrumentation_tr :
    case Tag_tr_instrumentation :
    case Tag_Kblare_tr :
    case Tag_tr_Kblare :
    case Tag_check_mem :
    case CHECK_SYSTEM_TAG_MEM_POLICY:
      {
        decode_ti_format(trace_value, annotation);
        break;
      }

    default:
      {
        break;
      }
    }

  return 0;
}
