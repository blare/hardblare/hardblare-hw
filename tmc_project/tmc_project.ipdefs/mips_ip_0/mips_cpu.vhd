---------------------------------------------------------------------
-- TITLE: MIPS CPU core
-- AUTHOR: Steve Rhoads (rhoadss@yahoo.com)
-- DATE CREATED: 2/15/01
-- FILENAME: mips_cpu.vhd
-- PROJECT: MIPS CPU core
-- COPYRIGHT: Software placed into the public domain by the author.
--    Software 'as is' without warranty.  Author liable for nothing.
-- DESCRIPTION:
-- Top level VHDL document that ties the eight other entities together.
-- Implements a MIPS CPU.  Based on information found in:
--    "MIPS RISC Architecture" by Gerry Kane and Joe Heinrich
--    and "The Designer's Guide to VHDL" by Peter J. Ashenden
-- An add instruction would take the following steps (see cpu.gif):
--    1.  The "pc_next" entity would have previously passed the program
--        counter (PC) to the "mem_ctrl" entity.
--    2.  "Mem_ctrl" passes the opcode to the "control" entity.
--    3.  "Control" converts the 32-bit opcode to a 60-bit VLWI opcode
--        and sends control signals to the other entities.
--    4.  Based on the rs_index and rt_index control signals, "reg_bank" 
--        sends the 32-bit reg_source and reg_target to "bus_mux".
--    5.  Based on the a_source and b_source control signals, "bus_mux"
--        multiplexes reg_source onto a_bus and reg_target onto b_bus.
--    6.  Based on the alu_func control signals, "alu" adds the values
--        from a_bus and b_bus and places the result on c_bus.
--    7.  Based on the c_source control signals, "bus_bux" multiplexes
--        c_bus onto reg_dest.
--    8.  Based on the rd_index control signal, "reg_bank" saves
--        reg_dest into the correct register.
-- The CPU is implemented as a two stage pipeline with step #1 in the
-- first stage and steps #2-8 occuring the second stage.
--
-- The CPU core was synthesized for 0.13 um line widths with an area
-- of 0.2 millimeters squared.  The maximum latency was less than 6 ns 
-- for a maximum clock speed of 150 MHz.
---------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use work.mips_pack.all;

entity mips_cpu is
   port(clk           : in std_logic;
        reset_in      : in std_logic;
        --stall         : in std_logic;
        intr_in       : in std_logic;

        i_mem_enable  : out std_logic;
        i_mem_address : out std_logic_vector(31 downto 0);
        i_mem_data_r  : in std_logic_vector(31 downto 0);
        i_mem_sel     : out std_logic_vector(3 downto 0);
        
        mem_address   : out std_logic_vector(31 downto 0);
        mem_data_w    : out std_logic_vector(31 downto 0);
        mem_data_r    : in std_logic_vector(31 downto 0);
        mem_sel       : out std_logic_vector(3 downto 0);
        mem_write     : out std_logic;
        mem_read      : out std_logic;
        mem_pause     : in std_logic;

        data_mem_cs         : out std_logic;
        annotations_mem_cs  : out std_logic;
        annotations_bb_cs   : out std_logic;
        trace_mem_cs        : out std_logic;
        kernel2monitor_cs   : out std_logic;
        monitor2kernel_cs   : out std_logic
    
        --t_pc          : out std_logic_vector(31 downto 0);
        --t_opcode      : out std_logic_vector(31 downto 0);
        --t_r_dest      : out std_logic_vector(31 downto 0)
        );
end; --entity mips_cpu

architecture logic of mips_cpu is

component pc_next
   port(clk          : in std_logic;
        reset_in     : in std_logic;
        stall        : in  std_logic;
        --pc_reg       : in  std_logic_vector(31 downto 2);
        pc_new       : in std_logic_vector(31 downto 2);
        take_branch  : in std_logic;
        pause_in     : in std_logic;
        opcode25_0   : in std_logic_vector(25 downto 0);
        pc_source    : in pc_source_type;
        i_mem_enable : out std_logic;
        pc_future    : out std_logic_vector(31 downto 0);
        pc_prev      : out std_logic_vector(31 downto 0);
        pc_out       : out std_logic_vector(31 downto 0));
end component;

--component mem_ctrl   
--   port(clk          : in std_logic;
--        reset_in     : in std_logic;
--        pause_in     : in std_logic;
--        hazard       : in std_logic;
--        nullify_op   : in std_logic;
--        address_pc   : in std_logic_vector(31 downto 0);        
--        opcode_out   : out std_logic_vector(31 downto 0);
                
--        data_read    : out std_logic_vector(31 downto 0);
--        pause_out    : out std_logic;
                
--        mem_address  : out std_logic_vector(31 downto 0);
--        mem_data_r   : in std_logic_vector(31 downto 0);
--        mem_read_v   : in std_logic;
--        mem_pause    : in std_logic);
--end component;

component pipeline_IF_ID is 
port    (
        clk, reset      : in  std_logic;
        stall           : in  std_logic;
        take_branch     : in  std_logic;
        pc_source       : in  pc_source_type;
        opcode          : in  std_logic_vector(31 downto 0);
        opcode_out      : out std_logic_vector(31 downto 0);
        pc              : in  std_logic_vector(31 downto 0);
        pc_out          : out std_logic_vector(31 downto 0) );
end component;

component control 
   port(opcode       : in  std_logic_vector(31 downto 0);
        intr_signal  : in  std_logic;
        stall        : out std_logic;
        rs_index     : out std_logic_vector(5 downto 0);
        rt_index     : out std_logic_vector(5 downto 0);
        rd_index     : out std_logic_vector(5 downto 0);
        imm_out      : out std_logic_vector(15 downto 0);
        alu_func     : out alu_function_type;
        shift_func   : out shift_function_type;
        mult_func    : out mult_function_type;
        branch_func  : out branch_function_type;
        a_source_out : out a_source_type;
        b_source_out : out b_source_type;
        c_source_out : out c_source_type;
        pc_source_out: out pc_source_type;
        ldr_instr    : out std_logic;
        mem_source_out:out mem_source_type);
end component;

component reg_bank
   port(clk            : in  std_logic;
        reset          : in  std_logic;
        pause          : in  std_logic;
        rs_index       : in  std_logic_vector(5 downto 0);
        rt_index       : in  std_logic_vector(5 downto 0);
        rd_index       : in  std_logic_vector(5 downto 0);
        reg_source_out : out std_logic_vector(31 downto 0);
        reg_target_out : out std_logic_vector(31 downto 0);
        reg_dest_new   : in  std_logic_vector(31 downto 0);
        intr_enable    : out std_logic);
end component;

component bus_mux 
   port(imm_in       : in  std_logic_vector(15 downto 0);
        reg_source   : in  std_logic_vector(31 downto 0);
        forward      : in  std_logic_vector(1 downto 0);
        forward_branch: in std_logic;
        a_mux        : in  a_source_type;
        a_out        : out std_logic_vector(31 downto 0);
        reg_target   : in  std_logic_vector(31 downto 0);
        b_mux        : in  b_source_type;
        b_out        : out std_logic_vector(31 downto 0);
        c_alu        : in  std_logic_vector(31 downto 0);
        c_bus        : in  std_logic_vector(31 downto 0);
        c_memory     : in  std_logic_vector(31 downto 0);
        c_pc         : in  std_logic_vector(31 downto 0);
        c_pc_np      : in  std_logic_vector(31 downto 0);
        c_mux        : in  c_source_type;
        c_imm_in     : in  std_logic_vector(15 downto 0);
        reg_dest_out : out std_logic_vector(31 downto 0);
        pc_new       : out std_logic_vector(31 downto 0) );
end component;


component hazard_detection_unit is 
port (
  rs_index_dec  : in  std_logic_vector(5 downto 0); -- exec stage
  rt_index_dec  : in  std_logic_vector(5 downto 0); -- exec stage
  rt_index_ex   : in  std_logic_vector(5 downto 0); -- mem stage
  c_source_ex   : in  c_source_type; 
  pc_pause      : out std_logic;  
  hazard        : out std_logic );
end component;


component pipeline_ID_IE is 
port    (
        clk, reset      : in std_logic;
        stall           : in std_logic;
        
        stall_if        : in  std_logic;
        stall_if_out    : out std_logic;
        
        a_out_in        : in std_logic_vector(31 downto 0);
        a_out           : out std_logic_vector(31 downto 0);
           
        b_out_in        : in std_logic_vector(31 downto 0);
        b_out           : out std_logic_vector(31 downto 0);

        a_source        : in  a_source_type;
        a_source_out    : out a_source_type;

        b_source        : in  b_source_type;
        b_source_out    : out b_source_type;

        c_source        : in c_source_type;
        c_source_out    : out c_source_type;

        c_pc            : in  std_logic_vector(31 downto 0);
        c_pc_out        : out std_logic_vector(31 downto 0);

        imm_in          : in std_logic_vector(15 downto 0);
        imm_out         : out std_logic_vector(15 downto 0);
        
        reg_source      : in  std_logic_vector(31 downto 0);
        reg_source_out  : out std_logic_vector(31 downto 0);

        rs_index        : in std_logic_vector(5 downto 0);
        rs_index_out    : out std_logic_vector(5 downto 0);

        rt_index        : in std_logic_vector(5 downto 0);
        rt_index_out    : out std_logic_vector(5 downto 0);

        rd_index        : in std_logic_vector(5 downto 0);
        rd_index_out    : out std_logic_vector(5 downto 0);

        reg_target      : in  std_logic_vector(31 downto 0);
        reg_target_out  : out  std_logic_vector(31 downto 0);            

        take_branch_in  : in std_logic;
        take_branch     : out std_logic;

        ldr_instr       : in  std_logic; 
        ldr_instr_out   : out std_logic;

        alu_func_i      : in alu_function_type;
        shift_func_i    : in shift_function_type;
        mult_func_i     : in mult_function_type;
        branch_func_i   : in branch_function_type;        
        pc_source       : in pc_source_type;
        mem_source      : in mem_source_type;

        alu_func        : out alu_function_type;
        shift_func      : out shift_function_type;
        mult_func       : out mult_function_type;
        branch_func     : out branch_function_type;        
        pc_source_out   : out pc_source_type;
        mem_source_out  : out mem_source_type );
end component;

component forward_unit is 
port (
    a_in        : in  std_logic_vector(31 downto 0);
    b_in        : in  std_logic_vector(31 downto 0);
    alu_p       : in  std_logic_vector(31 downto 0);
    alu_p_p     : in  std_logic_vector(31 downto 0);
    data_read_m : in  std_logic_vector(31 downto 0);
    reg_source  : in  std_logic_vector(31 downto 0);
    reg_target  : in  std_logic_vector(31 downto 0);
    branch_func : in  branch_function_type;
    rs_index_dec: in std_logic_vector(5 downto 0); -- dec stage
    rt_index_dec: in std_logic_vector(5 downto 0); -- dec stage
    rs_index    : in std_logic_vector(5 downto 0); -- exec stage
    rt_index    : in std_logic_vector(5 downto 0); -- exec stage
    rd_index    : in std_logic_vector(5 downto 0); -- exec stage
    rd_index_dec: in std_logic_vector(5 downto 0); -- dec stage
    rd_index_mem: in std_logic_vector(5 downto 0); -- mem stage
    rt_index_mem: in std_logic_vector(5 downto 0); -- mem stage
    rt_index_wb : in std_logic_vector(5 downto 0); -- wb stage
    rd_index_wb : in std_logic_vector(5 downto 0); -- wb stage
    ldr_instr   : in std_logic; -- wb of load instruction
    a_source    : in a_source_type; -- exec stage
    b_source    : in b_source_type; -- exec stage
    mem_source_x: in mem_source_type; -- exec
    mem_source_m: in mem_source_type; -- mem
    mem_source_w: in mem_source_type; -- wb
    c_source_mem: in c_source_type; -- mem stage
    c_source_wb : in c_source_type; -- wb stage 
    forward_c   : out std_logic_vector(1 downto 0);
    forward_d   : out std_logic_vector(1 downto 0);
    forward_branch : out std_logic;
    take_branch : out std_logic;
    a_out       : out std_logic_vector(31 downto 0);
    b_out       : out std_logic_vector(31 downto 0) );
end component;

component alu
   port(a_in         : in  std_logic_vector(31 downto 0);
        b_in         : in  std_logic_vector(31 downto 0);

        alu_function : in  alu_function_type;
        c_alu        : out std_logic_vector(31 downto 0) );
end component;

component shifter
   port(value        : in  std_logic_vector(31 downto 0);
        shift_amount : in  std_logic_vector(4 downto 0);
        shift_func   : in  shift_function_type;
        c_shift      : out std_logic_vector(31 downto 0));
end component;

component mult
   port(clk       : in std_logic;
        a, b      : in std_logic_vector(31 downto 0);
        mult_func : in mult_function_type;
        c_mult    : out std_logic_vector(31 downto 0);
        pause_out : out std_logic);
end component;

component pipeline_IE_IM is
  port(
    clk, reset      : in std_logic ;
    stall           : in std_logic ;
    hazard          : in std_logic ;
    forward_d       : in std_logic_vector(1 downto 0);
    c_alu_i         : in std_logic_vector(31 downto 0);
    c_shift_i       : in std_logic_vector(31 downto 0);
    c_mult_i        : in std_logic_vector(31 downto 0);
    c_source        : in c_source_type;
    pc_source       : in pc_source_type;
    c_pc            : in  std_logic_vector(31 downto 0);
    imm_in          : in std_logic_vector(15 downto 0);    
    mem_source_out_i: in mem_source_type;
    rt_index        : in std_logic_vector(5 downto 0);
    rd_index        : in std_logic_vector(5 downto 0);
    reg_target      : in  std_logic_vector(31 downto 0);
    ldr_instr       : in  std_logic; 
    ldr_instr_out   : out std_logic;
    reg_target_out  : out  std_logic_vector(31 downto 0);      
    c_alu           : out std_logic_vector(31 downto 0);
    c_shift         : out std_logic_vector(31 downto 0);
    c_mult          : out std_logic_vector(31 downto 0);
    c_source_out    : out c_source_type;
    pc_source_out   : out pc_source_type;
    c_pc_out        : out std_logic_vector(31 downto 0);
    imm_out         : out std_logic_vector(15 downto 0);
    rt_index_out    : out std_logic_vector(5 downto 0);
    rd_index_out    : out std_logic_vector(5 downto 0);
    forward_d_out   : out std_logic_vector(1 downto 0);
    mem_source_out  : out mem_source_type );
end component;

component data_mem_ctrl   
   port(clk          : in std_logic;
        reset_in     : in std_logic;
        forward_d    : in std_logic_vector(1 downto 0);
        c_bus_mem    : in std_logic_vector(31 downto 0);
        c_alu_wb     : in std_logic_vector(31 downto 0);

        address_data : in std_logic_vector(31 downto 0);
        mem_source   : in mem_source_type;
        data_write   : in std_logic_vector(31 downto 0);
        data_read    : out std_logic_vector(31 downto 0);
        pause_out    : out std_logic;
        
        mem_address  : out std_logic_vector(31 downto 0);
        mem_data_w   : out std_logic_vector(31 downto 0);
        mem_data_r_p : in std_logic_vector(data_width-1 downto 0);
        mem_data_r   : in std_logic_vector(31 downto 0);
        mem_byte_sel : out std_logic_vector(3 downto 0);
        mem_write    : out std_logic;
        mem_read     : out std_logic;
        mem_pause    : in std_logic);
end component;

component pipeline_IM_IWB is
  port(
    clk, reset    : in std_logic ;
    stall         : in std_logic ;
    hazard        : in std_logic ;
    c_alu_i       : in std_logic_vector(31 downto 0);
    c_shift_i     : in std_logic_vector(31 downto 0);
    c_mult_i      : in std_logic_vector(31 downto 0);
    c_source      : in c_source_type;
    pc_source     : in pc_source_type;
    c_pc          : in  std_logic_vector(31 downto 0);
    imm_in        : in std_logic_vector(15 downto 0);
    mem_data      : in std_logic_vector(31 downto 0);
    rt_index    : in std_logic_vector(5 downto 0);
    rd_index      : in std_logic_vector(5 downto 0);
    mem_source_out_i: in mem_source_type;
    ldr_instr       : in  std_logic; 
    
    ldr_instr_out   : out std_logic;
    c_alu         : out std_logic_vector(31 downto 0);
    c_shift       : out std_logic_vector(31 downto 0);
    c_mult        : out std_logic_vector(31 downto 0);
    c_source_out  : out c_source_type;
    pc_source_out : out pc_source_type;
    c_pc_out      : out  std_logic_vector(31 downto 0);
    imm_out       : out std_logic_vector(15 downto 0);
    rt_index_out  : out std_logic_vector(5 downto 0);
    rd_index_out  : out std_logic_vector(5 downto 0);
    mem_source_out : out mem_source_type;
    mem_data_out_p  : out std_logic_vector(31 downto 0); -- pipelined
    mem_data_out  : out std_logic_vector(31 downto 0) );
end component;
    
component io_controller is 
    port(
        mem_address_in      : in  std_logic_vector(31 downto 0);
        data_mem_cs         : out std_logic;
        annotations_mem_cs  : out std_logic;
        annotations_bb_cs   : out std_logic;
        trace_mem_cs		: out std_logic;
        kernel2monitor_cs   : out std_logic;    
        monitor2kernel_cs   : out std_logic
    );
end component;

   signal opcode  : std_logic_vector(31 downto 0);
   signal rs_index, rt_index, rd_index     : std_logic_vector(5 downto 0);
   signal rs_index_p, rt_index_p           : std_logic_vector(5 downto 0);
   signal rt_index_p_p, rt_index_p_p_p     : std_logic_vector(5 downto 0);
   signal rd_index_p, rd_index_p_p , rd_index_p_p_p     : std_logic_vector(5 downto 0);
   signal reg_source, reg_target, reg_dest : std_logic_vector(31 downto 0);
   signal a_bus, b_bus : std_logic_vector(31 downto 0);
   signal c_bus_ex : std_logic_vector(31 downto 0); -- ex stage (execute stage)
   signal c_bus_mem : std_logic_vector(31 downto 0); -- mem stage (memory access stage or memory stage)
   signal c_bus : std_logic_vector(31 downto 0); -- write back (wb) stage
   signal a_bus_p, b_bus_p, reg_dest_p : std_logic_vector(31 downto 0);
   signal a_bus_f, b_bus_f : std_logic_vector(31 downto 0);
   signal c_alu, c_shift, c_mult, c_memory, c_memory_i
        : std_logic_vector(31 downto 0);
   signal c_alu_p, c_shift_p, c_mult_p, c_memory_p
        : std_logic_vector(31 downto 0);
   signal c_alu_p_p, c_shift_p_p, c_mult_p_p
        : std_logic_vector(31 downto 0);
   signal imm            : std_logic_vector(15 downto 0);
   signal imm_p, imm_p_p,
          imm_p_p_p      : std_logic_vector(15 downto 0);
   signal pc, pc_new     : std_logic_vector(31 downto 0);
   attribute keep : string;
   attribute keep of pc : signal is "true";
   signal pc_future, pc_prev      : std_logic_vector(31 downto 0);
   signal pc_p, pc_p_p,
          pc_p_p_p, pc_p_p_p_p       : std_logic_vector(31 downto 0);
   
   signal alu_function_p    : alu_function_type;
   signal shift_function_p  : shift_function_type;
   signal mult_function_p   : mult_function_type;
   signal branch_function_p : branch_function_type;

   signal alu_function   : alu_function_type;
   signal shift_function : shift_function_type;
   signal mult_function  : mult_function_type;
   signal branch_function: branch_function_type;
   signal take_branch    : std_logic;
   signal take_branch_np : std_logic;
   signal a_source       : a_source_type;
   signal a_source_p     : a_source_type;      
   signal b_source       : b_source_type;
   signal b_source_p     : b_source_type;
   signal c_source       : c_source_type;
   signal c_source_p     : c_source_type;
   signal c_source_p_p   : c_source_type;
   signal c_source_p_p_p : c_source_type;
   signal pc_source      : pc_source_type;
   signal pc_source_p    : pc_source_type;
   signal pc_source_p_p  : pc_source_type;
   signal pc_source_p_p_p: pc_source_type;
   signal mem_source     : mem_source_type;
   signal mem_source_p   : mem_source_type;
   signal mem_source_p_p : mem_source_type;
   signal mem_source_p_p_p : mem_source_type;
   signal pause_mult     : std_logic;
   signal pause_memory   : std_logic;
   signal i_pause_memory : std_logic;
   signal i_mem_read_v   : std_logic := '1';
   signal pause          : std_logic;
   signal pause_hazard   : std_logic;
   signal hazard         : std_logic;   
   signal nullify_op     : std_logic;
   signal intr_enable    : std_logic;
   signal intr_signal    : std_logic;
   signal forward_a      : std_logic_vector(1 downto 0);
   signal forward_b      : std_logic_vector(1 downto 0);
   signal forward_c      : std_logic_vector(1 downto 0);
   signal forward_d      : std_logic_vector(1 downto 0); -- exec stage
   signal forward_d_p    : std_logic_vector(1 downto 0); -- mem stage 
   signal forward_branch : std_logic;
   signal opcode_p       : std_logic_vector(31 downto 0);
   signal flush          : std_logic;
   signal reg_source_p   : std_logic_vector(31 downto 0);   
   signal reg_target_p   : std_logic_vector(31 downto 0);
   signal reg_target_p_p : std_logic_vector(31 downto 0);
   signal ldr_instr      : std_logic; --dec stage
   signal ldr_instr_p    : std_logic; --ex stage
   signal ldr_instr_p_p  : std_logic; --mem stage
   signal ldr_instr_p_p_p: std_logic; --wb stage
   signal jump_address_reg: std_logic_vector(31 downto 0);
   signal jump_branch_addr: std_logic_vector(31 downto 0);
   signal stall          : std_logic;
   signal stall_c        : std_logic;
   signal stall_if       : std_logic;
   signal stall_if_p     : std_logic;
   signal stall_id       : std_logic;
   signal stall_if_id    : std_logic;
   signal i_mem_enable_s : std_logic;
   signal i_mem_enable_s1: std_logic;

--   signal mem_byte_sel   : std_logic_vector(3 downto 0);
--   signal mem_write      : std_logic;
begin  --architecture
  --stall <= '0';
   stall <= pause_memory;
   pause <= pause_mult or i_pause_memory;
   --pause <= pause_mult or i_pause_memory or pause_memory or pause_hazard;
   --nulify_op = pc_source==from_lbranch && take_branch=='0'
   nullify_op <= pc_source_p(1) and pc_source_p(0) and not take_branch_np;
   c_bus_ex <= c_alu or c_shift or c_mult; -- execute stage (exec stage)
   c_bus_mem <= c_alu_p or c_shift_p or c_mult_p; -- memory stage (mem stage)
   c_bus <= c_alu_p_p or c_shift_p_p or c_mult_p_p; -- wb stage (write back stage)
   intr_signal <= (intr_in and intr_enable) and 
                  (not pc_source_p_p(0) and not pc_source_p_p(1));  --from_inc4
   i_mem_sel <= "0000";

    --process(c_source, c_alu, a_bus)
    --begin
    --    case c_source is 
    --    when c_from_pc => 
    --        jump_address_reg <= c_alu; 
    --    when c_from_null => 
    --        jump_address_reg <= a_bus;
    --    when others => 
    --        jump_address_reg <= (others => '0');
    --    end case;
    --end process;

    --process(branch_function, jump_address_reg, pc_new)
    --begin
    --    case branch_function is 
    --    when branch_yes => 
    --        jump_branch_addr <= jump_address_reg; 
    --    when others => 
    --        jump_branch_addr <= pc_new;
    --    end case;
    --end process;
    
    pc_new <= c_bus_ex;
    --i_mem_enable <= i_mem_enable_s ;
   u1: pc_next PORT MAP (
        clk          => clk,
        reset_in     => reset_in,
        stall        => stall_if,
        take_branch  => take_branch_np, -- take_branch_np or take_branch
        pause_in     => pause,
        --pc_reg       => pc_p(31 downto 2),
        pc_new       => pc_new(31 downto 2), -- pc_new(31 downto 2) or c_alu
        opcode25_0   => opcode(25 downto 0),
        i_mem_enable => i_mem_enable_s,
        pc_source    => pc_source_p, -- pc_source or pc_source_p
        pc_future    => pc_future,
        pc_prev      => pc_prev,
        pc_out       => pc);

    --opcode <= i_mem_data_r;
    process(stall_if, stall_id, i_mem_data_r)
    begin
        if stall_if = '1' or (stall_id = '1' and take_branch = '1') then 
            opcode <= (others => '0');
        else
            opcode <= i_mem_data_r;
        end if;
    end process;
    i_mem_address <= pc;
    i_pause_memory <= '0';
    i_mem_enable <= not (stall_c and not stall_if_p);-- xor reset_in; -- xor stall_id);
    
   u2b : pipeline_IF_ID PORT MAP(
        clk          => clk,
        reset        => reset_in,
        stall        => stall_if,
        take_branch  => take_branch_np,
        pc_source    => pc_source_p,
        opcode       => opcode,
        opcode_out   => opcode_p,
        pc           => pc, 
        pc_out       => pc_p
    );
   
   
   stall_if <= stall_c ;
   
   u3: control PORT MAP (
        opcode        => opcode_p,
        intr_signal   => intr_signal,
        stall         => stall_c, -- stall instruction fetch
        rs_index      => rs_index,
        rt_index      => rt_index,
        rd_index      => rd_index,
        imm_out       => imm,
        alu_func      => alu_function,
        shift_func    => shift_function,
        mult_func     => mult_function,
        branch_func   => branch_function,
        a_source_out  => a_source,
        b_source_out  => b_source,
        c_source_out  => c_source,
        pc_source_out => pc_source,
        ldr_instr     => ldr_instr,
        mem_source_out=> mem_source);

   --u3b: hazard_detection_unit port map(
   -- rs_index_dec  => rs_index,
   -- rt_index_dec  => rt_index,
   -- rt_index_ex   => rt_index_p,
   -- c_source_ex   => c_source_p,
   -- pc_pause      => pause_hazard,    
   -- hazard        => hazard );

   u4: reg_bank port map (
        clk            => clk,
        reset          => reset_in, 
        pause          => pause, 
        rs_index       => rs_index,
        rt_index       => rt_index,
        rd_index       => rd_index_p_p_p,
        reg_source_out => reg_source,
        reg_target_out => reg_target,
        reg_dest_new   => reg_dest,
        intr_enable    => intr_enable);

   u5: bus_mux port map (
        imm_in       => imm,
        reg_source   => reg_source,
        forward      => forward_c,
        forward_branch=> forward_branch,
        
        a_mux        => a_source,
        a_out        => a_bus,

        reg_target   => reg_target,
        b_mux        => b_source,
        b_out        => b_bus,

        c_alu        => c_alu, -- from exec stage 
        c_bus        => c_bus,
        c_memory     => c_memory,
        c_pc         => pc_p_p_p_p,
        c_pc_np      => pc_p,               --dec
        c_mux        => c_source_p_p_p,
        c_imm_in     => imm_p_p_p,
        reg_dest_out => reg_dest,

        pc_new       => open -- pc_new
        );
    
    stall_id <= stall_if_p;
    
   u5b: pipeline_ID_IE port map(
        clk            => clk, 
        reset          => reset_in,
        stall          => stall_id, -- stop pipeline
        
        stall_if       => stall_if,
        stall_if_out   => stall_if_p,
        
        a_out_in       => a_bus,
        a_out          => a_bus_p,
       
        b_out_in       => b_bus,
        b_out          => b_bus_p, 

        a_source       => a_source, 
        a_source_out   => a_source_p, 

        b_source       => b_source, 
        b_source_out   => b_source_p, 

        c_source       => c_source,
        c_source_out   => c_source_p,
        
        c_pc           => pc_p,
        c_pc_out       => pc_p_p,

        rs_index       => rs_index,
        rs_index_out   => rs_index_p,

        rt_index       => rt_index,
        rt_index_out   => rt_index_p,
        
        reg_source     => reg_source,
        reg_source_out => reg_source_p,

        rd_index       => rd_index,
        rd_index_out   => rd_index_p,

        reg_target     => reg_target,
        reg_target_out => reg_target_p,

        imm_in         => imm,
        imm_out        => imm_p,

        take_branch_in => '0', 
        take_branch    => open, 

        ldr_instr      => ldr_instr, 
        ldr_instr_out  => ldr_instr_p,

        alu_func_i     => alu_function, 
        shift_func_i   => shift_function, 
        mult_func_i    => mult_function,
        branch_func_i  => branch_function, 
        pc_source      => pc_source, 
        mem_source     => mem_source,

        alu_func       => alu_function_p, 
        shift_func     => shift_function_p, 
        mult_func      => mult_function_p,
        branch_func    => branch_function_p, 
        pc_source_out  => pc_source_p, 
        mem_source_out => mem_source_p );   

   u6a: forward_unit port map(
        a_in         => a_bus_p,
        b_in         => b_bus_p,
        alu_p        => c_bus_mem,
        alu_p_p      => c_bus,
        data_read_m  => mem_data_r,
        reg_source   => reg_source_p,
        reg_target   => reg_target_p,
        branch_func  => branch_function_p,
        rs_index_dec => rs_index, 
        rt_index_dec => rt_index,
        rs_index     => rs_index_p,
        rt_index     => rt_index_p,
        rd_index     => rd_index_p,
        rd_index_dec => rd_index,
        rd_index_mem => rd_index_p_p,
        rd_index_wb  => rd_index_p_p_p,
        rt_index_mem => rt_index_p_p,
        rt_index_wb  => rt_index_p_p_p,
        ldr_instr    => ldr_instr_p_p_p,
        a_source     => a_source_p,
        b_source     => b_source_p,
        mem_source_x => mem_source_p,
        mem_source_m => mem_source_p_p,
        mem_source_w => mem_source_p_p_p,
        c_source_mem => c_source_p_p,
        c_source_wb  => c_source_p_p_p,
        forward_d => forward_d,
        forward_branch => forward_branch,
        take_branch  => take_branch_np,
        a_out        => a_bus_f,
        b_out        => b_bus_f
        );
    
    u6: alu port map (
        a_in         => a_bus_f,
        b_in         => b_bus_f,

        alu_function => alu_function_p,
        c_alu        => c_alu );

   u7: shifter port map (
        value        => b_bus_f,
        shift_amount => a_bus_f(4 downto 0),
        shift_func   => shift_function_p,
        c_shift      => c_shift);

   u8: mult port map (
        clk       => clk,
        a         => a_bus_f,
        b         => b_bus_f,
        mult_func => mult_function_p,
        c_mult    => c_mult,
        pause_out => pause_mult);

   u8b: pipeline_IE_IM port map(
        clk              => clk,
        reset            => reset_in,
        stall            => stall,
        forward_d        => forward_d,
        hazard           => hazard, 
        c_alu_i          => c_alu,
        c_shift_i        => c_shift,
        c_mult_i         =>  c_mult, 
        c_source         => c_source_p,
        pc_source        => pc_source_p,
        c_pc             => pc_p_p,
        imm_in           => imm_p,
        mem_source_out_i => mem_source_p,
        rt_index         => rt_index_p,
        rd_index         => rd_index_p,
        reg_target     => reg_target_p,
        reg_target_out => reg_target_p_p,

        ldr_instr      => ldr_instr_p, 
        ldr_instr_out  => ldr_instr_p_p,

        c_alu            => c_alu_p,
        c_shift          => c_shift_p, 
        c_mult           => c_mult_p, 
        c_source_out     => c_source_p_p,
        pc_source_out    => pc_source_p_p,
        c_pc_out         => pc_p_p_p,
        imm_out          => imm_p_p,
        rt_index_out     => rt_index_p_p,
        rd_index_out     => rd_index_p_p,
        forward_d_out    => forward_d_p,
        mem_source_out   => mem_source_p_p);

   u9: data_mem_ctrl PORT MAP (
       clk          => clk,
       reset_in     => reset_in,
       forward_d    => forward_d_p,
       c_bus_mem    => c_bus_mem,
       c_alu_wb     => c_bus,
       
       address_data => c_alu_p,
       mem_source   => mem_source_p_p,
       data_write   => reg_target_p_p,
       data_read    => c_memory,
       pause_out    => pause_memory,
       
       mem_address  => mem_address,
       mem_data_w   => mem_data_w,
       mem_data_r_p => c_memory_p,
       mem_data_r   => mem_data_r,
       mem_byte_sel => mem_sel,
       mem_write    => mem_write,
       mem_read     => mem_read,
       mem_pause    => mem_pause);

   u10: pipeline_IM_IWB port map(
        clk           => clk,
        reset         => reset_in,
        stall         => stall,
        hazard        => hazard, 
        c_alu_i       => c_alu_p,
        c_shift_i     => c_shift_p,
        c_mult_i      =>  c_mult_p,
        c_source      => c_source_p_p, 
        pc_source     => pc_source_p_p,
        c_pc          => pc_p_p_p,
        imm_in        => imm_p_p,
        mem_data      => c_memory,
        rt_index      => rt_index_p_p, 
        rd_index      => rd_index_p_p, 
        mem_source_out_i=> mem_source_p_p,
        ldr_instr      => ldr_instr_p_p, 
        ldr_instr_out  => ldr_instr_p_p_p,

        c_alu         => c_alu_p_p,
        c_shift       => c_shift_p_p, 
        c_mult        => c_mult_p_p, 
        c_source_out  => c_source_p_p_p, 
        pc_source_out => pc_source_p_p_p, 
        c_pc_out      => pc_p_p_p_p,
        imm_out       => imm_p_p_p,
        rt_index_out  => rt_index_p_p_p,
        rd_index_out  => rd_index_p_p_p,
        mem_source_out=> mem_source_p_p_p,
        mem_data_out_p=> c_memory_p,
        mem_data_out  => open );

   u11: io_controller port map(
        mem_address_in      => c_alu_p,
        data_mem_cs         => data_mem_cs,
        annotations_mem_cs  => annotations_mem_cs,
        annotations_bb_cs   => annotations_bb_cs,
        trace_mem_cs        => trace_mem_cs,
        kernel2monitor_cs   => kernel2monitor_cs,    
        monitor2kernel_cs   => monitor2kernel_cs
    );

   --t_pc <= pc;
   --t_opcode <= opcode;
   --t_r_dest <= reg_dest;


end; --architecture logic
