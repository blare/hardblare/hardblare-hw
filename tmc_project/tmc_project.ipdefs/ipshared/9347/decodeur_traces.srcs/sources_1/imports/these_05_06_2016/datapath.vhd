library ieee;
use ieee.std_logic_1164.all;
--suse ieee.numeric_std.All;

entity datapath is
generic (
    ctxtid_bits : std_logic_vector(1 downto 0) := "00"
    );
port (
  clk, reset, enable : in std_logic;
  trace_data_in : in std_logic_vector(7 downto 0);
  start_b, start_i, start_w : in std_logic;
  stop_b, stop_i, stop_w : out std_logic;
  pc : out std_logic_vector(31 downto 0);
  out_en, out_ctxt_en, fifo_overflow : out std_logic;
  waypoint_address, context_id : out std_logic_vector(31 downto 0);
  waypoint_address_en : out std_logic
);
end entity;

architecture structural of datapath is
  component decode_bap
  port(
  clk, reset, start_b, enable, enable_i : in std_logic;
  data_in : in std_logic_vector(7 downto 0);
  instruction_address : in std_logic_vector(31 downto 0);
  stop_b, w_en : out std_logic;
    pc : out std_logic_vector(31 downto 0)
  );
  end component ;

  component decode_i_sync
    port(
    clk,reset, start_i, enable : in std_logic;
    data_in : in std_logic_vector(7 downto 0);
    ctxtid_bits : in std_logic_vector(1 downto 0);
    data_out : out std_logic_vector(31 downto 0);
    context_id_out : out std_logic_vector(31 downto 0);
    stop_i, out_en, out_ctxt_en, fifo_overflow : out std_logic
    );
  end component;

--  component decode_waypoint
--      port(
--      clk, reset, start_w, enable  : in std_logic;
--      data_in : in std_logic_vector(7 downto 0);
--      waypoint_address : out std_logic_vector(31 downto 0);
--      stop_w, out_en : out std_logic
--      );
--  end component;

signal instruction_address_s : std_logic_vector(31 downto 0);
signal en_s_instruction, en_s_mod_address : std_logic;

begin
  u_decode_i_sync : decode_i_sync port map
  (
  clk => clk,
  reset => reset,
  start_i => start_i,
  enable => enable,
  data_in => trace_data_in,
  ctxtid_bits => ctxtid_bits,
  data_out => instruction_address_s,
  context_id_out => context_id,
  stop_i => stop_i,
  out_en => en_s_instruction,
  out_ctxt_en => out_ctxt_en,
  fifo_overflow => fifo_overflow
  );

  u_decode_bap : decode_bap port map
  (
  clk => clk,
  reset => reset,
  enable => enable,
  start_b => start_b,
  data_in => trace_data_in,
  instruction_address => instruction_address_s,
  stop_b => en_s_mod_address,
  w_en => out_en,
  enable_i => en_s_instruction,
  pc => pc
  );

--  u_decode_waypoint : decode_waypoint port map
--  (
--  clk => clk,
--  reset => reset,
--  enable => enable,
--  start_w => start_w,
--  data_in => trace_data_in,
--  waypoint_address => waypoint_address,
--  stop_w => stop_w,
--  out_en => waypoint_address_en
--  );

  stop_b <= en_s_mod_address;

end architecture;
