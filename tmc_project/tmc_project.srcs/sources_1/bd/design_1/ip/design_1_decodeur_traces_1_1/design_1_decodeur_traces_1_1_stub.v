// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
// Date        : Fri Jun  4 10:30:44 2021
// Host        : HardBlare-Workstation running 64-bit Ubuntu 16.04.6 LTS
// Command     : write_verilog -force -mode synth_stub
//               /home/mounir/HardBlare/hardblare-hw/tmc_project/tmc_project.srcs/sources_1/bd/design_1/ip/design_1_decodeur_traces_1_1/design_1_decodeur_traces_1_1_stub.v
// Design      : design_1_decodeur_traces_1_1
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "decodeur_traces,Vivado 2018.2" *)
module design_1_decodeur_traces_1_1(clk, reset, enable, trace_data, pc, w_en, w_ctxt_en, 
  fifo_overflow, waypoint_address, context_id, waypoint_address_en)
/* synthesis syn_black_box black_box_pad_pin="clk,reset,enable,trace_data[7:0],pc[31:0],w_en,w_ctxt_en,fifo_overflow,waypoint_address[31:0],context_id[31:0],waypoint_address_en" */;
  input clk;
  input reset;
  input enable;
  input [7:0]trace_data;
  output [31:0]pc;
  output w_en;
  output w_ctxt_en;
  output fifo_overflow;
  output [31:0]waypoint_address;
  output [31:0]context_id;
  output waypoint_address_en;
endmodule
