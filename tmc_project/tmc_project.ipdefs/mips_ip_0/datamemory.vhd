----------
--
-- AUTHOR               : M.Abdul WAHAB
-- DATE                 : 23/09/2016
-- Description          : This is an example of memory element
--                        Memory element of 256 x 8 (configurable via DATA_MEM_ADDRESS_WIDTH and data_mem_data_width)
--                        Block RAM is inferred through behavioral description. ('no change' mode)
--                        Internal output register is enabled.
--                        See "White Paper : HDL Coding Practices to Accelerate Design Performance" for
--                        more information
----------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
use work.dependencies_pack.all;

entity datamemory is  
  port (
    clk    : in  std_logic;
    mem_byte_sel : in std_logic_vector(3 downto 0);
    en     : in  std_logic;
    we_i   : in  std_logic;    
    addr_i : in  std_logic_vector(data_mem_address_width-1 downto 0);
    din_i  : in  std_logic_vector(data_mem_data_width-1 downto 0);
    dout_o : out std_logic_vector(data_mem_data_width-1 downto 0)
    );
end entity;

architecture archi_behav of datamemory is
  type mem_type is array(0 to (2**(data_mem_address_width))-1) of std_logic_vector(data_mem_data_width-1 downto 0);
  signal mem                    : mem_type := (others => (others=> '0'));
-- disable conflict avoidance logic
-- simultaneous read and write on the same memory cell
  --attribute syn_ramstyle of mem : signal is "no_rw_check";

  signal data, dout_r : std_logic_vector(data_mem_data_width-1 downto 0);
begin

  process(mem_byte_sel, din_i)
  begin
    if mem_byte_sel(0) = '1' then
       data(7 downto 0) <= din_i(7 downto 0);
    end if;
    if mem_byte_sel(1) = '1' then
       data(15 downto 8) <= din_i(15 downto 8);
    end if;
    if mem_byte_sel(2) = '1' then
       data(23 downto 16) <= din_i(23 downto 16);
    end if;
    if mem_byte_sel(3) = '1' then
       data(31 downto 24) <= din_i(31 downto 24);
    end if;
  end process; 


-- 'No Change' Mode
process(clk)
variable index: natural; 
begin
  index := 0;
  if (rising_edge(clk)) then
    if en = '1' then 
      if we_i = '1' then
        index := to_integer(unsigned(addr_i));        
        mem(index) <= data;        
      else
        dout_r <= mem(to_integer(unsigned(addr_i)));
      end if;
    end if;
  end if;
end process;

-- process(clk)
-- begin
--   if rising_edge(clk) then
--     dout_o <= dout_r;
--   end if;
-- end process;
  
  dout_o <= dout_r;
    
end architecture;
