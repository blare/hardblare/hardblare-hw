# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  set Page_0 [ipgui::add_page $IPINST -name "Page 0"]
  ipgui::add_param $IPINST -name "ethernet" -parent ${Page_0}
  ipgui::add_param $IPINST -name "log_file" -parent ${Page_0}
  ipgui::add_param $IPINST -name "memory_type" -parent ${Page_0}
  ipgui::add_param $IPINST -name "use_cache" -parent ${Page_0}


}

proc update_PARAM_VALUE.ethernet { PARAM_VALUE.ethernet } {
	# Procedure called to update ethernet when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.ethernet { PARAM_VALUE.ethernet } {
	# Procedure called to validate ethernet
	return true
}

proc update_PARAM_VALUE.log_file { PARAM_VALUE.log_file } {
	# Procedure called to update log_file when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.log_file { PARAM_VALUE.log_file } {
	# Procedure called to validate log_file
	return true
}

proc update_PARAM_VALUE.memory_type { PARAM_VALUE.memory_type } {
	# Procedure called to update memory_type when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.memory_type { PARAM_VALUE.memory_type } {
	# Procedure called to validate memory_type
	return true
}

proc update_PARAM_VALUE.use_cache { PARAM_VALUE.use_cache } {
	# Procedure called to update use_cache when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.use_cache { PARAM_VALUE.use_cache } {
	# Procedure called to validate use_cache
	return true
}


proc update_MODELPARAM_VALUE.memory_type { MODELPARAM_VALUE.memory_type PARAM_VALUE.memory_type } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.memory_type}] ${MODELPARAM_VALUE.memory_type}
}

proc update_MODELPARAM_VALUE.log_file { MODELPARAM_VALUE.log_file PARAM_VALUE.log_file } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.log_file}] ${MODELPARAM_VALUE.log_file}
}

proc update_MODELPARAM_VALUE.ethernet { MODELPARAM_VALUE.ethernet PARAM_VALUE.ethernet } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.ethernet}] ${MODELPARAM_VALUE.ethernet}
}

proc update_MODELPARAM_VALUE.use_cache { MODELPARAM_VALUE.use_cache PARAM_VALUE.use_cache } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.use_cache}] ${MODELPARAM_VALUE.use_cache}
}

