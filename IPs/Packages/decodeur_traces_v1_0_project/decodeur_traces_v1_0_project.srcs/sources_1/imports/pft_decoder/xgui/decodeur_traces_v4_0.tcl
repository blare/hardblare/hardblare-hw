# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  ipgui::add_page $IPINST -name "Page 0"

  ipgui::add_param $IPINST -name "ctxtid_bits"

}

proc update_PARAM_VALUE.ctxtid_bits { PARAM_VALUE.ctxtid_bits } {
	# Procedure called to update ctxtid_bits when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.ctxtid_bits { PARAM_VALUE.ctxtid_bits } {
	# Procedure called to validate ctxtid_bits
	return true
}


proc update_MODELPARAM_VALUE.ctxtid_bits { MODELPARAM_VALUE.ctxtid_bits PARAM_VALUE.ctxtid_bits } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.ctxtid_bits}] ${MODELPARAM_VALUE.ctxtid_bits}
}

