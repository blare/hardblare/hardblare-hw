//#include <stdint.h>
//#include <stdio.h>


typedef struct _mem_tag_entry
{
	uint32_t address;
	uint32_t tag;
} mem_tag_entry;


#define NUMBER_REGISTERS     32
extern uint32_t TRF[NUMBER_REGISTERS];
#define MEM_TAG_SIZE	 999
extern mem_tag_entry mem_tag[MEM_TAG_SIZE];
extern uint32_t offset_mem_tag;

#define LR_REGISTER 0x1D
#define SP_REGISTER 0x1E

uint32_t create_mem_entry_with_address(uint32_t address);
uint32_t search_value_of_address(uint32_t address);
uint32_t search_and_affect_value_of_address(uint32_t address, uint32_t tag);
uint32_t search_tag_from_lsm_hook(uint32_t address, uint32_t size);
void search_and_affect_tag_from_lsm_hook(uint32_t address, uint32_t size, uint32_t tag);

void dumpMem();
void dumpTRF();

