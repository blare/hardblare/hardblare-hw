library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity BRAM_to_FIFO is
    port(    
    -- from_BRAM
        from_bram_address: in std_logic_vector (31 downto 0);
        from_bram_clk: in std_logic;
        from_bram_data_write: in std_logic_vector (31 downto 0);
        from_bram_enable: in std_logic;
        from_bram_reset: in std_logic;
        from_bram_bytes_selection : in std_logic_vector (3 downto 0);
        to_bram_data_read : out std_logic_vector (31 downto 0);
        
    -- to_FIFO        
        to_fifo_data_write: out std_logic_vector (31 downto 0);
        to_fifo_write_enable: out std_logic;
        to_fifo_read_enable: out std_logic;
        to_fifo_clk: out std_logic;
        to_fifo_reset: out std_logic;
        from_fifo_data_read : in std_logic_vector (31 downto 0)
        
        );

end BRAM_to_FIFO;

architecture Behavioral of BRAM_to_FIFO is
    
    signal address: std_logic_vector (31 downto 0);
    signal clk: std_logic;
    signal data_write: std_logic_vector (31 downto 0);
    signal enable: std_logic;
    signal reset: std_logic;
    signal bytes_selection : std_logic_vector (3 downto 0);
     
    
begin

    -- Connection to FIFO
    to_fifo_data_write <= from_bram_data_write;
    to_fifo_write_enable <= '1' when (from_bram_bytes_selection = "1111" and from_bram_enable = '1') else '0';
    to_fifo_read_enable <= '1' when (from_bram_bytes_selection="0000" and from_bram_enable = '1') else '0';
    to_fifo_clk <= from_bram_clk;
    to_fifo_reset <= from_bram_reset;
    to_bram_data_read <= from_fifo_data_read;  

end Behavioral;
