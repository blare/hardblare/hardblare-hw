library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.All;

entity decodeur_traces is
    generic (ctxtid_bits : std_logic_vector(1 downto 0) := "11");
    port(
    clk, reset, enable : in std_logic;
    trace_data : in std_logic_vector(7 downto 0);
    pc : out std_logic_vector(31 downto 0);
    w_en, w_ctxt_en, fifo_overflow : out std_logic;
    waypoint_address, context_id : out std_logic_vector(31 downto 0);
    waypoint_address_en : out std_logic
    );
end entity;

architecture structural of decodeur_traces is
    
    component datapath
    generic (ctxtid_bits : std_logic_vector(1 downto 0) := "11");
    port (
    clk, reset, enable : in std_logic;
    trace_data_in : in std_logic_vector(7 downto 0);
    start_b, start_i, start_w : in std_logic;
    stop_b, stop_i, stop_w : out std_logic;
    pc : out std_logic_vector(31 downto 0);
    out_en, out_ctxt_en, fifo_overflow : out std_logic;
    waypoint_address, context_id : out std_logic_vector(31 downto 0);
    waypoint_address_en : out std_logic
    );
end component;

component pft_decoder_v2
port(
clk                 : in std_logic;
reset               : in std_logic;
data_in             : in std_logic_vector(7 downto 0);
data_out             : out std_logic_vector(7 downto 0);
enable 				: in std_logic;
start_i_sync        : out std_logic;
stop_i_sync         : in std_logic;
start_bap           : out std_logic;
stop_bap            : in std_logic;
start_waypoint      : out std_logic;
stop_waypoint       : in std_logic
);
end component;

signal start_b, start_i, start_w : std_logic;
signal stop_b, stop_i, stop_w : std_logic;
signal data_in_reg, trace_data_s : std_logic_vector(7 downto 0);
signal enable_reg : std_logic := '0';
signal enable_reg_reg : std_logic := '0';
signal trace_start, trace_start_reg: std_logic;
signal count : std_logic_vector(0 downto 0) := "0";
signal pc_s, context_id_s : std_logic_vector(31 downto 0);
signal out_en_s, w_ctxt_en_s : std_logic;
begin
    
    process(clk)
    begin 
        if rising_edge(clk) then
            if reset = '1' then 
                enable_reg <= '0' ;
                trace_start_reg <= '0';
            elsif (count = "1") then 
                enable_reg <= enable ;
                trace_start_reg <= trace_start;
            end if;
        end if;
    end process;
    
    process(clk)
    begin 
        if rising_edge(clk) then
            if reset = '1' then 
                enable_reg_reg <= '0' ;
            else 
                enable_reg_reg <= enable_reg ;
            end if;
        end if;
    end process;
    
    process(trace_data, count)
    begin 
        if (trace_data = x"80" and count = "0") then 
           trace_start <= '1'; 
        else  
            trace_start <= '0';
        end if; 
     end process;
     
     process(clk)
     begin
        if rising_edge(clk) then
            if (trace_start = '1') then 
                count <= std_logic_vector(unsigned(count) + 1);
            end if;
        end if;
     end process;
    
    process(clk)
    begin 
        if rising_edge(clk) then 
            if reset = '1' then 
                data_in_reg <= (others => '0');
            else
                data_in_reg <= trace_data;
            end if;
        end if; 
     end process;
     
    -- register outputs
    process(clk)
     begin 
         if rising_edge(clk) then 
             if reset = '1' then 
                 pc <= (others => '0');
                 w_en <=  '0';
                 w_ctxt_en <= '0';
                 context_id <= (others => '0');
             else
                 pc <= pc_s;
                 w_en <= out_en_s;
                 w_ctxt_en <= w_ctxt_en_s;
                 context_id <= context_id_s; 
             end if;
         end if; 
      end process;
    
    fsm : pft_decoder_v2 port map   
    (
    clk                 => clk,
    reset               => reset,
    data_in             => data_in_reg,
    data_out            => trace_data_s,
    enable              => enable_reg_reg,
    start_i_sync        => start_i,
    stop_i_sync         => stop_i,
    start_bap           => start_b,
    stop_bap            => stop_b,
    start_waypoint      => start_w,
    stop_waypoint       => stop_w
    );

    chemin : datapath 
    generic map(ctxtid_bits => ctxtid_bits)
    port map
    (
    clk => clk,
    reset => reset,
    trace_data_in => data_in_reg,
    enable => enable_reg_reg,
    start_b => start_b,
    start_i => start_i,
    stop_b => stop_b,
    stop_i => stop_i,
    start_w => start_w,
    stop_w => stop_w,
    pc =>  pc_s,
    out_en => out_en_s,
    out_ctxt_en => w_ctxt_en_s,
    fifo_overflow => fifo_overflow,
    waypoint_address => waypoint_address,
    context_id => context_id_s,
    waypoint_address_en => waypoint_address_en
    );
end architecture;
