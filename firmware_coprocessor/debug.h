#ifdef DEBUG_SIMULATION
   #define IF_SIMULATION(X) X
#else
   #define IF_SIMULATION(X)
#endif


#ifdef DEBUG_HARDWARE
  #define IF_HARDWARE_DEBUG(X) X
#else
  #define IF_HARDWARE_DEBUG(X)
#endif


#if !(defined DEBUG_SIMULATION)
  #define IF_NOT_SIMULATION(X) X
#else
  #define IF_NOT_SIMULATION(X)
#endif


inline void init_simulation_memories(void);
inline void debug_print_basic_block_table_size(uint32_t basic_block_table_size);
inline void debug_print_address_of_basic_block(uint32_t basic_block_address);
inline void debug_print_trace_value(uint32_t trace_value);
inline void debug_print_number_of_annotations(uint32_t number_of_annotations);
inline void debug_print_annotation_value(uint32_t annotation_value);

inline void debug_read_instrumentation(uint32_t value);
inline void debug_print_TRF_entry(uint32_t reg, uint32_t value);
inline void debug_decode_init_format(void);
inline void debug_decode_tr_format(void);

inline void debug_case_INSTR(void);
inline void debug_case_LW(void);
inline void debug_case_SW(void);
inline void debug_case_LCR(void);
inline void debug_case_TLB(void);
inline void debug_case_TAG_RRI(void);
inline void debug_case_LUI(void);
inline void debug_case_TAG_Reg_Imm();
inline void debug_print_TRF_lui(uint32_t destReg, uint32_t imm_raw, uint32_t  srcReg, uint32_t final_value );

inline void debug_case_TAG_reg_reg(void);
  inline void debug_case_TAG_mem_reg(void);
inline void debug_print_TRF_op_imm(uint32_t destReg, uint32_t srcReg, const char* op,uint32_t  imm, uint32_t tagValue);
inline void debug_print_TRF_imm(uint32_t destReg, uint32_t  imm_raw);
inline void debug_print_TRF_reg(uint32_t destReg, uint32_t  src_reg);
inline void debug_print_mem_reg_offset_reg(uint32_t destReg,  uint32_t valueDestReg,uint32_t valueSrcReg, char *op, uint32_t imm, uint32_t address, uint32_t  srcReg);
inline void debug_print_TRF_TRR(uint32_t  destReg,
                                uint32_t  srcReg1,
                                uint32_t  srcReg2,
                                uint32_t  valueSrcReg1,
                                uint32_t  valueSrcReg2,
                                uint32_t  value,
                                char * ARMopcode);

inline void debug_print_arm_opcode(uint32_t a, char* op , uint32_t b);

inline void debug_waiting_instrumentation_value(void);
inline void debug_case_TAG_arith_log(void);
inline void debug_case_TAG_check_reg(void);
inline void debug_print_dump_debug_mem(void);

inline void debug_ti_format(void);
inline void debug_ti_Tag_mem_tr(void);
inline void debug_Tag_mem_tr(uint32_t destReg, uint32_t srcReg, uint32_t addr, uint32_t  imm, const char *op, uint32_t Tag_value);
inline void debug_ti_Tag_tr_mem(void);
inline void debug_Tag_tr_mem(uint32_t destReg, uint32_t srcReg, uint32_t addr, uint32_t  imm, uint32_t valueTag, const char *op);

inline void debug_ti_Tag_mem_tr_2();
inline void debug_ti_Tag_tr_mem_2();
inline void debug_ti_Tag_instrumentation_tr();
inline void debug_ti_Tag_tr_instrumentation();
inline void debug_ti_Tag_Kblare_tr();
inline void debug_ti_Tag_tr_Kblare();
inline void debug_ti_Tag_check_mem();
inline void debug_raise_IRQ();
inline void debug_print_Tag_check_mem(uint32_t destReg, uint32_t valDestReg, uint32_t tag_val_addr, uint32_t srcReg, uint32_t valueOfTag, char *result);
inline void debug_Tag_tr_mem_2(uint32_t destReg, uint32_t srcReg, uint32_t addr, uint32_t  imm, const char *op, uint32_t Tag_value);

inline void debug_Tag_mem_tr(uint32_t destReg, uint32_t srcReg, uint32_t addr, uint32_t  imm, const char *op, uint32_t Tag_value);


inline void debug_print_get_kernel_to_monitor(uint32_t value);
inline void debug_print_waiting_kernel_to_monitor();
inline void debug_print_get_kernel_to_monitor_type(char* type);
inline void debug_print_get_monitor_to_kernel(uint32_t value);
inline void debug_print_waiting_monitor_to_kernel();
inline void debug_print_get_monitor_to_kernel_type(char* type);
inline void debug_print_syscall_protocol_type(char* type);

inline void debug_print_syscall_protocol_newline();

inline void debug_print_search_and_affect_tag_from_lsm_hook();
inline void debug_print_search_tag_from_lsm_hook();
inline void debug_print_search_tag_from_lsm_hook_result_tag(uint32_t value);
inline void debug_print_search_and_affect_value_of_address(uint32_t address, uint32_t value);
inline void debug_print_search_and_affect_value_of_address_not_found(uint32_t address);
inline void debug_print_create_mem_entry_with_address(uint32_t address, uint32_t tag);

inline void debug_print_search_value_of_address(uint32_t address, uint32_t tag);
inline void debug_print_search_value_of_address_not_found(uint32_t address);

inline void debug_print_dump_debug_mem();


