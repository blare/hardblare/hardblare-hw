----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02.04.2020 18:12:28
-- Design Name: 
-- Module Name: PL_to_PS_IRQ - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity PL_to_PS_IRQ is
generic(

    -- Number of IRQ
    ----------------------------------------
    NUM_INPUT_SIGNALS	    : integer	:= 6;
    
    -- Number of input signals
    ----------------------------------------
    FIFO_PTM_IDX	                 : integer	:= 0;
    DISPATCHER_IDX	                 : integer	:= 1;    
    FIFO_INSTRUMENTATION_IDX	     : integer	:= 2;  
    FIFO_TMC_IDX	                 : integer	:= 3;
    FIFO_KERNEL_TO_MONITOR_IDX	     : integer	:= 4;       
    FIFO_MONITOR_TO_KERNEL_IDX	     : integer	:= 5  
  

);
port(

        clk : in std_logic;
        reset : in std_logic;

        --  From AXI BRAM
        --------------------------------------------------------------------------
        from_PS_address: in std_logic_vector (31 downto 0);
        from_PS_clk: in std_logic;
        from_PS_data_in: in std_logic_vector (31 downto 0);
        from_PS_enable: in std_logic;
        from_PS_reset: in std_logic;
        from_PS_bytes_selection : in std_logic_vector (3 downto 0);
        from_PS_data_out: out std_logic_vector (31 downto 0);        
        
        --
        --  FIFO PTM Traces (READ Mode)
        --------------------------------------------------------------------------    
        fifo_ptm_empty                           : in std_logic;
        fifo_ptm_almost_empty                    : in std_logic;
        fifo_ptm_full                            : in std_logic;
        fifo_ptm_almost_full                     : in std_logic;
        
        --
        --  FIFO TMC (Write mode)
        --------------------------------------------------------------------------
        fifo_tmc_empty              : in std_logic;
        fifo_tmc_almost_empty       : in std_logic;             
        fifo_tmc_full               : in std_logic;
        fifo_tmc_almost_full        : in std_logic;     

        --
        --  Monitor to Kernel FIFO (Write mode)
        --------------------------------------------------------------------------     
        fifo_monitor_to_kernel_empty            : in std_logic;
        fifo_monitor_to_kernel_almost_empty     : in std_logic;
        fifo_monitor_to_kernel_full             : in std_logic;
        fifo_monitor_to_kernel_almost_full      : in std_logic;      

        --
        --  Kernel to Monitor FIFO (READ Mode)
        -------------------------------------------------------------------------- 
        fifo_kernel_to_monitor_empty                : in std_logic;
        fifo_kernel_to_monitor_almost_empty         : in std_logic;     
        fifo_kernel_to_monitor_full                 : in std_logic;
        fifo_kernel_to_monitor_almost_full          : in std_logic;    

        --
        --  Instrumentation FIFO
        --------------------------------------------------------------------------
        fifo_instrumentation_empty            : in std_logic;
        fifo_instrumentation_almost_empty     : in std_logic;
        fifo_instrumentation_full             : in std_logic; 
        fifo_instrumentation_almost_full      : in std_logic;
        
        --
        --  Dispatcher
        --------------------------------------------------------------------------
        dispatcher_generate_interrupt      : in std_logic;
        
        --
        --  IRQ Generated
        --------------------------------------------------------------------------       
        irq : out std_logic     
        
        
);



end PL_to_PS_IRQ;

architecture Behavioral of PL_to_PS_IRQ is

     -- Active Global Interrupts
	signal reg_global_intr_en	:std_logic := '0';           
	
	-- Set bits correspond to active interrupts
	signal reg_intr_en	     :std_logic_vector(31 downto 0) := x"00000000";             

    -- Ack
    signal reg_intr_ack	 :std_logic_vector(31 downto 0):= x"00000000";          

    -- Bits indicate which interrupts are pending
	signal reg_intr_pending :std_logic_vector(31 downto 0):= x"00000000";  
	
    -- Bits indicate which interrupts are pending
    signal signal_pending :std_logic_vector(31 downto 0):= x"00000000";          
	       
	function or_reduction (vec : in std_logic_vector) return std_logic is           
          variable res_v : std_logic := '0';  -- Null vec vector will also return '1'   
          begin                                                                         
          for i in vec'range loop                                                       
            res_v := res_v or vec(i);                                                   
          end loop;                                                                     
          return res_v;                                                                 
    end function;     
                                                                                                                                          
begin


   -- Update interrupt status
   processclk: process (clk)
    begin
   
       if( rising_edge(clk) ) then
       
             -- Reset
             if (reset = '1') then
               
                    reg_global_intr_en   <= '0';          
                    reg_intr_en          <= (others => '0');  
                    reg_intr_ack         <= (others => '0');
                    reg_intr_pending     <= (others => '0'); 

            else
            
            
                 if (reg_global_intr_en = '1') then 
                 
                        signal_pending(FIFO_PTM_IDX) <= fifo_ptm_full or fifo_ptm_almost_full ;
                        signal_pending(DISPATCHER_IDX) <= dispatcher_generate_interrupt ;
                        signal_pending(FIFO_INSTRUMENTATION_IDX) <= fifo_instrumentation_full or fifo_instrumentation_almost_full ;
                        signal_pending(FIFO_TMC_IDX) <= fifo_tmc_full or fifo_tmc_almost_full ;
                        signal_pending(FIFO_KERNEL_TO_MONITOR_IDX) <= fifo_kernel_to_monitor_full or fifo_kernel_to_monitor_almost_full ;        
                        signal_pending(FIFO_MONITOR_TO_KERNEL_IDX) <= fifo_monitor_to_kernel_full or fifo_monitor_to_kernel_almost_full ; 
                        
                         -- Acknowledge or Pending interrupt  
                         for i in signal_pending'range loop
                            -- ACK
                            if( reg_intr_ack(i) = '1' and reg_intr_pending(i) = '1') then
                                 reg_intr_pending(i) <= '0';
                                 reg_intr_ack(i) <= '0';
                             -- Interrupt    
                             elsif( signal_pending(i) = '1') then            
                                  reg_intr_pending(i) <= '1';
                             end if;
                         end loop; 
                         
                         -- Output interrupt
                         irq <= or_reduction(reg_intr_pending);  
  
                 end if;
                 
                    
                 -- Read registers
                 if( (from_PS_enable = '1') and (from_PS_bytes_selection = "0000") ) then        
                                                
                            case from_PS_address(5 downto 2) is                                                                
                              when "0000" =>                                                               
                                from_PS_data_out <= "0000000000000000000000000000000" & reg_global_intr_en;             
                              when "0001" =>                                                               
                                from_PS_data_out <= reg_intr_en;                                                                               
                              when "0010" =>                                                               
                                from_PS_data_out <= reg_intr_ack;                                        
                              when "0011" =>                                                              
                                from_PS_data_out <= reg_intr_pending;                                    
                              when others =>                                                                
                                from_PS_data_out  <= (others => '0');                                           
                            end case;
                    
                 -- Write registers    
                 elsif( (from_PS_enable = '1') and (from_PS_bytes_selection = "1111") ) then      
                                                     
                            case from_PS_address(5 downto 2) is                                                                
                              when "0000" =>                                                               
                                reg_global_intr_en <= from_PS_data_in(0);             
                              when "0001" =>                                                               
                                reg_intr_en <= from_PS_data_in;                                                                         
                              when "0010" =>                                                               
                                reg_intr_ack <= from_PS_data_in;                                                                      
                              when others =>                                                                                                          
                            end case;    
                            
                 else
                              
                 end if;      
                     
            end if;   
          
     end if;  
              
   
                           
end process;

                     

end Behavioral;
