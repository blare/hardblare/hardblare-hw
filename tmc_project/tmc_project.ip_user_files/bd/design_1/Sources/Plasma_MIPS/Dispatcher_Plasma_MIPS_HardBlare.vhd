---------------------------------------------------------------------
-- TITLE: Plasma (CPU core with memory)
-- AUTHOR: Steve Rhoads (rhoadss@yahoo.com), Mounir NASR ALLAH (mounir.nasrallah@centralesupelec.fr)
-- DATE CREATED: 6/4/02
-- FILENAME: plasma.vhd
-- PROJECT: Plasma CPU core
-- COPYRIGHT: Software placed into the public domain by the author.
--    Software 'as is' without warranty.  Author liable for nothing.
-- DESCRIPTION:
--    This entity combines the CPU core with memory and a UART.
--
-- Memory Map:
--   0x00000000 - 0x0000ffff   Internal RAM (8KB)
--   0x10000000 - 0x100fffff   External RAM (1MB)
--   Access all Misc registers with 32-bit accesses
--   0x20000000  Uart Write (will pause CPU if busy)
--   0x20000000  Uart Read
--   0x20000010  IRQ Mask
--   0x20000020  IRQ Status
--   0x20000030  GPIO0 Out Set bits
--   0x20000040  GPIO0 Out Clear bits
--   0x20000050  GPIOA In
--   0x20000060  Counter
--   0x20000070  Ethernet transmit count
--   IRQ bits:
--      7   GPIO31
--      6  ^GPIO31
--      5   EthernetSendDone
--      4   EthernetReceive
--      3   Counter(18)
--      2  ^Counter(18)
--      1  ^UartWriteBusy
--      0   UartDataAvailable
---------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use work.mlite_pack.all;

entity Dispatcher_Plasma_MIPS_HardBlare is
  generic(memory_type : string := "XILINX_16X"; --"DUAL_PORT_" "ALTERA_LPM";
         log_file    : string := "UNUSED";
         ethernet    : std_logic := '0';
         use_cache   : std_logic := '0');
   port(clk          : in std_logic;
        reset        : in std_logic;
        -- mem_pause_in : in std_logic;
        
       --
       --
       --  Firmware Code BRAM
       --------------------------------------------------------------------------
       bram_firmware_code_en: out std_logic;
       bram_firmware_code_reset: out std_logic;
       bram_firmware_code_clk: out std_logic;
       bram_firmware_code_address: out std_logic_vector(31 downto 0);
       bram_firmware_code_data_write: out std_logic_vector(31 downto 0);
       bram_firmware_code_bytes_selection: out std_logic_vector(3 downto 0);
       
       bram_firmware_code_data_read: in std_logic_vector(31 downto 0);

        
        --
        --  PTM Traces BRAM
        --------------------------------------------------------------------------
        bram_ptm_traces_en: out std_logic;
        bram_ptm_traces_reset: out std_logic;
        bram_ptm_traces_clk: out std_logic;
        bram_ptm_traces_address: out std_logic_vector(31 downto 0);
        bram_ptm_traces_data_write: out std_logic_vector(31 downto 0);
        bram_ptm_traces_bytes_selection: out std_logic_vector(3 downto 0); 
        
        bram_ptm_traces_data_read: in std_logic_vector(31 downto 0);  
        
        
        --
        --  Basic Block Table + Annotations BRAM
        --------------------------------------------------------------------------
        bram_bbt_annotations_en: out std_logic;
        bram_bbt_annotations_reset: out std_logic;
        bram_bbt_annotations_clk: out std_logic;
        bram_bbt_annotations_address: out std_logic_vector(31 downto 0);
        bram_bbt_annotations_data_write: out std_logic_vector(31 downto 0);
        bram_bbt_annotations_bytes_selection: out std_logic_vector(3 downto 0);
        
        bram_bbt_annotations_data_read: in std_logic_vector(31 downto 0); 
        
        
        --
        --  TMC BRAM
        --------------------------------------------------------------------------
        bram_tmc_en: out std_logic;
        bram_tmc_reset: out std_logic;
        bram_tmc_clk: out std_logic;
        bram_tmc_address: out std_logic_vector(31 downto 0);
        bram_tmc_data_write: out std_logic_vector(31 downto 0);
        bram_tmc_bytes_selection: out std_logic_vector(3 downto 0);    
        
        bram_tmc_data_read: in std_logic_vector(31 downto 0);     
        
                
        --
        --  Monitor to Kernel BRAM
        --------------------------------------------------------------------------
        bram_monitor_to_kernel_en: out std_logic;
        bram_monitor_to_kernel_reset: out std_logic;
        bram_monitor_to_kernel_clk: out std_logic;
        bram_monitor_to_kernel_address: out std_logic_vector(31 downto 0);
        bram_monitor_to_kernel_data_write: out std_logic_vector(31 downto 0);
        bram_monitor_to_kernel_bytes_selection: out std_logic_vector(3 downto 0);   
        
        bram_monitor_to_kernel_data_read: in std_logic_vector(31 downto 0); 

        
        --
        --  Kernel to Monitor BRAM
        --------------------------------------------------------------------------
        bram_kernel_to_monitor_en: out std_logic;
        bram_kernel_to_monitor_reset: out std_logic;
        bram_kernel_to_monitor_clk: out std_logic;
        bram_kernel_to_monitor_address: out std_logic_vector(31 downto 0);
        bram_kernel_to_monitor_data_write: out std_logic_vector(31 downto 0);
        bram_kernel_to_monitor_bytes_selection: out std_logic_vector(3 downto 0);
        
        bram_kernel_to_monitor_data_read: in std_logic_vector(31 downto 0)
        
);
        
end; --entity plasma

architecture logic of Dispatcher_Plasma_MIPS_HardBlare is

   signal address_next      : std_logic_vector(31 downto 2);
   signal byte_we_next      : std_logic_vector(3 downto 0);
   
   signal complete_address_next      : std_logic_vector(31 downto 0);
   
   signal cpu_address       : std_logic_vector(31 downto 0);
   
   signal cache_word_read       : std_logic_vector(31 downto 0);
   
   signal selection_of_bram : std_logic_vector(2 downto 0);
   
   signal cleaned_address_next       : std_logic_vector(31 downto 0);
   
   signal cpu_byte_we       : std_logic_vector(3 downto 0);
   signal cpu_data_w        : std_logic_vector(31 downto 0);
   signal cpu_data_r        : std_logic_vector(31 downto 0);
   signal cpu_pause         : std_logic;

   signal data_read_uart    : std_logic_vector(7 downto 0);
   signal write_enable      : std_logic;
   signal eth_pause_in      : std_logic;
   signal eth_pause         : std_logic;
   signal mem_busy          : std_logic;

   signal enable_misc       : std_logic;
   signal enable_uart       : std_logic;
   signal enable_uart_read  : std_logic;
   signal enable_uart_write : std_logic;
   signal enable_eth        : std_logic;

   signal uart_write_busy   : std_logic;
   signal uart_data_avail   : std_logic;
   signal irq_mask_reg      : std_logic_vector(7 downto 0);
   signal irq_status        : std_logic_vector(7 downto 0);
   signal irq               : std_logic;
   signal irq_eth_rec       : std_logic;
   signal irq_eth_send      : std_logic;
   signal counter_reg       : std_logic_vector(31 downto 0);

   signal ram_enable        : std_logic;
   signal ram_byte_we       : std_logic_vector(3 downto 0);
   signal ram_address       : std_logic_vector(31 downto 2);
   signal ram_data_w        : std_logic_vector(31 downto 0);
   signal ram_data_r        : std_logic_vector(31 downto 0);

   signal cache_access      : std_logic;
   signal cache_checking    : std_logic;
   signal cache_miss        : std_logic;
   signal cache_hit         : std_logic;
   
   signal signal_bram_ptm_traces_en  : std_logic;
   signal signal_bram_bbt_annotations_en: std_logic;
   signal signal_bram_annotations_en: std_logic;
   signal signal_bram_monitor_to_kernel_en: std_logic;
   signal signal_bram_kernel_to_monitor_en: std_logic;
   signal signal_bram_firmware_code_en: std_logic;
   
   

begin  --architecture

   write_enable <= '1' when cpu_byte_we /= "0000" else '0';
   
   mem_busy <= '0'; -- mem_pause_in;
   
   cache_hit <= cache_checking and not cache_miss;
      
   cpu_pause <= '0'; --(cpu_address(31 downto 29) != "000") and mem_busy; --or write_enable;
      
   cpu_address(1 downto 0) <= "00";
   
   complete_address_next <= address_next & "00";
   
   -- On determine la BRAM impliquee dans l'acces memoire
   selection_of_bram <= complete_address_next(31 downto 29);
   
   -- On nettoie l'adresse pour avoir une adresse basse (0x00000004)
   cleaned_address_next <= (complete_address_next(31 downto 29) & "00000000000000000000000000000") xor (complete_address_next) ;
   
   
   signal_bram_ptm_traces_en <= '0' ;                        
   signal_bram_bbt_annotations_en <= '0'; 
   signal_bram_annotations_en <= '0' ;  
   signal_bram_monitor_to_kernel_en <= '0' ; 
   signal_bram_kernel_to_monitor_en <= '0'; 
   signal_bram_firmware_code_en <= '0' ; 
   
   
   --------------------------------------------------------
   
   u1_cpu: mlite_cpu 
      PORT MAP (
         clk          => clk,
         reset_in     => reset,
         intr_in      => irq,
 
         address_next => address_next,             --before rising_edge(clk)
         byte_we_next => byte_we_next,

         address      => cpu_address(31 downto 2), --after rising_edge(clk)
         byte_we      => cpu_byte_we,
         data_w       => cpu_data_w,
         data_r       => cpu_data_r,
         mem_pause    => cpu_pause);

   
   
   opt_cache: if use_cache = '0' generate
        cache_access <= '0';
        cache_checking <= '0';
        cache_miss <= '0';
   end generate;


   
      misc_proc: process(clk, 
      reset,
      cpu_address,
      bram_ptm_traces_data_read, 
      bram_bbt_annotations_data_read, 
      bram_tmc_data_read , 
      bram_monitor_to_kernel_data_read , 
      bram_kernel_to_monitor_data_read , 
      bram_firmware_code_data_read,
      signal_bram_ptm_traces_en,
      signal_bram_bbt_annotations_en,
      signal_bram_annotations_en,
      signal_bram_monitor_to_kernel_en,
      signal_bram_kernel_to_monitor_en,
      signal_bram_firmware_code_en,
      cpu_pause, 
      write_enable,
      cache_checking,
      counter_reg, 
      cpu_data_w)
   begin

     case cpu_address(31 downto 29) is 
      -- PTM Traces a l'adresse 0x20000000   
           when "001" => 
               cpu_data_r <= bram_ptm_traces_data_read;
      -- Basic Block Table + Annotations a l'adresse 0x40000000  
           when "010" =>                
               cpu_data_r <= bram_bbt_annotations_data_read;
      -- Annotations a l'adresse 0x60000000
           when "011" => 
               cpu_data_r <= bram_tmc_data_read;
      -- Monitor to Kernel a l'adresse 0x80000000
           when "100" => 
               cpu_data_r <= bram_monitor_to_kernel_data_read;
      -- Kernel to Monitor a l'adresse 0xA0000000
           when "101" => 
               cpu_data_r <= bram_kernel_to_monitor_data_read;
      -- il s'agit de Firmware code
           when "000" => 
               cpu_data_r <= bram_firmware_code_data_read;
        when others =>
               cpu_data_r <= ZERO;
      end case;

     if reset = '1' then
        counter_reg <= ZERO;
     elsif rising_edge(clk) then
        counter_reg <= bv_inc(counter_reg);
     end if;   
   end process;

   ram_proc: process(address_next, cpu_address,
                     byte_we_next, cpu_data_w,
                     complete_address_next,
                     selection_of_bram,
                     cleaned_address_next,
                     
                     bram_ptm_traces_data_read, 
                     bram_bbt_annotations_data_read, 
                     bram_tmc_data_read , 
                     bram_monitor_to_kernel_data_read , 
                     bram_kernel_to_monitor_data_read,
                     bram_firmware_code_data_read
)
   begin
  
    
    case selection_of_bram is
    when "001" =>
        bram_ptm_traces_en <= '1';
        bram_bbt_annotations_en <= '0';
        bram_tmc_en <= '0';
        bram_monitor_to_kernel_en <= '0';
        bram_kernel_to_monitor_en <= '0';
        bram_firmware_code_en <= '0';
        
        bram_ptm_traces_bytes_selection <= byte_we_next; 
        bram_ptm_traces_address <= cleaned_address_next;
        bram_ptm_traces_data_write <= cpu_data_w;
    
    when  "010"  =>
        bram_ptm_traces_en <= '0';
        bram_bbt_annotations_en <= '1';
        bram_tmc_en <= '0';
        bram_monitor_to_kernel_en <= '0';
        bram_kernel_to_monitor_en <= '0';
        bram_firmware_code_en <= '0';
        
        bram_bbt_annotations_bytes_selection <= byte_we_next; 
        bram_bbt_annotations_address <= cleaned_address_next ;
        bram_bbt_annotations_data_write <= cpu_data_w;

    
    when "011"  =>
        
        bram_ptm_traces_en <= '0';
        bram_bbt_annotations_en <= '0';
        bram_tmc_en <= '1';
        bram_monitor_to_kernel_en <= '0';
        bram_kernel_to_monitor_en <= '0';
        bram_firmware_code_en <= '0';
        
        bram_tmc_bytes_selection <= byte_we_next; 
        bram_tmc_address <= cleaned_address_next;
        bram_tmc_data_write <= cpu_data_w;

        
     when  "100"  =>
        
        bram_ptm_traces_en <= '0';
        bram_bbt_annotations_en <= '0';
        bram_tmc_en <= '0';        
        bram_monitor_to_kernel_en <= '1';        
        bram_kernel_to_monitor_en <= '0';
        bram_firmware_code_en <= '0';
        
        bram_monitor_to_kernel_bytes_selection <= byte_we_next; 
        bram_monitor_to_kernel_address <= cleaned_address_next;
        bram_monitor_to_kernel_data_write <= cpu_data_w;

    
    when  "101"  =>
        bram_ptm_traces_en <= '0';
        bram_bbt_annotations_en <= '0';
        bram_tmc_en <= '0';        
        bram_monitor_to_kernel_en <= '0';
        bram_kernel_to_monitor_en <= '1';
        bram_firmware_code_en <= '0';
        
        bram_kernel_to_monitor_bytes_selection <= byte_we_next; 
        bram_kernel_to_monitor_address <= cleaned_address_next;
        bram_kernel_to_monitor_data_write <= cpu_data_w;


    when  "000"  =>
         bram_ptm_traces_en <= '0';
         bram_bbt_annotations_en <= '0';
         bram_tmc_en <= '0';        
         bram_monitor_to_kernel_en <= '0';
         bram_kernel_to_monitor_en <= '0';
         bram_firmware_code_en <= '1';
         
         bram_firmware_code_bytes_selection <= byte_we_next; 
         bram_firmware_code_address <= cleaned_address_next;
         bram_firmware_code_data_write <= cpu_data_w;
    
    when others =>     
end case;
 


   end process;
   

   bram_kernel_to_monitor_reset <= reset;
   bram_monitor_to_kernel_reset <= reset;
   bram_tmc_reset <= reset;
   bram_bbt_annotations_reset <= reset;
   bram_ptm_traces_reset <= reset;
   bram_firmware_code_reset <= reset;
         
   bram_firmware_code_clk <= clk;
   bram_ptm_traces_clk <= clk;
   bram_bbt_annotations_clk <= clk;
   bram_tmc_clk <= clk;
   bram_monitor_to_kernel_clk <= clk;
   bram_kernel_to_monitor_clk <= clk;     
    

end; --architecture logic
