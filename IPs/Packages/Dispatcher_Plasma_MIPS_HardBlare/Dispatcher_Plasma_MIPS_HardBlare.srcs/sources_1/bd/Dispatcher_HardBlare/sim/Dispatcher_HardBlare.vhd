--Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
--Date        : Thu Jan  9 07:15:02 2020
--Host        : HardBlare-Workstation running 64-bit Ubuntu 16.04.6 LTS
--Command     : generate_target Dispatcher_HardBlare.bd
--Design      : Dispatcher_HardBlare
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity Dispatcher_HardBlare is
  port (
    bram_bbt_annotations_address : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_bbt_annotations_bytes_selection : out STD_LOGIC_VECTOR ( 3 downto 0 );
    bram_bbt_annotations_clk : out STD_LOGIC;
    bram_bbt_annotations_data_read : in STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_bbt_annotations_data_write : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_bbt_annotations_en : out STD_LOGIC;
    bram_bbt_annotations_reset : out STD_LOGIC;
    bram_firmware_code_address : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_firmware_code_bytes_selection : out STD_LOGIC_VECTOR ( 3 downto 0 );
    bram_firmware_code_clk : out STD_LOGIC;
    bram_firmware_code_data_read : in STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_firmware_code_data_write : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_firmware_code_en : out STD_LOGIC;
    bram_firmware_code_reset : out STD_LOGIC;
    bram_kernel_to_monitor_address : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_kernel_to_monitor_bytes_selection : out STD_LOGIC_VECTOR ( 3 downto 0 );
    bram_kernel_to_monitor_clk : out STD_LOGIC;
    bram_kernel_to_monitor_data_read : in STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_kernel_to_monitor_data_write : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_kernel_to_monitor_en : out STD_LOGIC;
    bram_kernel_to_monitor_reset : out STD_LOGIC;
    bram_monitor_to_kernel_address : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_monitor_to_kernel_bytes_selection : out STD_LOGIC_VECTOR ( 3 downto 0 );
    bram_monitor_to_kernel_clk : out STD_LOGIC;
    bram_monitor_to_kernel_data_read : in STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_monitor_to_kernel_data_write : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_monitor_to_kernel_en : out STD_LOGIC;
    bram_monitor_to_kernel_reset : out STD_LOGIC;
    bram_ptm_traces_address : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_ptm_traces_bytes_selection : out STD_LOGIC_VECTOR ( 3 downto 0 );
    bram_ptm_traces_clk : out STD_LOGIC;
    bram_ptm_traces_data_read : in STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_ptm_traces_data_write : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_ptm_traces_en : out STD_LOGIC;
    bram_ptm_traces_reset : out STD_LOGIC;
    bram_tmc_address : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_tmc_bytes_selection : out STD_LOGIC_VECTOR ( 3 downto 0 );
    bram_tmc_clk : out STD_LOGIC;
    bram_tmc_data_read : in STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_tmc_data_write : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_tmc_en : out STD_LOGIC;
    bram_tmc_reset : out STD_LOGIC;
    clk : in STD_LOGIC;
    dispatcher_pause : in STD_LOGIC;
    ptm_is_enabled : in STD_LOGIC;
    reset : in STD_LOGIC
  );
  attribute CORE_GENERATION_INFO : string;
  attribute CORE_GENERATION_INFO of Dispatcher_HardBlare : entity is "Dispatcher_HardBlare,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLibrary=BlockDiagram,x_ipName=Dispatcher_HardBlare,x_ipVersion=1.00.a,x_ipLanguage=VHDL,numBlks=1,numReposBlks=1,numNonXlnxBlks=0,numHierBlks=0,maxHierDepth=0,numSysgenBlks=0,numHlsBlks=0,numHdlrefBlks=1,numPkgbdBlks=0,bdsource=USER,synth_mode=OOC_per_IP}";
  attribute HW_HANDOFF : string;
  attribute HW_HANDOFF of Dispatcher_HardBlare : entity is "Dispatcher_HardBlare.hwdef";
end Dispatcher_HardBlare;

architecture STRUCTURE of Dispatcher_HardBlare is
  component Dispatcher_HardBlare_Dispatcher_Plasma_MI_0_0 is
  port (
    clk : in STD_LOGIC;
    reset : in STD_LOGIC;
    bram_firmware_code_en : out STD_LOGIC;
    bram_firmware_code_reset : out STD_LOGIC;
    bram_firmware_code_clk : out STD_LOGIC;
    bram_firmware_code_address : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_firmware_code_data_write : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_firmware_code_bytes_selection : out STD_LOGIC_VECTOR ( 3 downto 0 );
    bram_firmware_code_data_read : in STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_ptm_traces_en : out STD_LOGIC;
    bram_ptm_traces_reset : out STD_LOGIC;
    bram_ptm_traces_clk : out STD_LOGIC;
    bram_ptm_traces_address : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_ptm_traces_data_write : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_ptm_traces_bytes_selection : out STD_LOGIC_VECTOR ( 3 downto 0 );
    bram_ptm_traces_data_read : in STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_bbt_annotations_en : out STD_LOGIC;
    bram_bbt_annotations_reset : out STD_LOGIC;
    bram_bbt_annotations_clk : out STD_LOGIC;
    bram_bbt_annotations_address : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_bbt_annotations_data_write : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_bbt_annotations_bytes_selection : out STD_LOGIC_VECTOR ( 3 downto 0 );
    bram_bbt_annotations_data_read : in STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_tmc_en : out STD_LOGIC;
    bram_tmc_reset : out STD_LOGIC;
    bram_tmc_clk : out STD_LOGIC;
    bram_tmc_address : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_tmc_data_write : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_tmc_bytes_selection : out STD_LOGIC_VECTOR ( 3 downto 0 );
    bram_tmc_data_read : in STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_monitor_to_kernel_en : out STD_LOGIC;
    bram_monitor_to_kernel_reset : out STD_LOGIC;
    bram_monitor_to_kernel_clk : out STD_LOGIC;
    bram_monitor_to_kernel_address : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_monitor_to_kernel_data_write : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_monitor_to_kernel_bytes_selection : out STD_LOGIC_VECTOR ( 3 downto 0 );
    bram_monitor_to_kernel_data_read : in STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_kernel_to_monitor_en : out STD_LOGIC;
    bram_kernel_to_monitor_reset : out STD_LOGIC;
    bram_kernel_to_monitor_clk : out STD_LOGIC;
    bram_kernel_to_monitor_address : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_kernel_to_monitor_data_write : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_kernel_to_monitor_bytes_selection : out STD_LOGIC_VECTOR ( 3 downto 0 );
    bram_kernel_to_monitor_data_read : in STD_LOGIC_VECTOR ( 31 downto 0 );
    dispatcher_pause : in STD_LOGIC;
    ptm_is_enabled : in STD_LOGIC
  );
  end component Dispatcher_HardBlare_Dispatcher_Plasma_MI_0_0;
  signal Dispatcher_Plasma_MI_0_bram_bbt_annotations_address : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal Dispatcher_Plasma_MI_0_bram_bbt_annotations_bytes_selection : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal Dispatcher_Plasma_MI_0_bram_bbt_annotations_clk : STD_LOGIC;
  signal Dispatcher_Plasma_MI_0_bram_bbt_annotations_data_write : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal Dispatcher_Plasma_MI_0_bram_bbt_annotations_en : STD_LOGIC;
  signal Dispatcher_Plasma_MI_0_bram_bbt_annotations_reset : STD_LOGIC;
  signal Dispatcher_Plasma_MI_0_bram_firmware_code_address : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal Dispatcher_Plasma_MI_0_bram_firmware_code_bytes_selection : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal Dispatcher_Plasma_MI_0_bram_firmware_code_clk : STD_LOGIC;
  signal Dispatcher_Plasma_MI_0_bram_firmware_code_data_write : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal Dispatcher_Plasma_MI_0_bram_firmware_code_en : STD_LOGIC;
  signal Dispatcher_Plasma_MI_0_bram_firmware_code_reset : STD_LOGIC;
  signal Dispatcher_Plasma_MI_0_bram_kernel_to_monitor_address : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal Dispatcher_Plasma_MI_0_bram_kernel_to_monitor_bytes_selection : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal Dispatcher_Plasma_MI_0_bram_kernel_to_monitor_clk : STD_LOGIC;
  signal Dispatcher_Plasma_MI_0_bram_kernel_to_monitor_data_write : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal Dispatcher_Plasma_MI_0_bram_kernel_to_monitor_en : STD_LOGIC;
  signal Dispatcher_Plasma_MI_0_bram_kernel_to_monitor_reset : STD_LOGIC;
  signal Dispatcher_Plasma_MI_0_bram_monitor_to_kernel_address : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal Dispatcher_Plasma_MI_0_bram_monitor_to_kernel_bytes_selection : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal Dispatcher_Plasma_MI_0_bram_monitor_to_kernel_clk : STD_LOGIC;
  signal Dispatcher_Plasma_MI_0_bram_monitor_to_kernel_data_write : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal Dispatcher_Plasma_MI_0_bram_monitor_to_kernel_en : STD_LOGIC;
  signal Dispatcher_Plasma_MI_0_bram_monitor_to_kernel_reset : STD_LOGIC;
  signal Dispatcher_Plasma_MI_0_bram_ptm_traces_address : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal Dispatcher_Plasma_MI_0_bram_ptm_traces_bytes_selection : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal Dispatcher_Plasma_MI_0_bram_ptm_traces_clk : STD_LOGIC;
  signal Dispatcher_Plasma_MI_0_bram_ptm_traces_data_write : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal Dispatcher_Plasma_MI_0_bram_ptm_traces_en : STD_LOGIC;
  signal Dispatcher_Plasma_MI_0_bram_ptm_traces_reset : STD_LOGIC;
  signal Dispatcher_Plasma_MI_0_bram_tmc_address : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal Dispatcher_Plasma_MI_0_bram_tmc_bytes_selection : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal Dispatcher_Plasma_MI_0_bram_tmc_clk : STD_LOGIC;
  signal Dispatcher_Plasma_MI_0_bram_tmc_data_write : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal Dispatcher_Plasma_MI_0_bram_tmc_en : STD_LOGIC;
  signal Dispatcher_Plasma_MI_0_bram_tmc_reset : STD_LOGIC;
  signal bram_bbt_annotations_data_read_0_1 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal bram_firmware_code_data_read_0_1 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal bram_kernel_to_monitor_data_read_0_1 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal bram_monitor_to_kernel_data_read_0_1 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal bram_ptm_traces_data_read_0_1 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal bram_tmc_data_read_0_1 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal clk_1 : STD_LOGIC;
  signal dispatcher_pause_0_1 : STD_LOGIC;
  signal ptm_is_enabled_0_1 : STD_LOGIC;
  signal reset_1 : STD_LOGIC;
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of bram_bbt_annotations_clk : signal is "xilinx.com:signal:clock:1.0 CLK.BRAM_BBT_ANNOTATIONS_CLK CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of bram_bbt_annotations_clk : signal is "XIL_INTERFACENAME CLK.BRAM_BBT_ANNOTATIONS_CLK, CLK_DOMAIN Dispatcher_HardBlare_Dispatcher_Plasma_MI_0_0_bram_bbt_annotations_clk, FREQ_HZ 100000000, PHASE 0.000";
  attribute X_INTERFACE_INFO of bram_bbt_annotations_reset : signal is "xilinx.com:signal:reset:1.0 RST.BRAM_BBT_ANNOTATIONS_RESET RST";
  attribute X_INTERFACE_PARAMETER of bram_bbt_annotations_reset : signal is "XIL_INTERFACENAME RST.BRAM_BBT_ANNOTATIONS_RESET, POLARITY ACTIVE_LOW";
  attribute X_INTERFACE_INFO of bram_firmware_code_clk : signal is "xilinx.com:signal:clock:1.0 CLK.BRAM_FIRMWARE_CODE_CLK CLK";
  attribute X_INTERFACE_PARAMETER of bram_firmware_code_clk : signal is "XIL_INTERFACENAME CLK.BRAM_FIRMWARE_CODE_CLK, CLK_DOMAIN Dispatcher_HardBlare_Dispatcher_Plasma_MI_0_0_bram_firmware_code_clk, FREQ_HZ 100000000, PHASE 0.000";
  attribute X_INTERFACE_INFO of bram_firmware_code_reset : signal is "xilinx.com:signal:reset:1.0 RST.BRAM_FIRMWARE_CODE_RESET RST";
  attribute X_INTERFACE_PARAMETER of bram_firmware_code_reset : signal is "XIL_INTERFACENAME RST.BRAM_FIRMWARE_CODE_RESET, POLARITY ACTIVE_LOW";
  attribute X_INTERFACE_INFO of bram_kernel_to_monitor_clk : signal is "xilinx.com:signal:clock:1.0 CLK.BRAM_KERNEL_TO_MONITOR_CLK CLK";
  attribute X_INTERFACE_PARAMETER of bram_kernel_to_monitor_clk : signal is "XIL_INTERFACENAME CLK.BRAM_KERNEL_TO_MONITOR_CLK, CLK_DOMAIN Dispatcher_HardBlare_Dispatcher_Plasma_MI_0_0_bram_kernel_to_monitor_clk, FREQ_HZ 100000000, PHASE 0.000";
  attribute X_INTERFACE_INFO of bram_kernel_to_monitor_reset : signal is "xilinx.com:signal:reset:1.0 RST.BRAM_KERNEL_TO_MONITOR_RESET RST";
  attribute X_INTERFACE_PARAMETER of bram_kernel_to_monitor_reset : signal is "XIL_INTERFACENAME RST.BRAM_KERNEL_TO_MONITOR_RESET, POLARITY ACTIVE_LOW";
  attribute X_INTERFACE_INFO of bram_monitor_to_kernel_clk : signal is "xilinx.com:signal:clock:1.0 CLK.BRAM_MONITOR_TO_KERNEL_CLK CLK";
  attribute X_INTERFACE_PARAMETER of bram_monitor_to_kernel_clk : signal is "XIL_INTERFACENAME CLK.BRAM_MONITOR_TO_KERNEL_CLK, CLK_DOMAIN Dispatcher_HardBlare_Dispatcher_Plasma_MI_0_0_bram_monitor_to_kernel_clk, FREQ_HZ 100000000, PHASE 0.000";
  attribute X_INTERFACE_INFO of bram_monitor_to_kernel_reset : signal is "xilinx.com:signal:reset:1.0 RST.BRAM_MONITOR_TO_KERNEL_RESET RST";
  attribute X_INTERFACE_PARAMETER of bram_monitor_to_kernel_reset : signal is "XIL_INTERFACENAME RST.BRAM_MONITOR_TO_KERNEL_RESET, POLARITY ACTIVE_LOW";
  attribute X_INTERFACE_INFO of bram_ptm_traces_clk : signal is "xilinx.com:signal:clock:1.0 CLK.BRAM_PTM_TRACES_CLK CLK";
  attribute X_INTERFACE_PARAMETER of bram_ptm_traces_clk : signal is "XIL_INTERFACENAME CLK.BRAM_PTM_TRACES_CLK, CLK_DOMAIN Dispatcher_HardBlare_Dispatcher_Plasma_MI_0_0_bram_ptm_traces_clk, FREQ_HZ 100000000, PHASE 0.000";
  attribute X_INTERFACE_INFO of bram_ptm_traces_reset : signal is "xilinx.com:signal:reset:1.0 RST.BRAM_PTM_TRACES_RESET RST";
  attribute X_INTERFACE_PARAMETER of bram_ptm_traces_reset : signal is "XIL_INTERFACENAME RST.BRAM_PTM_TRACES_RESET, POLARITY ACTIVE_LOW";
  attribute X_INTERFACE_INFO of bram_tmc_clk : signal is "xilinx.com:signal:clock:1.0 CLK.BRAM_TMC_CLK CLK";
  attribute X_INTERFACE_PARAMETER of bram_tmc_clk : signal is "XIL_INTERFACENAME CLK.BRAM_TMC_CLK, CLK_DOMAIN Dispatcher_HardBlare_Dispatcher_Plasma_MI_0_0_bram_tmc_clk, FREQ_HZ 100000000, PHASE 0.000";
  attribute X_INTERFACE_INFO of bram_tmc_reset : signal is "xilinx.com:signal:reset:1.0 RST.BRAM_TMC_RESET RST";
  attribute X_INTERFACE_PARAMETER of bram_tmc_reset : signal is "XIL_INTERFACENAME RST.BRAM_TMC_RESET, POLARITY ACTIVE_LOW";
begin
  bram_bbt_annotations_address(31 downto 0) <= Dispatcher_Plasma_MI_0_bram_bbt_annotations_address(31 downto 0);
  bram_bbt_annotations_bytes_selection(3 downto 0) <= Dispatcher_Plasma_MI_0_bram_bbt_annotations_bytes_selection(3 downto 0);
  bram_bbt_annotations_clk <= Dispatcher_Plasma_MI_0_bram_bbt_annotations_clk;
  bram_bbt_annotations_data_read_0_1(31 downto 0) <= bram_bbt_annotations_data_read(31 downto 0);
  bram_bbt_annotations_data_write(31 downto 0) <= Dispatcher_Plasma_MI_0_bram_bbt_annotations_data_write(31 downto 0);
  bram_bbt_annotations_en <= Dispatcher_Plasma_MI_0_bram_bbt_annotations_en;
  bram_bbt_annotations_reset <= Dispatcher_Plasma_MI_0_bram_bbt_annotations_reset;
  bram_firmware_code_address(31 downto 0) <= Dispatcher_Plasma_MI_0_bram_firmware_code_address(31 downto 0);
  bram_firmware_code_bytes_selection(3 downto 0) <= Dispatcher_Plasma_MI_0_bram_firmware_code_bytes_selection(3 downto 0);
  bram_firmware_code_clk <= Dispatcher_Plasma_MI_0_bram_firmware_code_clk;
  bram_firmware_code_data_read_0_1(31 downto 0) <= bram_firmware_code_data_read(31 downto 0);
  bram_firmware_code_data_write(31 downto 0) <= Dispatcher_Plasma_MI_0_bram_firmware_code_data_write(31 downto 0);
  bram_firmware_code_en <= Dispatcher_Plasma_MI_0_bram_firmware_code_en;
  bram_firmware_code_reset <= Dispatcher_Plasma_MI_0_bram_firmware_code_reset;
  bram_kernel_to_monitor_address(31 downto 0) <= Dispatcher_Plasma_MI_0_bram_kernel_to_monitor_address(31 downto 0);
  bram_kernel_to_monitor_bytes_selection(3 downto 0) <= Dispatcher_Plasma_MI_0_bram_kernel_to_monitor_bytes_selection(3 downto 0);
  bram_kernel_to_monitor_clk <= Dispatcher_Plasma_MI_0_bram_kernel_to_monitor_clk;
  bram_kernel_to_monitor_data_read_0_1(31 downto 0) <= bram_kernel_to_monitor_data_read(31 downto 0);
  bram_kernel_to_monitor_data_write(31 downto 0) <= Dispatcher_Plasma_MI_0_bram_kernel_to_monitor_data_write(31 downto 0);
  bram_kernel_to_monitor_en <= Dispatcher_Plasma_MI_0_bram_kernel_to_monitor_en;
  bram_kernel_to_monitor_reset <= Dispatcher_Plasma_MI_0_bram_kernel_to_monitor_reset;
  bram_monitor_to_kernel_address(31 downto 0) <= Dispatcher_Plasma_MI_0_bram_monitor_to_kernel_address(31 downto 0);
  bram_monitor_to_kernel_bytes_selection(3 downto 0) <= Dispatcher_Plasma_MI_0_bram_monitor_to_kernel_bytes_selection(3 downto 0);
  bram_monitor_to_kernel_clk <= Dispatcher_Plasma_MI_0_bram_monitor_to_kernel_clk;
  bram_monitor_to_kernel_data_read_0_1(31 downto 0) <= bram_monitor_to_kernel_data_read(31 downto 0);
  bram_monitor_to_kernel_data_write(31 downto 0) <= Dispatcher_Plasma_MI_0_bram_monitor_to_kernel_data_write(31 downto 0);
  bram_monitor_to_kernel_en <= Dispatcher_Plasma_MI_0_bram_monitor_to_kernel_en;
  bram_monitor_to_kernel_reset <= Dispatcher_Plasma_MI_0_bram_monitor_to_kernel_reset;
  bram_ptm_traces_address(31 downto 0) <= Dispatcher_Plasma_MI_0_bram_ptm_traces_address(31 downto 0);
  bram_ptm_traces_bytes_selection(3 downto 0) <= Dispatcher_Plasma_MI_0_bram_ptm_traces_bytes_selection(3 downto 0);
  bram_ptm_traces_clk <= Dispatcher_Plasma_MI_0_bram_ptm_traces_clk;
  bram_ptm_traces_data_read_0_1(31 downto 0) <= bram_ptm_traces_data_read(31 downto 0);
  bram_ptm_traces_data_write(31 downto 0) <= Dispatcher_Plasma_MI_0_bram_ptm_traces_data_write(31 downto 0);
  bram_ptm_traces_en <= Dispatcher_Plasma_MI_0_bram_ptm_traces_en;
  bram_ptm_traces_reset <= Dispatcher_Plasma_MI_0_bram_ptm_traces_reset;
  bram_tmc_address(31 downto 0) <= Dispatcher_Plasma_MI_0_bram_tmc_address(31 downto 0);
  bram_tmc_bytes_selection(3 downto 0) <= Dispatcher_Plasma_MI_0_bram_tmc_bytes_selection(3 downto 0);
  bram_tmc_clk <= Dispatcher_Plasma_MI_0_bram_tmc_clk;
  bram_tmc_data_read_0_1(31 downto 0) <= bram_tmc_data_read(31 downto 0);
  bram_tmc_data_write(31 downto 0) <= Dispatcher_Plasma_MI_0_bram_tmc_data_write(31 downto 0);
  bram_tmc_en <= Dispatcher_Plasma_MI_0_bram_tmc_en;
  bram_tmc_reset <= Dispatcher_Plasma_MI_0_bram_tmc_reset;
  clk_1 <= clk;
  dispatcher_pause_0_1 <= dispatcher_pause;
  ptm_is_enabled_0_1 <= ptm_is_enabled;
  reset_1 <= reset;
Dispatcher_Plasma_MI_0: component Dispatcher_HardBlare_Dispatcher_Plasma_MI_0_0
     port map (
      bram_bbt_annotations_address(31 downto 0) => Dispatcher_Plasma_MI_0_bram_bbt_annotations_address(31 downto 0),
      bram_bbt_annotations_bytes_selection(3 downto 0) => Dispatcher_Plasma_MI_0_bram_bbt_annotations_bytes_selection(3 downto 0),
      bram_bbt_annotations_clk => Dispatcher_Plasma_MI_0_bram_bbt_annotations_clk,
      bram_bbt_annotations_data_read(31 downto 0) => bram_bbt_annotations_data_read_0_1(31 downto 0),
      bram_bbt_annotations_data_write(31 downto 0) => Dispatcher_Plasma_MI_0_bram_bbt_annotations_data_write(31 downto 0),
      bram_bbt_annotations_en => Dispatcher_Plasma_MI_0_bram_bbt_annotations_en,
      bram_bbt_annotations_reset => Dispatcher_Plasma_MI_0_bram_bbt_annotations_reset,
      bram_firmware_code_address(31 downto 0) => Dispatcher_Plasma_MI_0_bram_firmware_code_address(31 downto 0),
      bram_firmware_code_bytes_selection(3 downto 0) => Dispatcher_Plasma_MI_0_bram_firmware_code_bytes_selection(3 downto 0),
      bram_firmware_code_clk => Dispatcher_Plasma_MI_0_bram_firmware_code_clk,
      bram_firmware_code_data_read(31 downto 0) => bram_firmware_code_data_read_0_1(31 downto 0),
      bram_firmware_code_data_write(31 downto 0) => Dispatcher_Plasma_MI_0_bram_firmware_code_data_write(31 downto 0),
      bram_firmware_code_en => Dispatcher_Plasma_MI_0_bram_firmware_code_en,
      bram_firmware_code_reset => Dispatcher_Plasma_MI_0_bram_firmware_code_reset,
      bram_kernel_to_monitor_address(31 downto 0) => Dispatcher_Plasma_MI_0_bram_kernel_to_monitor_address(31 downto 0),
      bram_kernel_to_monitor_bytes_selection(3 downto 0) => Dispatcher_Plasma_MI_0_bram_kernel_to_monitor_bytes_selection(3 downto 0),
      bram_kernel_to_monitor_clk => Dispatcher_Plasma_MI_0_bram_kernel_to_monitor_clk,
      bram_kernel_to_monitor_data_read(31 downto 0) => bram_kernel_to_monitor_data_read_0_1(31 downto 0),
      bram_kernel_to_monitor_data_write(31 downto 0) => Dispatcher_Plasma_MI_0_bram_kernel_to_monitor_data_write(31 downto 0),
      bram_kernel_to_monitor_en => Dispatcher_Plasma_MI_0_bram_kernel_to_monitor_en,
      bram_kernel_to_monitor_reset => Dispatcher_Plasma_MI_0_bram_kernel_to_monitor_reset,
      bram_monitor_to_kernel_address(31 downto 0) => Dispatcher_Plasma_MI_0_bram_monitor_to_kernel_address(31 downto 0),
      bram_monitor_to_kernel_bytes_selection(3 downto 0) => Dispatcher_Plasma_MI_0_bram_monitor_to_kernel_bytes_selection(3 downto 0),
      bram_monitor_to_kernel_clk => Dispatcher_Plasma_MI_0_bram_monitor_to_kernel_clk,
      bram_monitor_to_kernel_data_read(31 downto 0) => bram_monitor_to_kernel_data_read_0_1(31 downto 0),
      bram_monitor_to_kernel_data_write(31 downto 0) => Dispatcher_Plasma_MI_0_bram_monitor_to_kernel_data_write(31 downto 0),
      bram_monitor_to_kernel_en => Dispatcher_Plasma_MI_0_bram_monitor_to_kernel_en,
      bram_monitor_to_kernel_reset => Dispatcher_Plasma_MI_0_bram_monitor_to_kernel_reset,
      bram_ptm_traces_address(31 downto 0) => Dispatcher_Plasma_MI_0_bram_ptm_traces_address(31 downto 0),
      bram_ptm_traces_bytes_selection(3 downto 0) => Dispatcher_Plasma_MI_0_bram_ptm_traces_bytes_selection(3 downto 0),
      bram_ptm_traces_clk => Dispatcher_Plasma_MI_0_bram_ptm_traces_clk,
      bram_ptm_traces_data_read(31 downto 0) => bram_ptm_traces_data_read_0_1(31 downto 0),
      bram_ptm_traces_data_write(31 downto 0) => Dispatcher_Plasma_MI_0_bram_ptm_traces_data_write(31 downto 0),
      bram_ptm_traces_en => Dispatcher_Plasma_MI_0_bram_ptm_traces_en,
      bram_ptm_traces_reset => Dispatcher_Plasma_MI_0_bram_ptm_traces_reset,
      bram_tmc_address(31 downto 0) => Dispatcher_Plasma_MI_0_bram_tmc_address(31 downto 0),
      bram_tmc_bytes_selection(3 downto 0) => Dispatcher_Plasma_MI_0_bram_tmc_bytes_selection(3 downto 0),
      bram_tmc_clk => Dispatcher_Plasma_MI_0_bram_tmc_clk,
      bram_tmc_data_read(31 downto 0) => bram_tmc_data_read_0_1(31 downto 0),
      bram_tmc_data_write(31 downto 0) => Dispatcher_Plasma_MI_0_bram_tmc_data_write(31 downto 0),
      bram_tmc_en => Dispatcher_Plasma_MI_0_bram_tmc_en,
      bram_tmc_reset => Dispatcher_Plasma_MI_0_bram_tmc_reset,
      clk => clk_1,
      dispatcher_pause => dispatcher_pause_0_1,
      ptm_is_enabled => ptm_is_enabled_0_1,
      reset => reset_1
    );
end STRUCTURE;
