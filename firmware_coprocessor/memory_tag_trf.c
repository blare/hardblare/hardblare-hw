#include "types.h"
#include "decode.h"
#include "memory_tag_trf.h"
#include "address.h"
#include "debug.h"
#include "irq.h"


uint32_t TRF[NUMBER_REGISTERS] = {0};
uint32_t offset_mem_tag;
mem_tag_entry mem_tag[MEM_TAG_SIZE];


void dumpTRF()
	{

		#ifdef DEBUG

			int i;

			for(i = 0; i < NUMBER_REGISTERS; i++)
			  {
			    uint32_t val = TRF[i];
			    puts("TRF[");
			    print_hex(i);
			    puts("] = ");
			    print_hex(TRF[i]);
			    puts("\n");
			  }

		#endif
	}


void dumpMem()
{

	#ifdef DEBUG

	  int i;

	  for(i = 0; (i < MEM_TAG_SIZE) || (i == 0); i++)
	    {
	      puts("Tag(@");
	      print_hex(mem_tag[i].address);
	      puts("] = ");
	      print_hex(mem_tag[i].tag);
	      puts("\n");
	    }

	#endif
}



uint32_t create_mem_entry_with_address(uint32_t address) {

	if((offset_mem_tag < MEM_TAG_SIZE) || (offset_mem_tag == 0) )
    {
      mem_tag[offset_mem_tag].address = address;
      mem_tag[offset_mem_tag].tag = SECURITY_POLICY_DEFAULT_TAG[SECURITY_POLICY_DEFAULT_TAG_UNKNOWN];

      // DEBUG
      IF_SIMULATION(debug_print_create_mem_entry_with_address(mem_tag[offset_mem_tag].address, mem_tag[offset_mem_tag].tag); );

      offset_mem_tag++;

      return 1;
    }

	return 0;
}


uint32_t search_value_of_address(uint32_t address) {

	int i;

	for (i = 0; (i < offset_mem_tag) || (i == 0); i++)
	{
		if(mem_tag[i].address == address)
		{
      // DEBUG
      IF_SIMULATION( debug_print_search_value_of_address(address, mem_tag[i].tag) );

			return mem_tag[i].tag;
		}
	}

  // DEBUG
  IF_SIMULATION( debug_print_search_value_of_address_not_found (address) );

	return SECURITY_POLICY_DEFAULT_TAG[SECURITY_POLICY_DEFAULT_TAG_UNKNOWN];
}


uint32_t search_and_affect_value_of_address(uint32_t address, uint32_t tag) {

	int i;

 	for (i = 0; (i < offset_mem_tag) || (i == 0) ; i++) {

		if(mem_tag[i].address == address)
      {
        mem_tag[i].tag = tag;

        // DEBUG
        IF_SIMULATION( debug_print_search_and_affect_value_of_address(address, tag) );
        IF_HARDWARE_DEBUG( MemoryWrite(getDebugAddress(), 0xdddddddd);  MemoryWrite(getDebugAddress(), address);  MemoryWrite(getDebugAddress(), tag); MemoryWrite(getDebugAddress(), 0xdddddddd);  );

        return 1;
      }
	}

  // DEBUG
  IF_SIMULATION( debug_print_search_and_affect_value_of_address_not_found(address) );

	return 0;


}


void search_and_affect_tag_from_lsm_hook(uint32_t address, uint32_t size, uint32_t tag) {

  uint32_t i, tagged_address = 0;

  for (i = 0; i < size ; i++) {

    tagged_address = address + i;

    if(!search_and_affect_value_of_address(tagged_address, tag)) {
      if(!create_mem_entry_with_address(tagged_address)) {
        raise_irq_when_mem_tag_full(tagged_address, address, size,  tag);
      }

      if(!search_and_affect_value_of_address(tagged_address, tag)) {
        raise_irq_when_unfound_memory_address(tagged_address, address, size,  tag);
      }
    }
  }
}

uint32_t search_tag_from_lsm_hook(uint32_t address, uint32_t size) {

  uint32_t i, tagged_address = 0;
  uint32_t tag_value = 0;

  for (i = 0; i < size; i++) {

    tagged_address = address + i;
    tag_value = (*ARM_OPCODE_FUNC[SECURITY_POLICY_MERGING_TAG_RESULT_TO_LSM_HOOK])(tag_value, search_value_of_address(tagged_address));
    IF_HARDWARE_DEBUG( MemoryWrite(getDebugAddress(), 0xaaaaaaaa); MemoryWrite(getDebugAddress(), tagged_address); MemoryWrite(getDebugAddress(), tag_value); MemoryWrite(getDebugAddress(), 0xaaaaaaaa); );

  }

  // DEBUG
  IF_SIMULATION(debug_print_search_tag_from_lsm_hook_result_tag(tag_value););
  IF_HARDWARE_DEBUG( MemoryWrite(getDebugAddress(), 0xaaaaaaaa); MemoryWrite(getDebugAddress(), tag_value); MemoryWrite(getDebugAddress(), 0xaaaaaaaa); );

  return tag_value;
}









