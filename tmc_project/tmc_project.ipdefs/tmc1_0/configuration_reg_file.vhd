---------------------------------------------------------------------
-- TITLE: Configuration Register File
-- AUTHOR: MAW
-- DATE CREATED: 22/03/2018
-- FILENAME: configuration_reg_file.vhd
-- PROJECT: DIFT coprocessor
-- DESCRIPTION:
--    Implements a register bank with 16 registers that are 32-bits wide.
--    These registers are used to communicate security policy which is set by the kernel.
---------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.dependencies_pack.all;

entity configuration_reg_file is
port (
  clk         : in  std_logic;
  w_en        : in  std_logic; -- for registers 0 - 23
  sel_d       : in  std_logic_vector(3 downto 0);
  data_in     : in  std_logic_vector(31 downto 0); 
  configuration_array_o : out configuration_array
  --data_out_1  : out std_logic_vector(31 downto 0);
  --data_out_2  : out std_logic_vector(31 downto 0);
  --data_out_3  : out std_logic_vector(31 downto 0);
  --data_out_4  : out std_logic_vector(31 downto 0);
  --data_out_5  : out std_logic_vector(31 downto 0);
  --data_out_6  : out std_logic_vector(31 downto 0);
  --data_out_7  : out std_logic_vector(31 downto 0);
  --data_out_8  : out std_logic_vector(31 downto 0);
  --data_out_9  : out std_logic_vector(31 downto 0);
  --data_out_10  : out std_logic_vector(31 downto 0);
  --data_out_11  : out std_logic_vector(31 downto 0);
  --data_out_12  : out std_logic_vector(31 downto 0);
  --data_out_13  : out std_logic_vector(31 downto 0);
  --data_out_14  : out std_logic_vector(31 downto 0);
  --data_out_15 : out std_logic_vector(31 downto 0);
  --data_out_16  : out std_logic_vector(31 downto 0)
   );
end entity; --configuration_reg_file

architecture beahvioral of configuration_reg_file is
--  type reg_array_t is array (0 to 15) of std_logic_vector(31 downto 0);
  signal regf_s : configuration_array := (others => (others=>'0'));
  begin
    process(clk)
    begin
      if rising_edge(clk) then
        if w_en = '1' then
          regf_s(to_integer(unsigned(sel_d))) <= data_in;
        end if;
      end if; 
    end process;

    configuration_array_o <= regf_s;
end architecture; -- behavioral
