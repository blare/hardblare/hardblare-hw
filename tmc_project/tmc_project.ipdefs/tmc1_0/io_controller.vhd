library ieee;
use ieee.std_logic_1164.all;
--use work.mips_pack.all;
--use ieee.numeric_std.all;

entity io_controller is 
port(
	mem_address_in 		: in  std_logic_vector(31 downto 0);
	data_mem_cs			: out std_logic
	--configuration_mem_cs: out std_logic
);
end entity;

architecture behavioral of io_controller is 
alias physical_mem : std_logic_vector(1 downto 0) is mem_address_in(13 downto 12);
begin

	cs_process: process(mem_address_in)	
	begin
		case physical_mem is 
		when "00" => 
			data_mem_cs			<= '1';
			--configuration_mem_cs<= '0';
		when "01" => 
			data_mem_cs			<= '1';
			--configuration_mem_cs<= '1';
		when others => 
			data_mem_cs			<= '1';
			--configuration_mem_cs 	<= '0';
		end case;
	end process;

end architecture;
