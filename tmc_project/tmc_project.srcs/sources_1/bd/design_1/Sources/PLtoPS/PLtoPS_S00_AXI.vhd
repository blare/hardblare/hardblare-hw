library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity PLtoPS_S00_AXI is
	generic (
		-- Users to add parameters here

		-- User parameters ends
		-- Do not modify the parameters beyond this line

		-- Width of S_AXI data bus
		C_S_AXI_DATA_WIDTH	: integer	:= 32;
		-- Width of S_AXI address bus
		C_S_AXI_ADDR_WIDTH	: integer	:= 32
	);
	port (
		-- Users to add ports here
        
            
        -- FIFO instrumentation        
        fifo_instrumentation_empty            : in std_logic;
        fifo_instrumentation_almost_empty     : in std_logic;
        fifo_instrumentation_full             : in std_logic; 
        fifo_instrumentation_almost_full      : in std_logic;  
        fifo_instrumentation_write_ack        : in std_logic;
        fifo_instrumentation_read_ack         : in std_logic;
          
        -- FIFO TMC      
        fifo_tmc_empty              : in std_logic;
        fifo_tmc_almost_empty       : in std_logic;             
        fifo_tmc_full               : in std_logic;
        fifo_tmc_almost_full        : in std_logic;               
        fifo_tmc_write_ack          : in std_logic;
        fifo_tmc_read_ack           : in std_logic;         
                    
        -- FIFO Monitor to Kernel        
        fifo_monitor_to_kernel_empty            : in std_logic;
        fifo_monitor_to_kernel_almost_empty     : in std_logic;
        fifo_monitor_to_kernel_full             : in std_logic;
        fifo_monitor_to_kernel_almost_full      : in std_logic;        
        fifo_monitor_to_kernel_write_ack        : in std_logic;
        fifo_monitor_to_kernel_read_ack         : in std_logic;
                
        -- FIFO Kernel to Monitor
        fifo_kernel_to_monitor_empty                : in std_logic;
        fifo_kernel_to_monitor_almost_empty         : in std_logic;     
        fifo_kernel_to_monitor_full                    : in std_logic;
        fifo_kernel_to_monitor_almost_full          : in std_logic;     
        fifo_kernel_to_monitor_write_ack            : in std_logic;
        fifo_kernel_to_monitor_read_ack             : in std_logic;     
                    
        -- FIFO PTM
        fifo_ptm_empty                           : in std_logic;
        fifo_ptm_almost_empty                    : in std_logic;
        fifo_ptm_full                            : in std_logic;
        fifo_ptm_almost_full                     : in std_logic;
        fifo_ptm_write_ack                        : in std_logic;
        fifo_ptm_read_ack                        : in std_logic;     
                
        -- Dispatcher
       --  dispatcher_status              : in std_logic;
        dispatcher_read_enable          : in std_logic;
        dispatcher_write_enable         : in std_logic;
        dispatcher_address              : in std_logic_vector(31 downto 0);
        dispatcher_write_data_in        : in std_logic_vector(31 downto 0);
        dispatcher_data_out             : out std_logic_vector(31 downto 0);
        
        dispatcher_interruption_generate        : in std_logic;
        to_dispatcher_interruption_ack          : out std_logic;
        
        -- TMC
        tmc_status              : in std_logic;
        tmc_read_enable         : in std_logic;
        tmc_write_enable        : in std_logic;
        tmc_address       : in std_logic_vector(31 downto 0);
        tmc_write_data_in       : in std_logic_vector(31 downto 0);
        tmc_data_out      : out std_logic_vector(31 downto 0);
        tmc_interruption        : in std_logic;
        
		-- User ports ends
		-- Do not modify the ports beyond this line

		-- Global Clock Signal
		S_AXI_ACLK	: in std_logic;
		-- Global Reset Signal. This Signal is Active LOW
		S_AXI_ARESETN	: in std_logic;
		-- Write address (issued by master, acceped by Slave)
		S_AXI_AWADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		-- Write channel Protection type. This signal indicates the
    		-- privilege and security level of the transaction, and whether
    		-- the transaction is a data access or an instruction access.
		S_AXI_AWPROT	: in std_logic_vector(2 downto 0);
		-- Write address valid. This signal indicates that the master signaling
    		-- valid write address and control information.
		S_AXI_AWVALID	: in std_logic;
		-- Write address ready. This signal indicates that the slave is ready
    		-- to accept an address and associated control signals.
		S_AXI_AWREADY	: out std_logic;
		-- Write data (issued by master, acceped by Slave) 
		S_AXI_WDATA	: in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		-- Write strobes. This signal indicates which byte lanes hold
    		-- valid data. There is one write strobe bit for each eight
    		-- bits of the write data bus.    
		S_AXI_WSTRB	: in std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
		-- Write valid. This signal indicates that valid write
    		-- data and strobes are available.
		S_AXI_WVALID	: in std_logic;
		-- Write ready. This signal indicates that the slave
    		-- can accept the write data.
		S_AXI_WREADY	: out std_logic;
		-- Write response. This signal indicates the status
    		-- of the write transaction.
		S_AXI_BRESP	: out std_logic_vector(1 downto 0);
		-- Write response valid. This signal indicates that the channel
    		-- is signaling a valid write response.
		S_AXI_BVALID	: out std_logic;
		-- Response ready. This signal indicates that the master
    		-- can accept a write response.
		S_AXI_BREADY	: in std_logic;
		-- Read address (issued by master, acceped by Slave)
		S_AXI_ARADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		-- Protection type. This signal indicates the privilege
    		-- and security level of the transaction, and whether the
    		-- transaction is a data access or an instruction access.
		S_AXI_ARPROT	: in std_logic_vector(2 downto 0);
		-- Read address valid. This signal indicates that the channel
    		-- is signaling valid read address and control information.
		S_AXI_ARVALID	: in std_logic;
		-- Read address ready. This signal indicates that the slave is
    		-- ready to accept an address and associated control signals.
		S_AXI_ARREADY	: out std_logic;
		-- Read data (issued by slave)
		S_AXI_RDATA	: out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		-- Read response. This signal indicates the status of the
    		-- read transfer.
		S_AXI_RRESP	: out std_logic_vector(1 downto 0);
		-- Read valid. This signal indicates that the channel is
    		-- signaling the required read data.
		S_AXI_RVALID	: out std_logic;
		-- Read ready. This signal indicates that the master can
    		-- accept the read data and response information.
		S_AXI_RREADY	: in std_logic
	);
end PLtoPS_S00_AXI;

architecture arch_imp of PLtoPS_S00_AXI is

	-- AXI4LITE signals
	signal axi_awaddr	: std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
	signal axi_awready	: std_logic;
	signal axi_wready	: std_logic;
	signal axi_bresp	: std_logic_vector(1 downto 0);
	signal axi_bvalid	: std_logic;
	signal axi_araddr	: std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
	signal axi_arready	: std_logic;
	signal axi_rdata	: std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal axi_rresp	: std_logic_vector(1 downto 0);
	signal axi_rvalid	: std_logic;

	-- Example-specific design signals
	-- local parameter for addressing 32 bit / 64 bit C_S_AXI_DATA_WIDTH
	-- ADDR_LSB is used for addressing 32/64 bit registers/memories
	-- ADDR_LSB = 2 for 32 bits (n downto 2)
	-- ADDR_LSB = 3 for 64 bits (n downto 3)
	constant ADDR_LSB  : integer := (C_S_AXI_DATA_WIDTH/32)+ 1;
	constant OPT_MEM_ADDR_BITS : integer := 8;
	------------------------------------------------
	---- Signals for user logic register space example
	--------------------------------------------------
	---- Number of Slave Registers 512
	signal slv_reg0	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg1	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg2	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg3	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg4	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg5	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg6	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg7	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg8	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg9	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg10	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg11	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg12	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg13	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg14	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg15	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg16	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg17	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg18	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg19	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg20	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg21	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg22	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg23	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg24	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg25	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg26	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg27	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg28	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg29	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg30	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg31	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg32	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg33	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg34	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg35	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg36	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg37	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg38	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg39	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg40	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg41	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg42	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg43	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg44	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg45	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg46	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg47	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg48	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg49	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg50	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg51	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg52	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg53	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg54	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg55	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg56	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg57	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg58	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg59	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg60	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg61	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg62	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg63	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg64	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg65	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg66	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg67	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg68	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg69	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg70	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg71	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg72	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg73	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg74	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg75	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg76	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg77	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg78	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg79	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg80	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg81	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg82	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg83	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg84	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg85	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg86	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg87	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg88	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg89	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg90	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg91	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg92	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg93	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg94	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg95	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg96	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg97	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg98	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg99	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg100	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg101	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg102	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg103	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg104	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg105	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg106	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg107	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg108	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg109	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg110	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg111	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg112	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg113	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg114	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg115	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg116	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg117	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg118	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg119	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg120	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg121	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg122	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg123	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg124	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg125	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg126	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg127	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg128	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg129	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg130	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg131	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg132	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg133	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg134	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg135	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg136	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg137	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg138	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg139	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg140	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg141	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg142	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg143	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg144	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg145	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg146	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg147	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg148	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg149	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg150	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg151	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg152	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg153	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg154	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg155	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg156	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg157	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg158	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg159	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg160	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg161	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg162	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg163	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg164	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg165	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg166	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg167	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg168	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg169	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg170	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg171	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg172	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg173	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg174	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg175	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg176	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg177	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg178	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg179	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg180	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg181	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg182	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg183	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg184	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg185	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg186	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg187	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg188	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg189	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg190	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg191	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg192	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg193	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg194	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg195	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg196	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg197	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg198	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg199	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg200	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg201	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg202	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg203	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg204	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg205	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg206	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg207	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg208	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg209	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg210	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg211	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg212	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg213	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg214	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg215	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg216	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg217	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg218	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg219	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg220	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg221	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg222	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg223	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg224	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg225	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg226	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg227	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg228	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg229	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg230	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg231	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg232	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg233	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg234	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg235	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg236	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg237	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg238	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg239	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg240	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg241	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg242	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg243	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg244	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg245	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg246	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg247	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg248	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg249	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg250	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg251	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg252	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg253	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg254	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg255	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg256	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg257	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg258	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg259	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg260	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg261	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg262	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg263	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg264	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg265	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg266	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg267	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg268	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg269	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg270	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg271	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg272	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg273	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg274	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg275	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg276	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg277	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg278	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg279	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg280	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg281	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg282	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg283	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg284	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg285	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg286	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg287	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg288	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg289	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg290	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg291	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg292	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg293	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg294	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg295	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg296	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg297	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg298	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg299	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg300	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg301	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg302	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg303	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg304	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg305	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg306	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg307	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg308	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg309	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg310	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg311	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg312	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg313	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg314	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg315	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg316	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg317	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg318	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg319	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg320	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg321	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg322	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg323	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg324	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg325	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg326	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg327	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg328	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg329	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg330	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg331	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg332	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg333	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg334	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg335	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg336	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg337	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg338	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg339	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg340	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg341	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg342	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg343	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg344	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg345	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg346	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg347	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg348	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg349	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg350	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg351	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg352	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg353	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg354	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg355	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg356	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg357	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg358	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg359	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg360	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg361	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg362	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg363	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg364	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg365	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg366	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg367	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg368	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg369	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg370	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg371	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg372	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg373	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg374	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg375	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg376	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg377	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg378	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg379	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg380	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg381	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg382	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg383	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg384	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg385	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg386	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg387	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg388	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg389	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg390	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg391	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg392	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg393	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg394	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg395	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg396	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg397	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg398	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg399	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg400	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg401	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg402	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg403	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg404	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg405	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg406	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg407	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg408	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg409	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg410	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg411	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg412	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg413	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg414	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg415	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg416	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg417	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg418	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg419	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg420	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg421	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg422	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg423	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg424	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg425	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg426	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg427	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg428	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg429	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg430	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg431	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg432	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg433	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg434	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg435	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg436	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg437	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg438	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg439	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg440	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg441	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg442	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg443	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg444	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg445	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg446	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg447	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg448	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg449	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg450	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg451	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg452	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg453	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg454	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg455	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg456	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg457	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg458	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg459	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg460	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg461	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg462	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg463	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg464	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg465	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg466	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg467	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg468	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg469	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg470	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg471	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg472	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg473	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg474	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg475	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg476	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg477	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg478	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg479	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg480	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg481	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg482	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg483	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg484	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg485	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg486	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg487	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg488	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg489	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg490	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg491	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg492	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg493	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg494	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg495	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg496	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg497	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg498	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg499	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg500	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg501	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg502	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg503	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg504	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg505	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg506	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg507	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg508	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg509	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg510	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg511	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg_rden	: std_logic;
	signal slv_reg_wren	: std_logic;
	signal reg_data_out	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal byte_index	: integer;
	signal aw_en	: std_logic;

begin
	-- I/O Connections assignments

	S_AXI_AWREADY	<= axi_awready;
	S_AXI_WREADY	<= axi_wready;
	S_AXI_BRESP	<= axi_bresp;
	S_AXI_BVALID	<= axi_bvalid;
	S_AXI_ARREADY	<= axi_arready;
	S_AXI_RDATA	<= axi_rdata;
	S_AXI_RRESP	<= axi_rresp;
	S_AXI_RVALID	<= axi_rvalid;
	-- Implement axi_awready generation
	-- axi_awready is asserted for one S_AXI_ACLK clock cycle when both
	-- S_AXI_AWVALID and S_AXI_WVALID are asserted. axi_awready is
	-- de-asserted when reset is low.

	process (S_AXI_ACLK)
	begin
	  if rising_edge(S_AXI_ACLK) then 
	    if S_AXI_ARESETN = '0' then
	      axi_awready <= '0';
	      aw_en <= '1';
	    else
	      if (axi_awready = '0' and S_AXI_AWVALID = '1' and S_AXI_WVALID = '1' and aw_en = '1') then
	        -- slave is ready to accept write address when
	        -- there is a valid write address and write data
	        -- on the write address and data bus. This design 
	        -- expects no outstanding transactions. 
	        axi_awready <= '1';
	        elsif (S_AXI_BREADY = '1' and axi_bvalid = '1') then
	            aw_en <= '1';
	        	axi_awready <= '0';
	      else
	        axi_awready <= '0';
	      end if;
	    end if;
	  end if;
	end process;

	-- Implement axi_awaddr latching
	-- This process is used to latch the address when both 
	-- S_AXI_AWVALID and S_AXI_WVALID are valid. 

	process (S_AXI_ACLK)
	begin
	  if rising_edge(S_AXI_ACLK) then 
	    if S_AXI_ARESETN = '0' then
	      axi_awaddr <= (others => '0');
	    else
	      if (axi_awready = '0' and S_AXI_AWVALID = '1' and S_AXI_WVALID = '1' and aw_en = '1') then
	        -- Write Address latching
	        axi_awaddr <= S_AXI_AWADDR;
	      end if;
	    end if;
	  end if;                   
	end process; 

	-- Implement axi_wready generation
	-- axi_wready is asserted for one S_AXI_ACLK clock cycle when both
	-- S_AXI_AWVALID and S_AXI_WVALID are asserted. axi_wready is 
	-- de-asserted when reset is low. 

	process (S_AXI_ACLK)
	begin
	  if rising_edge(S_AXI_ACLK) then 
	    if S_AXI_ARESETN = '0' then
	      axi_wready <= '0';
	    else
	      if (axi_wready = '0' and S_AXI_WVALID = '1' and S_AXI_AWVALID = '1' and aw_en = '1') then
	          -- slave is ready to accept write data when 
	          -- there is a valid write address and write data
	          -- on the write address and data bus. This design 
	          -- expects no outstanding transactions.           
	          axi_wready <= '1';
	      else
	        axi_wready <= '0';
	      end if;
	    end if;
	  end if;
	end process; 

	-- Implement memory mapped register select and write logic generation
	-- The write data is accepted and written to memory mapped registers when
	-- axi_awready, S_AXI_WVALID, axi_wready and S_AXI_WVALID are asserted. Write strobes are used to
	-- select byte enables of slave registers while writing.
	-- These registers are cleared when reset (active low) is applied.
	-- Slave register write enable is asserted when valid address and data are available
	-- and the slave is ready to accept the write address and write data.
	slv_reg_wren <= axi_wready and S_AXI_WVALID and axi_awready and S_AXI_AWVALID ;

	process (S_AXI_ACLK)
	variable loc_addr :std_logic_vector(OPT_MEM_ADDR_BITS downto 0); 
	begin
	  if rising_edge(S_AXI_ACLK) then 
	    if S_AXI_ARESETN = '0' then
	      slv_reg0 <= (others => '0');
	      slv_reg1 <= (others => '0');
	      slv_reg2 <= (others => '0');
	      slv_reg3 <= (others => '0');
	      slv_reg4 <= (others => '0');
	      slv_reg5 <= (others => '0');
	      slv_reg6 <= (others => '0');
	      slv_reg7 <= (others => '0');
	      slv_reg8 <= (others => '0');
	      slv_reg9 <= (others => '0');
	      slv_reg10 <= (others => '0');
	      slv_reg11 <= (others => '0');
	      slv_reg12 <= (others => '0');
	      slv_reg13 <= (others => '0');
	      slv_reg14 <= (others => '0');
	      slv_reg15 <= (others => '0');
	      slv_reg16 <= (others => '0');
	      slv_reg17 <= (others => '0');
	      slv_reg18 <= (others => '0');
	      slv_reg19 <= (others => '0');
	      slv_reg20 <= (others => '0');
	      slv_reg21 <= (others => '0');
	      slv_reg22 <= (others => '0');
	      slv_reg23 <= (others => '0');
	      slv_reg24 <= (others => '0');
	      slv_reg25 <= (others => '0');
	      slv_reg26 <= (others => '0');
	      slv_reg27 <= (others => '0');
	      slv_reg28 <= (others => '0');
	      slv_reg29 <= (others => '0');
	      slv_reg30 <= (others => '0');
	      slv_reg31 <= (others => '0');
	      slv_reg32 <= (others => '0');
	      slv_reg33 <= (others => '0');
	      slv_reg34 <= (others => '0');
	      slv_reg35 <= (others => '0');
	      slv_reg36 <= (others => '0');
	      slv_reg37 <= (others => '0');
	      slv_reg38 <= (others => '0');
	      slv_reg39 <= (others => '0');
	      slv_reg40 <= (others => '0');
	      slv_reg41 <= (others => '0');
	      slv_reg42 <= (others => '0');
	      slv_reg43 <= (others => '0');
	      slv_reg44 <= (others => '0');
	      slv_reg45 <= (others => '0');
	      slv_reg46 <= (others => '0');
	      slv_reg47 <= (others => '0');
	      slv_reg48 <= (others => '0');
	      slv_reg49 <= (others => '0');
	      slv_reg50 <= (others => '0');
	      slv_reg51 <= (others => '0');
	      slv_reg52 <= (others => '0');
	      slv_reg53 <= (others => '0');
	      slv_reg54 <= (others => '0');
	      slv_reg55 <= (others => '0');
	      slv_reg56 <= (others => '0');
	      slv_reg57 <= (others => '0');
	      slv_reg58 <= (others => '0');
	      slv_reg59 <= (others => '0');
	      slv_reg60 <= (others => '0');
	      slv_reg61 <= (others => '0');
	      slv_reg62 <= (others => '0');
	      slv_reg63 <= (others => '0');
	      slv_reg64 <= (others => '0');
	      slv_reg65 <= (others => '0');
	      slv_reg66 <= (others => '0');
	      slv_reg67 <= (others => '0');
	      slv_reg68 <= (others => '0');
	      slv_reg69 <= (others => '0');
	      slv_reg70 <= (others => '0');
	      slv_reg71 <= (others => '0');
	      slv_reg72 <= (others => '0');
	      slv_reg73 <= (others => '0');
	      slv_reg74 <= (others => '0');
	      slv_reg75 <= (others => '0');
	      slv_reg76 <= (others => '0');
	      slv_reg77 <= (others => '0');
	      slv_reg78 <= (others => '0');
	      slv_reg79 <= (others => '0');
	      slv_reg80 <= (others => '0');
	      slv_reg81 <= (others => '0');
	      slv_reg82 <= (others => '0');
	      slv_reg83 <= (others => '0');
	      slv_reg84 <= (others => '0');
	      slv_reg85 <= (others => '0');
	      slv_reg86 <= (others => '0');
	      slv_reg87 <= (others => '0');
	      slv_reg88 <= (others => '0');
	      slv_reg89 <= (others => '0');
	      slv_reg90 <= (others => '0');
	      slv_reg91 <= (others => '0');
	      slv_reg92 <= (others => '0');
	      slv_reg93 <= (others => '0');
	      slv_reg94 <= (others => '0');
	      slv_reg95 <= (others => '0');
	      slv_reg96 <= (others => '0');
	      slv_reg97 <= (others => '0');
	      slv_reg98 <= (others => '0');
	      slv_reg99 <= (others => '0');
	      slv_reg100 <= (others => '0');
	      slv_reg101 <= (others => '0');
	      slv_reg102 <= (others => '0');
	      slv_reg103 <= (others => '0');
	      slv_reg104 <= (others => '0');
	      slv_reg105 <= (others => '0');
	      slv_reg106 <= (others => '0');
	      slv_reg107 <= (others => '0');
	      slv_reg108 <= (others => '0');
	      slv_reg109 <= (others => '0');
	      slv_reg110 <= (others => '0');
	      slv_reg111 <= (others => '0');
	      slv_reg112 <= (others => '0');
	      slv_reg113 <= (others => '0');
	      slv_reg114 <= (others => '0');
	      slv_reg115 <= (others => '0');
	      slv_reg116 <= (others => '0');
	      slv_reg117 <= (others => '0');
	      slv_reg118 <= (others => '0');
	      slv_reg119 <= (others => '0');
	      slv_reg120 <= (others => '0');
	      slv_reg121 <= (others => '0');
	      slv_reg122 <= (others => '0');
	      slv_reg123 <= (others => '0');
	      slv_reg124 <= (others => '0');
	      slv_reg125 <= (others => '0');
	      slv_reg126 <= (others => '0');
	      slv_reg127 <= (others => '0');
	      slv_reg128 <= (others => '0');
	      slv_reg129 <= (others => '0');
	      slv_reg130 <= (others => '0');
	      slv_reg131 <= (others => '0');
	      slv_reg132 <= (others => '0');
	      slv_reg133 <= (others => '0');
	      slv_reg134 <= (others => '0');
	      slv_reg135 <= (others => '0');
	      slv_reg136 <= (others => '0');
	      slv_reg137 <= (others => '0');
	      slv_reg138 <= (others => '0');
	      slv_reg139 <= (others => '0');
	      slv_reg140 <= (others => '0');
	      slv_reg141 <= (others => '0');
	      slv_reg142 <= (others => '0');
	      slv_reg143 <= (others => '0');
	      slv_reg144 <= (others => '0');
	      slv_reg145 <= (others => '0');
	      slv_reg146 <= (others => '0');
	      slv_reg147 <= (others => '0');
	      slv_reg148 <= (others => '0');
	      slv_reg149 <= (others => '0');
	      slv_reg150 <= (others => '0');
	      slv_reg151 <= (others => '0');
	      slv_reg152 <= (others => '0');
	      slv_reg153 <= (others => '0');
	      slv_reg154 <= (others => '0');
	      slv_reg155 <= (others => '0');
	      slv_reg156 <= (others => '0');
	      slv_reg157 <= (others => '0');
	      slv_reg158 <= (others => '0');
	      slv_reg159 <= (others => '0');
	      slv_reg160 <= (others => '0');
	      slv_reg161 <= (others => '0');
	      slv_reg162 <= (others => '0');
	      slv_reg163 <= (others => '0');
	      slv_reg164 <= (others => '0');
	      slv_reg165 <= (others => '0');
	      slv_reg166 <= (others => '0');
	      slv_reg167 <= (others => '0');
	      slv_reg168 <= (others => '0');
	      slv_reg169 <= (others => '0');
	      slv_reg170 <= (others => '0');
	      slv_reg171 <= (others => '0');
	      slv_reg172 <= (others => '0');
	      slv_reg173 <= (others => '0');
	      slv_reg174 <= (others => '0');
	      slv_reg175 <= (others => '0');
	      slv_reg176 <= (others => '0');
	      slv_reg177 <= (others => '0');
	      slv_reg178 <= (others => '0');
	      slv_reg179 <= (others => '0');
	      slv_reg180 <= (others => '0');
	      slv_reg181 <= (others => '0');
	      slv_reg182 <= (others => '0');
	      slv_reg183 <= (others => '0');
	      slv_reg184 <= (others => '0');
	      slv_reg185 <= (others => '0');
	      slv_reg186 <= (others => '0');
	      slv_reg187 <= (others => '0');
	      slv_reg188 <= (others => '0');
	      slv_reg189 <= (others => '0');
	      slv_reg190 <= (others => '0');
	      slv_reg191 <= (others => '0');
	      slv_reg192 <= (others => '0');
	      slv_reg193 <= (others => '0');
	      slv_reg194 <= (others => '0');
	      slv_reg195 <= (others => '0');
	      slv_reg196 <= (others => '0');
	      slv_reg197 <= (others => '0');
	      slv_reg198 <= (others => '0');
	      slv_reg199 <= (others => '0');
	      slv_reg200 <= (others => '0');
	      slv_reg201 <= (others => '0');
	      slv_reg202 <= (others => '0');
	      slv_reg203 <= (others => '0');
	      slv_reg204 <= (others => '0');
	      slv_reg205 <= (others => '0');
	      slv_reg206 <= (others => '0');
	      slv_reg207 <= (others => '0');
	      slv_reg208 <= (others => '0');
	      slv_reg209 <= (others => '0');
	      slv_reg210 <= (others => '0');
	      slv_reg211 <= (others => '0');
	      slv_reg212 <= (others => '0');
	      slv_reg213 <= (others => '0');
	      slv_reg214 <= (others => '0');
	      slv_reg215 <= (others => '0');
	      slv_reg216 <= (others => '0');
	      slv_reg217 <= (others => '0');
	      slv_reg218 <= (others => '0');
	      slv_reg219 <= (others => '0');
	      slv_reg220 <= (others => '0');
	      slv_reg221 <= (others => '0');
	      slv_reg222 <= (others => '0');
	      slv_reg223 <= (others => '0');
	      slv_reg224 <= (others => '0');
	      slv_reg225 <= (others => '0');
	      slv_reg226 <= (others => '0');
	      slv_reg227 <= (others => '0');
	      slv_reg228 <= (others => '0');
	      slv_reg229 <= (others => '0');
	      slv_reg230 <= (others => '0');
	      slv_reg231 <= (others => '0');
	      slv_reg232 <= (others => '0');
	      slv_reg233 <= (others => '0');
	      slv_reg234 <= (others => '0');
	      slv_reg235 <= (others => '0');
	      slv_reg236 <= (others => '0');
	      slv_reg237 <= (others => '0');
	      slv_reg238 <= (others => '0');
	      slv_reg239 <= (others => '0');
	      slv_reg240 <= (others => '0');
	      slv_reg241 <= (others => '0');
	      slv_reg242 <= (others => '0');
	      slv_reg243 <= (others => '0');
	      slv_reg244 <= (others => '0');
	      slv_reg245 <= (others => '0');
	      slv_reg246 <= (others => '0');
	      slv_reg247 <= (others => '0');
	      slv_reg248 <= (others => '0');
	      slv_reg249 <= (others => '0');
	      slv_reg250 <= (others => '0');
	      slv_reg251 <= (others => '0');
	      slv_reg252 <= (others => '0');
	      slv_reg253 <= (others => '0');
	      slv_reg254 <= (others => '0');
	      slv_reg255 <= (others => '0');
	      slv_reg256 <= (others => '0');
	      slv_reg257 <= (others => '0');
	      slv_reg258 <= (others => '0');
	      slv_reg259 <= (others => '0');
	      slv_reg260 <= (others => '0');
	      slv_reg261 <= (others => '0');
	      slv_reg262 <= (others => '0');
	      slv_reg263 <= (others => '0');
	      slv_reg264 <= (others => '0');
	      slv_reg265 <= (others => '0');
	      slv_reg266 <= (others => '0');
	      slv_reg267 <= (others => '0');
	      slv_reg268 <= (others => '0');
	      slv_reg269 <= (others => '0');
	      slv_reg270 <= (others => '0');
	      slv_reg271 <= (others => '0');
	      slv_reg272 <= (others => '0');
	      slv_reg273 <= (others => '0');
	      slv_reg274 <= (others => '0');
	      slv_reg275 <= (others => '0');
	      slv_reg276 <= (others => '0');
	      slv_reg277 <= (others => '0');
	      slv_reg278 <= (others => '0');
	      slv_reg279 <= (others => '0');
	      slv_reg280 <= (others => '0');
	      slv_reg281 <= (others => '0');
	      slv_reg282 <= (others => '0');
	      slv_reg283 <= (others => '0');
	      slv_reg284 <= (others => '0');
	      slv_reg285 <= (others => '0');
	      slv_reg286 <= (others => '0');
	      slv_reg287 <= (others => '0');
	      slv_reg288 <= (others => '0');
	      slv_reg289 <= (others => '0');
	      slv_reg290 <= (others => '0');
	      slv_reg291 <= (others => '0');
	      slv_reg292 <= (others => '0');
	      slv_reg293 <= (others => '0');
	      slv_reg294 <= (others => '0');
	      slv_reg295 <= (others => '0');
	      slv_reg296 <= (others => '0');
	      slv_reg297 <= (others => '0');
	      slv_reg298 <= (others => '0');
	      slv_reg299 <= (others => '0');
	      slv_reg300 <= (others => '0');
	      slv_reg301 <= (others => '0');
	      slv_reg302 <= (others => '0');
	      slv_reg303 <= (others => '0');
	      slv_reg304 <= (others => '0');
	      slv_reg305 <= (others => '0');
	      slv_reg306 <= (others => '0');
	      slv_reg307 <= (others => '0');
	      slv_reg308 <= (others => '0');
	      slv_reg309 <= (others => '0');
	      slv_reg310 <= (others => '0');
	      slv_reg311 <= (others => '0');
	      slv_reg312 <= (others => '0');
	      slv_reg313 <= (others => '0');
	      slv_reg314 <= (others => '0');
	      slv_reg315 <= (others => '0');
	      slv_reg316 <= (others => '0');
	      slv_reg317 <= (others => '0');
	      slv_reg318 <= (others => '0');
	      slv_reg319 <= (others => '0');
	      slv_reg320 <= (others => '0');
	      slv_reg321 <= (others => '0');
	      slv_reg322 <= (others => '0');
	      slv_reg323 <= (others => '0');
	      slv_reg324 <= (others => '0');
	      slv_reg325 <= (others => '0');
	      slv_reg326 <= (others => '0');
	      slv_reg327 <= (others => '0');
	      slv_reg328 <= (others => '0');
	      slv_reg329 <= (others => '0');
	      slv_reg330 <= (others => '0');
	      slv_reg331 <= (others => '0');
	      slv_reg332 <= (others => '0');
	      slv_reg333 <= (others => '0');
	      slv_reg334 <= (others => '0');
	      slv_reg335 <= (others => '0');
	      slv_reg336 <= (others => '0');
	      slv_reg337 <= (others => '0');
	      slv_reg338 <= (others => '0');
	      slv_reg339 <= (others => '0');
	      slv_reg340 <= (others => '0');
	      slv_reg341 <= (others => '0');
	      slv_reg342 <= (others => '0');
	      slv_reg343 <= (others => '0');
	      slv_reg344 <= (others => '0');
	      slv_reg345 <= (others => '0');
	      slv_reg346 <= (others => '0');
	      slv_reg347 <= (others => '0');
	      slv_reg348 <= (others => '0');
	      slv_reg349 <= (others => '0');
	      slv_reg350 <= (others => '0');
	      slv_reg351 <= (others => '0');
	      slv_reg352 <= (others => '0');
	      slv_reg353 <= (others => '0');
	      slv_reg354 <= (others => '0');
	      slv_reg355 <= (others => '0');
	      slv_reg356 <= (others => '0');
	      slv_reg357 <= (others => '0');
	      slv_reg358 <= (others => '0');
	      slv_reg359 <= (others => '0');
	      slv_reg360 <= (others => '0');
	      slv_reg361 <= (others => '0');
	      slv_reg362 <= (others => '0');
	      slv_reg363 <= (others => '0');
	      slv_reg364 <= (others => '0');
	      slv_reg365 <= (others => '0');
	      slv_reg366 <= (others => '0');
	      slv_reg367 <= (others => '0');
	      slv_reg368 <= (others => '0');
	      slv_reg369 <= (others => '0');
	      slv_reg370 <= (others => '0');
	      slv_reg371 <= (others => '0');
	      slv_reg372 <= (others => '0');
	      slv_reg373 <= (others => '0');
	      slv_reg374 <= (others => '0');
	      slv_reg375 <= (others => '0');
	      slv_reg376 <= (others => '0');
	      slv_reg377 <= (others => '0');
	      slv_reg378 <= (others => '0');
	      slv_reg379 <= (others => '0');
	      slv_reg380 <= (others => '0');
	      slv_reg381 <= (others => '0');
	      slv_reg382 <= (others => '0');
	      slv_reg383 <= (others => '0');
	      slv_reg384 <= (others => '0');
	      slv_reg385 <= (others => '0');
	      slv_reg386 <= (others => '0');
	      slv_reg387 <= (others => '0');
	      slv_reg388 <= (others => '0');
	      slv_reg389 <= (others => '0');
	      slv_reg390 <= (others => '0');
	      slv_reg391 <= (others => '0');
	      slv_reg392 <= (others => '0');
	      slv_reg393 <= (others => '0');
	      slv_reg394 <= (others => '0');
	      slv_reg395 <= (others => '0');
	      slv_reg396 <= (others => '0');
	      slv_reg397 <= (others => '0');
	      slv_reg398 <= (others => '0');
	      slv_reg399 <= (others => '0');
	      slv_reg400 <= (others => '0');
	      slv_reg401 <= (others => '0');
	      slv_reg402 <= (others => '0');
	      slv_reg403 <= (others => '0');
	      slv_reg404 <= (others => '0');
	      slv_reg405 <= (others => '0');
	      slv_reg406 <= (others => '0');
	      slv_reg407 <= (others => '0');
	      slv_reg408 <= (others => '0');
	      slv_reg409 <= (others => '0');
	      slv_reg410 <= (others => '0');
	      slv_reg411 <= (others => '0');
	      slv_reg412 <= (others => '0');
	      slv_reg413 <= (others => '0');
	      slv_reg414 <= (others => '0');
	      slv_reg415 <= (others => '0');
	      slv_reg416 <= (others => '0');
	      slv_reg417 <= (others => '0');
	      slv_reg418 <= (others => '0');
	      slv_reg419 <= (others => '0');
	      slv_reg420 <= (others => '0');
	      slv_reg421 <= (others => '0');
	      slv_reg422 <= (others => '0');
	      slv_reg423 <= (others => '0');
	      slv_reg424 <= (others => '0');
	      slv_reg425 <= (others => '0');
	      slv_reg426 <= (others => '0');
	      slv_reg427 <= (others => '0');
	      slv_reg428 <= (others => '0');
	      slv_reg429 <= (others => '0');
	      slv_reg430 <= (others => '0');
	      slv_reg431 <= (others => '0');
	      slv_reg432 <= (others => '0');
	      slv_reg433 <= (others => '0');
	      slv_reg434 <= (others => '0');
	      slv_reg435 <= (others => '0');
	      slv_reg436 <= (others => '0');
	      slv_reg437 <= (others => '0');
	      slv_reg438 <= (others => '0');
	      slv_reg439 <= (others => '0');
	      slv_reg440 <= (others => '0');
	      slv_reg441 <= (others => '0');
	      slv_reg442 <= (others => '0');
	      slv_reg443 <= (others => '0');
	      slv_reg444 <= (others => '0');
	      slv_reg445 <= (others => '0');
	      slv_reg446 <= (others => '0');
	      slv_reg447 <= (others => '0');
	      slv_reg448 <= (others => '0');
	      slv_reg449 <= (others => '0');
	      slv_reg450 <= (others => '0');
	      slv_reg451 <= (others => '0');
	      slv_reg452 <= (others => '0');
	      slv_reg453 <= (others => '0');
	      slv_reg454 <= (others => '0');
	      slv_reg455 <= (others => '0');
	      slv_reg456 <= (others => '0');
	      slv_reg457 <= (others => '0');
	      slv_reg458 <= (others => '0');
	      slv_reg459 <= (others => '0');
	      slv_reg460 <= (others => '0');
	      slv_reg461 <= (others => '0');
	      slv_reg462 <= (others => '0');
	      slv_reg463 <= (others => '0');
	      slv_reg464 <= (others => '0');
	      slv_reg465 <= (others => '0');
	      slv_reg466 <= (others => '0');
	      slv_reg467 <= (others => '0');
	      slv_reg468 <= (others => '0');
	      slv_reg469 <= (others => '0');
	      slv_reg470 <= (others => '0');
	      slv_reg471 <= (others => '0');
	      slv_reg472 <= (others => '0');
	      slv_reg473 <= (others => '0');
	      slv_reg474 <= (others => '0');
	      slv_reg475 <= (others => '0');
	      slv_reg476 <= (others => '0');
	      slv_reg477 <= (others => '0');
	      slv_reg478 <= (others => '0');
	      slv_reg479 <= (others => '0');
	      slv_reg480 <= (others => '0');
	      slv_reg481 <= (others => '0');
	      slv_reg482 <= (others => '0');
	      slv_reg483 <= (others => '0');
	      slv_reg484 <= (others => '0');
	      slv_reg485 <= (others => '0');
	      slv_reg486 <= (others => '0');
	      slv_reg487 <= (others => '0');
	      slv_reg488 <= (others => '0');
	      slv_reg489 <= (others => '0');
	      slv_reg490 <= (others => '0');
	      slv_reg491 <= (others => '0');
	      slv_reg492 <= (others => '0');
	      slv_reg493 <= (others => '0');
	      slv_reg494 <= (others => '0');
	      slv_reg495 <= (others => '0');
	      slv_reg496 <= (others => '0');
	      slv_reg497 <= (others => '0');
	      slv_reg498 <= (others => '0');
	      slv_reg499 <= (others => '0');
	      slv_reg500 <= (others => '0');
	      slv_reg501 <= (others => '0');
	      slv_reg502 <= (others => '0');
	      slv_reg503 <= (others => '0');
	      slv_reg504 <= (others => '0');
	      slv_reg505 <= (others => '0');
	      slv_reg506 <= (others => '0');
	      slv_reg507 <= (others => '0');
	      slv_reg508 <= (others => '0');
	      slv_reg509 <= (others => '0');
	      slv_reg510 <= (others => '0');
	      slv_reg511 <= (others => '0');
	    else
	     -- loc_addr := axi_awaddr(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB);      
    
    
         -- ********************************************************************************************************************************************
         -- ********************************************************************************************************************************************
         -- *************************************               Registers  assignement                   ***********************************************
         -- ********************************************************************************************************************************************
         -- ********************************************************************************************************************************************
          
          
          
          -- *******************************************************************************
          --  No "write" for FIFO signals
          -- *******************************************************************************
          
	      -- FIFO TMC = [ 0x00000000 ; ... ; 0x00000005 ]
	      slv_reg0 <= "0000000000000000000000000000000" & fifo_tmc_empty;
	      slv_reg1 <= "0000000000000000000000000000000" & fifo_tmc_almost_empty;
	      slv_reg2 <= "0000000000000000000000000000000" & fifo_tmc_full;
          slv_reg3 <= "0000000000000000000000000000000" & fifo_tmc_almost_full; 	      
          slv_reg4 <= "0000000000000000000000000000000" & fifo_tmc_write_ack;
          slv_reg5 <= "0000000000000000000000000000000" & fifo_tmc_read_ack;
          
          -- FIFO Monitor to Kernel = [ 0x00000006 ; ... ; 0x0000000B ]        
          slv_reg6 <= "0000000000000000000000000000000" & fifo_monitor_to_kernel_empty;
          slv_reg7 <= "0000000000000000000000000000000" & fifo_monitor_to_kernel_almost_empty;
     	  slv_reg8 <= "0000000000000000000000000000000" & fifo_monitor_to_kernel_full;
          slv_reg9 <= "0000000000000000000000000000000" & fifo_monitor_to_kernel_almost_full;
          slv_reg10 <= "0000000000000000000000000000000" & fifo_monitor_to_kernel_write_ack;
          slv_reg11 <= "0000000000000000000000000000000" & fifo_monitor_to_kernel_read_ack;     
          
          -- FIFO Kernel to Monitor = [ 0x0000000C ; ... ; 0x00000011 ]      
          slv_reg12 <= "0000000000000000000000000000000" & fifo_kernel_to_monitor_empty;
          slv_reg13 <= "0000000000000000000000000000000" & fifo_kernel_to_monitor_almost_empty;
          slv_reg14 <= "0000000000000000000000000000000" & fifo_kernel_to_monitor_full;
          slv_reg15 <= "0000000000000000000000000000000" & fifo_kernel_to_monitor_almost_full;     
          slv_reg16 <= "0000000000000000000000000000000" & fifo_kernel_to_monitor_write_ack;
          slv_reg17 <= "0000000000000000000000000000000" & fifo_kernel_to_monitor_read_ack;
          
          -- FIFO PTM = [ 0x00000012 ; ... ; 0x00000017 ]
          slv_reg18 <= "0000000000000000000000000000000" & fifo_ptm_empty;
          slv_reg19 <= "0000000000000000000000000000000" & fifo_ptm_almost_empty;           
          slv_reg20 <= "0000000000000000000000000000000" & fifo_ptm_full;
          slv_reg21 <= "0000000000000000000000000000000" & fifo_ptm_almost_full;
          slv_reg22 <= "0000000000000000000000000000000" & fifo_ptm_write_ack;
          slv_reg23 <= "0000000000000000000000000000000" & fifo_ptm_read_ack;
          
          -- FIFO instrumentation = [ 0x00000018 ; ... ; 0x0000001D ]	 	
          slv_reg24 <= "0000000000000000000000000000000" & fifo_instrumentation_empty;
          slv_reg25 <= "0000000000000000000000000000000" & fifo_instrumentation_almost_empty;
          slv_reg26 <= "0000000000000000000000000000000" & fifo_instrumentation_full;
          slv_reg27 <= "0000000000000000000000000000000" & fifo_instrumentation_almost_full;           
          slv_reg28 <= "0000000000000000000000000000000" & fifo_instrumentation_write_ack;
          slv_reg29 <= "0000000000000000000000000000000" & fifo_instrumentation_read_ack;

        --	***********************************************************************************************************
        --	    Dispatcher    = [ 0x0000001E ; ... ; 0x000000F0 ]
        --	***********************************************************************************************************
    
        case dispatcher_address( 8 downto 0) is
            when b"000011110" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg30 <= dispatcher_write_data_in;
            end if;
            when b"000011111" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg31 <= dispatcher_write_data_in;
            end if;
            when b"000100000" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg32 <= dispatcher_write_data_in;
            end if;
            when b"000100001" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg33 <= dispatcher_write_data_in;
            end if;
            when b"000100010" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg34 <= dispatcher_write_data_in;
            end if;
            when b"000100011" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg35 <= dispatcher_write_data_in;
            end if;
            when b"000100100" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg36 <= dispatcher_write_data_in;
            end if;
            when b"000100101" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg37 <= dispatcher_write_data_in;
            end if;
            when b"000100110" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg38 <= dispatcher_write_data_in;
            end if;
            when b"000100111" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg39 <= dispatcher_write_data_in;
            end if;
            when b"000101000" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg40 <= dispatcher_write_data_in;
            end if;
            when b"000101001" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg41 <= dispatcher_write_data_in;
            end if;
            when b"000101010" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg42 <= dispatcher_write_data_in;
            end if;
            when b"000101011" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg43 <= dispatcher_write_data_in;
            end if;
            when b"000101100" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg44 <= dispatcher_write_data_in;
            end if;
            when b"000101101" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg45 <= dispatcher_write_data_in;
            end if;
            when b"000101110" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg46 <= dispatcher_write_data_in;
            end if;
            when b"000101111" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg47 <= dispatcher_write_data_in;
            end if;
            when b"000110000" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg48 <= dispatcher_write_data_in;
            end if;
            when b"000110001" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg49 <= dispatcher_write_data_in;
            end if;
            when b"000110010" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg50 <= dispatcher_write_data_in;
            end if;
            when b"000110011" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg51 <= dispatcher_write_data_in;
            end if;
            when b"000110100" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg52 <= dispatcher_write_data_in;
            end if;
            when b"000110101" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg53 <= dispatcher_write_data_in;
            end if;
            when b"000110110" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg54 <= dispatcher_write_data_in;
            end if;
            when b"000110111" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg55 <= dispatcher_write_data_in;
            end if;
            
            when b"000111000" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg56 <= dispatcher_write_data_in;
            end if;
            when b"000111001" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg57 <= dispatcher_write_data_in;
            end if;
            when b"000111010" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg58 <= dispatcher_write_data_in;
            end if;
            when b"000111011" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg59 <= dispatcher_write_data_in;
            end if;
            when b"000111100" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg60 <= dispatcher_write_data_in;
            end if;
            when b"000111101" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg61 <= dispatcher_write_data_in;
            end if;
            when b"000111110" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg62 <= dispatcher_write_data_in;
            end if;
            when b"000111111" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg63 <= dispatcher_write_data_in;
            end if;
            when b"001000000" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg64 <= dispatcher_write_data_in;
            end if;
            when b"001000001" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg65 <= dispatcher_write_data_in;
            end if;
            when b"001000010" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg66 <= dispatcher_write_data_in;
            end if;
            when b"001000011" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg67 <= dispatcher_write_data_in;
            end if;
            when b"001000100" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg68 <= dispatcher_write_data_in;
            end if;
            when b"001000101" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg69 <= dispatcher_write_data_in;
            end if;
            when b"001000110" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg70 <= dispatcher_write_data_in;
            end if;
            when b"001000111" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg71 <= dispatcher_write_data_in;
            end if;
            when b"001001000" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg72 <= dispatcher_write_data_in;
            end if;
            when b"001001001" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg73 <= dispatcher_write_data_in;
            end if;
            when b"001001010" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg74 <= dispatcher_write_data_in;
            end if;
            when b"001001011" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg75 <= dispatcher_write_data_in;
            end if;
            when b"001001100" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg76 <= dispatcher_write_data_in;
            end if;
            when b"001001101" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg77 <= dispatcher_write_data_in;
            end if;
            when b"001001110" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg78 <= dispatcher_write_data_in;
            end if;
            when b"001001111" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg79 <= dispatcher_write_data_in;
            end if;
            when b"001010000" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg80 <= dispatcher_write_data_in;
            end if;
            when b"001010001" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg81 <= dispatcher_write_data_in;
            end if;
            when b"001010010" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg82 <= dispatcher_write_data_in;
            end if;
            when b"001010011" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg83 <= dispatcher_write_data_in;
            end if;
            when b"001010100" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg84 <= dispatcher_write_data_in;
            end if;
            when b"001010101" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg85 <= dispatcher_write_data_in;
            end if;
            when b"001010110" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg86 <= dispatcher_write_data_in;
            end if;
            when b"001010111" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg87 <= dispatcher_write_data_in;
            end if;
            when b"001011000" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg88 <= dispatcher_write_data_in;
            end if;
            when b"001011001" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg89 <= dispatcher_write_data_in;
            end if;
            when b"001011010" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg90 <= dispatcher_write_data_in;
            end if;
            when b"001011011" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg91 <= dispatcher_write_data_in;
            end if;
            when b"001011100" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg92 <= dispatcher_write_data_in;
            end if;
            when b"001011101" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg93 <= dispatcher_write_data_in;
            end if;
            when b"001011110" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg94 <= dispatcher_write_data_in;
            end if;
            when b"001011111" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg95 <= dispatcher_write_data_in;
            end if;
            when b"001100000" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg96 <= dispatcher_write_data_in;
            end if;
            when b"001100001" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg97 <= dispatcher_write_data_in;
            end if;
            when b"001100010" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg98 <= dispatcher_write_data_in;
            end if;
            when b"001100011" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg99 <= dispatcher_write_data_in;
            end if;
            when b"001100100" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg100 <= dispatcher_write_data_in;
            end if;
            when b"001100101" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg101 <= dispatcher_write_data_in;
            end if;
            when b"001100110" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg102 <= dispatcher_write_data_in;
            end if;
            when b"001100111" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg103 <= dispatcher_write_data_in;
            end if;
            when b"001101000" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg104 <= dispatcher_write_data_in;
            end if;
            when b"001101001" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg105 <= dispatcher_write_data_in;
            end if;
            when b"001101010" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg106 <= dispatcher_write_data_in;
            end if;
            when b"001101011" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg107 <= dispatcher_write_data_in;
            end if;
            when b"001101100" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg108 <= dispatcher_write_data_in;
            end if;
            when b"001101101" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg109 <= dispatcher_write_data_in;
            end if;
            when b"001101110" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg110 <= dispatcher_write_data_in;
            end if;
            when b"001101111" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg111 <= dispatcher_write_data_in;
            end if;
            when b"001110000" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg112 <= dispatcher_write_data_in;
            end if;
            when b"001110001" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg113 <= dispatcher_write_data_in;
            end if;
            when b"001110010" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg114 <= dispatcher_write_data_in;
            end if;
            when b"001110011" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg115 <= dispatcher_write_data_in;
            end if;
            when b"001110100" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg116 <= dispatcher_write_data_in;
            end if;
            when b"001110101" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg117 <= dispatcher_write_data_in;
            end if;
            when b"001110110" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg118 <= dispatcher_write_data_in;
            end if;
            when b"001110111" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg119 <= dispatcher_write_data_in;
            end if;
            when b"001111000" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg120 <= dispatcher_write_data_in;
            end if;
            when b"001111001" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg121 <= dispatcher_write_data_in;
            end if;
            when b"001111010" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg122 <= dispatcher_write_data_in;
            end if;
            when b"001111011" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg123 <= dispatcher_write_data_in;
            end if;
            when b"001111100" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg124 <= dispatcher_write_data_in;
            end if;
            when b"001111101" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg125 <= dispatcher_write_data_in;
            end if;
            when b"001111110" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg126 <= dispatcher_write_data_in;
            end if;
            when b"001111111" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg127 <= dispatcher_write_data_in;
            end if;
            when b"010000000" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg128 <= dispatcher_write_data_in;
            end if;
            when b"010000001" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg129 <= dispatcher_write_data_in;
            end if;
            when b"010000010" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg130 <= dispatcher_write_data_in;
            end if;
            when b"010000011" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg131 <= dispatcher_write_data_in;
            end if;
            when b"010000100" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg132 <= dispatcher_write_data_in;
            end if;
            when b"010000101" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg133 <= dispatcher_write_data_in;
            end if;
            when b"010000110" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg134 <= dispatcher_write_data_in;
            end if;
            when b"010000111" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg135 <= dispatcher_write_data_in;
            end if;
            when b"010001000" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg136 <= dispatcher_write_data_in;
            end if;
            when b"010001001" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg137 <= dispatcher_write_data_in;
            end if;
            when b"010001010" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg138 <= dispatcher_write_data_in;
            end if;
            when b"010001011" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg139 <= dispatcher_write_data_in;
            end if;
            when b"010001100" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg140 <= dispatcher_write_data_in;
            end if;
            when b"010001101" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg141 <= dispatcher_write_data_in;
            end if;
            when b"010001110" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg142 <= dispatcher_write_data_in;
            end if;
            when b"010001111" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg143 <= dispatcher_write_data_in;
            end if;
            when b"010010000" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg144 <= dispatcher_write_data_in;
            end if;
            when b"010010001" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg145 <= dispatcher_write_data_in;
            end if;
            when b"010010010" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg146 <= dispatcher_write_data_in;
            end if;
            when b"010010011" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg147 <= dispatcher_write_data_in;
            end if;
            when b"010010100" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg148 <= dispatcher_write_data_in;
            end if;
            when b"010010101" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg149 <= dispatcher_write_data_in;
            end if;
            when b"010010110" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg150 <= dispatcher_write_data_in;
            end if;
            when b"010010111" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg151 <= dispatcher_write_data_in;
            end if;
            when b"010011000" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg152 <= dispatcher_write_data_in;
            end if;
            when b"010011001" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg153 <= dispatcher_write_data_in;
            end if;
            when b"010011010" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg154 <= dispatcher_write_data_in;
            end if;
            when b"010011011" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg155 <= dispatcher_write_data_in;
            end if;
            when b"010011100" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg156 <= dispatcher_write_data_in;
            end if;
            when b"010011101" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg157 <= dispatcher_write_data_in;
            end if;
            when b"010011110" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg158 <= dispatcher_write_data_in;
            end if;
            when b"010011111" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg159 <= dispatcher_write_data_in;
            end if;
            when b"010100000" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg160 <= dispatcher_write_data_in;
            end if;
            when b"010100001" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg161 <= dispatcher_write_data_in;
            end if;
            when b"010100010" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg162 <= dispatcher_write_data_in;
            end if;
            when b"010100011" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg163 <= dispatcher_write_data_in;
            end if;
            when b"010100100" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg164 <= dispatcher_write_data_in;
            end if;
            when b"010100101" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg165 <= dispatcher_write_data_in;
            end if;
            when b"010100110" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg166 <= dispatcher_write_data_in;
            end if;
            when b"010100111" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg167 <= dispatcher_write_data_in;
            end if;
            when b"010101000" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg168 <= dispatcher_write_data_in;
            end if;
            when b"010101001" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg169 <= dispatcher_write_data_in;
            end if;
            when b"010101010" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg170 <= dispatcher_write_data_in;
            end if;
            when b"010101011" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg171 <= dispatcher_write_data_in;
            end if;
            when b"010101100" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg172 <= dispatcher_write_data_in;
            end if;
            when b"010101101" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg173 <= dispatcher_write_data_in;
            end if;
            when b"010101110" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg174 <= dispatcher_write_data_in;
            end if;
            when b"010101111" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg175 <= dispatcher_write_data_in;
            end if;
            when b"010110000" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg176 <= dispatcher_write_data_in;
            end if;
            when b"010110001" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg177 <= dispatcher_write_data_in;
            end if;
            when b"010110010" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg178 <= dispatcher_write_data_in;
            end if;
            when b"010110011" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg179 <= dispatcher_write_data_in;
            end if;
            when b"010110100" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg180 <= dispatcher_write_data_in;
            end if;
            when b"010110101" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg181 <= dispatcher_write_data_in;
            end if;
            when b"010110110" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg182 <= dispatcher_write_data_in;
            end if;
            when b"010110111" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg183 <= dispatcher_write_data_in;
            end if;
            when b"010111000" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg184 <= dispatcher_write_data_in;
            end if;
            when b"010111001" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg185 <= dispatcher_write_data_in;
            end if;
            when b"010111010" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg186 <= dispatcher_write_data_in;
            end if;
            when b"010111011" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg187 <= dispatcher_write_data_in;
            end if;
            when b"010111100" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg188 <= dispatcher_write_data_in;
            end if;
            when b"010111101" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg189 <= dispatcher_write_data_in;
            end if;
            when b"010111110" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg190 <= dispatcher_write_data_in;
            end if;
            when b"010111111" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg191 <= dispatcher_write_data_in;
            end if;
            when b"011000000" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg192 <= dispatcher_write_data_in;
            end if;
            when b"011000001" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg193 <= dispatcher_write_data_in;
            end if;
            when b"011000010" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg194 <= dispatcher_write_data_in;
            end if;
            when b"011000011" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg195 <= dispatcher_write_data_in;
            end if;
            when b"011000100" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg196 <= dispatcher_write_data_in;
            end if;
            when b"011000101" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg197 <= dispatcher_write_data_in;
            end if;
            when b"011000110" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg198 <= dispatcher_write_data_in;
            end if;
            when b"011000111" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg199 <= dispatcher_write_data_in;
            end if;
            when b"011001000" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg200 <= dispatcher_write_data_in;
            end if;
            when b"011001001" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg201 <= dispatcher_write_data_in;
            end if;
            when b"011001010" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg202 <= dispatcher_write_data_in;
            end if;
            when b"011001011" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg203 <= dispatcher_write_data_in;
            end if;
            when b"011001100" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg204 <= dispatcher_write_data_in;
            end if;
            when b"011001101" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg205 <= dispatcher_write_data_in;
            end if;
            when b"011001110" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg206 <= dispatcher_write_data_in;
            end if;
            when b"011001111" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg207 <= dispatcher_write_data_in;
            end if;
            when b"011010000" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg208 <= dispatcher_write_data_in;
            end if;
            when b"011010001" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg209 <= dispatcher_write_data_in;
            end if;
            when b"011010010" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg210 <= dispatcher_write_data_in;
            end if;
            when b"011010011" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg211 <= dispatcher_write_data_in;
            end if;
            when b"011010100" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg212 <= dispatcher_write_data_in;
            end if;
            when b"011010101" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg213 <= dispatcher_write_data_in;
            end if;
            when b"011010110" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg214 <= dispatcher_write_data_in;
            end if;
            when b"011010111" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg215 <= dispatcher_write_data_in;
            end if;
            when b"011011000" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg216 <= dispatcher_write_data_in;
            end if;
            when b"011011001" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg217 <= dispatcher_write_data_in;
            end if;
            when b"011011010" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg218 <= dispatcher_write_data_in;
            end if;
            when b"011011011" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg219 <= dispatcher_write_data_in;
            end if;
            when b"011011100" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg220 <= dispatcher_write_data_in;
            end if;
            when b"011011101" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg221 <= dispatcher_write_data_in;
            end if;
            when b"011011110" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg222 <= dispatcher_write_data_in;
            end if;
            when b"011011111" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg223 <= dispatcher_write_data_in;
            end if;
            when b"011100000" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg224 <= dispatcher_write_data_in;
            end if;
            when b"011100001" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg225 <= dispatcher_write_data_in;
            end if;
            when b"011100010" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg226 <= dispatcher_write_data_in;
            end if;
            when b"011100011" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg227 <= dispatcher_write_data_in;
            end if;
            when b"011100100" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg228 <= dispatcher_write_data_in;
            end if;
            when b"011100101" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg229 <= dispatcher_write_data_in;
            end if;
            when b"011100110" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg230 <= dispatcher_write_data_in;
            end if;
            when b"011100111" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg231 <= dispatcher_write_data_in;
            end if;
            when b"011101000" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg232 <= dispatcher_write_data_in;
            end if;
            when b"011101001" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg233 <= dispatcher_write_data_in;
            end if;
            when b"011101010" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg234 <= dispatcher_write_data_in;
            end if;
            when b"011101011" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg235 <= dispatcher_write_data_in;
            end if;
            when b"011101100" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg236 <= dispatcher_write_data_in;
            end if;
            when b"011101101" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg237 <= dispatcher_write_data_in;
            end if;
            when b"011101110" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg238 <= dispatcher_write_data_in;
            end if;
            when b"011101111" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg239 <= dispatcher_write_data_in;
            end if;
            when b"011110000" =>
            if ( dispatcher_write_enable = '1' ) then
              slv_reg240 <= dispatcher_write_data_in;
            end if;
            when others => 
        end case;


    -- **********************************************************************************************************
    --	  TMC write  = [ 0x000000F1 ; ... ; 0x000001FF ]
    -- *********************************************************************************************************

        case tmc_address( 8 downto 0) is
            when b"011110001" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg241 <= tmc_write_data_in;
            end if;
            when b"011110010" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg242 <= tmc_write_data_in;
            end if;
            when b"011110011" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg243 <= tmc_write_data_in;
            end if;
            when b"011110100" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg244 <= tmc_write_data_in;
            end if;
            when b"011110101" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg245 <= tmc_write_data_in;
            end if;
            when b"011110110" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg246 <= tmc_write_data_in;
            end if;
            when b"011110111" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg247 <= tmc_write_data_in;
            end if;
            when b"011111000" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg248 <= tmc_write_data_in;
            end if;
            when b"011111001" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg249 <= tmc_write_data_in;
            end if;
            when b"011111010" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg250 <= tmc_write_data_in;
            end if;
            when b"011111011" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg251 <= tmc_write_data_in;
            end if;
            when b"011111100" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg252 <= tmc_write_data_in;
            end if;
            when b"011111101" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg253 <= tmc_write_data_in;
            end if;
            when b"011111110" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg254 <= tmc_write_data_in;
            end if;
            when b"011111111" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg255 <= tmc_write_data_in;
            end if;
            when b"100000000" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg256 <= tmc_write_data_in;
            end if;
            when b"100000001" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg257 <= tmc_write_data_in;
            end if;
            when b"100000010" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg258 <= tmc_write_data_in;
            end if;
            when b"100000011" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg259 <= tmc_write_data_in;
            end if;
            when b"100000100" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg260 <= tmc_write_data_in;
            end if;
            when b"100000101" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg261 <= tmc_write_data_in;
            end if;
            when b"100000110" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg262 <= tmc_write_data_in;
            end if;
            when b"100000111" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg263 <= tmc_write_data_in;
            end if;
            when b"100001000" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg264 <= tmc_write_data_in;
            end if;
            when b"100001001" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg265 <= tmc_write_data_in;
            end if;
            when b"100001010" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg266 <= tmc_write_data_in;
            end if;
            when b"100001011" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg267 <= tmc_write_data_in;
            end if;
            when b"100001100" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg268 <= tmc_write_data_in;
            end if;
            when b"100001101" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg269 <= tmc_write_data_in;
            end if;
            when b"100001110" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg270 <= tmc_write_data_in;
            end if;
            when b"100001111" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg271 <= tmc_write_data_in;
            end if;
            when b"100010000" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg272 <= tmc_write_data_in;
            end if;
            when b"100010001" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg273 <= tmc_write_data_in;
            end if;
            when b"100010010" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg274 <= tmc_write_data_in;
            end if;
            when b"100010011" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg275 <= tmc_write_data_in;
            end if;
            when b"100010100" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg276 <= tmc_write_data_in;
            end if;
            when b"100010101" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg277 <= tmc_write_data_in;
            end if;
            when b"100010110" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg278 <= tmc_write_data_in;
            end if;
            when b"100010111" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg279 <= tmc_write_data_in;
            end if;
            when b"100011000" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg280 <= tmc_write_data_in;
            end if;
            when b"100011001" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg281 <= tmc_write_data_in;
            end if;
            when b"100011010" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg282 <= tmc_write_data_in;
            end if;
            when b"100011011" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg283 <= tmc_write_data_in;
            end if;
            when b"100011100" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg284 <= tmc_write_data_in;
            end if;
            when b"100011101" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg285 <= tmc_write_data_in;
            end if;
            when b"100011110" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg286 <= tmc_write_data_in;
            end if;
            when b"100011111" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg287 <= tmc_write_data_in;
            end if;
            when b"100100000" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg288 <= tmc_write_data_in;
            end if;
            when b"100100001" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg289 <= tmc_write_data_in;
            end if;
            when b"100100010" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg290 <= tmc_write_data_in;
            end if;
            when b"100100011" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg291 <= tmc_write_data_in;
            end if;
            when b"100100100" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg292 <= tmc_write_data_in;
            end if;
            when b"100100101" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg293 <= tmc_write_data_in;
            end if;
            when b"100100110" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg294 <= tmc_write_data_in;
            end if;
            when b"100100111" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg295 <= tmc_write_data_in;
            end if;
            when b"100101000" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg296 <= tmc_write_data_in;
            end if;
            when b"100101001" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg297 <= tmc_write_data_in;
            end if;
            when b"100101010" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg298 <= tmc_write_data_in;
            end if;
            when b"100101011" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg299 <= tmc_write_data_in;
            end if;
            when b"100101100" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg300 <= tmc_write_data_in;
            end if;
            when b"100101101" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg301 <= tmc_write_data_in;
            end if;
            when b"100101110" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg302 <= tmc_write_data_in;
            end if;
            when b"100101111" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg303 <= tmc_write_data_in;
            end if;
            when b"100110000" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg304 <= tmc_write_data_in;
            end if;
            when b"100110001" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg305 <= tmc_write_data_in;
            end if;
            when b"100110010" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg306 <= tmc_write_data_in;
            end if;
            when b"100110011" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg307 <= tmc_write_data_in;
            end if;
            when b"100110100" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg308 <= tmc_write_data_in;
            end if;
            when b"100110101" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg309 <= tmc_write_data_in;
            end if;
            when b"100110110" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg310 <= tmc_write_data_in;
            end if;
            when b"100110111" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg311 <= tmc_write_data_in;
            end if;
            when b"100111000" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg312 <= tmc_write_data_in;
            end if;
            when b"100111001" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg313 <= tmc_write_data_in;
            end if;
            when b"100111010" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg314 <= tmc_write_data_in;
            end if;
            when b"100111011" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg315 <= tmc_write_data_in;
            end if;
            when b"100111100" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg316 <= tmc_write_data_in;
            end if;
            when b"100111101" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg317 <= tmc_write_data_in;
            end if;
            when b"100111110" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg318 <= tmc_write_data_in;
            end if;
            when b"100111111" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg319 <= tmc_write_data_in;
            end if;
            when b"101000000" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg320 <= tmc_write_data_in;
            end if;
            when b"101000001" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg321 <= tmc_write_data_in;
            end if;
            when b"101000010" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg322 <= tmc_write_data_in;
            end if;
            when b"101000011" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg323 <= tmc_write_data_in;
            end if;
            when b"101000100" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg324 <= tmc_write_data_in;
            end if;
            when b"101000101" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg325 <= tmc_write_data_in;
            end if;
            when b"101000110" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg326 <= tmc_write_data_in;
            end if;
            when b"101000111" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg327 <= tmc_write_data_in;
            end if;
            when b"101001000" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg328 <= tmc_write_data_in;
            end if;
            when b"101001001" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg329 <= tmc_write_data_in;
            end if;
            when b"101001010" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg330 <= tmc_write_data_in;
            end if;
            when b"101001011" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg331 <= tmc_write_data_in;
            end if;
            when b"101001100" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg332 <= tmc_write_data_in;
            end if;
            when b"101001101" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg333 <= tmc_write_data_in;
            end if;
            when b"101001110" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg334 <= tmc_write_data_in;
            end if;
            when b"101001111" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg335 <= tmc_write_data_in;
            end if;
            when b"101010000" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg336 <= tmc_write_data_in;
            end if;
            when b"101010001" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg337 <= tmc_write_data_in;
            end if;
            when b"101010010" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg338 <= tmc_write_data_in;
            end if;
            when b"101010011" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg339 <= tmc_write_data_in;
            end if;
            when b"101010100" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg340 <= tmc_write_data_in;
            end if;
            when b"101010101" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg341 <= tmc_write_data_in;
            end if;
            when b"101010110" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg342 <= tmc_write_data_in;
            end if;
            when b"101010111" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg343 <= tmc_write_data_in;
            end if;
            when b"101011000" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg344 <= tmc_write_data_in;
            end if;
            when b"101011001" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg345 <= tmc_write_data_in;
            end if;
            when b"101011010" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg346 <= tmc_write_data_in;
            end if;
            when b"101011011" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg347 <= tmc_write_data_in;
            end if;
            when b"101011100" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg348 <= tmc_write_data_in;
            end if;
            when b"101011101" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg349 <= tmc_write_data_in;
            end if;
            when b"101011110" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg350 <= tmc_write_data_in;
            end if;
            when b"101011111" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg351 <= tmc_write_data_in;
            end if;
            when b"101100000" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg352 <= tmc_write_data_in;
            end if;
            when b"101100001" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg353 <= tmc_write_data_in;
            end if;
            when b"101100010" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg354 <= tmc_write_data_in;
            end if;
            when b"101100011" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg355 <= tmc_write_data_in;
            end if;
            when b"101100100" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg356 <= tmc_write_data_in;
            end if;
            when b"101100101" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg357 <= tmc_write_data_in;
            end if;
            when b"101100110" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg358 <= tmc_write_data_in;
            end if;
            when b"101100111" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg359 <= tmc_write_data_in;
            end if;
            when b"101101000" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg360 <= tmc_write_data_in;
            end if;
            when b"101101001" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg361 <= tmc_write_data_in;
            end if;
            when b"101101010" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg362 <= tmc_write_data_in;
            end if;
            when b"101101011" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg363 <= tmc_write_data_in;
            end if;
            when b"101101100" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg364 <= tmc_write_data_in;
            end if;
            when b"101101101" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg365 <= tmc_write_data_in;
            end if;
            when b"101101110" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg366 <= tmc_write_data_in;
            end if;
            when b"101101111" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg367 <= tmc_write_data_in;
            end if;
            when b"101110000" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg368 <= tmc_write_data_in;
            end if;
            when b"101110001" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg369 <= tmc_write_data_in;
            end if;
            when b"101110010" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg370 <= tmc_write_data_in;
            end if;
            when b"101110011" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg371 <= tmc_write_data_in;
            end if;
            when b"101110100" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg372 <= tmc_write_data_in;
            end if;
            when b"101110101" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg373 <= tmc_write_data_in;
            end if;
            when b"101110110" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg374 <= tmc_write_data_in;
            end if;
            when b"101110111" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg375 <= tmc_write_data_in;
            end if;
            when b"101111000" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg376 <= tmc_write_data_in;
            end if;
            when b"101111001" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg377 <= tmc_write_data_in;
            end if;
            when b"101111010" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg378 <= tmc_write_data_in;
            end if;
            when b"101111011" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg379 <= tmc_write_data_in;
            end if;
            when b"101111100" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg380 <= tmc_write_data_in;
            end if;
            when b"101111101" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg381 <= tmc_write_data_in;
            end if;
            when b"101111110" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg382 <= tmc_write_data_in;
            end if;
            when b"101111111" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg383 <= tmc_write_data_in;
            end if;
            when b"110000000" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg384 <= tmc_write_data_in;
            end if;
            when b"110000001" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg385 <= tmc_write_data_in;
            end if;
            when b"110000010" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg386 <= tmc_write_data_in;
            end if;
            when b"110000011" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg387 <= tmc_write_data_in;
            end if;
            when b"110000100" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg388 <= tmc_write_data_in;
            end if;
            when b"110000101" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg389 <= tmc_write_data_in;
            end if;
            when b"110000110" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg390 <= tmc_write_data_in;
            end if;
            when b"110000111" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg391 <= tmc_write_data_in;
            end if;
            when b"110001000" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg392 <= tmc_write_data_in;
            end if;
            when b"110001001" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg393 <= tmc_write_data_in;
            end if;
            when b"110001010" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg394 <= tmc_write_data_in;
            end if;
            when b"110001011" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg395 <= tmc_write_data_in;
            end if;
            when b"110001100" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg396 <= tmc_write_data_in;
            end if;
            when b"110001101" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg397 <= tmc_write_data_in;
            end if;
            when b"110001110" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg398 <= tmc_write_data_in;
            end if;
            when b"110001111" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg399 <= tmc_write_data_in;
            end if;
            when b"110010000" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg400 <= tmc_write_data_in;
            end if;
            when b"110010001" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg401 <= tmc_write_data_in;
            end if;
            when b"110010010" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg402 <= tmc_write_data_in;
            end if;
            when b"110010011" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg403 <= tmc_write_data_in;
            end if;
            when b"110010100" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg404 <= tmc_write_data_in;
            end if;
            when b"110010101" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg405 <= tmc_write_data_in;
            end if;
            when b"110010110" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg406 <= tmc_write_data_in;
            end if;
            when b"110010111" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg407 <= tmc_write_data_in;
            end if;
            when b"110011000" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg408 <= tmc_write_data_in;
            end if;
            when b"110011001" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg409 <= tmc_write_data_in;
            end if;
            when b"110011010" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg410 <= tmc_write_data_in;
            end if;
            when b"110011011" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg411 <= tmc_write_data_in;
            end if;
            when b"110011100" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg412 <= tmc_write_data_in;
            end if;
            when b"110011101" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg413 <= tmc_write_data_in;
            end if;
            when b"110011110" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg414 <= tmc_write_data_in;
            end if;
            when b"110011111" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg415 <= tmc_write_data_in;
            end if;
            when b"110100000" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg416 <= tmc_write_data_in;
            end if;
            when b"110100001" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg417 <= tmc_write_data_in;
            end if;
            when b"110100010" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg418 <= tmc_write_data_in;
            end if;
            when b"110100011" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg419 <= tmc_write_data_in;
            end if;
            when b"110100100" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg420 <= tmc_write_data_in;
            end if;
            when b"110100101" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg421 <= tmc_write_data_in;
            end if;
            when b"110100110" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg422 <= tmc_write_data_in;
            end if;
            when b"110100111" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg423 <= tmc_write_data_in;
            end if;
            when b"110101000" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg424 <= tmc_write_data_in;
            end if;
            when b"110101001" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg425 <= tmc_write_data_in;
            end if;
            when b"110101010" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg426 <= tmc_write_data_in;
            end if;
            when b"110101011" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg427 <= tmc_write_data_in;
            end if;
            when b"110101100" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg428 <= tmc_write_data_in;
            end if;
            when b"110101101" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg429 <= tmc_write_data_in;
            end if;
            when b"110101110" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg430 <= tmc_write_data_in;
            end if;
            when b"110101111" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg431 <= tmc_write_data_in;
            end if;
            when b"110110000" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg432 <= tmc_write_data_in;
            end if;
            when b"110110001" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg433 <= tmc_write_data_in;
            end if;
            when b"110110010" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg434 <= tmc_write_data_in;
            end if;
            when b"110110011" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg435 <= tmc_write_data_in;
            end if;
            when b"110110100" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg436 <= tmc_write_data_in;
            end if;
            when b"110110101" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg437 <= tmc_write_data_in;
            end if;
            when b"110110110" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg438 <= tmc_write_data_in;
            end if;
            when b"110110111" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg439 <= tmc_write_data_in;
            end if;
            when b"110111000" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg440 <= tmc_write_data_in;
            end if;
            when b"110111001" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg441 <= tmc_write_data_in;
            end if;
            when b"110111010" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg442 <= tmc_write_data_in;
            end if;
            when b"110111011" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg443 <= tmc_write_data_in;
            end if;
            when b"110111100" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg444 <= tmc_write_data_in;
            end if;
            when b"110111101" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg445 <= tmc_write_data_in;
            end if;
            when b"110111110" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg446 <= tmc_write_data_in;
            end if;
            when b"110111111" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg447 <= tmc_write_data_in;
            end if;
            when b"111000000" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg448 <= tmc_write_data_in;
            end if;
            when b"111000001" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg449 <= tmc_write_data_in;
            end if;
            when b"111000010" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg450 <= tmc_write_data_in;
            end if;
            when b"111000011" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg451 <= tmc_write_data_in;
            end if;
            when b"111000100" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg452 <= tmc_write_data_in;
            end if;
            when b"111000101" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg453 <= tmc_write_data_in;
            end if;
            when b"111000110" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg454 <= tmc_write_data_in;
            end if;
            when b"111000111" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg455 <= tmc_write_data_in;
            end if;
            when b"111001000" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg456 <= tmc_write_data_in;
            end if;
            when b"111001001" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg457 <= tmc_write_data_in;
            end if;
            when b"111001010" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg458 <= tmc_write_data_in;
            end if;
            when b"111001011" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg459 <= tmc_write_data_in;
            end if;
            when b"111001100" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg460 <= tmc_write_data_in;
            end if;
            when b"111001101" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg461 <= tmc_write_data_in;
            end if;
            when b"111001110" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg462 <= tmc_write_data_in;
            end if;
            when b"111001111" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg463 <= tmc_write_data_in;
            end if;
            when b"111010000" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg464 <= tmc_write_data_in;
            end if;
            when b"111010001" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg465 <= tmc_write_data_in;
            end if;
            when b"111010010" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg466 <= tmc_write_data_in;
            end if;
            when b"111010011" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg467 <= tmc_write_data_in;
            end if;
            when b"111010100" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg468 <= tmc_write_data_in;
            end if;
            when b"111010101" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg469 <= tmc_write_data_in;
            end if;
            when b"111010110" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg470 <= tmc_write_data_in;
            end if;
            when b"111010111" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg471 <= tmc_write_data_in;
            end if;
            when b"111011000" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg472 <= tmc_write_data_in;
            end if;
            when b"111011001" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg473 <= tmc_write_data_in;
            end if;
            when b"111011010" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg474 <= tmc_write_data_in;
            end if;
            when b"111011011" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg475 <= tmc_write_data_in;
            end if;
            when b"111011100" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg476 <= tmc_write_data_in;
            end if;
            when b"111011101" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg477 <= tmc_write_data_in;
            end if;
            when b"111011110" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg478 <= tmc_write_data_in;
            end if;
            when b"111011111" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg479 <= tmc_write_data_in;
            end if;
            when b"111100000" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg480 <= tmc_write_data_in;
            end if;
            when b"111100001" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg481 <= tmc_write_data_in;
            end if;
            when b"111100010" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg482 <= tmc_write_data_in;
            end if;
            when b"111100011" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg483 <= tmc_write_data_in;
            end if;
            when b"111100100" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg484 <= tmc_write_data_in;
            end if;
            when b"111100101" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg485 <= tmc_write_data_in;
            end if;
            when b"111100110" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg486 <= tmc_write_data_in;
            end if;
            when b"111100111" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg487 <= tmc_write_data_in;
            end if;
            when b"111101000" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg488 <= tmc_write_data_in;
            end if;
            when b"111101001" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg489 <= tmc_write_data_in;
            end if;
            when b"111101010" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg490 <= tmc_write_data_in;
            end if;
            when b"111101011" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg491 <= tmc_write_data_in;
            end if;
            when b"111101100" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg492 <= tmc_write_data_in;
            end if;
            when b"111101101" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg493 <= tmc_write_data_in;
            end if;
            when b"111101110" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg494 <= tmc_write_data_in;
            end if;
            when b"111101111" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg495 <= tmc_write_data_in;
            end if;
            when b"111110000" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg496 <= tmc_write_data_in;
            end if;
            when b"111110001" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg497 <= tmc_write_data_in;
            end if;
            when b"111110010" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg498 <= tmc_write_data_in;
            end if;
            when b"111110011" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg499 <= tmc_write_data_in;
            end if;
            when b"111110100" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg500 <= tmc_write_data_in;
            end if;
            when b"111110101" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg501 <= tmc_write_data_in;
            end if;
            when b"111110110" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg502 <= tmc_write_data_in;
            end if;
            when b"111110111" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg503 <= tmc_write_data_in;
            end if;
            when b"111111000" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg504 <= tmc_write_data_in;
            end if;
            when b"111111001" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg505 <= tmc_write_data_in;
            end if;
            when b"111111010" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg506 <= tmc_write_data_in;
            end if;
            when b"111111011" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg507 <= tmc_write_data_in;
            end if;
            when b"111111100" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg508 <= tmc_write_data_in;
            end if;
            when b"111111101" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg509 <= tmc_write_data_in;
            end if;
            when b"111111110" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg510 <= tmc_write_data_in;
            end if;
            when b"111111111" =>
            if ( tmc_write_enable = '1' ) then
              slv_reg511 <= tmc_write_data_in;
            end if; 
            when others =>
         end case;
	   end if;
	 end if;                   
    end process; 

	-- Implement write response logic generation
	-- The write response and response valid signals are asserted by the slave 
	-- when axi_wready, S_AXI_WVALID, axi_wready and S_AXI_WVALID are asserted.  
	-- This marks the acceptance of address and indicates the status of 
	-- write transaction.

	process (S_AXI_ACLK)
	begin
	  if rising_edge(S_AXI_ACLK) then 
	    if S_AXI_ARESETN = '0' then
	      axi_bvalid  <= '0';
	      axi_bresp   <= "00"; --need to work more on the responses
	    else
	      if (axi_awready = '1' and S_AXI_AWVALID = '1' and axi_wready = '1' and S_AXI_WVALID = '1' and axi_bvalid = '0'  ) then
	        axi_bvalid <= '1';
	        axi_bresp  <= "00"; 
	      elsif (S_AXI_BREADY = '1' and axi_bvalid = '1') then   --check if bready is asserted while bvalid is high)
	        axi_bvalid <= '0';                                 -- (there is a possibility that bready is always asserted high)
	      end if;
	    end if;
	  end if;                   
	end process; 

	-- Implement axi_arready generation
	-- axi_arready is asserted for one S_AXI_ACLK clock cycle when
	-- S_AXI_ARVALID is asserted. axi_awready is 
	-- de-asserted when reset (active low) is asserted. 
	-- The read address is also latched when S_AXI_ARVALID is 
	-- asserted. axi_araddr is reset to zero on reset assertion.

	process (S_AXI_ACLK)
	begin
	  if rising_edge(S_AXI_ACLK) then 
	    if S_AXI_ARESETN = '0' then
	      axi_arready <= '0';
	      axi_araddr  <= (others => '1');
	    else
	      if (axi_arready = '0' and S_AXI_ARVALID = '1') then
	        -- indicates that the slave has acceped the valid read address
	        axi_arready <= '1';
	        -- Read Address latching 
	        axi_araddr  <= S_AXI_ARADDR;           
	      else
	        axi_arready <= '0';
	      end if;
	    end if;
	  end if;                   
	end process; 

	-- Implement axi_arvalid generation
	-- axi_rvalid is asserted for one S_AXI_ACLK clock cycle when both 
	-- S_AXI_ARVALID and axi_arready are asserted. The slave registers 
	-- data are available on the axi_rdata bus at this instance. The 
	-- assertion of axi_rvalid marks the validity of read data on the 
	-- bus and axi_rresp indicates the status of read transaction.axi_rvalid 
	-- is deasserted on reset (active low). axi_rresp and axi_rdata are 
	-- cleared to zero on reset (active low).  
	process (S_AXI_ACLK)
	begin
	  if rising_edge(S_AXI_ACLK) then
	    if S_AXI_ARESETN = '0' then
	      axi_rvalid <= '0';
	      axi_rresp  <= "00";
	    else
	      if (axi_arready = '1' and S_AXI_ARVALID = '1' and axi_rvalid = '0') then
	        -- Valid read data is available at the read data bus
	        axi_rvalid <= '1';
	        axi_rresp  <= "00"; -- 'OKAY' response
	      elsif (axi_rvalid = '1' and S_AXI_RREADY = '1') then
	        -- Read data is accepted by the master
	        axi_rvalid <= '0';
	      end if;            
	    end if;
	  end if;
	end process;

	-- Implement memory mapped register select and read logic generation
	-- Slave register read enable is asserted when valid address is available
	-- and the slave is ready to accept the read address.
	slv_reg_rden <= axi_arready and S_AXI_ARVALID and (not axi_rvalid) ;

	process (slv_reg0, slv_reg1, slv_reg2, slv_reg3, slv_reg4, slv_reg5, slv_reg6, slv_reg7, slv_reg8, slv_reg9, slv_reg10, slv_reg11, slv_reg12, slv_reg13, slv_reg14, slv_reg15, slv_reg16, slv_reg17, slv_reg18, slv_reg19, slv_reg20, slv_reg21, slv_reg22, slv_reg23, slv_reg24, slv_reg25, slv_reg26, slv_reg27, slv_reg28, slv_reg29, slv_reg30, slv_reg31, slv_reg32, slv_reg33, slv_reg34, slv_reg35, slv_reg36, slv_reg37, slv_reg38, slv_reg39, slv_reg40, slv_reg41, slv_reg42, slv_reg43, slv_reg44, slv_reg45, slv_reg46, slv_reg47, slv_reg48, slv_reg49, slv_reg50, slv_reg51, slv_reg52, slv_reg53, slv_reg54, slv_reg55, slv_reg56, slv_reg57, slv_reg58, slv_reg59, slv_reg60, slv_reg61, slv_reg62, slv_reg63, slv_reg64, slv_reg65, slv_reg66, slv_reg67, slv_reg68, slv_reg69, slv_reg70, slv_reg71, slv_reg72, slv_reg73, slv_reg74, slv_reg75, slv_reg76, slv_reg77, slv_reg78, slv_reg79, slv_reg80, slv_reg81, slv_reg82, slv_reg83, slv_reg84, slv_reg85, slv_reg86, slv_reg87, slv_reg88, slv_reg89, slv_reg90, slv_reg91, slv_reg92, slv_reg93, slv_reg94, slv_reg95, slv_reg96, slv_reg97, slv_reg98, slv_reg99, slv_reg100, slv_reg101, slv_reg102, slv_reg103, slv_reg104, slv_reg105, slv_reg106, slv_reg107, slv_reg108, slv_reg109, slv_reg110, slv_reg111, slv_reg112, slv_reg113, slv_reg114, slv_reg115, slv_reg116, slv_reg117, slv_reg118, slv_reg119, slv_reg120, slv_reg121, slv_reg122, slv_reg123, slv_reg124, slv_reg125, slv_reg126, slv_reg127, slv_reg128, slv_reg129, slv_reg130, slv_reg131, slv_reg132, slv_reg133, slv_reg134, slv_reg135, slv_reg136, slv_reg137, slv_reg138, slv_reg139, slv_reg140, slv_reg141, slv_reg142, slv_reg143, slv_reg144, slv_reg145, slv_reg146, slv_reg147, slv_reg148, slv_reg149, slv_reg150, slv_reg151, slv_reg152, slv_reg153, slv_reg154, slv_reg155, slv_reg156, slv_reg157, slv_reg158, slv_reg159, slv_reg160, slv_reg161, slv_reg162, slv_reg163, slv_reg164, slv_reg165, slv_reg166, slv_reg167, slv_reg168, slv_reg169, slv_reg170, slv_reg171, slv_reg172, slv_reg173, slv_reg174, slv_reg175, slv_reg176, slv_reg177, slv_reg178, slv_reg179, slv_reg180, slv_reg181, slv_reg182, slv_reg183, slv_reg184, slv_reg185, slv_reg186, slv_reg187, slv_reg188, slv_reg189, slv_reg190, slv_reg191, slv_reg192, slv_reg193, slv_reg194, slv_reg195, slv_reg196, slv_reg197, slv_reg198, slv_reg199, slv_reg200, slv_reg201, slv_reg202, slv_reg203, slv_reg204, slv_reg205, slv_reg206, slv_reg207, slv_reg208, slv_reg209, slv_reg210, slv_reg211, slv_reg212, slv_reg213, slv_reg214, slv_reg215, slv_reg216, slv_reg217, slv_reg218, slv_reg219, slv_reg220, slv_reg221, slv_reg222, slv_reg223, slv_reg224, slv_reg225, slv_reg226, slv_reg227, slv_reg228, slv_reg229, slv_reg230, slv_reg231, slv_reg232, slv_reg233, slv_reg234, slv_reg235, slv_reg236, slv_reg237, slv_reg238, slv_reg239, slv_reg240, slv_reg241, slv_reg242, slv_reg243, slv_reg244, slv_reg245, slv_reg246, slv_reg247, slv_reg248, slv_reg249, slv_reg250, slv_reg251, slv_reg252, slv_reg253, slv_reg254, slv_reg255, slv_reg256, slv_reg257, slv_reg258, slv_reg259, slv_reg260, slv_reg261, slv_reg262, slv_reg263, slv_reg264, slv_reg265, slv_reg266, slv_reg267, slv_reg268, slv_reg269, slv_reg270, slv_reg271, slv_reg272, slv_reg273, slv_reg274, slv_reg275, slv_reg276, slv_reg277, slv_reg278, slv_reg279, slv_reg280, slv_reg281, slv_reg282, slv_reg283, slv_reg284, slv_reg285, slv_reg286, slv_reg287, slv_reg288, slv_reg289, slv_reg290, slv_reg291, slv_reg292, slv_reg293, slv_reg294, slv_reg295, slv_reg296, slv_reg297, slv_reg298, slv_reg299, slv_reg300, slv_reg301, slv_reg302, slv_reg303, slv_reg304, slv_reg305, slv_reg306, slv_reg307, slv_reg308, slv_reg309, slv_reg310, slv_reg311, slv_reg312, slv_reg313, slv_reg314, slv_reg315, slv_reg316, slv_reg317, slv_reg318, slv_reg319, slv_reg320, slv_reg321, slv_reg322, slv_reg323, slv_reg324, slv_reg325, slv_reg326, slv_reg327, slv_reg328, slv_reg329, slv_reg330, slv_reg331, slv_reg332, slv_reg333, slv_reg334, slv_reg335, slv_reg336, slv_reg337, slv_reg338, slv_reg339, slv_reg340, slv_reg341, slv_reg342, slv_reg343, slv_reg344, slv_reg345, slv_reg346, slv_reg347, slv_reg348, slv_reg349, slv_reg350, slv_reg351, slv_reg352, slv_reg353, slv_reg354, slv_reg355, slv_reg356, slv_reg357, slv_reg358, slv_reg359, slv_reg360, slv_reg361, slv_reg362, slv_reg363, slv_reg364, slv_reg365, slv_reg366, slv_reg367, slv_reg368, slv_reg369, slv_reg370, slv_reg371, slv_reg372, slv_reg373, slv_reg374, slv_reg375, slv_reg376, slv_reg377, slv_reg378, slv_reg379, slv_reg380, slv_reg381, slv_reg382, slv_reg383, slv_reg384, slv_reg385, slv_reg386, slv_reg387, slv_reg388, slv_reg389, slv_reg390, slv_reg391, slv_reg392, slv_reg393, slv_reg394, slv_reg395, slv_reg396, slv_reg397, slv_reg398, slv_reg399, slv_reg400, slv_reg401, slv_reg402, slv_reg403, slv_reg404, slv_reg405, slv_reg406, slv_reg407, slv_reg408, slv_reg409, slv_reg410, slv_reg411, slv_reg412, slv_reg413, slv_reg414, slv_reg415, slv_reg416, slv_reg417, slv_reg418, slv_reg419, slv_reg420, slv_reg421, slv_reg422, slv_reg423, slv_reg424, slv_reg425, slv_reg426, slv_reg427, slv_reg428, slv_reg429, slv_reg430, slv_reg431, slv_reg432, slv_reg433, slv_reg434, slv_reg435, slv_reg436, slv_reg437, slv_reg438, slv_reg439, slv_reg440, slv_reg441, slv_reg442, slv_reg443, slv_reg444, slv_reg445, slv_reg446, slv_reg447, slv_reg448, slv_reg449, slv_reg450, slv_reg451, slv_reg452, slv_reg453, slv_reg454, slv_reg455, slv_reg456, slv_reg457, slv_reg458, slv_reg459, slv_reg460, slv_reg461, slv_reg462, slv_reg463, slv_reg464, slv_reg465, slv_reg466, slv_reg467, slv_reg468, slv_reg469, slv_reg470, slv_reg471, slv_reg472, slv_reg473, slv_reg474, slv_reg475, slv_reg476, slv_reg477, slv_reg478, slv_reg479, slv_reg480, slv_reg481, slv_reg482, slv_reg483, slv_reg484, slv_reg485, slv_reg486, slv_reg487, slv_reg488, slv_reg489, slv_reg490, slv_reg491, slv_reg492, slv_reg493, slv_reg494, slv_reg495, slv_reg496, slv_reg497, slv_reg498, slv_reg499, slv_reg500, slv_reg501, slv_reg502, slv_reg503, slv_reg504, slv_reg505, slv_reg506, slv_reg507, slv_reg508, slv_reg509, slv_reg510, slv_reg511, axi_araddr, S_AXI_ARESETN, slv_reg_rden)
	variable loc_addr :std_logic_vector(OPT_MEM_ADDR_BITS downto 0);
	begin
	    -- Address decoding for reading registers
	    loc_addr := axi_araddr(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB);
	    
	    case loc_addr is
	    
	    -- ****************************************************************************************************
	    --  Read registers value
	    -- ****************************************************************************************************
	    
	    -- FIFO instrumentation	
	    ------------------------------------------------------------------------------------------------------
	      when b"000000000" =>
	        reg_data_out <= slv_reg0;
	      when b"000000001" =>
	        reg_data_out <= slv_reg1;
	      when b"000000010" =>
	        reg_data_out <= slv_reg2;
	      when b"000000011" =>
	        reg_data_out <= slv_reg3;
	      when b"000000100" =>
	        reg_data_out <= slv_reg4;
	      when b"000000101" =>
	        reg_data_out <= slv_reg5;

	        
	      -- FIFO TMC
          ------------------------------------------------------------------------------------------------------  
	      when b"000000110" =>
	        reg_data_out <= slv_reg6;
	      when b"000000111" =>
	        reg_data_out <= slv_reg7;
	      when b"000001000" =>
	        reg_data_out <= slv_reg8;
	      when b"000001001" =>
	        reg_data_out <= slv_reg9;
	      when b"000001010" =>
	        reg_data_out <= slv_reg10;
	      when b"000001011" =>
	        reg_data_out <= slv_reg11;

	        
          -- FIFO Monitor to Kernel
          ------------------------------------------------------------------------------------------------------  	        	        
	      when b"000001100" =>
	        reg_data_out <= slv_reg12;
	      when b"000001101" =>
	        reg_data_out <= slv_reg13;
	      when b"000001110" =>
	        reg_data_out <=  slv_reg14;
	      when b"000001111" =>
	        reg_data_out <= slv_reg15;
	      when b"000010000" =>
	        reg_data_out <=  slv_reg16;
	      when b"000010001" =>
	        reg_data_out <= slv_reg17;
	            
	        
          -- FIFO Kernel to Monitor
          ------------------------------------------------------------------------------------------------------                    
	      when b"000010010" =>
	        reg_data_out <= slv_reg18;
	      when b"000010011" =>
	        reg_data_out <= slv_reg19;
	      when b"000010100" =>
	        reg_data_out <= slv_reg20;
	      when b"000010101" =>
	        reg_data_out <= slv_reg21;
	      when b"000010110" =>
	        reg_data_out <= slv_reg22;
	      when b"000010111" =>
	        reg_data_out <= slv_reg23;
	        
                         
          -- FIFO PTM
          ------------------------------------------------------------------------------------------------------                               
	      when b"000011000" =>
	        reg_data_out <= slv_reg24;
	      when b"000011001" =>
	        reg_data_out <= slv_reg25;
	      when b"000011010" =>
	        reg_data_out <=slv_reg26;
	      when b"000011011" =>
	        reg_data_out <=  slv_reg27;
	      when b"000011100" =>
	        reg_data_out <= slv_reg28;
	      when b"000011101" =>
	        reg_data_out <= slv_reg29;
	 
	 
          -- Dispatcher
          ------------------------------------------------------------------------------------------------------  	        
	      when b"000011110" =>
	        reg_data_out <= slv_reg30;
	      when b"000011111" =>
	        reg_data_out <= slv_reg31;
	      when b"000100000" =>
	        reg_data_out <= slv_reg32;
	      when b"000100001" =>
	        reg_data_out <= slv_reg33;
	      when b"000100010" =>
	        reg_data_out <= slv_reg34;
	      when b"000100011" =>
	        reg_data_out <= slv_reg35;
	      when b"000100100" =>
	        reg_data_out <= slv_reg36;
	      when b"000100101" =>
	        reg_data_out <= slv_reg37;
	      when b"000100110" =>
	        reg_data_out <= slv_reg38;
	      when b"000100111" =>
	        reg_data_out <= slv_reg39;
	      when b"000101000" =>
	        reg_data_out <= slv_reg40;
	      when b"000101001" =>
	        reg_data_out <= slv_reg41;
	      when b"000101010" =>
	        reg_data_out <= slv_reg42;
	      when b"000101011" =>
	        reg_data_out <= slv_reg43;
	      when b"000101100" =>
	        reg_data_out <= slv_reg44;
	      when b"000101101" =>
	        reg_data_out <= slv_reg45;
	      when b"000101110" =>
	        reg_data_out <= slv_reg46;
	      when b"000101111" =>
	        reg_data_out <= slv_reg47;
	      when b"000110000" =>
	        reg_data_out <= slv_reg48;
	      when b"000110001" =>
	        reg_data_out <= slv_reg49;
	      when b"000110010" =>
	        reg_data_out <= slv_reg50;
	      when b"000110011" =>
	        reg_data_out <= slv_reg51;
	      when b"000110100" =>
	        reg_data_out <= slv_reg52;
	      when b"000110101" =>
	        reg_data_out <= slv_reg53;
	      when b"000110110" =>
	        reg_data_out <= slv_reg54;
	      when b"000110111" =>
	        reg_data_out <= slv_reg55;
	      when b"000111000" =>
	        reg_data_out <= slv_reg56;
	      when b"000111001" =>
	        reg_data_out <= slv_reg57;
	      when b"000111010" =>
	        reg_data_out <= slv_reg58;
	      when b"000111011" =>
	        reg_data_out <= slv_reg59;
	      when b"000111100" =>
	        reg_data_out <= slv_reg60;
	      when b"000111101" =>
	        reg_data_out <= slv_reg61;
	      when b"000111110" =>
	        reg_data_out <= slv_reg62;
	      when b"000111111" =>
	        reg_data_out <= slv_reg63;
	      when b"001000000" =>
	        reg_data_out <= slv_reg64;
	      when b"001000001" =>
	        reg_data_out <= slv_reg65;
	      when b"001000010" =>
	        reg_data_out <= slv_reg66;
	      when b"001000011" =>
	        reg_data_out <= slv_reg67;
	      when b"001000100" =>
	        reg_data_out <= slv_reg68;
	      when b"001000101" =>
	        reg_data_out <= slv_reg69;
	      when b"001000110" =>
	        reg_data_out <= slv_reg70;
	      when b"001000111" =>
	        reg_data_out <= slv_reg71;
	      when b"001001000" =>
	        reg_data_out <= slv_reg72;
	      when b"001001001" =>
	        reg_data_out <= slv_reg73;
	      when b"001001010" =>
	        reg_data_out <= slv_reg74;
	      when b"001001011" =>
	        reg_data_out <= slv_reg75;
	      when b"001001100" =>
	        reg_data_out <= slv_reg76;
	      when b"001001101" =>
	        reg_data_out <= slv_reg77;
	      when b"001001110" =>
	        reg_data_out <= slv_reg78;
	      when b"001001111" =>
	        reg_data_out <= slv_reg79;
	      when b"001010000" =>
	        reg_data_out <= slv_reg80;
	      when b"001010001" =>
	        reg_data_out <= slv_reg81;
	      when b"001010010" =>
	        reg_data_out <= slv_reg82;
	      when b"001010011" =>
	        reg_data_out <= slv_reg83;
	      when b"001010100" =>
	        reg_data_out <= slv_reg84;
	      when b"001010101" =>
	        reg_data_out <= slv_reg85;
	      when b"001010110" =>
	        reg_data_out <= slv_reg86;
	      when b"001010111" =>
	        reg_data_out <= slv_reg87;
	      when b"001011000" =>
	        reg_data_out <= slv_reg88;
	      when b"001011001" =>
	        reg_data_out <= slv_reg89;
	      when b"001011010" =>
	        reg_data_out <= slv_reg90;
	      when b"001011011" =>
	        reg_data_out <= slv_reg91;
	      when b"001011100" =>
	        reg_data_out <= slv_reg92;
	      when b"001011101" =>
	        reg_data_out <= slv_reg93;
	      when b"001011110" =>
	        reg_data_out <= slv_reg94;
	      when b"001011111" =>
	        reg_data_out <= slv_reg95;
	      when b"001100000" =>
	        reg_data_out <= slv_reg96;
	      when b"001100001" =>
	        reg_data_out <= slv_reg97;
	      when b"001100010" =>
	        reg_data_out <= slv_reg98;
	      when b"001100011" =>
	        reg_data_out <= slv_reg99;
	      when b"001100100" =>
	        reg_data_out <= slv_reg100;
	      when b"001100101" =>
	        reg_data_out <= slv_reg101;
	      when b"001100110" =>
	        reg_data_out <= slv_reg102;
	      when b"001100111" =>
	        reg_data_out <= slv_reg103;
	      when b"001101000" =>
	        reg_data_out <= slv_reg104;
	      when b"001101001" =>
	        reg_data_out <= slv_reg105;
	      when b"001101010" =>
	        reg_data_out <= slv_reg106;
	      when b"001101011" =>
	        reg_data_out <= slv_reg107;
	      when b"001101100" =>
	        reg_data_out <= slv_reg108;
	      when b"001101101" =>
	        reg_data_out <= slv_reg109;
	      when b"001101110" =>
	        reg_data_out <= slv_reg110;
	      when b"001101111" =>
	        reg_data_out <= slv_reg111;
	      when b"001110000" =>
	        reg_data_out <= slv_reg112;
	      when b"001110001" =>
	        reg_data_out <= slv_reg113;
	      when b"001110010" =>
	        reg_data_out <= slv_reg114;
	      when b"001110011" =>
	        reg_data_out <= slv_reg115;
	      when b"001110100" =>
	        reg_data_out <= slv_reg116;
	      when b"001110101" =>
	        reg_data_out <= slv_reg117;
	      when b"001110110" =>
	        reg_data_out <= slv_reg118;
	      when b"001110111" =>
	        reg_data_out <= slv_reg119;
	      when b"001111000" =>
	        reg_data_out <= slv_reg120;
	      when b"001111001" =>
	        reg_data_out <= slv_reg121;
	      when b"001111010" =>
	        reg_data_out <= slv_reg122;
	      when b"001111011" =>
	        reg_data_out <= slv_reg123;
	      when b"001111100" =>
	        reg_data_out <= slv_reg124;
	      when b"001111101" =>
	        reg_data_out <= slv_reg125;
	      when b"001111110" =>
	        reg_data_out <= slv_reg126;
	      when b"001111111" =>
	        reg_data_out <= slv_reg127;
	      when b"010000000" =>
	        reg_data_out <= slv_reg128;
	      when b"010000001" =>
	        reg_data_out <= slv_reg129;
	      when b"010000010" =>
	        reg_data_out <= slv_reg130;
	      when b"010000011" =>
	        reg_data_out <= slv_reg131;
	      when b"010000100" =>
	        reg_data_out <= slv_reg132;
	      when b"010000101" =>
	        reg_data_out <= slv_reg133;
	      when b"010000110" =>
	        reg_data_out <= slv_reg134;
	      when b"010000111" =>
	        reg_data_out <= slv_reg135;
	      when b"010001000" =>
	        reg_data_out <= slv_reg136;
	      when b"010001001" =>
	        reg_data_out <= slv_reg137;
	      when b"010001010" =>
	        reg_data_out <= slv_reg138;
	      when b"010001011" =>
	        reg_data_out <= slv_reg139;
	      when b"010001100" =>
	        reg_data_out <= slv_reg140;
	      when b"010001101" =>
	        reg_data_out <= slv_reg141;
	      when b"010001110" =>
	        reg_data_out <= slv_reg142;
	      when b"010001111" =>
	        reg_data_out <= slv_reg143;
	      when b"010010000" =>
	        reg_data_out <= slv_reg144;
	      when b"010010001" =>
	        reg_data_out <= slv_reg145;
	      when b"010010010" =>
	        reg_data_out <= slv_reg146;
	      when b"010010011" =>
	        reg_data_out <= slv_reg147;
	      when b"010010100" =>
	        reg_data_out <= slv_reg148;
	      when b"010010101" =>
	        reg_data_out <= slv_reg149;
	      when b"010010110" =>
	        reg_data_out <= slv_reg150;
	      when b"010010111" =>
	        reg_data_out <= slv_reg151;
	      when b"010011000" =>
	        reg_data_out <= slv_reg152;
	      when b"010011001" =>
	        reg_data_out <= slv_reg153;
	      when b"010011010" =>
	        reg_data_out <= slv_reg154;
	      when b"010011011" =>
	        reg_data_out <= slv_reg155;
	      when b"010011100" =>
	        reg_data_out <= slv_reg156;
	      when b"010011101" =>
	        reg_data_out <= slv_reg157;
	      when b"010011110" =>
	        reg_data_out <= slv_reg158;
	      when b"010011111" =>
	        reg_data_out <= slv_reg159;
	      when b"010100000" =>
	        reg_data_out <= slv_reg160;
	      when b"010100001" =>
	        reg_data_out <= slv_reg161;
	      when b"010100010" =>
	        reg_data_out <= slv_reg162;
	      when b"010100011" =>
	        reg_data_out <= slv_reg163;
	      when b"010100100" =>
	        reg_data_out <= slv_reg164;
	      when b"010100101" =>
	        reg_data_out <= slv_reg165;
	      when b"010100110" =>
	        reg_data_out <= slv_reg166;
	      when b"010100111" =>
	        reg_data_out <= slv_reg167;
	      when b"010101000" =>
	        reg_data_out <= slv_reg168;
	      when b"010101001" =>
	        reg_data_out <= slv_reg169;
	      when b"010101010" =>
	        reg_data_out <= slv_reg170;
	      when b"010101011" =>
	        reg_data_out <= slv_reg171;
	      when b"010101100" =>
	        reg_data_out <= slv_reg172;
	      when b"010101101" =>
	        reg_data_out <= slv_reg173;
	      when b"010101110" =>
	        reg_data_out <= slv_reg174;
	      when b"010101111" =>
	        reg_data_out <= slv_reg175;
	      when b"010110000" =>
	        reg_data_out <= slv_reg176;
	      when b"010110001" =>
	        reg_data_out <= slv_reg177;
	      when b"010110010" =>
	        reg_data_out <= slv_reg178;
	      when b"010110011" =>
	        reg_data_out <= slv_reg179;
	      when b"010110100" =>
	        reg_data_out <= slv_reg180;
	      when b"010110101" =>
	        reg_data_out <= slv_reg181;
	      when b"010110110" =>
	        reg_data_out <= slv_reg182;
	      when b"010110111" =>
	        reg_data_out <= slv_reg183;
	      when b"010111000" =>
	        reg_data_out <= slv_reg184;
	      when b"010111001" =>
	        reg_data_out <= slv_reg185;
	      when b"010111010" =>
	        reg_data_out <= slv_reg186;
	      when b"010111011" =>
	        reg_data_out <= slv_reg187;
	      when b"010111100" =>
	        reg_data_out <= slv_reg188;
	      when b"010111101" =>
	        reg_data_out <= slv_reg189;
	      when b"010111110" =>
	        reg_data_out <= slv_reg190;
	      when b"010111111" =>
	        reg_data_out <= slv_reg191;
	      when b"011000000" =>
	        reg_data_out <= slv_reg192;
	      when b"011000001" =>
	        reg_data_out <= slv_reg193;
	      when b"011000010" =>
	        reg_data_out <= slv_reg194;
	      when b"011000011" =>
	        reg_data_out <= slv_reg195;
	      when b"011000100" =>
	        reg_data_out <= slv_reg196;
	      when b"011000101" =>
	        reg_data_out <= slv_reg197;
	      when b"011000110" =>
	        reg_data_out <= slv_reg198;
	      when b"011000111" =>
	        reg_data_out <= slv_reg199;
	      when b"011001000" =>
	        reg_data_out <= slv_reg200;
	      when b"011001001" =>
	        reg_data_out <= slv_reg201;
	      when b"011001010" =>
	        reg_data_out <= slv_reg202;
	      when b"011001011" =>
	        reg_data_out <= slv_reg203;
	      when b"011001100" =>
	        reg_data_out <= slv_reg204;
	      when b"011001101" =>
	        reg_data_out <= slv_reg205;
	      when b"011001110" =>
	        reg_data_out <= slv_reg206;
	      when b"011001111" =>
	        reg_data_out <= slv_reg207;
	      when b"011010000" =>
	        reg_data_out <= slv_reg208;
	      when b"011010001" =>
	        reg_data_out <= slv_reg209;
	      when b"011010010" =>
	        reg_data_out <= slv_reg210;
	      when b"011010011" =>
	        reg_data_out <= slv_reg211;
	      when b"011010100" =>
	        reg_data_out <= slv_reg212;
	      when b"011010101" =>
	        reg_data_out <= slv_reg213;
	      when b"011010110" =>
	        reg_data_out <= slv_reg214;
	      when b"011010111" =>
	        reg_data_out <= slv_reg215;
	      when b"011011000" =>
	        reg_data_out <= slv_reg216;
	      when b"011011001" =>
	        reg_data_out <= slv_reg217;
	      when b"011011010" =>
	        reg_data_out <= slv_reg218;
	      when b"011011011" =>
	        reg_data_out <= slv_reg219;
	      when b"011011100" =>
	        reg_data_out <= slv_reg220;
	      when b"011011101" =>
	        reg_data_out <= slv_reg221;
	      when b"011011110" =>
	        reg_data_out <= slv_reg222;
	      when b"011011111" =>
	        reg_data_out <= slv_reg223;
	      when b"011100000" =>
	        reg_data_out <= slv_reg224;
	      when b"011100001" =>
	        reg_data_out <= slv_reg225;
	      when b"011100010" =>
	        reg_data_out <= slv_reg226;
	      when b"011100011" =>
	        reg_data_out <= slv_reg227;
	      when b"011100100" =>
	        reg_data_out <= slv_reg228;
	      when b"011100101" =>
	        reg_data_out <= slv_reg229;
	      when b"011100110" =>
	        reg_data_out <= slv_reg230;
	      when b"011100111" =>
	        reg_data_out <= slv_reg231;
	      when b"011101000" =>
	        reg_data_out <= slv_reg232;
	      when b"011101001" =>
	        reg_data_out <= slv_reg233;
	      when b"011101010" =>
	        reg_data_out <= slv_reg234;
	      when b"011101011" =>
	        reg_data_out <= slv_reg235;
	      when b"011101100" =>
	        reg_data_out <= slv_reg236;
	      when b"011101101" =>
	        reg_data_out <= slv_reg237;
	      when b"011101110" =>
	        reg_data_out <= slv_reg238;
	      when b"011101111" =>
	        reg_data_out <= slv_reg239;
	      when b"011110000" =>
	        reg_data_out <= slv_reg240;
	      when b"011110001" =>
	        reg_data_out <= slv_reg241;
	      when b"011110010" =>
	        reg_data_out <= slv_reg242;
	      when b"011110011" =>
	        reg_data_out <= slv_reg243;
	      when b"011110100" =>
	        reg_data_out <= slv_reg244;
	      when b"011110101" =>
	        reg_data_out <= slv_reg245;
	      when b"011110110" =>
	        reg_data_out <= slv_reg246;
	      when b"011110111" =>
	        reg_data_out <= slv_reg247;
	      when b"011111000" =>
	        reg_data_out <= slv_reg248;
	      when b"011111001" =>
	        reg_data_out <= slv_reg249;
	      when b"011111010" =>
	        reg_data_out <= slv_reg250;
	      when b"011111011" =>
	        reg_data_out <= slv_reg251;
	      when b"011111100" =>
	        reg_data_out <= slv_reg252;
	      when b"011111101" =>
	        reg_data_out <= slv_reg253;
	      when b"011111110" =>
	        reg_data_out <= slv_reg254;
	      when b"011111111" =>
	        reg_data_out <= slv_reg255;
	      when b"100000000" =>
	        reg_data_out <= slv_reg256;
	      when b"100000001" =>
	        reg_data_out <= slv_reg257;
	      when b"100000010" =>
	        reg_data_out <= slv_reg258;
	      when b"100000011" =>
	        reg_data_out <= slv_reg259;
	      when b"100000100" =>
	        reg_data_out <= slv_reg260;
	      when b"100000101" =>
	        reg_data_out <= slv_reg261;
	      when b"100000110" =>
	        reg_data_out <= slv_reg262;
	      when b"100000111" =>
	        reg_data_out <= slv_reg263;
	      when b"100001000" =>
	        reg_data_out <= slv_reg264;
	      when b"100001001" =>
	        reg_data_out <= slv_reg265;
	      when b"100001010" =>
	        reg_data_out <= slv_reg266;
	      when b"100001011" =>
	        reg_data_out <= slv_reg267;
	      when b"100001100" =>
	        reg_data_out <= slv_reg268;
	      when b"100001101" =>
	        reg_data_out <= slv_reg269;
	      when b"100001110" =>
	        reg_data_out <= slv_reg270;
	      when b"100001111" =>
	        reg_data_out <= slv_reg271;
	        
	      	 
	      -- TMC
	      ------------------------------------------------------------------------------------------------------    
	      when b"100010000" =>
	        reg_data_out <= slv_reg272;
	      when b"100010001" =>
	        reg_data_out <= slv_reg273;
	      when b"100010010" =>
	        reg_data_out <= slv_reg274;
	      when b"100010011" =>
	        reg_data_out <= slv_reg275;
	      when b"100010100" =>
	        reg_data_out <= slv_reg276;
	      when b"100010101" =>
	        reg_data_out <= slv_reg277;
	      when b"100010110" =>
	        reg_data_out <= slv_reg278;
	      when b"100010111" =>
	        reg_data_out <= slv_reg279;
	      when b"100011000" =>
	        reg_data_out <= slv_reg280;
	      when b"100011001" =>
	        reg_data_out <= slv_reg281;
	      when b"100011010" =>
	        reg_data_out <= slv_reg282;
	      when b"100011011" =>
	        reg_data_out <= slv_reg283;
	      when b"100011100" =>
	        reg_data_out <= slv_reg284;
	      when b"100011101" =>
	        reg_data_out <= slv_reg285;
	      when b"100011110" =>
	        reg_data_out <= slv_reg286;
	      when b"100011111" =>
	        reg_data_out <= slv_reg287;
	      when b"100100000" =>
	        reg_data_out <= slv_reg288;
	      when b"100100001" =>
	        reg_data_out <= slv_reg289;
	      when b"100100010" =>
	        reg_data_out <= slv_reg290;
	      when b"100100011" =>
	        reg_data_out <= slv_reg291;
	      when b"100100100" =>
	        reg_data_out <= slv_reg292;
	      when b"100100101" =>
	        reg_data_out <= slv_reg293;
	      when b"100100110" =>
	        reg_data_out <= slv_reg294;
	      when b"100100111" =>
	        reg_data_out <= slv_reg295;
	      when b"100101000" =>
	        reg_data_out <= slv_reg296;
	      when b"100101001" =>
	        reg_data_out <= slv_reg297;
	      when b"100101010" =>
	        reg_data_out <= slv_reg298;
	      when b"100101011" =>
	        reg_data_out <= slv_reg299;
	      when b"100101100" =>
	        reg_data_out <= slv_reg300;
	      when b"100101101" =>
	        reg_data_out <= slv_reg301;
	      when b"100101110" =>
	        reg_data_out <= slv_reg302;
	      when b"100101111" =>
	        reg_data_out <= slv_reg303;
	      when b"100110000" =>
	        reg_data_out <= slv_reg304;
	      when b"100110001" =>
	        reg_data_out <= slv_reg305;
	      when b"100110010" =>
	        reg_data_out <= slv_reg306;
	      when b"100110011" =>
	        reg_data_out <= slv_reg307;
	      when b"100110100" =>
	        reg_data_out <= slv_reg308;
	      when b"100110101" =>
	        reg_data_out <= slv_reg309;
	      when b"100110110" =>
	        reg_data_out <= slv_reg310;
	      when b"100110111" =>
	        reg_data_out <= slv_reg311;
	      when b"100111000" =>
	        reg_data_out <= slv_reg312;
	      when b"100111001" =>
	        reg_data_out <= slv_reg313;
	      when b"100111010" =>
	        reg_data_out <= slv_reg314;
	      when b"100111011" =>
	        reg_data_out <= slv_reg315;
	      when b"100111100" =>
	        reg_data_out <= slv_reg316;
	      when b"100111101" =>
	        reg_data_out <= slv_reg317;
	      when b"100111110" =>
	        reg_data_out <= slv_reg318;
	      when b"100111111" =>
	        reg_data_out <= slv_reg319;
	      when b"101000000" =>
	        reg_data_out <= slv_reg320;
	      when b"101000001" =>
	        reg_data_out <= slv_reg321;
	      when b"101000010" =>
	        reg_data_out <= slv_reg322;
	      when b"101000011" =>
	        reg_data_out <= slv_reg323;
	      when b"101000100" =>
	        reg_data_out <= slv_reg324;
	      when b"101000101" =>
	        reg_data_out <= slv_reg325;
	      when b"101000110" =>
	        reg_data_out <= slv_reg326;
	      when b"101000111" =>
	        reg_data_out <= slv_reg327;
	      when b"101001000" =>
	        reg_data_out <= slv_reg328;
	      when b"101001001" =>
	        reg_data_out <= slv_reg329;
	      when b"101001010" =>
	        reg_data_out <= slv_reg330;
	      when b"101001011" =>
	        reg_data_out <= slv_reg331;
	      when b"101001100" =>
	        reg_data_out <= slv_reg332;
	      when b"101001101" =>
	        reg_data_out <= slv_reg333;
	      when b"101001110" =>
	        reg_data_out <= slv_reg334;
	      when b"101001111" =>
	        reg_data_out <= slv_reg335;
	      when b"101010000" =>
	        reg_data_out <= slv_reg336;
	      when b"101010001" =>
	        reg_data_out <= slv_reg337;
	      when b"101010010" =>
	        reg_data_out <= slv_reg338;
	      when b"101010011" =>
	        reg_data_out <= slv_reg339;
	      when b"101010100" =>
	        reg_data_out <= slv_reg340;
	      when b"101010101" =>
	        reg_data_out <= slv_reg341;
	      when b"101010110" =>
	        reg_data_out <= slv_reg342;
	      when b"101010111" =>
	        reg_data_out <= slv_reg343;
	      when b"101011000" =>
	        reg_data_out <= slv_reg344;
	      when b"101011001" =>
	        reg_data_out <= slv_reg345;
	      when b"101011010" =>
	        reg_data_out <= slv_reg346;
	      when b"101011011" =>
	        reg_data_out <= slv_reg347;
	      when b"101011100" =>
	        reg_data_out <= slv_reg348;
	      when b"101011101" =>
	        reg_data_out <= slv_reg349;
	      when b"101011110" =>
	        reg_data_out <= slv_reg350;
	      when b"101011111" =>
	        reg_data_out <= slv_reg351;
	      when b"101100000" =>
	        reg_data_out <= slv_reg352;
	      when b"101100001" =>
	        reg_data_out <= slv_reg353;
	      when b"101100010" =>
	        reg_data_out <= slv_reg354;
	      when b"101100011" =>
	        reg_data_out <= slv_reg355;
	      when b"101100100" =>
	        reg_data_out <= slv_reg356;
	      when b"101100101" =>
	        reg_data_out <= slv_reg357;
	      when b"101100110" =>
	        reg_data_out <= slv_reg358;
	      when b"101100111" =>
	        reg_data_out <= slv_reg359;
	      when b"101101000" =>
	        reg_data_out <= slv_reg360;
	      when b"101101001" =>
	        reg_data_out <= slv_reg361;
	      when b"101101010" =>
	        reg_data_out <= slv_reg362;
	      when b"101101011" =>
	        reg_data_out <= slv_reg363;
	      when b"101101100" =>
	        reg_data_out <= slv_reg364;
	      when b"101101101" =>
	        reg_data_out <= slv_reg365;
	      when b"101101110" =>
	        reg_data_out <= slv_reg366;
	      when b"101101111" =>
	        reg_data_out <= slv_reg367;
	      when b"101110000" =>
	        reg_data_out <= slv_reg368;
	      when b"101110001" =>
	        reg_data_out <= slv_reg369;
	      when b"101110010" =>
	        reg_data_out <= slv_reg370;
	      when b"101110011" =>
	        reg_data_out <= slv_reg371;
	      when b"101110100" =>
	        reg_data_out <= slv_reg372;
	      when b"101110101" =>
	        reg_data_out <= slv_reg373;
	      when b"101110110" =>
	        reg_data_out <= slv_reg374;
	      when b"101110111" =>
	        reg_data_out <= slv_reg375;
	      when b"101111000" =>
	        reg_data_out <= slv_reg376;
	      when b"101111001" =>
	        reg_data_out <= slv_reg377;
	      when b"101111010" =>
	        reg_data_out <= slv_reg378;
	      when b"101111011" =>
	        reg_data_out <= slv_reg379;
	      when b"101111100" =>
	        reg_data_out <= slv_reg380;
	      when b"101111101" =>
	        reg_data_out <= slv_reg381;
	      when b"101111110" =>
	        reg_data_out <= slv_reg382;
	      when b"101111111" =>
	        reg_data_out <= slv_reg383;
	      when b"110000000" =>
	        reg_data_out <= slv_reg384;
	      when b"110000001" =>
	        reg_data_out <= slv_reg385;
	      when b"110000010" =>
	        reg_data_out <= slv_reg386;
	      when b"110000011" =>
	        reg_data_out <= slv_reg387;
	      when b"110000100" =>
	        reg_data_out <= slv_reg388;
	      when b"110000101" =>
	        reg_data_out <= slv_reg389;
	      when b"110000110" =>
	        reg_data_out <= slv_reg390;
	      when b"110000111" =>
	        reg_data_out <= slv_reg391;
	      when b"110001000" =>
	        reg_data_out <= slv_reg392;
	      when b"110001001" =>
	        reg_data_out <= slv_reg393;
	      when b"110001010" =>
	        reg_data_out <= slv_reg394;
	      when b"110001011" =>
	        reg_data_out <= slv_reg395;
	      when b"110001100" =>
	        reg_data_out <= slv_reg396;
	      when b"110001101" =>
	        reg_data_out <= slv_reg397;
	      when b"110001110" =>
	        reg_data_out <= slv_reg398;
	      when b"110001111" =>
	        reg_data_out <= slv_reg399;
	      when b"110010000" =>
	        reg_data_out <= slv_reg400;
	      when b"110010001" =>
	        reg_data_out <= slv_reg401;
	      when b"110010010" =>
	        reg_data_out <= slv_reg402;
	      when b"110010011" =>
	        reg_data_out <= slv_reg403;
	      when b"110010100" =>
	        reg_data_out <= slv_reg404;
	      when b"110010101" =>
	        reg_data_out <= slv_reg405;
	      when b"110010110" =>
	        reg_data_out <= slv_reg406;
	      when b"110010111" =>
	        reg_data_out <= slv_reg407;
	      when b"110011000" =>
	        reg_data_out <= slv_reg408;
	      when b"110011001" =>
	        reg_data_out <= slv_reg409;
	      when b"110011010" =>
	        reg_data_out <= slv_reg410;
	      when b"110011011" =>
	        reg_data_out <= slv_reg411;
	      when b"110011100" =>
	        reg_data_out <= slv_reg412;
	      when b"110011101" =>
	        reg_data_out <= slv_reg413;
	      when b"110011110" =>
	        reg_data_out <= slv_reg414;
	      when b"110011111" =>
	        reg_data_out <= slv_reg415;
	      when b"110100000" =>
	        reg_data_out <= slv_reg416;
	      when b"110100001" =>
	        reg_data_out <= slv_reg417;
	      when b"110100010" =>
	        reg_data_out <= slv_reg418;
	      when b"110100011" =>
	        reg_data_out <= slv_reg419;
	      when b"110100100" =>
	        reg_data_out <= slv_reg420;
	      when b"110100101" =>
	        reg_data_out <= slv_reg421;
	      when b"110100110" =>
	        reg_data_out <= slv_reg422;
	      when b"110100111" =>
	        reg_data_out <= slv_reg423;
	      when b"110101000" =>
	        reg_data_out <= slv_reg424;
	      when b"110101001" =>
	        reg_data_out <= slv_reg425;
	      when b"110101010" =>
	        reg_data_out <= slv_reg426;
	      when b"110101011" =>
	        reg_data_out <= slv_reg427;
	      when b"110101100" =>
	        reg_data_out <= slv_reg428;
	      when b"110101101" =>
	        reg_data_out <= slv_reg429;
	      when b"110101110" =>
	        reg_data_out <= slv_reg430;
	      when b"110101111" =>
	        reg_data_out <= slv_reg431;
	      when b"110110000" =>
	        reg_data_out <= slv_reg432;
	      when b"110110001" =>
	        reg_data_out <= slv_reg433;
	      when b"110110010" =>
	        reg_data_out <= slv_reg434;
	      when b"110110011" =>
	        reg_data_out <= slv_reg435;
	      when b"110110100" =>
	        reg_data_out <= slv_reg436;
	      when b"110110101" =>
	        reg_data_out <= slv_reg437;
	      when b"110110110" =>
	        reg_data_out <= slv_reg438;
	      when b"110110111" =>
	        reg_data_out <= slv_reg439;
	      when b"110111000" =>
	        reg_data_out <= slv_reg440;
	      when b"110111001" =>
	        reg_data_out <= slv_reg441;
	      when b"110111010" =>
	        reg_data_out <= slv_reg442;
	      when b"110111011" =>
	        reg_data_out <= slv_reg443;
	      when b"110111100" =>
	        reg_data_out <= slv_reg444;
	      when b"110111101" =>
	        reg_data_out <= slv_reg445;
	      when b"110111110" =>
	        reg_data_out <= slv_reg446;
	      when b"110111111" =>
	        reg_data_out <= slv_reg447;
	      when b"111000000" =>
	        reg_data_out <= slv_reg448;
	      when b"111000001" =>
	        reg_data_out <= slv_reg449;
	      when b"111000010" =>
	        reg_data_out <= slv_reg450;
	      when b"111000011" =>
	        reg_data_out <= slv_reg451;
	      when b"111000100" =>
	        reg_data_out <= slv_reg452;
	      when b"111000101" =>
	        reg_data_out <= slv_reg453;
	      when b"111000110" =>
	        reg_data_out <= slv_reg454;
	      when b"111000111" =>
	        reg_data_out <= slv_reg455;
	      when b"111001000" =>
	        reg_data_out <= slv_reg456;
	      when b"111001001" =>
	        reg_data_out <= slv_reg457;
	      when b"111001010" =>
	        reg_data_out <= slv_reg458;
	      when b"111001011" =>
	        reg_data_out <= slv_reg459;
	      when b"111001100" =>
	        reg_data_out <= slv_reg460;
	      when b"111001101" =>
	        reg_data_out <= slv_reg461;
	      when b"111001110" =>
	        reg_data_out <= slv_reg462;
	      when b"111001111" =>
	        reg_data_out <= slv_reg463;
	      when b"111010000" =>
	        reg_data_out <= slv_reg464;
	      when b"111010001" =>
	        reg_data_out <= slv_reg465;
	      when b"111010010" =>
	        reg_data_out <= slv_reg466;
	      when b"111010011" =>
	        reg_data_out <= slv_reg467;
	      when b"111010100" =>
	        reg_data_out <= slv_reg468;
	      when b"111010101" =>
	        reg_data_out <= slv_reg469;
	      when b"111010110" =>
	        reg_data_out <= slv_reg470;
	      when b"111010111" =>
	        reg_data_out <= slv_reg471;
	      when b"111011000" =>
	        reg_data_out <= slv_reg472;
	      when b"111011001" =>
	        reg_data_out <= slv_reg473;
	      when b"111011010" =>
	        reg_data_out <= slv_reg474;
	      when b"111011011" =>
	        reg_data_out <= slv_reg475;
	      when b"111011100" =>
	        reg_data_out <= slv_reg476;
	      when b"111011101" =>
	        reg_data_out <= slv_reg477;
	      when b"111011110" =>
	        reg_data_out <= slv_reg478;
	      when b"111011111" =>
	        reg_data_out <= slv_reg479;
	      when b"111100000" =>
	        reg_data_out <= slv_reg480;
	      when b"111100001" =>
	        reg_data_out <= slv_reg481;
	      when b"111100010" =>
	        reg_data_out <= slv_reg482;
	      when b"111100011" =>
	        reg_data_out <= slv_reg483;
	      when b"111100100" =>
	        reg_data_out <= slv_reg484;
	      when b"111100101" =>
	        reg_data_out <= slv_reg485;
	      when b"111100110" =>
	        reg_data_out <= slv_reg486;
	      when b"111100111" =>
	        reg_data_out <= slv_reg487;
	      when b"111101000" =>
	        reg_data_out <= slv_reg488;
	      when b"111101001" =>
	        reg_data_out <= slv_reg489;
	      when b"111101010" =>
	        reg_data_out <= slv_reg490;
	      when b"111101011" =>
	        reg_data_out <= slv_reg491;
	      when b"111101100" =>
	        reg_data_out <= slv_reg492;
	      when b"111101101" =>
	        reg_data_out <= slv_reg493;
	      when b"111101110" =>
	        reg_data_out <= slv_reg494;
	      when b"111101111" =>
	        reg_data_out <= slv_reg495;
	      when b"111110000" =>
	        reg_data_out <= slv_reg496;
	      when b"111110001" =>
	        reg_data_out <= slv_reg497;
	      when b"111110010" =>
	        reg_data_out <= slv_reg498;
	      when b"111110011" =>
	        reg_data_out <= slv_reg499;
	      when b"111110100" =>
	        reg_data_out <= slv_reg500;
	      when b"111110101" =>
	        reg_data_out <= slv_reg501;
	      when b"111110110" =>
	        reg_data_out <= slv_reg502;
	      when b"111110111" =>
	        reg_data_out <= slv_reg503;
	      when b"111111000" =>
	        reg_data_out <= slv_reg504;
	      when b"111111001" =>
	        reg_data_out <= slv_reg505;
	      when b"111111010" =>
	        reg_data_out <= slv_reg506;
	      when b"111111011" =>
	        reg_data_out <= slv_reg507;
	      when b"111111100" =>
	        reg_data_out <= slv_reg508;
	      when b"111111101" =>
	        reg_data_out <= slv_reg509;
	      when b"111111110" =>
	        reg_data_out <= slv_reg510;
	      when b"111111111" =>
	        reg_data_out <= slv_reg511;
	      when others =>
	        reg_data_out  <= (others => '0');
	    end case;
	    
	   end process;
	    
	    
	    
    process (S_AXI_ACLK)
      begin 	 
      if rising_edge(S_AXI_ACLK) then
      -- Dispatcher
      ------------------------------------------------------------------------------------------------------  
       if (dispatcher_read_enable = '1') then     
          case dispatcher_address(8 downto 0) is          
             when b"000011110" =>
               dispatcher_data_out <= slv_reg30;
             when b"000011111" =>
               dispatcher_data_out <= slv_reg31;
             when b"000100000" =>
               dispatcher_data_out <= slv_reg32;
             when b"000100001" =>
               dispatcher_data_out <= slv_reg33;
             when b"000100010" =>
               dispatcher_data_out <= slv_reg34;
             when b"000100011" =>
               dispatcher_data_out <= slv_reg35;
             when b"000100100" =>
               dispatcher_data_out <= slv_reg36;
             when b"000100101" =>
               dispatcher_data_out <= slv_reg37;
             when b"000100110" =>
               dispatcher_data_out <= slv_reg38;
             when b"000100111" =>
               dispatcher_data_out <= slv_reg39;
             when b"000101000" =>
               dispatcher_data_out <= slv_reg40;
             when b"000101001" =>
               dispatcher_data_out <= slv_reg41;
             when b"000101010" =>
               dispatcher_data_out <= slv_reg42;
             when b"000101011" =>
               dispatcher_data_out <= slv_reg43;
             when b"000101100" =>
               dispatcher_data_out <= slv_reg44;
             when b"000101101" =>
               dispatcher_data_out <= slv_reg45;
             when b"000101110" =>
               dispatcher_data_out <= slv_reg46;
             when b"000101111" =>
               dispatcher_data_out <= slv_reg47;
             when b"000110000" =>
               dispatcher_data_out <= slv_reg48;
             when b"000110001" =>
               dispatcher_data_out <= slv_reg49;
             when b"000110010" =>
               dispatcher_data_out <= slv_reg50;
             when b"000110011" =>
               dispatcher_data_out <= slv_reg51;
             when b"000110100" =>
               dispatcher_data_out <= slv_reg52;
             when b"000110101" =>
               dispatcher_data_out <= slv_reg53;
             when b"000110110" =>
               dispatcher_data_out <= slv_reg54;
             when b"000110111" =>
               dispatcher_data_out <= slv_reg55;
             when b"000111000" =>
               dispatcher_data_out <= slv_reg56;
             when b"000111001" =>
               dispatcher_data_out <= slv_reg57;
             when b"000111010" =>
               dispatcher_data_out <= slv_reg58;
             when b"000111011" =>
               dispatcher_data_out <= slv_reg59;
             when b"000111100" =>
               dispatcher_data_out <= slv_reg60;
             when b"000111101" =>
               dispatcher_data_out <= slv_reg61;
             when b"000111110" =>
               dispatcher_data_out <= slv_reg62;
             when b"000111111" =>
               dispatcher_data_out <= slv_reg63;
             when b"001000000" =>
               dispatcher_data_out <= slv_reg64;
             when b"001000001" =>
               dispatcher_data_out <= slv_reg65;
             when b"001000010" =>
               dispatcher_data_out <= slv_reg66;
             when b"001000011" =>
               dispatcher_data_out <= slv_reg67;
             when b"001000100" =>
               dispatcher_data_out <= slv_reg68;
             when b"001000101" =>
               dispatcher_data_out <= slv_reg69;
             when b"001000110" =>
               dispatcher_data_out <= slv_reg70;
             when b"001000111" =>
               dispatcher_data_out <= slv_reg71;
             when b"001001000" =>
               dispatcher_data_out <= slv_reg72;
             when b"001001001" =>
               dispatcher_data_out <= slv_reg73;
             when b"001001010" =>
               dispatcher_data_out <= slv_reg74;
             when b"001001011" =>
               dispatcher_data_out <= slv_reg75;
             when b"001001100" =>
               dispatcher_data_out <= slv_reg76;
             when b"001001101" =>
               dispatcher_data_out <= slv_reg77;
             when b"001001110" =>
               dispatcher_data_out <= slv_reg78;
             when b"001001111" =>
               dispatcher_data_out <= slv_reg79;
             when b"001010000" =>
               dispatcher_data_out <= slv_reg80;
             when b"001010001" =>
               dispatcher_data_out <= slv_reg81;
             when b"001010010" =>
               dispatcher_data_out <= slv_reg82;
             when b"001010011" =>
               dispatcher_data_out <= slv_reg83;
             when b"001010100" =>
               dispatcher_data_out <= slv_reg84;
             when b"001010101" =>
               dispatcher_data_out <= slv_reg85;
             when b"001010110" =>
               dispatcher_data_out <= slv_reg86;
             when b"001010111" =>
               dispatcher_data_out <= slv_reg87;
             when b"001011000" =>
               dispatcher_data_out <= slv_reg88;
             when b"001011001" =>
               dispatcher_data_out <= slv_reg89;
             when b"001011010" =>
               dispatcher_data_out <= slv_reg90;
             when b"001011011" =>
               dispatcher_data_out <= slv_reg91;
             when b"001011100" =>
               dispatcher_data_out <= slv_reg92;
             when b"001011101" =>
               dispatcher_data_out <= slv_reg93;
             when b"001011110" =>
               dispatcher_data_out <= slv_reg94;
             when b"001011111" =>
               dispatcher_data_out <= slv_reg95;
             when b"001100000" =>
               dispatcher_data_out <= slv_reg96;
             when b"001100001" =>
               dispatcher_data_out <= slv_reg97;
             when b"001100010" =>
               dispatcher_data_out <= slv_reg98;
             when b"001100011" =>
               dispatcher_data_out <= slv_reg99;
             when b"001100100" =>
               dispatcher_data_out <= slv_reg100;
             when b"001100101" =>
               dispatcher_data_out <= slv_reg101;
             when b"001100110" =>
               dispatcher_data_out <= slv_reg102;
             when b"001100111" =>
               dispatcher_data_out <= slv_reg103;
             when b"001101000" =>
               dispatcher_data_out <= slv_reg104;
             when b"001101001" =>
               dispatcher_data_out <= slv_reg105;
             when b"001101010" =>
               dispatcher_data_out <= slv_reg106;
             when b"001101011" =>
               dispatcher_data_out <= slv_reg107;
             when b"001101100" =>
               dispatcher_data_out <= slv_reg108;
             when b"001101101" =>
               dispatcher_data_out <= slv_reg109;
             when b"001101110" =>
               dispatcher_data_out <= slv_reg110;
             when b"001101111" =>
               dispatcher_data_out <= slv_reg111;
             when b"001110000" =>
               dispatcher_data_out <= slv_reg112;
             when b"001110001" =>
               dispatcher_data_out <= slv_reg113;
             when b"001110010" =>
               dispatcher_data_out <= slv_reg114;
             when b"001110011" =>
               dispatcher_data_out <= slv_reg115;
             when b"001110100" =>
               dispatcher_data_out <= slv_reg116;
             when b"001110101" =>
               dispatcher_data_out <= slv_reg117;
             when b"001110110" =>
               dispatcher_data_out <= slv_reg118;
             when b"001110111" =>
               dispatcher_data_out <= slv_reg119;
             when b"001111000" =>
               dispatcher_data_out <= slv_reg120;
             when b"001111001" =>
               dispatcher_data_out <= slv_reg121;
             when b"001111010" =>
               dispatcher_data_out <= slv_reg122;
             when b"001111011" =>
               dispatcher_data_out <= slv_reg123;
             when b"001111100" =>
               dispatcher_data_out <= slv_reg124;
             when b"001111101" =>
               dispatcher_data_out <= slv_reg125;
             when b"001111110" =>
               dispatcher_data_out <= slv_reg126;
             when b"001111111" =>
               dispatcher_data_out <= slv_reg127;
             when b"010000000" =>
               dispatcher_data_out <= slv_reg128;
             when b"010000001" =>
               dispatcher_data_out <= slv_reg129;
             when b"010000010" =>
               dispatcher_data_out <= slv_reg130;
             when b"010000011" =>
               dispatcher_data_out <= slv_reg131;
             when b"010000100" =>
               dispatcher_data_out <= slv_reg132;
             when b"010000101" =>
               dispatcher_data_out <= slv_reg133;
             when b"010000110" =>
               dispatcher_data_out <= slv_reg134;
             when b"010000111" =>
               dispatcher_data_out <= slv_reg135;
             when b"010001000" =>
               dispatcher_data_out <= slv_reg136;
             when b"010001001" =>
               dispatcher_data_out <= slv_reg137;
             when b"010001010" =>
               dispatcher_data_out <= slv_reg138;
             when b"010001011" =>
               dispatcher_data_out <= slv_reg139;
             when b"010001100" =>
               dispatcher_data_out <= slv_reg140;
             when b"010001101" =>
               dispatcher_data_out <= slv_reg141;
             when b"010001110" =>
               dispatcher_data_out <= slv_reg142;
             when b"010001111" =>
               dispatcher_data_out <= slv_reg143;
             when b"010010000" =>
               dispatcher_data_out <= slv_reg144;
             when b"010010001" =>
               dispatcher_data_out <= slv_reg145;
             when b"010010010" =>
               dispatcher_data_out <= slv_reg146;
             when b"010010011" =>
               dispatcher_data_out <= slv_reg147;
             when b"010010100" =>
               dispatcher_data_out <= slv_reg148;
             when b"010010101" =>
               dispatcher_data_out <= slv_reg149;
             when b"010010110" =>
               dispatcher_data_out <= slv_reg150;
             when b"010010111" =>
               dispatcher_data_out <= slv_reg151;
             when b"010011000" =>
               dispatcher_data_out <= slv_reg152;
             when b"010011001" =>
               dispatcher_data_out <= slv_reg153;
             when b"010011010" =>
               dispatcher_data_out <= slv_reg154;
             when b"010011011" =>
               dispatcher_data_out <= slv_reg155;
             when b"010011100" =>
               dispatcher_data_out <= slv_reg156;
             when b"010011101" =>
               dispatcher_data_out <= slv_reg157;
             when b"010011110" =>
               dispatcher_data_out <= slv_reg158;
             when b"010011111" =>
               dispatcher_data_out <= slv_reg159;
             when b"010100000" =>
               dispatcher_data_out <= slv_reg160;
             when b"010100001" =>
               dispatcher_data_out <= slv_reg161;
             when b"010100010" =>
               dispatcher_data_out <= slv_reg162;
             when b"010100011" =>
               dispatcher_data_out <= slv_reg163;
             when b"010100100" =>
               dispatcher_data_out <= slv_reg164;
             when b"010100101" =>
               dispatcher_data_out <= slv_reg165;
             when b"010100110" =>
               dispatcher_data_out <= slv_reg166;
             when b"010100111" =>
               dispatcher_data_out <= slv_reg167;
             when b"010101000" =>
               dispatcher_data_out <= slv_reg168;
             when b"010101001" =>
               dispatcher_data_out <= slv_reg169;
             when b"010101010" =>
               dispatcher_data_out <= slv_reg170;
             when b"010101011" =>
               dispatcher_data_out <= slv_reg171;
             when b"010101100" =>
               dispatcher_data_out <= slv_reg172;
             when b"010101101" =>
               dispatcher_data_out <= slv_reg173;
             when b"010101110" =>
               dispatcher_data_out <= slv_reg174;
             when b"010101111" =>
               dispatcher_data_out <= slv_reg175;
             when b"010110000" =>
               dispatcher_data_out <= slv_reg176;
             when b"010110001" =>
               dispatcher_data_out <= slv_reg177;
             when b"010110010" =>
               dispatcher_data_out <= slv_reg178;
             when b"010110011" =>
               dispatcher_data_out <= slv_reg179;
             when b"010110100" =>
               dispatcher_data_out <= slv_reg180;
             when b"010110101" =>
               dispatcher_data_out <= slv_reg181;
             when b"010110110" =>
               dispatcher_data_out <= slv_reg182;
             when b"010110111" =>
               dispatcher_data_out <= slv_reg183;
             when b"010111000" =>
               dispatcher_data_out <= slv_reg184;
             when b"010111001" =>
               dispatcher_data_out <= slv_reg185;
             when b"010111010" =>
               dispatcher_data_out <= slv_reg186;
             when b"010111011" =>
               dispatcher_data_out <= slv_reg187;
             when b"010111100" =>
               dispatcher_data_out <= slv_reg188;
             when b"010111101" =>
               dispatcher_data_out <= slv_reg189;
             when b"010111110" =>
               dispatcher_data_out <= slv_reg190;
             when b"010111111" =>
               dispatcher_data_out <= slv_reg191;
             when b"011000000" =>
               dispatcher_data_out <= slv_reg192;
             when b"011000001" =>
               dispatcher_data_out <= slv_reg193;
             when b"011000010" =>
               dispatcher_data_out <= slv_reg194;
             when b"011000011" =>
               dispatcher_data_out <= slv_reg195;
             when b"011000100" =>
               dispatcher_data_out <= slv_reg196;
             when b"011000101" =>
               dispatcher_data_out <= slv_reg197;
             when b"011000110" =>
               dispatcher_data_out <= slv_reg198;
             when b"011000111" =>
               dispatcher_data_out <= slv_reg199;
             when b"011001000" =>
               dispatcher_data_out <= slv_reg200;
             when b"011001001" =>
               dispatcher_data_out <= slv_reg201;
             when b"011001010" =>
               dispatcher_data_out <= slv_reg202;
             when b"011001011" =>
               dispatcher_data_out <= slv_reg203;
             when b"011001100" =>
               dispatcher_data_out <= slv_reg204;
             when b"011001101" =>
               dispatcher_data_out <= slv_reg205;
             when b"011001110" =>
               dispatcher_data_out <= slv_reg206;
             when b"011001111" =>
               dispatcher_data_out <= slv_reg207;
             when b"011010000" =>
               dispatcher_data_out <= slv_reg208;
             when b"011010001" =>
               dispatcher_data_out <= slv_reg209;
             when b"011010010" =>
               dispatcher_data_out <= slv_reg210;
             when b"011010011" =>
               dispatcher_data_out <= slv_reg211;
             when b"011010100" =>
               dispatcher_data_out <= slv_reg212;
             when b"011010101" =>
               dispatcher_data_out <= slv_reg213;
             when b"011010110" =>
               dispatcher_data_out <= slv_reg214;
             when b"011010111" =>
               dispatcher_data_out <= slv_reg215;
             when b"011011000" =>
               dispatcher_data_out <= slv_reg216;
             when b"011011001" =>
               dispatcher_data_out <= slv_reg217;
             when b"011011010" =>
               dispatcher_data_out <= slv_reg218;
             when b"011011011" =>
               dispatcher_data_out <= slv_reg219;
             when b"011011100" =>
               dispatcher_data_out <= slv_reg220;
             when b"011011101" =>
               dispatcher_data_out <= slv_reg221;
             when b"011011110" =>
               dispatcher_data_out <= slv_reg222;
             when b"011011111" =>
               dispatcher_data_out <= slv_reg223;
             when b"011100000" =>
               dispatcher_data_out <= slv_reg224;
             when b"011100001" =>
               dispatcher_data_out <= slv_reg225;
             when b"011100010" =>
               dispatcher_data_out <= slv_reg226;
             when b"011100011" =>
               dispatcher_data_out <= slv_reg227;
             when b"011100100" =>
               dispatcher_data_out <= slv_reg228;
             when b"011100101" =>
               dispatcher_data_out <= slv_reg229;
             when b"011100110" =>
               dispatcher_data_out <= slv_reg230;
             when b"011100111" =>
               dispatcher_data_out <= slv_reg231;
             when b"011101000" =>
               dispatcher_data_out <= slv_reg232;
             when b"011101001" =>
               dispatcher_data_out <= slv_reg233;
             when b"011101010" =>
               dispatcher_data_out <= slv_reg234;
             when b"011101011" =>
               dispatcher_data_out <= slv_reg235;
             when b"011101100" =>
               dispatcher_data_out <= slv_reg236;
             when b"011101101" =>
               dispatcher_data_out <= slv_reg237;
             when b"011101110" =>
               dispatcher_data_out <= slv_reg238;
             when b"011101111" =>
               dispatcher_data_out <= slv_reg239;
             when b"011110000" =>
               dispatcher_data_out <= slv_reg240;
             when others =>  
               dispatcher_data_out <= x"00000000";
             end case;
          end if;  
               
          -- TMC
          ------------------------------------------------------------------------------------------------------   
          if (tmc_read_enable = '1') then     
           case tmc_address(8 downto 0) is    
             when b"011110001" =>
               tmc_data_out <= slv_reg241;
             when b"011110010" =>
               tmc_data_out <= slv_reg242;
             when b"011110011" =>
               tmc_data_out <= slv_reg243;
             when b"011110100" =>
               tmc_data_out <= slv_reg244;
             when b"011110101" =>
               tmc_data_out <= slv_reg245;
             when b"011110110" =>
               tmc_data_out <= slv_reg246;
             when b"011110111" =>
               tmc_data_out <= slv_reg247;
             when b"011111000" =>
               tmc_data_out <= slv_reg248;
             when b"011111001" =>
               tmc_data_out <= slv_reg249;
             when b"011111010" =>
               tmc_data_out <= slv_reg250;
             when b"011111011" =>
               tmc_data_out <= slv_reg251;
             when b"011111100" =>
               tmc_data_out <= slv_reg252;
             when b"011111101" =>
               tmc_data_out <= slv_reg253;
             when b"011111110" =>
               tmc_data_out <= slv_reg254;
             when b"011111111" =>
               tmc_data_out <= slv_reg255;
             when b"100000000" =>
               tmc_data_out <= slv_reg256;
             when b"100000001" =>
               tmc_data_out <= slv_reg257;
             when b"100000010" =>
               tmc_data_out <= slv_reg258;
             when b"100000011" =>
               tmc_data_out <= slv_reg259;
             when b"100000100" =>
               tmc_data_out <= slv_reg260;
             when b"100000101" =>
               tmc_data_out <= slv_reg261;
             when b"100000110" =>
               tmc_data_out <= slv_reg262;
             when b"100000111" =>
               tmc_data_out <= slv_reg263;
             when b"100001000" =>
               tmc_data_out <= slv_reg264;
             when b"100001001" =>
               tmc_data_out <= slv_reg265;
             when b"100001010" =>
               tmc_data_out <= slv_reg266;
             when b"100001011" =>
               tmc_data_out <= slv_reg267;
             when b"100001100" =>
               tmc_data_out <= slv_reg268;
             when b"100001101" =>
               tmc_data_out <= slv_reg269;
             when b"100001110" =>
               tmc_data_out <= slv_reg270;
             when b"100001111" =>
               tmc_data_out <= slv_reg271;   
             when b"100010000" =>
               tmc_data_out <= slv_reg272;
             when b"100010001" =>
               tmc_data_out <= slv_reg273;
             when b"100010010" =>
               tmc_data_out <= slv_reg274;
             when b"100010011" =>
               tmc_data_out <= slv_reg275;
             when b"100010100" =>
               tmc_data_out <= slv_reg276;
             when b"100010101" =>
               tmc_data_out <= slv_reg277;
             when b"100010110" =>
               tmc_data_out <= slv_reg278;
             when b"100010111" =>
               tmc_data_out <= slv_reg279;
             when b"100011000" =>
               tmc_data_out <= slv_reg280;
             when b"100011001" =>
               tmc_data_out <= slv_reg281;
             when b"100011010" =>
               tmc_data_out <= slv_reg282;
             when b"100011011" =>
               tmc_data_out <= slv_reg283;
             when b"100011100" =>
               tmc_data_out <= slv_reg284;
             when b"100011101" =>
               tmc_data_out <= slv_reg285;
             when b"100011110" =>
               tmc_data_out <= slv_reg286;
             when b"100011111" =>
               tmc_data_out <= slv_reg287;
             when b"100100000" =>
               tmc_data_out <= slv_reg288;
             when b"100100001" =>
               tmc_data_out <= slv_reg289;
             when b"100100010" =>
               tmc_data_out <= slv_reg290;
             when b"100100011" =>
               tmc_data_out <= slv_reg291;
             when b"100100100" =>
               tmc_data_out <= slv_reg292;
             when b"100100101" =>
               tmc_data_out <= slv_reg293;
             when b"100100110" =>
               tmc_data_out <= slv_reg294;
             when b"100100111" =>
               tmc_data_out <= slv_reg295;
             when b"100101000" =>
               tmc_data_out <= slv_reg296;
             when b"100101001" =>
               tmc_data_out <= slv_reg297;
             when b"100101010" =>
               tmc_data_out <= slv_reg298;
             when b"100101011" =>
               tmc_data_out <= slv_reg299;
             when b"100101100" =>
               tmc_data_out <= slv_reg300;
             when b"100101101" =>
               tmc_data_out <= slv_reg301;
             when b"100101110" =>
               tmc_data_out <= slv_reg302;
             when b"100101111" =>
               tmc_data_out <= slv_reg303;
             when b"100110000" =>
               tmc_data_out <= slv_reg304;
             when b"100110001" =>
               tmc_data_out <= slv_reg305;
             when b"100110010" =>
               tmc_data_out <= slv_reg306;
             when b"100110011" =>
               tmc_data_out <= slv_reg307;
             when b"100110100" =>
               tmc_data_out <= slv_reg308;
             when b"100110101" =>
               tmc_data_out <= slv_reg309;
             when b"100110110" =>
               tmc_data_out <= slv_reg310;
             when b"100110111" =>
               tmc_data_out <= slv_reg311;
             when b"100111000" =>
               tmc_data_out <= slv_reg312;
             when b"100111001" =>
               tmc_data_out <= slv_reg313;
             when b"100111010" =>
               tmc_data_out <= slv_reg314;
             when b"100111011" =>
               tmc_data_out <= slv_reg315;
             when b"100111100" =>
               tmc_data_out <= slv_reg316;
             when b"100111101" =>
               tmc_data_out <= slv_reg317;
             when b"100111110" =>
               tmc_data_out <= slv_reg318;
             when b"100111111" =>
               tmc_data_out <= slv_reg319;
             when b"101000000" =>
               tmc_data_out <= slv_reg320;
             when b"101000001" =>
               tmc_data_out <= slv_reg321;
             when b"101000010" =>
               tmc_data_out <= slv_reg322;
             when b"101000011" =>
               tmc_data_out <= slv_reg323;
             when b"101000100" =>
               tmc_data_out <= slv_reg324;
             when b"101000101" =>
               tmc_data_out <= slv_reg325;
             when b"101000110" =>
               tmc_data_out <= slv_reg326;
             when b"101000111" =>
               tmc_data_out <= slv_reg327;
             when b"101001000" =>
               tmc_data_out <= slv_reg328;
             when b"101001001" =>
               tmc_data_out <= slv_reg329;
             when b"101001010" =>
               tmc_data_out <= slv_reg330;
             when b"101001011" =>
               tmc_data_out <= slv_reg331;
             when b"101001100" =>
               tmc_data_out <= slv_reg332;
             when b"101001101" =>
               tmc_data_out <= slv_reg333;
             when b"101001110" =>
               tmc_data_out <= slv_reg334;
             when b"101001111" =>
               tmc_data_out <= slv_reg335;
             when b"101010000" =>
               tmc_data_out <= slv_reg336;
             when b"101010001" =>
               tmc_data_out <= slv_reg337;
             when b"101010010" =>
               tmc_data_out <= slv_reg338;
             when b"101010011" =>
               tmc_data_out <= slv_reg339;
             when b"101010100" =>
               tmc_data_out <= slv_reg340;
             when b"101010101" =>
               tmc_data_out <= slv_reg341;
             when b"101010110" =>
               tmc_data_out <= slv_reg342;
             when b"101010111" =>
               tmc_data_out <= slv_reg343;
             when b"101011000" =>
               tmc_data_out <= slv_reg344;
             when b"101011001" =>
               tmc_data_out <= slv_reg345;
             when b"101011010" =>
               tmc_data_out <= slv_reg346;
             when b"101011011" =>
               tmc_data_out <= slv_reg347;
             when b"101011100" =>
               tmc_data_out <= slv_reg348;
             when b"101011101" =>
               tmc_data_out <= slv_reg349;
             when b"101011110" =>
               tmc_data_out <= slv_reg350;
             when b"101011111" =>
               tmc_data_out <= slv_reg351;
             when b"101100000" =>
               tmc_data_out <= slv_reg352;
             when b"101100001" =>
               tmc_data_out <= slv_reg353;
             when b"101100010" =>
               tmc_data_out <= slv_reg354;
             when b"101100011" =>
               tmc_data_out <= slv_reg355;
             when b"101100100" =>
               tmc_data_out <= slv_reg356;
             when b"101100101" =>
               tmc_data_out <= slv_reg357;
             when b"101100110" =>
               tmc_data_out <= slv_reg358;
             when b"101100111" =>
               tmc_data_out <= slv_reg359;
             when b"101101000" =>
               tmc_data_out <= slv_reg360;
             when b"101101001" =>
               tmc_data_out <= slv_reg361;
             when b"101101010" =>
               tmc_data_out <= slv_reg362;
             when b"101101011" =>
               tmc_data_out <= slv_reg363;
             when b"101101100" =>
               tmc_data_out <= slv_reg364;
             when b"101101101" =>
               tmc_data_out <= slv_reg365;
             when b"101101110" =>
               tmc_data_out <= slv_reg366;
             when b"101101111" =>
               tmc_data_out <= slv_reg367;
             when b"101110000" =>
               tmc_data_out <= slv_reg368;
             when b"101110001" =>
               tmc_data_out <= slv_reg369;
             when b"101110010" =>
               tmc_data_out <= slv_reg370;
             when b"101110011" =>
               tmc_data_out <= slv_reg371;
             when b"101110100" =>
               tmc_data_out <= slv_reg372;
             when b"101110101" =>
               tmc_data_out <= slv_reg373;
             when b"101110110" =>
               tmc_data_out <= slv_reg374;
             when b"101110111" =>
               tmc_data_out <= slv_reg375;
             when b"101111000" =>
               tmc_data_out <= slv_reg376;
             when b"101111001" =>
               tmc_data_out <= slv_reg377;
             when b"101111010" =>
               tmc_data_out <= slv_reg378;
             when b"101111011" =>
               tmc_data_out <= slv_reg379;
             when b"101111100" =>
               tmc_data_out <= slv_reg380;
             when b"101111101" =>
               tmc_data_out <= slv_reg381;
             when b"101111110" =>
               tmc_data_out <= slv_reg382;
             when b"101111111" =>
               tmc_data_out <= slv_reg383;
             when b"110000000" =>
               tmc_data_out <= slv_reg384;
             when b"110000001" =>
               tmc_data_out <= slv_reg385;
             when b"110000010" =>
               tmc_data_out <= slv_reg386;
             when b"110000011" =>
               tmc_data_out <= slv_reg387;
             when b"110000100" =>
               tmc_data_out <= slv_reg388;
             when b"110000101" =>
               tmc_data_out <= slv_reg389;
             when b"110000110" =>
               tmc_data_out <= slv_reg390;
             when b"110000111" =>
               tmc_data_out <= slv_reg391;
             when b"110001000" =>
               tmc_data_out <= slv_reg392;
             when b"110001001" =>
               tmc_data_out <= slv_reg393;
             when b"110001010" =>
               tmc_data_out <= slv_reg394;
             when b"110001011" =>
               tmc_data_out <= slv_reg395;
             when b"110001100" =>
               tmc_data_out <= slv_reg396;
             when b"110001101" =>
               tmc_data_out <= slv_reg397;
             when b"110001110" =>
               tmc_data_out <= slv_reg398;
             when b"110001111" =>
               tmc_data_out <= slv_reg399;
             when b"110010000" =>
               tmc_data_out <= slv_reg400;
             when b"110010001" =>
               tmc_data_out <= slv_reg401;
             when b"110010010" =>
               tmc_data_out <= slv_reg402;
             when b"110010011" =>
               tmc_data_out <= slv_reg403;
             when b"110010100" =>
               tmc_data_out <= slv_reg404;
             when b"110010101" =>
               tmc_data_out <= slv_reg405;
             when b"110010110" =>
               tmc_data_out <= slv_reg406;
             when b"110010111" =>
               tmc_data_out <= slv_reg407;
             when b"110011000" =>
               tmc_data_out <= slv_reg408;
             when b"110011001" =>
               tmc_data_out <= slv_reg409;
             when b"110011010" =>
               tmc_data_out <= slv_reg410;
             when b"110011011" =>
               tmc_data_out <= slv_reg411;
             when b"110011100" =>
               tmc_data_out <= slv_reg412;
             when b"110011101" =>
               tmc_data_out <= slv_reg413;
             when b"110011110" =>
               tmc_data_out <= slv_reg414;
             when b"110011111" =>
               tmc_data_out <= slv_reg415;
             when b"110100000" =>
               tmc_data_out <= slv_reg416;
             when b"110100001" =>
               tmc_data_out <= slv_reg417;
             when b"110100010" =>
               tmc_data_out <= slv_reg418;
             when b"110100011" =>
               tmc_data_out <= slv_reg419;
             when b"110100100" =>
               tmc_data_out <= slv_reg420;
             when b"110100101" =>
               tmc_data_out <= slv_reg421;
             when b"110100110" =>
               tmc_data_out <= slv_reg422;
             when b"110100111" =>
               tmc_data_out <= slv_reg423;
             when b"110101000" =>
               tmc_data_out <= slv_reg424;
             when b"110101001" =>
               tmc_data_out <= slv_reg425;
             when b"110101010" =>
               tmc_data_out <= slv_reg426;
             when b"110101011" =>
               tmc_data_out <= slv_reg427;
             when b"110101100" =>
               tmc_data_out <= slv_reg428;
             when b"110101101" =>
               tmc_data_out <= slv_reg429;
             when b"110101110" =>
               tmc_data_out <= slv_reg430;
             when b"110101111" =>
               tmc_data_out <= slv_reg431;
             when b"110110000" =>
               tmc_data_out <= slv_reg432;
             when b"110110001" =>
               tmc_data_out <= slv_reg433;
             when b"110110010" =>
               tmc_data_out <= slv_reg434;
             when b"110110011" =>
               tmc_data_out <= slv_reg435;
             when b"110110100" =>
               tmc_data_out <= slv_reg436;
             when b"110110101" =>
               tmc_data_out <= slv_reg437;
             when b"110110110" =>
               tmc_data_out <= slv_reg438;
             when b"110110111" =>
               tmc_data_out <= slv_reg439;
             when b"110111000" =>
               tmc_data_out <= slv_reg440;
             when b"110111001" =>
               tmc_data_out <= slv_reg441;
             when b"110111010" =>
               tmc_data_out <= slv_reg442;
             when b"110111011" =>
               tmc_data_out <= slv_reg443;
             when b"110111100" =>
               tmc_data_out <= slv_reg444;
             when b"110111101" =>
               tmc_data_out <= slv_reg445;
             when b"110111110" =>
               tmc_data_out <= slv_reg446;
             when b"110111111" =>
               tmc_data_out <= slv_reg447;
             when b"111000000" =>
               tmc_data_out <= slv_reg448;
             when b"111000001" =>
               tmc_data_out <= slv_reg449;
             when b"111000010" =>
               tmc_data_out <= slv_reg450;
             when b"111000011" =>
               tmc_data_out <= slv_reg451;
             when b"111000100" =>
               tmc_data_out <= slv_reg452;
             when b"111000101" =>
               tmc_data_out <= slv_reg453;
             when b"111000110" =>
               tmc_data_out <= slv_reg454;
             when b"111000111" =>
               tmc_data_out <= slv_reg455;
             when b"111001000" =>
               tmc_data_out <= slv_reg456;
             when b"111001001" =>
               tmc_data_out <= slv_reg457;
             when b"111001010" =>
               tmc_data_out <= slv_reg458;
             when b"111001011" =>
               tmc_data_out <= slv_reg459;
             when b"111001100" =>
               tmc_data_out <= slv_reg460;
             when b"111001101" =>
               tmc_data_out <= slv_reg461;
             when b"111001110" =>
               tmc_data_out <= slv_reg462;
             when b"111001111" =>
               tmc_data_out <= slv_reg463;
             when b"111010000" =>
               tmc_data_out <= slv_reg464;
             when b"111010001" =>
               tmc_data_out <= slv_reg465;
             when b"111010010" =>
               tmc_data_out <= slv_reg466;
             when b"111010011" =>
               tmc_data_out <= slv_reg467;
             when b"111010100" =>
               tmc_data_out <= slv_reg468;
             when b"111010101" =>
               tmc_data_out <= slv_reg469;
             when b"111010110" =>
               tmc_data_out <= slv_reg470;
             when b"111010111" =>
               tmc_data_out <= slv_reg471;
             when b"111011000" =>
               tmc_data_out <= slv_reg472;
             when b"111011001" =>
               tmc_data_out <= slv_reg473;
             when b"111011010" =>
               tmc_data_out <= slv_reg474;
             when b"111011011" =>
               tmc_data_out <= slv_reg475;
             when b"111011100" =>
               tmc_data_out <= slv_reg476;
             when b"111011101" =>
               tmc_data_out <= slv_reg477;
             when b"111011110" =>
               tmc_data_out <= slv_reg478;
             when b"111011111" =>
               tmc_data_out <= slv_reg479;
             when b"111100000" =>
               tmc_data_out <= slv_reg480;
             when b"111100001" =>
               tmc_data_out <= slv_reg481;
             when b"111100010" =>
               tmc_data_out <= slv_reg482;
             when b"111100011" =>
               tmc_data_out <= slv_reg483;
             when b"111100100" =>
               tmc_data_out <= slv_reg484;
             when b"111100101" =>
               tmc_data_out <= slv_reg485;
             when b"111100110" =>
               tmc_data_out <= slv_reg486;
             when b"111100111" =>
               tmc_data_out <= slv_reg487;
             when b"111101000" =>
               tmc_data_out <= slv_reg488;
             when b"111101001" =>
               tmc_data_out <= slv_reg489;
             when b"111101010" =>
               tmc_data_out <= slv_reg490;
             when b"111101011" =>
               tmc_data_out <= slv_reg491;
             when b"111101100" =>
               tmc_data_out <= slv_reg492;
             when b"111101101" =>
               tmc_data_out <= slv_reg493;
             when b"111101110" =>
               tmc_data_out <= slv_reg494;
             when b"111101111" =>
               tmc_data_out <= slv_reg495;
             when b"111110000" =>
               tmc_data_out <= slv_reg496;
             when b"111110001" =>
               tmc_data_out <= slv_reg497;
             when b"111110010" =>
               tmc_data_out <= slv_reg498;
             when b"111110011" =>
               tmc_data_out <= slv_reg499;
             when b"111110100" =>
               tmc_data_out <= slv_reg500;
             when b"111110101" =>
               tmc_data_out <= slv_reg501;
             when b"111110110" =>
               tmc_data_out <= slv_reg502;
             when b"111110111" =>
               tmc_data_out <= slv_reg503;
             when b"111111000" =>
               tmc_data_out <= slv_reg504;
             when b"111111001" =>
               tmc_data_out <= slv_reg505;
             when b"111111010" =>
               tmc_data_out <= slv_reg506;
             when b"111111011" =>
               tmc_data_out <= slv_reg507;
             when b"111111100" =>
               tmc_data_out <= slv_reg508;
             when b"111111101" =>
               tmc_data_out <= slv_reg509;
             when b"111111110" =>
               tmc_data_out <= slv_reg510;
             when b"111111111" =>
               tmc_data_out <= slv_reg511;
             when others =>
               tmc_data_out  <= (others => '0');
           end case;
	    end if;
	  end if; 
	end process; 

	-- Output register or memory read data
	process( S_AXI_ACLK ) is
	begin
	  if (rising_edge (S_AXI_ACLK)) then
	    if ( S_AXI_ARESETN = '0' ) then
	      axi_rdata  <= (others => '0');
	    else
	      if (slv_reg_rden = '1') then
	        -- When there is a valid read address (S_AXI_ARVALID) with 
	        -- acceptance of read address by the slave (axi_arready), 
	        -- output the read dada 
	        -- Read address mux
	          axi_rdata <= reg_data_out;     -- register read data
	      end if;   
	    end if;
	  end if;
	end process;


	-- Add user logic here

	-- User logic ends

end arch_imp;
