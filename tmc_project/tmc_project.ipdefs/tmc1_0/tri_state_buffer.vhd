library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity tbuff is
generic ( n: integer := 32;
	  m: integer := 26;
	  OFFSET: integer := 12);
    PORT(
        inp:IN STD_LOGIC_VECTOR(m-OFFSET-1 DOWNTO 0);
        sel:IN STD_LOGIC;
        outp:OUT STD_LOGIC_VECTOR(m-OFFSET-1 DOWNTO 0));
end tbuff;

architecture Behavioral of tbuff is
begin
    outp <= (others => '0') WHEN (sel = '0') ELSE 
	    inp;
end Behavioral;


