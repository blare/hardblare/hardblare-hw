# Compiler to be used

CC=gcc

GCC=mips-linux-gnu-gcc

CC_FLAGS=-g -fdelayed-branch -static -Wall -msoft-float -fno-builtin -fno-pic -mips1 -mno-abicalls  -fomit-frame-pointer -nostdlib -Ttext=0x00000000 -Tdata=0xE0000000

CC_FLAGS_DEBUG=-D DEBUG

OBJDUMP=mips-linux-gnu-objdump

OBJDUMP_FLAGS=--section=.text -d
