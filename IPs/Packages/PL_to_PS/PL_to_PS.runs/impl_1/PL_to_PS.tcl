# 
# Report generation script generated by Vivado
# 

proc create_report { reportName command } {
  set status "."
  append status $reportName ".fail"
  if { [file exists $status] } {
    eval file delete [glob $status]
  }
  send_msg_id runtcl-4 info "Executing : $command"
  set retval [eval catch { $command } msg]
  if { $retval != 0 } {
    set fp [open $status w]
    close $fp
    send_msg_id runtcl-5 warning "$msg"
  }
}
proc start_step { step } {
  set stopFile ".stop.rst"
  if {[file isfile .stop.rst]} {
    puts ""
    puts "*** Halting run - EA reset detected ***"
    puts ""
    puts ""
    return -code error
  }
  set beginFile ".$step.begin.rst"
  set platform "$::tcl_platform(platform)"
  set user "$::tcl_platform(user)"
  set pid [pid]
  set host ""
  if { [string equal $platform unix] } {
    if { [info exist ::env(HOSTNAME)] } {
      set host $::env(HOSTNAME)
    }
  } else {
    if { [info exist ::env(COMPUTERNAME)] } {
      set host $::env(COMPUTERNAME)
    }
  }
  set ch [open $beginFile w]
  puts $ch "<?xml version=\"1.0\"?>"
  puts $ch "<ProcessHandle Version=\"1\" Minor=\"0\">"
  puts $ch "    <Process Command=\".planAhead.\" Owner=\"$user\" Host=\"$host\" Pid=\"$pid\">"
  puts $ch "    </Process>"
  puts $ch "</ProcessHandle>"
  close $ch
}

proc end_step { step } {
  set endFile ".$step.end.rst"
  set ch [open $endFile w]
  close $ch
}

proc step_failed { step } {
  set endFile ".$step.error.rst"
  set ch [open $endFile w]
  close $ch
}


start_step init_design
set ACTIVE_STEP init_design
set rc [catch {
  create_msg_db init_design.pb
  create_project -in_memory -part xc7z020clg484-1
  set_property board_part em.avnet.com:zed:part0:1.2 [current_project]
  set_property design_mode GateLvl [current_fileset]
  set_param project.singleFileAddWarning.threshold 0
  set_property webtalk.parent_dir /home/mounir/HardBlare/hw_vivado/IPs/Packages/PL_to_PS/PL_to_PS.cache/wt [current_project]
  set_property parent.project_path /home/mounir/HardBlare/hw_vivado/IPs/Packages/PL_to_PS/PL_to_PS.xpr [current_project]
  set_property ip_repo_paths {
  /home/mounir/HardBlare/hw_vivado/IPs/Packages/PL_to_PS_1.0
  /home/mounir/HardBlare/hw_vivado/IPs/Packages/PL_to_PS_1.0
  /home/mounir/HardBlare/hw_vivado/ip_repo/PL_to_PS_IRQ_1.0
  /home/mounir/HardBlare/hw_vivado/IPs/Packages/PL_to_PS_IRQ_1.0
  /home/mounir/HardBlare/hw_vivado/ip_repo/myip_1.0
  /home/mounir/HardBlare/hw_vivado/IPs
  /home/mounir/HardBlare/hw_vivado/tmc_project/tmc_project.ipdefs/rest_gen_0
  /home/mounir/HardBlare/hw_vivado/tmc_project/tmc_project.ipdefs/instrumentation_ip_1.0
  /home/mounir/HardBlare/hw_vivado/tmc_project/tmc_project.ipdefs/AXI_master2_0
  /home/mounir/HardBlare/hw_vivado/tmc_project/tmc_project.ipdefs/mips_ip_0
  /home/mounir/HardBlare/hw_vivado/tmc_project/tmc_project.ipdefs/mem_ctl_0_0
  /home/mounir/Downloads/tmc_project/tmc_project.srcs/sources_1/bd/design_1/ip/design_1_decodeur_traces_0_0
  /home/mounir/HardBlare/hw_vivado/tmc_project/tmc_project.srcs
  /home/mounir/HardBlare/hw_vivado/tmc_project/tmc_project.ipdefs
  /home/mounir/HardBlare/hw_vivado/IPs/Packages/decodeur_traces_v1_0_project
  /home/mounir/HardBlare/hw_vivado/IPs/Packages/PLtoPS
} [current_project]
  set_property ip_output_repo /home/mounir/HardBlare/hw_vivado/IPs/Packages/PL_to_PS/PL_to_PS.cache/ip [current_project]
  set_property ip_cache_permissions {read write} [current_project]
  add_files -quiet /home/mounir/HardBlare/hw_vivado/IPs/Packages/PL_to_PS/PL_to_PS.runs/synth_1/PL_to_PS.dcp
  link_design -top PL_to_PS -part xc7z020clg484-1
  close_msg_db -file init_design.pb
} RESULT]
if {$rc} {
  step_failed init_design
  return -code error $RESULT
} else {
  end_step init_design
  unset ACTIVE_STEP 
}

start_step opt_design
set ACTIVE_STEP opt_design
set rc [catch {
  create_msg_db opt_design.pb
  opt_design 
  write_checkpoint -force PL_to_PS_opt.dcp
  create_report "impl_1_opt_report_drc_0" "report_drc -file PL_to_PS_drc_opted.rpt -pb PL_to_PS_drc_opted.pb -rpx PL_to_PS_drc_opted.rpx"
  close_msg_db -file opt_design.pb
} RESULT]
if {$rc} {
  step_failed opt_design
  return -code error $RESULT
} else {
  end_step opt_design
  unset ACTIVE_STEP 
}

start_step place_design
set ACTIVE_STEP place_design
set rc [catch {
  create_msg_db place_design.pb
  if { [llength [get_debug_cores -quiet] ] > 0 }  { 
    implement_debug_core 
  } 
  place_design 
  write_checkpoint -force PL_to_PS_placed.dcp
  create_report "impl_1_place_report_io_0" "report_io -file PL_to_PS_io_placed.rpt"
  create_report "impl_1_place_report_utilization_0" "report_utilization -file PL_to_PS_utilization_placed.rpt -pb PL_to_PS_utilization_placed.pb"
  create_report "impl_1_place_report_control_sets_0" "report_control_sets -verbose -file PL_to_PS_control_sets_placed.rpt"
  close_msg_db -file place_design.pb
} RESULT]
if {$rc} {
  step_failed place_design
  return -code error $RESULT
} else {
  end_step place_design
  unset ACTIVE_STEP 
}

start_step route_design
set ACTIVE_STEP route_design
set rc [catch {
  create_msg_db route_design.pb
  route_design 
  write_checkpoint -force PL_to_PS_routed.dcp
  create_report "impl_1_route_report_drc_0" "report_drc -file PL_to_PS_drc_routed.rpt -pb PL_to_PS_drc_routed.pb -rpx PL_to_PS_drc_routed.rpx"
  create_report "impl_1_route_report_methodology_0" "report_methodology -file PL_to_PS_methodology_drc_routed.rpt -pb PL_to_PS_methodology_drc_routed.pb -rpx PL_to_PS_methodology_drc_routed.rpx"
  create_report "impl_1_route_report_power_0" "report_power -file PL_to_PS_power_routed.rpt -pb PL_to_PS_power_summary_routed.pb -rpx PL_to_PS_power_routed.rpx"
  create_report "impl_1_route_report_route_status_0" "report_route_status -file PL_to_PS_route_status.rpt -pb PL_to_PS_route_status.pb"
  create_report "impl_1_route_report_timing_summary_0" "report_timing_summary -max_paths 10 -file PL_to_PS_timing_summary_routed.rpt -pb PL_to_PS_timing_summary_routed.pb -rpx PL_to_PS_timing_summary_routed.rpx -warn_on_violation "
  create_report "impl_1_route_report_incremental_reuse_0" "report_incremental_reuse -file PL_to_PS_incremental_reuse_routed.rpt"
  create_report "impl_1_route_report_clock_utilization_0" "report_clock_utilization -file PL_to_PS_clock_utilization_routed.rpt"
  create_report "impl_1_route_report_bus_skew_0" "report_bus_skew -warn_on_violation -file PL_to_PS_bus_skew_routed.rpt -pb PL_to_PS_bus_skew_routed.pb -rpx PL_to_PS_bus_skew_routed.rpx"
  close_msg_db -file route_design.pb
} RESULT]
if {$rc} {
  write_checkpoint -force PL_to_PS_routed_error.dcp
  step_failed route_design
  return -code error $RESULT
} else {
  end_step route_design
  unset ACTIVE_STEP 
}

start_step write_bitstream
set ACTIVE_STEP write_bitstream
set rc [catch {
  create_msg_db write_bitstream.pb
  catch { write_mem_info -force PL_to_PS.mmi }
  write_bitstream -force PL_to_PS.bit 
  catch {write_debug_probes -quiet -force PL_to_PS}
  catch {file copy -force PL_to_PS.ltx debug_nets.ltx}
  close_msg_db -file write_bitstream.pb
} RESULT]
if {$rc} {
  step_failed write_bitstream
  return -code error $RESULT
} else {
  end_step write_bitstream
  unset ACTIVE_STEP 
}

