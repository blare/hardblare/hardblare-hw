
#include "types.h"
#include "no_os.h"
#include "decode.h"
#include "memory_tag_trf.h"
#include "address.h"
#include "debug.h"


// Search annotations offset for a given trace address (from PTM) and returning it
static inline uint32_t search_annotations_offset_in_bbt(const uint32_t wanted_address){

  uint32_t basic_block_table_size, *i_entry_address, i_entry_value, i;

  basic_block_table_size = MemoryRead(LOADED_BBT_ANNOTATION_ADDRESS);

  // DEBUG
  // IF_SIMULATION( debug_print_basic_block_table_size(basic_block_table_size); );
  // IF_HARDWARE_DEBUG(MemoryWrite(getDebugAddress(), basic_block_table_size););

  i_entry_address = LOADED_BBT_ANNOTATION_ADDRESS;
  i_entry_address++;

  for (i = 0; i < basic_block_table_size ; i++)
    {
      i_entry_value = MemoryRead(i_entry_address);

      // DEBUG
      //IF_SIMULATION(debug_print_address_of_basic_block(i_entry_value););
      //IF_HARDWARE_DEBUG(MemoryWrite(getDebugAddress(),i_entry_value););

      // If the address match with the entry i
      if (wanted_address == i_entry_value)
        {
          i_entry_address++;
          uint32_t value = MemoryRead(i_entry_address);

          // DEBUG
          // IF_SIMULATION(debug_print_address_of_basic_block(value););
          // IF_HARDWARE_DEBUG(MemoryWrite(getDebugAddress(), value););

          return value;
        }
      i_entry_address++;
      i_entry_address++;
    }
  // If we didn't find the wanted address, we return 0 as an error.
  return 0x0;
}


// Decoding annotations to the second stage
static inline uint32_t decoding_annotations(const uint32_t trace_value, const uint32_t* _addr_read, const uint32_t _size){

  uint32_t *addr_to_read, annotation_value, ret, i;
  addr_to_read = (uint32_t*)_addr_read;
  ret = 0;
  i = 0;

  for (i = 0; i < _size; i++)
    {
      annotation_value = MemoryRead(addr_to_read);

      // DEBUG
      IF_HARDWARE_DEBUG(MemoryWrite(getDebugAddress(),annotation_value););
      IF_SIMULATION(debug_print_annotation_value(annotation_value););
      // Decoding annotation
      decode_annotation(trace_value, annotation_value);
      // Incr
      addr_to_read++;
      ret++;
    }

  return ret;
}


void config_security_policy() {

  uint32_t *config_address = (uint32_t *)SECURITY_POLICY_CONFIG;
  uint32_t config_value = 0;

  uint32_t i = 0;

  for(i = 0; i < SECURITY_POLICY_ARM_OPCODE_FUNC_NUMBER; i++) {
    config_value = MemoryRead(config_address);
    ARM_OPCODE_FUNC[i] = ARM_OPCODE_FUNC_OPERATOR[config_value];

    // DEBUG
    IF_HARDWARE_DEBUG( MemoryWrite(getDebugAddress(), config_value); );
    IF_SIMULATION(debug_print_annotation_value(config_value););

    config_address++;
  }

  for(i = 0; i < SECURITY_POLICY_DEFAULT_TAG_NUMBER; i++) {
    config_value = MemoryRead(config_address);
    SECURITY_POLICY_DEFAULT_TAG[i] = config_value;

    // DEBUG
    IF_HARDWARE_DEBUG( MemoryWrite(getDebugAddress(),config_value););
    IF_SIMULATION(debug_print_annotation_value(config_value););

    config_address++;
  }

  return;
}


void retrieve_registers_values() {

  TRF[SP_REGISTER] = wait_read_instrumentation_value();

  TRF[LR_REGISTER] = wait_read_instrumentation_value();

}


void wait_ptm_trace(void) {

  uint32_t fifo_ptm_is_empty;

  do {
    fifo_ptm_is_empty = MemoryRead(FIFO_PTM_EMPTY);

    IF_HARDWARE_DEBUG( MemoryWrite(getDebugAddress(), 0xa9a9a9a9 ); );

  } while(fifo_ptm_is_empty != 0);

  return;

}


// Entry point for the MIPS
int main(){

  uint32_t *annotations_address,
    *number_of_annotations_address,
    trace_value, annotations_offset,
    number_of_annotations, i;
  i = 0;

  // Init offset mem_tag
  offset_mem_tag = 0;

  // begin
  trace_value = 0;

  // Debug
  IF_SIMULATION( init_simulation_memories(); );

  // Loop for ignoring first traces equals to 0
  // Loop for ignoring first traces until we receiving
  // the first syscall trace (0xffff 0008)
  do{

    // Loop for waiting that FIFO PTM is not empty, active wait for trace (fifo_ptm_is_empty != 0)
    wait_ptm_trace();
    // Reading the trace
    trace_value = MemoryRead(nextPTMtraceAddress());

    // DEBUG
    IF_SIMULATION( debug_print_trace_value(trace_value); );
    IF_HARDWARE_DEBUG( MemoryWrite(getDebugAddress(), trace_value););

  } while( (trace_value != TRACE_VALUE_SYSCALL ) );

  // We configure the security policy and retrieving values of registers (SP / LR)
  retrieve_registers_values();
  config_security_policy();

  // DEBUG
  IF_HARDWARE_DEBUG( MemoryWrite(getDebugAddress(), 0xf8f8f8f8); );
  IF_SIMULATION( puts("\n FIRST SYSCALL : STARTING POINT : \n"); );


  // Main loop (infinite loop) for reading traces and loading annotatione_coprocessor/main_firmware.c
  while(1) {

    // Loop for waiting that FIFO PTM is not empty, active wait for trace (fifo_ptm_is_empty != 0)
    wait_ptm_trace();
    // Reading the next trace
    trace_value = MemoryRead(nextPTMtraceAddress());

    // DEBUG
    IF_HARDWARE_DEBUG( MemoryWrite(getDebugAddress(), trace_value); );
    IF_SIMULATION( debug_print_trace_value( trace_value ); );

    // We are looking for the annotations offset for this trace value
    annotations_offset = search_annotations_offset_in_bbt(trace_value);

    // DEBUG
    IF_HARDWARE_DEBUG( MemoryWrite(getDebugAddress(), annotations_offset ); );
    IF_SIMULATION( debug_print_address_of_basic_block( annotations_offset ); );

    // There is no annotations for this trace
    if (annotations_offset == 0x0) {
    // TODO: [ERROR] We didn't find the address in the Basic Block Table !
    // Raising an exception ???
    }
    else {
    // Compute the address of the annotations
      number_of_annotations_address = (uint32_t *)((uint32_t)LOADED_BBT_ANNOTATION_ADDRESS + (uint32_t)annotations_offset);

      annotations_address = number_of_annotations_address;
      annotations_address++;

      // Read the number of annotations
      number_of_annotations = MemoryRead(number_of_annotations_address);

      // DEBUG
      IF_HARDWARE_DEBUG( MemoryWrite(getDebugAddress(), number_of_annotations); );
      IF_SIMULATION( debug_print_number_of_annotations(number_of_annotations); );

      if(number_of_annotations == 0) {
        // There is no annotations
      }
      else {
        // Loading annotations to the second stage
        decoding_annotations(trace_value , annotations_address, number_of_annotations);
      }

    }


  }


  return 0;
}

