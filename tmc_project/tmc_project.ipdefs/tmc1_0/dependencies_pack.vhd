---------------------------------------------------------------------
-- TITLE: dependencies package
-- AUTHOR: MAW
-- DATE CREATED: 07/11/2017
-- FILENAME: dependencies_pack.vhd
-- PROJECT: DIFT coprocessor
-- DESCRIPTION:
-- All constants required for dependencies (TMC) core unit
---------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package dependencies_pack is

	constant ZERO          : std_logic_vector(31 downto 0) :=
	      "00000000000000000000000000000000";
	constant ONES          : std_logic_vector(31 downto 0) :=
	      "11111111111111111111111111111111";
	
	constant instrumentation_address : std_logic_vector(31 downto 0) := X"40000000";
	constant kblare_address 		 : std_logic_vector(31 downto 0) := X"44000000";

	constant data_mem_address_width : integer := 10;
    constant inst_mem_address_width : integer := 16;
    constant data_mem_data_width    : integer := 32;      

  	constant nb_sec_policy : integer range 1 to 4 := 4;
  	constant nb_regs : integer range 1 to 5 := 4;
  	-- configuration registers type 
  	type sec_policy_registers_array is array(0 to (2*nb_sec_policy)-1) of std_logic_vector(31 downto 0);
  	type configuration_array is array(0 to 2**(nb_regs)-1) of std_logic_vector(31 downto 0);
  	--alias sec_policy_registers_array : configuration_array(0 to 2*(nb_sec_policy)-1);

  	-- virtual mem source type
  	subtype virtual_mem_source_type is std_logic_vector(1 downto 0);
  	constant virtual_mem_none			 : virtual_mem_source_type := "00";
  	constant virtual_mem_instrumentation : virtual_mem_source_type := "01";
  	constant virtual_mem_blare			 : virtual_mem_source_type := "10";
  	constant virtual_mem_adder			 : virtual_mem_source_type := "11";

  	-- Tag size type 
  	subtype tag_size_type is std_logic_vector(2 downto 0);
  	constant tag_size_none		: tag_size_type := "000";
  	constant tag_size_one		: tag_size_type := "001";
  	constant tag_size_two		: tag_size_type := "010";
  	constant tag_size_four		: tag_size_type := "010";
  	constant tag_size_eight		: tag_size_type := "011";
  	constant tag_size_sixteen	: tag_size_type := "100";
  	constant tag_size_word		: tag_size_type := "101";

  	type tag_size_policies is array(0 to nb_sec_policy-1) of tag_size_type; 
   	
   	-- Informations recovered from security policy
   	-- operations supported by tag ALU (This can also be encoded in instruction set for programmability ...)
	subtype tag_alu_function_type is std_logic_vector(3 downto 0);
	constant tag_alu_nothing   : tag_alu_function_type := "0000";
	constant tag_alu_add       : tag_alu_function_type := "0010";
	constant tag_alu_subtract  : tag_alu_function_type := "0011";
	constant tag_alu_or        : tag_alu_function_type := "0100";
	constant tag_alu_and       : tag_alu_function_type := "0101";
	constant tag_alu_xor       : tag_alu_function_type := "0110";
	constant tag_alu_nor       : tag_alu_function_type := "0111";
	constant tag_alu_copy_src1 : tag_alu_function_type := "1000";
	constant tag_alu_copy_src2 : tag_alu_function_type := "1001";
   	   	
   	-- tag propagation enables (which tags to propagate)
   	subtype tag_prop_type is std_logic_vector(2 downto 0);
   	constant tag_source_prop_none		: tag_prop_type := "111";
   	constant tag_source_prop_off		: tag_prop_type := "000";
   	constant tag_source_prop_en			: tag_prop_type := "001"; -- propagation only from source
   	constant tag_source_address_prop_en	: tag_prop_type := "010"; -- propagation only from source_address 
   	constant tag_source_and_address		: tag_prop_type := "011"; -- propagation from both source and source address
	constant tag_dest_address_prop_en	: tag_prop_type := "100";

	-- tag check enables (which tags to propagate)
   	subtype tag_check_type is std_logic_vector(2 downto 0);	
   	constant tag_source_check_none		: tag_check_type := "000";
   	constant tag_source_check_en		: tag_check_type := "001";
   	constant tag_source_2_check_en		: tag_check_type := "111";
   	constant tag_source_address_check_en: tag_check_type := "010";
	constant tag_dest_check_en			: tag_check_type := "011";
	constant tag_dest_address_check_en	: tag_check_type := "100";
	constant tag_pc_check				: tag_check_type := "101"; -- it happens when a jump instruction propagates an input tag to the PC
	constant tag_instr_check			: tag_check_type := "110"; -- it happens when executing tainted code	

   	-- tag Check operations 
	subtype tag_alu_check_function_type is std_logic_vector(1 downto 0);
	constant tag_alu_check_none     	: tag_alu_check_function_type := "00";
	constant tag_alu_check_eq     		: tag_alu_check_function_type := "01";
	
	-- end informations recovered from security policy
	
	-- Information recovered from dependencies 
	-- DO NOT CHANGE. IT IS USED AS INDEX IN REGISTERS_SEC_POLICY.VHD
	-- IF YOU CHANGE HERE, YOU NEED TO CHANGE THE INDEXING OF LOOPS IN 
	-- REGISTERS_SEC_POLICY.VHD ACCORDINGLY ! 
	subtype arm_opcode_type is std_logic_vector(3 downto 0); 
--	constant opcode_mov		   		: arm_opcode_type := "0000";
--	constant opcode_arith_log 		: arm_opcode_type := "0001"; -- register to register moves, loads, stores and jumps (Move to PC)
--	constant opcode_comp 			: arm_opcode_type := "0010"; -- ADD, SUB, AND, OR, ..
--	constant opcode_fp_mov		 	: arm_opcode_type := "0011"; -- 
--	constant opcode_fp_arith_log	: arm_opcode_type := "0100";
--	constant opcode_fp_comp			: arm_opcode_type := "0101"; 
--	constant opcode_custom1			: arm_opcode_type := "0110";
--	constant opcode_custom2			: arm_opcode_type := "0111";
--	constant opcode_custom3			: arm_opcode_type := "1000";
--	constant opcode_custom4			: arm_opcode_type := "1001";
--	constant opcode_arith_log_comp	: arm_opcode_type := "1010";
--	constant opcode_none 			: arm_opcode_type := "1011";
----	constant opcode_custom1			: arm_opcode_type := "1100";
----	constant opcode_custom1			: arm_opcode_type := "1101";
----	constant opcode_custom1			: arm_opcode_type := "1110";
----	constant opcode_custom1			: arm_opcode_type := "1111";
	constant opcode_none 			: arm_opcode_type := "0000";
	constant opcode_mov		   		: arm_opcode_type := "0001";
	constant opcode_arith_log 		: arm_opcode_type := "0010"; -- register to register moves, loads, stores and jumps (Move to PC)
	constant opcode_comp 			: arm_opcode_type := "0011"; -- ADD, SUB, AND, OR, ..
	constant opcode_arith_log_comp	: arm_opcode_type := "0100"; -- 
	constant opcode_fp_mov		 	: arm_opcode_type := "0101";
	constant opcode_fp_arith_log	: arm_opcode_type := "0110"; 
	constant opcode_fp_comp			: arm_opcode_type := "0111";
	constant opcode_custom1			: arm_opcode_type := "1000";
	constant opcode_custom2			: arm_opcode_type := "1001";
	constant opcode_custom3			: arm_opcode_type := "1010";
	constant opcode_custom4			: arm_opcode_type := "1011";
--	constant opcode_custom1			: arm_opcode_type := "1100";
--	constant opcode_custom1			: arm_opcode_type := "1101";
--	constant opcode_custom1			: arm_opcode_type := "1110";
--	constant opcode_custom1			: arm_opcode_type := "1111";
	
	-- 
	subtype reg_src1_source_type is std_logic_vector(1 downto 0); 
	constant reg_src1_from_reg		: reg_src1_source_type := "00";
	constant reg_src1_from_mem		: reg_src1_source_type := "01";
	constant reg_src1_from_instr	: reg_src1_source_type := "10";
	--constant reg_src1_from_kblare	: reg_src1_source_type := "11";


	subtype reg_src2_source_type is std_logic_vector(2 downto 0); 
	constant reg_src2_not_used		: reg_src2_source_type := "000";
	constant reg_src2_from_reg		: reg_src2_source_type := "001";
	constant reg_src2_from_imm		: reg_src2_source_type := "010";
	constant reg_src2_from_upper_imm: reg_src2_source_type := "011";
	constant reg_src2_from_unsigned_imm: reg_src2_source_type := "111";

	subtype reg_dst_source_type is std_logic_vector(2 downto 0); 
	constant reg_dst_not_used		: reg_dst_source_type := "000";
	constant reg_dst_from_alu		: reg_dst_source_type := "001";
	constant reg_dst_from_mem		: reg_dst_source_type := "010";
	constant reg_dst_from_data_mem	: reg_dst_source_type := "110";
	--constant reg_dst_from_instr		: reg_dst_source_type := "011";
	--constant reg_dst_from_kblare	: reg_dst_source_type := "100";

	subtype tag_alu_function_source_type is std_logic_vector(1 downto 0); 
	constant tag_alu_function_none			: tag_alu_function_source_type := "00";
	constant tag_alu_function_from_sec_pol	: tag_alu_function_source_type := "01";
	constant tag_alu_function_from_opcode	: tag_alu_function_source_type := "10";
	

	subtype tag_mem_source_type is std_logic_vector(1 downto 0);
	constant tag_mem_none    : tag_mem_source_type := "00";
	constant tag_mem_read32  : tag_mem_source_type := "01";
	constant tag_mem_write32 : tag_mem_source_type := "10";
	constant tag_mem_tlb_write : tag_mem_source_type := "11";
	
	 -- mode(32=1,16=2,8=3), signed, write
    subtype mem_source_type is std_logic_vector(3 downto 0);
    constant mem_none    : mem_source_type := "0000";
    constant mem_read32  : mem_source_type := "0100";
    constant mem_write32 : mem_source_type := "0101";
    constant mem_read16  : mem_source_type := "1000";
    constant mem_read16s : mem_source_type := "1010";
    constant mem_write16 : mem_source_type := "1001";
    constant mem_read8   : mem_source_type := "1100";
    constant mem_read8s  : mem_source_type := "1110";
    constant mem_write8  : mem_source_type := "1101";

    -- records to ease interface connections
    type BRAM_out_interface is record 
    	--clk		  : std_logic;
	    --rst       : std_logic;
	  	en        : std_logic;
	  	w_en      : std_logic_vector(3 downto 0);
	  	address_o : std_logic_vector(31 downto 0);
	  	data_out  : std_logic_vector(31 downto 0);
  	end record BRAM_out_interface;

  	type BRAM_in_interface is record 
    	rd_data		: std_logic_vector(31 downto 0);
  	end record BRAM_in_interface;

  	type custom_axi_master_out_interface is record
  		go 					: std_logic;
  		RNW					: std_logic;
  		address 			: std_logic_vector(31 downto 0);
  		increment_burst		: std_logic;
  		clear_data_fifos 	: std_logic;
  		burst_length 		: std_logic_vector(7 downto 0);
  		burst_size 			: std_logic_vector(6 downto 0);
  		read_fifo_en 		: std_logic;
  		write_fifo_en 		: std_logic;
  		write_data 			: std_logic_vector(31 downto 0);
	end record custom_axi_master_out_interface;

	type custom_axi_master_in_interface is record
		error 				: std_logic;
		busy 				: std_logic;
		done 				: std_logic;
		read_data			: std_logic_vector(31 downto 0);
		write_fifo_empty 	: std_logic;
		write_fifo_full		: std_logic;
		read_fifo_empty 	: std_logic;
		read_fifo_full 		: std_logic;
	end record custom_axi_master_in_interface;

	-- functions
	function   adder(a     : in std_logic_vector(32 downto 0);
                     b     : in std_logic_vector(32 downto 0);
                     do_sub: in std_logic) return std_logic_vector;
    function bv_increment(a : in std_logic_vector(31 downto 2)
                                          ) return std_logic_vector; 


end; --package 

package body dependencies_pack is

function    adder(a     : in std_logic_vector(32 downto 0);
                  b     : in std_logic_vector(32 downto 0);
                  do_sub: in std_logic) return std_logic_vector is        
   variable result   : std_logic_vector(32 downto 0);
begin     
	if do_sub = '0' then
      result := std_logic_vector(signed(a) + signed(b));
   else
      result := std_logic_vector(signed(a) - signed(b));   
   end if;   
   return result;
end; --function adder

function bv_increment(a : in std_logic_vector(31 downto 2)
                     ) return std_logic_vector is
   variable carry_in : std_logic;
   variable result   : std_logic_vector(31 downto 2);
begin
   result := "000000000000000000000000000000";
   carry_in := '1';
   for index in 2 to 31 loop
      result(index) := a(index) xor carry_in;
      carry_in := a(index) and carry_in;
   end loop;
   return result;
end; --function

end; -- package body
