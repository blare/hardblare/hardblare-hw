---------------------------------------------------------------------
-- TITLE: Arithmetic Logic Unit
-- AUTHOR: MAW
-- DATE CREATED: 25/10/2017
-- FILENAME: tag_alu.vhd
-- PROJECT: DIFT Coprocessor
-- DESCRIPTION:
--    Implements the tag ALU.
---------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use work.dependencies_pack.all;

entity tag_alu is
   port(a_in         : in  std_logic_vector(31 downto 0);
        b_in         : in  std_logic_vector(31 downto 0);

        mem_access   : in std_logic;
        instrument   : in std_logic;
        instr_value  : in std_logic_vector(31 downto 0);
        tag_alu_p    : in  std_logic_vector(31 downto 0);
        tag_alu_p_p  : in  std_logic_vector(31 downto 0);
        forward_a    : in  std_logic_vector(1 downto 0);
        forward_b    : in  std_logic_vector(1 downto 0);

        tag_alu_func : in  tag_alu_function_type;
        c_alu        : out std_logic_vector(31 downto 0) );
end; --tag_alu

architecture logic of tag_alu is
 
begin

alu_proc: process(a_in, b_in, tag_alu_func, forward_a, forward_b, 
                  tag_alu_p, tag_alu_p_p, instrument, instr_value) 
   variable c           : std_logic_vector(31 downto 0);
   variable aa, bb, sum : std_logic_vector(32 downto 0);
   variable do_sub      : std_logic;
   variable a_eq_b      : std_logic;
   variable a_zero      : std_logic;
   variable sign_ext    : std_logic;
   variable a_input     : std_logic_vector(31 downto 0);
   variable b_input     : std_logic_vector(31 downto 0);
begin
   c := ZERO;

   if forward_a = "10" and mem_access = '0' then 
      a_input := tag_alu_p;
   elsif forward_a = "01"  and mem_access = '0' then
      a_input := tag_alu_p_p;
   else 
      a_input := a_in;
   end if;
   
   
   if instrument = '1' then 
      a_input := instr_value;
   else
      a_input := a_in;
   end if;


   if forward_b = "10" and mem_access = '0' then 
      b_input := tag_alu_p;
   elsif forward_b = "01" and mem_access = '0' then 
      b_input := tag_alu_p_p;
   else
      b_input := b_in;
   end if;
   --b_input := b_in;

   if tag_alu_func = tag_alu_add then
      do_sub := '0';
   else
      do_sub := '1';
   end if;

   aa := (a_input(31)) & a_input;
   bb := (b_input(31)) & b_input;
   sum := adder(aa, bb, do_sub);

   if a_input = b_input then
      a_eq_b := '1';
   else
      a_eq_b := '0';
   end if;

   if a_input = ZERO then
      a_zero := '1';
   else
      a_zero := '0';
   end if;

   case tag_alu_func is
   when tag_alu_add | tag_alu_subtract => --c=a+b
      c := sum(31 downto 0);   
   when tag_alu_or =>             --c=a|b
      c := a_input or b_input;
   when tag_alu_and =>                --c=a&b
      c := a_input and b_input;
   when tag_alu_xor =>                --c=a^b
      c := a_input xor b_input;
   when tag_alu_nor =>                --c=~(a|b)
      c := a_input nor b_input;
   when tag_alu_copy_src1 =>          -- c=a
      c := a_input;
   when tag_alu_copy_src2 =>          -- c=b
      c := b_input;
   when others =>                 --tag_alu_func = alu_nothing
      c := ZERO;
   end case;

   c_alu <= c;
end process;

end; --architecture logic

