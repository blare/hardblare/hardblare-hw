---------------------------------------------------------------------
-- TITLE: Arithmetic Logic Unit
-- AUTHOR: MAW
-- DATE CREATED: 25/10/2017
-- FILENAME: tag_check.vhd
-- PROJECT: DIFT Coprocessor
-- DESCRIPTION:
--    Implements the tag check ALU.
---------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.dependencies_pack.all;

entity tag_check is
   port(reg_source_1      : in  std_logic_vector(31 downto 0);
        reg_source_2      : in  std_logic_vector(31 downto 0);
        reg_dest          : in  std_logic_vector(31 downto 0);
        source_addr       : in  std_logic_vector(31 downto 0);
        dest_addr         : in  std_logic_vector(31 downto 0);
        tag_pc            : in  std_logic_vector(31 downto 0);
        tag_instruction   : in  std_logic_vector(31 downto 0);
        tag_check_src     : in  tag_check_type;        
        --tag_check_func    : in  tag_alu_check_function_type;
        stop_propagation  : out std_logic );
end; --tag_check

architecture logic of tag_check is
constant tag_check_value : std_logic_vector(31 downto 0) := X"ffffffff"; -- X"-1-1-1-1"; X"-------1 "
constant tag_check_instruction : std_logic_vector(31 downto 0) := X"00000001";
constant tag_check_pc : std_logic_vector(31 downto 0) := X"00000001";
signal tag_check_s : std_logic;
begin

-- @TODO: need to specify tag check value that should raise an exception. 
-- The tag values must be compared with some value/threshold after which the security violation should be raised. 
-- This value is the one we must specify through an IP or existing interface.
-- Custom IP registers could be used ...
-- For simulation purposes, need to manually fix before simulation of different cases
alu_proc: process(reg_source_1, reg_source_2, reg_dest, tag_pc, 
                  source_addr, dest_addr, tag_check_src, tag_instruction) 
   variable tag_check  : std_logic;
begin
   
    case tag_check_src is 
      when tag_source_check_en =>
        if std_match(reg_source_1,tag_check_value) then 
            tag_check := '1'; 
            report "Tag SOURCE 1 Check failed " severity error;
        elsif std_match(reg_source_2,tag_check_value) then 
            tag_check := '1'; 
            report "Tag SOURCE 2 Check failed " severity error;
        else 
            tag_check := '0'; 
        end if;

      when tag_source_2_check_en =>
        if std_match(reg_source_2,tag_check_value) then 
            tag_check := '1';   
            report "Tag SOURCE 2 Check failed " severity error;
        else 
            tag_check := '0'; 
        end if;  

      when tag_source_address_check_en => 
        if std_match(source_addr,tag_check_value) then 
            tag_check := '1';
            report "Tag SOURCE ADDRESS Check failed " severity error;
        else 
            tag_check := '0'; 
        end if;
      when tag_dest_check_en => 
        if std_match(reg_dest,tag_check_value) then 
            tag_check := '1';
            report "Tag DESTINTATION Check failed " severity error;
        else 
            tag_check := '0'; 
        end if;

      when tag_dest_address_check_en => 
        if std_match(dest_addr,tag_check_value) then 
            tag_check := '1';       
            report "Tag DESTINTATION ADDRESS Check failed " severity error;  
        else 
            tag_check := '0'; 
        end if;  

      when tag_pc_check => 
        if std_match(tag_pc,tag_check_pc) then 
            tag_check := '1';
            report "Tag PC Check failed " severity error;           
        else 
            tag_check := '0'; 
        end if;  

      when tag_instr_check => 
        if std_match(tag_instruction,tag_check_instruction) then 
            tag_check := '1';
            report "Tag Instruction Check failed " severity error;  
        else 
            tag_check := '0'; 
        end if;  

      when others => 
        tag_check := '0';
        --report "Tag Check OK " severity note;  
    end case;

    if tag_check = '1' then 
        stop_propagation <= '1';
    else 
        stop_propagation <= '0';
    end if;
    tag_check_s <= tag_check;
end process;


end; --architecture logic

