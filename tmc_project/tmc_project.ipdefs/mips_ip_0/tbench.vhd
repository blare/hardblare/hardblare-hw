---------------------------------------------------------------------
-- TITLE: Test Bench
-- AUTHOR: Steve Rhoads (rhoadss@yahoo.com)
-- DATE CREATED: 4/21/01
-- FILENAME: tbench.vhd
-- PROJECT: MIPS CPU core
-- COPYRIGHT: Software placed into the public domain by the author.
--    Software 'as is' without warranty.  Author liable for nothing.
-- DESCRIPTION:
--    This entity provides a test bench for testing the MIPS CPU core.
---------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use work.mips_pack.all;

entity tbench is
   port(clk_out : out std_logic;
        pc      : out std_logic_vector(31 downto 0)
       );
end; --entity tbench

architecture logic of tbench is

component mips_cpu
   port(clk         : in std_logic;
        reset_in    : in std_logic;
        --stall       : in std_logic;
        intr_in     : in std_logic;

        mem_address : out std_logic_vector(31 downto 0);
        mem_data_w  : out std_logic_vector(31 downto 0);
        mem_data_r  : in std_logic_vector(31 downto 0);
        mem_sel     : out std_logic_vector(3 downto 0);
        mem_write   : out std_logic;d
        mem_read     : out std_logic;
        mem_pause   : in std_logic;

        i_mem_enable  : out std_logic;
        i_mem_address : out std_logic_vector(31 downto 0);        
        i_mem_data_r  : in std_logic_vector(31 downto 0);
        i_mem_sel     : out std_logic_vector(3 downto 0);        
        --i_mem_pause   : in std_logic;

        t_pc        : out std_logic_vector(31 downto 0);
        t_opcode    : out std_logic_vector(31 downto 0);
        t_r_dest    : out std_logic_vector(31 downto 0)
        );
end component;

component ram
   generic(load_file_name : string);     
   port(clk, reset        : in std_logic;
        mem_byte_sel      : in std_logic_vector(3 downto 0);
        mem_write         : in std_logic;
        mem_address       : in std_logic_vector;
        mem_data_w        : in std_logic_vector(31 downto 0);
        mem_data_r        : out std_logic_vector(31 downto 0));
end component;

component datamemory is  
  port (
    clk,reset    : in  std_logic;
    mem_byte_sel : in std_logic_vector(3 downto 0);
    en     : in  std_logic;
    we_i   : in  std_logic;    
    addr_i : in  std_logic_vector(data_mem_address_width-1 downto 0);
    din_i  : in  std_logic_vector(data_width-1 downto 0);
    dout_o : out std_logic_vector(data_width-1 downto 0)
    );
end component;

component write_ram   
   port(clk, reset        : in std_logic;
        mem_byte_sel      : in std_logic_vector(3 downto 0);
        mem_write         : in std_logic;
        mem_address       : in std_logic_vector;
        mem_data_w        : in std_logic_vector(31 downto 0);
        mem_data_r        : out std_logic_vector(31 downto 0));
end component;


   signal clk           : std_logic := '0';
   signal reset         : std_logic := '1'; --, '0' after 100 ns;
   signal stall         : std_logic := '1'; 
   signal interrupt     : std_logic := '0';
   signal mem_sel       : std_logic_vector(3 downto 0);
   signal mem_write     : std_logic;
   signal mem_read      : std_logic;
   signal mem_address   : std_logic_vector(31 downto 0);
   signal mem_data_w    : std_logic_vector(31 downto 0);
   signal mem_data_r    : std_logic_vector(31 downto 0);
   signal mem_pause     : std_logic;
   signal i_mem_byte_sel: std_logic_vector(3 downto 0);
   signal i_mem_write   : std_logic;
   signal i_mem_address : std_logic_vector(31 downto 0);
   signal i_mem_data_w  : std_logic_vector(31 downto 0);
   signal i_mem_data_r  : std_logic_vector(31 downto 0);
   signal i_mem_pause   : std_logic;
   signal t_pc          : std_logic_vector(31 downto 0);
   signal t_opcode      : std_logic_vector(31 downto 0);
   signal t_r_dest      : std_logic_vector(31 downto 0);
   signal mem_byte_sel  : std_logic_vector(3 downto 0);
   signal i_mem_enable  : std_logic;

   signal mem_cs         : std_logic;
   signal annotations_cs : std_logic;
   signal annot_bb_cs    : std_logic;
   signal mem_data_out   : std_logic_vector(data_width-1 downto 0);
   signal annot_data_out      : std_logic_vector(data_width-1 downto 0);
   signal annot_bb_data_out   : std_logic_vector(data_width-1 downto 0);
   signal cpu_data_out     : std_logic_vector(31 downto 0);  
   signal data_in          : std_logic_vector(31 downto 0);

begin  --architecture
   clk <= not clk after 10 ns;
   reset <= '0' after 100 ns;
   stall <= '0';
   mem_pause <= '0';
   i_mem_pause <= '0';

   u1: mips_cpu PORT MAP (
        clk          => clk,
        reset_in     => reset,
        --stall        => stall, 
        intr_in      => interrupt,

        mem_address  => mem_address,
        mem_data_w   => cpu_data_out,
        mem_data_r   => mem_data_out,
        mem_sel      => mem_byte_sel,
        mem_write    => mem_write,
        mem_read     => mem_read,
        mem_pause    => mem_pause,
        
        i_mem_enable   => i_mem_enable,
        i_mem_address  => i_mem_address,        
        i_mem_data_r   => i_mem_data_r,
        i_mem_sel      => i_mem_byte_sel,        
        --i_mem_pause    => i_mem_pause,

        t_pc         => t_pc,
        t_opcode     => t_opcode,
        t_r_dest     => t_r_dest);

   u2: ram generic map ("E:\Vivado\2017_1\mips_new\code.txt")
        PORT MAP (
        clk          => clk,
        reset        => reset,
        mem_byte_sel => i_mem_byte_sel,
        mem_write    => '0',
        mem_address  => i_mem_address(inst_mem_address_width-1 downto 0),
        mem_data_w   => i_mem_data_w,
        mem_data_r   => i_mem_data_r);

   u3: datamemory port map (
        clk           => clk,
        reset         => reset,
        en            => mem_cs,
        mem_byte_sel  => mem_byte_sel,
        we_i          => mem_write,
        addr_i        => mem_address(data_mem_address_width-1 downto 0),
        din_i         => cpu_data_out,
        dout_o        => mem_data_out);
     --u3: write_ram
     --   PORT MAP (
     --   clk          => clk,
     --   reset        => reset,
     --   mem_byte_sel => mem_byte_sel,
     --   mem_write    => mem_write,
     --   mem_address  => mem_address(data_mem_address_width-1 downto 0),
     --   mem_data_w   => mem_data_w,
     --   mem_data_r   => mem_data_r);

  process(mem_write, mem_address)
  begin
    if (mem_write = '1') then
      case mem_address(data_mem_address_width-3 downto data_mem_address_width-4) is 
      when "00" => -- addresses from 0x0 0000 to 0x0 FFFF 
        mem_cs <= '1';
        annotations_cs <= '0'; 
        annot_bb_cs <= '0';
      when "01" => -- addresses from 0x1 0000 to 0x1 FFFF 
        mem_cs <= '0';
        annotations_cs <= '1'; 
        annot_bb_cs <= '0';
      when "10" => -- addresses from 0x2 0000 to 0x2 FFFF 
        mem_cs <= '0';
        annotations_cs <= '0'; 
        annot_bb_cs <= '1';
      when others => 
        mem_cs <= '1'; -- default data memory is on 
        annotations_cs <= '0'; 
        annot_bb_cs <= '0';
      end case;
    else 
      mem_cs <= '1'; -- default data memory is on 
      annotations_cs <= '0';
      annot_bb_cs <= '0';
    end if;
  end process;   



      -- input mux for CPU data bus 
  data_in <= 
    mem_data_out when mem_cs = '1' else 
    annot_data_out when annotations_cs = '1' else
    annot_bb_data_out when annot_bb_cs = '1' else
    mem_data_out; -- default

   clk_out <= clk;
   pc <= t_pc;



end; --architecture logic

