library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.dependencies_pack.all;

entity control_dependencies is
   port(clk, reset              : in std_logic;
        opcode                  : in  std_logic_vector(31 downto 0);
        -- required to specify correct operation for security policies
        tag_prop_sources        : in tag_prop_type;
        en_read_instrumentation : out std_logic;
        en_read_kblare_ps2pl    : out std_logic;
        reg_dst                 : out std_logic_vector(4 downto 0);
        reg_src1                : out std_logic_vector(4 downto 0);
        reg_src2                : out std_logic_vector(4 downto 0);
        reg_dst_fp              : out std_logic_vector(4 downto 0);
        reg_src1_fp             : out std_logic_vector(4 downto 0);
        reg_src2_fp             : out std_logic_vector(4 downto 0);
        config_reg_index        : out std_logic_vector(4 downto 0);
        tag_alu_func            : out tag_alu_function_type; 
        arm_opcode_class        : out arm_opcode_type;
        immediate               : out std_logic_vector(15 downto 0);
        --instrumentation_data    : out std_logic_vector(31 downto 0);
        kblare_data_o           : out std_logic_vector(31 downto 0);
        en_write_kblare_o       : out std_logic;
        kblare_pl2ps_write_address_r : out std_logic_vector(31 downto 0);
        virtual_mem_source      : out virtual_mem_source_type;
        --en_write_kblare_i       : in  std_logic; -- not used.
        --kblare_data_i           : in  std_logic_vector(31 downto 0);
        reg_dst_source          : out reg_dst_source_type;
        reg_src1_source         : out reg_src1_source_type;
        reg_src2_source         : out reg_src2_source_type;
        --instrument              : out std_logic;
        rf_en                   : out std_logic;
        rf_2_en                 : out std_logic;
        rf_w_en                 : out std_logic;
        fp_rf_en                : out std_logic;
        fp_rf_2_en              : out std_logic;
        fp_rf_w_en              : out std_logic;
        config_reg_file_w_en    : out std_logic;
        stall                   : out std_logic;
        mem_source_o            : out mem_source_type;
        mmu_write_en            : out std_logic;
        data_mem_address        : out std_logic; -- If data_mem_address = '1' it means that we need to enable local data mem ctl 
        tag_mem_source_o        : out tag_mem_source_type;
        null_instruction        : out std_logic;
        tag_alu_function_source : out tag_alu_function_source_type );
end; --entity control_dependencies


architecture logic of control_dependencies is 

signal en_read_instrumentation_s    : std_logic;
signal en_read_kblare_s             : std_logic;
signal en_write_kblare_s            : std_logic;
signal instrumentation_address      : std_logic_vector(31 downto 0) := (others => '0');
signal kblare_pl2ps_write_address   : std_logic_vector(31 downto 0) := (others => '0');
signal kblare_ps2pl_read_address    : std_logic_vector(31 downto 0) := (others => '0');
--signal instrumentation_address_r    : std_logic_vector(31 downto 0);
--signal kblare_pl2ps_write_address_r : std_logic_vector(31 downto 0);
--signal kblare_ps2pl_read_address_r  : std_logic_vector(31 downto 0);
signal instrumentation_bram_o       : BRAM_out_interface;
signal instrumentation_bram_i       : BRAM_in_interface;
signal kblare_read_bram_o           : BRAM_out_interface;
signal kblare_read_bram_i           : BRAM_in_interface;
signal kblare_write_bram_o          : BRAM_out_interface;
--signal kblare_data_i                : std_logic_vector(31 downto 0);
--signal kblare_write_bram_i          : BRAM_in_interface;

--component bram_reader is
--port(
--  clk       : in  std_logic;
--  reset     : in  std_logic;
--  rd_en     : in  std_logic; -- read enable
--  address   : in  std_logic_vector(31 downto 0);
--  rd_data   : out std_logic_vector(31 downto 0);
--  -- BRAM interface
--  bram_o    : out BRAM_out_interface;
--  bram_i    : in  BRAM_in_interface
--);
--end component;

--component bram_writer is
--port(
--  clk       : in  std_logic;
--  reset     : in  std_logic;
--  enable    : in  std_logic;
--  address   : in  std_logic_vector(31 downto 0);
--  data_in   : in  std_logic_vector(31 downto 0);
  
--  -- BRAM interface
--  bram_o    : out BRAM_out_interface
--  --bram_i    : in  BRAM_in_interface;
--);
--end component;

begin

--process(clk)
--begin
--    if rising_edge(clk) then
--        if reset = '1' then 
--            instrumentation_address_r <= (others => '0');
--            kblare_pl2ps_write_address_r <= (others => '0'); 
--            kblare_ps2pl_read_address_r <= (others => '0');
--        else
--            instrumentation_address_r <= instrumentation_address;
--            kblare_pl2ps_write_address_r <= kblare_pl2ps_write_address;
--            kblare_ps2pl_read_address_r <= kblare_ps2pl_read_address;            
--        end if;
--    end if;
--end process;

control_proc: process(opcode, instrumentation_address, kblare_pl2ps_write_address, kblare_ps2pl_read_address,tag_prop_sources)
variable op_dep, tag_func                       : std_logic_vector(5 downto 0);
variable reg_dst_v, reg_src1_v, reg_src2_v      : std_logic_vector(4 downto 0);
variable reg_dst_fp_v, reg_src1_fp_v            : std_logic_vector(4 downto 0);
variable reg_src2_fp_v                          : std_logic_vector(4 downto 0);
variable arm_opcode_t                           : arm_opcode_type;
variable immediate_v                            : std_logic_vector(15 downto 0);
variable reg_dst_source_v                       : reg_dst_source_type;
variable reg_src1_source_v                      : reg_src1_source_type;
variable reg_src2_source_v                      : reg_src2_source_type;
variable tag_alu_function_source_v              : tag_alu_function_source_type;
variable mem_source_v                           : mem_source_type;
variable tag_mem_source                         : tag_mem_source_type;
variable tag_function                           : tag_alu_function_type;
variable tag_function_op                        : std_logic_vector(5 downto 0);
variable fp_reg_file_en                         : std_logic;
variable fp_reg_file_2_en                       : std_logic;
variable fp_reg_file_w_en                       : std_logic;
variable reg_file_en                            : std_logic;
variable reg_file_2_en                          : std_logic;
variable reg_file_w_en                          : std_logic;
variable data_mem_address_v                     : std_logic;
variable virtual_mem_source_v                   : virtual_mem_source_type;
variable config_reg_index_v                     : std_logic_vector(4 downto 0);
variable config_reg_file_w_en_v                 : std_logic;
variable mmu_write_en_v                         : std_logic;
begin
    reg_file_en := '0';
    reg_file_2_en := '0';
    config_reg_file_w_en_v := '0';
    reg_file_w_en := '0';
    fp_reg_file_en := '0';
    fp_reg_file_2_en := '0';
    fp_reg_file_w_en := '0';
    op_dep := opcode(31 downto 26);
--    if (opcode(31) = '1') then 
--        op_dep := opcode(31 downto 26);
--    else 
--        op_dep := ZERO(5 downto 0);
--    end if;
    arm_opcode_t := ZERO(3 downto 0);
    reg_dst_v := opcode(21 downto 17);
    reg_src1_v := opcode(16 downto 12);
    reg_src2_v := opcode(11 downto 7);
    reg_dst_fp_v := opcode(21 downto 17);
    reg_src1_fp_v := opcode(16 downto 12);
    reg_src2_fp_v := opcode(11 downto 7);
    tag_function_op := opcode(5 downto 0);
    reg_src1_source_v := reg_src1_from_reg;
    reg_src2_source_v := reg_src2_not_used;
    immediate_v := "0000" & opcode(11 downto 0);
    tag_function := tag_alu_nothing;
    tag_alu_function_source_v := tag_alu_function_none;
    reg_dst_source_v := reg_dst_from_alu;
    tag_mem_source := tag_mem_none;
    mem_source_v := mem_none;
    en_read_instrumentation_s <= '0';
    en_read_kblare_s <= '0';
    en_write_kblare_s <= '0';
    data_mem_address_v := '0';
    virtual_mem_source_v := virtual_mem_none;
    config_reg_index_v := (others => '0');
    mmu_write_en_v := '0';
    null_instruction <= '0';
    stall <= '0';

    case op_dep is 
    when "000000" => 
        null_instruction <= '1';
    -- LW/SW instructions
    when "000001" => -- Load word (LW) 0x01
        reg_file_en := '1';
        reg_file_w_en := '1';
        reg_src1_v := opcode(20 downto 16);
        reg_src2_source_v := reg_src2_from_imm;
        immediate_v := opcode(15 downto 0);
        tag_alu_function_source_v := tag_alu_function_from_opcode;
        tag_function := tag_alu_add;
        reg_dst_v := opcode(25 downto 21);
        reg_dst_source_v := reg_dst_from_data_mem;
        mem_source_v := mem_read32;
        data_mem_address_v := '1';
    when "000010" => -- Store word (SW) 0x02
        reg_file_en := '1';
        reg_file_2_en := '1';
        reg_src1_v := opcode(20 downto 16);
        reg_src2_source_v := reg_src2_from_imm;
        immediate_v := opcode(15 downto 0);
        tag_alu_function_source_v := tag_alu_function_from_opcode;
        tag_function := tag_alu_add;
        reg_dst_v := opcode(25 downto 21);
        reg_dst_source_v := reg_dst_from_data_mem;
        mem_source_v := mem_write32;
        data_mem_address_v := '1';
    when "000011" =>  -- Load configuration register 0x03
        reg_file_en := '1';
        reg_src1_v := opcode(20 downto 16);
        reg_src2_source_v := reg_src2_from_imm;
        immediate_v := opcode(15 downto 0);
        tag_alu_function_source_v := tag_alu_function_from_opcode;
        tag_function := tag_alu_add;
        --reg_dst_v := reg_src1_v;
        config_reg_file_w_en_v := '1';
        config_reg_index_v := opcode(25 downto 21);
        reg_dst_source_v := reg_dst_from_data_mem;
        mem_source_v := mem_read32; -- load operation
        data_mem_address_v := '1';
    when "000100" =>  -- Write a TLB entry  0x04
        reg_file_en := '1';
        reg_file_w_en := '0';
        reg_src2_source_v := reg_src2_not_used;
        tag_function := tag_alu_copy_src1;
        mmu_write_en_v := '1';
        tag_alu_function_source_v := tag_alu_function_from_opcode;
        reg_src1_v := opcode(20 downto 16);
        tag_mem_source := tag_mem_tlb_write;
        --immediate_v := "0000" & opcode(11 downto 0);
        --arm_opcode_t := opcode(25 downto 22);
        virtual_mem_source_v := virtual_mem_adder;
    when "000101" => -- Load word FP (LW_FP) 0x05
        mmu_write_en_v := '0';
        virtual_mem_source_v := virtual_mem_none;
        reg_file_en := '1';
        fp_reg_file_w_en := '1';
        reg_src1_v := opcode(20 downto 16);
        reg_src2_source_v := reg_src2_from_imm;
        immediate_v := opcode(15 downto 0);
        tag_alu_function_source_v := tag_alu_function_from_opcode;
        tag_function := tag_alu_add;
        reg_dst_v := opcode(25 downto 21);
        reg_dst_source_v := reg_dst_from_data_mem;
        mem_source_v := mem_read32;
        data_mem_address_v := '1';
    when "000110" => -- Store word FP (SW_FP) 0x06
        mmu_write_en_v := '0';
        virtual_mem_source_v := virtual_mem_none;
        reg_file_en := '1';
        fp_reg_file_2_en := '1';
        reg_src1_v := opcode(25 downto 21);
        reg_src2_source_v := reg_src2_from_imm;
        immediate_v := opcode(15 downto 0);
        tag_alu_function_source_v := tag_alu_function_from_opcode;
        tag_function := tag_alu_add;
        --reg_dst_v := opcode(25 downto 21);
        reg_src2_fp_v := opcode(20 downto 16);
        reg_dst_source_v := reg_dst_from_data_mem;
        mem_source_v := mem_write32;
        data_mem_address_v := '1';
    when "000111" => -- tag_rri (rd = rs + imm_16) 0x07
        mmu_write_en_v := '0';
        virtual_mem_source_v := virtual_mem_none;
        reg_file_en := '1';
        reg_dst_v := opcode(25 downto 21);
        reg_src2_source_v := reg_src2_from_imm;
        immediate_v := opcode(15 downto 0);
        tag_alu_function_source_v := tag_alu_function_from_opcode;
        tag_function := tag_alu_add;
        --reg_dst_v := opcode(25 downto 21);
        reg_src1_v := opcode(20 downto 16);
        reg_dst_source_v := reg_dst_from_alu;
        reg_file_w_en := '1';
    -- format used in this instruction is Init format
    when "001000" => -- MOV_TRF_2_TRF-FP Source is TRF (+ GRF) => DESTINATION is TRF-FP
        mmu_write_en_v := '0';
        virtual_mem_source_v := virtual_mem_none;
        reg_file_en := '1'; -- source 
        reg_file_w_en := '0'; -- not required here
        fp_reg_file_en := '0'; -- not used here
        fp_reg_file_w_en := '1'; -- destination
        reg_dst_v := opcode(25 downto 21);
        reg_src2_source_v := reg_src2_from_reg;
        immediate_v := opcode(15 downto 0); -- not used 
        tag_alu_function_source_v := tag_alu_function_from_opcode;
        tag_function := tag_alu_copy_src1;
        --reg_dst_v := opcode(25 downto 21);
        reg_src1_v := opcode(20 downto 16);
        reg_dst_source_v := reg_dst_from_alu;
    when "001001" => -- MOV_TRF-FP_2_TRF Source is TRF-FP => DESTINATION is TRF
        mmu_write_en_v := '0';
        virtual_mem_source_v := virtual_mem_none;
        reg_file_en := '0'; -- not required here
        reg_file_w_en := '1'; -- destination
        fp_reg_file_en := '1'; -- source
        fp_reg_file_w_en := '0'; -- not required here
        reg_dst_v := opcode(25 downto 21);
        reg_src2_source_v := reg_src2_from_reg;
        immediate_v := opcode(15 downto 0); -- not used 
        tag_alu_function_source_v := tag_alu_function_from_opcode;
        tag_function := tag_alu_copy_src1;
        --reg_dst_v := opcode(25 downto 21);
        reg_src1_v := opcode(20 downto 16);
        reg_dst_source_v := reg_dst_from_alu;
    -- tag initialization opcodes
    when "100000" =>  -- trf(reg_dst_v) <= imm 0x20 (tag_reg_imm)
        reg_file_w_en := '1';
        reg_src2_source_v := reg_src2_from_unsigned_imm;
        tag_alu_function_source_v := tag_alu_function_from_opcode;
        tag_function := tag_alu_copy_src2;
        reg_dst_v := opcode(25 downto 21);
        reg_src1_v := opcode(20 downto 16);
        immediate_v := opcode(15 downto 0);
    when "100001" => -- trf(reg_dst_v) <= reg_src1_v 0x21
        reg_file_en := '1';
        reg_file_w_en := '1';
        reg_src2_source_v := reg_src2_not_used;
        tag_alu_function_source_v := tag_alu_function_from_opcode;
        tag_function := tag_alu_copy_src1;
        reg_dst_v := opcode(25 downto 21);
        reg_src1_v := opcode(20 downto 16);
        immediate_v := opcode(15 downto 0);
    -- @TODO: FIX this opcode ...
    when "100010" => -- mem(trf(reg_dst_v) + imm) <= reg_src1_v 0x22
        reg_file_en := '1';
        --reg_file_2_en := '1';
        reg_src2_v := opcode(20 downto 16);
        reg_file_2_en := '1';
        reg_file_w_en := '0';
        tag_function := tag_alu_add;
        reg_src2_source_v := reg_src2_from_imm;
        tag_alu_function_source_v := tag_alu_function_from_opcode;
        reg_dst_v := opcode(25 downto 21);
        reg_src1_v := reg_dst_v;
        --reg_src2_v := opcode(20 downto 16); -- not used by the instruction        
        --reg_src1_v := opcode(20 downto 16);
        immediate_v := opcode(15 downto 0);
        reg_dst_source_v := reg_dst_from_mem;
        tag_mem_source := tag_mem_write32;
        virtual_mem_source_v := virtual_mem_adder;
    when "101011" => -- reg_dst = reg_src |  <<16  (lui) 0x2b
        reg_file_en := '1';
        reg_file_w_en := '1';
        reg_src2_source_v := reg_src2_from_upper_imm;
        tag_alu_function_source_v := tag_alu_function_from_opcode;
        tag_function := tag_alu_or;
        reg_dst_v := opcode(25 downto 21);
        reg_src1_v := opcode(20 downto 16);
        immediate_v := opcode(15 downto 0);
    -- tag update opcodes    
    when "100011" => -- trf(reg_dst_v) <= trf(reg_src1_v) OPERATIOn trf(reg_src2_v) 0x23
        reg_file_en := '1';
        reg_file_2_en := '1';
        reg_file_w_en := '1';
        reg_src2_source_v := reg_src2_from_reg;
        tag_alu_function_source_v := tag_alu_function_from_sec_pol;
        arm_opcode_t := opcode(25 downto 22);
    -- @TODO: FIX this opcode ...
    when "100100" => -- tag(Mem(trf(reg_dst_v) + offset)) <= trf(reg_src) 0x24
        reg_file_en := '1';
        reg_src2_source_v := reg_src2_from_imm;
        arm_opcode_t := opcode(25 downto 22);
        -- it's security policy but it needs to be done through control signals so we consider it the same as an opcode
        tag_alu_function_source_v := tag_alu_function_from_sec_pol;
        virtual_mem_source_v := virtual_mem_adder;
        immediate_v := "0000" & opcode(11 downto 0);
        --tag_function := tag_alu_add;
        --reg_dst_source_v := reg_dst_not_used;
        -- Dealing with special MOV case
        case tag_prop_sources is
            when tag_source_prop_en =>
                reg_dst_source_v := reg_dst_from_alu;
                reg_file_w_en := '1';
                tag_function := tag_alu_copy_src1;
            when tag_source_address_prop_en => 
                tag_mem_source := tag_mem_write32;
                reg_file_w_en := '0';
                reg_dst_source_v := reg_dst_from_mem;
                tag_function := tag_alu_add;
            when tag_source_and_address =>
                tag_mem_source := tag_mem_write32;
                tag_function := tag_alu_add;
                reg_dst_source_v := reg_dst_from_mem;
            when others => 
                reg_dst_source_v := reg_dst_not_used;
                tag_function := tag_alu_nothing;
        end case;
    -- @TODO: FIX this opcode ...       
    when "100101" => -- trf(reg_dst_v) <= tag(Mem(trf(reg_src))) 0x25
        reg_file_en := '1';
        reg_file_w_en := '1';
        reg_src2_source_v := reg_src2_from_imm;
        tag_alu_function_source_v := tag_alu_function_from_sec_pol;    
        --tag_function := tag_alu_copy_src1;
        reg_dst_source_v := reg_dst_from_mem;
        tag_mem_source := tag_mem_read32;
        stall <= '1';
        arm_opcode_t := opcode(25 downto 22);
        immediate_v := "0000" & opcode(11 downto 0);
        virtual_mem_source_v := virtual_mem_adder;
    when "101100" => -- tag(Mem(trf(reg_dst_v + offset))) <= trf(reg_src) 0x2c
        reg_file_en := '1';
        reg_file_2_en := '1'; -- reg_src_2
        reg_file_w_en := '0';
        reg_src2_source_v := reg_src2_from_imm;
        tag_alu_function_source_v := tag_alu_function_from_opcode;
        reg_src2_v := reg_src1_v;
        reg_src1_v := reg_dst_v;
        tag_function := tag_alu_add;
        reg_dst_source_v := reg_dst_from_alu;
        tag_mem_source := tag_mem_write32;
        immediate_v := "0000" & opcode(11 downto 0);
        arm_opcode_t := opcode(25 downto 22);
 --       stall <= '1';
        virtual_mem_source_v := virtual_mem_adder;
    when "101101" => -- trf(reg_dst_v) <= tag(Mem(trf(reg_src))) 0x2d
        reg_file_en := '1';
        reg_file_w_en := '1';
        reg_src2_source_v := reg_src2_from_imm;
        tag_alu_function_source_v := tag_alu_function_from_opcode;
        tag_function := tag_alu_add;
        reg_dst_source_v := reg_dst_from_mem;
        tag_mem_source := tag_mem_read32;
        stall <= '1';
        immediate_v := "0000" & opcode(11 downto 0);
        virtual_mem_source_v := virtual_mem_adder;
        --arm_opcode_t := opcode(25 downto 22);
    when "100110" => -- trf(reg_dst_v) <= trf(reg_src1_v) FUNCTION trf(reg_src2_v) 0x26
        reg_file_en := '1';
        reg_file_2_en := '1';
        reg_file_w_en := '1';
        tag_alu_function_source_v := tag_alu_function_from_opcode; 
        reg_src2_source_v := reg_src2_from_reg;
        case tag_function_op is 
            when "000000" =>  -- 0x00 ADD
                tag_function := tag_alu_add; 
            when "000001" =>  -- 0x01 SUB
                tag_function := tag_alu_subtract;
            when "000010" =>
                tag_function := tag_alu_or;
            when "000011" =>
                tag_function := tag_alu_and;
            when "000100" =>
                tag_function := tag_alu_xor;
            when "000101" =>
                tag_function := tag_alu_nor;
            when "000110" =>
                tag_function := tag_alu_copy_src1;
            when "000111" => 
                tag_function := tag_alu_copy_src2;
            when others => 
                tag_function := tag_alu_nothing;
        end case;
    -- @TODO: Fix memory instructions    
    when "100111" => -- Mem(instrumentation)+offset <= trf(reg_src1_v); 0x27
        -- the 2nd register value is the write value in data_mem_ctl
        -- Therefore, we enable 2nd register read and we take reg_src1_v operand
        reg_file_2_en := '1';
        reg_file_w_en := '0';
        reg_src2_v := reg_src1_v;
        --reg_dst_source_v := reg_dst_from_instr;
        reg_dst_source_v := reg_dst_from_mem;
        en_read_instrumentation_s <= '1';
        --tag_mem_source := tag_mem_write32;
        --reg_dst_source_v := reg_dst_from_alu;
        tag_function := tag_alu_nothing;
        virtual_mem_source_v := virtual_mem_instrumentation;
        tag_mem_source := tag_mem_write32;
        --instrumentation_address <= std_logic_vector(unsigned(instrumentation_address) + 4);
    -- @TODO: Fix memory instructions    
    when "101000" => -- reg_dst <= tag(Mem(instrumentation)+offset); 0x28
        reg_file_en := '0';
        reg_file_w_en := '1';
        reg_src1_source_v := reg_src1_from_instr; 
        en_read_instrumentation_s <= '1';
        virtual_mem_source_v := virtual_mem_instrumentation;
        tag_mem_source := tag_mem_read32;
        stall <= '1';
        --instrumentation_address <= std_logic_vector(unsigned(instrumentation_address) + 4);
    --@TODO: test in simulation the behavior 
    when "101001" => -- Kblare_Mem <= trf(reg_src1) write tag to KBLARE IP (PL2PS) 0x29
        -- the 2nd register value is the write value in data_mem_ctl
        -- Therefore, we enable 2nd register read and we take reg_src1_v operand
        reg_file_2_en := '1';
        reg_file_w_en := '0';
        reg_src2_v := reg_src1_v;
        tag_alu_function_source_v := tag_alu_function_from_opcode;
        tag_function := tag_alu_nothing;
        en_write_kblare_s <= '1';
        tag_mem_source := tag_mem_write32; 
        reg_dst_source_v := reg_dst_not_used;
        virtual_mem_source_v := virtual_mem_blare;
    
    when "101010" => -- reg_dst <= KBLARE_Mem 0x2a (PS2PL)
        reg_file_en := '0';
        reg_file_w_en := '1';
        --reg_dst_source_v := reg_dst_from_kblare;
        reg_dst_source_v := reg_dst_from_mem;
        en_read_kblare_s <= '1';
        virtual_mem_source_v := virtual_mem_blare;
        tag_mem_source := tag_mem_read32;                
        stall <= '1';
        --kblare_ps2pl_read_address <= std_logic_vector(unsigned(kblare_ps2pl_read_address) + 4);
    -- Tag check instructions 
    --@TODO: Fix these opcodes
    when "101110" => -- check registers 0x2e
        reg_file_en := '1';
        reg_file_2_en := '1';
        reg_file_w_en := '0';
        reg_dst_source_v := reg_dst_not_used;
    when "101111" => -- check tag of memory address 0x2f
        reg_file_en := '1';
        reg_file_2_en := '1';
        reg_file_w_en := '0';
        reg_dst_source_v := reg_dst_not_used;
    --------------------------------------------------------------------
    --- Floating point instructions 
    --------------------------------------------------------------------
    -- tag initialization opcodes
    when "110000" =>  -- trf_fp(reg_dst_v) <= imm  0x30
        --fp_reg_file_en := '1';
        fp_reg_file_w_en := '1';
        reg_src2_source_v := reg_src2_from_imm;
        tag_alu_function_source_v := tag_alu_function_from_opcode;
        tag_function := tag_alu_copy_src2;
        reg_dst_v := opcode(25 downto 21);
        reg_src1_v := opcode(20 downto 16);
        immediate_v := opcode(15 downto 0);
    when "110001" => -- trf(reg_dst_v) <= reg_src1_v 0x31
        fp_reg_file_en := '1';
        fp_reg_file_w_en := '1';
        reg_src2_source_v := reg_src2_not_used;
        tag_alu_function_source_v := tag_alu_function_from_opcode;
        tag_function := tag_alu_copy_src1;
        reg_dst_v := opcode(25 downto 21);
        reg_src1_v := opcode(20 downto 16);
        immediate_v := opcode(15 downto 0);
    -- @TODO: FIX this opcode ...
    when "110010" => -- mem(trf(reg_dst_v + imm)) <= reg_src1_v 0x32
        reg_src2_v := opcode(20 downto 16);
        fp_reg_file_2_en := '1';
        reg_file_en := '1';
        --reg_file_2_en := '1';
        fp_reg_file_w_en := '0';
        tag_function := tag_alu_add;
        reg_src2_source_v := reg_src2_from_imm;
        tag_alu_function_source_v := tag_alu_function_from_opcode;
        --reg_dst_v := opcode(25 downto 21);
        reg_src1_v := opcode(25 downto 21);
        immediate_v := opcode(15 downto 0);
        reg_dst_source_v := reg_dst_from_alu;
        tag_mem_source := tag_mem_write32;
        virtual_mem_source_v := virtual_mem_adder;
    when "111011" => -- reg_dst = reg_src | imm_16<<16  (lui FP) 0x3b
        fp_reg_file_en := '1';
        fp_reg_file_w_en := '1';
        reg_src2_source_v := reg_src2_from_upper_imm;
        tag_alu_function_source_v := tag_alu_function_from_opcode;
        tag_function := tag_alu_or;
        reg_dst_v := opcode(25 downto 21);
        reg_src1_v := opcode(20 downto 16);
        immediate_v := opcode(15 downto 0);
    
    -- tag update opcodes    
    when "110011" => -- trf_fp(reg_dst_v) <= trf_fp(reg_src1_v) OPERATIOn trf_fp(reg_src2_v) 0x33
        fp_reg_file_en := '1';
        fp_reg_file_2_en := '1';
        fp_reg_file_w_en := '1';
        reg_src2_source_v := reg_src2_from_reg;
        tag_alu_function_source_v := tag_alu_function_from_sec_pol;
        arm_opcode_t := opcode(25 downto 22);
    -- @TODO: FIX this opcode ...
    -- The reg_dest_v is a register that contains memory address. So, it 
    -- should be a general purpose register. It can not be in the FP regfile.
    when "110100" => -- tag(Mem(trf(reg_dst_v) + offset)) <= trf_fp(reg_src) 0x34
        fp_reg_file_en := '1'; -- to read tag (trf_fp(reg_src))
        reg_file_en := '1';
        reg_src2_source_v := reg_src2_from_imm;
        arm_opcode_t := opcode(25 downto 22);
        reg_file_en := '1'; -- to read reg_dst_v
        fp_reg_file_w_en := '0';
        tag_alu_function_source_v := tag_alu_function_from_sec_pol;
        immediate_v := "0000" & opcode(11 downto 0);
        --tag_function := tag_alu_add;
        --reg_dst_source_v := reg_dst_from_mem;
        reg_dst_source_v := reg_dst_not_used;
        tag_mem_source := tag_mem_write32;
        virtual_mem_source_v := virtual_mem_adder;
    -- @TODO: FIX this opcode ...       
    when "110101" => -- trf(reg_dst_v) <= tag(Mem(trf(reg_src + offset))) 0x35
        fp_reg_file_en := '1';
        fp_reg_file_w_en := '1';
        reg_src2_source_v := reg_src2_from_imm;
        tag_alu_function_source_v := tag_alu_function_from_sec_pol;
        --tag_function := tag_alu_copy_src1;
        reg_dst_source_v := reg_dst_from_mem;
        tag_mem_source := tag_mem_read32;
        arm_opcode_t := opcode(25 downto 22);
        immediate_v := "0000" & opcode(11 downto 0);
        virtual_mem_source_v := virtual_mem_adder;
        stall <= '1';
    -- @TODO: FIX this opcode ...        
    when "111100" => -- tag(Mem(trf(reg_dst_v + offset))) <= trf(reg_src) 0x3c
        reg_file_en := '1';
        fp_reg_file_2_en := '1';
        fp_reg_file_w_en := '0';
        reg_src2_source_v := reg_src2_from_imm;
        tag_alu_function_source_v := tag_alu_function_from_opcode;
        reg_src2_v := reg_src1_v;
        reg_src1_v := reg_dst_v;
        tag_function := tag_alu_add;
        reg_dst_source_v := reg_dst_from_alu;
        tag_mem_source := tag_mem_write32;
        arm_opcode_t := opcode(25 downto 22);
        immediate_v := "0000" & opcode(11 downto 0);
        virtual_mem_source_v := virtual_mem_adder;
    -- @TODO: FIX this opcode ...       
    when "111101" => -- trf(reg_dst_v) <= tag(Mem(trf(reg_src))) 0x3d
        reg_file_en := '1';
        fp_reg_file_w_en := '1';
        reg_src2_source_v := reg_src2_from_imm;
        tag_alu_function_source_v := tag_alu_function_from_opcode;
        tag_function := tag_alu_add;
        reg_dst_source_v := reg_dst_from_mem;
        tag_mem_source := tag_mem_read32;
        stall <= '1';
        immediate_v := "0000" & opcode(11 downto 0);
        virtual_mem_source_v := virtual_mem_adder;
        --arm_opcode_t := opcode(25 downto 22);
    when "110110" => -- trf(reg_dst_v) <= trf(reg_src1_v) FUNCTION trf(reg_src2_v) 0x36
        fp_reg_file_en := '1';
        fp_reg_file_2_en := '1';
        fp_reg_file_w_en := '1';
        tag_alu_function_source_v := tag_alu_function_from_opcode; 
        reg_src2_source_v := reg_src2_from_reg;
        case tag_function_op is 
            when "000000" =>  -- 0x00 ADD
                tag_function := tag_alu_add; 
            when "000001" =>  -- 0x01 SUB
                tag_function := tag_alu_subtract;     
            when "000010" =>
                tag_function := tag_alu_or;
            when "000011" =>
                tag_function := tag_alu_and;
            when "000100" =>
                tag_function := tag_alu_xor;
            when "000101" =>
                tag_function := tag_alu_nor;
            when "000110" =>
                tag_function := tag_alu_copy_src1;
            when "000111" => 
                tag_function := tag_alu_copy_src2;
            when others => 
                tag_function := tag_alu_nothing;
        end case;
    -- @TODO: Fix memory instructions    
    when "110111" => -- Mem(instrumentation) <= trf_fp(reg_src1_v); 0x37
        fp_reg_file_en := '1';
        fp_reg_file_w_en := '0';
        --reg_dst_source_v := reg_dst_from_instr;
        reg_dst_source_v := reg_dst_from_mem;
        en_read_instrumentation_s <= '1';
        tag_mem_source := tag_mem_write32;
        reg_dst_source_v := reg_dst_not_used;
        virtual_mem_source_v := virtual_mem_instrumentation;
        --instrumentation_address <= std_logic_vector(unsigned(instrumentation_address) + 4);
    when "111000" => -- reg_dst <= tag(Mem(instrumentation)); 0x38 (vldr arm instruction)
        --fp_reg_file_en := '1';
        fp_reg_file_w_en := '1';
        reg_src1_source_v := reg_src1_from_instr; 
        en_read_instrumentation_s <= '1';
        virtual_mem_source_v := virtual_mem_instrumentation;
        --tag_mem_source := tag_mem_read32;
        --instrumentation_address <= std_logic_vector(unsigned(instrumentation_address) + 4);
    --@TODO: test in simulation the behavior 
    when "111001" => -- Kblare_Mem <= reg_src1 write tag to KBLARE IP PL2PS  0x39
        fp_reg_file_en := '1';
        fp_reg_file_w_en := '1';
        tag_alu_function_source_v := tag_alu_function_from_opcode;
        tag_function := tag_alu_copy_src1;
        en_write_kblare_s <= '1';        
        --tag_mem_source := tag_mem_write32; 
        reg_dst_source_v := reg_dst_not_used;
        virtual_mem_source_v := virtual_mem_blare;
    when "111010" => -- reg_dst <= KBLARE_Mem  0x3a
        --fp_reg_file_en := '1';
        --fp_reg_file_w_en := '1';
        ----reg_dst_source_v := reg_dst_from_kblare;
        --reg_dst_source_v := reg_dst_from_mem;
        --en_read_kblare_s <= '1';
        --virtual_mem_source_v := virtual_mem_blare;
        --tag_mem_source := tag_mem_read32;                
        --kblare_ps2pl_read_address <= std_logic_vector(unsigned(kblare_ps2pl_read_address) + 4);
    -- Tag check instructions 
    --@TODO: Fix these opcodes
    when "111110" => -- check registers 0x3e
        fp_reg_file_en := '1';
        fp_reg_file_2_en := '1';
        fp_reg_file_w_en := '0';
        reg_dst_source_v := reg_dst_not_used;
    when "111111" => -- check tag of memory address 0x3f
        fp_reg_file_en := '1';
        fp_reg_file_2_en := '1';
        fp_reg_file_w_en := '0';
        reg_dst_source_v := reg_dst_not_used;

    when others => 
        reg_dst_source_v := reg_dst_not_used;
    end case;
    -- variables
    --reg_file_en := not fp_reg_file_en;
    --reg_file_w_en := not fp_reg_file_w_en;
    -- signals assignments with variables
    virtual_mem_source <= virtual_mem_source_v;
    mem_source_o <= mem_source_v;
    tag_mem_source_o <= tag_mem_source;
    tag_alu_func <= tag_function;
    reg_dst <= reg_dst_v;
    reg_src1 <= reg_src1_v;
    reg_src2 <= reg_src2_v;
    arm_opcode_class <= arm_opcode_t;
    immediate <= immediate_v;
    reg_dst_source <= reg_dst_source_v;
    reg_src2_source <= reg_src2_source_v;
    tag_alu_function_source <= tag_alu_function_source_v;
    reg_src1_source <= reg_src1_source_v;
    rf_en <= reg_file_en;
    rf_2_en <= reg_file_2_en;
    rf_w_en <= reg_file_w_en;
    fp_rf_en <= fp_reg_file_en;
    fp_rf_2_en <= fp_reg_file_2_en;
    fp_rf_w_en <= fp_reg_file_w_en;
    data_mem_address <= data_mem_address_v;
    config_reg_index <= config_reg_index_v;
    mmu_write_en <= mmu_write_en_v;
    config_reg_file_w_en <= config_reg_file_w_en_v;
end process;
    
    en_write_kblare_o <= en_write_kblare_s;

    process(clk)
    variable kblare_pl2ps_write_address_v : std_logic_vector(31 downto 0);
    begin
    if rising_edge(clk) then
        if reset = '1' then 
            kblare_pl2ps_write_address_v := (others => '0');
        else
            if en_write_kblare_s = '1' then 
                kblare_pl2ps_write_address_v := std_logic_vector(unsigned(kblare_pl2ps_write_address) + 4);
            end if;
        end if;
        kblare_pl2ps_write_address <= kblare_pl2ps_write_address_v;
    end if;    
    end process;
        


    --instrumentation_inst: bram_reader port map(
    --    clk         => clk, 
    --    reset       => reset, 
    --    rd_en       => en_read_instrumentation_s,
    --    address     => instrumentation_address_r,
    --    rd_data     => instrumentation_data,
    --    bram_o      => instrumentation_bram_o, 
    --    bram_i      => instrumentation_bram_i
    --); 

    --kblare_ps2pl_read_inst: bram_reader port map(
    --    clk         => clk, 
    --    reset       => reset, 
    --    rd_en       => en_read_kblare_s,
    --    address     => kblare_ps2pl_read_address_r,
    --    rd_data     => kblare_data_o,
    --    bram_o      => kblare_read_bram_o, 
    --    bram_i      => kblare_read_bram_i
    --);

    --kblare_pl2ps_write_inst: bram_writer port map(
    --    clk         => clk, 
    --    reset       => reset, 
    --    enable      => en_write_kblare_i, -- pipelined version of en_write_kblare_s;
    --    address     => kblare_pl2ps_write_address, 
    --    data_in     => kblare_data_i,
    --    bram_o      => kblare_write_bram_o
    --);
    en_read_instrumentation <= en_read_instrumentation_s;
    en_read_kblare_ps2pl <= en_read_kblare_s;
end; -- architecture 
