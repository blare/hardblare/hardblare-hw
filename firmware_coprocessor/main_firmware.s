	.file	1 "main_firmware.c"
	.section .mdebug.abi32
	.previous
	.nan	legacy
	.gnu_attribute 4, 1
	.text
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	search_annotations_offset_in_bbt
	.type	search_annotations_offset_in_bbt, @function
search_annotations_offset_in_bbt:
	.frame	$fp,32,$31		# vars= 24, regs= 1/0, args= 0, gp= 0
	.mask	0x40000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	sw	$fp,28($sp)
	move	$fp,$sp
	sw	$4,32($fp)
	li	$2,1073741824			# 0x40000000
	lw	$2,0($2)
	sw	$2,4($fp)
	li	$2,1073741824			# 0x40000000
	ori	$2,$2,0x4
	sw	$2,8($fp)
	sw	$0,0($fp)
	j	$L2
	nop

$L5:
	lw	$2,0($fp)
	sll	$2,$2,3
	lw	$3,8($fp)
	addu	$2,$3,$2
	sw	$2,12($fp)
	lw	$2,12($fp)
	lw	$2,0($2)
	sw	$2,16($fp)
	lw	$3,32($fp)
	lw	$2,16($fp)
	bne	$3,$2,$L3
	nop

	lw	$2,12($fp)
	addiu	$2,$2,4
	lw	$2,0($2)
	sw	$2,20($fp)
	lw	$2,20($fp)
	j	$L4
	nop

$L3:
	lw	$2,0($fp)
	addiu	$2,$2,1
	sw	$2,0($fp)
$L2:
	lw	$3,0($fp)
	lw	$2,4($fp)
	sltu	$2,$3,$2
	bne	$2,$0,$L5
	nop

	move	$2,$0
$L4:
	move	$sp,$fp
	lw	$fp,28($sp)
	addiu	$sp,$sp,32
	j	$31
	nop

	.set	macro
	.set	reorder
	.end	search_annotations_offset_in_bbt
	.size	search_annotations_offset_in_bbt, .-search_annotations_offset_in_bbt
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	writting_annotations_to_second_stage
	.type	writting_annotations_to_second_stage, @function
writting_annotations_to_second_stage:
	.frame	$fp,24,$31		# vars= 16, regs= 1/0, args= 0, gp= 0
	.mask	0x40000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-24
	sw	$fp,20($sp)
	move	$fp,$sp
	sw	$4,24($fp)
	sw	$5,28($fp)
	sw	$6,32($fp)
	sw	$0,0($fp)
	sw	$0,4($fp)
	sw	$0,4($fp)
	j	$L7
	nop

$L8:
	lw	$2,4($fp)
	sll	$2,$2,2
	lw	$3,24($fp)
	addu	$2,$3,$2
	lw	$2,0($2)
	sw	$2,8($fp)
	lw	$2,4($fp)
	sll	$2,$2,2
	lw	$3,32($fp)
	addu	$2,$3,$2
	lw	$3,8($fp)
	sw	$3,0($2)
	lw	$2,0($fp)
	addiu	$2,$2,1
	sw	$2,0($fp)
	lw	$2,4($fp)
	addiu	$2,$2,1
	sw	$2,4($fp)
$L7:
	lw	$3,4($fp)
	lw	$2,28($fp)
	sltu	$2,$3,$2
	bne	$2,$0,$L8
	nop

	lw	$2,0($fp)
	move	$sp,$fp
	lw	$fp,20($sp)
	addiu	$sp,$sp,24
	j	$31
	nop

	.set	macro
	.set	reorder
	.end	writting_annotations_to_second_stage
	.size	writting_annotations_to_second_stage, .-writting_annotations_to_second_stage
	.align	2
	.globl	main
	.set	nomips16
	.set	nomicromips
	.ent	main
	.type	main, @function
main:
	.frame	$fp,72,$31		# vars= 48, regs= 2/0, args= 16, gp= 0
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-72
	sw	$31,68($sp)
	sw	$fp,64($sp)
	move	$fp,$sp
$L11:
	li	$2,1610612736			# 0x60000000
	sw	$2,24($fp)
	sw	$0,32($fp)
	sw	$0,20($fp)
	li	$2,536870912			# 0x20000000
	sw	$2,16($fp)
	li	$2,1073741824			# 0x40000000
	sw	$2,36($fp)
$L13:
	lw	$2,28($fp)
	sll	$3,$2,2
	li	$2,536870912			# 0x20000000
	addu	$2,$3,$2
	lw	$2,0($2)
	sw	$2,20($fp)
	lw	$2,28($fp)
	sltu	$2,$2,6
	bne	$2,$0,$L12
	nop

	sw	$0,28($fp)
	li	$2,536870912			# 0x20000000
	sw	$2,16($fp)
$L12:
	lw	$2,28($fp)
	addiu	$2,$2,1
	sw	$2,28($fp)
	lw	$2,20($fp)
	beq	$2,$0,$L13
	nop

$L17:
	lw	$2,20($fp)
	bne	$2,$0,$L14
	nop

	nop
	j	$L11
	nop

$L14:
	lw	$2,20($fp)
	move	$4,$2
	jal	search_annotations_offset_in_bbt
	nop

	sw	$2,40($fp)
	lw	$2,40($fp)
	beq	$2,$0,$L16
	nop

	lw	$2,40($fp)
	addiu	$2,$2,4
	sw	$2,40($fp)
	lw	$2,40($fp)
	sll	$3,$2,2
	li	$2,1073741824			# 0x40000000
	addu	$2,$3,$2
	sw	$2,44($fp)
	lw	$2,44($fp)
	addiu	$2,$2,1
	sw	$2,48($fp)
	lw	$2,44($fp)
	lw	$2,0($2)
	sw	$2,52($fp)
	lw	$2,52($fp)
	beq	$2,$0,$L16
	nop

	lw	$4,48($fp)
	lw	$5,52($fp)
	lw	$6,24($fp)
	jal	writting_annotations_to_second_stage
	nop

	sw	$2,56($fp)
	lw	$3,56($fp)
	lw	$2,52($fp)
	bne	$3,$2,$L16
	nop

	lw	$2,52($fp)
	sll	$2,$2,2
	lw	$3,24($fp)
	addu	$2,$3,$2
	sw	$2,24($fp)
$L16:
	lw	$2,16($fp)
	lw	$2,0($2)
	sw	$2,20($fp)
	lw	$2,16($fp)
	addiu	$2,$2,4
	sw	$2,16($fp)
	j	$L17
	nop

	.set	macro
	.set	reorder
	.end	main
	.size	main, .-main
	.ident	"GCC: (GNU) 4.8.1"
