-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
-- Date        : Sun Jun  7 12:02:52 2020
-- Host        : HardBlare-Workstation running 64-bit Ubuntu 16.04.6 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top design_1_BRAM_to_FIFO_0_0 -prefix
--               design_1_BRAM_to_FIFO_0_0_ design_1_BRAM_to_FIFO_0_2_sim_netlist.vhdl
-- Design      : design_1_BRAM_to_FIFO_0_2
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_BRAM_to_FIFO_0_0_BRAM_to_FIFO is
  port (
    to_fifo_write_enable : out STD_LOGIC;
    to_fifo_read_enable : out STD_LOGIC;
    from_bram_bytes_selection : in STD_LOGIC_VECTOR ( 3 downto 0 );
    from_bram_enable : in STD_LOGIC
  );
end design_1_BRAM_to_FIFO_0_0_BRAM_to_FIFO;

architecture STRUCTURE of design_1_BRAM_to_FIFO_0_0_BRAM_to_FIFO is
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of to_fifo_read_enable0 : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of to_fifo_write_enable0 : label is "soft_lutpair0";
begin
to_fifo_read_enable0: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00010000"
    )
        port map (
      I0 => from_bram_bytes_selection(1),
      I1 => from_bram_bytes_selection(0),
      I2 => from_bram_bytes_selection(2),
      I3 => from_bram_bytes_selection(3),
      I4 => from_bram_enable,
      O => to_fifo_read_enable
    );
to_fifo_write_enable0: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80000000"
    )
        port map (
      I0 => from_bram_bytes_selection(1),
      I1 => from_bram_bytes_selection(0),
      I2 => from_bram_bytes_selection(2),
      I3 => from_bram_bytes_selection(3),
      I4 => from_bram_enable,
      O => to_fifo_write_enable
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_BRAM_to_FIFO_0_0 is
  port (
    from_bram_address : in STD_LOGIC_VECTOR ( 31 downto 0 );
    from_bram_clk : in STD_LOGIC;
    from_bram_data_write : in STD_LOGIC_VECTOR ( 31 downto 0 );
    from_bram_enable : in STD_LOGIC;
    from_bram_reset : in STD_LOGIC;
    from_bram_bytes_selection : in STD_LOGIC_VECTOR ( 3 downto 0 );
    to_bram_data_read : out STD_LOGIC_VECTOR ( 31 downto 0 );
    to_fifo_data_write : out STD_LOGIC_VECTOR ( 31 downto 0 );
    to_fifo_write_enable : out STD_LOGIC;
    to_fifo_read_enable : out STD_LOGIC;
    to_fifo_clk : out STD_LOGIC;
    to_fifo_reset : out STD_LOGIC;
    from_fifo_data_read : in STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_1_BRAM_to_FIFO_0_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_1_BRAM_to_FIFO_0_0 : entity is "design_1_BRAM_to_FIFO_0_2,BRAM_to_FIFO,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of design_1_BRAM_to_FIFO_0_0 : entity is "yes";
  attribute ip_definition_source : string;
  attribute ip_definition_source of design_1_BRAM_to_FIFO_0_0 : entity is "package_project";
  attribute x_core_info : string;
  attribute x_core_info of design_1_BRAM_to_FIFO_0_0 : entity is "BRAM_to_FIFO,Vivado 2018.2";
end design_1_BRAM_to_FIFO_0_0;

architecture STRUCTURE of design_1_BRAM_to_FIFO_0_0 is
  signal \^from_bram_clk\ : STD_LOGIC;
  signal \^from_bram_data_write\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \^from_bram_reset\ : STD_LOGIC;
  signal \^from_fifo_data_read\ : STD_LOGIC_VECTOR ( 31 downto 0 );
begin
  \^from_bram_clk\ <= from_bram_clk;
  \^from_bram_data_write\(31 downto 0) <= from_bram_data_write(31 downto 0);
  \^from_bram_reset\ <= from_bram_reset;
  \^from_fifo_data_read\(31 downto 0) <= from_fifo_data_read(31 downto 0);
  to_bram_data_read(31 downto 0) <= \^from_fifo_data_read\(31 downto 0);
  to_fifo_clk <= \^from_bram_clk\;
  to_fifo_data_write(31 downto 0) <= \^from_bram_data_write\(31 downto 0);
  to_fifo_reset <= \^from_bram_reset\;
U0: entity work.design_1_BRAM_to_FIFO_0_0_BRAM_to_FIFO
     port map (
      from_bram_bytes_selection(3 downto 0) => from_bram_bytes_selection(3 downto 0),
      from_bram_enable => from_bram_enable,
      to_fifo_read_enable => to_fifo_read_enable,
      to_fifo_write_enable => to_fifo_write_enable
    );
end STRUCTURE;
