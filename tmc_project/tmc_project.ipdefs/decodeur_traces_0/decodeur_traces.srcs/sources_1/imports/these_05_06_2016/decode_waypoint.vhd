library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.All;

entity decode_waypoint is
    port(
    clk, reset, start_w, enable  : in std_logic;
    data_in : in std_logic_vector(7 downto 0);
    waypoint_address : out std_logic_vector(31 downto 0);
    stop_w, out_en : out std_logic
    );
end entity;

architecture archi_comportementale of decode_waypoint is
    -- 14 states
    type state_type is (wait_state, waypoint, waypoint_00, waypoint_01, waypoint_10, waypoint_11, waypoint_20, waypoint_21, waypoint_30, waypoint_31, waypoint_4, waypoint_4_ib, waypoint_ib, wait_enable);
    signal state_reg,state_next, temp_state   : state_type := wait_state;
    signal data_reg_s		: std_logic_vector(7 downto 0);

    signal output_data_0 : std_logic_vector(5 downto 0);
    signal output_data_1 : std_logic_vector(6 downto 0);
    signal output_data_2 : std_logic_vector(6 downto 0);
    signal output_data_3 : std_logic_vector(6 downto 0);
    signal output_data_4 : std_logic_vector(2 downto 0);

    signal out_en_s, stop_s : std_logic;
    -- control signals
    signal en_0, en_1, en_2, en_3, en_4 : std_logic;
    signal en_0_reg, en_1_reg, en_2_reg, en_3_reg, en_4_reg : std_logic;    
    begin

    process(clk)
    begin
        if (clk'event and clk='1') then  -- condition better than rising_edge(clk) because of the fact that it also detects 'Z' to '1' transitions
            en_0_reg <= en_0;
            en_1_reg <= en_1;
            en_2_reg <= en_2;
            en_3_reg <= en_3;
            en_4_reg <= en_4;
            data_reg_s <= data_in;
            if reset = '1' then
                state_reg <= wait_state;
            elsif (enable = '1') then
                state_reg <= state_next;
            elsif (enable = '0') then
                state_reg <= wait_enable;
            end if;
        end if;
    end process;

    -----------------------------------------
    -----------------------------------------
    ----- N E X T  S T A T E  L O G I C -----
    -----------------------------------------
    -----------------------------------------
    process(state_reg, data_reg_s, start_w, temp_state)
    begin
        case state_reg is
            when waypoint =>
            -- The waypoint address indicates the last instruction that was executed.
            -- It does not indicate whether that instruction passed its condition code test.
            en_0 <= '1';
            en_1 <= '0';
            en_2 <= '0';
            en_3 <= '0';
            en_4 <= '0';
            out_en_s <= '0';
            stop_s <= '0';
            if data_reg_s(7) = '1' then
                state_next <= waypoint_01;
            else
                state_next <= waypoint_00;
            end if;

            when waypoint_01 =>
            en_0 <= '0';
            en_1 <= '1';
            en_2 <= '0';
            en_3 <= '0';
            en_4 <= '0';
            out_en_s <= '0';
            stop_s <= '0';
            if (data_reg_s(7) = '1') then
                state_next <= waypoint_11;
            else
                state_next <= waypoint_10;
            end if;

            when waypoint_11 =>
            en_0 <= '0';
            en_1 <= '0';
            en_2 <= '1';
            en_3 <= '0';
            en_4 <= '0';
            out_en_s <= '0';
            stop_s <= '0';
            if (data_reg_s(7) = '1') then
                state_next <= waypoint_21;
            else
                state_next <= waypoint_20;
            end if;

            when waypoint_21 =>
            en_0 <= '0';
            en_1 <= '0';
            en_2 <= '0';
            en_3 <= '1';
            en_4 <= '0';
            out_en_s <= '0';
            stop_s <= '0';
            if (data_reg_s(7) = '1') then
                state_next <= waypoint_31;
            else
                state_next <= waypoint_30;
            end if;

            when waypoint_31 =>
            en_4 <= '1';
            en_0 <= '0';
            en_1 <= '0';
            en_2 <= '0';
            en_3 <= '0';
            out_en_s <= '0';
            stop_s <= '0';
            if (data_reg_s(6) = '1') then
                state_next <= waypoint_4_ib;
            else
                state_next <= waypoint_4;
            end if;

            when waypoint_4_ib =>
            en_0 <= '0';
            en_1 <= '0';
            en_2 <= '0';
            en_3 <= '0';
            en_4 <= '0';
            out_en_s <= '0';
            stop_s <= '0';
            state_next <= waypoint_ib;

            when waypoint_ib | waypoint_00 | waypoint_10 | waypoint_20 | waypoint_30 | waypoint_4 =>
            out_en_s <= '1';
            stop_s <= '1';
            en_0 <= '0';
            en_1 <= '0';
            en_2 <= '0';
            en_3 <= '0';
            en_4 <= '0';
            if (data_reg_s = X"72") then
                state_next <= waypoint;
            else
                state_next <= wait_state;
            end if;

            when wait_state =>
            en_0 <= '0';
            en_1 <= '0';
            en_2 <= '0';
            en_3 <= '0';
            en_4 <= '0';
            out_en_s <= '0';
            stop_s <= '0';
            if start_w = '1' then
                state_next <= waypoint;
            else
                state_next <= wait_state;
            end if;
            
            when wait_enable =>
            en_0 <= '0';
            en_1 <= '0';
            en_2 <= '0';
            en_3 <= '0';
            en_4 <= '0';
            out_en_s <= '0';
            stop_s <= '0';
            state_next <= temp_state; 
        end case;
    end process;

    process(clk)
    begin
        if (rising_edge(clk)) then
            if en_0 = '1' then
                output_data_0 <= data_reg_s(6 downto 1);
            end if;
        end if;
    end process;

    process(clk)
    begin
        if rising_edge(clk) then 
            if en_1 = '1' then
                output_data_1 <= data_reg_s(6 downto 0);
            end if;
        end if;
    end process;

    process(clk)
        begin
            if rising_edge(clk) then 
                if en_2 = '1' then
                    output_data_2 <= data_reg_s(6 downto 0);
                end if;
            end if;
        end process;

    process(clk)
    begin
        if rising_edge(clk) then 
            if en_3 = '1' then
                output_data_3 <= data_reg_s(6 downto 0);
            end if;
        end if;
    end process;

    process(clk)
    begin
        if rising_edge(clk) then 
            if en_4 = '1' then
                output_data_4 <= data_reg_s(2 downto 0);
            end if;
        end if;
    end process;

    waypoint_address <= output_data_4 & output_data_3 & output_data_2 & output_data_1 & output_data_0 & "00";
    out_en <= out_en_s;
    stop_w <= stop_s;
end architecture;
