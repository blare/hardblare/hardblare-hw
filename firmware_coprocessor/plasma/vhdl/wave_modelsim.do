onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix hexadecimal /tbench/u1_plasma/clk
add wave -noupdate -radix hexadecimal /tbench/u1_plasma/reset
add wave -noupdate -radix hexadecimal /tbench/u1_plasma/uart_write
add wave -noupdate -radix hexadecimal /tbench/u1_plasma/uart_read
add wave -noupdate -radix hexadecimal /tbench/u1_plasma/address
add wave -noupdate -radix hexadecimal /tbench/u1_plasma/byte_we
add wave -noupdate -radix hexadecimal /tbench/u1_plasma/data_write
add wave -noupdate -radix hexadecimal /tbench/u1_plasma/data_read
add wave -noupdate -radix hexadecimal /tbench/u1_plasma/mem_pause_in
add wave -noupdate -radix hexadecimal /tbench/u1_plasma/no_ddr_start
add wave -noupdate -radix hexadecimal /tbench/u1_plasma/no_ddr_stop
add wave -noupdate -radix hexadecimal /tbench/u1_plasma/gpio0_out
add wave -noupdate -radix hexadecimal /tbench/u1_plasma/gpioA_in
add wave -noupdate -radix hexadecimal /tbench/u1_plasma/address_next
add wave -noupdate -radix hexadecimal /tbench/u1_plasma/byte_we_next
add wave -noupdate -radix hexadecimal /tbench/u1_plasma/cpu_address
add wave -noupdate -radix hexadecimal /tbench/u1_plasma/cpu_byte_we
add wave -noupdate -radix hexadecimal /tbench/u1_plasma/cpu_data_w
add wave -noupdate -radix hexadecimal /tbench/u1_plasma/cpu_data_r
add wave -noupdate -radix hexadecimal /tbench/u1_plasma/cpu_pause
add wave -noupdate -radix hexadecimal /tbench/u1_plasma/data_read_uart
add wave -noupdate -radix hexadecimal /tbench/u1_plasma/write_enable
add wave -noupdate -radix hexadecimal /tbench/u1_plasma/eth_pause_in
add wave -noupdate -radix hexadecimal /tbench/u1_plasma/eth_pause
add wave -noupdate -radix hexadecimal /tbench/u1_plasma/mem_busy
add wave -noupdate -radix hexadecimal /tbench/u1_plasma/enable_misc
add wave -noupdate -radix hexadecimal /tbench/u1_plasma/enable_uart
add wave -noupdate -radix hexadecimal /tbench/u1_plasma/enable_uart_read
add wave -noupdate -radix hexadecimal /tbench/u1_plasma/enable_uart_write
add wave -noupdate -radix hexadecimal /tbench/u1_plasma/enable_eth
add wave -noupdate -radix hexadecimal /tbench/u1_plasma/gpio0_reg
add wave -noupdate -radix hexadecimal /tbench/u1_plasma/uart_write_busy
add wave -noupdate -radix hexadecimal /tbench/u1_plasma/uart_data_avail
add wave -noupdate -radix hexadecimal /tbench/u1_plasma/irq_mask_reg
add wave -noupdate -radix hexadecimal /tbench/u1_plasma/irq_status
add wave -noupdate -radix hexadecimal /tbench/u1_plasma/irq
add wave -noupdate -radix hexadecimal /tbench/u1_plasma/irq_eth_rec
add wave -noupdate -radix hexadecimal /tbench/u1_plasma/irq_eth_send
add wave -noupdate -radix hexadecimal /tbench/u1_plasma/counter_reg
add wave -noupdate -radix hexadecimal /tbench/u1_plasma/ram_enable
add wave -noupdate -radix hexadecimal /tbench/u1_plasma/ram_byte_we
add wave -noupdate -radix hexadecimal /tbench/u1_plasma/ram_address
add wave -noupdate -radix hexadecimal /tbench/u1_plasma/ram_data_w
add wave -noupdate -radix hexadecimal /tbench/u1_plasma/ram_data_r
add wave -noupdate -radix hexadecimal /tbench/u1_plasma/cache_access
add wave -noupdate -radix hexadecimal /tbench/u1_plasma/cache_checking
add wave -noupdate -radix hexadecimal /tbench/u1_plasma/cache_miss
add wave -noupdate -radix hexadecimal /tbench/u1_plasma/cache_hit
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {9999961546 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 255
configure wave -valuecolwidth 61
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {97951914 ps} {99233654 ps}
