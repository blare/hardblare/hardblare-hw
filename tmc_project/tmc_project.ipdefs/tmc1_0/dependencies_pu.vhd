---------------------------------------------------------------------
-- TITLE: dependencies processing unit
-- AUTHOR: MAW
-- DATE CREATED: 07/11/2017
-- FILENAME: dependencies_pu.vhd
-- PROJECT: DIFT coprocessor
-- DESCRIPTION:
-- Top level VHDL document that ties the eight other entities together.
-- Implements a DIFT Coprocessor.
---------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.dependencies_pack.all;

entity dependencies_pu is
   port(clk           : in std_logic;
        reset_in      : in std_logic;
        pc_enable     : in std_logic;
        stall         : in std_logic;
        intr_in       : in std_logic;
        --initial_config_sec_policy_registers : in sec_policy_registers_array;

        i_mem_clk               : out std_logic;
        i_mem_reset             : out std_logic;
        i_mem_en                : out std_logic;
        i_mem_address           : out std_logic_vector(31 downto 0);
        i_mem_data_w            : out std_logic_vector(31 downto 0);
        i_mem_data_r            : in std_logic_vector(31 downto 0);
        i_mem_sel               : out std_logic_vector(3 downto 0);

        mem_clk                 : out std_logic;
        mem_reset               : out std_logic;
        mem_address             : out std_logic_vector(31 downto 0);
        mem_data_w              : out std_logic_vector(31 downto 0);
        mem_data_r              : in std_logic_vector(31 downto 0);
        mem_sel                 : out std_logic_vector(3 downto 0);
        mem_en                  : out std_logic;
        
        -- config_mem
        config_mem_clk          : out std_logic;
        config_mem_reset        : out std_logic;
        config_mem_address      : out std_logic_vector(31 downto 0);
        config_mem_data_w       : out std_logic_vector(31 downto 0) := (others => '0');
        config_mem_data_r       : in std_logic_vector(31 downto 0);
        config_mem_sel          : out std_logic_vector(3 downto 0) := "1111";
        config_mem_en           : out std_logic;
        -- instrumentation fifo read interface
        instrumentation_empty   : in std_logic;
        instrumentation_data_r  : in  std_logic_vector(31 downto 0);
        instrumentation_enable  : out std_logic;

        -- kblare_ps2pl fifo read interface
        --kblare_ps2pl_empty      : in  std_logic;
        --kblare_ps2pl_data_r     : in  std_logic_vector(31 downto 0);
        --kblare_ps2pl_enable     : out std_logic;

        -- kblare_pl2ps fifo write interface
        --kblare_pl2ps_full       : in  std_logic;
        --kblare_pl2ps_data_w     : out std_logic_vector(31 downto 0) := (others => '0');
        --kblare_pl2ps_enable     : out std_logic;

        -- kblare_ps2pl bram interface
        kblare_ps2pl_clk       : out std_logic;
        kblare_ps2pl_reset     : out std_logic;
        kblare_ps2pl_address   : out std_logic_vector(31 downto 0);
        kblare_ps2pl_data_w    : out std_logic_vector(31 downto 0);
        kblare_ps2pl_data_r    : in std_logic_vector(31 downto 0);
        kblare_ps2pl_sel       : out std_logic_vector(3 downto 0);
        kblare_ps2pl_en        : out std_logic;

        -- kblare_pl2ps bram interface
        kblare_pl2ps_clk       : out std_logic;
        kblare_pl2ps_reset     : out std_logic;
        kblare_pl2ps_address   : out std_logic_vector(31 downto 0);
        kblare_pl2ps_data_w    : out std_logic_vector(31 downto 0);
        kblare_pl2ps_data_r    : in std_logic_vector(31 downto 0);
        kblare_pl2ps_sel       : out std_logic_vector(3 downto 0);
        kblare_pl2ps_en        : out std_logic;

        ---- axi master interface
	axim_done	: in std_logic;
	axim_error_in: in std_logic;

	axim_address : out std_logic_vector(31 downto 0);
	axim_data_out : out std_logic_vector(31 downto 0);
	axim_data_in : in std_logic_vector(31 downto 0);
	axim_read_en : out std_logic;
	axim_write_en : out std_logic;

	axim_start	: out std_logic;

	enable_type_out: out std_logic_vector(1 downto 0);
--	data_read_out  : out  std_logic_vector(31 downto 0);
--	address_ram_store_out : out std_logic_vector(31 downto 0);
--	mdata_out_store_out: out std_logic_vector(31 downto 0);
--	state_out: out std_logic;

--	write_enable_out : out std_logic;
--	write_data_out : out std_logic_vector(31 downto 0);
--	address_from_MMU_out : out std_logic_vector(25 downto 0);
	data_from_TMC_out : out std_logic_vector(31 downto 0);
	virtual_mem_address_out : out std_logic_vector(31 downto 0);
--	virtual_mem_select_p_out : out std_logic_vector(1 downto 0);
--	c_alu_out : out std_logic_vector(31 downto 0);
	pause_out    : out std_logic;
--	a_bus_p_out : out std_logic_vector(31 downto 0);
	b_bus_p_out : out std_logic_vector(31 downto 0);
--	c_alu_p_out : out std_logic_vector(31 downto 0);
--	c_alu_p_p_out : out std_logic_vector(31 downto 0);
--	a_source_out : out std_logic_vector(1 downto 0);
--	enable_reg_file_s_out : out std_logic;
	reg_source_1_gp_out : out std_logic_vector(31 downto 0);
        -- local BRAM interface
      --  tag_clk       : out std_logic;
      --  tag_rst       : out std_logic;
      --  tag_en        : out std_logic;
      --  tag_write     : out std_logic;
      --  tag_w_en      : out std_logic_vector(3 downto 0);
      --  tag_address_o : out std_logic_vector(31 downto 0);
      --  tag_data_out  : out std_logic_vector(31 downto 0);
      --  tag_rd_data   : in  std_logic_vector(31 downto 0);

        -- interrupt signal(s)
        interrupt     : out std_logic;
        -- debug signals
        t_pc          : out std_logic_vector(31 downto 0);
        t_opcode      : out std_logic_vector(31 downto 0);
        t_r_dest      : out std_logic_vector(31 downto 0)
        );
end; --entity dependencies_pu

architecture logic of dependencies_pu is

component pc_next
   port(clk          : in std_logic;
        reset_in     : in std_logic;
        enable       : in std_logic;
        pc_new       : in std_logic_vector(31 downto 2);
        pause_in     : in std_logic;
        pc_out       : out std_logic_vector(31 downto 0));
end component;

--component mem_ctrl   
--   port(clk          : in std_logic;
--        reset_in     : in std_logic;
--        pause_in     : in std_logic;
--        hazard       : in std_logic;
--        nullify_op   : in std_logic;
--        address_pc   : in std_logic_vector(31 downto 0);
--        opcode_out   : out std_logic_vector(31 downto 0);
                
--        data_read    : out std_logic_vector(31 downto 0);
--        pause_out    : out std_logic;
                
--        mem_address  : out std_logic_vector(31 downto 0);
--        mem_data_r   : in std_logic_vector(31 downto 0);
--        mem_read_v   : in std_logic;
--       mem_pause    : in std_logic);
--end component;

component control_dependencies is
   port(clk, reset              : in std_logic;
        opcode                  : in  std_logic_vector(31 downto 0);
        tag_prop_sources        : in tag_prop_type;
        en_read_instrumentation : out std_logic;
        en_read_kblare_ps2pl    : out std_logic;
        reg_dst                 : out std_logic_vector(4 downto 0);
        reg_src1                : out std_logic_vector(4 downto 0);
        reg_src2                : out std_logic_vector(4 downto 0);
        config_reg_index        : out std_logic_vector(4 downto 0);
        tag_alu_func            : out tag_alu_function_type; 
        arm_opcode_class        : out arm_opcode_type;
        immediate               : out std_logic_vector(15 downto 0);
        --instrumentation_data    : out std_logic_vector(31 downto 0);
        kblare_data_o           : out std_logic_vector(31 downto 0);
        en_write_kblare_o       : out std_logic;
        kblare_pl2ps_write_address_r : out std_logic_vector(31 downto 0);
        virtual_mem_source      : out virtual_mem_source_type;
        --en_write_kblare_i       : in  std_logic;
        --kblare_data_i           : in  std_logic_vector(31 downto 0);
        reg_dst_source          : out reg_dst_source_type;
        reg_src1_source         : out reg_src1_source_type;
        reg_src2_source         : out reg_src2_source_type;
        --instrument              : out std_logic;
        rf_en                   : out std_logic;
        rf_2_en                 : out std_logic;
        rf_w_en                 : out std_logic;
        fp_rf_en                : out std_logic;
        fp_rf_2_en              : out std_logic;
        fp_rf_w_en              : out std_logic;
        config_reg_file_w_en    : out std_logic;
        stall                   : out std_logic;
        mem_source_o            : out mem_source_type;
        mmu_write_en            : out std_logic;
        data_mem_address        : out std_logic;
        tag_mem_source_o        : out tag_mem_source_type;
        null_instruction        : out std_logic;
        tag_alu_function_source : out tag_alu_function_source_type );
end component;

component registers_sec_policy is
   port(clk            : in  std_logic;
        enable_sec_pol : in  std_logic_vector(nb_sec_policy-1 downto 0); -- which security policy to enable
        config_regs    : in  configuration_array;
        arm_opcode_t   : in  arm_opcode_type;
        tag_size       : in  tag_size_policies; --array containing tag size for each security policy
        tag_alu_func   : out tag_alu_function_type; 
        tag_prop_en    : out tag_prop_type;
        tag_check_en   : out tag_check_type;
        tag_alu_check  : out tag_alu_check_function_type );
end component; -- registers_sec_policy

component reg_file is
port (
  clk         : in  std_logic;
  enable      : in  std_logic;
  enable_2    : in  std_logic;
  w_en        : in  std_logic;
  data_in     : in  std_logic_vector(31 downto 0); 
  data_out_a  : out std_logic_vector(31 downto 0);
  data_out_b  : out std_logic_vector(31 downto 0);
  data_out_c  : out std_logic_vector(31 downto 0); -- pc tag value output
  sel_a       : in  std_logic_vector(4 downto 0); 
  sel_b       : in  std_logic_vector(4 downto 0);
  sel_d       : in  std_logic_vector(4 downto 0) );
end component;  --reg_file

component dep_bus_mux is
   port(
        reset                 : in  std_logic;
        imm_in                : in  std_logic_vector(15 downto 0);
        en_rf                 : in  std_logic;
        en_rf_fp              : in  std_logic;
        en_rf_2               : in  std_logic;
        en_rf_2_fp            : in  std_logic;
        reg_source_gp         : in  std_logic_vector(31 downto 0);
        reg_source_fp         : in  std_logic_vector(31 downto 0);
        reg_source            : out std_logic_vector(31 downto 0);
                          
        a_mux                 : in  reg_src1_source_type;
        a_out                 : out std_logic_vector(31 downto 0);
          
        reg_source_gp_2       : in  std_logic_vector(31 downto 0);
        reg_source_fp_2       : in  std_logic_vector(31 downto 0);
        reg_source_2          : out std_logic_vector(31 downto 0);
        b_mux                 : in  reg_src2_source_type;
        b_out                 : out std_logic_vector(31 downto 0);
        
        tag_prop_sources      : in tag_prop_type;
        tag_check_sources     : in tag_check_type;

        enable_sec_policies   : in  std_logic_vector(nb_sec_policy-1 downto 0);
        tag_alu_function_src  : in  tag_alu_function_source_type;
        tag_alu_function_op   : in  tag_alu_function_type;
        tag_alu_function_sec  : in  tag_alu_function_type;
        tag_alu_function_o    : out tag_alu_function_type;
          
        c_bus                 : in  std_logic_vector(31 downto 0);
        c_memory              : in  std_logic_vector(31 downto 0);
        c_tag                 : in  std_logic_vector(31 downto 0);
        --c_instrumentation     : in  std_logic_vector(31 downto 0);
        --c_kblare              : in  std_logic_vector(31 downto 0);
        c_mux                 : in  reg_dst_source_type;
        reg_dest_out          : out std_logic_vector(31 downto 0) );
end component;


component pipeline_TD_TE is 
port (
    clk, reset      : in std_logic;
    pause           : in  std_logic;
    stall           : in std_logic;
    a_in            : in  std_logic_vector(31 downto 0);
    a_out           : out std_logic_vector(31 downto 0);
            
    b_in            : in  std_logic_vector(31 downto 0);
    b_out           : out std_logic_vector(31 downto 0);

    en_read_instrumentation     : in  std_logic;
    en_read_instrumentation_o   : out std_logic;

    en_read_kblare_ps2pl        : in  std_logic;
    en_read_kblare_ps2pl_o      : out std_logic;

    en_write_kblare : in  std_logic;
    en_write_kblare_o:out std_logic;

    write_pl2ps_addr: in std_logic_vector(31 downto 0);
    write_pl2ps_addr_o: out std_logic_vector(31 downto 0);

    imm_in          : in  std_logic_vector(15 downto 0);
    imm_out         : out std_logic_vector(15 downto 0);

    reg_source_1    : in  std_logic_vector(31 downto 0);
    reg_source_1_out: out std_logic_vector(31 downto 0);

    reg_source_2    : in  std_logic_vector(31 downto 0);
    reg_source_2_out: out std_logic_vector(31 downto 0);

    reg_pc          : in  std_logic_vector(31 downto 0);
    reg_pc_out      : out std_logic_vector(31 downto 0);

    reg_instr       : in  std_logic_vector(31 downto 0);
    reg_instr_out   : out std_logic_vector(31 downto 0);
  
    rs_index        : in  std_logic_vector(4 downto 0);
    rs_index_out    : out std_logic_vector(4 downto 0);

    rs2_index       : in  std_logic_vector(4 downto 0);
    rs2_index_out   : out std_logic_vector(4 downto 0);

    rd_index        : in  std_logic_vector(4 downto 0);
    rd_index_out    : out std_logic_vector(4 downto 0);

    c_mux           : in  reg_dst_source_type;
    c_mux_o         : out reg_dst_source_type;

    tag_check_src   : in  tag_check_type;
    tag_check_src_o : out tag_check_type;

    tag_mem_source  : in  tag_mem_source_type;
    tag_mem_source_o: out tag_mem_source_type;

    virtual_mem_src : in  virtual_mem_source_type;
    virtual_mem_src_o: out virtual_mem_source_type;

    mem_source      : in  mem_source_type;
    mem_source_o    : out mem_source_type;

    rf_en           : in  std_logic;
    rf_en_out       : out std_logic;

    rf_w_en         : in  std_logic;
    rf_w_en_out     : out std_logic;

    fp_rf_en        : in  std_logic;
    fp_rf_en_out    : out std_logic;

    fp_rf_w_en      : in  std_logic;
    fp_rf_w_en_out  : out std_logic;

    mmu_write_en    : in  std_logic;
    mmu_write_en_o  : out std_logic;

    conf_rf_wen     : in  std_logic;
    conf_rf_wen_o   : out std_logic;

    config_reg_index        : in  std_logic_vector(3 downto 0);
    config_reg_index_out    : out std_logic_vector(3 downto 0);

    tag_alu_func_i  : in  tag_alu_function_type;
    tag_alu_func    : out tag_alu_function_type );
end component;

component tag_alu is
   port(a_in         : in  std_logic_vector(31 downto 0);
        b_in         : in  std_logic_vector(31 downto 0);

        mem_access   : in std_logic;
        instrument   : in std_logic;
        instr_value  : in std_logic_vector(31 downto 0);
        tag_alu_p    : in  std_logic_vector(31 downto 0);
        tag_alu_p_p  : in  std_logic_vector(31 downto 0);
        forward_a    : in  std_logic_vector(1 downto 0);
        forward_b    : in  std_logic_vector(1 downto 0);

        tag_alu_func : in  tag_alu_function_type;
        c_alu        : out std_logic_vector(31 downto 0) );
end component;

component forward_unit is 
port (
  null_instruction: in std_logic;
  rs1_index_dec: in std_logic_vector(4 downto 0); -- dec stage
  rs1_index   : in std_logic_vector(4 downto 0); -- exec stage

  rs2_index_dec: in std_logic_vector(4 downto 0); -- dec stage
  rs2_index   : in std_logic_vector(4 downto 0); -- exec stage
  --rs2_index_mem: in std_logic_vector(5 downto 0); -- mem stage
  --rs2_index_wb : in std_logic_vector(5 downto 0); -- wb stage

  rd_index  : in std_logic_vector(4 downto 0); -- exec stage
  rd_index_wb : in std_logic_vector(4 downto 0); -- wb stage
  rd_index_dec: in std_logic_vector(4 downto 0); -- dec stage
  rd_index_mem: in std_logic_vector(4 downto 0); -- mem stage 

  tag_mem_source_ex   : in  tag_mem_source_type; -- exec stage
  --ldr_instr   : in std_logic; -- wb of load instruction 
  forward_a : out std_logic_vector(1 downto 0);
  forward_b : out std_logic_vector(1 downto 0)
  );
end component;

component pipeline_TE_TM is 
port (
  clk, reset      : in std_logic;
  stall           : in std_logic;  
  en_write_kblare : in  std_logic;
  en_write_kblare_o:out std_logic;
  write_pl2ps_addr: in std_logic_vector(31 downto 0);
  write_pl2ps_addr_o: out std_logic_vector(31 downto 0);
  rd_index        : in  std_logic_vector(4 downto 0);
  rd_index_out    : out std_logic_vector(4 downto 0);
  c_mux           : in  reg_dst_source_type;
  c_mux_o         : out reg_dst_source_type;
  reg_source_1    : in  std_logic_vector(31 downto 0);
  reg_source_1_out: out std_logic_vector(31 downto 0);
  reg_source_2    : in  std_logic_vector(31 downto 0);
  reg_source_2_out: out std_logic_vector(31 downto 0);
  reg_pc          : in  std_logic_vector(31 downto 0);
  reg_pc_out      : out std_logic_vector(31 downto 0);
  reg_instr       : in  std_logic_vector(31 downto 0);
  reg_instr_out   : out std_logic_vector(31 downto 0);
  tag_check_src   : in  tag_check_type;
  tag_check_src_o : out tag_check_type;
  tag_mem_source  : in  tag_mem_source_type;
  tag_mem_source_o: out tag_mem_source_type;
  mem_source      : in  mem_source_type;
  mem_source_o    : out mem_source_type;
  rf_en           : in  std_logic;
  rf_en_out       : out std_logic;

  rf_w_en         : in  std_logic;
  rf_w_en_out     : out std_logic;

  fp_rf_en        : in  std_logic;
  fp_rf_en_out    : out std_logic;

  fp_rf_w_en      : in  std_logic;
  fp_rf_w_en_out  : out std_logic;

  conf_rf_wen     : in  std_logic;
  conf_rf_wen_o   : out std_logic;

  config_reg_index        : in  std_logic_vector(3 downto 0);
  config_reg_index_out    : out std_logic_vector(3 downto 0);

  c_alu           : in  std_logic_vector(31 downto 0);
  c_alu_o         : out std_logic_vector(31 downto 0) );
end component;


component data_mem_ctrl is
   port(clk          : in std_logic;
        reset_in     : in std_logic;
 
       
        address_data : in std_logic_vector(31 downto 0);
        mem_source_2 : in mem_source_type;
        data_mem_address: in std_logic;
        data_write   : in std_logic_vector(31 downto 0);
        data_read    : out std_logic_vector(31 downto 0);
       
        pause_out    : out std_logic;
 
       
       
        mem_address  : out std_logic_vector(31 downto 0);
        mem_data_w   : out std_logic_vector(31 downto 0);
        mem_data_r   : in std_logic_vector(31 downto 0);
        tag_mem_data_r:in std_logic_vector(31 downto 0);
        mem_byte_sel : out std_logic_vector(3 downto 0);
        mem_write    : out std_logic;
        mem_read     : out std_logic
        );
end component;

COMPONENT cache_top is
  generic(n: integer := 32;
      m: integer := 26;
          TLB_ENTRIES: integer := 64;
      OFFSET: integer := 12);
  Port(clock, reset:in std_logic;
       address_in:in std_logic_vector(31 downto 0);
       data_out:out std_logic_vector(n-1 downto 0);
       enable_type: in tag_mem_source_type;

       r_valid_out:out std_logic;

       data_in:in std_logic_vector(n-1 downto 0); 

--	address_ram_store_out : out std_logic_vector(31 downto 0);
--	mdata_out_store_out: out std_logic_vector(31 downto 0);
--	state_out: out std_logic;

--	write_enable_out : out std_logic;
--	write_data_out : out std_logic_vector(31 downto 0);
--	address_from_MMU_out : out std_logic_vector(25 downto 0);

        axim_done	: in std_logic;
	axim_error_in: in std_logic;

	axim_address : out std_logic_vector(31 downto 0);
	axim_data_out : out std_logic_vector(31 downto 0);
	axim_data_in : in std_logic_vector(31 downto 0);
	axim_read_en : out std_logic;
	axim_write_en : out std_logic;

	axim_start	: out std_logic);

   --    memory_out: out BRAM_out_interface;
   --     memory_in: in BRAM_in_interface
   --     mdata_valid:in std_logic
	
end COMPONENT;

component pipeline_tm_twb is 
port (
    clk, reset      : in  std_logic;
    stall           : in  std_logic;
    en_write_kblare : in  std_logic;
    en_write_kblare_o:out std_logic;
    rd_index        : in  std_logic_vector(4 downto 0);
    rd_index_out    : out std_logic_vector(4 downto 0);
    c_mux           : in  reg_dst_source_type;
    c_mux_o         : out reg_dst_source_type;
    c_alu           : in  std_logic_vector(31 downto 0);
    c_alu_o         : out std_logic_vector(31 downto 0);
    c_mem           : in  std_logic_vector(31 downto 0);
    c_mem_o         : out std_logic_vector(31 downto 0);
    c_tag           : in  std_logic_vector(31 downto 0);
    c_tag_o         : out std_logic_vector(31 downto 0);
    c_instr         : in  std_logic_vector(31 downto 0);
    c_instr_o       : out std_logic_vector(31 downto 0);
    reg_source_1    : in  std_logic_vector(31 downto 0);
    reg_source_1_out: out std_logic_vector(31 downto 0);
    reg_source_2    : in  std_logic_vector(31 downto 0);
    reg_source_2_out: out std_logic_vector(31 downto 0);
    reg_pc          : in  std_logic_vector(31 downto 0);
    reg_pc_out      : out std_logic_vector(31 downto 0);
    reg_instr       : in  std_logic_vector(31 downto 0);
    reg_instr_out   : out std_logic_vector(31 downto 0);
    tag_check_src   : in  tag_check_type;
    tag_check_src_o : out tag_check_type;
    rf_en           : in  std_logic;
    rf_en_out       : out std_logic;

    rf_w_en         : in  std_logic;
    rf_w_en_out     : out std_logic;

    fp_rf_en        : in  std_logic;
    fp_rf_en_out    : out std_logic;

    conf_rf_wen     : in  std_logic;
    conf_rf_wen_o   : out std_logic;

    fp_rf_w_en      : in  std_logic;
    fp_rf_w_en_out  : out std_logic;

    config_reg_index        : in  std_logic_vector(3 downto 0);
    config_reg_index_out    : out std_logic_vector(3 downto 0)
  );
end component;

component tag_check is 
port(
    reg_source_1      : in  std_logic_vector(31 downto 0);
    reg_source_2      : in  std_logic_vector(31 downto 0);
    reg_dest          : in  std_logic_vector(31 downto 0);
    source_addr       : in  std_logic_vector(31 downto 0);
    dest_addr         : in  std_logic_vector(31 downto 0);
    tag_pc            : in  std_logic_vector(31 downto 0);
    tag_instruction   : in  std_logic_vector(31 downto 0);
    tag_check_src     : in  tag_check_type;        
    --tag_check_func    : in  tag_alu_check_function_type;
    stop_propagation  : out std_logic );
end component; --tag_check

    -- MMU
--    component MMU_IP
--        PORT (
--        clk : IN STD_LOGIC;
--        reset : IN STD_LOGIC;
--        enable : IN STD_LOGIC;
--        w_enable : IN STD_LOGIC;
--        address_in : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        address_out : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
--        busy : OUT STD_LOGIC;
--        out_en : OUT STD_LOGIC
--      );
--    end component;
    
    component io_controller is 
    port(
      mem_address_in    : in  std_logic_vector(31 downto 0);
      data_mem_cs     : out std_logic
      --configuration_mem_cs: out std_logic
    );
    end component;

    -- Configuration IP
--    component configuration_ip is 
--        port(
--        clk           : in  std_logic;
--        enable        : in  std_logic;
--        write_address : in  std_logic_vector(3-1 downto 0); -- 8 regs
----        read_address  : in  std_logic_vector(3-1 downto 0); -- 8 regs
--        write_value   : in  std_logic_vector(31 downto 0);
--        data_output   : out configuration_array
--        );
--    end component;

    -- AXI Master
  --  component AXI_master_0 is 
  --  PORT (
  --  go : IN STD_LOGIC;
  --  error : OUT STD_LOGIC;
  --  RNW : IN STD_LOGIC;
  --  busy : OUT STD_LOGIC;
  --  done : OUT STD_LOGIC;
  --  address : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
  --  write_data : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
  --  read_data : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
  --  burst_length : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
  --  burst_size : IN STD_LOGIC_VECTOR(6 DOWNTO 0);
  --  increment_burst : IN STD_LOGIC;
  --  clear_data_fifos : IN STD_LOGIC;
  --  write_fifo_en : IN STD_LOGIC;
  --  write_fifo_empty : OUT STD_LOGIC;
  --  write_fifo_full : OUT STD_LOGIC;
  --  read_fifo_en : IN STD_LOGIC;
  --  read_fifo_empty : OUT STD_LOGIC;
  --  read_fifo_full : OUT STD_LOGIC;
  --  m_axi_aclk : IN STD_LOGIC;
  --  m_axi_aresetn : IN STD_LOGIC;
  --  m_axi_arready : IN STD_LOGIC;
  --  m_axi_arvalid : OUT STD_LOGIC;
  --  m_axi_araddr : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
  --  m_axi_arid : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
  --  m_axi_arlen : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
  --  m_axi_arsize : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
  --  m_axi_arburst : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
  --  m_axi_arlock : OUT STD_LOGIC;
  --  m_axi_arcache : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
  --  m_axi_arprot : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
  --  m_axi_arqos : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
  --  m_axi_arregion : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
  --  m_axi_rready : OUT STD_LOGIC;
  --  m_axi_rvalid : IN STD_LOGIC;
  --  m_axi_rdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
  --  m_axi_rresp : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
  --  m_axi_rid : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
  --  m_axi_rlast : IN STD_LOGIC;
  --  m_axi_awready : IN STD_LOGIC;
  --  m_axi_awvalid : OUT STD_LOGIC;
  --  m_axi_awaddr : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
  --  m_axi_awid : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
  --  m_axi_awlen : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
  --  m_axi_awsize : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
  --  m_axi_awburst : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
  --  m_axi_awlock : OUT STD_LOGIC;
  --  m_axi_awcache : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
  --  m_axi_awprot : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
  --  m_axi_awqos : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
  --  m_axi_awregion : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
  --  m_axi_wready : IN STD_LOGIC;
  --  m_axi_wvalid : OUT STD_LOGIC;
  --  m_axi_wid : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
  --  m_axi_wdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
  --  m_axi_wstrb : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
  --  m_axi_wlast : OUT STD_LOGIC;
  --  m_axi_bready : OUT STD_LOGIC;
  --  m_axi_bvalid : IN STD_LOGIC;
  --  m_axi_bresp : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
  --  m_axi_bid : IN STD_LOGIC_VECTOR(3 DOWNTO 0)
  --);
  --END component;

  component configuration_reg_file 
  port (
  clk         : in  std_logic;
  w_en        : in  std_logic; -- for registers 0 - 15
  sel_d       : in  std_logic_vector(3 downto 0);
  data_in     : in  std_logic_vector(31 downto 0); 
  configuration_array_o : out configuration_array
  --data_in     : in  std_logic_vector(31 downto 0); 
  --data_out_1  : out std_logic_vector(31 downto 0);
  --data_out_2  : out std_logic_vector(31 downto 0);
  --data_out_3  : out std_logic_vector(31 downto 0);
  --data_out_4  : out std_logic_vector(31 downto 0);
  --data_out_5  : out std_logic_vector(31 downto 0);
  --data_out_6  : out std_logic_vector(31 downto 0);
  --data_out_7  : out std_logic_vector(31 downto 0);
  --data_out_8  : out std_logic_vector(31 downto 0);
  --data_out_9  : out std_logic_vector(31 downto 0);
  --data_out_10  : out std_logic_vector(31 downto 0);
  --data_out_11  : out std_logic_vector(31 downto 0);
  --data_out_12  : out std_logic_vector(31 downto 0);
  --data_out_13  : out std_logic_vector(31 downto 0);
  --data_out_14  : out std_logic_vector(31 downto 0);
  --data_out_15 : out std_logic_vector(31 downto 0);
  --data_out_16  : out std_logic_vector(31 downto 0)
   );
  end component;


   signal opcode  : std_logic_vector(31 downto 0);
   signal opcode_pause  : std_logic_vector(31 downto 0);
   signal rs1_index, rs2_index, rd_index              : std_logic_vector(4 downto 0);
   signal rs1_index_p, rs2_index_p                    : std_logic_vector(4 downto 0);
   signal rd_index_p, rd_index_p_p , rd_index_p_p_p   : std_logic_vector(4 downto 0);
   signal reg_source_1, reg_source_2, reg_dest        : std_logic_vector(31 downto 0);
   signal reg_source_1_gp, reg_source_2_gp            : std_logic_vector(31 downto 0);
   signal reg_source_1_p, reg_source_2_p              : std_logic_vector(31 downto 0);
   signal reg_source_1_p_p, reg_source_2_p_p          : std_logic_vector(31 downto 0);
   signal reg_source_1_p_p_p, reg_source_2_p_p_p      : std_logic_vector(31 downto 0);
   signal reg_source_1_fp, reg_source_2_fp            : std_logic_vector(31 downto 0);
   signal a_bus, b_bus, c_bus           : std_logic_vector(31 downto 0);
   signal a_bus_p, b_bus_p, reg_dest_p  : std_logic_vector(31 downto 0);
   signal c_alu, c_memory, c_memory_i, c_instrumentation
        : std_logic_vector(31 downto 0);
   signal c_tag : std_logic_vector(31 downto 0);
   signal c_tag_p : std_logic_vector(31 downto 0);
   signal c_alu_p, c_memory_p, c_instrumentation_p, kblare_write_data, c_kblare_p
        : std_logic_vector(31 downto 0);
   signal c_alu_p_p, c_memory_p_p
        : std_logic_vector(31 downto 0);
   signal imm            : std_logic_vector(15 downto 0);
   signal imm_p, imm_p_p,
          imm_p_p_p      : std_logic_vector(15 downto 0);
   signal pc, pc_new     : std_logic_vector(31 downto 0);
   signal pc_p, pc_p_p,
          pc_p_p_p       : std_logic_vector(31 downto 0);
   
   signal tag_alu_function_opcode   : tag_alu_function_type;
   signal tag_alu_function_sec_pol  : tag_alu_function_type;
   signal tag_alu_function          : tag_alu_function_type;
   signal tag_alu_function_p        : tag_alu_function_type;
  
   signal a_source                  : reg_src1_source_type;
   signal b_source                  : reg_src2_source_type;
   signal c_source                  : reg_dst_source_type;
   signal c_source_p                : reg_dst_source_type;
   signal c_source_p_p              : reg_dst_source_type;
   signal c_source_p_p_p            : reg_dst_source_type;
   signal tag_mem_source            : tag_mem_source_type;
   signal tag_mem_source_p          : tag_mem_source_type;
   signal tag_mem_source_p_p        : tag_mem_source_type;
   signal mem_source                : mem_source_type;
   signal mem_source_p              : mem_source_type;
   signal mem_source_p_p            : mem_source_type;
   signal pause_mult                : std_logic;
   signal pause_memory              : std_logic;
   signal i_pause_memory            : std_logic;
   signal i_mem_read_v              : std_logic := '1';
   signal pause                     : std_logic;
   signal pause_reg                 : std_logic;
   signal pause_hazard              : std_logic := '0';
   signal hazard                    : std_logic := '0';
   signal nullify_op                : std_logic;
   signal intr_enable               : std_logic;
   signal intr_signal               : std_logic;
   signal forward_a                 : std_logic_vector(1 downto 0);
   signal forward_b                 : std_logic_vector(1 downto 0);
   signal arm_opcode_class          : arm_opcode_type;
   signal tag_alu_function_source   : tag_alu_function_source_type;
   signal tag_size                  : tag_size_policies;
   signal enable_security_policies  : std_logic_vector(nb_sec_policy-1 downto 0) := (others => '0');
   signal tag_prop_sources          : tag_prop_type;
   signal tag_check_sources         : tag_check_type; -- decode stage
   signal tag_check_sources_p       : tag_check_type; -- ex stage
   signal tag_check_sources_p_p     : tag_check_type; -- mem stage
   signal tag_check_sources_p_p_p   : tag_check_type; -- wb stage
   signal tag_check_function        : tag_alu_check_function_type; 
   signal enable_reg_file           : std_logic;
   signal enable_reg_file_2         : std_logic;
   signal enable_reg_file_s         : std_logic; -- dec stage
   signal enable_reg_file_p         : std_logic; -- ex stage
   signal enable_reg_file_p_p       : std_logic; -- mem stage 
   signal enable_reg_file_p_p_p     : std_logic; -- wb stage
   signal write_enable_reg_file     : std_logic;
   signal write_enable_reg_file_s   : std_logic; -- decode stage
   signal write_enable_reg_file_p   : std_logic; -- ex stage 
   signal write_enable_reg_file_p_p : std_logic; -- mem stage 
   signal write_enable_reg_file_p_p_p: std_logic; -- wb stage
   signal enable_reg_file_fp        : std_logic;
   signal enable_reg_file_fp_2      : std_logic;
   signal enable_reg_file_fp_s      : std_logic; -- decode stage
   signal enable_reg_file_fp_p      : std_logic; -- ex stage
   signal enable_reg_file_fp_p_p    : std_logic; -- mem stage
   signal enable_reg_file_fp_p_p_p  : std_logic; -- wb stage
   signal write_enable_reg_file_fp  : std_logic; 
   signal write_enable_reg_file_fp_s: std_logic; -- decode stage
   signal write_enable_reg_file_fp_p: std_logic; -- ex stage
   signal write_enable_reg_file_fp_p_p: std_logic; -- mem stage
   signal write_enable_reg_file_fp_p_p_p: std_logic; -- wb stage
   signal en_read_instrumentation   : std_logic;
   signal en_read_kblare            : std_logic;
   signal en_write_kblare           : std_logic;
   signal en_write_kblare_p         : std_logic; -- Ex
   signal en_write_kblare_p_p       : std_logic; -- Mem 
   signal en_write_kblare_p_p_p     : std_logic; -- WB
   signal tag_arm_pc_reg            : std_logic_vector(31 downto 0);
   signal tag_arm_pc_reg_p          : std_logic_vector(31 downto 0);
   signal tag_arm_pc_reg_p_p        : std_logic_vector(31 downto 0);
   signal tag_arm_pc_reg_p_p_p      : std_logic_vector(31 downto 0);
   signal tag_current_instruction   : std_logic_vector(31 downto 0);
   signal tag_current_instruction_p : std_logic_vector(31 downto 0);
   signal tag_current_instruction_p_p: std_logic_vector(31 downto 0);
   signal tag_current_instruction_p_p_p: std_logic_vector(31 downto 0);   


   signal data_mem_address          : std_logic;
   --signal axim                      : custom_axi_master_out_interface;
   --signal axim_in                   : custom_axi_master_in_interface;
 --  signal tag_mem_out               : BRAM_out_interface;
 --  signal tag_mem_in                : BRAM_in_interface;
   signal instrument                : std_logic := '0';
   --signal configuration_array_rf    : configuration_array := (others => (others => '0'));
   signal mem_access                : std_logic;
   signal mem_access_p              : std_logic;
--   signal mmu_en                    : std_logic;
   signal mmu_busy                  : std_logic;
   signal mmu_out_en                : std_logic;
   signal physical_mem_address      : std_logic_vector(31 downto 0); -- 0x1
   signal pl2ps_address             : std_logic_vector(31 downto 0);
   signal pl2ps_address_p           : std_logic_vector(31 downto 0);
   signal pl2ps_address_p_p         : std_logic_vector(31 downto 0);
   signal virtual_mem_address       : std_logic_vector(31 downto 0);
  -- signal virtual_mem_address_reg      : std_logic_vector(31 downto 0);
   signal virtual_mem_select        : virtual_mem_source_type;
   signal virtual_mem_select_p      : virtual_mem_source_type;
   --signal instrumentation_value : std_logic_vector(31 downto 0); 
   signal enable_write_config_reg_file : std_logic;
   signal config_reg_index             : std_logic_vector(4 downto 0);
   signal config_reg_wen               : std_logic;
   signal config_reg_wen_p             : std_logic;
   signal config_reg_wen_p_p           : std_logic;
   signal config_reg_wen_p_p_p         : std_logic;
   signal config_reg_index_p           : std_logic_vector(3 downto 0);
   signal config_reg_index_p_p         : std_logic_vector(3 downto 0);
   signal config_reg_index_p_p_p       : std_logic_vector(3 downto 0);

--   signal config_reg_write_addr : std_logic_vector(nb_regs-1 downto 0);
--   signal config_reg_write_value : std_logic_vector(31 downto 0);   
--   signal register_read_addr : std_logic_vector(nb_regs-1 downto 0);
   --signal register_read_en : std_logic; - '1' always so removed.
   signal configuration_arr : configuration_array := (others => (others => '0'));
   signal mmu_write_en : std_logic;
   signal mmu_write_en_p : std_logic;
   signal mmu_write_address_in : std_logic_vector(31 downto 0) := (others => '0');
   signal axim_in_error : std_logic;
   signal axim_in_busy : std_logic;
   signal axim_in_done : std_logic;
   signal axim_in_read_data : std_logic_vector(31 downto 0);
   signal axim_in_write_fifo_empty : std_logic;
   signal axim_in_write_fifo_full : std_logic;
   signal axim_in_read_fifo_empty : std_logic;
   signal axim_in_read_fifo_full : std_logic;
   
   signal stall_f     : std_logic := '0'; -- stall fetch stage
   signal stall_d     : std_logic := '0'; -- stall decode stage
   signal stall_dec     : std_logic := '0'; -- stall decode stage
   signal stall_d_temp: std_logic := '0';
   signal stall_ex    : std_logic := '0'; -- stall execute stage
   signal stall_mem   : std_logic := '0'; -- stall mem stage
   signal stall_s       : std_logic := '0';
   signal stall_wb      : std_logic := '0'; -- stall wb stage
   signal stall_ex_mem  : std_logic := '0'; -- stall ex/mem stage
   signal stall_mem_wb  : std_logic := '0'; -- stall mem/wb stage

   signal en_read_instrumentation_p   : std_logic;
   signal en_read_kblare_ps2pl_p      : std_logic;
   signal mem_read : std_logic;
   signal mem_write : std_logic;
   signal data_mem_cs : std_logic;
   signal config_mem_cs : std_logic;
   signal instrumentation_enable_s : std_logic;
   signal kblare_ps2pl_enable_s : std_logic;
   signal mem_address_s : std_logic_vector(31 downto 0);
   signal pc_enable_s   : std_logic;

   signal i_mem_data_r_reg : std_logic_vector(31 downto 0);
   signal i_mem_data_r_reg_stall : std_logic_vector(31 downto 0);

  --signal kblare_ps2pl_clk       : std_logic := '0';
  --signal kblare_ps2pl_reset     : std_logic;
  signal kblare_ps2pl_address_s   : std_logic_vector(31 downto 0) := (others => '0');
  --signal kblare_ps2pl_data_r    : std_logic_vector(31 downto 0);
  --signal kblare_ps2pl_sel       : std_logic_vector(3 downto 0);
  --signal kblare_ps2pl_en        : std_logic;
  signal kblare_ps2pl_enable      : std_logic;
  signal kblare_ps2pl_enable_reg  : std_logic;

  --signal kblare_pl2ps_clk       : std_logic := '0';
  --signal kblare_pl2ps_reset     : std_logic;
  signal kblare_pl2ps_address_s   : std_logic_vector(31 downto 0) := (others => '0');
  --signal kblare_pl2ps_data_w    : std_logic_vector(31 downto 0);
  --signal kblare_pl2ps_data_r    : std_logic_vector(31 downto 0);
  --signal kblare_pl2ps_sel       : std_logic_vector(3 downto 0);
  --signal kblare_pl2ps_en        : std_logic;
  signal kblare_pl2ps_enable      : std_logic;
  signal kblare_pl2ps_enable_reg  : std_logic;

  signal kblare_ps2pl_empty : std_logic := '0';
  signal kblare_pl2ps_full : std_logic := '0';

  signal null_instruction : std_logic;

   signal clock, reset, r_valid_out, mdata_valid_sig: std_logic;

TYPE STALL_STATE_TYPE IS (INITIAL, STALL_IT);
   SIGNAL stall_state   : STALL_STATE_TYPE := INITIAL;

begin  --architecture
    
--    config_reg_write_addr <= configuration_arr(0)(31 downto (31-nb_regs+1));
--    config_reg_write_value <= configuration_arr(1);

	enable_type_out <= tag_mem_source_p;
--	data_read_out   <= c_tag;
	data_from_TMC_out <= reg_source_2_p;
	virtual_mem_address_out <= virtual_mem_address;
--	virtual_mem_select_p_out <= virtual_mem_select_p;
--	c_alu_out <= c_alu;
	pause_out <= pause;
--	a_bus_p_out <= a_bus_p;
	b_bus_p_out <= b_bus_p;
--	c_alu_p_out <= c_alu_p;
--	c_alu_p_p_out <= c_alu_p_p;
--	a_source_out <= a_source;
--	enable_reg_file_s_out <= enable_reg_file_s;
	reg_source_1_gp_out <= reg_source_1_gp;

   mmu_write_address_in <= c_alu;
   kblare_ps2pl_enable <= kblare_ps2pl_enable_s;
   instrumentation_enable <= instrumentation_enable_s;
   i_mem_clk <= clk;
   mem_clk <= clk;
   i_mem_reset <= reset_in;
   mem_reset <= reset_in;
   config_mem_clk <= clk;
   config_mem_reset <= reset_in;
   i_mem_en <= pc_enable;
   i_mem_sel <= "1111";
   enable_security_policies <= "0001"; 
   tag_size(0) <= tag_size_word;
   tag_size(1) <= tag_size_word;
   tag_size(2) <= tag_size_word;
   tag_size(3) <= tag_size_word;
   --with (pause) select pc_enable_s <= 
                                  --'1' when others;
                                  --'0' when '1',
    --pc_enable_s <= not pause xor pc_enable;
    pc_enable_s <= pc_enable;
   --pc_enable_s <= '0' when reset_in = '1', 
   --               '1' when pause = '1',
   --                else  '0';

   --enable_reg_file <= not reset_in and not enable_reg_file_fp;
   --enable_reg_file_fp <= ;
   enable_reg_file          <= enable_reg_file_s; -- or write_enable_reg_file_p_p_p;
   enable_reg_file_fp       <= enable_reg_file_fp_s; -- or write_enable_reg_file_fp_p_p_p;
   write_enable_reg_file    <= write_enable_reg_file_p_p_p;
   write_enable_reg_file_fp <= write_enable_reg_file_fp_p_p_p;
   enable_write_config_reg_file <= config_reg_wen_p_p_p;
   --pause <= '0';
   --stall_s <= '0';
   --pause <= pause_memory;
   --stall_s <= pause_memory;
   --nulify_op = pc_source==from_lbranch && take_branch=='0'
   --nullify_op <= pc_source_p(1) and pc_source_p(0);
   c_bus <= c_alu_p_p;
   intr_signal <= (intr_in and intr_enable) ; -- and 
--                  (not pc_source_p(0) and not pc_source_p(1));  --from_inc4
    --pause <= stall_d;

  process(reset_in, stall_d, mmu_out_en, clk)
  begin
    if rising_edge(clk) then
        CASE stall_state IS
        WHEN INITIAL=>
				pause <= '0';
				if (stall_d = '1' and mmu_out_en = '0') then
					stall_state <= STALL_IT;
					opcode_pause <= opcode;
					pause <= '1';
				end if;
		when STALL_IT =>
			--	pause <= '1';	
				if (mmu_out_en = '1') then
					stall_state <= INITIAL;
					pause <= '0';
				end if;
        when others =>
                stall_state <= INITIAL;
        END CASE;
      
   end if;
  end process;

   u1: pc_next PORT MAP (
        clk          => clk,
        reset_in     => reset_in,
        enable       => pc_enable_s,
        pause_in     => pause,
        pc_new       => pc_new(31 downto 2), -- pc_new(31 downto 2) or c_alu                
        pc_out       => pc );
    
    pc_new(1 downto 0) <= "00";
   --u2: mem_ctrl PORT MAP (
   --     clk          => clk,
   --     reset_in     => reset_in,
   --     pause_in     => pause,
   --     hazard       => pause_hazard,
   --     nullify_op   => nullify_op,
   --     address_pc   => pc,
   --     opcode_out   => opcode,
                
   --     data_read    => c_memory_i,
   --     pause_out    => i_pause_memory,
        
   --     mem_address  => i_mem_address,
   --     mem_data_r   => i_mem_data_r,
   --     mem_read_v   => i_mem_read_v,
   --     mem_pause    => i_mem_pause );

  --process(reset_in, pause_in, i_mem_data_r)
  process(clk)
  begin
    if rising_edge(clk) then 
        if reset_in = '1' then 
            i_mem_data_r_reg <= (others => '0');
            i_mem_data_r_reg_stall <= (others => '0');
            --stall_d <= '0';
        else
            i_mem_data_r_reg <= i_mem_data_r;
            i_mem_data_r_reg_stall <= i_mem_data_r_reg;
            --stall_d <= stall_d_temp;
        end if;
    end if;
  end process;

  --process(reset_in,mmu_out_en,stall_d_temp)
  --begin
  --  if reset_in = '1' or (mmu_out_en = '1' and stall_d_temp = '1') then 
  --    stall_d <= '0';
  --  else 
  --    stall_d <= stall_d_temp;
  --  end if;
  --end process;

  stall_d <= stall_d_temp;

  process(clk, reset_in, pause, i_mem_data_r, i_mem_data_r_reg)
  --process(clk)
  begin
    if rising_edge(clk) then
       if reset_in = '1' then 
          opcode <= ZERO;
       elsif (pause = '0' and stall_d = '1') then  
          opcode <= i_mem_data_r_reg_stall;
       elsif (pause = '1') then  
          opcode <= opcode_pause;
       else 
          opcode <= i_mem_data_r_reg;
        end if;
    end if;
  end process;

  i_mem_address <= pc;

  process(clk)
  begin
    if rising_edge(clk) then
    --    opcode_pause <= opcode;
        if pause = '1' then 
            pause_reg <= '1'; 
        else
            pause_reg <= '0'; 
        end if;
    end if;
  end process;
        

   u3: control_dependencies PORT MAP (
        clk                     => clk,
        reset                   => reset_in,
        opcode                  => opcode,
        tag_prop_sources        => tag_prop_sources,
        en_read_instrumentation => instrumentation_enable_s,
        --instrumentation_address_r=>instrumentation_address,
        en_read_kblare_ps2pl    => kblare_ps2pl_enable_s,
        --kblare_ps2pl_read_address_r=>kblare_ps2pl_address,
        reg_dst                 => rd_index,
        reg_src1                => rs1_index,
        reg_src2                => rs2_index,
        config_reg_index        => config_reg_index,
        tag_alu_func            => tag_alu_function_opcode,
        arm_opcode_class        => arm_opcode_class,
        immediate               => imm,
        --instrumentation_data    => c_instrumentation,
        kblare_data_o           => kblare_write_data,
        en_write_kblare_o       => en_write_kblare,
        kblare_pl2ps_write_address_r=>pl2ps_address,
        virtual_mem_source      => virtual_mem_select,
        --en_write_kblare_i       => en_write_kblare_p_p,
        --kblare_data_i           => reg_source_1, -- pipelined...
        reg_dst_source          => c_source,
        reg_src1_source         => a_source,
        reg_src2_source         => b_source,
        --instrument              => instrument,
        rf_en                   => enable_reg_file_s,
        rf_2_en                 => enable_reg_file_2,
        rf_w_en                 => write_enable_reg_file_s,
        fp_rf_en                => enable_reg_file_fp_s,
        fp_rf_2_en              => enable_reg_file_fp_2,
        fp_rf_w_en              => write_enable_reg_file_fp_s,
        config_reg_file_w_en    => config_reg_wen,
        stall                   => stall_d_temp,
        mem_source_o            => mem_source,
        mmu_write_en            => mmu_write_en,
        data_mem_address        => data_mem_address,
        tag_mem_source_o        => tag_mem_source,
        null_instruction        => null_instruction,
        tag_alu_function_source => tag_alu_function_source );

   --initial_config_sec_policy_registers <= configuration_arr(0 to (2*nb_sec_policy)-1);

   u3b: registers_sec_policy port map (
        clk            => clk, 
        enable_sec_pol => enable_security_policies, 
        config_regs    => configuration_arr,
        arm_opcode_t   => arm_opcode_class,
        tag_size       => tag_size, 
        tag_alu_func   => tag_alu_function_sec_pol, 
        tag_prop_en    => tag_prop_sources,
        tag_check_en   => tag_check_sources,
        tag_alu_check  => tag_check_function );

   u4: reg_file port map (
        clk            => clk,
        enable         => enable_reg_file,
        enable_2       => enable_reg_file_2,
        w_en           => write_enable_reg_file,
        data_in        => reg_dest,
        data_out_a     => reg_source_1_gp,
        data_out_b     => reg_source_2_gp,
        data_out_c     => tag_arm_pc_reg,
        sel_a          => rs1_index,
        sel_b          => rs2_index,
        sel_d          => rd_index_p_p_p);

   u4b: reg_file port map (
        clk            => clk,
        enable         => enable_reg_file_fp,
        enable_2       => enable_reg_file_fp_2,
        w_en           => write_enable_reg_file_fp,
        data_in        => reg_dest,
        data_out_a     => reg_source_1_fp,
        data_out_b     => reg_source_2_fp,
        data_out_c     => open,
        sel_a          => rs1_index,
        sel_b          => rs2_index,
        sel_d          => rd_index_p_p_p);

   u4c: configuration_reg_file port map(
        clk         => clk,
        w_en        => enable_write_config_reg_file,
        sel_d       => config_reg_index_p_p_p, -- Currently 16 registers 
        data_in     => reg_dest,
        configuration_array_o => configuration_arr
    );

   u5: dep_bus_mux port map (
        reset              => reset_in,
        imm_in             => imm,
        en_rf              => enable_reg_file_s,
        en_rf_fp           => enable_reg_file_fp_s,
        en_rf_2            => enable_reg_file_2,
        en_rf_2_fp         => enable_reg_file_fp_2,
        reg_source_gp      => reg_source_1_gp,
        reg_source_fp      => reg_source_1_fp,
        reg_source         => reg_source_1,
                       
        a_mux              => a_source,
        a_out              => a_bus,
       
        reg_source_gp_2    => reg_source_2_gp,
        reg_source_fp_2    => reg_source_2_fp,
        reg_source_2       => reg_source_2,
        b_mux              => b_source,
        b_out              => b_bus,
        
        tag_prop_sources   => tag_prop_sources,
        tag_check_sources  => tag_check_sources,
       
        enable_sec_policies   => enable_security_policies,
        tag_alu_function_src  => tag_alu_function_source,
        tag_alu_function_op   => tag_alu_function_opcode,
        tag_alu_function_sec  => tag_alu_function_sec_pol,
        tag_alu_function_o    => tag_alu_function,

        c_bus              => c_bus,
        c_memory           => c_memory_p,
        c_tag              => c_tag_p,
        --c_instrumentation  => c_instrumentation_p,
        --c_kblare           => c_kblare_p,
        c_mux              => c_source_p_p_p,
        reg_dest_out       => reg_dest );
   
--   u5a: configuration_ip PORT MAP(
--        clk => clk,
--        enable => config_reg_write_enable,
--        write_address => config_write_addr,
--        write_value => config_write_value,
--        data_output => configuration_arr
--      );   
    --qn <= configuration_arr(1);
   
   u5b: pipeline_TD_TE port map(
        clk            => clk, 
        reset          => reset_in,
        pause          => stall_dec, -- pause pipeline
        stall          => stall_dec, -- stall pipeline
        a_in           => a_bus,
        a_out          => a_bus_p,
       
        b_in           => b_bus,
        b_out          => b_bus_p,

        reg_source_1    => reg_source_1,
        reg_source_1_out=> reg_source_1_p,

        reg_source_2    => reg_source_2,
        reg_source_2_out=> reg_source_2_p,

        en_read_instrumentation => instrumentation_enable_s,
        en_read_instrumentation_o => en_read_instrumentation_p,

        en_read_kblare_ps2pl => kblare_ps2pl_enable_s,
        en_read_kblare_ps2pl_o => en_read_kblare_ps2pl_p,

        en_write_kblare   => en_write_kblare,
        en_write_kblare_o => en_write_kblare_p,

        write_pl2ps_addr => pl2ps_address,
        write_pl2ps_addr_o => pl2ps_address_p,

        rs_index       => rs1_index,
        rs_index_out   => rs1_index_p,

        rs2_index      => rs2_index,
        rs2_index_out  => rs2_index_p,

        rd_index       => rd_index,
        rd_index_out   => rd_index_p,

        imm_in         => imm,
        imm_out        => imm_p,

        c_mux          => c_source,
        c_mux_o        => c_source_p,

        reg_pc         => tag_arm_pc_reg,
        reg_pc_out     => tag_arm_pc_reg_p,

        reg_instr      => tag_current_instruction,
        reg_instr_out  => tag_current_instruction_p,

        tag_check_src  => tag_check_sources,
        tag_check_src_o=> tag_check_sources_p,

        tag_mem_source  => tag_mem_source,
        tag_mem_source_o=> tag_mem_source_p,

        virtual_mem_src => virtual_mem_select,
        virtual_mem_src_o=>virtual_mem_select_p,

        mem_source      => mem_source,
        mem_source_o    => mem_source_p,

        rf_en           => enable_reg_file_s,
        rf_en_out       => enable_reg_file_p,

        rf_w_en         => write_enable_reg_file_s,
        rf_w_en_out     => write_enable_reg_file_p,

        fp_rf_en        => enable_reg_file_fp_s,
        fp_rf_en_out    => enable_reg_file_fp_p,

        fp_rf_w_en      => write_enable_reg_file_fp_s,
        fp_rf_w_en_out  => write_enable_reg_file_fp_p,

        mmu_write_en    => mmu_write_en,
        mmu_write_en_o  => mmu_write_en_p,

        conf_rf_wen     => config_reg_wen,
        conf_rf_wen_o   => config_reg_wen_p,

        config_reg_index     => config_reg_index(3 downto 0),
        config_reg_index_out => config_reg_index_p,

        tag_alu_func_i => tag_alu_function,
        tag_alu_func   => tag_alu_function_p);

   u6: tag_alu port map (
        a_in          => a_bus_p,
        b_in          => b_bus_p,
  
        mem_access    => mem_access,
        instrument    => instrument,
        instr_value   => c_instrumentation, -- instrumentation_value
        tag_alu_p     => c_alu_p,
        tag_alu_p_p   => c_alu_p_p,
        forward_a     => forward_a,
        forward_b     => forward_b,
  
        tag_alu_func  => tag_alu_function_p, -- tag_alu_function_p
        c_alu         => c_alu);

   u6b: forward_unit port map (
        null_instruction => null_instruction,
        rs1_index_dec   => rs1_index,
        rs1_index       => rs1_index_p,

        rs2_index_dec   => rs2_index,
        rs2_index       => rs2_index_p,
        --rs2_index_mem   => ,
        --rs2_index_wb    => open,

        rd_index        => rd_index_p,
        rd_index_wb     => rd_index_p_p_p,
        rd_index_dec    => rd_index,
        rd_index_mem    => rd_index_p_p,

        tag_mem_source_ex => tag_mem_source_p, 
        --ldr_instr       => open,
        forward_a       => forward_a,
        forward_b       => forward_b);

    with tag_mem_source_p select mem_access <=
        '1' when tag_mem_read32, 
        '1' when tag_mem_write32,
        '1' when tag_mem_tlb_write,
        '0' when others;
    
    --virtual_mem_select <= "01" when 
    with virtual_mem_select_p select virtual_mem_address <= 
      c_alu when virtual_mem_adder,
      instrumentation_data_r when virtual_mem_instrumentation,
      kblare_ps2pl_data_r when virtual_mem_blare,
      ZERO when others;

--    process(clk)
--    begin
--      if (rising_edge(clk)) then
--        virtual_mem_address_reg <= virtual_mem_address;
--      end if;
--    end process;
    
    
    process(kblare_ps2pl_empty, en_read_kblare_ps2pl_p, instrumentation_empty, en_read_instrumentation_p,
            kblare_pl2ps_full, en_write_kblare_p)
    begin
        if en_read_kblare_ps2pl_p = '1' and kblare_ps2pl_empty = '1' then
          stall_ex <= '1';
        elsif en_read_instrumentation_p = '1' and instrumentation_empty = '1' then
          stall_ex <= '1';
        elsif en_write_kblare_p = '1' and kblare_pl2ps_full = '1' then
          stall_ex <= '1';
        else 
          stall_ex <= '0';
        end if;
    end process;

    process(clk)
    begin
      if rising_edge(clk) then 
        mem_access_p <= mem_access;
      end if;
    end process;
--  mmu_en <= mem_access or mem_access_p;

--   u6c: MMU_IP PORT MAP (
--       clk => clk,
--       reset => reset_in,
--       enable => mmu_en, -- ex stage
--       w_enable => mmu_write_en_p, -- ex stage
--       address_in => virtual_mem_address, -- virtual address of the program 
--       address_out => physical_mem_address, -- physical mem address where tag is located
--       busy => mmu_busy, -- mmu_busy
--       out_en => mmu_out_en
--     );

  r_valid_out <= r_valid_out;

  -- if mmu is busy stall the previous pipeline stages 
  --process(r_valid_out)
  --begin
  --  if r_valid_out = '0' then 
  --    stall_ex <= '1';
  --  else
  --    stall_ex <= '0';
  --  end if;
  --end process;

  stall_ex_mem <= stall_d_temp or stall_ex;

    u8b: pipeline_te_tm port map(
       clk              => clk,
       reset            => reset_in,
       stall            => stall_ex_mem,
       en_write_kblare  => en_write_kblare_p,
       en_write_kblare_o=> en_write_kblare_p_p,
       write_pl2ps_addr => pl2ps_address_p,
       write_pl2ps_addr_o => pl2ps_address_p_p,
       rd_index         => rd_index_p, 
       rd_index_out     => rd_index_p_p,
       c_mux            => c_source_p,
       c_mux_o          => c_source_p_p,
       reg_source_1   => reg_source_1_p,
       reg_source_1_out=> reg_source_1_p_p,

       reg_source_2   => reg_source_2_p,
       reg_source_2_out=> reg_source_2_p_p,

       reg_pc         => tag_arm_pc_reg_p,
       reg_pc_out     => tag_arm_pc_reg_p_p,

       reg_instr      => tag_current_instruction_p,
       reg_instr_out  => tag_current_instruction_p_p,

       tag_check_src  => tag_check_sources_p,
       tag_check_src_o=> tag_check_sources_p_p,

       tag_mem_source  => tag_mem_source_p,
       tag_mem_source_o=> tag_mem_source_p_p,

       mem_source      => mem_source_p,
       mem_source_o    => mem_source_p_p,

       rf_en           => enable_reg_file_p,
       rf_en_out       => enable_reg_file_p_p,

       rf_w_en         => write_enable_reg_file_p,
       rf_w_en_out     => write_enable_reg_file_p_p,

       fp_rf_en        => enable_reg_file_fp_p,
       fp_rf_en_out    => enable_reg_file_fp_p_p,

       fp_rf_w_en      => write_enable_reg_file_fp_p,
       fp_rf_w_en_out  => write_enable_reg_file_fp_p_p,

       conf_rf_wen     => config_reg_wen_p,
       conf_rf_wen_o   => config_reg_wen_p_p,

       config_reg_index     => config_reg_index_p,
       config_reg_index_out => config_reg_index_p_p,

       c_alu            => c_alu,   
       c_alu_o          => c_alu_p);

   u9: data_mem_ctrl PORT MAP (
       clk            => clk,
       reset_in       => reset_in,
       
       address_data   => c_alu_p,
       
       mem_source_2   => mem_source_p_p,
       data_mem_address=>data_mem_address,
       data_write     => reg_source_2_p_p, -- @TODO: FIXME ...reg_dest --reg_source_2_p_p
       data_read      => c_memory,
       
       pause_out      => pause_memory,
       
       
       mem_address    => mem_address_s,
       mem_data_w     => mem_data_w,
       mem_data_r     => mem_data_r,
       tag_mem_data_r => axim_data_in, 
       mem_byte_sel   => mem_sel,
       mem_write      => mem_write,
       mem_read       => mem_read
       );

      --process(tag_mem_source_p_p, axim_i_done, axim_i_busy)
      --begin 
      --  if tag_mem_source_p_p /= tag_mem_none and axim_i_busy = '1' then 
      --    stall_mem <= '1';
      --  else --if axim_i_done = '1' then 
      --    stall_mem <= '0';
      --  end if;
      --end process;
      stall_mem <= '0';

    process(clk)
    begin
    if (rising_edge(clk)) then
            if(axim_done = '1') then
                mdata_valid_sig <= '1';
            else
                mdata_valid_sig <= '0';
            end if;
    end if;
    end process;


    --mmu_out_en <= '0' after 0 ns, 
    --              '1' after 1800 ns, 
    --              '0' after 1820 ns;
    
    u9a:cache_top generic map (
        n => 32,
        m => 26,
        TLB_entries => 64,
        OFFSET => 12
    )
    Port map(
        clk, 
        reset_in, 
        virtual_mem_address,
        c_tag,
        tag_mem_source_p, --tag_mem_source_p_p
        --    r_enable:in std_logic;
        mmu_out_en,

        reg_source_2_p, --reg_source_2_p_p

--	address_ram_store_out,
--	mdata_out_store_out,
--	state_out,

--	write_enable_out,
--	write_data_out,
--	address_from_MMU_out,

	axim_done,
	axim_error_in,

	axim_address,
	axim_data_out,
	axim_data_in,
	axim_read_en,
	axim_write_en,

	axim_start   
        --    w_enable:in std_logic;
        --    TLB_update_enable:in std_logic
      --  memory_out => tag_mem_out,
      --  mdata_valid => mdata_valid_sig,
      --  memory_in => tag_mem_in
    );

 --   with tag_mem_out.w_en select 
 --       tag_write <= '1' when "1111",
 --                    '0' when others;
    
    u9b: io_controller port map(
      mem_address_in => mem_address_s,
      data_mem_cs    => data_mem_cs
      --configuration_mem_cs => open
      );

    mem_en <= (mem_read or mem_write) and data_mem_cs;
    config_mem_cs <= config_reg_wen_p_p;
    config_mem_en <= (mem_read or mem_write) and config_mem_cs;
    mem_address <= mem_address_s;
    config_mem_address <= mem_address_s;

      --axim_o_go                 <= axim.go          ;
      --axim_o_RNW                <= axim.RNW         ;
      --axim_o_address            <= axim.address       ;
      --axim_o_increment_burst    <= axim.increment_burst;
      --axim_o_clear_data_fifos   <= axim.clear_data_fifos ;
      --axim_o_burst_length       <= axim.burst_length    ;
      --axim_o_burst_size         <= axim.burst_size      ;
      --axim_o_read_fifo_en       <= axim.read_fifo_en    ;
      --axim_o_write_fifo_en      <= axim.write_fifo_en     ;
      --axim_o_write_data         <= axim.write_data      ;

      --axim_in.error             <= axim_i_error           ;
      --axim_in.busy              <= axim_i_busy            ;
      --axim_in.done              <= axim_i_done            ;
      --axim_in.read_data         <= axim_i_read_data       ;
      --axim_in.write_fifo_empty  <= axim_i_write_fifo_empty;
      --axim_in.write_fifo_full   <= axim_i_write_fifo_full ;
      --axim_in.read_fifo_empty   <= axim_i_read_fifo_empty ;

     --   tag_clk       <= clk;
     --   tag_rst       <= reset_in;
     --   tag_en        <= tag_mem_out.en;
     --   tag_w_en      <= tag_mem_out.w_en;
     --   tag_address_o <= tag_mem_out.address_o;
     --   tag_data_out  <= tag_mem_out.data_out;
        --tag_rd_data   : std_logic_vector(31 downto 0);


      --axim_in.read_fifo_full    <= axim_i_read_fifo_full  ;

  -- u9b: AXI_master_0 port map(
  --  go => axim.go, 
  --  error => axim_in_error,
  --  RNW => axim.RNW, 
  --  busy => axim_in_busy,
  --  done => axim_in_done,
  --  address => axim.address, 
  --  write_data => axim.write_data, 
  --  read_data => axim_in_read_data,
  --  burst_length => axim.burst_length, 
  --  burst_size => axim.burst_size, 
  --  increment_burst => axim.increment_burst, 
  --  clear_data_fifos => axim.clear_data_fifos, 
  --  write_fifo_en => axim.write_fifo_en, 
  --  write_fifo_empty => axim_in_write_fifo_empty,
  --  write_fifo_full => axim_in_write_fifo_full,
  --  read_fifo_en => axim.read_fifo_en, 
  --  read_fifo_empty => axim_in_read_fifo_empty,
  --  read_fifo_full => axim_in_read_fifo_full,
  --  -- AXI Master interface
  --  m_axi_aclk => m_axi_aclk,
  --  m_axi_aresetn => m_axi_aresetn,
  --  m_axi_arready => m_axi_arready,
  --  m_axi_arvalid => m_axi_arvalid,
  --  m_axi_araddr => m_axi_araddr,
  --  m_axi_arid => m_axi_arid,
  --  m_axi_arlen => m_axi_arlen,
  --  m_axi_arsize => m_axi_arsize,
  --  m_axi_arburst => m_axi_arburst,
  --  m_axi_arlock => m_axi_arlock,
  --  m_axi_arcache => m_axi_arcache,
  --  m_axi_arprot => m_axi_arprot,
  --  m_axi_arqos => m_axi_arqos,
  --  m_axi_arregion => m_axi_arregion,
  --  m_axi_rready => m_axi_rready,
  --  m_axi_rvalid => m_axi_rvalid,
  --  m_axi_rdata => m_axi_rdata,
  --  m_axi_rresp => m_axi_rresp,
  --  m_axi_rid => m_axi_rid,
  --  m_axi_rlast => m_axi_rlast,
  --  m_axi_awready => m_axi_awready,
  --  m_axi_awvalid => m_axi_awvalid,
  --  m_axi_awaddr => m_axi_awaddr,
  --  m_axi_awid => m_axi_awid,
  --  m_axi_awlen => m_axi_awlen,
  --  m_axi_awsize => m_axi_awsize,
  --  m_axi_awburst => m_axi_awburst,
  --  m_axi_awlock => m_axi_awlock,
  --  m_axi_awcache => m_axi_awcache, 
  --  m_axi_awprot => m_axi_awprot,
  --  m_axi_awqos => m_axi_awqos,
  --  m_axi_awregion => m_axi_awregion,
  --  m_axi_wready => m_axi_wready,
  --  m_axi_wvalid => m_axi_wvalid,
  --  m_axi_wid => m_axi_wid,
  --  m_axi_wdata => m_axi_wdata,
  --  m_axi_wstrb => m_axi_wstrb,
  --  m_axi_wlast => m_axi_wlast,
  --  m_axi_bready => m_axi_bready,
  --  m_axi_bvalid => m_axi_bvalid,
  --  m_axi_bresp => m_axi_bresp,
  --  m_axi_bid => m_axi_bid
  --);


  stall_mem_wb <= stall_ex_mem or stall_wb;

   u10: pipeline_tm_twb port map(
      clk           => clk,
      reset         => reset_in,
      stall         => stall_mem_wb,
      en_write_kblare=> en_write_kblare_p_p,

      en_write_kblare_o=> en_write_kblare_p_p_p,
      rd_index      => rd_index_p_p, 
      rd_index_out  => rd_index_p_p_p,
      c_mux         => c_source_p_p,
      c_mux_o       => c_source_p_p_p,
      c_alu         => c_alu_p,
      c_alu_o       => c_alu_p_p,
      c_mem         => c_memory,
      c_mem_o       => c_memory_p,
      c_tag         => c_tag,
      c_tag_o       => c_tag_p,
      c_instr       => c_instrumentation,
      c_instr_o     => c_instrumentation_p,
      reg_source_1   => reg_source_1_p_p,
      reg_source_1_out=> reg_source_1_p_p_p,

      reg_source_2   => reg_source_2_p_p,
      reg_source_2_out=> reg_source_2_p_p_p,

      reg_pc         => tag_arm_pc_reg_p_p,
      reg_pc_out     => tag_arm_pc_reg_p_p_p,

      reg_instr      => tag_current_instruction_p_p,
      reg_instr_out  => tag_current_instruction_p_p_p,
      
      tag_check_src  => tag_check_sources_p_p,
      tag_check_src_o=> tag_check_sources_p_p_p,

      rf_en           => enable_reg_file_p_p,
      rf_en_out       => enable_reg_file_p_p_p,

      rf_w_en         => write_enable_reg_file_p_p,
      rf_w_en_out     => write_enable_reg_file_p_p_p,

      fp_rf_en        => enable_reg_file_fp_p_p,
      fp_rf_en_out    => enable_reg_file_fp_p_p_p,

      conf_rf_wen     => config_reg_wen_p_p,
      conf_rf_wen_o   => config_reg_wen_p_p_p,

      config_reg_index     => config_reg_index_p_p,
      config_reg_index_out => config_reg_index_p_p_p,

      fp_rf_w_en      => write_enable_reg_file_fp_p_p,
      fp_rf_w_en_out  => write_enable_reg_file_fp_p_p_p

      --c_kblare      => c_kblare, 
      --c_kblare_o    => c_kblare_p
    );

   u11: tag_check port map(
    reg_source_1      => reg_source_1_p_p_p, -- write back stage
    reg_source_2      => reg_source_2_p_p_p, -- wb stage
    reg_dest          => c_bus, -- wb stage
    source_addr       => c_alu_p_p, -- wb stage
    dest_addr         => c_alu_p_p, -- wb stage 
    tag_pc            => tag_arm_pc_reg_p_p_p, -- wb stage
    tag_instruction   => tag_current_instruction_p_p_p, -- wb stage
    tag_check_src     => tag_check_sources_p_p_p, -- wb
    stop_propagation  => interrupt
    );

--   u10: pipeline_IM_IWB port map(
--        clk           => clk,
--        reset         => reset_in,
--        stall         => stall,
--        c_alu_i       => c_alu_p,
--        c_shift_i     => c_shift_p,
--        c_mult_i      =>  c_mult_p,
--        c_source      => c_source_p_p, 
--        pc_source     => pc_source_p_p,
--        c_pc          => pc_p_p,
--        imm_in        => imm_p_p,
--        mem_data      => c_memory,
--        rd_index      => rd_index_p_p, 
--
--        c_alu         => c_alu_p_p,
--        c_shift       => c_shift_p_p, 
--        c_mult        => c_mult_p_p, 
--        c_source_out  => c_source_p_p_p, 
--        pc_source_out => pc_source_p_p_p, 
--        c_pc_out      => pc_p_p_p,
--        imm_out       => imm_p_p_p,
--        rd_index_out  => rd_index_p_p_p,
--        mem_data_out  => c_memory_p );

    t_pc <= pc;
    t_opcode <= opcode;
    t_r_dest <= reg_dest;

    
    --instrumentation_address <= ;
    --instrumentation_enable  <= ;

    -- kblare_ps2pl bram interface
    --kblare_ps2pl_address <= ;
--    kblare_ps2pl_data_w  <= (others => '0');
    --kblare_ps2pl_enable  <= ;

    -- kblare_pl2ps bram interface
    kblare_pl2ps_data_w  <= reg_source_2_p_p; -- it should be register 0 to get the tag of output ...
    kblare_pl2ps_enable  <= en_write_kblare_p_p;

    process(kblare_ps2pl_enable, kblare_ps2pl_address_s)
    begin
      if (kblare_ps2pl_enable = '1') then 
        kblare_ps2pl_address_s <= std_logic_vector(unsigned(kblare_ps2pl_address_s)+4) ;
      else
        kblare_ps2pl_address_s <= kblare_ps2pl_address_s;
      end if;
    end process;

    process(kblare_pl2ps_enable, kblare_pl2ps_address_s)
    begin
      if (kblare_pl2ps_enable = '1') then 
        kblare_pl2ps_address_s <= std_logic_vector(unsigned(kblare_pl2ps_address_s)+4) ;
      else
        kblare_pl2ps_address_s <= kblare_pl2ps_address_s;
      end if;
    end process;

    kblare_ps2pl_empty <= '0';
    kblare_pl2ps_full <= '0';

    kblare_ps2pl_clk <= clk;
    kblare_ps2pl_reset <= reset_in;
    kblare_ps2pl_address <= kblare_ps2pl_address_s;
    --kblare_ps2pl_mem_data_r <= kblare_ps2pl_data_r;
    kblare_ps2pl_sel <= x"f";
    kblare_ps2pl_data_w <= (others => '0');
    kblare_ps2pl_en <= kblare_ps2pl_enable_reg;


    kblare_pl2ps_clk <= clk;
    kblare_pl2ps_reset <= reset_in;
    kblare_pl2ps_address <= kblare_pl2ps_address_s;
    --kblare_pl2ps_mem_data_r <= kblare_pl2ps_data_r;
    kblare_pl2ps_sel <= x"f";
    --kblare_pl2ps_data_w <= kblare_pl2ps_data_w;
    kblare_pl2ps_en <= kblare_pl2ps_enable_reg;
end; --architecture logic

