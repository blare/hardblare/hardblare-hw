onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -group ram /tbench/u1_plasma/u2_ram/clk
add wave -noupdate -group ram /tbench/u1_plasma/u2_ram/enable
add wave -noupdate -group ram /tbench/u1_plasma/u2_ram/write_byte_enable
add wave -noupdate -group ram /tbench/u1_plasma/u2_ram/address
add wave -noupdate -group ram /tbench/u1_plasma/u2_ram/data_write
add wave -noupdate -group ram /tbench/u1_plasma/u2_ram/data_read
add wave -noupdate -group u1_cpu /tbench/u1_plasma/u1_cpu/clk
add wave -noupdate -group u1_cpu /tbench/u1_plasma/u1_cpu/reset_in
add wave -noupdate -group u1_cpu /tbench/u1_plasma/u1_cpu/intr_in
add wave -noupdate -group u1_cpu /tbench/u1_plasma/u1_cpu/address_next
add wave -noupdate -group u1_cpu /tbench/u1_plasma/u1_cpu/byte_we_next
add wave -noupdate -group u1_cpu /tbench/u1_plasma/u1_cpu/address
add wave -noupdate -group u1_cpu /tbench/u1_plasma/u1_cpu/byte_we
add wave -noupdate -group u1_cpu /tbench/u1_plasma/u1_cpu/data_w
add wave -noupdate -group u1_cpu /tbench/u1_plasma/u1_cpu/data_r
add wave -noupdate -group u1_cpu /tbench/u1_plasma/u1_cpu/mem_pause
add wave -noupdate -group u1_cpu /tbench/u1_plasma/u1_cpu/opcode
add wave -noupdate -group u1_cpu /tbench/u1_plasma/u1_cpu/rs_index
add wave -noupdate -group u1_cpu /tbench/u1_plasma/u1_cpu/rt_index
add wave -noupdate -group u1_cpu /tbench/u1_plasma/u1_cpu/rd_index
add wave -noupdate -group u1_cpu /tbench/u1_plasma/u1_cpu/rd_indexD
add wave -noupdate -group u1_cpu /tbench/u1_plasma/u1_cpu/reg_source
add wave -noupdate -group u1_cpu /tbench/u1_plasma/u1_cpu/reg_target
add wave -noupdate -group u1_cpu /tbench/u1_plasma/u1_cpu/reg_dest
add wave -noupdate -group u1_cpu /tbench/u1_plasma/u1_cpu/reg_destD
add wave -noupdate -group u1_cpu /tbench/u1_plasma/u1_cpu/a_bus
add wave -noupdate -group u1_cpu /tbench/u1_plasma/u1_cpu/a_busD
add wave -noupdate -group u1_cpu /tbench/u1_plasma/u1_cpu/b_bus
add wave -noupdate -group u1_cpu /tbench/u1_plasma/u1_cpu/b_busD
add wave -noupdate -group u1_cpu /tbench/u1_plasma/u1_cpu/c_bus
add wave -noupdate -group u1_cpu /tbench/u1_plasma/u1_cpu/c_alu
add wave -noupdate -group u1_cpu /tbench/u1_plasma/u1_cpu/c_shift
add wave -noupdate -group u1_cpu /tbench/u1_plasma/u1_cpu/c_mult
add wave -noupdate -group u1_cpu /tbench/u1_plasma/u1_cpu/c_memory
add wave -noupdate -group u1_cpu /tbench/u1_plasma/u1_cpu/imm
add wave -noupdate -group u1_cpu /tbench/u1_plasma/u1_cpu/pc_future
add wave -noupdate -group u1_cpu /tbench/u1_plasma/u1_cpu/pc_current
add wave -noupdate -group u1_cpu /tbench/u1_plasma/u1_cpu/pc_plus4
add wave -noupdate -group u1_cpu /tbench/u1_plasma/u1_cpu/alu_func
add wave -noupdate -group u1_cpu /tbench/u1_plasma/u1_cpu/alu_funcD
add wave -noupdate -group u1_cpu /tbench/u1_plasma/u1_cpu/shift_func
add wave -noupdate -group u1_cpu /tbench/u1_plasma/u1_cpu/shift_funcD
add wave -noupdate -group u1_cpu /tbench/u1_plasma/u1_cpu/mult_func
add wave -noupdate -group u1_cpu /tbench/u1_plasma/u1_cpu/mult_funcD
add wave -noupdate -group u1_cpu /tbench/u1_plasma/u1_cpu/branch_func
add wave -noupdate -group u1_cpu /tbench/u1_plasma/u1_cpu/take_branch
add wave -noupdate -group u1_cpu /tbench/u1_plasma/u1_cpu/a_source
add wave -noupdate -group u1_cpu /tbench/u1_plasma/u1_cpu/b_source
add wave -noupdate -group u1_cpu /tbench/u1_plasma/u1_cpu/c_source
add wave -noupdate -group u1_cpu /tbench/u1_plasma/u1_cpu/pc_source
add wave -noupdate -group u1_cpu /tbench/u1_plasma/u1_cpu/mem_source
add wave -noupdate -group u1_cpu /tbench/u1_plasma/u1_cpu/pause_mult
add wave -noupdate -group u1_cpu /tbench/u1_plasma/u1_cpu/pause_ctrl
add wave -noupdate -group u1_cpu /tbench/u1_plasma/u1_cpu/pause_pipeline
add wave -noupdate -group u1_cpu /tbench/u1_plasma/u1_cpu/pause_any
add wave -noupdate -group u1_cpu /tbench/u1_plasma/u1_cpu/pause_non_ctrl
add wave -noupdate -group u1_cpu /tbench/u1_plasma/u1_cpu/pause_bank
add wave -noupdate -group u1_cpu /tbench/u1_plasma/u1_cpu/nullify_op
add wave -noupdate -group u1_cpu /tbench/u1_plasma/u1_cpu/intr_enable
add wave -noupdate -group u1_cpu /tbench/u1_plasma/u1_cpu/intr_signal
add wave -noupdate -group u1_cpu /tbench/u1_plasma/u1_cpu/exception_sig
add wave -noupdate -group u1_cpu /tbench/u1_plasma/u1_cpu/reset_reg
add wave -noupdate -group u1_cpu /tbench/u1_plasma/u1_cpu/reset
add wave -noupdate -group pc_next -radix hexadecimal /tbench/u1_plasma/u1_cpu/u1_pc_next/clk
add wave -noupdate -group pc_next -radix hexadecimal /tbench/u1_plasma/u1_cpu/u1_pc_next/reset_in
add wave -noupdate -group pc_next -radix hexadecimal /tbench/u1_plasma/u1_cpu/u1_pc_next/pc_new
add wave -noupdate -group pc_next -radix hexadecimal /tbench/u1_plasma/u1_cpu/u1_pc_next/take_branch
add wave -noupdate -group pc_next -radix hexadecimal /tbench/u1_plasma/u1_cpu/u1_pc_next/pause_in
add wave -noupdate -group pc_next -radix hexadecimal /tbench/u1_plasma/u1_cpu/u1_pc_next/opcode25_0
add wave -noupdate -group pc_next -radix hexadecimal /tbench/u1_plasma/u1_cpu/u1_pc_next/pc_source
add wave -noupdate -group pc_next -radix hexadecimal /tbench/u1_plasma/u1_cpu/u1_pc_next/pc_future
add wave -noupdate -group pc_next -radix hexadecimal /tbench/u1_plasma/u1_cpu/u1_pc_next/pc_current
add wave -noupdate -group pc_next -radix hexadecimal /tbench/u1_plasma/u1_cpu/u1_pc_next/pc_plus4
add wave -noupdate -group pc_next -radix hexadecimal /tbench/u1_plasma/u1_cpu/u1_pc_next/pc_reg
add wave -noupdate -group mem_ctrl -radix hexadecimal /tbench/u1_plasma/u1_cpu/u2_mem_ctrl/clk
add wave -noupdate -group mem_ctrl -radix hexadecimal /tbench/u1_plasma/u1_cpu/u2_mem_ctrl/reset_in
add wave -noupdate -group mem_ctrl -radix hexadecimal /tbench/u1_plasma/u1_cpu/u2_mem_ctrl/pause_in
add wave -noupdate -group mem_ctrl -radix hexadecimal /tbench/u1_plasma/u1_cpu/u2_mem_ctrl/nullify_op
add wave -noupdate -group mem_ctrl -radix hexadecimal /tbench/u1_plasma/u1_cpu/u2_mem_ctrl/address_pc
add wave -noupdate -group mem_ctrl -radix hexadecimal /tbench/u1_plasma/u1_cpu/u2_mem_ctrl/opcode_out
add wave -noupdate -group mem_ctrl -radix hexadecimal /tbench/u1_plasma/u1_cpu/u2_mem_ctrl/address_in
add wave -noupdate -group mem_ctrl -radix hexadecimal /tbench/u1_plasma/u1_cpu/u2_mem_ctrl/mem_source
add wave -noupdate -group mem_ctrl -radix hexadecimal /tbench/u1_plasma/u1_cpu/u2_mem_ctrl/data_write
add wave -noupdate -group mem_ctrl -radix hexadecimal /tbench/u1_plasma/u1_cpu/u2_mem_ctrl/data_read
add wave -noupdate -group mem_ctrl -radix hexadecimal /tbench/u1_plasma/u1_cpu/u2_mem_ctrl/pause_out
add wave -noupdate -group mem_ctrl -radix hexadecimal /tbench/u1_plasma/u1_cpu/u2_mem_ctrl/address_next
add wave -noupdate -group mem_ctrl -radix hexadecimal /tbench/u1_plasma/u1_cpu/u2_mem_ctrl/byte_we_next
add wave -noupdate -group mem_ctrl -radix hexadecimal /tbench/u1_plasma/u1_cpu/u2_mem_ctrl/address
add wave -noupdate -group mem_ctrl -radix hexadecimal /tbench/u1_plasma/u1_cpu/u2_mem_ctrl/byte_we
add wave -noupdate -group mem_ctrl -radix hexadecimal /tbench/u1_plasma/u1_cpu/u2_mem_ctrl/data_w
add wave -noupdate -group mem_ctrl -radix hexadecimal /tbench/u1_plasma/u1_cpu/u2_mem_ctrl/data_r
add wave -noupdate -group mem_ctrl -radix hexadecimal /tbench/u1_plasma/u1_cpu/u2_mem_ctrl/opcode_reg
add wave -noupdate -group mem_ctrl -radix hexadecimal /tbench/u1_plasma/u1_cpu/u2_mem_ctrl/next_opcode_reg
add wave -noupdate -group mem_ctrl -radix hexadecimal /tbench/u1_plasma/u1_cpu/u2_mem_ctrl/address_reg
add wave -noupdate -group mem_ctrl -radix hexadecimal /tbench/u1_plasma/u1_cpu/u2_mem_ctrl/byte_we_reg
add wave -noupdate -group mem_ctrl -radix hexadecimal /tbench/u1_plasma/u1_cpu/u2_mem_ctrl/mem_state_reg
add wave -noupdate -group control /tbench/u1_plasma/u1_cpu/u3_control/opcode
add wave -noupdate -group control /tbench/u1_plasma/u1_cpu/u3_control/intr_signal
add wave -noupdate -group control /tbench/u1_plasma/u1_cpu/u3_control/rs_index
add wave -noupdate -group control /tbench/u1_plasma/u1_cpu/u3_control/rt_index
add wave -noupdate -group control /tbench/u1_plasma/u1_cpu/u3_control/rd_index
add wave -noupdate -group control /tbench/u1_plasma/u1_cpu/u3_control/imm_out
add wave -noupdate -group control /tbench/u1_plasma/u1_cpu/u3_control/alu_func
add wave -noupdate -group control /tbench/u1_plasma/u1_cpu/u3_control/shift_func
add wave -noupdate -group control /tbench/u1_plasma/u1_cpu/u3_control/mult_func
add wave -noupdate -group control /tbench/u1_plasma/u1_cpu/u3_control/branch_func
add wave -noupdate -group control /tbench/u1_plasma/u1_cpu/u3_control/a_source_out
add wave -noupdate -group control /tbench/u1_plasma/u1_cpu/u3_control/b_source_out
add wave -noupdate -group control /tbench/u1_plasma/u1_cpu/u3_control/c_source_out
add wave -noupdate -group control /tbench/u1_plasma/u1_cpu/u3_control/pc_source_out
add wave -noupdate -group control /tbench/u1_plasma/u1_cpu/u3_control/mem_source_out
add wave -noupdate -group control /tbench/u1_plasma/u1_cpu/u3_control/exception_out
add wave -noupdate -group reg_bank /tbench/u1_plasma/u1_cpu/u4_reg_bank/clk
add wave -noupdate -group reg_bank /tbench/u1_plasma/u1_cpu/u4_reg_bank/reset_in
add wave -noupdate -group reg_bank /tbench/u1_plasma/u1_cpu/u4_reg_bank/pause
add wave -noupdate -group reg_bank /tbench/u1_plasma/u1_cpu/u4_reg_bank/rs_index
add wave -noupdate -group reg_bank /tbench/u1_plasma/u1_cpu/u4_reg_bank/rt_index
add wave -noupdate -group reg_bank /tbench/u1_plasma/u1_cpu/u4_reg_bank/rd_index
add wave -noupdate -group reg_bank /tbench/u1_plasma/u1_cpu/u4_reg_bank/reg_source_out
add wave -noupdate -group reg_bank /tbench/u1_plasma/u1_cpu/u4_reg_bank/reg_target_out
add wave -noupdate -group reg_bank /tbench/u1_plasma/u1_cpu/u4_reg_bank/reg_dest_new
add wave -noupdate -group reg_bank /tbench/u1_plasma/u1_cpu/u4_reg_bank/intr_enable
add wave -noupdate -group reg_bank /tbench/u1_plasma/u1_cpu/u4_reg_bank/intr_enable_reg
add wave -noupdate -group reg_bank /tbench/u1_plasma/u1_cpu/u4_reg_bank/addr_read1
add wave -noupdate -group reg_bank /tbench/u1_plasma/u1_cpu/u4_reg_bank/addr_read2
add wave -noupdate -group reg_bank /tbench/u1_plasma/u1_cpu/u4_reg_bank/addr_write
add wave -noupdate -group reg_bank /tbench/u1_plasma/u1_cpu/u4_reg_bank/data_out1
add wave -noupdate -group reg_bank /tbench/u1_plasma/u1_cpu/u4_reg_bank/data_out2
add wave -noupdate -group reg_bank /tbench/u1_plasma/u1_cpu/u4_reg_bank/write_enable
add wave -noupdate -group bus_mux /tbench/u1_plasma/u1_cpu/u5_bus_mux/imm_in
add wave -noupdate -group bus_mux /tbench/u1_plasma/u1_cpu/u5_bus_mux/reg_source
add wave -noupdate -group bus_mux /tbench/u1_plasma/u1_cpu/u5_bus_mux/a_mux
add wave -noupdate -group bus_mux /tbench/u1_plasma/u1_cpu/u5_bus_mux/a_out
add wave -noupdate -group bus_mux /tbench/u1_plasma/u1_cpu/u5_bus_mux/reg_target
add wave -noupdate -group bus_mux /tbench/u1_plasma/u1_cpu/u5_bus_mux/b_mux
add wave -noupdate -group bus_mux /tbench/u1_plasma/u1_cpu/u5_bus_mux/b_out
add wave -noupdate -group bus_mux /tbench/u1_plasma/u1_cpu/u5_bus_mux/c_bus
add wave -noupdate -group bus_mux /tbench/u1_plasma/u1_cpu/u5_bus_mux/c_memory
add wave -noupdate -group bus_mux /tbench/u1_plasma/u1_cpu/u5_bus_mux/c_pc
add wave -noupdate -group bus_mux /tbench/u1_plasma/u1_cpu/u5_bus_mux/c_pc_plus4
add wave -noupdate -group bus_mux /tbench/u1_plasma/u1_cpu/u5_bus_mux/c_mux
add wave -noupdate -group bus_mux /tbench/u1_plasma/u1_cpu/u5_bus_mux/reg_dest_out
add wave -noupdate -group bus_mux /tbench/u1_plasma/u1_cpu/u5_bus_mux/branch_func
add wave -noupdate -group bus_mux /tbench/u1_plasma/u1_cpu/u5_bus_mux/take_branch
add wave -noupdate -group alu /tbench/u1_plasma/u1_cpu/u6_alu/a_in
add wave -noupdate -group alu /tbench/u1_plasma/u1_cpu/u6_alu/b_in
add wave -noupdate -group alu /tbench/u1_plasma/u1_cpu/u6_alu/alu_function
add wave -noupdate -group alu /tbench/u1_plasma/u1_cpu/u6_alu/c_alu
add wave -noupdate -group alu /tbench/u1_plasma/u1_cpu/u6_alu/do_add
add wave -noupdate -group alu /tbench/u1_plasma/u1_cpu/u6_alu/sum
add wave -noupdate -group alu /tbench/u1_plasma/u1_cpu/u6_alu/less_than
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {99997673479 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 248
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {99997520133 ps} {99998825326 ps}
