library IEEE; 
use ieee.std_logic_1164.All;
use work.dependencies_pack.all;


entity pipeline_tm_twb is 
port (
    clk, reset      : in  std_logic;
    stall           : in  std_logic;

    en_write_kblare : in  std_logic;
    en_write_kblare_o:out std_logic;

    rd_index        : in  std_logic_vector(4 downto 0);
    rd_index_out    : out std_logic_vector(4 downto 0);

    c_mux           : in  reg_dst_source_type;
    c_mux_o         : out reg_dst_source_type;

    c_alu           : in  std_logic_vector(31 downto 0);
    c_alu_o         : out std_logic_vector(31 downto 0);

    c_mem           : in  std_logic_vector(31 downto 0);
    c_mem_o         : out std_logic_vector(31 downto 0);

    c_tag           : in  std_logic_vector(31 downto 0);
    c_tag_o         : out std_logic_vector(31 downto 0);

    c_instr         : in  std_logic_vector(31 downto 0);
    c_instr_o       : out std_logic_vector(31 downto 0);

    reg_source_1    : in  std_logic_vector(31 downto 0);
    reg_source_1_out: out std_logic_vector(31 downto 0);

    reg_source_2    : in  std_logic_vector(31 downto 0);
    reg_source_2_out: out std_logic_vector(31 downto 0);

    reg_pc          : in  std_logic_vector(31 downto 0);
    reg_pc_out      : out std_logic_vector(31 downto 0);

    reg_instr       : in  std_logic_vector(31 downto 0);
    reg_instr_out   : out std_logic_vector(31 downto 0);

    tag_check_src   : in  tag_check_type;
    tag_check_src_o : out tag_check_type;

    rf_en           : in  std_logic;
    rf_en_out       : out std_logic;

    rf_w_en         : in  std_logic;
    rf_w_en_out     : out std_logic;

    fp_rf_en        : in  std_logic;
    fp_rf_en_out    : out std_logic;

    fp_rf_w_en      : in  std_logic;
    fp_rf_w_en_out  : out std_logic;

    conf_rf_wen     : in  std_logic;
    conf_rf_wen_o   : out std_logic;


    config_reg_index        : in  std_logic_vector(3 downto 0);
    config_reg_index_out    : out std_logic_vector(3 downto 0)
    --c_kblare        : in  std_logic_vector(31 downto 0);
    --c_kblare_o      : out std_logic_vector(31 downto 0)
    );
end entity; 

architecture behavioral of pipeline_tm_twb is 
    
signal en_write_kblare_reg      : std_logic;
signal rd_index_out_reg         : std_logic_vector(4 downto 0);
signal c_alu_reg        : std_logic_vector(31 downto 0);
signal c_mem_reg        : std_logic_vector(31 downto 0);
signal c_tag_reg        : std_logic_vector(31 downto 0);
signal c_instr_reg      : std_logic_vector(31 downto 0);
--signal c_kblare_reg   : std_logic_vector(31 downto 0);
signal c_mux_reg        : reg_dst_source_type;
signal reg_source_1_out_reg     : std_logic_vector(31 downto 0);
signal reg_source_2_out_reg     : std_logic_vector(31 downto 0);
signal reg_pc_reg               : std_logic_vector(31 downto 0);
signal reg_instr_reg            : std_logic_vector(31 downto 0);
signal tag_check_src_reg        : tag_check_type;
signal rf_en_reg                : std_logic;
signal rf_w_en_reg              : std_logic;
signal fp_rf_en_reg             : std_logic;
signal fp_rf_w_en_reg           : std_logic;
signal conf_rf_wen_reg          : std_logic;
signal stall_r                  : std_logic;
signal config_reg_index_out_reg : std_logic_vector(3 downto 0);

begin

process(clk) 
begin
        if rising_edge(clk) then 
            if (reset = '1' or stall = '1') then
                en_write_kblare_reg <= '0';
                rd_index_out_reg    <= (others => '0');                
                c_alu_reg       <= (others => '0'); 
                c_mem_reg       <= (others => '0'); 
                c_tag_reg       <= (others => '0'); 
                c_instr_reg     <= (others => '0'); 
                --c_kblare_reg    <= (others => '0');
                c_mux_reg       <= (others => '0');
                reg_source_1_out_reg<= (others => '0');
                reg_source_2_out_reg<= (others => '0');
                reg_pc_reg          <= (others => '0');
                reg_instr_reg       <= (others => '0');
                tag_check_src_reg   <= (others => '0');
                conf_rf_wen_reg     <= '0';
                rf_en_reg           <= '0';
                rf_w_en_reg         <= '0';
                fp_rf_en_reg        <= '0';
                fp_rf_w_en_reg      <= '0';
                config_reg_index_out_reg <= (others => '0');
                stall_r <= '0';
            else
                stall_r <= stall;
                --if stall_r  = '0' then                    
                    en_write_kblare_reg <= en_write_kblare;
                    rd_index_out_reg    <= rd_index;
                    c_alu_reg       <= c_alu;
                    c_mem_reg       <= c_mem;
                    c_tag_reg       <= c_tag; 
                    c_instr_reg     <= c_instr;
                    --c_kblare_reg  <= c_kblare;
                    c_mux_reg       <= c_mux;
                    reg_source_1_out_reg<= reg_source_1;
                    reg_source_2_out_reg<= reg_source_2;
                    reg_pc_reg          <= reg_pc;
                    reg_instr_reg       <= reg_instr;
                    tag_check_src_reg   <= tag_check_src;
                    rf_en_reg           <= rf_en;
                    rf_w_en_reg         <= rf_w_en;
                    fp_rf_en_reg        <= fp_rf_en;
                    fp_rf_w_en_reg      <= fp_rf_w_en;
                    conf_rf_wen_reg     <= conf_rf_wen;
                    config_reg_index_out_reg <= config_reg_index;
                --else 
                --    en_write_kblare_reg <= en_write_kblare_reg; 
                --    rd_index_out_reg    <= rd_index_out_reg;
                --    c_alu_reg       <= c_alu_reg;
                --    c_mem_reg       <= c_mem_reg;
                --    c_tag_reg       <= c_tag_reg; 
                --    c_instr_reg     <= c_instr_reg;
                --    --c_kblare_reg    <= c_kblare_reg;
                --    c_mux_reg       <= c_mux_reg;
                --    reg_source_1_out_reg<= reg_source_1_out_reg;
                --    reg_source_2_out_reg<= reg_source_2_out_reg;
                --    reg_pc_reg          <= reg_pc;
                --    reg_instr_reg       <= reg_instr;
                --    tag_check_src_reg   <= tag_check_src_reg;
                --    rf_en_reg           <= rf_en_reg;
                --    rf_w_en_reg         <= rf_w_en_reg;
                --    fp_rf_en_reg        <= fp_rf_en_reg;
                --    fp_rf_w_en_reg      <= fp_rf_w_en_reg;
                --    conf_rf_wen_reg     <= conf_rf_wen_reg;
                --    config_reg_index_out_reg <= config_reg_index_out_reg;
                --end if;
            end if;
        end if;
end process;

en_write_kblare_o   <= en_write_kblare_reg;
rd_index_out        <=  rd_index_out_reg ;
c_alu_o             <= c_alu_reg;
c_mem_o             <= c_mem_reg;
c_tag_o             <= c_tag; 
c_instr_o           <= c_instr_reg;
--c_kblare_o          <= c_kblare_reg;
c_mux_o             <= c_mux_reg;
reg_source_1_out    <= reg_source_1_out_reg;
reg_source_2_out    <= reg_source_2_out_reg;
reg_pc_out          <= reg_pc_reg;
reg_instr_out       <= reg_instr_reg;
tag_check_src_o     <= tag_check_src_reg;
rf_en_out           <= rf_en_reg;
rf_w_en_out         <= rf_w_en_reg;
fp_rf_en_out        <= fp_rf_en_reg;
fp_rf_w_en_out      <= fp_rf_w_en_reg;
conf_rf_wen_o       <= conf_rf_wen_reg;
config_reg_index_out <= config_reg_index_out_reg;
end architecture;

