-- simple memory controller to write to BRAM
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity mem_ctl is
generic(
    width : integer range 1 to 32 := 8;
    address_width : integer range 1 to 32 := 32
);
port(
  clk, reset : in std_logic;
  data_v : in std_logic;
  data_in : in std_logic_vector(width-1 downto 0);  
  rst,en : out std_logic;
  w_en : out std_logic_vector(3 downto 0);
  data_out : out std_logic_vector(width-1 downto 0);
  addr : out std_logic_vector(address_width-1 downto 0)
);
end entity;

architecture rtl of mem_ctl is
signal temp_addr : std_logic_vector(address_width-1 downto 0) := (others => '0');
signal data_reg_s : std_logic_vector(width-1 downto 0);
signal data_v_reg : std_logic;
signal w_en_s : std_logic_vector(3 downto 0);
attribute keep : string;
attribute keep of w_en_s : signal is "true";
begin

    process(clk) 
    begin
        if rising_edge(clk) then 
            if reset = '1' then 
                data_v_reg <= '0';
                data_reg_s <= (others => '0');
            elsif (data_v = '1') then 
                data_v_reg <= data_v;
                data_reg_s <= data_in;
            else
                data_v_reg <= '0';
                data_reg_s <= (others => '0');
            end if;
        end if;
    end process;
  
  process(clk)
  begin    
    if (rising_edge(clk)) then
        if reset = '1' then 
            data_out <= (others => '0');
            temp_addr <= (others => '0');
            w_en_s <= "0000"; 
            en <= '0';                    
        elsif (data_v_reg = '1') then            
            data_out <= data_reg_s;
            temp_addr <= std_logic_vector(unsigned(temp_addr) + to_unsigned(4,address_width));
            w_en_s <= "1111"; 
            en <= '1';
        else
            temp_addr <= temp_addr;
            data_out <= (others => '0');
            w_en_s <= "0000"; -- 4 bytes (1 bit per byte)
            en <= '0';
        end if;
    end if;
  end process;
  addr <= temp_addr;
  rst <= '0';    
  w_en <= w_en_s;
end architecture;
