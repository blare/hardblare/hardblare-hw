library ieee;
use ieee.std_logic_1164.all;
--use ieee.std_logic_arith.all;
use IEEE.numeric_std.all;
use work.byte_pkg.all;

entity TLB is
generic ( n: integer := 32;
	  m: integer := 26;
	  TLB_ENTRIES: integer := 64;
	  OFFSET: integer := 12);
port (clock, reset: in std_logic;
    pn:in std_logic_vector(31-OFFSET downto 0); -- 20 bit virtual page number, 20+12=32
--    fn_in:in std_logic_vector(m-OFFSET-1 downto 0); -- 14 bit frame number, 14+12=26
    fn_out:out std_logic_vector(m-OFFSET-1 downto 0);
    r:in std_logic;
    w:in std_logic;
--    TLB_update_done:out std_logic;
    hit:out std_logic);
end TLB;

architecture Behavioral of TLB is

--	subtype byte is std_logic_vector(n-13 downto 0); 
--    	type byte_array_t is array(natural range <>) of byte;

component tbuff is
generic ( n: integer := 32;
	  m: integer := 26;
	  OFFSET: integer := 12);
PORT( inp:IN std_logic_vector(m-OFFSET-1 DOWNTO 0);
    sel:IN std_logic;
   outp:OUT std_logic_vector(m-OFFSET-1 DOWNTO 0));
end component;

component comparator is
generic ( n: integer := 32;
	  m: integer := 26;
	  OFFSET: integer := 12);
Port(
    IN1:in std_logic_vector(31-OFFSET downto 0);
    IN2:in std_logic_vector(31-OFFSET downto 0);
    r:in std_logic;
    equal:out std_logic);
end component;

component out_sel is
generic ( n: integer := 32;
	  m: integer := 26;
	  TLB_ENTRIES: integer := 64;
	  OFFSET: integer := 12);
Port(clock, reset: in std_logic;
    inp:in byte_array_t(0 to TLB_ENTRIES-1);
    outp:out std_logic_vector(m-OFFSET-1 downto 0);
    selector:in std_logic_vector(0 to TLB_ENTRIES-1));
end component;

type tlb_type is array (0 to TLB_ENTRIES-1) of std_logic_vector(31+m-OFFSET-OFFSET downto 0); --Total 14 entries and each is 34=20+14 bit wide
signal tlb: tlb_type;--type eqtype is array (0 to 7) of std_logic;
signal equal:std_logic_vector(0 to TLB_ENTRIES-1);
signal out_array:byte_array_t(0 to TLB_ENTRIES-1);
signal Counter: integer range 0 to TLB_ENTRIES-1 := 0;
signal tempfn:std_logic_vector(m-OFFSET-1 downto 0);
signal temp_hit:std_logic := '0';
--signal TLB_update_done_tmp:std_logic;

begin
    -- initializing the 14 comparators, tri-state buffers
    comps:for i in 0 to TLB_ENTRIES-1 generate
        compx: comparator generic map (n, m, OFFSET) port map(tlb(i)(31+m-OFFSET-OFFSET downto m-OFFSET), pn, r, equal(i));
        tbuffx: tbuff generic map (n, m, OFFSET) port map(tlb(i)(m-OFFSET-1 downto 0), equal(i) , out_array(i));
    end generate comps;
    --inializing the output multiplexer
    MUX:out_sel generic map (n, m, TLB_ENTRIES, OFFSET) port map (clock, reset, out_array, tempfn, equal);

 --   process(clock)
   -- begin
	--if (rising_edge(clock)) then
	--	if r='1' then
	--		for i in equal'range loop
    	--			temp_hit <= temp_hit or equal(i);
  	--		end loop;
--
---			if temp_hit = '1'  then
   --             		hit<='1';
     --           		fn_out<=tempfn;
       --     		else
         --       		hit <='0';
           --     		fn_out <= (others => '0');
            --		end if;
		--else
		--	temp_hit <= '0';
		--end if;
--	end if;
  --  end process;

--    process(clock,reset)
--    begin
--	if (rising_edge(clock)) then
		
--	end if;
--    end process;
    
--    process (clock)
--    begin
--        if (rising_edge(clock)) AND r='1' then
--            for i in 0 to m-OFFSET-1 loop
--		if temp_hit = '1'  then
--                	hit<='1';
--                	fn_out<=tempfn;
--            	else
--               	hit <='0';
--                	fn_out <= (others => '0');
--            	end if;
--	   end loop;
--        end if;
--    end process;

 --   TLB_update_done_tmp <= '0' when w='0';
 --   TLB_update_done <= TLB_update_done_tmp;

    process (clock,reset)
    begin
        if (rising_edge(clock)) then
		if (reset = '1') then
		--	TLB_update_done_tmp <= '0';
			hit <='0';
                	fn_out <= (others => '0');
		else
			if w='1' then
            			tlb(Counter) <= pn & std_logic_vector(to_unsigned(Counter,m-OFFSET));
            			if Counter < TLB_ENTRIES-1 then
                			Counter <= Counter +1;
			--		TLB_update_done_tmp <= '0';
            			else
                			Counter <=0;
			--		TLB_update_done_tmp <= '1';
            			end if;
        		end if;

		--	if w='0' then
		--		TLB_update_done_tmp <= '0';
		--	end if;
			
			if r='1' then
				for i in equal'range loop
    				
					if equal(i) = '1'  then
                				hit<='1';
						temp_hit <='1';
                			--	fn_out<=tempfn;
						exit;
            				else
                				hit <='0';
                				fn_out <= (others => '0');
            				end if;
				end loop;
		 	end if;

			if temp_hit = '1' then
				fn_out<=tempfn;
				hit <= '0';
				temp_hit <='0';
			end if;
		end if;
	end if;
    end process;

end Behavioral;


