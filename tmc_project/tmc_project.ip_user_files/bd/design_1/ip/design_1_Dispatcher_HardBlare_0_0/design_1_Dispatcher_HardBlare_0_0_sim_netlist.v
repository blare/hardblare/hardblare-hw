// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
// Date        : Sat Jun 20 13:50:32 2020
// Host        : HardBlare-Workstation running 64-bit Ubuntu 16.04.6 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_1_Dispatcher_HardBlare_0_0 -prefix
//               design_1_Dispatcher_HardBlare_0_0_ design_1_Dispatcher_HardBlare_0_0_sim_netlist.v
// Design      : design_1_Dispatcher_HardBlare_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module design_1_Dispatcher_HardBlare_0_0_Dispatcher_Plasma_MIPS_HardBlare
   (bram_firmware_code_en,
    bram_firmware_code_address,
    bram_firmware_code_data_write,
    bram_firmware_code_bytes_selection,
    bram_bbt_annotations_en,
    bram_bbt_annotations_address,
    bram_bbt_annotations_data_write,
    bram_bbt_annotations_bytes_selection,
    fifo_ptm_en,
    fifo_monitor_to_kernel_en,
    fifo_monitor_to_kernel_data_write,
    fifo_kernel_to_monitor_en,
    fifo_read_instrumentation_request_read_en,
    bram_debug_en,
    bram_debug_address,
    bram_debug_data_write,
    bram_debug_bytes_selection,
    PLtoPS_enable,
    PLtoPS_address,
    PLtoPS_write_data,
    PLtoPS_bytes_selection,
    PLtoPS_generate_interrupt,
    bram_sec_policy_config_en,
    bram_sec_policy_config_address,
    bram_sec_policy_config_data_write,
    bram_sec_policy_config_bytes_selection,
    bram_debug_data_read,
    bram_sec_policy_config_data_read,
    fifo_kernel_to_monitor_empty,
    fifo_kernel_to_monitor_almost_empty,
    fifo_kernel_to_monitor_full,
    fifo_monitor_to_kernel_empty,
    fifo_monitor_to_kernel_almost_empty,
    fifo_monitor_to_kernel_full,
    fifo_instrumentation_empty,
    fifo_instrumentation_almost_empty,
    fifo_instrumentation_full,
    clk,
    reset,
    fifo_ptm_empty,
    PLtoPS_readed_data,
    fifo_kernel_to_monitor_data_read,
    fifo_read_instrumentation_data_read,
    fifo_ptm_data_read,
    bram_bbt_annotations_data_read,
    bram_firmware_code_data_read,
    fifo_kernel_to_monitor_almost_full,
    fifo_instrumentation_almost_full,
    fifo_monitor_to_kernel_almost_full,
    fifo_ptm_almost_full,
    fifo_ptm_almost_empty,
    fifo_ptm_full);
  output bram_firmware_code_en;
  output [17:0]bram_firmware_code_address;
  output [31:0]bram_firmware_code_data_write;
  output [3:0]bram_firmware_code_bytes_selection;
  output bram_bbt_annotations_en;
  output [17:0]bram_bbt_annotations_address;
  output [31:0]bram_bbt_annotations_data_write;
  output [3:0]bram_bbt_annotations_bytes_selection;
  output fifo_ptm_en;
  output fifo_monitor_to_kernel_en;
  output [31:0]fifo_monitor_to_kernel_data_write;
  output fifo_kernel_to_monitor_en;
  output fifo_read_instrumentation_request_read_en;
  output bram_debug_en;
  output [17:0]bram_debug_address;
  output [31:0]bram_debug_data_write;
  output [3:0]bram_debug_bytes_selection;
  output PLtoPS_enable;
  output [17:0]PLtoPS_address;
  output [31:0]PLtoPS_write_data;
  output [3:0]PLtoPS_bytes_selection;
  output PLtoPS_generate_interrupt;
  output bram_sec_policy_config_en;
  output [17:0]bram_sec_policy_config_address;
  output [31:0]bram_sec_policy_config_data_write;
  output [3:0]bram_sec_policy_config_bytes_selection;
  input [31:0]bram_debug_data_read;
  input [31:0]bram_sec_policy_config_data_read;
  input fifo_kernel_to_monitor_empty;
  input fifo_kernel_to_monitor_almost_empty;
  input fifo_kernel_to_monitor_full;
  input fifo_monitor_to_kernel_empty;
  input fifo_monitor_to_kernel_almost_empty;
  input fifo_monitor_to_kernel_full;
  input fifo_instrumentation_empty;
  input fifo_instrumentation_almost_empty;
  input fifo_instrumentation_full;
  input clk;
  input reset;
  input fifo_ptm_empty;
  input [31:0]PLtoPS_readed_data;
  input [31:0]fifo_kernel_to_monitor_data_read;
  input [31:0]fifo_read_instrumentation_data_read;
  input [31:0]fifo_ptm_data_read;
  input [31:0]bram_bbt_annotations_data_read;
  input [31:0]bram_firmware_code_data_read;
  input fifo_kernel_to_monitor_almost_full;
  input fifo_instrumentation_almost_full;
  input fifo_monitor_to_kernel_almost_full;
  input fifo_ptm_almost_full;
  input fifo_ptm_almost_empty;
  input fifo_ptm_full;

  wire [17:0]PLtoPS_address;
  wire [3:0]PLtoPS_bytes_selection;
  wire PLtoPS_enable;
  wire PLtoPS_generate_interrupt;
  wire [31:0]PLtoPS_readed_data;
  wire [31:0]PLtoPS_write_data;
  wire [17:0]bram_bbt_annotations_address;
  wire [3:0]bram_bbt_annotations_bytes_selection;
  wire [31:0]bram_bbt_annotations_data_read;
  wire [31:0]bram_bbt_annotations_data_write;
  wire bram_bbt_annotations_en;
  wire [17:0]bram_debug_address;
  wire [3:0]bram_debug_bytes_selection;
  wire [31:0]bram_debug_data_read;
  wire [31:0]bram_debug_data_write;
  wire bram_debug_en;
  wire [17:0]bram_firmware_code_address;
  wire [3:0]bram_firmware_code_bytes_selection;
  wire [31:0]bram_firmware_code_data_read;
  wire [31:0]bram_firmware_code_data_write;
  wire bram_firmware_code_en;
  wire [17:0]bram_sec_policy_config_address;
  wire [3:0]bram_sec_policy_config_bytes_selection;
  wire [31:0]bram_sec_policy_config_data_read;
  wire [31:0]bram_sec_policy_config_data_write;
  wire bram_sec_policy_config_en;
  wire [3:0]byte_we_next;
  wire clk;
  wire [19:2]complete_address_next;
  wire [31:0]cpu_data_w;
  wire fifo_instrumentation_almost_empty;
  wire fifo_instrumentation_almost_full;
  wire fifo_instrumentation_empty;
  wire fifo_instrumentation_full;
  wire fifo_kernel_to_monitor_almost_empty;
  wire fifo_kernel_to_monitor_almost_full;
  wire [31:0]fifo_kernel_to_monitor_data_read;
  wire fifo_kernel_to_monitor_empty;
  wire fifo_kernel_to_monitor_en;
  wire fifo_kernel_to_monitor_full;
  wire fifo_monitor_to_kernel_almost_empty;
  wire fifo_monitor_to_kernel_almost_full;
  wire [31:0]fifo_monitor_to_kernel_data_write;
  wire fifo_monitor_to_kernel_empty;
  wire fifo_monitor_to_kernel_en;
  wire fifo_monitor_to_kernel_full;
  wire fifo_ptm_almost_empty;
  wire fifo_ptm_almost_full;
  wire [31:0]fifo_ptm_data_read;
  wire fifo_ptm_empty;
  wire fifo_ptm_en;
  wire fifo_ptm_full;
  wire [31:0]fifo_read_instrumentation_data_read;
  wire fifo_read_instrumentation_request_read_en;
  wire intr_enable;
  wire intr_enable_reg_i_1_n_0;
  wire mode_reg_i_1_n_0;
  wire negate_reg_i_1_n_0;
  wire [0:0]reg_dest;
  wire reset;
  wire sign2_reg_i_1_n_0;
  wire sign_reg_i_1_n_0;
  wire u1_cpu_n_11;
  wire u1_cpu_n_3;
  wire u1_cpu_n_30;
  wire u1_cpu_n_31;
  wire u1_cpu_n_32;
  wire u1_cpu_n_33;
  wire u1_cpu_n_34;
  wire u1_cpu_n_35;
  wire u1_cpu_n_36;
  wire u1_cpu_n_37;
  wire u1_cpu_n_38;
  wire u1_cpu_n_4;
  wire u1_cpu_n_43;
  wire u1_cpu_n_44;
  wire u1_cpu_n_45;
  wire u1_cpu_n_46;
  wire u1_cpu_n_47;
  wire u1_cpu_n_48;
  wire u1_cpu_n_49;
  wire u1_cpu_n_51;
  wire u1_cpu_n_52;
  wire u1_cpu_n_6;
  wire u1_cpu_n_7;
  wire u1_cpu_n_8;
  wire u1_cpu_n_85;
  wire u1_cpu_n_9;
  wire \u4_reg_bank/eqOp__4 ;
  wire \u8_mult/mode_reg ;
  wire \u8_mult/negate_reg ;
  wire \u8_mult/sign2_reg ;
  wire \u8_mult/sign_reg__6 ;

  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \PLtoPS_address_reg[10] 
       (.CLR(1'b0),
        .D(complete_address_next[10]),
        .G(u1_cpu_n_37),
        .GE(1'b1),
        .Q(PLtoPS_address[8]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \PLtoPS_address_reg[11] 
       (.CLR(1'b0),
        .D(complete_address_next[11]),
        .G(u1_cpu_n_37),
        .GE(1'b1),
        .Q(PLtoPS_address[9]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \PLtoPS_address_reg[12] 
       (.CLR(1'b0),
        .D(complete_address_next[12]),
        .G(u1_cpu_n_37),
        .GE(1'b1),
        .Q(PLtoPS_address[10]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \PLtoPS_address_reg[13] 
       (.CLR(1'b0),
        .D(complete_address_next[13]),
        .G(u1_cpu_n_37),
        .GE(1'b1),
        .Q(PLtoPS_address[11]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \PLtoPS_address_reg[14] 
       (.CLR(1'b0),
        .D(complete_address_next[14]),
        .G(u1_cpu_n_37),
        .GE(1'b1),
        .Q(PLtoPS_address[12]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \PLtoPS_address_reg[15] 
       (.CLR(1'b0),
        .D(complete_address_next[15]),
        .G(u1_cpu_n_37),
        .GE(1'b1),
        .Q(PLtoPS_address[13]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \PLtoPS_address_reg[16] 
       (.CLR(1'b0),
        .D(complete_address_next[16]),
        .G(u1_cpu_n_37),
        .GE(1'b1),
        .Q(PLtoPS_address[14]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \PLtoPS_address_reg[17] 
       (.CLR(1'b0),
        .D(complete_address_next[17]),
        .G(u1_cpu_n_37),
        .GE(1'b1),
        .Q(PLtoPS_address[15]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \PLtoPS_address_reg[18] 
       (.CLR(1'b0),
        .D(complete_address_next[18]),
        .G(u1_cpu_n_37),
        .GE(1'b1),
        .Q(PLtoPS_address[16]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \PLtoPS_address_reg[19] 
       (.CLR(1'b0),
        .D(complete_address_next[19]),
        .G(u1_cpu_n_37),
        .GE(1'b1),
        .Q(PLtoPS_address[17]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \PLtoPS_address_reg[2] 
       (.CLR(1'b0),
        .D(complete_address_next[2]),
        .G(u1_cpu_n_37),
        .GE(1'b1),
        .Q(PLtoPS_address[0]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \PLtoPS_address_reg[3] 
       (.CLR(1'b0),
        .D(complete_address_next[3]),
        .G(u1_cpu_n_37),
        .GE(1'b1),
        .Q(PLtoPS_address[1]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \PLtoPS_address_reg[4] 
       (.CLR(1'b0),
        .D(complete_address_next[4]),
        .G(u1_cpu_n_37),
        .GE(1'b1),
        .Q(PLtoPS_address[2]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \PLtoPS_address_reg[5] 
       (.CLR(1'b0),
        .D(complete_address_next[5]),
        .G(u1_cpu_n_37),
        .GE(1'b1),
        .Q(PLtoPS_address[3]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \PLtoPS_address_reg[6] 
       (.CLR(1'b0),
        .D(complete_address_next[6]),
        .G(u1_cpu_n_37),
        .GE(1'b1),
        .Q(PLtoPS_address[4]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \PLtoPS_address_reg[7] 
       (.CLR(1'b0),
        .D(complete_address_next[7]),
        .G(u1_cpu_n_37),
        .GE(1'b1),
        .Q(PLtoPS_address[5]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \PLtoPS_address_reg[8] 
       (.CLR(1'b0),
        .D(complete_address_next[8]),
        .G(u1_cpu_n_37),
        .GE(1'b1),
        .Q(PLtoPS_address[6]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \PLtoPS_address_reg[9] 
       (.CLR(1'b0),
        .D(complete_address_next[9]),
        .G(u1_cpu_n_37),
        .GE(1'b1),
        .Q(PLtoPS_address[7]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \PLtoPS_bytes_selection_reg[0] 
       (.CLR(1'b0),
        .D(byte_we_next[0]),
        .G(u1_cpu_n_37),
        .GE(1'b1),
        .Q(PLtoPS_bytes_selection[0]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \PLtoPS_bytes_selection_reg[1] 
       (.CLR(1'b0),
        .D(byte_we_next[1]),
        .G(u1_cpu_n_37),
        .GE(1'b1),
        .Q(PLtoPS_bytes_selection[1]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \PLtoPS_bytes_selection_reg[2] 
       (.CLR(1'b0),
        .D(byte_we_next[2]),
        .G(u1_cpu_n_37),
        .GE(1'b1),
        .Q(PLtoPS_bytes_selection[2]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \PLtoPS_bytes_selection_reg[3] 
       (.CLR(1'b0),
        .D(byte_we_next[3]),
        .G(u1_cpu_n_37),
        .GE(1'b1),
        .Q(PLtoPS_bytes_selection[3]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    PLtoPS_enable_reg
       (.CLR(1'b0),
        .D(u1_cpu_n_49),
        .G(u1_cpu_n_34),
        .GE(1'b1),
        .Q(PLtoPS_enable));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    PLtoPS_generate_interrupt_reg
       (.CLR(1'b0),
        .D(u1_cpu_n_45),
        .G(u1_cpu_n_34),
        .GE(1'b1),
        .Q(PLtoPS_generate_interrupt));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \PLtoPS_write_data_reg[0] 
       (.CLR(1'b0),
        .D(cpu_data_w[0]),
        .G(u1_cpu_n_38),
        .GE(1'b1),
        .Q(PLtoPS_write_data[0]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \PLtoPS_write_data_reg[10] 
       (.CLR(1'b0),
        .D(cpu_data_w[10]),
        .G(u1_cpu_n_38),
        .GE(1'b1),
        .Q(PLtoPS_write_data[10]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \PLtoPS_write_data_reg[11] 
       (.CLR(1'b0),
        .D(cpu_data_w[11]),
        .G(u1_cpu_n_38),
        .GE(1'b1),
        .Q(PLtoPS_write_data[11]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \PLtoPS_write_data_reg[12] 
       (.CLR(1'b0),
        .D(cpu_data_w[12]),
        .G(u1_cpu_n_38),
        .GE(1'b1),
        .Q(PLtoPS_write_data[12]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \PLtoPS_write_data_reg[13] 
       (.CLR(1'b0),
        .D(cpu_data_w[13]),
        .G(u1_cpu_n_38),
        .GE(1'b1),
        .Q(PLtoPS_write_data[13]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \PLtoPS_write_data_reg[14] 
       (.CLR(1'b0),
        .D(cpu_data_w[14]),
        .G(u1_cpu_n_38),
        .GE(1'b1),
        .Q(PLtoPS_write_data[14]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \PLtoPS_write_data_reg[15] 
       (.CLR(1'b0),
        .D(cpu_data_w[15]),
        .G(u1_cpu_n_38),
        .GE(1'b1),
        .Q(PLtoPS_write_data[15]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \PLtoPS_write_data_reg[16] 
       (.CLR(1'b0),
        .D(cpu_data_w[16]),
        .G(u1_cpu_n_38),
        .GE(1'b1),
        .Q(PLtoPS_write_data[16]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \PLtoPS_write_data_reg[17] 
       (.CLR(1'b0),
        .D(cpu_data_w[17]),
        .G(u1_cpu_n_38),
        .GE(1'b1),
        .Q(PLtoPS_write_data[17]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \PLtoPS_write_data_reg[18] 
       (.CLR(1'b0),
        .D(cpu_data_w[18]),
        .G(u1_cpu_n_38),
        .GE(1'b1),
        .Q(PLtoPS_write_data[18]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \PLtoPS_write_data_reg[19] 
       (.CLR(1'b0),
        .D(cpu_data_w[19]),
        .G(u1_cpu_n_38),
        .GE(1'b1),
        .Q(PLtoPS_write_data[19]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \PLtoPS_write_data_reg[1] 
       (.CLR(1'b0),
        .D(cpu_data_w[1]),
        .G(u1_cpu_n_38),
        .GE(1'b1),
        .Q(PLtoPS_write_data[1]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \PLtoPS_write_data_reg[20] 
       (.CLR(1'b0),
        .D(cpu_data_w[20]),
        .G(u1_cpu_n_38),
        .GE(1'b1),
        .Q(PLtoPS_write_data[20]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \PLtoPS_write_data_reg[21] 
       (.CLR(1'b0),
        .D(cpu_data_w[21]),
        .G(u1_cpu_n_38),
        .GE(1'b1),
        .Q(PLtoPS_write_data[21]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \PLtoPS_write_data_reg[22] 
       (.CLR(1'b0),
        .D(cpu_data_w[22]),
        .G(u1_cpu_n_38),
        .GE(1'b1),
        .Q(PLtoPS_write_data[22]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \PLtoPS_write_data_reg[23] 
       (.CLR(1'b0),
        .D(cpu_data_w[23]),
        .G(u1_cpu_n_38),
        .GE(1'b1),
        .Q(PLtoPS_write_data[23]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \PLtoPS_write_data_reg[24] 
       (.CLR(1'b0),
        .D(cpu_data_w[24]),
        .G(u1_cpu_n_38),
        .GE(1'b1),
        .Q(PLtoPS_write_data[24]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \PLtoPS_write_data_reg[25] 
       (.CLR(1'b0),
        .D(cpu_data_w[25]),
        .G(u1_cpu_n_38),
        .GE(1'b1),
        .Q(PLtoPS_write_data[25]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \PLtoPS_write_data_reg[26] 
       (.CLR(1'b0),
        .D(cpu_data_w[26]),
        .G(u1_cpu_n_38),
        .GE(1'b1),
        .Q(PLtoPS_write_data[26]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \PLtoPS_write_data_reg[27] 
       (.CLR(1'b0),
        .D(cpu_data_w[27]),
        .G(u1_cpu_n_38),
        .GE(1'b1),
        .Q(PLtoPS_write_data[27]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \PLtoPS_write_data_reg[28] 
       (.CLR(1'b0),
        .D(cpu_data_w[28]),
        .G(u1_cpu_n_38),
        .GE(1'b1),
        .Q(PLtoPS_write_data[28]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \PLtoPS_write_data_reg[29] 
       (.CLR(1'b0),
        .D(cpu_data_w[29]),
        .G(u1_cpu_n_38),
        .GE(1'b1),
        .Q(PLtoPS_write_data[29]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \PLtoPS_write_data_reg[2] 
       (.CLR(1'b0),
        .D(cpu_data_w[2]),
        .G(u1_cpu_n_38),
        .GE(1'b1),
        .Q(PLtoPS_write_data[2]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \PLtoPS_write_data_reg[30] 
       (.CLR(1'b0),
        .D(cpu_data_w[30]),
        .G(u1_cpu_n_38),
        .GE(1'b1),
        .Q(PLtoPS_write_data[30]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \PLtoPS_write_data_reg[31] 
       (.CLR(1'b0),
        .D(cpu_data_w[31]),
        .G(u1_cpu_n_38),
        .GE(1'b1),
        .Q(PLtoPS_write_data[31]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \PLtoPS_write_data_reg[3] 
       (.CLR(1'b0),
        .D(cpu_data_w[3]),
        .G(u1_cpu_n_38),
        .GE(1'b1),
        .Q(PLtoPS_write_data[3]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \PLtoPS_write_data_reg[4] 
       (.CLR(1'b0),
        .D(cpu_data_w[4]),
        .G(u1_cpu_n_38),
        .GE(1'b1),
        .Q(PLtoPS_write_data[4]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \PLtoPS_write_data_reg[5] 
       (.CLR(1'b0),
        .D(cpu_data_w[5]),
        .G(u1_cpu_n_38),
        .GE(1'b1),
        .Q(PLtoPS_write_data[5]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \PLtoPS_write_data_reg[6] 
       (.CLR(1'b0),
        .D(cpu_data_w[6]),
        .G(u1_cpu_n_38),
        .GE(1'b1),
        .Q(PLtoPS_write_data[6]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \PLtoPS_write_data_reg[7] 
       (.CLR(1'b0),
        .D(cpu_data_w[7]),
        .G(u1_cpu_n_38),
        .GE(1'b1),
        .Q(PLtoPS_write_data[7]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \PLtoPS_write_data_reg[8] 
       (.CLR(1'b0),
        .D(cpu_data_w[8]),
        .G(u1_cpu_n_38),
        .GE(1'b1),
        .Q(PLtoPS_write_data[8]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \PLtoPS_write_data_reg[9] 
       (.CLR(1'b0),
        .D(cpu_data_w[9]),
        .G(u1_cpu_n_38),
        .GE(1'b1),
        .Q(PLtoPS_write_data[9]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_bbt_annotations_address_reg[10] 
       (.CLR(1'b0),
        .D(complete_address_next[10]),
        .G(u1_cpu_n_33),
        .GE(1'b1),
        .Q(bram_bbt_annotations_address[8]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_bbt_annotations_address_reg[11] 
       (.CLR(1'b0),
        .D(complete_address_next[11]),
        .G(u1_cpu_n_33),
        .GE(1'b1),
        .Q(bram_bbt_annotations_address[9]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_bbt_annotations_address_reg[12] 
       (.CLR(1'b0),
        .D(complete_address_next[12]),
        .G(u1_cpu_n_33),
        .GE(1'b1),
        .Q(bram_bbt_annotations_address[10]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_bbt_annotations_address_reg[13] 
       (.CLR(1'b0),
        .D(complete_address_next[13]),
        .G(u1_cpu_n_33),
        .GE(1'b1),
        .Q(bram_bbt_annotations_address[11]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_bbt_annotations_address_reg[14] 
       (.CLR(1'b0),
        .D(complete_address_next[14]),
        .G(u1_cpu_n_33),
        .GE(1'b1),
        .Q(bram_bbt_annotations_address[12]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_bbt_annotations_address_reg[15] 
       (.CLR(1'b0),
        .D(complete_address_next[15]),
        .G(u1_cpu_n_33),
        .GE(1'b1),
        .Q(bram_bbt_annotations_address[13]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_bbt_annotations_address_reg[16] 
       (.CLR(1'b0),
        .D(complete_address_next[16]),
        .G(u1_cpu_n_33),
        .GE(1'b1),
        .Q(bram_bbt_annotations_address[14]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_bbt_annotations_address_reg[17] 
       (.CLR(1'b0),
        .D(complete_address_next[17]),
        .G(u1_cpu_n_33),
        .GE(1'b1),
        .Q(bram_bbt_annotations_address[15]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_bbt_annotations_address_reg[18] 
       (.CLR(1'b0),
        .D(complete_address_next[18]),
        .G(u1_cpu_n_33),
        .GE(1'b1),
        .Q(bram_bbt_annotations_address[16]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_bbt_annotations_address_reg[19] 
       (.CLR(1'b0),
        .D(complete_address_next[19]),
        .G(u1_cpu_n_33),
        .GE(1'b1),
        .Q(bram_bbt_annotations_address[17]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_bbt_annotations_address_reg[2] 
       (.CLR(1'b0),
        .D(complete_address_next[2]),
        .G(u1_cpu_n_33),
        .GE(1'b1),
        .Q(bram_bbt_annotations_address[0]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_bbt_annotations_address_reg[3] 
       (.CLR(1'b0),
        .D(complete_address_next[3]),
        .G(u1_cpu_n_33),
        .GE(1'b1),
        .Q(bram_bbt_annotations_address[1]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_bbt_annotations_address_reg[4] 
       (.CLR(1'b0),
        .D(complete_address_next[4]),
        .G(u1_cpu_n_33),
        .GE(1'b1),
        .Q(bram_bbt_annotations_address[2]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_bbt_annotations_address_reg[5] 
       (.CLR(1'b0),
        .D(complete_address_next[5]),
        .G(u1_cpu_n_33),
        .GE(1'b1),
        .Q(bram_bbt_annotations_address[3]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_bbt_annotations_address_reg[6] 
       (.CLR(1'b0),
        .D(complete_address_next[6]),
        .G(u1_cpu_n_33),
        .GE(1'b1),
        .Q(bram_bbt_annotations_address[4]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_bbt_annotations_address_reg[7] 
       (.CLR(1'b0),
        .D(complete_address_next[7]),
        .G(u1_cpu_n_33),
        .GE(1'b1),
        .Q(bram_bbt_annotations_address[5]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_bbt_annotations_address_reg[8] 
       (.CLR(1'b0),
        .D(complete_address_next[8]),
        .G(u1_cpu_n_33),
        .GE(1'b1),
        .Q(bram_bbt_annotations_address[6]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_bbt_annotations_address_reg[9] 
       (.CLR(1'b0),
        .D(complete_address_next[9]),
        .G(u1_cpu_n_33),
        .GE(1'b1),
        .Q(bram_bbt_annotations_address[7]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_bbt_annotations_bytes_selection_reg[0] 
       (.CLR(1'b0),
        .D(byte_we_next[0]),
        .G(u1_cpu_n_33),
        .GE(1'b1),
        .Q(bram_bbt_annotations_bytes_selection[0]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_bbt_annotations_bytes_selection_reg[1] 
       (.CLR(1'b0),
        .D(byte_we_next[1]),
        .G(u1_cpu_n_33),
        .GE(1'b1),
        .Q(bram_bbt_annotations_bytes_selection[1]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_bbt_annotations_bytes_selection_reg[2] 
       (.CLR(1'b0),
        .D(byte_we_next[2]),
        .G(u1_cpu_n_33),
        .GE(1'b1),
        .Q(bram_bbt_annotations_bytes_selection[2]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_bbt_annotations_bytes_selection_reg[3] 
       (.CLR(1'b0),
        .D(byte_we_next[3]),
        .G(u1_cpu_n_33),
        .GE(1'b1),
        .Q(bram_bbt_annotations_bytes_selection[3]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_bbt_annotations_data_write_reg[0] 
       (.CLR(1'b0),
        .D(cpu_data_w[0]),
        .G(u1_cpu_n_33),
        .GE(1'b1),
        .Q(bram_bbt_annotations_data_write[0]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_bbt_annotations_data_write_reg[10] 
       (.CLR(1'b0),
        .D(cpu_data_w[10]),
        .G(u1_cpu_n_33),
        .GE(1'b1),
        .Q(bram_bbt_annotations_data_write[10]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_bbt_annotations_data_write_reg[11] 
       (.CLR(1'b0),
        .D(cpu_data_w[11]),
        .G(u1_cpu_n_33),
        .GE(1'b1),
        .Q(bram_bbt_annotations_data_write[11]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_bbt_annotations_data_write_reg[12] 
       (.CLR(1'b0),
        .D(cpu_data_w[12]),
        .G(u1_cpu_n_33),
        .GE(1'b1),
        .Q(bram_bbt_annotations_data_write[12]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_bbt_annotations_data_write_reg[13] 
       (.CLR(1'b0),
        .D(cpu_data_w[13]),
        .G(u1_cpu_n_33),
        .GE(1'b1),
        .Q(bram_bbt_annotations_data_write[13]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_bbt_annotations_data_write_reg[14] 
       (.CLR(1'b0),
        .D(cpu_data_w[14]),
        .G(u1_cpu_n_33),
        .GE(1'b1),
        .Q(bram_bbt_annotations_data_write[14]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_bbt_annotations_data_write_reg[15] 
       (.CLR(1'b0),
        .D(cpu_data_w[15]),
        .G(u1_cpu_n_33),
        .GE(1'b1),
        .Q(bram_bbt_annotations_data_write[15]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_bbt_annotations_data_write_reg[16] 
       (.CLR(1'b0),
        .D(cpu_data_w[16]),
        .G(u1_cpu_n_33),
        .GE(1'b1),
        .Q(bram_bbt_annotations_data_write[16]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_bbt_annotations_data_write_reg[17] 
       (.CLR(1'b0),
        .D(cpu_data_w[17]),
        .G(u1_cpu_n_33),
        .GE(1'b1),
        .Q(bram_bbt_annotations_data_write[17]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_bbt_annotations_data_write_reg[18] 
       (.CLR(1'b0),
        .D(cpu_data_w[18]),
        .G(u1_cpu_n_33),
        .GE(1'b1),
        .Q(bram_bbt_annotations_data_write[18]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_bbt_annotations_data_write_reg[19] 
       (.CLR(1'b0),
        .D(cpu_data_w[19]),
        .G(u1_cpu_n_33),
        .GE(1'b1),
        .Q(bram_bbt_annotations_data_write[19]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_bbt_annotations_data_write_reg[1] 
       (.CLR(1'b0),
        .D(cpu_data_w[1]),
        .G(u1_cpu_n_33),
        .GE(1'b1),
        .Q(bram_bbt_annotations_data_write[1]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_bbt_annotations_data_write_reg[20] 
       (.CLR(1'b0),
        .D(cpu_data_w[20]),
        .G(u1_cpu_n_33),
        .GE(1'b1),
        .Q(bram_bbt_annotations_data_write[20]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_bbt_annotations_data_write_reg[21] 
       (.CLR(1'b0),
        .D(cpu_data_w[21]),
        .G(u1_cpu_n_33),
        .GE(1'b1),
        .Q(bram_bbt_annotations_data_write[21]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_bbt_annotations_data_write_reg[22] 
       (.CLR(1'b0),
        .D(cpu_data_w[22]),
        .G(u1_cpu_n_33),
        .GE(1'b1),
        .Q(bram_bbt_annotations_data_write[22]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_bbt_annotations_data_write_reg[23] 
       (.CLR(1'b0),
        .D(cpu_data_w[23]),
        .G(u1_cpu_n_33),
        .GE(1'b1),
        .Q(bram_bbt_annotations_data_write[23]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_bbt_annotations_data_write_reg[24] 
       (.CLR(1'b0),
        .D(cpu_data_w[24]),
        .G(u1_cpu_n_33),
        .GE(1'b1),
        .Q(bram_bbt_annotations_data_write[24]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_bbt_annotations_data_write_reg[25] 
       (.CLR(1'b0),
        .D(cpu_data_w[25]),
        .G(u1_cpu_n_33),
        .GE(1'b1),
        .Q(bram_bbt_annotations_data_write[25]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_bbt_annotations_data_write_reg[26] 
       (.CLR(1'b0),
        .D(cpu_data_w[26]),
        .G(u1_cpu_n_33),
        .GE(1'b1),
        .Q(bram_bbt_annotations_data_write[26]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_bbt_annotations_data_write_reg[27] 
       (.CLR(1'b0),
        .D(cpu_data_w[27]),
        .G(u1_cpu_n_33),
        .GE(1'b1),
        .Q(bram_bbt_annotations_data_write[27]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_bbt_annotations_data_write_reg[28] 
       (.CLR(1'b0),
        .D(cpu_data_w[28]),
        .G(u1_cpu_n_33),
        .GE(1'b1),
        .Q(bram_bbt_annotations_data_write[28]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_bbt_annotations_data_write_reg[29] 
       (.CLR(1'b0),
        .D(cpu_data_w[29]),
        .G(u1_cpu_n_33),
        .GE(1'b1),
        .Q(bram_bbt_annotations_data_write[29]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_bbt_annotations_data_write_reg[2] 
       (.CLR(1'b0),
        .D(cpu_data_w[2]),
        .G(u1_cpu_n_33),
        .GE(1'b1),
        .Q(bram_bbt_annotations_data_write[2]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_bbt_annotations_data_write_reg[30] 
       (.CLR(1'b0),
        .D(cpu_data_w[30]),
        .G(u1_cpu_n_33),
        .GE(1'b1),
        .Q(bram_bbt_annotations_data_write[30]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_bbt_annotations_data_write_reg[31] 
       (.CLR(1'b0),
        .D(cpu_data_w[31]),
        .G(u1_cpu_n_33),
        .GE(1'b1),
        .Q(bram_bbt_annotations_data_write[31]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_bbt_annotations_data_write_reg[3] 
       (.CLR(1'b0),
        .D(cpu_data_w[3]),
        .G(u1_cpu_n_33),
        .GE(1'b1),
        .Q(bram_bbt_annotations_data_write[3]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_bbt_annotations_data_write_reg[4] 
       (.CLR(1'b0),
        .D(cpu_data_w[4]),
        .G(u1_cpu_n_33),
        .GE(1'b1),
        .Q(bram_bbt_annotations_data_write[4]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_bbt_annotations_data_write_reg[5] 
       (.CLR(1'b0),
        .D(cpu_data_w[5]),
        .G(u1_cpu_n_33),
        .GE(1'b1),
        .Q(bram_bbt_annotations_data_write[5]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_bbt_annotations_data_write_reg[6] 
       (.CLR(1'b0),
        .D(cpu_data_w[6]),
        .G(u1_cpu_n_33),
        .GE(1'b1),
        .Q(bram_bbt_annotations_data_write[6]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_bbt_annotations_data_write_reg[7] 
       (.CLR(1'b0),
        .D(cpu_data_w[7]),
        .G(u1_cpu_n_33),
        .GE(1'b1),
        .Q(bram_bbt_annotations_data_write[7]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_bbt_annotations_data_write_reg[8] 
       (.CLR(1'b0),
        .D(cpu_data_w[8]),
        .G(u1_cpu_n_33),
        .GE(1'b1),
        .Q(bram_bbt_annotations_data_write[8]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_bbt_annotations_data_write_reg[9] 
       (.CLR(1'b0),
        .D(cpu_data_w[9]),
        .G(u1_cpu_n_33),
        .GE(1'b1),
        .Q(bram_bbt_annotations_data_write[9]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    bram_bbt_annotations_en_reg
       (.CLR(1'b0),
        .D(u1_cpu_n_33),
        .G(u1_cpu_n_34),
        .GE(1'b1),
        .Q(bram_bbt_annotations_en));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_debug_address_reg[10] 
       (.CLR(1'b0),
        .D(complete_address_next[10]),
        .G(u1_cpu_n_36),
        .GE(1'b1),
        .Q(bram_debug_address[8]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_debug_address_reg[11] 
       (.CLR(1'b0),
        .D(complete_address_next[11]),
        .G(u1_cpu_n_36),
        .GE(1'b1),
        .Q(bram_debug_address[9]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_debug_address_reg[12] 
       (.CLR(1'b0),
        .D(complete_address_next[12]),
        .G(u1_cpu_n_36),
        .GE(1'b1),
        .Q(bram_debug_address[10]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_debug_address_reg[13] 
       (.CLR(1'b0),
        .D(complete_address_next[13]),
        .G(u1_cpu_n_36),
        .GE(1'b1),
        .Q(bram_debug_address[11]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_debug_address_reg[14] 
       (.CLR(1'b0),
        .D(complete_address_next[14]),
        .G(u1_cpu_n_36),
        .GE(1'b1),
        .Q(bram_debug_address[12]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_debug_address_reg[15] 
       (.CLR(1'b0),
        .D(complete_address_next[15]),
        .G(u1_cpu_n_36),
        .GE(1'b1),
        .Q(bram_debug_address[13]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_debug_address_reg[16] 
       (.CLR(1'b0),
        .D(complete_address_next[16]),
        .G(u1_cpu_n_36),
        .GE(1'b1),
        .Q(bram_debug_address[14]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_debug_address_reg[17] 
       (.CLR(1'b0),
        .D(complete_address_next[17]),
        .G(u1_cpu_n_36),
        .GE(1'b1),
        .Q(bram_debug_address[15]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_debug_address_reg[18] 
       (.CLR(1'b0),
        .D(complete_address_next[18]),
        .G(u1_cpu_n_36),
        .GE(1'b1),
        .Q(bram_debug_address[16]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_debug_address_reg[19] 
       (.CLR(1'b0),
        .D(complete_address_next[19]),
        .G(u1_cpu_n_36),
        .GE(1'b1),
        .Q(bram_debug_address[17]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_debug_address_reg[2] 
       (.CLR(1'b0),
        .D(complete_address_next[2]),
        .G(u1_cpu_n_36),
        .GE(1'b1),
        .Q(bram_debug_address[0]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_debug_address_reg[3] 
       (.CLR(1'b0),
        .D(complete_address_next[3]),
        .G(u1_cpu_n_36),
        .GE(1'b1),
        .Q(bram_debug_address[1]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_debug_address_reg[4] 
       (.CLR(1'b0),
        .D(complete_address_next[4]),
        .G(u1_cpu_n_36),
        .GE(1'b1),
        .Q(bram_debug_address[2]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_debug_address_reg[5] 
       (.CLR(1'b0),
        .D(complete_address_next[5]),
        .G(u1_cpu_n_36),
        .GE(1'b1),
        .Q(bram_debug_address[3]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_debug_address_reg[6] 
       (.CLR(1'b0),
        .D(complete_address_next[6]),
        .G(u1_cpu_n_36),
        .GE(1'b1),
        .Q(bram_debug_address[4]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_debug_address_reg[7] 
       (.CLR(1'b0),
        .D(complete_address_next[7]),
        .G(u1_cpu_n_36),
        .GE(1'b1),
        .Q(bram_debug_address[5]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_debug_address_reg[8] 
       (.CLR(1'b0),
        .D(complete_address_next[8]),
        .G(u1_cpu_n_36),
        .GE(1'b1),
        .Q(bram_debug_address[6]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_debug_address_reg[9] 
       (.CLR(1'b0),
        .D(complete_address_next[9]),
        .G(u1_cpu_n_36),
        .GE(1'b1),
        .Q(bram_debug_address[7]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_debug_bytes_selection_reg[0] 
       (.CLR(1'b0),
        .D(byte_we_next[0]),
        .G(u1_cpu_n_36),
        .GE(1'b1),
        .Q(bram_debug_bytes_selection[0]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_debug_bytes_selection_reg[1] 
       (.CLR(1'b0),
        .D(byte_we_next[1]),
        .G(u1_cpu_n_36),
        .GE(1'b1),
        .Q(bram_debug_bytes_selection[1]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_debug_bytes_selection_reg[2] 
       (.CLR(1'b0),
        .D(byte_we_next[2]),
        .G(u1_cpu_n_36),
        .GE(1'b1),
        .Q(bram_debug_bytes_selection[2]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_debug_bytes_selection_reg[3] 
       (.CLR(1'b0),
        .D(byte_we_next[3]),
        .G(u1_cpu_n_36),
        .GE(1'b1),
        .Q(bram_debug_bytes_selection[3]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_debug_data_write_reg[0] 
       (.CLR(1'b0),
        .D(cpu_data_w[0]),
        .G(u1_cpu_n_36),
        .GE(1'b1),
        .Q(bram_debug_data_write[0]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_debug_data_write_reg[10] 
       (.CLR(1'b0),
        .D(cpu_data_w[10]),
        .G(u1_cpu_n_36),
        .GE(1'b1),
        .Q(bram_debug_data_write[10]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_debug_data_write_reg[11] 
       (.CLR(1'b0),
        .D(cpu_data_w[11]),
        .G(u1_cpu_n_36),
        .GE(1'b1),
        .Q(bram_debug_data_write[11]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_debug_data_write_reg[12] 
       (.CLR(1'b0),
        .D(cpu_data_w[12]),
        .G(u1_cpu_n_36),
        .GE(1'b1),
        .Q(bram_debug_data_write[12]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_debug_data_write_reg[13] 
       (.CLR(1'b0),
        .D(cpu_data_w[13]),
        .G(u1_cpu_n_36),
        .GE(1'b1),
        .Q(bram_debug_data_write[13]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_debug_data_write_reg[14] 
       (.CLR(1'b0),
        .D(cpu_data_w[14]),
        .G(u1_cpu_n_36),
        .GE(1'b1),
        .Q(bram_debug_data_write[14]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_debug_data_write_reg[15] 
       (.CLR(1'b0),
        .D(cpu_data_w[15]),
        .G(u1_cpu_n_36),
        .GE(1'b1),
        .Q(bram_debug_data_write[15]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_debug_data_write_reg[16] 
       (.CLR(1'b0),
        .D(cpu_data_w[16]),
        .G(u1_cpu_n_36),
        .GE(1'b1),
        .Q(bram_debug_data_write[16]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_debug_data_write_reg[17] 
       (.CLR(1'b0),
        .D(cpu_data_w[17]),
        .G(u1_cpu_n_36),
        .GE(1'b1),
        .Q(bram_debug_data_write[17]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_debug_data_write_reg[18] 
       (.CLR(1'b0),
        .D(cpu_data_w[18]),
        .G(u1_cpu_n_36),
        .GE(1'b1),
        .Q(bram_debug_data_write[18]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_debug_data_write_reg[19] 
       (.CLR(1'b0),
        .D(cpu_data_w[19]),
        .G(u1_cpu_n_36),
        .GE(1'b1),
        .Q(bram_debug_data_write[19]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_debug_data_write_reg[1] 
       (.CLR(1'b0),
        .D(cpu_data_w[1]),
        .G(u1_cpu_n_36),
        .GE(1'b1),
        .Q(bram_debug_data_write[1]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_debug_data_write_reg[20] 
       (.CLR(1'b0),
        .D(cpu_data_w[20]),
        .G(u1_cpu_n_36),
        .GE(1'b1),
        .Q(bram_debug_data_write[20]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_debug_data_write_reg[21] 
       (.CLR(1'b0),
        .D(cpu_data_w[21]),
        .G(u1_cpu_n_36),
        .GE(1'b1),
        .Q(bram_debug_data_write[21]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_debug_data_write_reg[22] 
       (.CLR(1'b0),
        .D(cpu_data_w[22]),
        .G(u1_cpu_n_36),
        .GE(1'b1),
        .Q(bram_debug_data_write[22]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_debug_data_write_reg[23] 
       (.CLR(1'b0),
        .D(cpu_data_w[23]),
        .G(u1_cpu_n_36),
        .GE(1'b1),
        .Q(bram_debug_data_write[23]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_debug_data_write_reg[24] 
       (.CLR(1'b0),
        .D(cpu_data_w[24]),
        .G(u1_cpu_n_36),
        .GE(1'b1),
        .Q(bram_debug_data_write[24]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_debug_data_write_reg[25] 
       (.CLR(1'b0),
        .D(cpu_data_w[25]),
        .G(u1_cpu_n_36),
        .GE(1'b1),
        .Q(bram_debug_data_write[25]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_debug_data_write_reg[26] 
       (.CLR(1'b0),
        .D(cpu_data_w[26]),
        .G(u1_cpu_n_36),
        .GE(1'b1),
        .Q(bram_debug_data_write[26]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_debug_data_write_reg[27] 
       (.CLR(1'b0),
        .D(cpu_data_w[27]),
        .G(u1_cpu_n_36),
        .GE(1'b1),
        .Q(bram_debug_data_write[27]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_debug_data_write_reg[28] 
       (.CLR(1'b0),
        .D(cpu_data_w[28]),
        .G(u1_cpu_n_36),
        .GE(1'b1),
        .Q(bram_debug_data_write[28]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_debug_data_write_reg[29] 
       (.CLR(1'b0),
        .D(cpu_data_w[29]),
        .G(u1_cpu_n_36),
        .GE(1'b1),
        .Q(bram_debug_data_write[29]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_debug_data_write_reg[2] 
       (.CLR(1'b0),
        .D(cpu_data_w[2]),
        .G(u1_cpu_n_36),
        .GE(1'b1),
        .Q(bram_debug_data_write[2]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_debug_data_write_reg[30] 
       (.CLR(1'b0),
        .D(cpu_data_w[30]),
        .G(u1_cpu_n_36),
        .GE(1'b1),
        .Q(bram_debug_data_write[30]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_debug_data_write_reg[31] 
       (.CLR(1'b0),
        .D(cpu_data_w[31]),
        .G(u1_cpu_n_36),
        .GE(1'b1),
        .Q(bram_debug_data_write[31]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_debug_data_write_reg[3] 
       (.CLR(1'b0),
        .D(cpu_data_w[3]),
        .G(u1_cpu_n_36),
        .GE(1'b1),
        .Q(bram_debug_data_write[3]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_debug_data_write_reg[4] 
       (.CLR(1'b0),
        .D(cpu_data_w[4]),
        .G(u1_cpu_n_36),
        .GE(1'b1),
        .Q(bram_debug_data_write[4]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_debug_data_write_reg[5] 
       (.CLR(1'b0),
        .D(cpu_data_w[5]),
        .G(u1_cpu_n_36),
        .GE(1'b1),
        .Q(bram_debug_data_write[5]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_debug_data_write_reg[6] 
       (.CLR(1'b0),
        .D(cpu_data_w[6]),
        .G(u1_cpu_n_36),
        .GE(1'b1),
        .Q(bram_debug_data_write[6]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_debug_data_write_reg[7] 
       (.CLR(1'b0),
        .D(cpu_data_w[7]),
        .G(u1_cpu_n_36),
        .GE(1'b1),
        .Q(bram_debug_data_write[7]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_debug_data_write_reg[8] 
       (.CLR(1'b0),
        .D(cpu_data_w[8]),
        .G(u1_cpu_n_36),
        .GE(1'b1),
        .Q(bram_debug_data_write[8]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_debug_data_write_reg[9] 
       (.CLR(1'b0),
        .D(cpu_data_w[9]),
        .G(u1_cpu_n_36),
        .GE(1'b1),
        .Q(bram_debug_data_write[9]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    bram_debug_en_reg
       (.CLR(1'b0),
        .D(u1_cpu_n_30),
        .G(u1_cpu_n_34),
        .GE(1'b1),
        .Q(bram_debug_en));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_firmware_code_address_reg[10] 
       (.CLR(1'b0),
        .D(complete_address_next[10]),
        .G(u1_cpu_n_32),
        .GE(1'b1),
        .Q(bram_firmware_code_address[8]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_firmware_code_address_reg[11] 
       (.CLR(1'b0),
        .D(complete_address_next[11]),
        .G(u1_cpu_n_32),
        .GE(1'b1),
        .Q(bram_firmware_code_address[9]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_firmware_code_address_reg[12] 
       (.CLR(1'b0),
        .D(complete_address_next[12]),
        .G(u1_cpu_n_32),
        .GE(1'b1),
        .Q(bram_firmware_code_address[10]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_firmware_code_address_reg[13] 
       (.CLR(1'b0),
        .D(complete_address_next[13]),
        .G(u1_cpu_n_32),
        .GE(1'b1),
        .Q(bram_firmware_code_address[11]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_firmware_code_address_reg[14] 
       (.CLR(1'b0),
        .D(complete_address_next[14]),
        .G(u1_cpu_n_32),
        .GE(1'b1),
        .Q(bram_firmware_code_address[12]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_firmware_code_address_reg[15] 
       (.CLR(1'b0),
        .D(complete_address_next[15]),
        .G(u1_cpu_n_32),
        .GE(1'b1),
        .Q(bram_firmware_code_address[13]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_firmware_code_address_reg[16] 
       (.CLR(1'b0),
        .D(complete_address_next[16]),
        .G(u1_cpu_n_32),
        .GE(1'b1),
        .Q(bram_firmware_code_address[14]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_firmware_code_address_reg[17] 
       (.CLR(1'b0),
        .D(complete_address_next[17]),
        .G(u1_cpu_n_32),
        .GE(1'b1),
        .Q(bram_firmware_code_address[15]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_firmware_code_address_reg[18] 
       (.CLR(1'b0),
        .D(complete_address_next[18]),
        .G(u1_cpu_n_32),
        .GE(1'b1),
        .Q(bram_firmware_code_address[16]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_firmware_code_address_reg[19] 
       (.CLR(1'b0),
        .D(complete_address_next[19]),
        .G(u1_cpu_n_32),
        .GE(1'b1),
        .Q(bram_firmware_code_address[17]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_firmware_code_address_reg[2] 
       (.CLR(1'b0),
        .D(complete_address_next[2]),
        .G(u1_cpu_n_32),
        .GE(1'b1),
        .Q(bram_firmware_code_address[0]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_firmware_code_address_reg[3] 
       (.CLR(1'b0),
        .D(complete_address_next[3]),
        .G(u1_cpu_n_32),
        .GE(1'b1),
        .Q(bram_firmware_code_address[1]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_firmware_code_address_reg[4] 
       (.CLR(1'b0),
        .D(complete_address_next[4]),
        .G(u1_cpu_n_32),
        .GE(1'b1),
        .Q(bram_firmware_code_address[2]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_firmware_code_address_reg[5] 
       (.CLR(1'b0),
        .D(complete_address_next[5]),
        .G(u1_cpu_n_32),
        .GE(1'b1),
        .Q(bram_firmware_code_address[3]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_firmware_code_address_reg[6] 
       (.CLR(1'b0),
        .D(complete_address_next[6]),
        .G(u1_cpu_n_32),
        .GE(1'b1),
        .Q(bram_firmware_code_address[4]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_firmware_code_address_reg[7] 
       (.CLR(1'b0),
        .D(complete_address_next[7]),
        .G(u1_cpu_n_32),
        .GE(1'b1),
        .Q(bram_firmware_code_address[5]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_firmware_code_address_reg[8] 
       (.CLR(1'b0),
        .D(complete_address_next[8]),
        .G(u1_cpu_n_32),
        .GE(1'b1),
        .Q(bram_firmware_code_address[6]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_firmware_code_address_reg[9] 
       (.CLR(1'b0),
        .D(complete_address_next[9]),
        .G(u1_cpu_n_32),
        .GE(1'b1),
        .Q(bram_firmware_code_address[7]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_firmware_code_bytes_selection_reg[0] 
       (.CLR(1'b0),
        .D(byte_we_next[0]),
        .G(u1_cpu_n_32),
        .GE(1'b1),
        .Q(bram_firmware_code_bytes_selection[0]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_firmware_code_bytes_selection_reg[1] 
       (.CLR(1'b0),
        .D(byte_we_next[1]),
        .G(u1_cpu_n_32),
        .GE(1'b1),
        .Q(bram_firmware_code_bytes_selection[1]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_firmware_code_bytes_selection_reg[2] 
       (.CLR(1'b0),
        .D(byte_we_next[2]),
        .G(u1_cpu_n_32),
        .GE(1'b1),
        .Q(bram_firmware_code_bytes_selection[2]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_firmware_code_bytes_selection_reg[3] 
       (.CLR(1'b0),
        .D(byte_we_next[3]),
        .G(u1_cpu_n_32),
        .GE(1'b1),
        .Q(bram_firmware_code_bytes_selection[3]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_firmware_code_data_write_reg[0] 
       (.CLR(1'b0),
        .D(cpu_data_w[0]),
        .G(u1_cpu_n_32),
        .GE(1'b1),
        .Q(bram_firmware_code_data_write[0]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_firmware_code_data_write_reg[10] 
       (.CLR(1'b0),
        .D(cpu_data_w[10]),
        .G(u1_cpu_n_32),
        .GE(1'b1),
        .Q(bram_firmware_code_data_write[10]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_firmware_code_data_write_reg[11] 
       (.CLR(1'b0),
        .D(cpu_data_w[11]),
        .G(u1_cpu_n_32),
        .GE(1'b1),
        .Q(bram_firmware_code_data_write[11]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_firmware_code_data_write_reg[12] 
       (.CLR(1'b0),
        .D(cpu_data_w[12]),
        .G(u1_cpu_n_32),
        .GE(1'b1),
        .Q(bram_firmware_code_data_write[12]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_firmware_code_data_write_reg[13] 
       (.CLR(1'b0),
        .D(cpu_data_w[13]),
        .G(u1_cpu_n_32),
        .GE(1'b1),
        .Q(bram_firmware_code_data_write[13]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_firmware_code_data_write_reg[14] 
       (.CLR(1'b0),
        .D(cpu_data_w[14]),
        .G(u1_cpu_n_32),
        .GE(1'b1),
        .Q(bram_firmware_code_data_write[14]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_firmware_code_data_write_reg[15] 
       (.CLR(1'b0),
        .D(cpu_data_w[15]),
        .G(u1_cpu_n_32),
        .GE(1'b1),
        .Q(bram_firmware_code_data_write[15]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_firmware_code_data_write_reg[16] 
       (.CLR(1'b0),
        .D(cpu_data_w[16]),
        .G(u1_cpu_n_32),
        .GE(1'b1),
        .Q(bram_firmware_code_data_write[16]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_firmware_code_data_write_reg[17] 
       (.CLR(1'b0),
        .D(cpu_data_w[17]),
        .G(u1_cpu_n_32),
        .GE(1'b1),
        .Q(bram_firmware_code_data_write[17]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_firmware_code_data_write_reg[18] 
       (.CLR(1'b0),
        .D(cpu_data_w[18]),
        .G(u1_cpu_n_32),
        .GE(1'b1),
        .Q(bram_firmware_code_data_write[18]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_firmware_code_data_write_reg[19] 
       (.CLR(1'b0),
        .D(cpu_data_w[19]),
        .G(u1_cpu_n_32),
        .GE(1'b1),
        .Q(bram_firmware_code_data_write[19]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_firmware_code_data_write_reg[1] 
       (.CLR(1'b0),
        .D(cpu_data_w[1]),
        .G(u1_cpu_n_32),
        .GE(1'b1),
        .Q(bram_firmware_code_data_write[1]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_firmware_code_data_write_reg[20] 
       (.CLR(1'b0),
        .D(cpu_data_w[20]),
        .G(u1_cpu_n_32),
        .GE(1'b1),
        .Q(bram_firmware_code_data_write[20]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_firmware_code_data_write_reg[21] 
       (.CLR(1'b0),
        .D(cpu_data_w[21]),
        .G(u1_cpu_n_32),
        .GE(1'b1),
        .Q(bram_firmware_code_data_write[21]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_firmware_code_data_write_reg[22] 
       (.CLR(1'b0),
        .D(cpu_data_w[22]),
        .G(u1_cpu_n_32),
        .GE(1'b1),
        .Q(bram_firmware_code_data_write[22]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_firmware_code_data_write_reg[23] 
       (.CLR(1'b0),
        .D(cpu_data_w[23]),
        .G(u1_cpu_n_32),
        .GE(1'b1),
        .Q(bram_firmware_code_data_write[23]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_firmware_code_data_write_reg[24] 
       (.CLR(1'b0),
        .D(cpu_data_w[24]),
        .G(u1_cpu_n_32),
        .GE(1'b1),
        .Q(bram_firmware_code_data_write[24]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_firmware_code_data_write_reg[25] 
       (.CLR(1'b0),
        .D(cpu_data_w[25]),
        .G(u1_cpu_n_32),
        .GE(1'b1),
        .Q(bram_firmware_code_data_write[25]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_firmware_code_data_write_reg[26] 
       (.CLR(1'b0),
        .D(cpu_data_w[26]),
        .G(u1_cpu_n_32),
        .GE(1'b1),
        .Q(bram_firmware_code_data_write[26]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_firmware_code_data_write_reg[27] 
       (.CLR(1'b0),
        .D(cpu_data_w[27]),
        .G(u1_cpu_n_32),
        .GE(1'b1),
        .Q(bram_firmware_code_data_write[27]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_firmware_code_data_write_reg[28] 
       (.CLR(1'b0),
        .D(cpu_data_w[28]),
        .G(u1_cpu_n_32),
        .GE(1'b1),
        .Q(bram_firmware_code_data_write[28]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_firmware_code_data_write_reg[29] 
       (.CLR(1'b0),
        .D(cpu_data_w[29]),
        .G(u1_cpu_n_32),
        .GE(1'b1),
        .Q(bram_firmware_code_data_write[29]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_firmware_code_data_write_reg[2] 
       (.CLR(1'b0),
        .D(cpu_data_w[2]),
        .G(u1_cpu_n_32),
        .GE(1'b1),
        .Q(bram_firmware_code_data_write[2]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_firmware_code_data_write_reg[30] 
       (.CLR(1'b0),
        .D(cpu_data_w[30]),
        .G(u1_cpu_n_32),
        .GE(1'b1),
        .Q(bram_firmware_code_data_write[30]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_firmware_code_data_write_reg[31] 
       (.CLR(1'b0),
        .D(cpu_data_w[31]),
        .G(u1_cpu_n_32),
        .GE(1'b1),
        .Q(bram_firmware_code_data_write[31]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_firmware_code_data_write_reg[3] 
       (.CLR(1'b0),
        .D(cpu_data_w[3]),
        .G(u1_cpu_n_32),
        .GE(1'b1),
        .Q(bram_firmware_code_data_write[3]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_firmware_code_data_write_reg[4] 
       (.CLR(1'b0),
        .D(cpu_data_w[4]),
        .G(u1_cpu_n_32),
        .GE(1'b1),
        .Q(bram_firmware_code_data_write[4]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_firmware_code_data_write_reg[5] 
       (.CLR(1'b0),
        .D(cpu_data_w[5]),
        .G(u1_cpu_n_32),
        .GE(1'b1),
        .Q(bram_firmware_code_data_write[5]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_firmware_code_data_write_reg[6] 
       (.CLR(1'b0),
        .D(cpu_data_w[6]),
        .G(u1_cpu_n_32),
        .GE(1'b1),
        .Q(bram_firmware_code_data_write[6]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_firmware_code_data_write_reg[7] 
       (.CLR(1'b0),
        .D(cpu_data_w[7]),
        .G(u1_cpu_n_32),
        .GE(1'b1),
        .Q(bram_firmware_code_data_write[7]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_firmware_code_data_write_reg[8] 
       (.CLR(1'b0),
        .D(cpu_data_w[8]),
        .G(u1_cpu_n_32),
        .GE(1'b1),
        .Q(bram_firmware_code_data_write[8]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_firmware_code_data_write_reg[9] 
       (.CLR(1'b0),
        .D(cpu_data_w[9]),
        .G(u1_cpu_n_32),
        .GE(1'b1),
        .Q(bram_firmware_code_data_write[9]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    bram_firmware_code_en_reg
       (.CLR(1'b0),
        .D(u1_cpu_n_32),
        .G(u1_cpu_n_34),
        .GE(1'b1),
        .Q(bram_firmware_code_en));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_sec_policy_config_address_reg[10] 
       (.CLR(1'b0),
        .D(complete_address_next[10]),
        .G(u1_cpu_n_43),
        .GE(1'b1),
        .Q(bram_sec_policy_config_address[8]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_sec_policy_config_address_reg[11] 
       (.CLR(1'b0),
        .D(complete_address_next[11]),
        .G(u1_cpu_n_43),
        .GE(1'b1),
        .Q(bram_sec_policy_config_address[9]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_sec_policy_config_address_reg[12] 
       (.CLR(1'b0),
        .D(complete_address_next[12]),
        .G(u1_cpu_n_43),
        .GE(1'b1),
        .Q(bram_sec_policy_config_address[10]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_sec_policy_config_address_reg[13] 
       (.CLR(1'b0),
        .D(complete_address_next[13]),
        .G(u1_cpu_n_43),
        .GE(1'b1),
        .Q(bram_sec_policy_config_address[11]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_sec_policy_config_address_reg[14] 
       (.CLR(1'b0),
        .D(complete_address_next[14]),
        .G(u1_cpu_n_43),
        .GE(1'b1),
        .Q(bram_sec_policy_config_address[12]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_sec_policy_config_address_reg[15] 
       (.CLR(1'b0),
        .D(complete_address_next[15]),
        .G(u1_cpu_n_43),
        .GE(1'b1),
        .Q(bram_sec_policy_config_address[13]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_sec_policy_config_address_reg[16] 
       (.CLR(1'b0),
        .D(complete_address_next[16]),
        .G(u1_cpu_n_43),
        .GE(1'b1),
        .Q(bram_sec_policy_config_address[14]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_sec_policy_config_address_reg[17] 
       (.CLR(1'b0),
        .D(complete_address_next[17]),
        .G(u1_cpu_n_43),
        .GE(1'b1),
        .Q(bram_sec_policy_config_address[15]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_sec_policy_config_address_reg[18] 
       (.CLR(1'b0),
        .D(complete_address_next[18]),
        .G(u1_cpu_n_43),
        .GE(1'b1),
        .Q(bram_sec_policy_config_address[16]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_sec_policy_config_address_reg[19] 
       (.CLR(1'b0),
        .D(complete_address_next[19]),
        .G(u1_cpu_n_43),
        .GE(1'b1),
        .Q(bram_sec_policy_config_address[17]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_sec_policy_config_address_reg[2] 
       (.CLR(1'b0),
        .D(complete_address_next[2]),
        .G(u1_cpu_n_43),
        .GE(1'b1),
        .Q(bram_sec_policy_config_address[0]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_sec_policy_config_address_reg[3] 
       (.CLR(1'b0),
        .D(complete_address_next[3]),
        .G(u1_cpu_n_43),
        .GE(1'b1),
        .Q(bram_sec_policy_config_address[1]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_sec_policy_config_address_reg[4] 
       (.CLR(1'b0),
        .D(complete_address_next[4]),
        .G(u1_cpu_n_43),
        .GE(1'b1),
        .Q(bram_sec_policy_config_address[2]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_sec_policy_config_address_reg[5] 
       (.CLR(1'b0),
        .D(complete_address_next[5]),
        .G(u1_cpu_n_43),
        .GE(1'b1),
        .Q(bram_sec_policy_config_address[3]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_sec_policy_config_address_reg[6] 
       (.CLR(1'b0),
        .D(complete_address_next[6]),
        .G(u1_cpu_n_43),
        .GE(1'b1),
        .Q(bram_sec_policy_config_address[4]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_sec_policy_config_address_reg[7] 
       (.CLR(1'b0),
        .D(complete_address_next[7]),
        .G(u1_cpu_n_43),
        .GE(1'b1),
        .Q(bram_sec_policy_config_address[5]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_sec_policy_config_address_reg[8] 
       (.CLR(1'b0),
        .D(complete_address_next[8]),
        .G(u1_cpu_n_43),
        .GE(1'b1),
        .Q(bram_sec_policy_config_address[6]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_sec_policy_config_address_reg[9] 
       (.CLR(1'b0),
        .D(complete_address_next[9]),
        .G(u1_cpu_n_43),
        .GE(1'b1),
        .Q(bram_sec_policy_config_address[7]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_sec_policy_config_bytes_selection_reg[0] 
       (.CLR(1'b0),
        .D(byte_we_next[0]),
        .G(u1_cpu_n_43),
        .GE(1'b1),
        .Q(bram_sec_policy_config_bytes_selection[0]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_sec_policy_config_bytes_selection_reg[1] 
       (.CLR(1'b0),
        .D(byte_we_next[1]),
        .G(u1_cpu_n_43),
        .GE(1'b1),
        .Q(bram_sec_policy_config_bytes_selection[1]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_sec_policy_config_bytes_selection_reg[2] 
       (.CLR(1'b0),
        .D(byte_we_next[2]),
        .G(u1_cpu_n_43),
        .GE(1'b1),
        .Q(bram_sec_policy_config_bytes_selection[2]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_sec_policy_config_bytes_selection_reg[3] 
       (.CLR(1'b0),
        .D(byte_we_next[3]),
        .G(u1_cpu_n_43),
        .GE(1'b1),
        .Q(bram_sec_policy_config_bytes_selection[3]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_sec_policy_config_data_write_reg[0] 
       (.CLR(1'b0),
        .D(cpu_data_w[0]),
        .G(u1_cpu_n_43),
        .GE(1'b1),
        .Q(bram_sec_policy_config_data_write[0]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_sec_policy_config_data_write_reg[10] 
       (.CLR(1'b0),
        .D(cpu_data_w[10]),
        .G(u1_cpu_n_43),
        .GE(1'b1),
        .Q(bram_sec_policy_config_data_write[10]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_sec_policy_config_data_write_reg[11] 
       (.CLR(1'b0),
        .D(cpu_data_w[11]),
        .G(u1_cpu_n_43),
        .GE(1'b1),
        .Q(bram_sec_policy_config_data_write[11]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_sec_policy_config_data_write_reg[12] 
       (.CLR(1'b0),
        .D(cpu_data_w[12]),
        .G(u1_cpu_n_43),
        .GE(1'b1),
        .Q(bram_sec_policy_config_data_write[12]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_sec_policy_config_data_write_reg[13] 
       (.CLR(1'b0),
        .D(cpu_data_w[13]),
        .G(u1_cpu_n_43),
        .GE(1'b1),
        .Q(bram_sec_policy_config_data_write[13]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_sec_policy_config_data_write_reg[14] 
       (.CLR(1'b0),
        .D(cpu_data_w[14]),
        .G(u1_cpu_n_43),
        .GE(1'b1),
        .Q(bram_sec_policy_config_data_write[14]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_sec_policy_config_data_write_reg[15] 
       (.CLR(1'b0),
        .D(cpu_data_w[15]),
        .G(u1_cpu_n_43),
        .GE(1'b1),
        .Q(bram_sec_policy_config_data_write[15]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_sec_policy_config_data_write_reg[16] 
       (.CLR(1'b0),
        .D(cpu_data_w[16]),
        .G(u1_cpu_n_43),
        .GE(1'b1),
        .Q(bram_sec_policy_config_data_write[16]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_sec_policy_config_data_write_reg[17] 
       (.CLR(1'b0),
        .D(cpu_data_w[17]),
        .G(u1_cpu_n_43),
        .GE(1'b1),
        .Q(bram_sec_policy_config_data_write[17]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_sec_policy_config_data_write_reg[18] 
       (.CLR(1'b0),
        .D(cpu_data_w[18]),
        .G(u1_cpu_n_43),
        .GE(1'b1),
        .Q(bram_sec_policy_config_data_write[18]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_sec_policy_config_data_write_reg[19] 
       (.CLR(1'b0),
        .D(cpu_data_w[19]),
        .G(u1_cpu_n_43),
        .GE(1'b1),
        .Q(bram_sec_policy_config_data_write[19]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_sec_policy_config_data_write_reg[1] 
       (.CLR(1'b0),
        .D(cpu_data_w[1]),
        .G(u1_cpu_n_43),
        .GE(1'b1),
        .Q(bram_sec_policy_config_data_write[1]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_sec_policy_config_data_write_reg[20] 
       (.CLR(1'b0),
        .D(cpu_data_w[20]),
        .G(u1_cpu_n_43),
        .GE(1'b1),
        .Q(bram_sec_policy_config_data_write[20]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_sec_policy_config_data_write_reg[21] 
       (.CLR(1'b0),
        .D(cpu_data_w[21]),
        .G(u1_cpu_n_43),
        .GE(1'b1),
        .Q(bram_sec_policy_config_data_write[21]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_sec_policy_config_data_write_reg[22] 
       (.CLR(1'b0),
        .D(cpu_data_w[22]),
        .G(u1_cpu_n_43),
        .GE(1'b1),
        .Q(bram_sec_policy_config_data_write[22]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_sec_policy_config_data_write_reg[23] 
       (.CLR(1'b0),
        .D(cpu_data_w[23]),
        .G(u1_cpu_n_43),
        .GE(1'b1),
        .Q(bram_sec_policy_config_data_write[23]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_sec_policy_config_data_write_reg[24] 
       (.CLR(1'b0),
        .D(cpu_data_w[24]),
        .G(u1_cpu_n_43),
        .GE(1'b1),
        .Q(bram_sec_policy_config_data_write[24]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_sec_policy_config_data_write_reg[25] 
       (.CLR(1'b0),
        .D(cpu_data_w[25]),
        .G(u1_cpu_n_43),
        .GE(1'b1),
        .Q(bram_sec_policy_config_data_write[25]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_sec_policy_config_data_write_reg[26] 
       (.CLR(1'b0),
        .D(cpu_data_w[26]),
        .G(u1_cpu_n_43),
        .GE(1'b1),
        .Q(bram_sec_policy_config_data_write[26]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_sec_policy_config_data_write_reg[27] 
       (.CLR(1'b0),
        .D(cpu_data_w[27]),
        .G(u1_cpu_n_43),
        .GE(1'b1),
        .Q(bram_sec_policy_config_data_write[27]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_sec_policy_config_data_write_reg[28] 
       (.CLR(1'b0),
        .D(cpu_data_w[28]),
        .G(u1_cpu_n_43),
        .GE(1'b1),
        .Q(bram_sec_policy_config_data_write[28]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_sec_policy_config_data_write_reg[29] 
       (.CLR(1'b0),
        .D(cpu_data_w[29]),
        .G(u1_cpu_n_43),
        .GE(1'b1),
        .Q(bram_sec_policy_config_data_write[29]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_sec_policy_config_data_write_reg[2] 
       (.CLR(1'b0),
        .D(cpu_data_w[2]),
        .G(u1_cpu_n_43),
        .GE(1'b1),
        .Q(bram_sec_policy_config_data_write[2]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_sec_policy_config_data_write_reg[30] 
       (.CLR(1'b0),
        .D(cpu_data_w[30]),
        .G(u1_cpu_n_43),
        .GE(1'b1),
        .Q(bram_sec_policy_config_data_write[30]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_sec_policy_config_data_write_reg[31] 
       (.CLR(1'b0),
        .D(cpu_data_w[31]),
        .G(u1_cpu_n_43),
        .GE(1'b1),
        .Q(bram_sec_policy_config_data_write[31]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_sec_policy_config_data_write_reg[3] 
       (.CLR(1'b0),
        .D(cpu_data_w[3]),
        .G(u1_cpu_n_43),
        .GE(1'b1),
        .Q(bram_sec_policy_config_data_write[3]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_sec_policy_config_data_write_reg[4] 
       (.CLR(1'b0),
        .D(cpu_data_w[4]),
        .G(u1_cpu_n_43),
        .GE(1'b1),
        .Q(bram_sec_policy_config_data_write[4]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_sec_policy_config_data_write_reg[5] 
       (.CLR(1'b0),
        .D(cpu_data_w[5]),
        .G(u1_cpu_n_43),
        .GE(1'b1),
        .Q(bram_sec_policy_config_data_write[5]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_sec_policy_config_data_write_reg[6] 
       (.CLR(1'b0),
        .D(cpu_data_w[6]),
        .G(u1_cpu_n_43),
        .GE(1'b1),
        .Q(bram_sec_policy_config_data_write[6]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_sec_policy_config_data_write_reg[7] 
       (.CLR(1'b0),
        .D(cpu_data_w[7]),
        .G(u1_cpu_n_43),
        .GE(1'b1),
        .Q(bram_sec_policy_config_data_write[7]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_sec_policy_config_data_write_reg[8] 
       (.CLR(1'b0),
        .D(cpu_data_w[8]),
        .G(u1_cpu_n_43),
        .GE(1'b1),
        .Q(bram_sec_policy_config_data_write[8]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bram_sec_policy_config_data_write_reg[9] 
       (.CLR(1'b0),
        .D(cpu_data_w[9]),
        .G(u1_cpu_n_43),
        .GE(1'b1),
        .Q(bram_sec_policy_config_data_write[9]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    bram_sec_policy_config_en_reg
       (.CLR(1'b0),
        .D(u1_cpu_n_31),
        .G(u1_cpu_n_34),
        .GE(1'b1),
        .Q(bram_sec_policy_config_en));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    fifo_kernel_to_monitor_en_reg
       (.CLR(1'b0),
        .D(u1_cpu_n_47),
        .G(u1_cpu_n_34),
        .GE(1'b1),
        .Q(fifo_kernel_to_monitor_en));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \fifo_monitor_to_kernel_data_write_reg[0] 
       (.CLR(1'b0),
        .D(cpu_data_w[0]),
        .G(u1_cpu_n_35),
        .GE(1'b1),
        .Q(fifo_monitor_to_kernel_data_write[0]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \fifo_monitor_to_kernel_data_write_reg[10] 
       (.CLR(1'b0),
        .D(cpu_data_w[10]),
        .G(u1_cpu_n_35),
        .GE(1'b1),
        .Q(fifo_monitor_to_kernel_data_write[10]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \fifo_monitor_to_kernel_data_write_reg[11] 
       (.CLR(1'b0),
        .D(cpu_data_w[11]),
        .G(u1_cpu_n_35),
        .GE(1'b1),
        .Q(fifo_monitor_to_kernel_data_write[11]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \fifo_monitor_to_kernel_data_write_reg[12] 
       (.CLR(1'b0),
        .D(cpu_data_w[12]),
        .G(u1_cpu_n_35),
        .GE(1'b1),
        .Q(fifo_monitor_to_kernel_data_write[12]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \fifo_monitor_to_kernel_data_write_reg[13] 
       (.CLR(1'b0),
        .D(cpu_data_w[13]),
        .G(u1_cpu_n_35),
        .GE(1'b1),
        .Q(fifo_monitor_to_kernel_data_write[13]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \fifo_monitor_to_kernel_data_write_reg[14] 
       (.CLR(1'b0),
        .D(cpu_data_w[14]),
        .G(u1_cpu_n_35),
        .GE(1'b1),
        .Q(fifo_monitor_to_kernel_data_write[14]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \fifo_monitor_to_kernel_data_write_reg[15] 
       (.CLR(1'b0),
        .D(cpu_data_w[15]),
        .G(u1_cpu_n_35),
        .GE(1'b1),
        .Q(fifo_monitor_to_kernel_data_write[15]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \fifo_monitor_to_kernel_data_write_reg[16] 
       (.CLR(1'b0),
        .D(cpu_data_w[16]),
        .G(u1_cpu_n_35),
        .GE(1'b1),
        .Q(fifo_monitor_to_kernel_data_write[16]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \fifo_monitor_to_kernel_data_write_reg[17] 
       (.CLR(1'b0),
        .D(cpu_data_w[17]),
        .G(u1_cpu_n_35),
        .GE(1'b1),
        .Q(fifo_monitor_to_kernel_data_write[17]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \fifo_monitor_to_kernel_data_write_reg[18] 
       (.CLR(1'b0),
        .D(cpu_data_w[18]),
        .G(u1_cpu_n_35),
        .GE(1'b1),
        .Q(fifo_monitor_to_kernel_data_write[18]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \fifo_monitor_to_kernel_data_write_reg[19] 
       (.CLR(1'b0),
        .D(cpu_data_w[19]),
        .G(u1_cpu_n_35),
        .GE(1'b1),
        .Q(fifo_monitor_to_kernel_data_write[19]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \fifo_monitor_to_kernel_data_write_reg[1] 
       (.CLR(1'b0),
        .D(cpu_data_w[1]),
        .G(u1_cpu_n_35),
        .GE(1'b1),
        .Q(fifo_monitor_to_kernel_data_write[1]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \fifo_monitor_to_kernel_data_write_reg[20] 
       (.CLR(1'b0),
        .D(cpu_data_w[20]),
        .G(u1_cpu_n_35),
        .GE(1'b1),
        .Q(fifo_monitor_to_kernel_data_write[20]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \fifo_monitor_to_kernel_data_write_reg[21] 
       (.CLR(1'b0),
        .D(cpu_data_w[21]),
        .G(u1_cpu_n_35),
        .GE(1'b1),
        .Q(fifo_monitor_to_kernel_data_write[21]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \fifo_monitor_to_kernel_data_write_reg[22] 
       (.CLR(1'b0),
        .D(cpu_data_w[22]),
        .G(u1_cpu_n_35),
        .GE(1'b1),
        .Q(fifo_monitor_to_kernel_data_write[22]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \fifo_monitor_to_kernel_data_write_reg[23] 
       (.CLR(1'b0),
        .D(cpu_data_w[23]),
        .G(u1_cpu_n_35),
        .GE(1'b1),
        .Q(fifo_monitor_to_kernel_data_write[23]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \fifo_monitor_to_kernel_data_write_reg[24] 
       (.CLR(1'b0),
        .D(cpu_data_w[24]),
        .G(u1_cpu_n_35),
        .GE(1'b1),
        .Q(fifo_monitor_to_kernel_data_write[24]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \fifo_monitor_to_kernel_data_write_reg[25] 
       (.CLR(1'b0),
        .D(cpu_data_w[25]),
        .G(u1_cpu_n_35),
        .GE(1'b1),
        .Q(fifo_monitor_to_kernel_data_write[25]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \fifo_monitor_to_kernel_data_write_reg[26] 
       (.CLR(1'b0),
        .D(cpu_data_w[26]),
        .G(u1_cpu_n_35),
        .GE(1'b1),
        .Q(fifo_monitor_to_kernel_data_write[26]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \fifo_monitor_to_kernel_data_write_reg[27] 
       (.CLR(1'b0),
        .D(cpu_data_w[27]),
        .G(u1_cpu_n_35),
        .GE(1'b1),
        .Q(fifo_monitor_to_kernel_data_write[27]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \fifo_monitor_to_kernel_data_write_reg[28] 
       (.CLR(1'b0),
        .D(cpu_data_w[28]),
        .G(u1_cpu_n_35),
        .GE(1'b1),
        .Q(fifo_monitor_to_kernel_data_write[28]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \fifo_monitor_to_kernel_data_write_reg[29] 
       (.CLR(1'b0),
        .D(cpu_data_w[29]),
        .G(u1_cpu_n_35),
        .GE(1'b1),
        .Q(fifo_monitor_to_kernel_data_write[29]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \fifo_monitor_to_kernel_data_write_reg[2] 
       (.CLR(1'b0),
        .D(cpu_data_w[2]),
        .G(u1_cpu_n_35),
        .GE(1'b1),
        .Q(fifo_monitor_to_kernel_data_write[2]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \fifo_monitor_to_kernel_data_write_reg[30] 
       (.CLR(1'b0),
        .D(cpu_data_w[30]),
        .G(u1_cpu_n_35),
        .GE(1'b1),
        .Q(fifo_monitor_to_kernel_data_write[30]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \fifo_monitor_to_kernel_data_write_reg[31] 
       (.CLR(1'b0),
        .D(cpu_data_w[31]),
        .G(u1_cpu_n_35),
        .GE(1'b1),
        .Q(fifo_monitor_to_kernel_data_write[31]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \fifo_monitor_to_kernel_data_write_reg[3] 
       (.CLR(1'b0),
        .D(cpu_data_w[3]),
        .G(u1_cpu_n_35),
        .GE(1'b1),
        .Q(fifo_monitor_to_kernel_data_write[3]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \fifo_monitor_to_kernel_data_write_reg[4] 
       (.CLR(1'b0),
        .D(cpu_data_w[4]),
        .G(u1_cpu_n_35),
        .GE(1'b1),
        .Q(fifo_monitor_to_kernel_data_write[4]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \fifo_monitor_to_kernel_data_write_reg[5] 
       (.CLR(1'b0),
        .D(cpu_data_w[5]),
        .G(u1_cpu_n_35),
        .GE(1'b1),
        .Q(fifo_monitor_to_kernel_data_write[5]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \fifo_monitor_to_kernel_data_write_reg[6] 
       (.CLR(1'b0),
        .D(cpu_data_w[6]),
        .G(u1_cpu_n_35),
        .GE(1'b1),
        .Q(fifo_monitor_to_kernel_data_write[6]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \fifo_monitor_to_kernel_data_write_reg[7] 
       (.CLR(1'b0),
        .D(cpu_data_w[7]),
        .G(u1_cpu_n_35),
        .GE(1'b1),
        .Q(fifo_monitor_to_kernel_data_write[7]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \fifo_monitor_to_kernel_data_write_reg[8] 
       (.CLR(1'b0),
        .D(cpu_data_w[8]),
        .G(u1_cpu_n_35),
        .GE(1'b1),
        .Q(fifo_monitor_to_kernel_data_write[8]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \fifo_monitor_to_kernel_data_write_reg[9] 
       (.CLR(1'b0),
        .D(cpu_data_w[9]),
        .G(u1_cpu_n_35),
        .GE(1'b1),
        .Q(fifo_monitor_to_kernel_data_write[9]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    fifo_monitor_to_kernel_en_reg
       (.CLR(1'b0),
        .D(u1_cpu_n_46),
        .G(u1_cpu_n_34),
        .GE(1'b1),
        .Q(fifo_monitor_to_kernel_en));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    fifo_ptm_en_reg
       (.CLR(1'b0),
        .D(u1_cpu_n_44),
        .G(u1_cpu_n_34),
        .GE(1'b1),
        .Q(fifo_ptm_en));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    fifo_read_instrumentation_request_read_en_reg
       (.CLR(1'b0),
        .D(u1_cpu_n_48),
        .G(u1_cpu_n_34),
        .GE(1'b1),
        .Q(fifo_read_instrumentation_request_read_en));
  LUT4 #(
    .INIT(16'h2320)) 
    intr_enable_reg_i_1
       (.I0(reg_dest),
        .I1(\u4_reg_bank/eqOp__4 ),
        .I2(u1_cpu_n_85),
        .I3(intr_enable),
        .O(intr_enable_reg_i_1_n_0));
  LUT3 #(
    .INIT(8'hB8)) 
    mode_reg_i_1
       (.I0(u1_cpu_n_11),
        .I1(u1_cpu_n_52),
        .I2(\u8_mult/mode_reg ),
        .O(mode_reg_i_1_n_0));
  LUT6 #(
    .INIT(64'hCCCFCFCB00000008)) 
    negate_reg_i_1
       (.I0(u1_cpu_n_51),
        .I1(u1_cpu_n_8),
        .I2(u1_cpu_n_9),
        .I3(u1_cpu_n_7),
        .I4(u1_cpu_n_6),
        .I5(\u8_mult/negate_reg ),
        .O(negate_reg_i_1_n_0));
  LUT5 #(
    .INIT(32'hF3FFA200)) 
    sign2_reg_i_1
       (.I0(u1_cpu_n_3),
        .I1(u1_cpu_n_9),
        .I2(u1_cpu_n_8),
        .I3(\u8_mult/sign2_reg ),
        .I4(u1_cpu_n_4),
        .O(sign2_reg_i_1_n_0));
  LUT6 #(
    .INIT(64'h2000FFFF20000000)) 
    sign_reg_i_1
       (.I0(\u8_mult/sign_reg__6 ),
        .I1(u1_cpu_n_8),
        .I2(u1_cpu_n_6),
        .I3(u1_cpu_n_9),
        .I4(\u8_mult/sign2_reg ),
        .I5(u1_cpu_n_3),
        .O(sign_reg_i_1_n_0));
  design_1_Dispatcher_HardBlare_0_0_mlite_cpu u1_cpu
       (.D(complete_address_next),
        .E(u1_cpu_n_32),
        .\PLtoPS_bytes_selection[3] (u1_cpu_n_37),
        .PLtoPS_enable(u1_cpu_n_49),
        .PLtoPS_generate_interrupt(u1_cpu_n_45),
        .PLtoPS_readed_data(PLtoPS_readed_data),
        .\PLtoPS_write_data[31] (u1_cpu_n_38),
        .\aa_reg_reg[30] (u1_cpu_n_11),
        .\bram_bbt_annotations_address[2] (u1_cpu_n_33),
        .bram_bbt_annotations_data_read(bram_bbt_annotations_data_read),
        .\bram_debug_bytes_selection[3] (u1_cpu_n_36),
        .bram_debug_data_read(bram_debug_data_read),
        .bram_debug_en(u1_cpu_n_30),
        .\bram_firmware_code_bytes_selection[3] (byte_we_next),
        .bram_firmware_code_data_read(bram_firmware_code_data_read),
        .\bram_firmware_code_data_write[31] (cpu_data_w),
        .\bram_sec_policy_config_bytes_selection[3] (u1_cpu_n_43),
        .bram_sec_policy_config_data_read(bram_sec_policy_config_data_read),
        .bram_sec_policy_config_en(u1_cpu_n_31),
        .bram_sec_policy_config_en_0(u1_cpu_n_34),
        .clk(clk),
        .\count_reg_reg[5] (u1_cpu_n_52),
        .eqOp__4(\u4_reg_bank/eqOp__4 ),
        .fifo_instrumentation_almost_empty(fifo_instrumentation_almost_empty),
        .fifo_instrumentation_almost_full(fifo_instrumentation_almost_full),
        .fifo_instrumentation_empty(fifo_instrumentation_empty),
        .fifo_instrumentation_full(fifo_instrumentation_full),
        .fifo_kernel_to_monitor_almost_empty(fifo_kernel_to_monitor_almost_empty),
        .fifo_kernel_to_monitor_almost_full(fifo_kernel_to_monitor_almost_full),
        .fifo_kernel_to_monitor_data_read(fifo_kernel_to_monitor_data_read),
        .fifo_kernel_to_monitor_empty(fifo_kernel_to_monitor_empty),
        .fifo_kernel_to_monitor_en(u1_cpu_n_47),
        .fifo_kernel_to_monitor_full(fifo_kernel_to_monitor_full),
        .fifo_monitor_to_kernel_almost_empty(fifo_monitor_to_kernel_almost_empty),
        .fifo_monitor_to_kernel_almost_full(fifo_monitor_to_kernel_almost_full),
        .\fifo_monitor_to_kernel_data_write[31] (u1_cpu_n_35),
        .fifo_monitor_to_kernel_empty(fifo_monitor_to_kernel_empty),
        .fifo_monitor_to_kernel_en(u1_cpu_n_46),
        .fifo_monitor_to_kernel_full(fifo_monitor_to_kernel_full),
        .fifo_ptm_almost_empty(fifo_ptm_almost_empty),
        .fifo_ptm_almost_full(fifo_ptm_almost_full),
        .fifo_ptm_data_read(fifo_ptm_data_read),
        .fifo_ptm_empty(fifo_ptm_empty),
        .fifo_ptm_en(u1_cpu_n_44),
        .fifo_ptm_full(fifo_ptm_full),
        .fifo_read_instrumentation_data_read(fifo_read_instrumentation_data_read),
        .fifo_read_instrumentation_request_read_en(u1_cpu_n_48),
        .intr_enable(intr_enable),
        .intr_enable_reg_reg(reg_dest),
        .intr_enable_reg_reg_0(u1_cpu_n_85),
        .intr_enable_reg_reg_1(intr_enable_reg_i_1_n_0),
        .\lower_reg_reg[31] (u1_cpu_n_6),
        .\lower_reg_reg[31]_0 (u1_cpu_n_7),
        .\lower_reg_reg[31]_1 (u1_cpu_n_8),
        .\lower_reg_reg[31]_2 (u1_cpu_n_9),
        .mode_reg(\u8_mult/mode_reg ),
        .mode_reg_reg(mode_reg_i_1_n_0),
        .negate_reg(\u8_mult/negate_reg ),
        .negate_reg_reg(u1_cpu_n_51),
        .negate_reg_reg_0(negate_reg_i_1_n_0),
        .reset(reset),
        .sign2_reg(\u8_mult/sign2_reg ),
        .sign2_reg_reg(u1_cpu_n_4),
        .sign_reg__6(\u8_mult/sign_reg__6 ),
        .sign_reg_reg(u1_cpu_n_3),
        .sign_reg_reg_0(sign_reg_i_1_n_0),
        .sign_reg_reg_1(sign2_reg_i_1_n_0));
endmodule

module design_1_Dispatcher_HardBlare_0_0_bus_mux
   (CO,
    S,
    \opcode_reg_reg[25] ,
    \opcode_reg_reg[25]_0 );
  output [0:0]CO;
  input [3:0]S;
  input [3:0]\opcode_reg_reg[25] ;
  input [2:0]\opcode_reg_reg[25]_0 ;

  wire [0:0]CO;
  wire [3:0]S;
  wire [3:0]\opcode_reg_reg[25] ;
  wire [2:0]\opcode_reg_reg[25]_0 ;
  wire take_branch1_carry__0_n_0;
  wire take_branch1_carry__0_n_1;
  wire take_branch1_carry__0_n_2;
  wire take_branch1_carry__0_n_3;
  wire take_branch1_carry__1_n_2;
  wire take_branch1_carry__1_n_3;
  wire take_branch1_carry_n_0;
  wire take_branch1_carry_n_1;
  wire take_branch1_carry_n_2;
  wire take_branch1_carry_n_3;
  wire [3:0]NLW_take_branch1_carry_O_UNCONNECTED;
  wire [3:0]NLW_take_branch1_carry__0_O_UNCONNECTED;
  wire [3:3]NLW_take_branch1_carry__1_CO_UNCONNECTED;
  wire [3:0]NLW_take_branch1_carry__1_O_UNCONNECTED;

  CARRY4 take_branch1_carry
       (.CI(1'b0),
        .CO({take_branch1_carry_n_0,take_branch1_carry_n_1,take_branch1_carry_n_2,take_branch1_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_take_branch1_carry_O_UNCONNECTED[3:0]),
        .S(S));
  CARRY4 take_branch1_carry__0
       (.CI(take_branch1_carry_n_0),
        .CO({take_branch1_carry__0_n_0,take_branch1_carry__0_n_1,take_branch1_carry__0_n_2,take_branch1_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_take_branch1_carry__0_O_UNCONNECTED[3:0]),
        .S(\opcode_reg_reg[25] ));
  CARRY4 take_branch1_carry__1
       (.CI(take_branch1_carry__0_n_0),
        .CO({NLW_take_branch1_carry__1_CO_UNCONNECTED[3],CO,take_branch1_carry__1_n_2,take_branch1_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_take_branch1_carry__1_O_UNCONNECTED[3:0]),
        .S({1'b0,\opcode_reg_reg[25]_0 }));
endmodule

(* CHECK_LICENSE_TYPE = "design_1_Dispatcher_HardBlare_0_0,Dispatcher_Plasma_MIPS_HardBlare,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* ip_definition_source = "package_project" *) 
(* x_core_info = "Dispatcher_Plasma_MIPS_HardBlare,Vivado 2018.2" *) 
(* NotValidForBitStream *)
module design_1_Dispatcher_HardBlare_0_0
   (clk,
    reset,
    bram_firmware_code_en,
    bram_firmware_code_address,
    bram_firmware_code_data_write,
    bram_firmware_code_bytes_selection,
    bram_firmware_code_data_read,
    bram_firmware_code_clk,
    bram_firmware_code_reset,
    bram_bbt_annotations_en,
    bram_bbt_annotations_data_read,
    bram_bbt_annotations_address,
    bram_bbt_annotations_data_write,
    bram_bbt_annotations_bytes_selection,
    bram_bbt_annotations_clk,
    bram_bbt_annotations_reset,
    fifo_ptm_en,
    fifo_ptm_data_read,
    fifo_ptm_empty,
    fifo_ptm_almost_empty,
    fifo_ptm_full,
    fifo_ptm_almost_full,
    fifo_ptm_clk,
    fifo_ptm_reset,
    fifo_monitor_to_kernel_en,
    fifo_monitor_to_kernel_data_write,
    fifo_monitor_to_kernel_empty,
    fifo_monitor_to_kernel_almost_empty,
    fifo_monitor_to_kernel_full,
    fifo_monitor_to_kernel_almost_full,
    fifo_monitor_to_kernel_clk,
    fifo_monitor_to_kernel_reset,
    fifo_kernel_to_monitor_en,
    fifo_kernel_to_monitor_data_read,
    fifo_kernel_to_monitor_empty,
    fifo_kernel_to_monitor_almost_empty,
    fifo_kernel_to_monitor_full,
    fifo_kernel_to_monitor_almost_full,
    fifo_kernel_to_monitor_clk,
    fifo_kernel_to_monitor_reset,
    fifo_read_instrumentation_request_read_en,
    fifo_read_instrumentation_data_read,
    fifo_instrumentation_empty,
    fifo_instrumentation_almost_empty,
    fifo_instrumentation_full,
    fifo_instrumentation_almost_full,
    bram_debug_en,
    bram_debug_address,
    bram_debug_data_write,
    bram_debug_bytes_selection,
    bram_debug_data_read,
    bram_debug_clk,
    bram_debug_reset,
    PLtoPS_clk,
    PLtoPS_reset,
    PLtoPS_enable,
    PLtoPS_address,
    PLtoPS_write_data,
    PLtoPS_bytes_selection,
    PLtoPS_readed_data,
    PLtoPS_generate_interrupt,
    bram_sec_policy_config_en,
    bram_sec_policy_config_data_read,
    bram_sec_policy_config_address,
    bram_sec_policy_config_data_write,
    bram_sec_policy_config_bytes_selection,
    bram_sec_policy_config_clk,
    bram_sec_policy_config_reset);
  input clk;
  input reset;
  output bram_firmware_code_en;
  output [31:0]bram_firmware_code_address;
  output [31:0]bram_firmware_code_data_write;
  output [3:0]bram_firmware_code_bytes_selection;
  input [31:0]bram_firmware_code_data_read;
  output bram_firmware_code_clk;
  output bram_firmware_code_reset;
  output bram_bbt_annotations_en;
  input [31:0]bram_bbt_annotations_data_read;
  output [31:0]bram_bbt_annotations_address;
  output [31:0]bram_bbt_annotations_data_write;
  output [3:0]bram_bbt_annotations_bytes_selection;
  output bram_bbt_annotations_clk;
  output bram_bbt_annotations_reset;
  output fifo_ptm_en;
  input [31:0]fifo_ptm_data_read;
  input fifo_ptm_empty;
  input fifo_ptm_almost_empty;
  input fifo_ptm_full;
  input fifo_ptm_almost_full;
  output fifo_ptm_clk;
  output fifo_ptm_reset;
  output fifo_monitor_to_kernel_en;
  output [31:0]fifo_monitor_to_kernel_data_write;
  input fifo_monitor_to_kernel_empty;
  input fifo_monitor_to_kernel_almost_empty;
  input fifo_monitor_to_kernel_full;
  input fifo_monitor_to_kernel_almost_full;
  output fifo_monitor_to_kernel_clk;
  output fifo_monitor_to_kernel_reset;
  output fifo_kernel_to_monitor_en;
  input [31:0]fifo_kernel_to_monitor_data_read;
  input fifo_kernel_to_monitor_empty;
  input fifo_kernel_to_monitor_almost_empty;
  input fifo_kernel_to_monitor_full;
  input fifo_kernel_to_monitor_almost_full;
  output fifo_kernel_to_monitor_clk;
  output fifo_kernel_to_monitor_reset;
  output fifo_read_instrumentation_request_read_en;
  input [31:0]fifo_read_instrumentation_data_read;
  input fifo_instrumentation_empty;
  input fifo_instrumentation_almost_empty;
  input fifo_instrumentation_full;
  input fifo_instrumentation_almost_full;
  output bram_debug_en;
  output [31:0]bram_debug_address;
  output [31:0]bram_debug_data_write;
  output [3:0]bram_debug_bytes_selection;
  input [31:0]bram_debug_data_read;
  output bram_debug_clk;
  output bram_debug_reset;
  output PLtoPS_clk;
  output PLtoPS_reset;
  output PLtoPS_enable;
  output [31:0]PLtoPS_address;
  output [31:0]PLtoPS_write_data;
  output [3:0]PLtoPS_bytes_selection;
  input [31:0]PLtoPS_readed_data;
  output PLtoPS_generate_interrupt;
  output bram_sec_policy_config_en;
  input [31:0]bram_sec_policy_config_data_read;
  output [31:0]bram_sec_policy_config_address;
  output [31:0]bram_sec_policy_config_data_write;
  output [3:0]bram_sec_policy_config_bytes_selection;
  output bram_sec_policy_config_clk;
  output bram_sec_policy_config_reset;

  wire \<const0> ;
  wire [19:2]\^PLtoPS_address ;
  wire [3:0]PLtoPS_bytes_selection;
  wire PLtoPS_enable;
  wire PLtoPS_generate_interrupt;
  wire [31:0]PLtoPS_readed_data;
  wire [31:0]PLtoPS_write_data;
  wire [19:2]\^bram_bbt_annotations_address ;
  wire [3:0]bram_bbt_annotations_bytes_selection;
  wire [31:0]bram_bbt_annotations_data_read;
  wire [31:0]bram_bbt_annotations_data_write;
  wire bram_bbt_annotations_en;
  wire [19:2]\^bram_debug_address ;
  wire [3:0]bram_debug_bytes_selection;
  wire [31:0]bram_debug_data_read;
  wire [31:0]bram_debug_data_write;
  wire bram_debug_en;
  wire [19:2]\^bram_firmware_code_address ;
  wire [3:0]bram_firmware_code_bytes_selection;
  wire [31:0]bram_firmware_code_data_read;
  wire [31:0]bram_firmware_code_data_write;
  wire bram_firmware_code_en;
  wire [19:2]\^bram_sec_policy_config_address ;
  wire [3:0]bram_sec_policy_config_bytes_selection;
  wire [31:0]bram_sec_policy_config_data_read;
  wire [31:0]bram_sec_policy_config_data_write;
  wire bram_sec_policy_config_en;
  wire clk;
  wire fifo_instrumentation_almost_empty;
  wire fifo_instrumentation_almost_full;
  wire fifo_instrumentation_empty;
  wire fifo_instrumentation_full;
  wire fifo_kernel_to_monitor_almost_empty;
  wire fifo_kernel_to_monitor_almost_full;
  wire [31:0]fifo_kernel_to_monitor_data_read;
  wire fifo_kernel_to_monitor_empty;
  wire fifo_kernel_to_monitor_en;
  wire fifo_kernel_to_monitor_full;
  wire fifo_monitor_to_kernel_almost_empty;
  wire fifo_monitor_to_kernel_almost_full;
  wire [31:0]fifo_monitor_to_kernel_data_write;
  wire fifo_monitor_to_kernel_empty;
  wire fifo_monitor_to_kernel_en;
  wire fifo_monitor_to_kernel_full;
  wire fifo_ptm_almost_empty;
  wire fifo_ptm_almost_full;
  wire [31:0]fifo_ptm_data_read;
  wire fifo_ptm_empty;
  wire fifo_ptm_en;
  wire fifo_ptm_full;
  wire [31:0]fifo_read_instrumentation_data_read;
  wire fifo_read_instrumentation_request_read_en;
  wire reset;

  assign PLtoPS_address[31] = \<const0> ;
  assign PLtoPS_address[30] = \<const0> ;
  assign PLtoPS_address[29] = \<const0> ;
  assign PLtoPS_address[28] = \<const0> ;
  assign PLtoPS_address[27] = \<const0> ;
  assign PLtoPS_address[26] = \<const0> ;
  assign PLtoPS_address[25] = \<const0> ;
  assign PLtoPS_address[24] = \<const0> ;
  assign PLtoPS_address[23] = \<const0> ;
  assign PLtoPS_address[22] = \<const0> ;
  assign PLtoPS_address[21] = \<const0> ;
  assign PLtoPS_address[20] = \<const0> ;
  assign PLtoPS_address[19:2] = \^PLtoPS_address [19:2];
  assign PLtoPS_address[1] = \<const0> ;
  assign PLtoPS_address[0] = \<const0> ;
  assign PLtoPS_clk = clk;
  assign PLtoPS_reset = reset;
  assign bram_bbt_annotations_address[31] = \<const0> ;
  assign bram_bbt_annotations_address[30] = \<const0> ;
  assign bram_bbt_annotations_address[29] = \<const0> ;
  assign bram_bbt_annotations_address[28] = \<const0> ;
  assign bram_bbt_annotations_address[27] = \<const0> ;
  assign bram_bbt_annotations_address[26] = \<const0> ;
  assign bram_bbt_annotations_address[25] = \<const0> ;
  assign bram_bbt_annotations_address[24] = \<const0> ;
  assign bram_bbt_annotations_address[23] = \<const0> ;
  assign bram_bbt_annotations_address[22] = \<const0> ;
  assign bram_bbt_annotations_address[21] = \<const0> ;
  assign bram_bbt_annotations_address[20] = \<const0> ;
  assign bram_bbt_annotations_address[19:2] = \^bram_bbt_annotations_address [19:2];
  assign bram_bbt_annotations_address[1] = \<const0> ;
  assign bram_bbt_annotations_address[0] = \<const0> ;
  assign bram_bbt_annotations_clk = clk;
  assign bram_bbt_annotations_reset = reset;
  assign bram_debug_address[31] = \<const0> ;
  assign bram_debug_address[30] = \<const0> ;
  assign bram_debug_address[29] = \<const0> ;
  assign bram_debug_address[28] = \<const0> ;
  assign bram_debug_address[27] = \<const0> ;
  assign bram_debug_address[26] = \<const0> ;
  assign bram_debug_address[25] = \<const0> ;
  assign bram_debug_address[24] = \<const0> ;
  assign bram_debug_address[23] = \<const0> ;
  assign bram_debug_address[22] = \<const0> ;
  assign bram_debug_address[21] = \<const0> ;
  assign bram_debug_address[20] = \<const0> ;
  assign bram_debug_address[19:2] = \^bram_debug_address [19:2];
  assign bram_debug_address[1] = \<const0> ;
  assign bram_debug_address[0] = \<const0> ;
  assign bram_debug_clk = clk;
  assign bram_debug_reset = reset;
  assign bram_firmware_code_address[31] = \<const0> ;
  assign bram_firmware_code_address[30] = \<const0> ;
  assign bram_firmware_code_address[29] = \<const0> ;
  assign bram_firmware_code_address[28] = \<const0> ;
  assign bram_firmware_code_address[27] = \<const0> ;
  assign bram_firmware_code_address[26] = \<const0> ;
  assign bram_firmware_code_address[25] = \<const0> ;
  assign bram_firmware_code_address[24] = \<const0> ;
  assign bram_firmware_code_address[23] = \<const0> ;
  assign bram_firmware_code_address[22] = \<const0> ;
  assign bram_firmware_code_address[21] = \<const0> ;
  assign bram_firmware_code_address[20] = \<const0> ;
  assign bram_firmware_code_address[19:2] = \^bram_firmware_code_address [19:2];
  assign bram_firmware_code_address[1] = \<const0> ;
  assign bram_firmware_code_address[0] = \<const0> ;
  assign bram_firmware_code_clk = clk;
  assign bram_firmware_code_reset = reset;
  assign bram_sec_policy_config_address[31] = \<const0> ;
  assign bram_sec_policy_config_address[30] = \<const0> ;
  assign bram_sec_policy_config_address[29] = \<const0> ;
  assign bram_sec_policy_config_address[28] = \<const0> ;
  assign bram_sec_policy_config_address[27] = \<const0> ;
  assign bram_sec_policy_config_address[26] = \<const0> ;
  assign bram_sec_policy_config_address[25] = \<const0> ;
  assign bram_sec_policy_config_address[24] = \<const0> ;
  assign bram_sec_policy_config_address[23] = \<const0> ;
  assign bram_sec_policy_config_address[22] = \<const0> ;
  assign bram_sec_policy_config_address[21] = \<const0> ;
  assign bram_sec_policy_config_address[20] = \<const0> ;
  assign bram_sec_policy_config_address[19:2] = \^bram_sec_policy_config_address [19:2];
  assign bram_sec_policy_config_address[1] = \<const0> ;
  assign bram_sec_policy_config_address[0] = \<const0> ;
  assign bram_sec_policy_config_clk = clk;
  assign bram_sec_policy_config_reset = reset;
  assign fifo_kernel_to_monitor_clk = clk;
  assign fifo_kernel_to_monitor_reset = reset;
  assign fifo_monitor_to_kernel_clk = clk;
  assign fifo_monitor_to_kernel_reset = reset;
  assign fifo_ptm_clk = clk;
  assign fifo_ptm_reset = reset;
  GND GND
       (.G(\<const0> ));
  design_1_Dispatcher_HardBlare_0_0_Dispatcher_Plasma_MIPS_HardBlare U0
       (.PLtoPS_address(\^PLtoPS_address ),
        .PLtoPS_bytes_selection(PLtoPS_bytes_selection),
        .PLtoPS_enable(PLtoPS_enable),
        .PLtoPS_generate_interrupt(PLtoPS_generate_interrupt),
        .PLtoPS_readed_data(PLtoPS_readed_data),
        .PLtoPS_write_data(PLtoPS_write_data),
        .bram_bbt_annotations_address(\^bram_bbt_annotations_address ),
        .bram_bbt_annotations_bytes_selection(bram_bbt_annotations_bytes_selection),
        .bram_bbt_annotations_data_read(bram_bbt_annotations_data_read),
        .bram_bbt_annotations_data_write(bram_bbt_annotations_data_write),
        .bram_bbt_annotations_en(bram_bbt_annotations_en),
        .bram_debug_address(\^bram_debug_address ),
        .bram_debug_bytes_selection(bram_debug_bytes_selection),
        .bram_debug_data_read(bram_debug_data_read),
        .bram_debug_data_write(bram_debug_data_write),
        .bram_debug_en(bram_debug_en),
        .bram_firmware_code_address(\^bram_firmware_code_address ),
        .bram_firmware_code_bytes_selection(bram_firmware_code_bytes_selection),
        .bram_firmware_code_data_read(bram_firmware_code_data_read),
        .bram_firmware_code_data_write(bram_firmware_code_data_write),
        .bram_firmware_code_en(bram_firmware_code_en),
        .bram_sec_policy_config_address(\^bram_sec_policy_config_address ),
        .bram_sec_policy_config_bytes_selection(bram_sec_policy_config_bytes_selection),
        .bram_sec_policy_config_data_read(bram_sec_policy_config_data_read),
        .bram_sec_policy_config_data_write(bram_sec_policy_config_data_write),
        .bram_sec_policy_config_en(bram_sec_policy_config_en),
        .clk(clk),
        .fifo_instrumentation_almost_empty(fifo_instrumentation_almost_empty),
        .fifo_instrumentation_almost_full(fifo_instrumentation_almost_full),
        .fifo_instrumentation_empty(fifo_instrumentation_empty),
        .fifo_instrumentation_full(fifo_instrumentation_full),
        .fifo_kernel_to_monitor_almost_empty(fifo_kernel_to_monitor_almost_empty),
        .fifo_kernel_to_monitor_almost_full(fifo_kernel_to_monitor_almost_full),
        .fifo_kernel_to_monitor_data_read(fifo_kernel_to_monitor_data_read),
        .fifo_kernel_to_monitor_empty(fifo_kernel_to_monitor_empty),
        .fifo_kernel_to_monitor_en(fifo_kernel_to_monitor_en),
        .fifo_kernel_to_monitor_full(fifo_kernel_to_monitor_full),
        .fifo_monitor_to_kernel_almost_empty(fifo_monitor_to_kernel_almost_empty),
        .fifo_monitor_to_kernel_almost_full(fifo_monitor_to_kernel_almost_full),
        .fifo_monitor_to_kernel_data_write(fifo_monitor_to_kernel_data_write),
        .fifo_monitor_to_kernel_empty(fifo_monitor_to_kernel_empty),
        .fifo_monitor_to_kernel_en(fifo_monitor_to_kernel_en),
        .fifo_monitor_to_kernel_full(fifo_monitor_to_kernel_full),
        .fifo_ptm_almost_empty(fifo_ptm_almost_empty),
        .fifo_ptm_almost_full(fifo_ptm_almost_full),
        .fifo_ptm_data_read(fifo_ptm_data_read),
        .fifo_ptm_empty(fifo_ptm_empty),
        .fifo_ptm_en(fifo_ptm_en),
        .fifo_ptm_full(fifo_ptm_full),
        .fifo_read_instrumentation_data_read(fifo_read_instrumentation_data_read),
        .fifo_read_instrumentation_request_read_en(fifo_read_instrumentation_request_read_en),
        .reset(reset));
endmodule

module design_1_Dispatcher_HardBlare_0_0_mem_ctrl
   (\address_reg_reg[24]_0 ,
    \lower_reg_reg[31] ,
    \lower_reg_reg[31]_0 ,
    \lower_reg_reg[31]_1 ,
    \lower_reg_reg[31]_2 ,
    \pc_reg_reg[25] ,
    \bb_reg_reg[31] ,
    intr_enable_reg_reg,
    \bb_reg_reg[31]_0 ,
    \bb_reg_reg[31]_1 ,
    \bb_reg_reg[31]_2 ,
    D,
    \pc_reg_reg[14] ,
    \bb_reg_reg[1] ,
    \bb_reg_reg[1]_0 ,
    \bb_reg_reg[1]_1 ,
    \bb_reg_reg[1]_2 ,
    \bb_reg_reg[1]_3 ,
    \pc_reg_reg[28] ,
    \aa_reg_reg[30] ,
    WE,
    \bb_reg_reg[31]_3 ,
    reg_dest,
    intr_enable_reg_reg_0,
    \bram_sec_policy_config_address[19] ,
    \pc_reg_reg[3] ,
    \pc_reg_reg[3]_0 ,
    \pc_reg_reg[31] ,
    \pc_reg_reg[30] ,
    \pc_reg_reg[29] ,
    \pc_reg_reg[27] ,
    \pc_reg_reg[26] ,
    \pc_reg_reg[25]_0 ,
    \pc_reg_reg[24] ,
    \pc_reg_reg[23] ,
    \pc_reg_reg[22] ,
    \pc_reg_reg[21] ,
    \pc_reg_reg[20] ,
    \pc_reg_reg[19] ,
    \pc_reg_reg[18] ,
    \pc_reg_reg[17] ,
    \pc_reg_reg[16] ,
    \pc_reg_reg[15] ,
    \pc_reg_reg[14]_0 ,
    \pc_reg_reg[13] ,
    \pc_reg_reg[12] ,
    \pc_reg_reg[11] ,
    \pc_reg_reg[10] ,
    \pc_reg_reg[9] ,
    \pc_reg_reg[8] ,
    \pc_reg_reg[7] ,
    \pc_reg_reg[6] ,
    \pc_reg_reg[5] ,
    \pc_reg_reg[4] ,
    \pc_reg_reg[2] ,
    \upper_reg_reg[31] ,
    \lower_reg_reg[31]_3 ,
    \lower_reg_reg[0] ,
    E,
    \pc_reg_reg[31]_0 ,
    \bram_firmware_code_bytes_selection[3] ,
    S,
    \pc_reg_reg[28]_0 ,
    \pc_reg_reg[28]_1 ,
    \pc_reg_reg[30]_0 ,
    \pc_reg_reg[31]_1 ,
    \pc_reg_reg[30]_1 ,
    intr_enable_reg_reg_1,
    \pc_reg_reg[25]_1 ,
    \pc_reg_reg[7]_0 ,
    \pc_reg_reg[2]_0 ,
    \aa_reg_reg[31] ,
    sign_reg__6,
    negate_reg_reg,
    DPRA0,
    DPRA1,
    DPRA2,
    DPRA3,
    \bb_reg_reg[31]_4 ,
    \count_reg_reg[5] ,
    \aa_reg_reg[31]_0 ,
    \count_reg_reg[5]_0 ,
    \bram_firmware_code_data_write[31] ,
    intr_enable_reg_reg_2,
    \bb_reg_reg[1]_4 ,
    \count_reg_reg[4] ,
    \lower_reg_reg[31]_4 ,
    sign2_reg,
    bram_debug_en,
    bram_sec_policy_config_en,
    \bram_firmware_code_address[2] ,
    \bram_bbt_annotations_address[2] ,
    bram_sec_policy_config_en_0,
    \fifo_monitor_to_kernel_data_write[31] ,
    \bram_debug_bytes_selection[3] ,
    \PLtoPS_bytes_selection[3] ,
    \PLtoPS_write_data[31] ,
    \bram_sec_policy_config_bytes_selection[3] ,
    fifo_ptm_en,
    PLtoPS_generate_interrupt,
    fifo_monitor_to_kernel_en,
    fifo_kernel_to_monitor_en,
    fifo_read_instrumentation_request_read_en,
    PLtoPS_enable,
    clk,
    negate_reg_reg_0,
    Q,
    bram_debug_data_read,
    bram_sec_policy_config_data_read,
    fifo_kernel_to_monitor_empty,
    fifo_kernel_to_monitor_almost_empty,
    fifo_kernel_to_monitor_full,
    fifo_monitor_to_kernel_empty,
    fifo_monitor_to_kernel_almost_empty,
    fifo_monitor_to_kernel_full,
    fifo_instrumentation_empty,
    fifo_instrumentation_almost_empty,
    fifo_instrumentation_full,
    p_48_in,
    pc_current,
    intr_enable,
    DPO,
    data_out1B,
    DPO122_out,
    DPO118_out,
    DPO114_out,
    DPO110_out,
    DPO106_out,
    DPO102_out,
    DPO98_out,
    DPO94_out,
    DPO90_out,
    DPO86_out,
    DPO82_out,
    DPO78_out,
    DPO74_out,
    DPO70_out,
    DPO66_out,
    DPO62_out,
    DPO58_out,
    DPO54_out,
    DPO50_out,
    DPO46_out,
    DPO42_out,
    DPO38_out,
    DPO34_out,
    DPO30_out,
    DPO26_out,
    DPO22_out,
    DPO18_out,
    DPO14_out,
    DPO10_out,
    DPO6_out,
    DPO2_out,
    pc_plus4,
    \aa_reg_reg[31]_1 ,
    \aa_reg_reg[28] ,
    \bb_reg_reg[0] ,
    \aa_reg_reg[26] ,
    \bb_reg_reg[0]_0 ,
    \aa_reg_reg[24] ,
    \bb_reg_reg[0]_1 ,
    \pc_reg_reg[29]_0 ,
    \pc_reg_reg[28]_2 ,
    sign_reg_reg,
    \lower_reg_reg[31]_5 ,
    mode_reg_reg,
    lower_reg0__62,
    mode_reg_reg_0,
    neqOp0_in,
    fifo_ptm_empty,
    CO,
    reg_target,
    \lower_reg_reg[31]_6 ,
    \lower_reg_reg[29] ,
    \lower_reg_reg[28] ,
    \lower_reg_reg[27] ,
    \lower_reg_reg[26] ,
    \lower_reg_reg[26]_0 ,
    \lower_reg_reg[24] ,
    \upper_reg_reg[25] ,
    \lower_reg_reg[22] ,
    \upper_reg_reg[23] ,
    \lower_reg_reg[21] ,
    \upper_reg_reg[21] ,
    \upper_reg_reg[20] ,
    \upper_reg_reg[19] ,
    \upper_reg_reg[18] ,
    \lower_reg_reg[16] ,
    \upper_reg_reg[16] ,
    \lower_reg_reg[13] ,
    \lower_reg_reg[12] ,
    \lower_reg_reg[11] ,
    \lower_reg_reg[11]_0 ,
    \lower_reg_reg[9] ,
    \lower_reg_reg[8] ,
    \lower_reg_reg[7] ,
    \lower_reg_reg[6] ,
    \lower_reg_reg[7]_0 ,
    \upper_reg_reg[6] ,
    \upper_reg_reg[5] ,
    \upper_reg_reg[4] ,
    \upper_reg_reg[3] ,
    \lower_reg_reg[2] ,
    \upper_reg_reg[31]_0 ,
    \upper_reg_reg[28] ,
    \upper_reg_reg[27] ,
    \upper_reg_reg[26] ,
    \upper_reg_reg[24] ,
    \upper_reg_reg[22] ,
    \upper_reg_reg[19]_0 ,
    \upper_reg_reg[14] ,
    \upper_reg_reg[13] ,
    \upper_reg_reg[12] ,
    \upper_reg_reg[11] ,
    \upper_reg_reg[9] ,
    \upper_reg_reg[8] ,
    \upper_reg_reg[7] ,
    \upper_reg_reg[6]_0 ,
    \upper_reg_reg[4]_0 ,
    \bb_reg_reg[31]_5 ,
    \lower_reg_reg[30] ,
    \bb_reg_reg[0]_2 ,
    \aa_reg_reg[30]_0 ,
    \bb_reg_reg[0]_3 ,
    \aa_reg_reg[22] ,
    \bb_reg_reg[0]_4 ,
    \aa_reg_reg[20] ,
    \bb_reg_reg[0]_5 ,
    \aa_reg_reg[18] ,
    \bb_reg_reg[0]_6 ,
    \aa_reg_reg[16] ,
    \bb_reg_reg[0]_7 ,
    \aa_reg_reg[14] ,
    \bb_reg_reg[0]_8 ,
    \aa_reg_reg[12] ,
    \bb_reg_reg[0]_9 ,
    \aa_reg_reg[10] ,
    \bb_reg_reg[0]_10 ,
    \aa_reg_reg[8] ,
    \bb_reg_reg[0]_11 ,
    \aa_reg_reg[6] ,
    \bb_reg_reg[0]_12 ,
    \aa_reg_reg[4] ,
    \bb_reg_reg[0]_13 ,
    \aa_reg_reg[2] ,
    \aa_reg_reg[1] ,
    \count_reg_reg[4]_0 ,
    data0,
    sign_reg1_out,
    reset,
    \reset_reg_reg[3] ,
    PLtoPS_readed_data,
    fifo_kernel_to_monitor_data_read,
    fifo_read_instrumentation_data_read,
    fifo_ptm_data_read,
    bram_bbt_annotations_data_read,
    bram_firmware_code_data_read,
    fifo_kernel_to_monitor_almost_full,
    fifo_instrumentation_almost_full,
    fifo_monitor_to_kernel_almost_full,
    fifo_ptm_almost_full,
    fifo_ptm_almost_empty,
    fifo_ptm_full);
  output \address_reg_reg[24]_0 ;
  output \lower_reg_reg[31] ;
  output \lower_reg_reg[31]_0 ;
  output \lower_reg_reg[31]_1 ;
  output \lower_reg_reg[31]_2 ;
  output \pc_reg_reg[25] ;
  output \bb_reg_reg[31] ;
  output intr_enable_reg_reg;
  output \bb_reg_reg[31]_0 ;
  output \bb_reg_reg[31]_1 ;
  output \bb_reg_reg[31]_2 ;
  output [2:0]D;
  output [0:0]\pc_reg_reg[14] ;
  output \bb_reg_reg[1] ;
  output \bb_reg_reg[1]_0 ;
  output \bb_reg_reg[1]_1 ;
  output \bb_reg_reg[1]_2 ;
  output \bb_reg_reg[1]_3 ;
  output \pc_reg_reg[28] ;
  output \aa_reg_reg[30] ;
  output WE;
  output \bb_reg_reg[31]_3 ;
  output [30:0]reg_dest;
  output [0:0]intr_enable_reg_reg_0;
  output [17:0]\bram_sec_policy_config_address[19] ;
  output \pc_reg_reg[3] ;
  output \pc_reg_reg[3]_0 ;
  output \pc_reg_reg[31] ;
  output \pc_reg_reg[30] ;
  output \pc_reg_reg[29] ;
  output \pc_reg_reg[27] ;
  output \pc_reg_reg[26] ;
  output \pc_reg_reg[25]_0 ;
  output \pc_reg_reg[24] ;
  output \pc_reg_reg[23] ;
  output \pc_reg_reg[22] ;
  output \pc_reg_reg[21] ;
  output \pc_reg_reg[20] ;
  output \pc_reg_reg[19] ;
  output \pc_reg_reg[18] ;
  output \pc_reg_reg[17] ;
  output \pc_reg_reg[16] ;
  output \pc_reg_reg[15] ;
  output \pc_reg_reg[14]_0 ;
  output \pc_reg_reg[13] ;
  output \pc_reg_reg[12] ;
  output \pc_reg_reg[11] ;
  output \pc_reg_reg[10] ;
  output \pc_reg_reg[9] ;
  output \pc_reg_reg[8] ;
  output \pc_reg_reg[7] ;
  output \pc_reg_reg[6] ;
  output \pc_reg_reg[5] ;
  output \pc_reg_reg[4] ;
  output \pc_reg_reg[2] ;
  output [31:0]\upper_reg_reg[31] ;
  output [31:0]\lower_reg_reg[31]_3 ;
  output \lower_reg_reg[0] ;
  output [0:0]E;
  output \pc_reg_reg[31]_0 ;
  output [3:0]\bram_firmware_code_bytes_selection[3] ;
  output [3:0]S;
  output [3:0]\pc_reg_reg[28]_0 ;
  output [2:0]\pc_reg_reg[28]_1 ;
  output \pc_reg_reg[30]_0 ;
  output \pc_reg_reg[31]_1 ;
  output \pc_reg_reg[30]_1 ;
  output intr_enable_reg_reg_1;
  output \pc_reg_reg[25]_1 ;
  output \pc_reg_reg[7]_0 ;
  output \pc_reg_reg[2]_0 ;
  output [31:0]\aa_reg_reg[31] ;
  output sign_reg__6;
  output negate_reg_reg;
  output DPRA0;
  output DPRA1;
  output DPRA2;
  output DPRA3;
  output [31:0]\bb_reg_reg[31]_4 ;
  output \count_reg_reg[5] ;
  output [0:0]\aa_reg_reg[31]_0 ;
  output [0:0]\count_reg_reg[5]_0 ;
  output [31:0]\bram_firmware_code_data_write[31] ;
  output intr_enable_reg_reg_2;
  output \bb_reg_reg[1]_4 ;
  output \count_reg_reg[4] ;
  output [0:0]\lower_reg_reg[31]_4 ;
  output sign2_reg;
  output bram_debug_en;
  output bram_sec_policy_config_en;
  output [0:0]\bram_firmware_code_address[2] ;
  output [0:0]\bram_bbt_annotations_address[2] ;
  output bram_sec_policy_config_en_0;
  output [0:0]\fifo_monitor_to_kernel_data_write[31] ;
  output [0:0]\bram_debug_bytes_selection[3] ;
  output [0:0]\PLtoPS_bytes_selection[3] ;
  output [0:0]\PLtoPS_write_data[31] ;
  output [0:0]\bram_sec_policy_config_bytes_selection[3] ;
  output fifo_ptm_en;
  output PLtoPS_generate_interrupt;
  output fifo_monitor_to_kernel_en;
  output fifo_kernel_to_monitor_en;
  output fifo_read_instrumentation_request_read_en;
  output PLtoPS_enable;
  input clk;
  input negate_reg_reg_0;
  input [1:0]Q;
  input [31:0]bram_debug_data_read;
  input [31:0]bram_sec_policy_config_data_read;
  input fifo_kernel_to_monitor_empty;
  input fifo_kernel_to_monitor_almost_empty;
  input fifo_kernel_to_monitor_full;
  input fifo_monitor_to_kernel_empty;
  input fifo_monitor_to_kernel_almost_empty;
  input fifo_monitor_to_kernel_full;
  input fifo_instrumentation_empty;
  input fifo_instrumentation_almost_empty;
  input fifo_instrumentation_full;
  input p_48_in;
  input [31:2]pc_current;
  input intr_enable;
  input DPO;
  input [31:0]data_out1B;
  input DPO122_out;
  input DPO118_out;
  input DPO114_out;
  input DPO110_out;
  input DPO106_out;
  input DPO102_out;
  input DPO98_out;
  input DPO94_out;
  input DPO90_out;
  input DPO86_out;
  input DPO82_out;
  input DPO78_out;
  input DPO74_out;
  input DPO70_out;
  input DPO66_out;
  input DPO62_out;
  input DPO58_out;
  input DPO54_out;
  input DPO50_out;
  input DPO46_out;
  input DPO42_out;
  input DPO38_out;
  input DPO34_out;
  input DPO30_out;
  input DPO26_out;
  input DPO22_out;
  input DPO18_out;
  input DPO14_out;
  input DPO10_out;
  input DPO6_out;
  input DPO2_out;
  input [27:0]pc_plus4;
  input [30:0]\aa_reg_reg[31]_1 ;
  input \aa_reg_reg[28] ;
  input \bb_reg_reg[0] ;
  input \aa_reg_reg[26] ;
  input \bb_reg_reg[0]_0 ;
  input \aa_reg_reg[24] ;
  input \bb_reg_reg[0]_1 ;
  input \pc_reg_reg[29]_0 ;
  input \pc_reg_reg[28]_2 ;
  input sign_reg_reg;
  input [31:0]\lower_reg_reg[31]_5 ;
  input mode_reg_reg;
  input lower_reg0__62;
  input mode_reg_reg_0;
  input neqOp0_in;
  input fifo_ptm_empty;
  input [0:0]CO;
  input [31:0]reg_target;
  input \lower_reg_reg[31]_6 ;
  input \lower_reg_reg[29] ;
  input \lower_reg_reg[28] ;
  input \lower_reg_reg[27] ;
  input \lower_reg_reg[26] ;
  input \lower_reg_reg[26]_0 ;
  input \lower_reg_reg[24] ;
  input \upper_reg_reg[25] ;
  input \lower_reg_reg[22] ;
  input \upper_reg_reg[23] ;
  input \lower_reg_reg[21] ;
  input \upper_reg_reg[21] ;
  input \upper_reg_reg[20] ;
  input \upper_reg_reg[19] ;
  input \upper_reg_reg[18] ;
  input \lower_reg_reg[16] ;
  input \upper_reg_reg[16] ;
  input \lower_reg_reg[13] ;
  input \lower_reg_reg[12] ;
  input \lower_reg_reg[11] ;
  input \lower_reg_reg[11]_0 ;
  input \lower_reg_reg[9] ;
  input \lower_reg_reg[8] ;
  input \lower_reg_reg[7] ;
  input \lower_reg_reg[6] ;
  input \lower_reg_reg[7]_0 ;
  input \upper_reg_reg[6] ;
  input \upper_reg_reg[5] ;
  input \upper_reg_reg[4] ;
  input \upper_reg_reg[3] ;
  input \lower_reg_reg[2] ;
  input [23:0]\upper_reg_reg[31]_0 ;
  input \upper_reg_reg[28] ;
  input \upper_reg_reg[27] ;
  input \upper_reg_reg[26] ;
  input \upper_reg_reg[24] ;
  input \upper_reg_reg[22] ;
  input \upper_reg_reg[19]_0 ;
  input \upper_reg_reg[14] ;
  input \upper_reg_reg[13] ;
  input \upper_reg_reg[12] ;
  input \upper_reg_reg[11] ;
  input \upper_reg_reg[9] ;
  input \upper_reg_reg[8] ;
  input \upper_reg_reg[7] ;
  input \upper_reg_reg[6]_0 ;
  input \upper_reg_reg[4]_0 ;
  input [30:0]\bb_reg_reg[31]_5 ;
  input \lower_reg_reg[30] ;
  input \bb_reg_reg[0]_2 ;
  input \aa_reg_reg[30]_0 ;
  input \bb_reg_reg[0]_3 ;
  input \aa_reg_reg[22] ;
  input \bb_reg_reg[0]_4 ;
  input \aa_reg_reg[20] ;
  input \bb_reg_reg[0]_5 ;
  input \aa_reg_reg[18] ;
  input \bb_reg_reg[0]_6 ;
  input \aa_reg_reg[16] ;
  input \bb_reg_reg[0]_7 ;
  input \aa_reg_reg[14] ;
  input \bb_reg_reg[0]_8 ;
  input \aa_reg_reg[12] ;
  input \bb_reg_reg[0]_9 ;
  input \aa_reg_reg[10] ;
  input \bb_reg_reg[0]_10 ;
  input \aa_reg_reg[8] ;
  input \bb_reg_reg[0]_11 ;
  input \aa_reg_reg[6] ;
  input \bb_reg_reg[0]_12 ;
  input \aa_reg_reg[4] ;
  input \bb_reg_reg[0]_13 ;
  input \aa_reg_reg[2] ;
  input \aa_reg_reg[1] ;
  input \count_reg_reg[4]_0 ;
  input [0:0]data0;
  input sign_reg1_out;
  input reset;
  input [3:0]\reset_reg_reg[3] ;
  input [31:0]PLtoPS_readed_data;
  input [31:0]fifo_kernel_to_monitor_data_read;
  input [31:0]fifo_read_instrumentation_data_read;
  input [31:0]fifo_ptm_data_read;
  input [31:0]bram_bbt_annotations_data_read;
  input [31:0]bram_firmware_code_data_read;
  input fifo_kernel_to_monitor_almost_full;
  input fifo_instrumentation_almost_full;
  input fifo_monitor_to_kernel_almost_full;
  input fifo_ptm_almost_full;
  input fifo_ptm_almost_empty;
  input fifo_ptm_full;

  wire [0:0]CO;
  wire [2:0]D;
  wire DPO;
  wire DPO102_out;
  wire DPO106_out;
  wire DPO10_out;
  wire DPO110_out;
  wire DPO114_out;
  wire DPO118_out;
  wire DPO122_out;
  wire DPO14_out;
  wire DPO18_out;
  wire DPO22_out;
  wire DPO26_out;
  wire DPO2_out;
  wire DPO30_out;
  wire DPO34_out;
  wire DPO38_out;
  wire DPO42_out;
  wire DPO46_out;
  wire DPO50_out;
  wire DPO54_out;
  wire DPO58_out;
  wire DPO62_out;
  wire DPO66_out;
  wire DPO6_out;
  wire DPO70_out;
  wire DPO74_out;
  wire DPO78_out;
  wire DPO82_out;
  wire DPO86_out;
  wire DPO90_out;
  wire DPO94_out;
  wire DPO98_out;
  wire DPRA0;
  wire DPRA1;
  wire DPRA2;
  wire DPRA3;
  wire [0:0]E;
  wire [0:0]\PLtoPS_bytes_selection[3] ;
  wire PLtoPS_enable;
  wire PLtoPS_generate_interrupt;
  wire PLtoPS_generate_interrupt_reg_i_2_n_0;
  wire [31:0]PLtoPS_readed_data;
  wire [0:0]\PLtoPS_write_data[31] ;
  wire \PLtoPS_write_data_reg[31]_i_2_n_0 ;
  wire [1:0]Q;
  wire [3:0]S;
  wire WE;
  wire [31:0]a_bus;
  wire \aa_reg[0]_i_2_n_0 ;
  wire \aa_reg[10]_i_2_n_0 ;
  wire \aa_reg[10]_i_4_n_0 ;
  wire \aa_reg[11]_i_2_n_0 ;
  wire \aa_reg[11]_i_4_n_0 ;
  wire \aa_reg[12]_i_2_n_0 ;
  wire \aa_reg[12]_i_4_n_0 ;
  wire \aa_reg[13]_i_2_n_0 ;
  wire \aa_reg[13]_i_4_n_0 ;
  wire \aa_reg[14]_i_2_n_0 ;
  wire \aa_reg[14]_i_4_n_0 ;
  wire \aa_reg[15]_i_2_n_0 ;
  wire \aa_reg[15]_i_4_n_0 ;
  wire \aa_reg[16]_i_2_n_0 ;
  wire \aa_reg[16]_i_4_n_0 ;
  wire \aa_reg[17]_i_2_n_0 ;
  wire \aa_reg[17]_i_4_n_0 ;
  wire \aa_reg[18]_i_2_n_0 ;
  wire \aa_reg[18]_i_4_n_0 ;
  wire \aa_reg[19]_i_2_n_0 ;
  wire \aa_reg[19]_i_4_n_0 ;
  wire \aa_reg[1]_i_2_n_0 ;
  wire \aa_reg[20]_i_2_n_0 ;
  wire \aa_reg[20]_i_4_n_0 ;
  wire \aa_reg[21]_i_2_n_0 ;
  wire \aa_reg[21]_i_4_n_0 ;
  wire \aa_reg[22]_i_2_n_0 ;
  wire \aa_reg[22]_i_3_n_0 ;
  wire \aa_reg[23]_i_2_n_0 ;
  wire \aa_reg[23]_i_3_n_0 ;
  wire \aa_reg[24]_i_2_n_0 ;
  wire \aa_reg[24]_i_3_n_0 ;
  wire \aa_reg[25]_i_2_n_0 ;
  wire \aa_reg[25]_i_3_n_0 ;
  wire \aa_reg[26]_i_2_n_0 ;
  wire \aa_reg[26]_i_3_n_0 ;
  wire \aa_reg[27]_i_2_n_0 ;
  wire \aa_reg[27]_i_3_n_0 ;
  wire \aa_reg[28]_i_2_n_0 ;
  wire \aa_reg[28]_i_3_n_0 ;
  wire \aa_reg[28]_i_4_n_0 ;
  wire \aa_reg[28]_i_6_n_0 ;
  wire \aa_reg[29]_i_2_n_0 ;
  wire \aa_reg[29]_i_3_n_0 ;
  wire \aa_reg[29]_i_4_n_0 ;
  wire \aa_reg[29]_i_5_n_0 ;
  wire \aa_reg[2]_i_2_n_0 ;
  wire \aa_reg[2]_i_4_n_0 ;
  wire \aa_reg[30]_i_2_n_0 ;
  wire \aa_reg[30]_i_3_n_0 ;
  wire \aa_reg[30]_i_5_n_0 ;
  wire \aa_reg[30]_i_7_n_0 ;
  wire \aa_reg[31]_i_3_n_0 ;
  wire \aa_reg[31]_i_4_n_0 ;
  wire \aa_reg[31]_i_5_n_0 ;
  wire \aa_reg[31]_i_6_n_0 ;
  wire \aa_reg[31]_i_7_n_0 ;
  wire \aa_reg[3]_i_2_n_0 ;
  wire \aa_reg[3]_i_4_n_0 ;
  wire \aa_reg[4]_i_2_n_0 ;
  wire \aa_reg[4]_i_3_n_0 ;
  wire \aa_reg[5]_i_2_n_0 ;
  wire \aa_reg[5]_i_4_n_0 ;
  wire \aa_reg[6]_i_2_n_0 ;
  wire \aa_reg[6]_i_4_n_0 ;
  wire \aa_reg[7]_i_2_n_0 ;
  wire \aa_reg[7]_i_4_n_0 ;
  wire \aa_reg[8]_i_2_n_0 ;
  wire \aa_reg[8]_i_4_n_0 ;
  wire \aa_reg[9]_i_2_n_0 ;
  wire \aa_reg[9]_i_4_n_0 ;
  wire \aa_reg_reg[10] ;
  wire \aa_reg_reg[12] ;
  wire \aa_reg_reg[14] ;
  wire \aa_reg_reg[16] ;
  wire \aa_reg_reg[18] ;
  wire \aa_reg_reg[1] ;
  wire \aa_reg_reg[20] ;
  wire \aa_reg_reg[22] ;
  wire \aa_reg_reg[24] ;
  wire \aa_reg_reg[26] ;
  wire \aa_reg_reg[28] ;
  wire \aa_reg_reg[2] ;
  wire \aa_reg_reg[30] ;
  wire \aa_reg_reg[30]_0 ;
  wire [31:0]\aa_reg_reg[31] ;
  wire [0:0]\aa_reg_reg[31]_0 ;
  wire [30:0]\aa_reg_reg[31]_1 ;
  wire \aa_reg_reg[4] ;
  wire \aa_reg_reg[6] ;
  wire \aa_reg_reg[8] ;
  wire \address_reg[31]_i_4_n_0 ;
  wire \address_reg[31]_i_5_n_0 ;
  wire \address_reg_reg[24]_0 ;
  wire [2:0]alu_funcD;
  wire [31:0]b_bus;
  wire [0:0]b_source;
  wire \bb_reg[10]_i_2_n_0 ;
  wire \bb_reg[11]_i_2_n_0 ;
  wire \bb_reg[12]_i_2_n_0 ;
  wire \bb_reg[13]_i_2_n_0 ;
  wire \bb_reg[14]_i_2_n_0 ;
  wire \bb_reg[15]_i_2_n_0 ;
  wire \bb_reg[16]_i_2_n_0 ;
  wire \bb_reg[17]_i_2_n_0 ;
  wire \bb_reg[18]_i_2_n_0 ;
  wire \bb_reg[19]_i_2_n_0 ;
  wire \bb_reg[20]_i_2_n_0 ;
  wire \bb_reg[21]_i_2_n_0 ;
  wire \bb_reg[22]_i_2_n_0 ;
  wire \bb_reg[23]_i_2_n_0 ;
  wire \bb_reg[24]_i_2_n_0 ;
  wire \bb_reg[25]_i_2_n_0 ;
  wire \bb_reg[26]_i_2_n_0 ;
  wire \bb_reg[27]_i_2_n_0 ;
  wire \bb_reg[28]_i_2_n_0 ;
  wire \bb_reg[29]_i_2_n_0 ;
  wire \bb_reg[2]_i_2_n_0 ;
  wire \bb_reg[31]_i_3_n_0 ;
  wire \bb_reg[31]_i_5_n_0 ;
  wire \bb_reg[31]_i_6_n_0 ;
  wire \bb_reg[31]_i_8_n_0 ;
  wire \bb_reg[31]_i_9_n_0 ;
  wire \bb_reg[3]_i_2_n_0 ;
  wire \bb_reg[4]_i_2_n_0 ;
  wire \bb_reg[5]_i_2_n_0 ;
  wire \bb_reg[6]_i_2_n_0 ;
  wire \bb_reg[7]_i_2_n_0 ;
  wire \bb_reg[8]_i_2_n_0 ;
  wire \bb_reg[9]_i_2_n_0 ;
  wire \bb_reg_reg[0] ;
  wire \bb_reg_reg[0]_0 ;
  wire \bb_reg_reg[0]_1 ;
  wire \bb_reg_reg[0]_10 ;
  wire \bb_reg_reg[0]_11 ;
  wire \bb_reg_reg[0]_12 ;
  wire \bb_reg_reg[0]_13 ;
  wire \bb_reg_reg[0]_2 ;
  wire \bb_reg_reg[0]_3 ;
  wire \bb_reg_reg[0]_4 ;
  wire \bb_reg_reg[0]_5 ;
  wire \bb_reg_reg[0]_6 ;
  wire \bb_reg_reg[0]_7 ;
  wire \bb_reg_reg[0]_8 ;
  wire \bb_reg_reg[0]_9 ;
  wire \bb_reg_reg[1] ;
  wire \bb_reg_reg[1]_0 ;
  wire \bb_reg_reg[1]_1 ;
  wire \bb_reg_reg[1]_2 ;
  wire \bb_reg_reg[1]_3 ;
  wire \bb_reg_reg[1]_4 ;
  wire \bb_reg_reg[31] ;
  wire \bb_reg_reg[31]_0 ;
  wire \bb_reg_reg[31]_1 ;
  wire \bb_reg_reg[31]_2 ;
  wire \bb_reg_reg[31]_3 ;
  wire [31:0]\bb_reg_reg[31]_4 ;
  wire [30:0]\bb_reg_reg[31]_5 ;
  wire [0:0]\bram_bbt_annotations_address[2] ;
  wire [31:0]bram_bbt_annotations_data_read;
  wire [0:0]\bram_debug_bytes_selection[3] ;
  wire [31:0]bram_debug_data_read;
  wire bram_debug_en;
  wire [0:0]\bram_firmware_code_address[2] ;
  wire \bram_firmware_code_address_reg[10]_i_10_n_0 ;
  wire \bram_firmware_code_address_reg[10]_i_16_n_0 ;
  wire \bram_firmware_code_address_reg[10]_i_6_n_0 ;
  wire \bram_firmware_code_address_reg[10]_i_8_n_0 ;
  wire \bram_firmware_code_address_reg[11]_i_15_n_0 ;
  wire \bram_firmware_code_address_reg[11]_i_6_n_0 ;
  wire \bram_firmware_code_address_reg[11]_i_9_n_0 ;
  wire \bram_firmware_code_address_reg[12]_i_16_n_0 ;
  wire \bram_firmware_code_address_reg[12]_i_6_n_0 ;
  wire \bram_firmware_code_address_reg[12]_i_8_n_0 ;
  wire \bram_firmware_code_address_reg[13]_i_16_n_0 ;
  wire \bram_firmware_code_address_reg[13]_i_6_n_0 ;
  wire \bram_firmware_code_address_reg[13]_i_8_n_0 ;
  wire \bram_firmware_code_address_reg[14]_i_17_n_0 ;
  wire \bram_firmware_code_address_reg[14]_i_30_n_0 ;
  wire \bram_firmware_code_address_reg[14]_i_31_n_0 ;
  wire \bram_firmware_code_address_reg[14]_i_32_n_0 ;
  wire \bram_firmware_code_address_reg[14]_i_33_n_0 ;
  wire \bram_firmware_code_address_reg[14]_i_6_n_0 ;
  wire \bram_firmware_code_address_reg[14]_i_9_n_0 ;
  wire \bram_firmware_code_address_reg[15]_i_10_n_0 ;
  wire \bram_firmware_code_address_reg[15]_i_15_n_0 ;
  wire \bram_firmware_code_address_reg[15]_i_20_n_0 ;
  wire \bram_firmware_code_address_reg[15]_i_23_n_0 ;
  wire \bram_firmware_code_address_reg[15]_i_7_n_0 ;
  wire \bram_firmware_code_address_reg[15]_i_8_n_0 ;
  wire \bram_firmware_code_address_reg[16]_i_12_n_0 ;
  wire \bram_firmware_code_address_reg[16]_i_7_n_0 ;
  wire \bram_firmware_code_address_reg[17]_i_10_n_0 ;
  wire \bram_firmware_code_address_reg[17]_i_11_n_0 ;
  wire \bram_firmware_code_address_reg[17]_i_13_n_0 ;
  wire \bram_firmware_code_address_reg[17]_i_15_n_0 ;
  wire \bram_firmware_code_address_reg[17]_i_20_n_0 ;
  wire \bram_firmware_code_address_reg[17]_i_22_n_0 ;
  wire \bram_firmware_code_address_reg[17]_i_25_n_0 ;
  wire \bram_firmware_code_address_reg[17]_i_26_n_0 ;
  wire \bram_firmware_code_address_reg[17]_i_27_n_0 ;
  wire \bram_firmware_code_address_reg[17]_i_28_n_0 ;
  wire \bram_firmware_code_address_reg[17]_i_4_n_0 ;
  wire \bram_firmware_code_address_reg[17]_i_9_n_0 ;
  wire \bram_firmware_code_address_reg[18]_i_7_n_0 ;
  wire \bram_firmware_code_address_reg[19]_i_10_n_0 ;
  wire \bram_firmware_code_address_reg[19]_i_11_n_0 ;
  wire \bram_firmware_code_address_reg[19]_i_13_n_0 ;
  wire \bram_firmware_code_address_reg[19]_i_14_n_0 ;
  wire \bram_firmware_code_address_reg[19]_i_17_n_0 ;
  wire \bram_firmware_code_address_reg[19]_i_2_n_0 ;
  wire \bram_firmware_code_address_reg[19]_i_32_n_0 ;
  wire \bram_firmware_code_address_reg[19]_i_33_n_0 ;
  wire \bram_firmware_code_address_reg[19]_i_34_n_0 ;
  wire \bram_firmware_code_address_reg[19]_i_35_n_0 ;
  wire \bram_firmware_code_address_reg[19]_i_6_n_0 ;
  wire \bram_firmware_code_address_reg[2]_i_8_n_0 ;
  wire \bram_firmware_code_address_reg[2]_i_9_n_0 ;
  wire \bram_firmware_code_address_reg[3]_i_7_n_0 ;
  wire \bram_firmware_code_address_reg[3]_i_9_n_0 ;
  wire \bram_firmware_code_address_reg[4]_i_8_n_0 ;
  wire \bram_firmware_code_address_reg[4]_i_9_n_0 ;
  wire \bram_firmware_code_address_reg[5]_i_7_n_0 ;
  wire \bram_firmware_code_address_reg[5]_i_9_n_0 ;
  wire \bram_firmware_code_address_reg[6]_i_8_n_0 ;
  wire \bram_firmware_code_address_reg[6]_i_9_n_0 ;
  wire \bram_firmware_code_address_reg[7]_i_8_n_0 ;
  wire \bram_firmware_code_address_reg[7]_i_9_n_0 ;
  wire \bram_firmware_code_address_reg[8]_i_15_n_0 ;
  wire \bram_firmware_code_address_reg[8]_i_6_n_0 ;
  wire \bram_firmware_code_address_reg[8]_i_8_n_0 ;
  wire \bram_firmware_code_address_reg[9]_i_10_n_0 ;
  wire \bram_firmware_code_address_reg[9]_i_16_n_0 ;
  wire \bram_firmware_code_address_reg[9]_i_6_n_0 ;
  wire \bram_firmware_code_address_reg[9]_i_9_n_0 ;
  wire [3:0]\bram_firmware_code_bytes_selection[3] ;
  wire \bram_firmware_code_bytes_selection_reg[0]_i_2_n_0 ;
  wire \bram_firmware_code_bytes_selection_reg[1]_i_2_n_0 ;
  wire \bram_firmware_code_bytes_selection_reg[2]_i_2_n_0 ;
  wire \bram_firmware_code_bytes_selection_reg[3]_i_2_n_0 ;
  wire [31:0]bram_firmware_code_data_read;
  wire [31:0]\bram_firmware_code_data_write[31] ;
  wire \bram_firmware_code_data_write_reg[31]_i_10_n_0 ;
  wire \bram_firmware_code_data_write_reg[31]_i_11_n_0 ;
  wire \bram_firmware_code_data_write_reg[31]_i_2_n_0 ;
  wire [17:0]\bram_sec_policy_config_address[19] ;
  wire [0:0]\bram_sec_policy_config_bytes_selection[3] ;
  wire [31:0]bram_sec_policy_config_data_read;
  wire bram_sec_policy_config_en;
  wire bram_sec_policy_config_en_0;
  wire [1:1]branch_func;
  wire bv_adder1474_out;
  wire bv_adder14__0;
  wire bv_adder1598_out;
  wire bv_adder2459_out;
  wire bv_adder24__0;
  wire bv_adder2578_out;
  wire bv_adder3444_out;
  wire bv_adder34__0;
  wire bv_adder3558_out;
  wire bv_adder4429_out;
  wire bv_adder44__0;
  wire bv_adder4538_out;
  wire bv_adder489_out;
  wire bv_adder4__0;
  wire bv_adder5118_out;
  wire bv_adder5414_out;
  wire bv_adder54__0;
  wire bv_adder5518_out;
  wire [31:2]c_alu;
  wire [31:1]c_mult;
  wire [31:0]c_shift;
  wire [1:1]c_source;
  wire clk;
  wire [31:20]complete_address_next;
  wire \count_reg[5]_i_12_n_0 ;
  wire \count_reg[5]_i_13_n_0 ;
  wire \count_reg[5]_i_14_n_0 ;
  wire \count_reg[5]_i_15_n_0 ;
  wire \count_reg[5]_i_16_n_0 ;
  wire \count_reg[5]_i_17_n_0 ;
  wire \count_reg[5]_i_18_n_0 ;
  wire \count_reg_reg[4] ;
  wire \count_reg_reg[4]_0 ;
  wire \count_reg_reg[5] ;
  wire [0:0]\count_reg_reg[5]_0 ;
  wire [3:0]cpu_address_misc_ip_selection_of_memory;
  wire [3:0]cpu_address_misc_signals_selection_of_memory;
  wire [3:0]cpu_address_selection_of_memory_area;
  wire [31:0]cpu_data_r;
  wire [0:0]data0;
  wire [31:0]data_out1B;
  wire [15:15]data_read_var__15;
  wire eqOp;
  wire fifo_instrumentation_almost_empty;
  wire fifo_instrumentation_almost_full;
  wire fifo_instrumentation_empty;
  wire fifo_instrumentation_full;
  wire fifo_kernel_to_monitor_almost_empty;
  wire fifo_kernel_to_monitor_almost_full;
  wire [31:0]fifo_kernel_to_monitor_data_read;
  wire fifo_kernel_to_monitor_empty;
  wire fifo_kernel_to_monitor_en;
  wire fifo_kernel_to_monitor_full;
  wire fifo_monitor_to_kernel_almost_empty;
  wire fifo_monitor_to_kernel_almost_full;
  wire [0:0]\fifo_monitor_to_kernel_data_write[31] ;
  wire fifo_monitor_to_kernel_empty;
  wire fifo_monitor_to_kernel_en;
  wire fifo_monitor_to_kernel_full;
  wire fifo_ptm_almost_empty;
  wire fifo_ptm_almost_full;
  wire [31:0]fifo_ptm_data_read;
  wire fifo_ptm_empty;
  wire fifo_ptm_en;
  wire fifo_ptm_en_reg_i_2_n_0;
  wire fifo_ptm_en_reg_i_3_n_0;
  wire fifo_ptm_en_reg_i_4_n_0;
  wire fifo_ptm_en_reg_i_5_n_0;
  wire fifo_ptm_full;
  wire [31:0]fifo_read_instrumentation_data_read;
  wire fifo_read_instrumentation_request_read_en;
  wire intr_enable;
  wire intr_enable_reg_reg;
  wire [0:0]intr_enable_reg_reg_0;
  wire intr_enable_reg_reg_1;
  wire intr_enable_reg_reg_2;
  wire lower_reg0__62;
  wire \lower_reg[0]_i_2_n_0 ;
  wire \lower_reg[10]_i_2_n_0 ;
  wire \lower_reg[11]_i_2_n_0 ;
  wire \lower_reg[12]_i_2_n_0 ;
  wire \lower_reg[13]_i_2_n_0 ;
  wire \lower_reg[14]_i_2_n_0 ;
  wire \lower_reg[15]_i_2_n_0 ;
  wire \lower_reg[16]_i_2_n_0 ;
  wire \lower_reg[17]_i_2_n_0 ;
  wire \lower_reg[18]_i_2_n_0 ;
  wire \lower_reg[19]_i_2_n_0 ;
  wire \lower_reg[1]_i_2_n_0 ;
  wire \lower_reg[20]_i_2_n_0 ;
  wire \lower_reg[21]_i_2_n_0 ;
  wire \lower_reg[22]_i_2_n_0 ;
  wire \lower_reg[23]_i_2_n_0 ;
  wire \lower_reg[24]_i_2_n_0 ;
  wire \lower_reg[25]_i_2_n_0 ;
  wire \lower_reg[26]_i_2_n_0 ;
  wire \lower_reg[27]_i_2_n_0 ;
  wire \lower_reg[28]_i_2_n_0 ;
  wire \lower_reg[29]_i_2_n_0 ;
  wire \lower_reg[2]_i_2_n_0 ;
  wire \lower_reg[30]_i_2_n_0 ;
  wire \lower_reg[31]_i_3_n_0 ;
  wire \lower_reg[3]_i_2_n_0 ;
  wire \lower_reg[4]_i_2_n_0 ;
  wire \lower_reg[5]_i_2_n_0 ;
  wire \lower_reg[6]_i_2_n_0 ;
  wire \lower_reg[7]_i_2_n_0 ;
  wire \lower_reg[8]_i_2_n_0 ;
  wire \lower_reg[9]_i_2_n_0 ;
  wire \lower_reg_reg[0] ;
  wire \lower_reg_reg[11] ;
  wire \lower_reg_reg[11]_0 ;
  wire \lower_reg_reg[12] ;
  wire \lower_reg_reg[13] ;
  wire \lower_reg_reg[16] ;
  wire \lower_reg_reg[21] ;
  wire \lower_reg_reg[22] ;
  wire \lower_reg_reg[24] ;
  wire \lower_reg_reg[26] ;
  wire \lower_reg_reg[26]_0 ;
  wire \lower_reg_reg[27] ;
  wire \lower_reg_reg[28] ;
  wire \lower_reg_reg[29] ;
  wire \lower_reg_reg[2] ;
  wire \lower_reg_reg[30] ;
  wire \lower_reg_reg[31] ;
  wire \lower_reg_reg[31]_0 ;
  wire \lower_reg_reg[31]_1 ;
  wire \lower_reg_reg[31]_2 ;
  wire [31:0]\lower_reg_reg[31]_3 ;
  wire [0:0]\lower_reg_reg[31]_4 ;
  wire [31:0]\lower_reg_reg[31]_5 ;
  wire \lower_reg_reg[31]_6 ;
  wire \lower_reg_reg[6] ;
  wire \lower_reg_reg[7] ;
  wire \lower_reg_reg[7]_0 ;
  wire \lower_reg_reg[8] ;
  wire \lower_reg_reg[9] ;
  wire [3:0]mem_source;
  wire mem_state_reg;
  wire mem_state_reg_i_2_n_0;
  wire mem_state_reg_reg_n_0;
  wire mode_reg_reg;
  wire mode_reg_reg_0;
  wire negate_reg_reg;
  wire negate_reg_reg_0;
  wire neqOp0_in;
  wire \next_opcode_reg[0]_i_10_n_0 ;
  wire \next_opcode_reg[0]_i_11_n_0 ;
  wire \next_opcode_reg[0]_i_12_n_0 ;
  wire \next_opcode_reg[0]_i_13_n_0 ;
  wire \next_opcode_reg[0]_i_14_n_0 ;
  wire \next_opcode_reg[0]_i_2_n_0 ;
  wire \next_opcode_reg[0]_i_3_n_0 ;
  wire \next_opcode_reg[0]_i_4_n_0 ;
  wire \next_opcode_reg[0]_i_5_n_0 ;
  wire \next_opcode_reg[0]_i_6_n_0 ;
  wire \next_opcode_reg[0]_i_7_n_0 ;
  wire \next_opcode_reg[0]_i_8_n_0 ;
  wire \next_opcode_reg[0]_i_9_n_0 ;
  wire \next_opcode_reg[10]_i_2_n_0 ;
  wire \next_opcode_reg[10]_i_3_n_0 ;
  wire \next_opcode_reg[10]_i_4_n_0 ;
  wire \next_opcode_reg[11]_i_2_n_0 ;
  wire \next_opcode_reg[11]_i_3_n_0 ;
  wire \next_opcode_reg[11]_i_4_n_0 ;
  wire \next_opcode_reg[12]_i_2_n_0 ;
  wire \next_opcode_reg[12]_i_3_n_0 ;
  wire \next_opcode_reg[12]_i_4_n_0 ;
  wire \next_opcode_reg[13]_i_2_n_0 ;
  wire \next_opcode_reg[13]_i_3_n_0 ;
  wire \next_opcode_reg[13]_i_4_n_0 ;
  wire \next_opcode_reg[14]_i_2_n_0 ;
  wire \next_opcode_reg[14]_i_3_n_0 ;
  wire \next_opcode_reg[14]_i_4_n_0 ;
  wire \next_opcode_reg[15]_i_2_n_0 ;
  wire \next_opcode_reg[15]_i_3_n_0 ;
  wire \next_opcode_reg[15]_i_4_n_0 ;
  wire \next_opcode_reg[16]_i_2_n_0 ;
  wire \next_opcode_reg[16]_i_3_n_0 ;
  wire \next_opcode_reg[16]_i_4_n_0 ;
  wire \next_opcode_reg[17]_i_2_n_0 ;
  wire \next_opcode_reg[17]_i_3_n_0 ;
  wire \next_opcode_reg[17]_i_4_n_0 ;
  wire \next_opcode_reg[18]_i_2_n_0 ;
  wire \next_opcode_reg[18]_i_3_n_0 ;
  wire \next_opcode_reg[18]_i_4_n_0 ;
  wire \next_opcode_reg[19]_i_2_n_0 ;
  wire \next_opcode_reg[19]_i_3_n_0 ;
  wire \next_opcode_reg[19]_i_4_n_0 ;
  wire \next_opcode_reg[1]_i_2_n_0 ;
  wire \next_opcode_reg[1]_i_3_n_0 ;
  wire \next_opcode_reg[1]_i_4_n_0 ;
  wire \next_opcode_reg[20]_i_2_n_0 ;
  wire \next_opcode_reg[20]_i_3_n_0 ;
  wire \next_opcode_reg[20]_i_4_n_0 ;
  wire \next_opcode_reg[21]_i_2_n_0 ;
  wire \next_opcode_reg[21]_i_3_n_0 ;
  wire \next_opcode_reg[21]_i_4_n_0 ;
  wire \next_opcode_reg[22]_i_2_n_0 ;
  wire \next_opcode_reg[22]_i_3_n_0 ;
  wire \next_opcode_reg[22]_i_4_n_0 ;
  wire \next_opcode_reg[23]_i_2_n_0 ;
  wire \next_opcode_reg[23]_i_3_n_0 ;
  wire \next_opcode_reg[23]_i_4_n_0 ;
  wire \next_opcode_reg[24]_i_2_n_0 ;
  wire \next_opcode_reg[24]_i_3_n_0 ;
  wire \next_opcode_reg[24]_i_4_n_0 ;
  wire \next_opcode_reg[25]_i_2_n_0 ;
  wire \next_opcode_reg[25]_i_3_n_0 ;
  wire \next_opcode_reg[25]_i_4_n_0 ;
  wire \next_opcode_reg[26]_i_2_n_0 ;
  wire \next_opcode_reg[26]_i_3_n_0 ;
  wire \next_opcode_reg[26]_i_4_n_0 ;
  wire \next_opcode_reg[27]_i_2_n_0 ;
  wire \next_opcode_reg[27]_i_3_n_0 ;
  wire \next_opcode_reg[27]_i_4_n_0 ;
  wire \next_opcode_reg[28]_i_2_n_0 ;
  wire \next_opcode_reg[28]_i_3_n_0 ;
  wire \next_opcode_reg[28]_i_4_n_0 ;
  wire \next_opcode_reg[29]_i_2_n_0 ;
  wire \next_opcode_reg[29]_i_3_n_0 ;
  wire \next_opcode_reg[29]_i_4_n_0 ;
  wire \next_opcode_reg[2]_i_2_n_0 ;
  wire \next_opcode_reg[2]_i_3_n_0 ;
  wire \next_opcode_reg[2]_i_4_n_0 ;
  wire \next_opcode_reg[30]_i_2_n_0 ;
  wire \next_opcode_reg[30]_i_3_n_0 ;
  wire \next_opcode_reg[30]_i_4_n_0 ;
  wire \next_opcode_reg[31]_i_1_n_0 ;
  wire \next_opcode_reg[31]_i_3_n_0 ;
  wire \next_opcode_reg[31]_i_4_n_0 ;
  wire \next_opcode_reg[31]_i_5_n_0 ;
  wire \next_opcode_reg[3]_i_2_n_0 ;
  wire \next_opcode_reg[3]_i_3_n_0 ;
  wire \next_opcode_reg[3]_i_4_n_0 ;
  wire \next_opcode_reg[4]_i_2_n_0 ;
  wire \next_opcode_reg[4]_i_3_n_0 ;
  wire \next_opcode_reg[4]_i_4_n_0 ;
  wire \next_opcode_reg[5]_i_2_n_0 ;
  wire \next_opcode_reg[5]_i_3_n_0 ;
  wire \next_opcode_reg[5]_i_4_n_0 ;
  wire \next_opcode_reg[6]_i_2_n_0 ;
  wire \next_opcode_reg[6]_i_3_n_0 ;
  wire \next_opcode_reg[6]_i_4_n_0 ;
  wire \next_opcode_reg[7]_i_2_n_0 ;
  wire \next_opcode_reg[7]_i_3_n_0 ;
  wire \next_opcode_reg[7]_i_4_n_0 ;
  wire \next_opcode_reg[8]_i_2_n_0 ;
  wire \next_opcode_reg[8]_i_3_n_0 ;
  wire \next_opcode_reg[8]_i_4_n_0 ;
  wire \next_opcode_reg[9]_i_2_n_0 ;
  wire \next_opcode_reg[9]_i_3_n_0 ;
  wire \next_opcode_reg[9]_i_4_n_0 ;
  wire \next_opcode_reg_reg_n_0_[0] ;
  wire \next_opcode_reg_reg_n_0_[10] ;
  wire \next_opcode_reg_reg_n_0_[11] ;
  wire \next_opcode_reg_reg_n_0_[12] ;
  wire \next_opcode_reg_reg_n_0_[13] ;
  wire \next_opcode_reg_reg_n_0_[14] ;
  wire \next_opcode_reg_reg_n_0_[15] ;
  wire \next_opcode_reg_reg_n_0_[16] ;
  wire \next_opcode_reg_reg_n_0_[17] ;
  wire \next_opcode_reg_reg_n_0_[18] ;
  wire \next_opcode_reg_reg_n_0_[19] ;
  wire \next_opcode_reg_reg_n_0_[1] ;
  wire \next_opcode_reg_reg_n_0_[20] ;
  wire \next_opcode_reg_reg_n_0_[21] ;
  wire \next_opcode_reg_reg_n_0_[22] ;
  wire \next_opcode_reg_reg_n_0_[23] ;
  wire \next_opcode_reg_reg_n_0_[24] ;
  wire \next_opcode_reg_reg_n_0_[25] ;
  wire \next_opcode_reg_reg_n_0_[26] ;
  wire \next_opcode_reg_reg_n_0_[27] ;
  wire \next_opcode_reg_reg_n_0_[28] ;
  wire \next_opcode_reg_reg_n_0_[29] ;
  wire \next_opcode_reg_reg_n_0_[2] ;
  wire \next_opcode_reg_reg_n_0_[30] ;
  wire \next_opcode_reg_reg_n_0_[31] ;
  wire \next_opcode_reg_reg_n_0_[3] ;
  wire \next_opcode_reg_reg_n_0_[4] ;
  wire \next_opcode_reg_reg_n_0_[5] ;
  wire \next_opcode_reg_reg_n_0_[6] ;
  wire \next_opcode_reg_reg_n_0_[7] ;
  wire \next_opcode_reg_reg_n_0_[8] ;
  wire \next_opcode_reg_reg_n_0_[9] ;
  wire [31:0]opcode;
  wire opcode_next1__0;
  wire \opcode_reg[0]_i_1_n_0 ;
  wire \opcode_reg[10]_i_1_n_0 ;
  wire \opcode_reg[11]_i_1_n_0 ;
  wire \opcode_reg[12]_i_1_n_0 ;
  wire \opcode_reg[13]_i_1_n_0 ;
  wire \opcode_reg[14]_i_1_n_0 ;
  wire \opcode_reg[15]_i_1_n_0 ;
  wire \opcode_reg[16]_i_1_n_0 ;
  wire \opcode_reg[17]_i_1_n_0 ;
  wire \opcode_reg[18]_i_1_n_0 ;
  wire \opcode_reg[19]_i_1_n_0 ;
  wire \opcode_reg[1]_i_1_n_0 ;
  wire \opcode_reg[20]_i_1_n_0 ;
  wire \opcode_reg[21]_i_1_n_0 ;
  wire \opcode_reg[22]_i_1_n_0 ;
  wire \opcode_reg[23]_i_1_n_0 ;
  wire \opcode_reg[24]_i_1_n_0 ;
  wire \opcode_reg[25]_i_1_n_0 ;
  wire \opcode_reg[26]_i_1_n_0 ;
  wire \opcode_reg[27]_i_1_n_0 ;
  wire \opcode_reg[28]_i_1_n_0 ;
  wire \opcode_reg[29]_i_1_n_0 ;
  wire \opcode_reg[2]_i_1_n_0 ;
  wire \opcode_reg[30]_i_1_n_0 ;
  wire \opcode_reg[31]_i_1_n_0 ;
  wire \opcode_reg[31]_i_2_n_0 ;
  wire \opcode_reg[31]_i_3_n_0 ;
  wire \opcode_reg[31]_i_5_n_0 ;
  wire \opcode_reg[3]_i_1_n_0 ;
  wire \opcode_reg[4]_i_1_n_0 ;
  wire \opcode_reg[5]_i_1_n_0 ;
  wire \opcode_reg[6]_i_1_n_0 ;
  wire \opcode_reg[7]_i_1_n_0 ;
  wire \opcode_reg[8]_i_1_n_0 ;
  wire \opcode_reg[9]_i_1_n_0 ;
  wire p_0_in;
  wire p_48_in;
  wire pause_in;
  wire pause_in0_out;
  wire [31:2]pc_current;
  wire [31:2]pc_future;
  wire [31:2]pc_new;
  wire [27:0]pc_plus4;
  wire \pc_reg[20]_i_7_n_0 ;
  wire \pc_reg[21]_i_7_n_0 ;
  wire \pc_reg[22]_i_12_n_0 ;
  wire \pc_reg[22]_i_5_n_0 ;
  wire \pc_reg[23]_i_7_n_0 ;
  wire \pc_reg[24]_i_11_n_0 ;
  wire \pc_reg[24]_i_15_n_0 ;
  wire \pc_reg[24]_i_20_n_0 ;
  wire \pc_reg[24]_i_21_n_0 ;
  wire \pc_reg[24]_i_22_n_0 ;
  wire \pc_reg[24]_i_23_n_0 ;
  wire \pc_reg[24]_i_4_n_0 ;
  wire \pc_reg[24]_i_7_n_0 ;
  wire \pc_reg[24]_i_8_n_0 ;
  wire \pc_reg[25]_i_7_n_0 ;
  wire \pc_reg[26]_i_10_n_0 ;
  wire \pc_reg[26]_i_12_n_0 ;
  wire \pc_reg[26]_i_4_n_0 ;
  wire \pc_reg[26]_i_7_n_0 ;
  wire \pc_reg[26]_i_8_n_0 ;
  wire \pc_reg[27]_i_11_n_0 ;
  wire \pc_reg[27]_i_15_n_0 ;
  wire \pc_reg[27]_i_19_n_0 ;
  wire \pc_reg[27]_i_20_n_0 ;
  wire \pc_reg[27]_i_21_n_0 ;
  wire \pc_reg[27]_i_22_n_0 ;
  wire \pc_reg[27]_i_4_n_0 ;
  wire \pc_reg[27]_i_7_n_0 ;
  wire \pc_reg[27]_i_8_n_0 ;
  wire \pc_reg[28]_i_15_n_0 ;
  wire \pc_reg[28]_i_7_n_0 ;
  wire \pc_reg[28]_i_9_n_0 ;
  wire \pc_reg[29]_i_10_n_0 ;
  wire \pc_reg[29]_i_13_n_0 ;
  wire \pc_reg[29]_i_4_n_0 ;
  wire \pc_reg[29]_i_7_n_0 ;
  wire \pc_reg[29]_i_8_n_0 ;
  wire \pc_reg[30]_i_10_n_0 ;
  wire \pc_reg[30]_i_4_n_0 ;
  wire \pc_reg[31]_i_10_n_0 ;
  wire \pc_reg[31]_i_12_n_0 ;
  wire \pc_reg[31]_i_16_n_0 ;
  wire \pc_reg[31]_i_17_n_0 ;
  wire \pc_reg[31]_i_18_n_0 ;
  wire \pc_reg[31]_i_19_n_0 ;
  wire \pc_reg[31]_i_20_n_0 ;
  wire \pc_reg[31]_i_21_n_0 ;
  wire \pc_reg[31]_i_23_n_0 ;
  wire \pc_reg[31]_i_25_n_0 ;
  wire \pc_reg[31]_i_28_n_0 ;
  wire \pc_reg[3]_i_3_n_0 ;
  wire \pc_reg[3]_i_4_n_0 ;
  wire \pc_reg[3]_i_5_n_0 ;
  wire \pc_reg_reg[10] ;
  wire \pc_reg_reg[11] ;
  wire \pc_reg_reg[12] ;
  wire \pc_reg_reg[13] ;
  wire [0:0]\pc_reg_reg[14] ;
  wire \pc_reg_reg[14]_0 ;
  wire \pc_reg_reg[15] ;
  wire \pc_reg_reg[16] ;
  wire \pc_reg_reg[17] ;
  wire \pc_reg_reg[18] ;
  wire \pc_reg_reg[19] ;
  wire \pc_reg_reg[20] ;
  wire \pc_reg_reg[21] ;
  wire \pc_reg_reg[22] ;
  wire \pc_reg_reg[23] ;
  wire \pc_reg_reg[24] ;
  wire \pc_reg_reg[25] ;
  wire \pc_reg_reg[25]_0 ;
  wire \pc_reg_reg[25]_1 ;
  wire \pc_reg_reg[26] ;
  wire \pc_reg_reg[27] ;
  wire \pc_reg_reg[28] ;
  wire [3:0]\pc_reg_reg[28]_0 ;
  wire [2:0]\pc_reg_reg[28]_1 ;
  wire \pc_reg_reg[28]_2 ;
  wire \pc_reg_reg[29] ;
  wire \pc_reg_reg[29]_0 ;
  wire \pc_reg_reg[2] ;
  wire \pc_reg_reg[2]_0 ;
  wire \pc_reg_reg[30] ;
  wire \pc_reg_reg[30]_0 ;
  wire \pc_reg_reg[30]_1 ;
  wire \pc_reg_reg[31] ;
  wire \pc_reg_reg[31]_0 ;
  wire \pc_reg_reg[31]_1 ;
  wire \pc_reg_reg[3] ;
  wire \pc_reg_reg[3]_0 ;
  wire \pc_reg_reg[4] ;
  wire \pc_reg_reg[5] ;
  wire \pc_reg_reg[6] ;
  wire \pc_reg_reg[7] ;
  wire \pc_reg_reg[7]_0 ;
  wire \pc_reg_reg[8] ;
  wire \pc_reg_reg[9] ;
  wire [1:0]pc_source;
  wire [5:0]rd_indexD;
  wire [30:0]reg_dest;
  wire [31:0]reg_source;
  wire [31:0]reg_target;
  wire reset;
  wire [3:0]\reset_reg_reg[3] ;
  wire [5:0]rs_index;
  wire [28:16]shift16L;
  wire [30:30]shift16R;
  wire [28:26]shift1L;
  wire [30:30]shift1R;
  wire [31:2]shift2L;
  wire [30:0]shift2R;
  wire [28:4]shift4L;
  wire [31:8]shift4R;
  wire [31:8]shift8L;
  wire [31:0]shift8R;
  wire sign2_reg;
  wire sign_reg1_out;
  wire sign_reg__6;
  wire sign_reg_i_10_n_0;
  wire sign_reg_i_11_n_0;
  wire sign_reg_i_12_n_0;
  wire sign_reg_i_4_n_0;
  wire sign_reg_i_5_n_0;
  wire sign_reg_i_6_n_0;
  wire sign_reg_i_7_n_0;
  wire sign_reg_i_9_n_0;
  wire sign_reg_reg;
  wire take_branch;
  wire take_branch1_carry_i_17_n_0;
  wire take_branch1_carry_i_18_n_0;
  wire [3:0]\u3_control/alu_function__23 ;
  wire [2:0]\u3_control/c_source__14 ;
  wire \u3_control/is_syscall__2 ;
  wire [4:0]\u3_control/rd__4 ;
  wire \u4_reg_bank/write_enable ;
  wire \u6_alu/bv_adder013_out ;
  wire \u6_alu/bv_adder01_out ;
  wire \u6_alu/bv_adder022_out ;
  wire \u6_alu/bv_adder028_out ;
  wire \u6_alu/bv_adder034_out ;
  wire \u6_alu/bv_adder037_out ;
  wire \u6_alu/bv_adder043_out ;
  wire \u6_alu/bv_adder049_out ;
  wire \u6_alu/bv_adder052_out ;
  wire \u6_alu/bv_adder058_out ;
  wire \u6_alu/bv_adder064_out ;
  wire \u6_alu/bv_adder067_out ;
  wire \u6_alu/bv_adder073_out ;
  wire \u6_alu/bv_adder07_out ;
  wire \u6_alu/bv_adder082_out ;
  wire \u6_alu/bv_adder088_out ;
  wire \u6_alu/bv_adder0__1 ;
  wire \u6_alu/bv_adder11__3 ;
  wire \u6_alu/bv_adder13__3 ;
  wire \u6_alu/bv_adder15__3 ;
  wire \u6_alu/bv_adder17__3 ;
  wire \u6_alu/bv_adder19__3 ;
  wire \u6_alu/bv_adder21__3 ;
  wire \u6_alu/bv_adder23__3 ;
  wire \u6_alu/bv_adder25__3 ;
  wire \u6_alu/bv_adder27__3 ;
  wire \u6_alu/bv_adder29__3 ;
  wire \u6_alu/bv_adder31__3 ;
  wire \u6_alu/bv_adder33__3 ;
  wire \u6_alu/bv_adder35__3 ;
  wire \u6_alu/bv_adder37__3 ;
  wire \u6_alu/bv_adder39__3 ;
  wire \u6_alu/bv_adder3__3 ;
  wire \u6_alu/bv_adder41__3 ;
  wire \u6_alu/bv_adder43__3 ;
  wire \u6_alu/bv_adder45__3 ;
  wire \u6_alu/bv_adder47__3 ;
  wire \u6_alu/bv_adder49__3 ;
  wire \u6_alu/bv_adder51__3 ;
  wire \u6_alu/bv_adder53__3 ;
  wire \u6_alu/bv_adder55__3 ;
  wire \u6_alu/bv_adder57__3 ;
  wire \u6_alu/bv_adder59__3 ;
  wire \u6_alu/bv_adder61__3 ;
  wire \u6_alu/bv_adder63__3 ;
  wire \u6_alu/bv_adder7__3 ;
  wire \u6_alu/bv_adder9__3 ;
  wire \u6_alu/c_alu1127_out ;
  wire \u6_alu/c_alu1__0 ;
  wire \u8_mult/eqOp92_in ;
  wire \u8_mult/eqOp__1 ;
  wire \upper_reg[0]_i_4_n_0 ;
  wire \upper_reg[0]_i_5_n_0 ;
  wire \upper_reg[0]_i_6_n_0 ;
  wire \upper_reg[10]_i_2_n_0 ;
  wire \upper_reg[10]_i_3_n_0 ;
  wire \upper_reg[11]_i_2_n_0 ;
  wire \upper_reg[11]_i_3_n_0 ;
  wire \upper_reg[12]_i_2_n_0 ;
  wire \upper_reg[12]_i_3_n_0 ;
  wire \upper_reg[13]_i_2_n_0 ;
  wire \upper_reg[13]_i_3_n_0 ;
  wire \upper_reg[14]_i_2_n_0 ;
  wire \upper_reg[14]_i_3_n_0 ;
  wire \upper_reg[15]_i_2_n_0 ;
  wire \upper_reg[15]_i_3_n_0 ;
  wire \upper_reg[16]_i_2_n_0 ;
  wire \upper_reg[16]_i_3_n_0 ;
  wire \upper_reg[17]_i_2_n_0 ;
  wire \upper_reg[17]_i_3_n_0 ;
  wire \upper_reg[18]_i_2_n_0 ;
  wire \upper_reg[18]_i_3_n_0 ;
  wire \upper_reg[19]_i_2_n_0 ;
  wire \upper_reg[19]_i_3_n_0 ;
  wire \upper_reg[1]_i_2_n_0 ;
  wire \upper_reg[1]_i_3_n_0 ;
  wire \upper_reg[20]_i_2_n_0 ;
  wire \upper_reg[20]_i_3_n_0 ;
  wire \upper_reg[21]_i_2_n_0 ;
  wire \upper_reg[21]_i_3_n_0 ;
  wire \upper_reg[22]_i_2_n_0 ;
  wire \upper_reg[22]_i_3_n_0 ;
  wire \upper_reg[23]_i_2_n_0 ;
  wire \upper_reg[23]_i_3_n_0 ;
  wire \upper_reg[24]_i_2_n_0 ;
  wire \upper_reg[24]_i_3_n_0 ;
  wire \upper_reg[25]_i_2_n_0 ;
  wire \upper_reg[25]_i_3_n_0 ;
  wire \upper_reg[26]_i_2_n_0 ;
  wire \upper_reg[26]_i_3_n_0 ;
  wire \upper_reg[27]_i_2_n_0 ;
  wire \upper_reg[27]_i_3_n_0 ;
  wire \upper_reg[28]_i_2_n_0 ;
  wire \upper_reg[28]_i_3_n_0 ;
  wire \upper_reg[29]_i_2_n_0 ;
  wire \upper_reg[29]_i_3_n_0 ;
  wire \upper_reg[2]_i_2_n_0 ;
  wire \upper_reg[2]_i_3_n_0 ;
  wire \upper_reg[30]_i_2_n_0 ;
  wire \upper_reg[30]_i_3_n_0 ;
  wire \upper_reg[31]_i_4_n_0 ;
  wire \upper_reg[31]_i_5_n_0 ;
  wire \upper_reg[3]_i_2_n_0 ;
  wire \upper_reg[3]_i_3_n_0 ;
  wire \upper_reg[4]_i_2_n_0 ;
  wire \upper_reg[4]_i_3_n_0 ;
  wire \upper_reg[5]_i_2_n_0 ;
  wire \upper_reg[5]_i_3_n_0 ;
  wire \upper_reg[6]_i_2_n_0 ;
  wire \upper_reg[6]_i_3_n_0 ;
  wire \upper_reg[7]_i_2_n_0 ;
  wire \upper_reg[7]_i_3_n_0 ;
  wire \upper_reg[8]_i_2_n_0 ;
  wire \upper_reg[8]_i_3_n_0 ;
  wire \upper_reg[9]_i_2_n_0 ;
  wire \upper_reg[9]_i_3_n_0 ;
  wire \upper_reg_reg[11] ;
  wire \upper_reg_reg[12] ;
  wire \upper_reg_reg[13] ;
  wire \upper_reg_reg[14] ;
  wire \upper_reg_reg[16] ;
  wire \upper_reg_reg[18] ;
  wire \upper_reg_reg[19] ;
  wire \upper_reg_reg[19]_0 ;
  wire \upper_reg_reg[20] ;
  wire \upper_reg_reg[21] ;
  wire \upper_reg_reg[22] ;
  wire \upper_reg_reg[23] ;
  wire \upper_reg_reg[24] ;
  wire \upper_reg_reg[25] ;
  wire \upper_reg_reg[26] ;
  wire \upper_reg_reg[27] ;
  wire \upper_reg_reg[28] ;
  wire [31:0]\upper_reg_reg[31] ;
  wire [23:0]\upper_reg_reg[31]_0 ;
  wire \upper_reg_reg[3] ;
  wire \upper_reg_reg[4] ;
  wire \upper_reg_reg[4]_0 ;
  wire \upper_reg_reg[5] ;
  wire \upper_reg_reg[6] ;
  wire \upper_reg_reg[6]_0 ;
  wire \upper_reg_reg[7] ;
  wire \upper_reg_reg[8] ;
  wire \upper_reg_reg[9] ;
  wire \xilinx_16x1d.reg_loop[0].reg_bit1a_i_11_n_0 ;
  wire \xilinx_16x1d.reg_loop[0].reg_bit1a_i_12_n_0 ;
  wire \xilinx_16x1d.reg_loop[0].reg_bit1a_i_15_n_0 ;
  wire \xilinx_16x1d.reg_loop[0].reg_bit1a_i_23_n_0 ;
  wire \xilinx_16x1d.reg_loop[0].reg_bit1a_i_27_n_0 ;
  wire \xilinx_16x1d.reg_loop[0].reg_bit1a_i_28_n_0 ;
  wire \xilinx_16x1d.reg_loop[0].reg_bit1a_i_30_n_0 ;
  wire \xilinx_16x1d.reg_loop[0].reg_bit1a_i_34_n_0 ;
  wire \xilinx_16x1d.reg_loop[0].reg_bit1a_i_35_n_0 ;
  wire \xilinx_16x1d.reg_loop[0].reg_bit1a_i_38_n_0 ;
  wire \xilinx_16x1d.reg_loop[0].reg_bit1a_i_43_n_0 ;
  wire \xilinx_16x1d.reg_loop[0].reg_bit1a_i_50_n_0 ;
  wire \xilinx_16x1d.reg_loop[0].reg_bit1a_i_51_n_0 ;
  wire \xilinx_16x1d.reg_loop[0].reg_bit1a_i_52_n_0 ;
  wire \xilinx_16x1d.reg_loop[0].reg_bit1a_i_55_n_0 ;
  wire \xilinx_16x1d.reg_loop[0].reg_bit1a_i_59_n_0 ;
  wire \xilinx_16x1d.reg_loop[0].reg_bit1a_i_60_n_0 ;
  wire \xilinx_16x1d.reg_loop[0].reg_bit1a_i_61_n_0 ;
  wire \xilinx_16x1d.reg_loop[0].reg_bit1a_i_62_n_0 ;
  wire \xilinx_16x1d.reg_loop[0].reg_bit1a_i_63_n_0 ;
  wire \xilinx_16x1d.reg_loop[0].reg_bit1a_i_64_n_0 ;
  wire \xilinx_16x1d.reg_loop[0].reg_bit1a_i_65_n_0 ;
  wire \xilinx_16x1d.reg_loop[0].reg_bit1a_i_66_n_0 ;
  wire \xilinx_16x1d.reg_loop[0].reg_bit1a_i_67_n_0 ;
  wire \xilinx_16x1d.reg_loop[0].reg_bit1a_i_68_n_0 ;
  wire \xilinx_16x1d.reg_loop[0].reg_bit1a_i_69_n_0 ;
  wire \xilinx_16x1d.reg_loop[0].reg_bit1a_i_70_n_0 ;
  wire \xilinx_16x1d.reg_loop[0].reg_bit1a_i_73_n_0 ;
  wire \xilinx_16x1d.reg_loop[0].reg_bit1a_i_78_n_0 ;
  wire \xilinx_16x1d.reg_loop[0].reg_bit2a_i_10_n_0 ;
  wire \xilinx_16x1d.reg_loop[0].reg_bit2a_i_11_n_0 ;
  wire \xilinx_16x1d.reg_loop[0].reg_bit2a_i_12_n_0 ;
  wire \xilinx_16x1d.reg_loop[0].reg_bit2a_i_5_n_0 ;
  wire \xilinx_16x1d.reg_loop[0].reg_bit2a_i_6_n_0 ;
  wire \xilinx_16x1d.reg_loop[0].reg_bit2a_i_7_n_0 ;
  wire \xilinx_16x1d.reg_loop[0].reg_bit2a_i_8_n_0 ;
  wire \xilinx_16x1d.reg_loop[0].reg_bit2a_i_9_n_0 ;
  wire \xilinx_16x1d.reg_loop[10].reg_bit1a_i_2_n_0 ;
  wire \xilinx_16x1d.reg_loop[10].reg_bit1a_i_3_n_0 ;
  wire \xilinx_16x1d.reg_loop[10].reg_bit1a_i_4_n_0 ;
  wire \xilinx_16x1d.reg_loop[11].reg_bit1a_i_2_n_0 ;
  wire \xilinx_16x1d.reg_loop[11].reg_bit1a_i_3_n_0 ;
  wire \xilinx_16x1d.reg_loop[11].reg_bit1a_i_4_n_0 ;
  wire \xilinx_16x1d.reg_loop[12].reg_bit1a_i_2_n_0 ;
  wire \xilinx_16x1d.reg_loop[12].reg_bit1a_i_3_n_0 ;
  wire \xilinx_16x1d.reg_loop[12].reg_bit1a_i_4_n_0 ;
  wire \xilinx_16x1d.reg_loop[13].reg_bit1a_i_2_n_0 ;
  wire \xilinx_16x1d.reg_loop[13].reg_bit1a_i_3_n_0 ;
  wire \xilinx_16x1d.reg_loop[13].reg_bit1a_i_4_n_0 ;
  wire \xilinx_16x1d.reg_loop[14].reg_bit1a_i_2_n_0 ;
  wire \xilinx_16x1d.reg_loop[14].reg_bit1a_i_3_n_0 ;
  wire \xilinx_16x1d.reg_loop[14].reg_bit1a_i_4_n_0 ;
  wire \xilinx_16x1d.reg_loop[15].reg_bit1a_i_2_n_0 ;
  wire \xilinx_16x1d.reg_loop[15].reg_bit1a_i_3_n_0 ;
  wire \xilinx_16x1d.reg_loop[15].reg_bit1a_i_4_n_0 ;
  wire \xilinx_16x1d.reg_loop[16].reg_bit1a_i_2_n_0 ;
  wire \xilinx_16x1d.reg_loop[16].reg_bit1a_i_3_n_0 ;
  wire \xilinx_16x1d.reg_loop[16].reg_bit1a_i_4_n_0 ;
  wire \xilinx_16x1d.reg_loop[16].reg_bit1a_i_5_n_0 ;
  wire \xilinx_16x1d.reg_loop[17].reg_bit1a_i_2_n_0 ;
  wire \xilinx_16x1d.reg_loop[17].reg_bit1a_i_3_n_0 ;
  wire \xilinx_16x1d.reg_loop[17].reg_bit1a_i_4_n_0 ;
  wire \xilinx_16x1d.reg_loop[18].reg_bit1a_i_2_n_0 ;
  wire \xilinx_16x1d.reg_loop[18].reg_bit1a_i_3_n_0 ;
  wire \xilinx_16x1d.reg_loop[18].reg_bit1a_i_4_n_0 ;
  wire \xilinx_16x1d.reg_loop[19].reg_bit1a_i_2_n_0 ;
  wire \xilinx_16x1d.reg_loop[19].reg_bit1a_i_3_n_0 ;
  wire \xilinx_16x1d.reg_loop[19].reg_bit1a_i_4_n_0 ;
  wire \xilinx_16x1d.reg_loop[1].reg_bit1a_i_10_n_0 ;
  wire \xilinx_16x1d.reg_loop[1].reg_bit1a_i_12_n_0 ;
  wire \xilinx_16x1d.reg_loop[1].reg_bit1a_i_2_n_0 ;
  wire \xilinx_16x1d.reg_loop[1].reg_bit1a_i_3_n_0 ;
  wire \xilinx_16x1d.reg_loop[1].reg_bit1a_i_4_n_0 ;
  wire \xilinx_16x1d.reg_loop[1].reg_bit1a_i_5_n_0 ;
  wire \xilinx_16x1d.reg_loop[1].reg_bit1a_i_6_n_0 ;
  wire \xilinx_16x1d.reg_loop[20].reg_bit1a_i_2_n_0 ;
  wire \xilinx_16x1d.reg_loop[20].reg_bit1a_i_3_n_0 ;
  wire \xilinx_16x1d.reg_loop[20].reg_bit1a_i_4_n_0 ;
  wire \xilinx_16x1d.reg_loop[21].reg_bit1a_i_2_n_0 ;
  wire \xilinx_16x1d.reg_loop[21].reg_bit1a_i_3_n_0 ;
  wire \xilinx_16x1d.reg_loop[21].reg_bit1a_i_4_n_0 ;
  wire \xilinx_16x1d.reg_loop[22].reg_bit1a_i_2_n_0 ;
  wire \xilinx_16x1d.reg_loop[22].reg_bit1a_i_3_n_0 ;
  wire \xilinx_16x1d.reg_loop[22].reg_bit1a_i_4_n_0 ;
  wire \xilinx_16x1d.reg_loop[23].reg_bit1a_i_2_n_0 ;
  wire \xilinx_16x1d.reg_loop[23].reg_bit1a_i_3_n_0 ;
  wire \xilinx_16x1d.reg_loop[23].reg_bit1a_i_4_n_0 ;
  wire \xilinx_16x1d.reg_loop[24].reg_bit1a_i_2_n_0 ;
  wire \xilinx_16x1d.reg_loop[24].reg_bit1a_i_3_n_0 ;
  wire \xilinx_16x1d.reg_loop[24].reg_bit1a_i_4_n_0 ;
  wire \xilinx_16x1d.reg_loop[25].reg_bit1a_i_2_n_0 ;
  wire \xilinx_16x1d.reg_loop[25].reg_bit1a_i_3_n_0 ;
  wire \xilinx_16x1d.reg_loop[25].reg_bit1a_i_4_n_0 ;
  wire \xilinx_16x1d.reg_loop[26].reg_bit1a_i_2_n_0 ;
  wire \xilinx_16x1d.reg_loop[26].reg_bit1a_i_3_n_0 ;
  wire \xilinx_16x1d.reg_loop[26].reg_bit1a_i_4_n_0 ;
  wire \xilinx_16x1d.reg_loop[27].reg_bit1a_i_2_n_0 ;
  wire \xilinx_16x1d.reg_loop[27].reg_bit1a_i_3_n_0 ;
  wire \xilinx_16x1d.reg_loop[27].reg_bit1a_i_4_n_0 ;
  wire \xilinx_16x1d.reg_loop[28].reg_bit1a_i_3_n_0 ;
  wire \xilinx_16x1d.reg_loop[28].reg_bit1a_i_4_n_0 ;
  wire \xilinx_16x1d.reg_loop[29].reg_bit1a_i_2_n_0 ;
  wire \xilinx_16x1d.reg_loop[29].reg_bit1a_i_3_n_0 ;
  wire \xilinx_16x1d.reg_loop[29].reg_bit1a_i_4_n_0 ;
  wire \xilinx_16x1d.reg_loop[2].reg_bit1a_i_2_n_0 ;
  wire \xilinx_16x1d.reg_loop[2].reg_bit1a_i_3_n_0 ;
  wire \xilinx_16x1d.reg_loop[2].reg_bit1a_i_4_n_0 ;
  wire \xilinx_16x1d.reg_loop[2].reg_bit1a_i_5_n_0 ;
  wire \xilinx_16x1d.reg_loop[30].reg_bit1a_i_2_n_0 ;
  wire \xilinx_16x1d.reg_loop[30].reg_bit1a_i_3_n_0 ;
  wire \xilinx_16x1d.reg_loop[30].reg_bit1a_i_4_n_0 ;
  wire \xilinx_16x1d.reg_loop[31].reg_bit1a_i_2_n_0 ;
  wire \xilinx_16x1d.reg_loop[31].reg_bit1a_i_3_n_0 ;
  wire \xilinx_16x1d.reg_loop[31].reg_bit1a_i_4_n_0 ;
  wire \xilinx_16x1d.reg_loop[3].reg_bit1a_i_3_n_0 ;
  wire \xilinx_16x1d.reg_loop[3].reg_bit1a_i_4_n_0 ;
  wire \xilinx_16x1d.reg_loop[3].reg_bit1a_i_5_n_0 ;
  wire \xilinx_16x1d.reg_loop[3].reg_bit1a_i_6_n_0 ;
  wire \xilinx_16x1d.reg_loop[4].reg_bit1a_i_2_n_0 ;
  wire \xilinx_16x1d.reg_loop[4].reg_bit1a_i_3_n_0 ;
  wire \xilinx_16x1d.reg_loop[4].reg_bit1a_i_4_n_0 ;
  wire \xilinx_16x1d.reg_loop[4].reg_bit1a_i_5_n_0 ;
  wire \xilinx_16x1d.reg_loop[5].reg_bit1a_i_2_n_0 ;
  wire \xilinx_16x1d.reg_loop[5].reg_bit1a_i_3_n_0 ;
  wire \xilinx_16x1d.reg_loop[5].reg_bit1a_i_4_n_0 ;
  wire \xilinx_16x1d.reg_loop[5].reg_bit1a_i_5_n_0 ;
  wire \xilinx_16x1d.reg_loop[6].reg_bit1a_i_2_n_0 ;
  wire \xilinx_16x1d.reg_loop[6].reg_bit1a_i_3_n_0 ;
  wire \xilinx_16x1d.reg_loop[6].reg_bit1a_i_4_n_0 ;
  wire \xilinx_16x1d.reg_loop[6].reg_bit1a_i_5_n_0 ;
  wire \xilinx_16x1d.reg_loop[7].reg_bit1a_i_2_n_0 ;
  wire \xilinx_16x1d.reg_loop[7].reg_bit1a_i_3_n_0 ;
  wire \xilinx_16x1d.reg_loop[7].reg_bit1a_i_4_n_0 ;
  wire \xilinx_16x1d.reg_loop[7].reg_bit1a_i_5_n_0 ;
  wire \xilinx_16x1d.reg_loop[8].reg_bit1a_i_2_n_0 ;
  wire \xilinx_16x1d.reg_loop[8].reg_bit1a_i_3_n_0 ;
  wire \xilinx_16x1d.reg_loop[8].reg_bit1a_i_4_n_0 ;
  wire \xilinx_16x1d.reg_loop[9].reg_bit1a_i_2_n_0 ;
  wire \xilinx_16x1d.reg_loop[9].reg_bit1a_i_3_n_0 ;
  wire \xilinx_16x1d.reg_loop[9].reg_bit1a_i_4_n_0 ;

  (* SOFT_HLUTNM = "soft_lutpair81" *) 
  LUT4 #(
    .INIT(16'h0080)) 
    \PLtoPS_address_reg[19]_i_1 
       (.I0(complete_address_next[30]),
        .I1(complete_address_next[29]),
        .I2(complete_address_next[28]),
        .I3(complete_address_next[31]),
        .O(\PLtoPS_bytes_selection[3] ));
  (* SOFT_HLUTNM = "soft_lutpair136" *) 
  LUT3 #(
    .INIT(8'h80)) 
    PLtoPS_enable_reg_i_1
       (.I0(complete_address_next[28]),
        .I1(complete_address_next[29]),
        .I2(complete_address_next[30]),
        .O(PLtoPS_enable));
  LUT6 #(
    .INIT(64'h0000100000000000)) 
    PLtoPS_generate_interrupt_reg_i_1
       (.I0(complete_address_next[29]),
        .I1(complete_address_next[23]),
        .I2(PLtoPS_generate_interrupt_reg_i_2_n_0),
        .I3(complete_address_next[24]),
        .I4(complete_address_next[28]),
        .I5(complete_address_next[31]),
        .O(PLtoPS_generate_interrupt));
  LUT6 #(
    .INIT(64'h0000000040000000)) 
    PLtoPS_generate_interrupt_reg_i_2
       (.I0(complete_address_next[27]),
        .I1(complete_address_next[25]),
        .I2(complete_address_next[26]),
        .I3(complete_address_next[20]),
        .I4(complete_address_next[22]),
        .I5(complete_address_next[21]),
        .O(PLtoPS_generate_interrupt_reg_i_2_n_0));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \PLtoPS_write_data_reg[31]_i_1 
       (.I0(complete_address_next[30]),
        .I1(complete_address_next[28]),
        .I2(\PLtoPS_write_data_reg[31]_i_2_n_0 ),
        .I3(\bram_firmware_code_bytes_selection[3] [0]),
        .I4(complete_address_next[29]),
        .I5(complete_address_next[31]),
        .O(\PLtoPS_write_data[31] ));
  LUT6 #(
    .INIT(64'h0000000000800000)) 
    \PLtoPS_write_data_reg[31]_i_2 
       (.I0(fifo_ptm_en_reg_i_3_n_0),
        .I1(fifo_ptm_en_reg_i_4_n_0),
        .I2(\opcode_reg[31]_i_3_n_0 ),
        .I3(pause_in0_out),
        .I4(fifo_ptm_en_reg_i_5_n_0),
        .I5(mem_state_reg_reg_n_0),
        .O(\PLtoPS_write_data_reg[31]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hE0FFE000)) 
    \aa_reg[0]_i_1 
       (.I0(\aa_reg[29]_i_2_n_0 ),
        .I1(\aa_reg[29]_i_3_n_0 ),
        .I2(\aa_reg_reg[31]_1 [0]),
        .I3(\lower_reg_reg[31]_1 ),
        .I4(\aa_reg[0]_i_2_n_0 ),
        .O(\aa_reg_reg[31] [0]));
  (* SOFT_HLUTNM = "soft_lutpair79" *) 
  LUT4 #(
    .INIT(16'hF808)) 
    \aa_reg[0]_i_2 
       (.I0(\aa_reg_reg[31]_1 [0]),
        .I1(\aa_reg[31]_i_5_n_0 ),
        .I2(\aa_reg_reg[30] ),
        .I3(a_bus[0]),
        .O(\aa_reg[0]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hE0FFE000)) 
    \aa_reg[10]_i_1 
       (.I0(\aa_reg[29]_i_2_n_0 ),
        .I1(\aa_reg[29]_i_3_n_0 ),
        .I2(\aa_reg_reg[31]_1 [10]),
        .I3(\lower_reg_reg[31]_1 ),
        .I4(\aa_reg[10]_i_2_n_0 ),
        .O(\aa_reg_reg[31] [10]));
  LUT6 #(
    .INIT(64'hF8F80808C8F83808)) 
    \aa_reg[10]_i_2 
       (.I0(\aa_reg_reg[31]_1 [10]),
        .I1(\aa_reg[31]_i_5_n_0 ),
        .I2(\aa_reg_reg[30] ),
        .I3(b_bus[31]),
        .I4(a_bus[10]),
        .I5(\aa_reg[10]_i_4_n_0 ),
        .O(\aa_reg[10]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h88B8)) 
    \aa_reg[10]_i_3 
       (.I0(pc_current[10]),
        .I1(\upper_reg[0]_i_5_n_0 ),
        .I2(reg_source[10]),
        .I3(\upper_reg[0]_i_4_n_0 ),
        .O(a_bus[10]));
  LUT2 #(
    .INIT(4'h2)) 
    \aa_reg[10]_i_4 
       (.I0(\aa_reg[9]_i_4_n_0 ),
        .I1(a_bus[9]),
        .O(\aa_reg[10]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hE0FFE000)) 
    \aa_reg[11]_i_1 
       (.I0(\aa_reg[29]_i_2_n_0 ),
        .I1(\aa_reg[29]_i_3_n_0 ),
        .I2(\aa_reg_reg[31]_1 [11]),
        .I3(\lower_reg_reg[31]_1 ),
        .I4(\aa_reg[11]_i_2_n_0 ),
        .O(\aa_reg_reg[31] [11]));
  LUT6 #(
    .INIT(64'hF8F80808C8F83808)) 
    \aa_reg[11]_i_2 
       (.I0(\aa_reg_reg[31]_1 [11]),
        .I1(\aa_reg[31]_i_5_n_0 ),
        .I2(\aa_reg_reg[30] ),
        .I3(b_bus[31]),
        .I4(a_bus[11]),
        .I5(\aa_reg[11]_i_4_n_0 ),
        .O(\aa_reg[11]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h88B8)) 
    \aa_reg[11]_i_3 
       (.I0(pc_current[11]),
        .I1(\upper_reg[0]_i_5_n_0 ),
        .I2(reg_source[11]),
        .I3(\upper_reg[0]_i_4_n_0 ),
        .O(a_bus[11]));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \aa_reg[11]_i_4 
       (.I0(\aa_reg[10]_i_4_n_0 ),
        .I1(a_bus[10]),
        .O(\aa_reg[11]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hE0FFE000)) 
    \aa_reg[12]_i_1 
       (.I0(\aa_reg[29]_i_2_n_0 ),
        .I1(\aa_reg[29]_i_3_n_0 ),
        .I2(\aa_reg_reg[31]_1 [12]),
        .I3(\lower_reg_reg[31]_1 ),
        .I4(\aa_reg[12]_i_2_n_0 ),
        .O(\aa_reg_reg[31] [12]));
  LUT6 #(
    .INIT(64'hF8F80808C8F83808)) 
    \aa_reg[12]_i_2 
       (.I0(\aa_reg_reg[31]_1 [12]),
        .I1(\aa_reg[31]_i_5_n_0 ),
        .I2(\aa_reg_reg[30] ),
        .I3(b_bus[31]),
        .I4(a_bus[12]),
        .I5(\aa_reg[12]_i_4_n_0 ),
        .O(\aa_reg[12]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h88B8)) 
    \aa_reg[12]_i_3 
       (.I0(pc_current[12]),
        .I1(\upper_reg[0]_i_5_n_0 ),
        .I2(reg_source[12]),
        .I3(\upper_reg[0]_i_4_n_0 ),
        .O(a_bus[12]));
  LUT2 #(
    .INIT(4'h2)) 
    \aa_reg[12]_i_4 
       (.I0(\aa_reg[11]_i_4_n_0 ),
        .I1(a_bus[11]),
        .O(\aa_reg[12]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hE0FFE000)) 
    \aa_reg[13]_i_1 
       (.I0(\aa_reg[29]_i_2_n_0 ),
        .I1(\aa_reg[29]_i_3_n_0 ),
        .I2(\aa_reg_reg[31]_1 [13]),
        .I3(\lower_reg_reg[31]_1 ),
        .I4(\aa_reg[13]_i_2_n_0 ),
        .O(\aa_reg_reg[31] [13]));
  LUT6 #(
    .INIT(64'hF8F80808C8F83808)) 
    \aa_reg[13]_i_2 
       (.I0(\aa_reg_reg[31]_1 [13]),
        .I1(\aa_reg[31]_i_5_n_0 ),
        .I2(\aa_reg_reg[30] ),
        .I3(b_bus[31]),
        .I4(a_bus[13]),
        .I5(\aa_reg[13]_i_4_n_0 ),
        .O(\aa_reg[13]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h88B8)) 
    \aa_reg[13]_i_3 
       (.I0(pc_current[13]),
        .I1(\upper_reg[0]_i_5_n_0 ),
        .I2(reg_source[13]),
        .I3(\upper_reg[0]_i_4_n_0 ),
        .O(a_bus[13]));
  LUT2 #(
    .INIT(4'h2)) 
    \aa_reg[13]_i_4 
       (.I0(\aa_reg[12]_i_4_n_0 ),
        .I1(a_bus[12]),
        .O(\aa_reg[13]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hE0FFE000)) 
    \aa_reg[14]_i_1 
       (.I0(\aa_reg[29]_i_2_n_0 ),
        .I1(\aa_reg[29]_i_3_n_0 ),
        .I2(\aa_reg_reg[31]_1 [14]),
        .I3(\lower_reg_reg[31]_1 ),
        .I4(\aa_reg[14]_i_2_n_0 ),
        .O(\aa_reg_reg[31] [14]));
  LUT6 #(
    .INIT(64'hF8F80808C8F83808)) 
    \aa_reg[14]_i_2 
       (.I0(\aa_reg_reg[31]_1 [14]),
        .I1(\aa_reg[31]_i_5_n_0 ),
        .I2(\aa_reg_reg[30] ),
        .I3(b_bus[31]),
        .I4(a_bus[14]),
        .I5(\aa_reg[14]_i_4_n_0 ),
        .O(\aa_reg[14]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h88B8)) 
    \aa_reg[14]_i_3 
       (.I0(pc_current[14]),
        .I1(\upper_reg[0]_i_5_n_0 ),
        .I2(reg_source[14]),
        .I3(\upper_reg[0]_i_4_n_0 ),
        .O(a_bus[14]));
  LUT2 #(
    .INIT(4'h2)) 
    \aa_reg[14]_i_4 
       (.I0(\aa_reg[13]_i_4_n_0 ),
        .I1(a_bus[13]),
        .O(\aa_reg[14]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hE0FFE000)) 
    \aa_reg[15]_i_1 
       (.I0(\aa_reg[29]_i_2_n_0 ),
        .I1(\aa_reg[29]_i_3_n_0 ),
        .I2(\aa_reg_reg[31]_1 [15]),
        .I3(\lower_reg_reg[31]_1 ),
        .I4(\aa_reg[15]_i_2_n_0 ),
        .O(\aa_reg_reg[31] [15]));
  LUT6 #(
    .INIT(64'hF8F80808C8F83808)) 
    \aa_reg[15]_i_2 
       (.I0(\aa_reg_reg[31]_1 [15]),
        .I1(\aa_reg[31]_i_5_n_0 ),
        .I2(\aa_reg_reg[30] ),
        .I3(b_bus[31]),
        .I4(a_bus[15]),
        .I5(\aa_reg[15]_i_4_n_0 ),
        .O(\aa_reg[15]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h88B8)) 
    \aa_reg[15]_i_3 
       (.I0(pc_current[15]),
        .I1(\upper_reg[0]_i_5_n_0 ),
        .I2(reg_source[15]),
        .I3(\upper_reg[0]_i_4_n_0 ),
        .O(a_bus[15]));
  LUT2 #(
    .INIT(4'h2)) 
    \aa_reg[15]_i_4 
       (.I0(\aa_reg[14]_i_4_n_0 ),
        .I1(a_bus[14]),
        .O(\aa_reg[15]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hE0FFE000)) 
    \aa_reg[16]_i_1 
       (.I0(\aa_reg[29]_i_2_n_0 ),
        .I1(\aa_reg[29]_i_3_n_0 ),
        .I2(\aa_reg_reg[31]_1 [16]),
        .I3(\lower_reg_reg[31]_1 ),
        .I4(\aa_reg[16]_i_2_n_0 ),
        .O(\aa_reg_reg[31] [16]));
  LUT6 #(
    .INIT(64'hF8F80808C8F83808)) 
    \aa_reg[16]_i_2 
       (.I0(\aa_reg_reg[31]_1 [16]),
        .I1(\aa_reg[31]_i_5_n_0 ),
        .I2(\aa_reg_reg[30] ),
        .I3(b_bus[31]),
        .I4(a_bus[16]),
        .I5(\aa_reg[16]_i_4_n_0 ),
        .O(\aa_reg[16]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h88B8)) 
    \aa_reg[16]_i_3 
       (.I0(pc_current[16]),
        .I1(\upper_reg[0]_i_5_n_0 ),
        .I2(reg_source[16]),
        .I3(\upper_reg[0]_i_4_n_0 ),
        .O(a_bus[16]));
  LUT2 #(
    .INIT(4'h2)) 
    \aa_reg[16]_i_4 
       (.I0(\aa_reg[15]_i_4_n_0 ),
        .I1(a_bus[15]),
        .O(\aa_reg[16]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hE0FFE000)) 
    \aa_reg[17]_i_1 
       (.I0(\aa_reg[29]_i_2_n_0 ),
        .I1(\aa_reg[29]_i_3_n_0 ),
        .I2(\aa_reg_reg[31]_1 [17]),
        .I3(\lower_reg_reg[31]_1 ),
        .I4(\aa_reg[17]_i_2_n_0 ),
        .O(\aa_reg_reg[31] [17]));
  LUT6 #(
    .INIT(64'hF8F80808C8F83808)) 
    \aa_reg[17]_i_2 
       (.I0(\aa_reg_reg[31]_1 [17]),
        .I1(\aa_reg[31]_i_5_n_0 ),
        .I2(\aa_reg_reg[30] ),
        .I3(b_bus[31]),
        .I4(a_bus[17]),
        .I5(\aa_reg[17]_i_4_n_0 ),
        .O(\aa_reg[17]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h88B8)) 
    \aa_reg[17]_i_3 
       (.I0(pc_current[17]),
        .I1(\upper_reg[0]_i_5_n_0 ),
        .I2(reg_source[17]),
        .I3(\upper_reg[0]_i_4_n_0 ),
        .O(a_bus[17]));
  LUT2 #(
    .INIT(4'h2)) 
    \aa_reg[17]_i_4 
       (.I0(\aa_reg[16]_i_4_n_0 ),
        .I1(a_bus[16]),
        .O(\aa_reg[17]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hE0FFE000)) 
    \aa_reg[18]_i_1 
       (.I0(\aa_reg[29]_i_2_n_0 ),
        .I1(\aa_reg[29]_i_3_n_0 ),
        .I2(\aa_reg_reg[31]_1 [18]),
        .I3(\lower_reg_reg[31]_1 ),
        .I4(\aa_reg[18]_i_2_n_0 ),
        .O(\aa_reg_reg[31] [18]));
  LUT6 #(
    .INIT(64'hF8F80808C8F83808)) 
    \aa_reg[18]_i_2 
       (.I0(\aa_reg_reg[31]_1 [18]),
        .I1(\aa_reg[31]_i_5_n_0 ),
        .I2(\aa_reg_reg[30] ),
        .I3(b_bus[31]),
        .I4(a_bus[18]),
        .I5(\aa_reg[18]_i_4_n_0 ),
        .O(\aa_reg[18]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h88B8)) 
    \aa_reg[18]_i_3 
       (.I0(pc_current[18]),
        .I1(\upper_reg[0]_i_5_n_0 ),
        .I2(reg_source[18]),
        .I3(\upper_reg[0]_i_4_n_0 ),
        .O(a_bus[18]));
  LUT2 #(
    .INIT(4'h2)) 
    \aa_reg[18]_i_4 
       (.I0(\aa_reg[17]_i_4_n_0 ),
        .I1(a_bus[17]),
        .O(\aa_reg[18]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hE0FFE000)) 
    \aa_reg[19]_i_1 
       (.I0(\aa_reg[29]_i_2_n_0 ),
        .I1(\aa_reg[29]_i_3_n_0 ),
        .I2(\aa_reg_reg[31]_1 [19]),
        .I3(\lower_reg_reg[31]_1 ),
        .I4(\aa_reg[19]_i_2_n_0 ),
        .O(\aa_reg_reg[31] [19]));
  LUT6 #(
    .INIT(64'hF8F80808C8F83808)) 
    \aa_reg[19]_i_2 
       (.I0(\aa_reg_reg[31]_1 [19]),
        .I1(\aa_reg[31]_i_5_n_0 ),
        .I2(\aa_reg_reg[30] ),
        .I3(b_bus[31]),
        .I4(a_bus[19]),
        .I5(\aa_reg[19]_i_4_n_0 ),
        .O(\aa_reg[19]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h88B8)) 
    \aa_reg[19]_i_3 
       (.I0(pc_current[19]),
        .I1(\upper_reg[0]_i_5_n_0 ),
        .I2(reg_source[19]),
        .I3(\upper_reg[0]_i_4_n_0 ),
        .O(a_bus[19]));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \aa_reg[19]_i_4 
       (.I0(\aa_reg[18]_i_4_n_0 ),
        .I1(a_bus[18]),
        .O(\aa_reg[19]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hE0FFE000)) 
    \aa_reg[1]_i_1 
       (.I0(\aa_reg[29]_i_2_n_0 ),
        .I1(\aa_reg[29]_i_3_n_0 ),
        .I2(\aa_reg_reg[31]_1 [1]),
        .I3(\lower_reg_reg[31]_1 ),
        .I4(\aa_reg[1]_i_2_n_0 ),
        .O(\aa_reg_reg[31] [1]));
  LUT6 #(
    .INIT(64'hC8F83808F8F80808)) 
    \aa_reg[1]_i_2 
       (.I0(\aa_reg_reg[31]_1 [1]),
        .I1(\aa_reg[31]_i_5_n_0 ),
        .I2(\aa_reg_reg[30] ),
        .I3(b_bus[31]),
        .I4(a_bus[1]),
        .I5(a_bus[0]),
        .O(\aa_reg[1]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h00E2)) 
    \aa_reg[1]_i_3 
       (.I0(reg_source[1]),
        .I1(\upper_reg[0]_i_4_n_0 ),
        .I2(opcode[7]),
        .I3(\upper_reg[0]_i_5_n_0 ),
        .O(a_bus[1]));
  LUT5 #(
    .INIT(32'hE0FFE000)) 
    \aa_reg[20]_i_1 
       (.I0(\aa_reg[29]_i_2_n_0 ),
        .I1(\aa_reg[29]_i_3_n_0 ),
        .I2(\aa_reg_reg[31]_1 [20]),
        .I3(\lower_reg_reg[31]_1 ),
        .I4(\aa_reg[20]_i_2_n_0 ),
        .O(\aa_reg_reg[31] [20]));
  LUT6 #(
    .INIT(64'hF8F80808C8F83808)) 
    \aa_reg[20]_i_2 
       (.I0(\aa_reg_reg[31]_1 [20]),
        .I1(\aa_reg[31]_i_5_n_0 ),
        .I2(\aa_reg_reg[30] ),
        .I3(b_bus[31]),
        .I4(a_bus[20]),
        .I5(\aa_reg[20]_i_4_n_0 ),
        .O(\aa_reg[20]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h88B8)) 
    \aa_reg[20]_i_3 
       (.I0(pc_current[20]),
        .I1(\upper_reg[0]_i_5_n_0 ),
        .I2(reg_source[20]),
        .I3(\upper_reg[0]_i_4_n_0 ),
        .O(a_bus[20]));
  LUT2 #(
    .INIT(4'h2)) 
    \aa_reg[20]_i_4 
       (.I0(\aa_reg[19]_i_4_n_0 ),
        .I1(a_bus[19]),
        .O(\aa_reg[20]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hE0FFE000)) 
    \aa_reg[21]_i_1 
       (.I0(\aa_reg[29]_i_2_n_0 ),
        .I1(\aa_reg[29]_i_3_n_0 ),
        .I2(\aa_reg_reg[31]_1 [21]),
        .I3(\lower_reg_reg[31]_1 ),
        .I4(\aa_reg[21]_i_2_n_0 ),
        .O(\aa_reg_reg[31] [21]));
  LUT6 #(
    .INIT(64'hF8F80808C8F83808)) 
    \aa_reg[21]_i_2 
       (.I0(\aa_reg_reg[31]_1 [21]),
        .I1(\aa_reg[31]_i_5_n_0 ),
        .I2(\aa_reg_reg[30] ),
        .I3(b_bus[31]),
        .I4(a_bus[21]),
        .I5(\aa_reg[21]_i_4_n_0 ),
        .O(\aa_reg[21]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h88B8)) 
    \aa_reg[21]_i_3 
       (.I0(pc_current[21]),
        .I1(\upper_reg[0]_i_5_n_0 ),
        .I2(reg_source[21]),
        .I3(\upper_reg[0]_i_4_n_0 ),
        .O(a_bus[21]));
  (* SOFT_HLUTNM = "soft_lutpair138" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \aa_reg[21]_i_4 
       (.I0(\aa_reg[20]_i_4_n_0 ),
        .I1(a_bus[20]),
        .O(\aa_reg[21]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hE0FFE000)) 
    \aa_reg[22]_i_1 
       (.I0(\aa_reg[29]_i_2_n_0 ),
        .I1(\aa_reg[29]_i_3_n_0 ),
        .I2(\aa_reg_reg[31]_1 [22]),
        .I3(\lower_reg_reg[31]_1 ),
        .I4(\aa_reg[22]_i_2_n_0 ),
        .O(\aa_reg_reg[31] [22]));
  LUT6 #(
    .INIT(64'hCC99F000CCCCF000)) 
    \aa_reg[22]_i_2 
       (.I0(\aa_reg[22]_i_3_n_0 ),
        .I1(a_bus[22]),
        .I2(\aa_reg_reg[31]_1 [22]),
        .I3(\aa_reg[31]_i_5_n_0 ),
        .I4(\aa_reg_reg[30] ),
        .I5(b_bus[31]),
        .O(\aa_reg[22]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair138" *) 
  LUT3 #(
    .INIT(8'h04)) 
    \aa_reg[22]_i_3 
       (.I0(a_bus[20]),
        .I1(\aa_reg[20]_i_4_n_0 ),
        .I2(a_bus[21]),
        .O(\aa_reg[22]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h88B8)) 
    \aa_reg[22]_i_4 
       (.I0(pc_current[22]),
        .I1(\upper_reg[0]_i_5_n_0 ),
        .I2(reg_source[22]),
        .I3(\upper_reg[0]_i_4_n_0 ),
        .O(a_bus[22]));
  LUT5 #(
    .INIT(32'hE0FFE000)) 
    \aa_reg[23]_i_1 
       (.I0(\aa_reg[29]_i_2_n_0 ),
        .I1(\aa_reg[29]_i_3_n_0 ),
        .I2(\aa_reg_reg[31]_1 [23]),
        .I3(\lower_reg_reg[31]_1 ),
        .I4(\aa_reg[23]_i_2_n_0 ),
        .O(\aa_reg_reg[31] [23]));
  LUT6 #(
    .INIT(64'hCC99F000CCCCF000)) 
    \aa_reg[23]_i_2 
       (.I0(\aa_reg[23]_i_3_n_0 ),
        .I1(a_bus[23]),
        .I2(\aa_reg_reg[31]_1 [23]),
        .I3(\aa_reg[31]_i_5_n_0 ),
        .I4(\aa_reg_reg[30] ),
        .I5(b_bus[31]),
        .O(\aa_reg[23]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair139" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \aa_reg[23]_i_3 
       (.I0(\aa_reg[22]_i_3_n_0 ),
        .I1(a_bus[22]),
        .O(\aa_reg[23]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h88B8)) 
    \aa_reg[23]_i_4 
       (.I0(pc_current[23]),
        .I1(\upper_reg[0]_i_5_n_0 ),
        .I2(reg_source[23]),
        .I3(\upper_reg[0]_i_4_n_0 ),
        .O(a_bus[23]));
  LUT5 #(
    .INIT(32'hE0FFE000)) 
    \aa_reg[24]_i_1 
       (.I0(\aa_reg[29]_i_2_n_0 ),
        .I1(\aa_reg[29]_i_3_n_0 ),
        .I2(\aa_reg_reg[31]_1 [24]),
        .I3(\lower_reg_reg[31]_1 ),
        .I4(\aa_reg[24]_i_2_n_0 ),
        .O(\aa_reg_reg[31] [24]));
  LUT6 #(
    .INIT(64'hCC99F000CCCCF000)) 
    \aa_reg[24]_i_2 
       (.I0(\aa_reg[24]_i_3_n_0 ),
        .I1(a_bus[24]),
        .I2(\aa_reg_reg[31]_1 [24]),
        .I3(\aa_reg[31]_i_5_n_0 ),
        .I4(\aa_reg_reg[30] ),
        .I5(b_bus[31]),
        .O(\aa_reg[24]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair139" *) 
  LUT3 #(
    .INIT(8'h04)) 
    \aa_reg[24]_i_3 
       (.I0(a_bus[22]),
        .I1(\aa_reg[22]_i_3_n_0 ),
        .I2(a_bus[23]),
        .O(\aa_reg[24]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hE0FFE000)) 
    \aa_reg[25]_i_1 
       (.I0(\aa_reg[29]_i_2_n_0 ),
        .I1(\aa_reg[29]_i_3_n_0 ),
        .I2(\aa_reg_reg[31]_1 [25]),
        .I3(\lower_reg_reg[31]_1 ),
        .I4(\aa_reg[25]_i_2_n_0 ),
        .O(\aa_reg_reg[31] [25]));
  LUT6 #(
    .INIT(64'hCC99F000CCCCF000)) 
    \aa_reg[25]_i_2 
       (.I0(\aa_reg[25]_i_3_n_0 ),
        .I1(a_bus[25]),
        .I2(\aa_reg_reg[31]_1 [25]),
        .I3(\aa_reg[31]_i_5_n_0 ),
        .I4(\aa_reg_reg[30] ),
        .I5(b_bus[31]),
        .O(\aa_reg[25]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT4 #(
    .INIT(16'h0004)) 
    \aa_reg[25]_i_3 
       (.I0(a_bus[23]),
        .I1(\aa_reg[22]_i_3_n_0 ),
        .I2(a_bus[22]),
        .I3(a_bus[24]),
        .O(\aa_reg[25]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h88B8)) 
    \aa_reg[25]_i_4 
       (.I0(pc_current[25]),
        .I1(\upper_reg[0]_i_5_n_0 ),
        .I2(reg_source[25]),
        .I3(\upper_reg[0]_i_4_n_0 ),
        .O(a_bus[25]));
  LUT5 #(
    .INIT(32'hE0FFE000)) 
    \aa_reg[26]_i_1 
       (.I0(\aa_reg[29]_i_2_n_0 ),
        .I1(\aa_reg[29]_i_3_n_0 ),
        .I2(\aa_reg_reg[31]_1 [26]),
        .I3(\lower_reg_reg[31]_1 ),
        .I4(\aa_reg[26]_i_2_n_0 ),
        .O(\aa_reg_reg[31] [26]));
  LUT6 #(
    .INIT(64'hCC99F000CCCCF000)) 
    \aa_reg[26]_i_2 
       (.I0(\aa_reg[26]_i_3_n_0 ),
        .I1(a_bus[26]),
        .I2(\aa_reg_reg[31]_1 [26]),
        .I3(\aa_reg[31]_i_5_n_0 ),
        .I4(\aa_reg_reg[30] ),
        .I5(b_bus[31]),
        .O(\aa_reg[26]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT5 #(
    .INIT(32'h00000010)) 
    \aa_reg[26]_i_3 
       (.I0(a_bus[24]),
        .I1(a_bus[22]),
        .I2(\aa_reg[22]_i_3_n_0 ),
        .I3(a_bus[23]),
        .I4(a_bus[25]),
        .O(\aa_reg[26]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hE0FFE000)) 
    \aa_reg[27]_i_1 
       (.I0(\aa_reg[29]_i_2_n_0 ),
        .I1(\aa_reg[29]_i_3_n_0 ),
        .I2(\aa_reg_reg[31]_1 [27]),
        .I3(\lower_reg_reg[31]_1 ),
        .I4(\aa_reg[27]_i_2_n_0 ),
        .O(\aa_reg_reg[31] [27]));
  LUT6 #(
    .INIT(64'hCC99F000CCCCF000)) 
    \aa_reg[27]_i_2 
       (.I0(\aa_reg[27]_i_3_n_0 ),
        .I1(a_bus[27]),
        .I2(\aa_reg_reg[31]_1 [27]),
        .I3(\aa_reg[31]_i_5_n_0 ),
        .I4(\aa_reg_reg[30] ),
        .I5(b_bus[31]),
        .O(\aa_reg[27]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000010)) 
    \aa_reg[27]_i_3 
       (.I0(a_bus[25]),
        .I1(a_bus[23]),
        .I2(\aa_reg[22]_i_3_n_0 ),
        .I3(a_bus[22]),
        .I4(a_bus[24]),
        .I5(a_bus[26]),
        .O(\aa_reg[27]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \aa_reg[28]_i_1 
       (.I0(\aa_reg[28]_i_2_n_0 ),
        .I1(\lower_reg_reg[31]_1 ),
        .I2(\aa_reg[28]_i_3_n_0 ),
        .I3(\aa_reg[31]_i_5_n_0 ),
        .I4(\aa_reg[28]_i_4_n_0 ),
        .O(\aa_reg_reg[31] [28]));
  (* SOFT_HLUTNM = "soft_lutpair84" *) 
  LUT3 #(
    .INIT(8'hE0)) 
    \aa_reg[28]_i_2 
       (.I0(\aa_reg[29]_i_2_n_0 ),
        .I1(\aa_reg[29]_i_3_n_0 ),
        .I2(\aa_reg_reg[31]_1 [28]),
        .O(\aa_reg[28]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \aa_reg[28]_i_3 
       (.I0(a_bus[28]),
        .I1(\aa_reg_reg[30] ),
        .I2(\aa_reg_reg[31]_1 [28]),
        .O(\aa_reg[28]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT4 #(
    .INIT(16'hA028)) 
    \aa_reg[28]_i_4 
       (.I0(\aa_reg_reg[30] ),
        .I1(b_bus[31]),
        .I2(a_bus[28]),
        .I3(\aa_reg[28]_i_6_n_0 ),
        .O(\aa_reg[28]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h88B8)) 
    \aa_reg[28]_i_5 
       (.I0(pc_current[28]),
        .I1(\upper_reg[0]_i_5_n_0 ),
        .I2(reg_source[28]),
        .I3(\upper_reg[0]_i_4_n_0 ),
        .O(a_bus[28]));
  LUT2 #(
    .INIT(4'h2)) 
    \aa_reg[28]_i_6 
       (.I0(\aa_reg[27]_i_3_n_0 ),
        .I1(a_bus[27]),
        .O(\aa_reg[28]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hE0FFE000)) 
    \aa_reg[29]_i_1 
       (.I0(\aa_reg[29]_i_2_n_0 ),
        .I1(\aa_reg[29]_i_3_n_0 ),
        .I2(\aa_reg_reg[31]_1 [29]),
        .I3(\lower_reg_reg[31]_1 ),
        .I4(\aa_reg[29]_i_4_n_0 ),
        .O(\aa_reg_reg[31] [29]));
  LUT2 #(
    .INIT(4'hE)) 
    \aa_reg[29]_i_2 
       (.I0(\lower_reg_reg[31] ),
        .I1(\lower_reg_reg[31]_2 ),
        .O(\aa_reg[29]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'hBA)) 
    \aa_reg[29]_i_3 
       (.I0(\lower_reg_reg[31]_2 ),
        .I1(\lower_reg_reg[31] ),
        .I2(\lower_reg_reg[31]_0 ),
        .O(\aa_reg[29]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hF8F80808C8F83808)) 
    \aa_reg[29]_i_4 
       (.I0(\aa_reg_reg[31]_1 [29]),
        .I1(\aa_reg[31]_i_5_n_0 ),
        .I2(\aa_reg_reg[30] ),
        .I3(b_bus[31]),
        .I4(a_bus[29]),
        .I5(\aa_reg[29]_i_5_n_0 ),
        .O(\aa_reg[29]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT3 #(
    .INIT(8'h04)) 
    \aa_reg[29]_i_5 
       (.I0(a_bus[27]),
        .I1(\aa_reg[27]_i_3_n_0 ),
        .I2(a_bus[28]),
        .O(\aa_reg[29]_i_5_n_0 ));
  LUT5 #(
    .INIT(32'hE0FFE000)) 
    \aa_reg[2]_i_1 
       (.I0(\aa_reg[29]_i_2_n_0 ),
        .I1(\aa_reg[29]_i_3_n_0 ),
        .I2(\aa_reg_reg[31]_1 [2]),
        .I3(\lower_reg_reg[31]_1 ),
        .I4(\aa_reg[2]_i_2_n_0 ),
        .O(\aa_reg_reg[31] [2]));
  LUT6 #(
    .INIT(64'hF8F80808C8F83808)) 
    \aa_reg[2]_i_2 
       (.I0(\aa_reg_reg[31]_1 [2]),
        .I1(\aa_reg[31]_i_5_n_0 ),
        .I2(\aa_reg_reg[30] ),
        .I3(b_bus[31]),
        .I4(a_bus[2]),
        .I5(\aa_reg[2]_i_4_n_0 ),
        .O(\aa_reg[2]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAAAACFC0)) 
    \aa_reg[2]_i_3 
       (.I0(pc_current[2]),
        .I1(opcode[8]),
        .I2(\upper_reg[0]_i_4_n_0 ),
        .I3(reg_source[2]),
        .I4(\upper_reg[0]_i_5_n_0 ),
        .O(a_bus[2]));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \aa_reg[2]_i_4 
       (.I0(a_bus[0]),
        .I1(a_bus[1]),
        .O(\aa_reg[2]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hB8BBB888B888B888)) 
    \aa_reg[30]_i_1 
       (.I0(\aa_reg[30]_i_2_n_0 ),
        .I1(\lower_reg_reg[31]_1 ),
        .I2(\aa_reg[30]_i_3_n_0 ),
        .I3(\aa_reg[31]_i_5_n_0 ),
        .I4(\aa_reg_reg[30] ),
        .I5(\aa_reg[30]_i_5_n_0 ),
        .O(\aa_reg_reg[31] [30]));
  (* SOFT_HLUTNM = "soft_lutpair105" *) 
  LUT3 #(
    .INIT(8'hE0)) 
    \aa_reg[30]_i_2 
       (.I0(\aa_reg[29]_i_2_n_0 ),
        .I1(\aa_reg[29]_i_3_n_0 ),
        .I2(\aa_reg_reg[31]_1 [30]),
        .O(\aa_reg[30]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair105" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \aa_reg[30]_i_3 
       (.I0(a_bus[30]),
        .I1(\aa_reg_reg[30] ),
        .I2(\aa_reg_reg[31]_1 [30]),
        .O(\aa_reg[30]_i_3_n_0 ));
  LUT3 #(
    .INIT(8'h2A)) 
    \aa_reg[30]_i_4 
       (.I0(\lower_reg_reg[31]_2 ),
        .I1(\lower_reg_reg[31] ),
        .I2(\lower_reg_reg[31]_0 ),
        .O(\aa_reg_reg[30] ));
  (* SOFT_HLUTNM = "soft_lutpair118" *) 
  LUT3 #(
    .INIT(8'h9C)) 
    \aa_reg[30]_i_5 
       (.I0(\aa_reg[30]_i_7_n_0 ),
        .I1(a_bus[30]),
        .I2(b_bus[31]),
        .O(\aa_reg[30]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'h88B8)) 
    \aa_reg[30]_i_6 
       (.I0(pc_current[30]),
        .I1(\upper_reg[0]_i_5_n_0 ),
        .I2(reg_source[30]),
        .I3(\upper_reg[0]_i_4_n_0 ),
        .O(a_bus[30]));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT4 #(
    .INIT(16'h0004)) 
    \aa_reg[30]_i_7 
       (.I0(a_bus[28]),
        .I1(\aa_reg[27]_i_3_n_0 ),
        .I2(a_bus[27]),
        .I3(a_bus[29]),
        .O(\aa_reg[30]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF0000D000)) 
    \aa_reg[31]_i_1 
       (.I0(\lower_reg_reg[31]_2 ),
        .I1(\lower_reg_reg[31]_1 ),
        .I2(\lower_reg_reg[0] ),
        .I3(neqOp0_in),
        .I4(mode_reg_reg),
        .I5(\count_reg_reg[5] ),
        .O(\aa_reg_reg[31]_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \aa_reg[31]_i_2 
       (.I0(\aa_reg[31]_i_3_n_0 ),
        .I1(\lower_reg_reg[31]_1 ),
        .I2(\aa_reg[31]_i_4_n_0 ),
        .I3(\aa_reg[31]_i_5_n_0 ),
        .I4(\aa_reg[31]_i_6_n_0 ),
        .O(\aa_reg_reg[31] [31]));
  (* SOFT_HLUTNM = "soft_lutpair84" *) 
  LUT4 #(
    .INIT(16'hCDC8)) 
    \aa_reg[31]_i_3 
       (.I0(\aa_reg[29]_i_2_n_0 ),
        .I1(\bb_reg_reg[31]_5 [0]),
        .I2(\aa_reg[29]_i_3_n_0 ),
        .I3(b_bus[0]),
        .O(\aa_reg[31]_i_3_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \aa_reg[31]_i_4 
       (.I0(a_bus[31]),
        .I1(\aa_reg_reg[30] ),
        .I2(\bb_reg_reg[31]_5 [0]),
        .O(\aa_reg[31]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h7)) 
    \aa_reg[31]_i_5 
       (.I0(\lower_reg_reg[31] ),
        .I1(\lower_reg_reg[31]_2 ),
        .O(\aa_reg[31]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT5 #(
    .INIT(32'hACFFAC00)) 
    \aa_reg[31]_i_6 
       (.I0(\aa_reg[31]_i_7_n_0 ),
        .I1(a_bus[31]),
        .I2(b_bus[31]),
        .I3(\aa_reg_reg[30] ),
        .I4(b_bus[0]),
        .O(\aa_reg[31]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h00000010FFFFFFEF)) 
    \aa_reg[31]_i_7 
       (.I0(a_bus[30]),
        .I1(a_bus[28]),
        .I2(\aa_reg[27]_i_3_n_0 ),
        .I3(a_bus[27]),
        .I4(a_bus[29]),
        .I5(a_bus[31]),
        .O(\aa_reg[31]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hE0FFE000)) 
    \aa_reg[3]_i_1 
       (.I0(\aa_reg[29]_i_2_n_0 ),
        .I1(\aa_reg[29]_i_3_n_0 ),
        .I2(\aa_reg_reg[31]_1 [3]),
        .I3(\lower_reg_reg[31]_1 ),
        .I4(\aa_reg[3]_i_2_n_0 ),
        .O(\aa_reg_reg[31] [3]));
  LUT6 #(
    .INIT(64'hF8F80808C8F83808)) 
    \aa_reg[3]_i_2 
       (.I0(\aa_reg_reg[31]_1 [3]),
        .I1(\aa_reg[31]_i_5_n_0 ),
        .I2(\aa_reg_reg[30] ),
        .I3(b_bus[31]),
        .I4(a_bus[3]),
        .I5(\aa_reg[3]_i_4_n_0 ),
        .O(\aa_reg[3]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAAAACFC0)) 
    \aa_reg[3]_i_3 
       (.I0(pc_current[3]),
        .I1(opcode[9]),
        .I2(\upper_reg[0]_i_4_n_0 ),
        .I3(reg_source[3]),
        .I4(\upper_reg[0]_i_5_n_0 ),
        .O(a_bus[3]));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT3 #(
    .INIT(8'h01)) 
    \aa_reg[3]_i_4 
       (.I0(a_bus[1]),
        .I1(a_bus[0]),
        .I2(a_bus[2]),
        .O(\aa_reg[3]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hE0FFE000)) 
    \aa_reg[4]_i_1 
       (.I0(\aa_reg[29]_i_2_n_0 ),
        .I1(\aa_reg[29]_i_3_n_0 ),
        .I2(\aa_reg_reg[31]_1 [4]),
        .I3(\lower_reg_reg[31]_1 ),
        .I4(\aa_reg[4]_i_2_n_0 ),
        .O(\aa_reg_reg[31] [4]));
  LUT6 #(
    .INIT(64'hF8F80808C8F83808)) 
    \aa_reg[4]_i_2 
       (.I0(\aa_reg_reg[31]_1 [4]),
        .I1(\aa_reg[31]_i_5_n_0 ),
        .I2(\aa_reg_reg[30] ),
        .I3(b_bus[31]),
        .I4(a_bus[4]),
        .I5(\aa_reg[4]_i_3_n_0 ),
        .O(\aa_reg[4]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    \aa_reg[4]_i_3 
       (.I0(a_bus[2]),
        .I1(a_bus[0]),
        .I2(a_bus[1]),
        .I3(a_bus[3]),
        .O(\aa_reg[4]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hE0FFE000)) 
    \aa_reg[5]_i_1 
       (.I0(\aa_reg[29]_i_2_n_0 ),
        .I1(\aa_reg[29]_i_3_n_0 ),
        .I2(\aa_reg_reg[31]_1 [5]),
        .I3(\lower_reg_reg[31]_1 ),
        .I4(\aa_reg[5]_i_2_n_0 ),
        .O(\aa_reg_reg[31] [5]));
  LUT6 #(
    .INIT(64'hF8F80808C8F83808)) 
    \aa_reg[5]_i_2 
       (.I0(\aa_reg_reg[31]_1 [5]),
        .I1(\aa_reg[31]_i_5_n_0 ),
        .I2(\aa_reg_reg[30] ),
        .I3(b_bus[31]),
        .I4(a_bus[5]),
        .I5(\aa_reg[5]_i_4_n_0 ),
        .O(\aa_reg[5]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h88B8)) 
    \aa_reg[5]_i_3 
       (.I0(pc_current[5]),
        .I1(\upper_reg[0]_i_5_n_0 ),
        .I2(reg_source[5]),
        .I3(\upper_reg[0]_i_4_n_0 ),
        .O(a_bus[5]));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT5 #(
    .INIT(32'h00000001)) 
    \aa_reg[5]_i_4 
       (.I0(a_bus[3]),
        .I1(a_bus[1]),
        .I2(a_bus[0]),
        .I3(a_bus[2]),
        .I4(a_bus[4]),
        .O(\aa_reg[5]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hE0FFE000)) 
    \aa_reg[6]_i_1 
       (.I0(\aa_reg[29]_i_2_n_0 ),
        .I1(\aa_reg[29]_i_3_n_0 ),
        .I2(\aa_reg_reg[31]_1 [6]),
        .I3(\lower_reg_reg[31]_1 ),
        .I4(\aa_reg[6]_i_2_n_0 ),
        .O(\aa_reg_reg[31] [6]));
  LUT6 #(
    .INIT(64'hF8F80808C8F83808)) 
    \aa_reg[6]_i_2 
       (.I0(\aa_reg_reg[31]_1 [6]),
        .I1(\aa_reg[31]_i_5_n_0 ),
        .I2(\aa_reg_reg[30] ),
        .I3(b_bus[31]),
        .I4(a_bus[6]),
        .I5(\aa_reg[6]_i_4_n_0 ),
        .O(\aa_reg[6]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h88B8)) 
    \aa_reg[6]_i_3 
       (.I0(pc_current[6]),
        .I1(\upper_reg[0]_i_5_n_0 ),
        .I2(reg_source[6]),
        .I3(\upper_reg[0]_i_4_n_0 ),
        .O(a_bus[6]));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \aa_reg[6]_i_4 
       (.I0(a_bus[4]),
        .I1(a_bus[2]),
        .I2(a_bus[0]),
        .I3(a_bus[1]),
        .I4(a_bus[3]),
        .I5(a_bus[5]),
        .O(\aa_reg[6]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hE0FFE000)) 
    \aa_reg[7]_i_1 
       (.I0(\aa_reg[29]_i_2_n_0 ),
        .I1(\aa_reg[29]_i_3_n_0 ),
        .I2(\aa_reg_reg[31]_1 [7]),
        .I3(\lower_reg_reg[31]_1 ),
        .I4(\aa_reg[7]_i_2_n_0 ),
        .O(\aa_reg_reg[31] [7]));
  LUT6 #(
    .INIT(64'hF8F80808C8F83808)) 
    \aa_reg[7]_i_2 
       (.I0(\aa_reg_reg[31]_1 [7]),
        .I1(\aa_reg[31]_i_5_n_0 ),
        .I2(\aa_reg_reg[30] ),
        .I3(b_bus[31]),
        .I4(a_bus[7]),
        .I5(\aa_reg[7]_i_4_n_0 ),
        .O(\aa_reg[7]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h88B8)) 
    \aa_reg[7]_i_3 
       (.I0(pc_current[7]),
        .I1(\upper_reg[0]_i_5_n_0 ),
        .I2(reg_source[7]),
        .I3(\upper_reg[0]_i_4_n_0 ),
        .O(a_bus[7]));
  LUT6 #(
    .INIT(64'h0000000000000010)) 
    \aa_reg[7]_i_4 
       (.I0(a_bus[5]),
        .I1(a_bus[3]),
        .I2(\aa_reg[2]_i_4_n_0 ),
        .I3(a_bus[2]),
        .I4(a_bus[4]),
        .I5(a_bus[6]),
        .O(\aa_reg[7]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hE0FFE000)) 
    \aa_reg[8]_i_1 
       (.I0(\aa_reg[29]_i_2_n_0 ),
        .I1(\aa_reg[29]_i_3_n_0 ),
        .I2(\aa_reg_reg[31]_1 [8]),
        .I3(\lower_reg_reg[31]_1 ),
        .I4(\aa_reg[8]_i_2_n_0 ),
        .O(\aa_reg_reg[31] [8]));
  LUT6 #(
    .INIT(64'hF8F80808C8F83808)) 
    \aa_reg[8]_i_2 
       (.I0(\aa_reg_reg[31]_1 [8]),
        .I1(\aa_reg[31]_i_5_n_0 ),
        .I2(\aa_reg_reg[30] ),
        .I3(b_bus[31]),
        .I4(a_bus[8]),
        .I5(\aa_reg[8]_i_4_n_0 ),
        .O(\aa_reg[8]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h88B8)) 
    \aa_reg[8]_i_3 
       (.I0(pc_current[8]),
        .I1(\upper_reg[0]_i_5_n_0 ),
        .I2(reg_source[8]),
        .I3(\upper_reg[0]_i_4_n_0 ),
        .O(a_bus[8]));
  (* SOFT_HLUTNM = "soft_lutpair82" *) 
  LUT3 #(
    .INIT(8'h04)) 
    \aa_reg[8]_i_4 
       (.I0(a_bus[6]),
        .I1(\aa_reg[6]_i_4_n_0 ),
        .I2(a_bus[7]),
        .O(\aa_reg[8]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hE0FFE000)) 
    \aa_reg[9]_i_1 
       (.I0(\aa_reg[29]_i_2_n_0 ),
        .I1(\aa_reg[29]_i_3_n_0 ),
        .I2(\aa_reg_reg[31]_1 [9]),
        .I3(\lower_reg_reg[31]_1 ),
        .I4(\aa_reg[9]_i_2_n_0 ),
        .O(\aa_reg_reg[31] [9]));
  LUT6 #(
    .INIT(64'hF8F80808C8F83808)) 
    \aa_reg[9]_i_2 
       (.I0(\aa_reg_reg[31]_1 [9]),
        .I1(\aa_reg[31]_i_5_n_0 ),
        .I2(\aa_reg_reg[30] ),
        .I3(b_bus[31]),
        .I4(a_bus[9]),
        .I5(\aa_reg[9]_i_4_n_0 ),
        .O(\aa_reg[9]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h88B8)) 
    \aa_reg[9]_i_3 
       (.I0(pc_current[9]),
        .I1(\upper_reg[0]_i_5_n_0 ),
        .I2(reg_source[9]),
        .I3(\upper_reg[0]_i_4_n_0 ),
        .O(a_bus[9]));
  (* SOFT_HLUTNM = "soft_lutpair82" *) 
  LUT4 #(
    .INIT(16'h0004)) 
    \aa_reg[9]_i_4 
       (.I0(a_bus[7]),
        .I1(\aa_reg[6]_i_4_n_0 ),
        .I2(a_bus[6]),
        .I3(a_bus[8]),
        .O(\aa_reg[9]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hF4F1B0E0)) 
    \address_reg[20]_i_1 
       (.I0(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I1(mem_state_reg_reg_n_0),
        .I2(pc_future[20]),
        .I3(pause_in0_out),
        .I4(pc_new[20]),
        .O(complete_address_next[20]));
  LUT4 #(
    .INIT(16'hAAB8)) 
    \address_reg[20]_i_2 
       (.I0(pc_current[20]),
        .I1(pause_in),
        .I2(\pc_reg_reg[20] ),
        .I3(\address_reg_reg[24]_0 ),
        .O(pc_future[20]));
  LUT5 #(
    .INIT(32'hF4F1B0E0)) 
    \address_reg[21]_i_1 
       (.I0(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I1(mem_state_reg_reg_n_0),
        .I2(pc_future[21]),
        .I3(pause_in0_out),
        .I4(pc_new[21]),
        .O(complete_address_next[21]));
  LUT4 #(
    .INIT(16'hAAB8)) 
    \address_reg[21]_i_2 
       (.I0(pc_current[21]),
        .I1(pause_in),
        .I2(\pc_reg_reg[21] ),
        .I3(\address_reg_reg[24]_0 ),
        .O(pc_future[21]));
  LUT5 #(
    .INIT(32'hF4F1B0E0)) 
    \address_reg[22]_i_1 
       (.I0(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I1(mem_state_reg_reg_n_0),
        .I2(pc_future[22]),
        .I3(pause_in0_out),
        .I4(pc_new[22]),
        .O(complete_address_next[22]));
  LUT4 #(
    .INIT(16'hAAB8)) 
    \address_reg[22]_i_2 
       (.I0(pc_current[22]),
        .I1(pause_in),
        .I2(\pc_reg_reg[22] ),
        .I3(\address_reg_reg[24]_0 ),
        .O(pc_future[22]));
  LUT5 #(
    .INIT(32'hF4F1B0E0)) 
    \address_reg[23]_i_1 
       (.I0(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I1(mem_state_reg_reg_n_0),
        .I2(pc_future[23]),
        .I3(pause_in0_out),
        .I4(pc_new[23]),
        .O(complete_address_next[23]));
  LUT4 #(
    .INIT(16'hAAB8)) 
    \address_reg[23]_i_2 
       (.I0(pc_current[23]),
        .I1(pause_in),
        .I2(\pc_reg_reg[23] ),
        .I3(\address_reg_reg[24]_0 ),
        .O(pc_future[23]));
  LUT5 #(
    .INIT(32'hF4F1B0E0)) 
    \address_reg[24]_i_1 
       (.I0(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I1(mem_state_reg_reg_n_0),
        .I2(pc_future[24]),
        .I3(pause_in0_out),
        .I4(pc_new[24]),
        .O(complete_address_next[24]));
  LUT4 #(
    .INIT(16'hAAB8)) 
    \address_reg[24]_i_2 
       (.I0(pc_current[24]),
        .I1(pause_in),
        .I2(\pc_reg_reg[24] ),
        .I3(\address_reg_reg[24]_0 ),
        .O(pc_future[24]));
  LUT5 #(
    .INIT(32'hF4F1B0E0)) 
    \address_reg[25]_i_1 
       (.I0(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I1(mem_state_reg_reg_n_0),
        .I2(pc_future[25]),
        .I3(pause_in0_out),
        .I4(pc_new[25]),
        .O(complete_address_next[25]));
  LUT4 #(
    .INIT(16'hAAB8)) 
    \address_reg[25]_i_2 
       (.I0(pc_current[25]),
        .I1(pause_in),
        .I2(\pc_reg_reg[25]_0 ),
        .I3(\address_reg_reg[24]_0 ),
        .O(pc_future[25]));
  LUT5 #(
    .INIT(32'hF4F1B0E0)) 
    \address_reg[26]_i_1 
       (.I0(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I1(mem_state_reg_reg_n_0),
        .I2(pc_future[26]),
        .I3(pause_in0_out),
        .I4(pc_new[26]),
        .O(complete_address_next[26]));
  LUT4 #(
    .INIT(16'hAAB8)) 
    \address_reg[26]_i_2 
       (.I0(pc_current[26]),
        .I1(pause_in),
        .I2(\pc_reg_reg[26] ),
        .I3(\address_reg_reg[24]_0 ),
        .O(pc_future[26]));
  LUT5 #(
    .INIT(32'hF4F1B0E0)) 
    \address_reg[27]_i_1 
       (.I0(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I1(mem_state_reg_reg_n_0),
        .I2(pc_future[27]),
        .I3(pause_in0_out),
        .I4(pc_new[27]),
        .O(complete_address_next[27]));
  LUT4 #(
    .INIT(16'hAAB8)) 
    \address_reg[27]_i_2 
       (.I0(pc_current[27]),
        .I1(pause_in),
        .I2(\pc_reg_reg[27] ),
        .I3(\address_reg_reg[24]_0 ),
        .O(pc_future[27]));
  LUT5 #(
    .INIT(32'hF4F1B0E0)) 
    \address_reg[28]_i_1 
       (.I0(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I1(mem_state_reg_reg_n_0),
        .I2(pc_future[28]),
        .I3(pause_in0_out),
        .I4(pc_new[28]),
        .O(complete_address_next[28]));
  LUT4 #(
    .INIT(16'hAAB8)) 
    \address_reg[28]_i_2 
       (.I0(pc_current[28]),
        .I1(pause_in),
        .I2(\pc_reg_reg[28] ),
        .I3(\address_reg_reg[24]_0 ),
        .O(pc_future[28]));
  LUT5 #(
    .INIT(32'hF4F1B0E0)) 
    \address_reg[29]_i_1 
       (.I0(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I1(mem_state_reg_reg_n_0),
        .I2(pc_future[29]),
        .I3(pause_in0_out),
        .I4(pc_new[29]),
        .O(complete_address_next[29]));
  LUT4 #(
    .INIT(16'hAAB8)) 
    \address_reg[29]_i_2 
       (.I0(pc_current[29]),
        .I1(pause_in),
        .I2(\pc_reg_reg[29] ),
        .I3(\address_reg_reg[24]_0 ),
        .O(pc_future[29]));
  LUT5 #(
    .INIT(32'hF4F1B0E0)) 
    \address_reg[30]_i_1 
       (.I0(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I1(mem_state_reg_reg_n_0),
        .I2(pc_future[30]),
        .I3(pause_in0_out),
        .I4(pc_new[30]),
        .O(complete_address_next[30]));
  LUT4 #(
    .INIT(16'hAAB8)) 
    \address_reg[30]_i_2 
       (.I0(pc_current[30]),
        .I1(pause_in),
        .I2(\pc_reg_reg[30] ),
        .I3(\address_reg_reg[24]_0 ),
        .O(pc_future[30]));
  LUT5 #(
    .INIT(32'hF4F1B0E0)) 
    \address_reg[31]_i_1 
       (.I0(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I1(mem_state_reg_reg_n_0),
        .I2(pc_future[31]),
        .I3(pause_in0_out),
        .I4(pc_new[31]),
        .O(complete_address_next[31]));
  LUT6 #(
    .INIT(64'hFF00FF00FF54FE54)) 
    \address_reg[31]_i_2 
       (.I0(pause_in),
        .I1(\pc_reg_reg[29]_0 ),
        .I2(\address_reg[31]_i_4_n_0 ),
        .I3(pc_current[31]),
        .I4(\pc_reg[3]_i_4_n_0 ),
        .I5(\address_reg_reg[24]_0 ),
        .O(pc_future[31]));
  LUT5 #(
    .INIT(32'hA8A8A808)) 
    \address_reg[31]_i_4 
       (.I0(pc_source[1]),
        .I1(pc_plus4[27]),
        .I2(take_branch),
        .I3(\address_reg[31]_i_5_n_0 ),
        .I4(c_alu[31]),
        .O(\address_reg[31]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFEFEFEAEAEAEFEAE)) 
    \address_reg[31]_i_5 
       (.I0(c_mult[31]),
        .I1(\pc_reg[31]_i_25_n_0 ),
        .I2(\bram_firmware_code_address_reg[15]_i_7_n_0 ),
        .I3(shift8L[31]),
        .I4(a_bus[4]),
        .I5(shift8L[15]),
        .O(\address_reg[31]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF8000000F400)) 
    \address_reg[31]_i_6 
       (.I0(\lower_reg_reg[29] ),
        .I1(\pc_reg_reg[30]_0 ),
        .I2(\pc_reg_reg[31]_1 ),
        .I3(\pc_reg_reg[30]_1 ),
        .I4(\bram_firmware_code_address_reg[19]_i_10_n_0 ),
        .I5(\lower_reg_reg[31]_5 [31]),
        .O(c_mult[31]));
  FDCE \address_reg_reg[20] 
       (.C(clk),
        .CE(mem_state_reg),
        .CLR(\address_reg_reg[24]_0 ),
        .D(complete_address_next[20]),
        .Q(cpu_address_misc_signals_selection_of_memory[0]));
  FDCE \address_reg_reg[21] 
       (.C(clk),
        .CE(mem_state_reg),
        .CLR(\address_reg_reg[24]_0 ),
        .D(complete_address_next[21]),
        .Q(cpu_address_misc_signals_selection_of_memory[1]));
  FDCE \address_reg_reg[22] 
       (.C(clk),
        .CE(mem_state_reg),
        .CLR(\address_reg_reg[24]_0 ),
        .D(complete_address_next[22]),
        .Q(cpu_address_misc_signals_selection_of_memory[2]));
  FDCE \address_reg_reg[23] 
       (.C(clk),
        .CE(mem_state_reg),
        .CLR(\address_reg_reg[24]_0 ),
        .D(complete_address_next[23]),
        .Q(cpu_address_misc_signals_selection_of_memory[3]));
  FDCE \address_reg_reg[24] 
       (.C(clk),
        .CE(mem_state_reg),
        .CLR(\address_reg_reg[24]_0 ),
        .D(complete_address_next[24]),
        .Q(cpu_address_misc_ip_selection_of_memory[0]));
  FDCE \address_reg_reg[25] 
       (.C(clk),
        .CE(mem_state_reg),
        .CLR(\address_reg_reg[24]_0 ),
        .D(complete_address_next[25]),
        .Q(cpu_address_misc_ip_selection_of_memory[1]));
  FDCE \address_reg_reg[26] 
       (.C(clk),
        .CE(mem_state_reg),
        .CLR(\address_reg_reg[24]_0 ),
        .D(complete_address_next[26]),
        .Q(cpu_address_misc_ip_selection_of_memory[2]));
  FDCE \address_reg_reg[27] 
       (.C(clk),
        .CE(mem_state_reg),
        .CLR(\address_reg_reg[24]_0 ),
        .D(complete_address_next[27]),
        .Q(cpu_address_misc_ip_selection_of_memory[3]));
  FDCE \address_reg_reg[28] 
       (.C(clk),
        .CE(mem_state_reg),
        .CLR(\address_reg_reg[24]_0 ),
        .D(complete_address_next[28]),
        .Q(cpu_address_selection_of_memory_area[0]));
  FDCE \address_reg_reg[29] 
       (.C(clk),
        .CE(mem_state_reg),
        .CLR(\address_reg_reg[24]_0 ),
        .D(complete_address_next[29]),
        .Q(cpu_address_selection_of_memory_area[1]));
  FDCE \address_reg_reg[30] 
       (.C(clk),
        .CE(mem_state_reg),
        .CLR(\address_reg_reg[24]_0 ),
        .D(complete_address_next[30]),
        .Q(cpu_address_selection_of_memory_area[2]));
  FDCE \address_reg_reg[31] 
       (.C(clk),
        .CE(mem_state_reg),
        .CLR(\address_reg_reg[24]_0 ),
        .D(complete_address_next[31]),
        .Q(cpu_address_selection_of_memory_area[3]));
  (* SOFT_HLUTNM = "soft_lutpair98" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \bb_reg[0]_i_1 
       (.I0(b_bus[0]),
        .I1(\count_reg_reg[5] ),
        .I2(\bb_reg_reg[31]_5 [0]),
        .O(\bb_reg_reg[31]_4 [0]));
  LUT6 #(
    .INIT(64'hBF40FFFFBF400000)) 
    \bb_reg[10]_i_1 
       (.I0(\bb_reg[10]_i_2_n_0 ),
        .I1(b_bus[31]),
        .I2(\bb_reg[31]_i_3_n_0 ),
        .I3(b_bus[10]),
        .I4(\count_reg_reg[5] ),
        .I5(\bb_reg_reg[31]_5 [10]),
        .O(\bb_reg_reg[31]_4 [10]));
  LUT2 #(
    .INIT(4'h2)) 
    \bb_reg[10]_i_2 
       (.I0(\bb_reg[9]_i_2_n_0 ),
        .I1(b_bus[9]),
        .O(\bb_reg[10]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB8F3B8C0)) 
    \bb_reg[10]_i_3 
       (.I0(opcode[8]),
        .I1(b_source),
        .I2(opcode[10]),
        .I3(\bb_reg[31]_i_6_n_0 ),
        .I4(reg_target[10]),
        .O(b_bus[10]));
  LUT6 #(
    .INIT(64'hBF40FFFFBF400000)) 
    \bb_reg[11]_i_1 
       (.I0(\bb_reg[11]_i_2_n_0 ),
        .I1(b_bus[31]),
        .I2(\bb_reg[31]_i_3_n_0 ),
        .I3(b_bus[11]),
        .I4(\count_reg_reg[5] ),
        .I5(\bb_reg_reg[31]_5 [11]),
        .O(\bb_reg_reg[31]_4 [11]));
  LUT2 #(
    .INIT(4'h2)) 
    \bb_reg[11]_i_2 
       (.I0(\bb_reg[10]_i_2_n_0 ),
        .I1(b_bus[10]),
        .O(\bb_reg[11]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB8F3B8C0)) 
    \bb_reg[11]_i_3 
       (.I0(opcode[9]),
        .I1(b_source),
        .I2(opcode[11]),
        .I3(\bb_reg[31]_i_6_n_0 ),
        .I4(reg_target[11]),
        .O(b_bus[11]));
  LUT6 #(
    .INIT(64'hBF40FFFFBF400000)) 
    \bb_reg[12]_i_1 
       (.I0(\bb_reg[12]_i_2_n_0 ),
        .I1(b_bus[31]),
        .I2(\bb_reg[31]_i_3_n_0 ),
        .I3(b_bus[12]),
        .I4(\count_reg_reg[5] ),
        .I5(\bb_reg_reg[31]_5 [12]),
        .O(\bb_reg_reg[31]_4 [12]));
  LUT2 #(
    .INIT(4'h2)) 
    \bb_reg[12]_i_2 
       (.I0(\bb_reg[11]_i_2_n_0 ),
        .I1(b_bus[11]),
        .O(\bb_reg[12]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB8F3B8C0)) 
    \bb_reg[12]_i_3 
       (.I0(opcode[10]),
        .I1(b_source),
        .I2(\pc_reg_reg[14] ),
        .I3(\bb_reg[31]_i_6_n_0 ),
        .I4(reg_target[12]),
        .O(b_bus[12]));
  LUT6 #(
    .INIT(64'hBF40FFFFBF400000)) 
    \bb_reg[13]_i_1 
       (.I0(\bb_reg[13]_i_2_n_0 ),
        .I1(b_bus[31]),
        .I2(\bb_reg[31]_i_3_n_0 ),
        .I3(b_bus[13]),
        .I4(\count_reg_reg[5] ),
        .I5(\bb_reg_reg[31]_5 [13]),
        .O(\bb_reg_reg[31]_4 [13]));
  LUT2 #(
    .INIT(4'h2)) 
    \bb_reg[13]_i_2 
       (.I0(\bb_reg[12]_i_2_n_0 ),
        .I1(b_bus[12]),
        .O(\bb_reg[13]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB8F3B8C0)) 
    \bb_reg[13]_i_3 
       (.I0(opcode[11]),
        .I1(b_source),
        .I2(opcode[13]),
        .I3(\bb_reg[31]_i_6_n_0 ),
        .I4(reg_target[13]),
        .O(b_bus[13]));
  LUT6 #(
    .INIT(64'hBF40FFFFBF400000)) 
    \bb_reg[14]_i_1 
       (.I0(\bb_reg[14]_i_2_n_0 ),
        .I1(b_bus[31]),
        .I2(\bb_reg[31]_i_3_n_0 ),
        .I3(b_bus[14]),
        .I4(\count_reg_reg[5] ),
        .I5(\bb_reg_reg[31]_5 [14]),
        .O(\bb_reg_reg[31]_4 [14]));
  LUT2 #(
    .INIT(4'h2)) 
    \bb_reg[14]_i_2 
       (.I0(\bb_reg[13]_i_2_n_0 ),
        .I1(b_bus[13]),
        .O(\bb_reg[14]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB8F3B8C0)) 
    \bb_reg[14]_i_3 
       (.I0(\pc_reg_reg[14] ),
        .I1(b_source),
        .I2(opcode[14]),
        .I3(\bb_reg[31]_i_6_n_0 ),
        .I4(reg_target[14]),
        .O(b_bus[14]));
  LUT6 #(
    .INIT(64'hBF40FFFFBF400000)) 
    \bb_reg[15]_i_1 
       (.I0(\bb_reg[15]_i_2_n_0 ),
        .I1(b_bus[31]),
        .I2(\bb_reg[31]_i_3_n_0 ),
        .I3(b_bus[15]),
        .I4(\count_reg_reg[5] ),
        .I5(\bb_reg_reg[31]_5 [15]),
        .O(\bb_reg_reg[31]_4 [15]));
  LUT2 #(
    .INIT(4'h2)) 
    \bb_reg[15]_i_2 
       (.I0(\bb_reg[14]_i_2_n_0 ),
        .I1(b_bus[14]),
        .O(\bb_reg[15]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB8F3B8C0)) 
    \bb_reg[15]_i_3 
       (.I0(opcode[13]),
        .I1(b_source),
        .I2(opcode[15]),
        .I3(\bb_reg[31]_i_6_n_0 ),
        .I4(reg_target[15]),
        .O(b_bus[15]));
  LUT6 #(
    .INIT(64'hBF40FFFFBF400000)) 
    \bb_reg[16]_i_1 
       (.I0(\bb_reg[16]_i_2_n_0 ),
        .I1(b_bus[31]),
        .I2(\bb_reg[31]_i_3_n_0 ),
        .I3(b_bus[16]),
        .I4(\count_reg_reg[5] ),
        .I5(\bb_reg_reg[31]_5 [16]),
        .O(\bb_reg_reg[31]_4 [16]));
  LUT2 #(
    .INIT(4'h2)) 
    \bb_reg[16]_i_2 
       (.I0(\bb_reg[15]_i_2_n_0 ),
        .I1(b_bus[15]),
        .O(\bb_reg[16]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB833B800)) 
    \bb_reg[16]_i_3 
       (.I0(opcode[14]),
        .I1(b_source),
        .I2(opcode[15]),
        .I3(\bb_reg[31]_i_6_n_0 ),
        .I4(reg_target[16]),
        .O(b_bus[16]));
  LUT6 #(
    .INIT(64'hBF40FFFFBF400000)) 
    \bb_reg[17]_i_1 
       (.I0(\bb_reg[17]_i_2_n_0 ),
        .I1(b_bus[31]),
        .I2(\bb_reg[31]_i_3_n_0 ),
        .I3(b_bus[17]),
        .I4(\count_reg_reg[5] ),
        .I5(\bb_reg_reg[31]_5 [17]),
        .O(\bb_reg_reg[31]_4 [17]));
  LUT2 #(
    .INIT(4'h2)) 
    \bb_reg[17]_i_2 
       (.I0(\bb_reg[16]_i_2_n_0 ),
        .I1(b_bus[16]),
        .O(\bb_reg[17]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT4 #(
    .INIT(16'h88B8)) 
    \bb_reg[17]_i_3 
       (.I0(opcode[15]),
        .I1(\bb_reg[31]_i_6_n_0 ),
        .I2(reg_target[17]),
        .I3(b_source),
        .O(b_bus[17]));
  LUT6 #(
    .INIT(64'hBF40FFFFBF400000)) 
    \bb_reg[18]_i_1 
       (.I0(\bb_reg[18]_i_2_n_0 ),
        .I1(b_bus[31]),
        .I2(\bb_reg[31]_i_3_n_0 ),
        .I3(b_bus[18]),
        .I4(\count_reg_reg[5] ),
        .I5(\bb_reg_reg[31]_5 [18]),
        .O(\bb_reg_reg[31]_4 [18]));
  LUT2 #(
    .INIT(4'h2)) 
    \bb_reg[18]_i_2 
       (.I0(\bb_reg[17]_i_2_n_0 ),
        .I1(b_bus[17]),
        .O(\bb_reg[18]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT4 #(
    .INIT(16'h88B8)) 
    \bb_reg[18]_i_3 
       (.I0(opcode[15]),
        .I1(\bb_reg[31]_i_6_n_0 ),
        .I2(reg_target[18]),
        .I3(b_source),
        .O(b_bus[18]));
  LUT6 #(
    .INIT(64'hBF40FFFFBF400000)) 
    \bb_reg[19]_i_1 
       (.I0(\bb_reg[19]_i_2_n_0 ),
        .I1(b_bus[31]),
        .I2(\bb_reg[31]_i_3_n_0 ),
        .I3(b_bus[19]),
        .I4(\count_reg_reg[5] ),
        .I5(\bb_reg_reg[31]_5 [19]),
        .O(\bb_reg_reg[31]_4 [19]));
  LUT2 #(
    .INIT(4'h2)) 
    \bb_reg[19]_i_2 
       (.I0(\bb_reg[18]_i_2_n_0 ),
        .I1(b_bus[18]),
        .O(\bb_reg[19]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT4 #(
    .INIT(16'h88B8)) 
    \bb_reg[19]_i_3 
       (.I0(opcode[15]),
        .I1(\bb_reg[31]_i_6_n_0 ),
        .I2(reg_target[19]),
        .I3(b_source),
        .O(b_bus[19]));
  LUT6 #(
    .INIT(64'h7F80FFFF7F800000)) 
    \bb_reg[1]_i_1 
       (.I0(b_bus[0]),
        .I1(b_bus[31]),
        .I2(\bb_reg[31]_i_3_n_0 ),
        .I3(b_bus[1]),
        .I4(\count_reg_reg[5] ),
        .I5(\bb_reg_reg[31]_5 [1]),
        .O(\bb_reg_reg[31]_4 [1]));
  LUT5 #(
    .INIT(32'h4D484848)) 
    \bb_reg[1]_i_2 
       (.I0(b_source),
        .I1(opcode[0]),
        .I2(\bb_reg[31]_i_6_n_0 ),
        .I3(data0),
        .I4(\bb_reg_reg[1]_4 ),
        .O(b_bus[0]));
  (* SOFT_HLUTNM = "soft_lutpair75" *) 
  LUT4 #(
    .INIT(16'h4D48)) 
    \bb_reg[1]_i_3 
       (.I0(b_source),
        .I1(opcode[1]),
        .I2(\bb_reg[31]_i_6_n_0 ),
        .I3(reg_target[1]),
        .O(b_bus[1]));
  LUT6 #(
    .INIT(64'hBF40FFFFBF400000)) 
    \bb_reg[20]_i_1 
       (.I0(\bb_reg[20]_i_2_n_0 ),
        .I1(b_bus[31]),
        .I2(\bb_reg[31]_i_3_n_0 ),
        .I3(b_bus[20]),
        .I4(\count_reg_reg[5] ),
        .I5(\bb_reg_reg[31]_5 [20]),
        .O(\bb_reg_reg[31]_4 [20]));
  LUT2 #(
    .INIT(4'h2)) 
    \bb_reg[20]_i_2 
       (.I0(\bb_reg[19]_i_2_n_0 ),
        .I1(b_bus[19]),
        .O(\bb_reg[20]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT4 #(
    .INIT(16'h88B8)) 
    \bb_reg[20]_i_3 
       (.I0(opcode[15]),
        .I1(\bb_reg[31]_i_6_n_0 ),
        .I2(reg_target[20]),
        .I3(b_source),
        .O(b_bus[20]));
  LUT6 #(
    .INIT(64'hBF40FFFFBF400000)) 
    \bb_reg[21]_i_1 
       (.I0(\bb_reg[21]_i_2_n_0 ),
        .I1(b_bus[31]),
        .I2(\bb_reg[31]_i_3_n_0 ),
        .I3(b_bus[21]),
        .I4(\count_reg_reg[5] ),
        .I5(\bb_reg_reg[31]_5 [21]),
        .O(\bb_reg_reg[31]_4 [21]));
  LUT2 #(
    .INIT(4'h2)) 
    \bb_reg[21]_i_2 
       (.I0(\bb_reg[20]_i_2_n_0 ),
        .I1(b_bus[20]),
        .O(\bb_reg[21]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT4 #(
    .INIT(16'h88B8)) 
    \bb_reg[21]_i_3 
       (.I0(opcode[15]),
        .I1(\bb_reg[31]_i_6_n_0 ),
        .I2(reg_target[21]),
        .I3(b_source),
        .O(b_bus[21]));
  LUT6 #(
    .INIT(64'hBF40FFFFBF400000)) 
    \bb_reg[22]_i_1 
       (.I0(\bb_reg[22]_i_2_n_0 ),
        .I1(b_bus[31]),
        .I2(\bb_reg[31]_i_3_n_0 ),
        .I3(b_bus[22]),
        .I4(\count_reg_reg[5] ),
        .I5(\bb_reg_reg[31]_5 [22]),
        .O(\bb_reg_reg[31]_4 [22]));
  (* SOFT_HLUTNM = "soft_lutpair135" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \bb_reg[22]_i_2 
       (.I0(\bb_reg[21]_i_2_n_0 ),
        .I1(b_bus[21]),
        .O(\bb_reg[22]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT4 #(
    .INIT(16'h88B8)) 
    \bb_reg[22]_i_3 
       (.I0(opcode[15]),
        .I1(\bb_reg[31]_i_6_n_0 ),
        .I2(reg_target[22]),
        .I3(b_source),
        .O(b_bus[22]));
  LUT6 #(
    .INIT(64'hBF40FFFFBF400000)) 
    \bb_reg[23]_i_1 
       (.I0(\bb_reg[23]_i_2_n_0 ),
        .I1(b_bus[31]),
        .I2(\bb_reg[31]_i_3_n_0 ),
        .I3(b_bus[23]),
        .I4(\count_reg_reg[5] ),
        .I5(\bb_reg_reg[31]_5 [23]),
        .O(\bb_reg_reg[31]_4 [23]));
  (* SOFT_HLUTNM = "soft_lutpair135" *) 
  LUT3 #(
    .INIT(8'h04)) 
    \bb_reg[23]_i_2 
       (.I0(b_bus[21]),
        .I1(\bb_reg[21]_i_2_n_0 ),
        .I2(b_bus[22]),
        .O(\bb_reg[23]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair70" *) 
  LUT4 #(
    .INIT(16'h88B8)) 
    \bb_reg[23]_i_3 
       (.I0(opcode[15]),
        .I1(\bb_reg[31]_i_6_n_0 ),
        .I2(reg_target[23]),
        .I3(b_source),
        .O(b_bus[23]));
  LUT6 #(
    .INIT(64'hBF40FFFFBF400000)) 
    \bb_reg[24]_i_1 
       (.I0(\bb_reg[24]_i_2_n_0 ),
        .I1(b_bus[31]),
        .I2(\bb_reg[31]_i_3_n_0 ),
        .I3(b_bus[24]),
        .I4(\count_reg_reg[5] ),
        .I5(\bb_reg_reg[31]_5 [24]),
        .O(\bb_reg_reg[31]_4 [24]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT4 #(
    .INIT(16'h0004)) 
    \bb_reg[24]_i_2 
       (.I0(b_bus[22]),
        .I1(\bb_reg[21]_i_2_n_0 ),
        .I2(b_bus[21]),
        .I3(b_bus[23]),
        .O(\bb_reg[24]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair70" *) 
  LUT4 #(
    .INIT(16'h88B8)) 
    \bb_reg[24]_i_3 
       (.I0(opcode[15]),
        .I1(\bb_reg[31]_i_6_n_0 ),
        .I2(reg_target[24]),
        .I3(b_source),
        .O(b_bus[24]));
  LUT6 #(
    .INIT(64'hBF40FFFFBF400000)) 
    \bb_reg[25]_i_1 
       (.I0(\bb_reg[25]_i_2_n_0 ),
        .I1(b_bus[31]),
        .I2(\bb_reg[31]_i_3_n_0 ),
        .I3(b_bus[25]),
        .I4(\count_reg_reg[5] ),
        .I5(\bb_reg_reg[31]_5 [25]),
        .O(\bb_reg_reg[31]_4 [25]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT5 #(
    .INIT(32'h00000010)) 
    \bb_reg[25]_i_2 
       (.I0(b_bus[23]),
        .I1(b_bus[21]),
        .I2(\bb_reg[21]_i_2_n_0 ),
        .I3(b_bus[22]),
        .I4(b_bus[24]),
        .O(\bb_reg[25]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair71" *) 
  LUT4 #(
    .INIT(16'h88B8)) 
    \bb_reg[25]_i_3 
       (.I0(opcode[15]),
        .I1(\bb_reg[31]_i_6_n_0 ),
        .I2(reg_target[25]),
        .I3(b_source),
        .O(b_bus[25]));
  LUT6 #(
    .INIT(64'hBF40FFFFBF400000)) 
    \bb_reg[26]_i_1 
       (.I0(\bb_reg[26]_i_2_n_0 ),
        .I1(b_bus[31]),
        .I2(\bb_reg[31]_i_3_n_0 ),
        .I3(b_bus[26]),
        .I4(\count_reg_reg[5] ),
        .I5(\bb_reg_reg[31]_5 [26]),
        .O(\bb_reg_reg[31]_4 [26]));
  LUT6 #(
    .INIT(64'h0000000000000010)) 
    \bb_reg[26]_i_2 
       (.I0(b_bus[24]),
        .I1(b_bus[22]),
        .I2(\bb_reg[21]_i_2_n_0 ),
        .I3(b_bus[21]),
        .I4(b_bus[23]),
        .I5(b_bus[25]),
        .O(\bb_reg[26]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair71" *) 
  LUT4 #(
    .INIT(16'h88B8)) 
    \bb_reg[26]_i_3 
       (.I0(opcode[15]),
        .I1(\bb_reg[31]_i_6_n_0 ),
        .I2(reg_target[26]),
        .I3(b_source),
        .O(b_bus[26]));
  LUT6 #(
    .INIT(64'hBF40FFFFBF400000)) 
    \bb_reg[27]_i_1 
       (.I0(\bb_reg[27]_i_2_n_0 ),
        .I1(b_bus[31]),
        .I2(\bb_reg[31]_i_3_n_0 ),
        .I3(b_bus[27]),
        .I4(\count_reg_reg[5] ),
        .I5(\bb_reg_reg[31]_5 [27]),
        .O(\bb_reg_reg[31]_4 [27]));
  LUT2 #(
    .INIT(4'h2)) 
    \bb_reg[27]_i_2 
       (.I0(\bb_reg[26]_i_2_n_0 ),
        .I1(b_bus[26]),
        .O(\bb_reg[27]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair72" *) 
  LUT4 #(
    .INIT(16'h88B8)) 
    \bb_reg[27]_i_3 
       (.I0(opcode[15]),
        .I1(\bb_reg[31]_i_6_n_0 ),
        .I2(reg_target[27]),
        .I3(b_source),
        .O(b_bus[27]));
  LUT6 #(
    .INIT(64'hBF40FFFFBF400000)) 
    \bb_reg[28]_i_1 
       (.I0(\bb_reg[28]_i_2_n_0 ),
        .I1(b_bus[31]),
        .I2(\bb_reg[31]_i_3_n_0 ),
        .I3(b_bus[28]),
        .I4(\count_reg_reg[5] ),
        .I5(\bb_reg_reg[31]_5 [28]),
        .O(\bb_reg_reg[31]_4 [28]));
  (* SOFT_HLUTNM = "soft_lutpair90" *) 
  LUT3 #(
    .INIT(8'h04)) 
    \bb_reg[28]_i_2 
       (.I0(b_bus[26]),
        .I1(\bb_reg[26]_i_2_n_0 ),
        .I2(b_bus[27]),
        .O(\bb_reg[28]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair72" *) 
  LUT4 #(
    .INIT(16'h88B8)) 
    \bb_reg[28]_i_3 
       (.I0(opcode[15]),
        .I1(\bb_reg[31]_i_6_n_0 ),
        .I2(reg_target[28]),
        .I3(b_source),
        .O(b_bus[28]));
  LUT6 #(
    .INIT(64'hBF40FFFFBF400000)) 
    \bb_reg[29]_i_1 
       (.I0(\bb_reg[29]_i_2_n_0 ),
        .I1(b_bus[31]),
        .I2(\bb_reg[31]_i_3_n_0 ),
        .I3(b_bus[29]),
        .I4(\count_reg_reg[5] ),
        .I5(\bb_reg_reg[31]_5 [29]),
        .O(\bb_reg_reg[31]_4 [29]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT4 #(
    .INIT(16'h0004)) 
    \bb_reg[29]_i_2 
       (.I0(b_bus[27]),
        .I1(\bb_reg[26]_i_2_n_0 ),
        .I2(b_bus[26]),
        .I3(b_bus[28]),
        .O(\bb_reg[29]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair73" *) 
  LUT4 #(
    .INIT(16'h88B8)) 
    \bb_reg[29]_i_3 
       (.I0(opcode[15]),
        .I1(\bb_reg[31]_i_6_n_0 ),
        .I2(reg_target[29]),
        .I3(b_source),
        .O(b_bus[29]));
  LUT6 #(
    .INIT(64'hBF40FFFFBF400000)) 
    \bb_reg[2]_i_1 
       (.I0(\bb_reg[2]_i_2_n_0 ),
        .I1(b_bus[31]),
        .I2(\bb_reg[31]_i_3_n_0 ),
        .I3(b_bus[2]),
        .I4(\count_reg_reg[5] ),
        .I5(\bb_reg_reg[31]_5 [2]),
        .O(\bb_reg_reg[31]_4 [2]));
  (* SOFT_HLUTNM = "soft_lutpair98" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \bb_reg[2]_i_2 
       (.I0(b_bus[0]),
        .I1(b_bus[1]),
        .O(\bb_reg[2]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB8F3B8C0)) 
    \bb_reg[2]_i_3 
       (.I0(opcode[0]),
        .I1(b_source),
        .I2(opcode[2]),
        .I3(\bb_reg[31]_i_6_n_0 ),
        .I4(reg_target[2]),
        .O(b_bus[2]));
  LUT6 #(
    .INIT(64'hBF40FFFFBF400000)) 
    \bb_reg[30]_i_1 
       (.I0(\bb_reg[31]_i_5_n_0 ),
        .I1(b_bus[31]),
        .I2(\bb_reg[31]_i_3_n_0 ),
        .I3(b_bus[30]),
        .I4(\count_reg_reg[5] ),
        .I5(\bb_reg_reg[31]_5 [30]),
        .O(\bb_reg_reg[31]_4 [30]));
  LUT5 #(
    .INIT(32'h08880808)) 
    \bb_reg[31]_i_1 
       (.I0(\count_reg_reg[5] ),
        .I1(b_bus[31]),
        .I2(\bb_reg[31]_i_3_n_0 ),
        .I3(b_bus[30]),
        .I4(\bb_reg[31]_i_5_n_0 ),
        .O(\bb_reg_reg[31]_4 [31]));
  LUT4 #(
    .INIT(16'h88B8)) 
    \bb_reg[31]_i_2 
       (.I0(opcode[15]),
        .I1(\bb_reg[31]_i_6_n_0 ),
        .I2(reg_target[31]),
        .I3(b_source),
        .O(b_bus[31]));
  LUT4 #(
    .INIT(16'h0614)) 
    \bb_reg[31]_i_3 
       (.I0(\lower_reg_reg[31]_0 ),
        .I1(\lower_reg_reg[31]_2 ),
        .I2(\lower_reg_reg[31]_1 ),
        .I3(\lower_reg_reg[31] ),
        .O(\bb_reg[31]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair73" *) 
  LUT4 #(
    .INIT(16'h88B8)) 
    \bb_reg[31]_i_4 
       (.I0(opcode[15]),
        .I1(\bb_reg[31]_i_6_n_0 ),
        .I2(reg_target[30]),
        .I3(b_source),
        .O(b_bus[30]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT5 #(
    .INIT(32'h00000010)) 
    \bb_reg[31]_i_5 
       (.I0(b_bus[28]),
        .I1(b_bus[26]),
        .I2(\bb_reg[26]_i_2_n_0 ),
        .I3(b_bus[27]),
        .I4(b_bus[29]),
        .O(\bb_reg[31]_i_5_n_0 ));
  LUT3 #(
    .INIT(8'h02)) 
    \bb_reg[31]_i_6 
       (.I0(\bb_reg[31]_i_8_n_0 ),
        .I1(\u3_control/is_syscall__2 ),
        .I2(opcode[30]),
        .O(\bb_reg[31]_i_6_n_0 ));
  LUT4 #(
    .INIT(16'h0100)) 
    \bb_reg[31]_i_7 
       (.I0(opcode[30]),
        .I1(\u3_control/is_syscall__2 ),
        .I2(opcode[31]),
        .I3(\bb_reg[31]_i_9_n_0 ),
        .O(b_source));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT5 #(
    .INIT(32'h555566FE)) 
    \bb_reg[31]_i_8 
       (.I0(opcode[28]),
        .I1(opcode[31]),
        .I2(opcode[26]),
        .I3(opcode[27]),
        .I4(opcode[29]),
        .O(\bb_reg[31]_i_8_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair77" *) 
  LUT4 #(
    .INIT(16'h70F2)) 
    \bb_reg[31]_i_9 
       (.I0(opcode[26]),
        .I1(opcode[29]),
        .I2(opcode[28]),
        .I3(opcode[27]),
        .O(\bb_reg[31]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hBF40FFFFBF400000)) 
    \bb_reg[3]_i_1 
       (.I0(\bb_reg[3]_i_2_n_0 ),
        .I1(b_bus[31]),
        .I2(\bb_reg[31]_i_3_n_0 ),
        .I3(b_bus[3]),
        .I4(\count_reg_reg[5] ),
        .I5(\bb_reg_reg[31]_5 [3]),
        .O(\bb_reg_reg[31]_4 [3]));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT3 #(
    .INIT(8'h01)) 
    \bb_reg[3]_i_2 
       (.I0(b_bus[1]),
        .I1(b_bus[0]),
        .I2(b_bus[2]),
        .O(\bb_reg[3]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB8F3B8C0)) 
    \bb_reg[3]_i_3 
       (.I0(opcode[1]),
        .I1(b_source),
        .I2(opcode[3]),
        .I3(\bb_reg[31]_i_6_n_0 ),
        .I4(reg_target[3]),
        .O(b_bus[3]));
  LUT6 #(
    .INIT(64'hBF40FFFFBF400000)) 
    \bb_reg[4]_i_1 
       (.I0(\bb_reg[4]_i_2_n_0 ),
        .I1(b_bus[31]),
        .I2(\bb_reg[31]_i_3_n_0 ),
        .I3(b_bus[4]),
        .I4(\count_reg_reg[5] ),
        .I5(\bb_reg_reg[31]_5 [4]),
        .O(\bb_reg_reg[31]_4 [4]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    \bb_reg[4]_i_2 
       (.I0(b_bus[2]),
        .I1(b_bus[0]),
        .I2(b_bus[1]),
        .I3(b_bus[3]),
        .O(\bb_reg[4]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB8F3B8C0)) 
    \bb_reg[4]_i_3 
       (.I0(opcode[2]),
        .I1(b_source),
        .I2(opcode[4]),
        .I3(\bb_reg[31]_i_6_n_0 ),
        .I4(reg_target[4]),
        .O(b_bus[4]));
  LUT6 #(
    .INIT(64'hBF40FFFFBF400000)) 
    \bb_reg[5]_i_1 
       (.I0(\bb_reg[5]_i_2_n_0 ),
        .I1(b_bus[31]),
        .I2(\bb_reg[31]_i_3_n_0 ),
        .I3(b_bus[5]),
        .I4(\count_reg_reg[5] ),
        .I5(\bb_reg_reg[31]_5 [5]),
        .O(\bb_reg_reg[31]_4 [5]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT5 #(
    .INIT(32'h00000001)) 
    \bb_reg[5]_i_2 
       (.I0(b_bus[3]),
        .I1(b_bus[1]),
        .I2(b_bus[0]),
        .I3(b_bus[2]),
        .I4(b_bus[4]),
        .O(\bb_reg[5]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB8F3B8C0)) 
    \bb_reg[5]_i_3 
       (.I0(opcode[3]),
        .I1(b_source),
        .I2(opcode[5]),
        .I3(\bb_reg[31]_i_6_n_0 ),
        .I4(reg_target[5]),
        .O(b_bus[5]));
  LUT6 #(
    .INIT(64'hBF40FFFFBF400000)) 
    \bb_reg[6]_i_1 
       (.I0(\bb_reg[6]_i_2_n_0 ),
        .I1(b_bus[31]),
        .I2(\bb_reg[31]_i_3_n_0 ),
        .I3(b_bus[6]),
        .I4(\count_reg_reg[5] ),
        .I5(\bb_reg_reg[31]_5 [6]),
        .O(\bb_reg_reg[31]_4 [6]));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \bb_reg[6]_i_2 
       (.I0(b_bus[4]),
        .I1(b_bus[2]),
        .I2(b_bus[0]),
        .I3(b_bus[1]),
        .I4(b_bus[3]),
        .I5(b_bus[5]),
        .O(\bb_reg[6]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB8F3B8C0)) 
    \bb_reg[6]_i_3 
       (.I0(opcode[4]),
        .I1(b_source),
        .I2(opcode[6]),
        .I3(\bb_reg[31]_i_6_n_0 ),
        .I4(reg_target[6]),
        .O(b_bus[6]));
  LUT6 #(
    .INIT(64'hBF40FFFFBF400000)) 
    \bb_reg[7]_i_1 
       (.I0(\bb_reg[7]_i_2_n_0 ),
        .I1(b_bus[31]),
        .I2(\bb_reg[31]_i_3_n_0 ),
        .I3(b_bus[7]),
        .I4(\count_reg_reg[5] ),
        .I5(\bb_reg_reg[31]_5 [7]),
        .O(\bb_reg_reg[31]_4 [7]));
  (* SOFT_HLUTNM = "soft_lutpair132" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \bb_reg[7]_i_2 
       (.I0(\bb_reg[6]_i_2_n_0 ),
        .I1(b_bus[6]),
        .O(\bb_reg[7]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB8F3B8C0)) 
    \bb_reg[7]_i_3 
       (.I0(opcode[5]),
        .I1(b_source),
        .I2(opcode[7]),
        .I3(\bb_reg[31]_i_6_n_0 ),
        .I4(reg_target[7]),
        .O(b_bus[7]));
  LUT6 #(
    .INIT(64'hBF40FFFFBF400000)) 
    \bb_reg[8]_i_1 
       (.I0(\bb_reg[8]_i_2_n_0 ),
        .I1(b_bus[31]),
        .I2(\bb_reg[31]_i_3_n_0 ),
        .I3(b_bus[8]),
        .I4(\count_reg_reg[5] ),
        .I5(\bb_reg_reg[31]_5 [8]),
        .O(\bb_reg_reg[31]_4 [8]));
  (* SOFT_HLUTNM = "soft_lutpair132" *) 
  LUT3 #(
    .INIT(8'h04)) 
    \bb_reg[8]_i_2 
       (.I0(b_bus[6]),
        .I1(\bb_reg[6]_i_2_n_0 ),
        .I2(b_bus[7]),
        .O(\bb_reg[8]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB8F3B8C0)) 
    \bb_reg[8]_i_3 
       (.I0(opcode[6]),
        .I1(b_source),
        .I2(opcode[8]),
        .I3(\bb_reg[31]_i_6_n_0 ),
        .I4(reg_target[8]),
        .O(b_bus[8]));
  LUT6 #(
    .INIT(64'hBF40FFFFBF400000)) 
    \bb_reg[9]_i_1 
       (.I0(\bb_reg[9]_i_2_n_0 ),
        .I1(b_bus[31]),
        .I2(\bb_reg[31]_i_3_n_0 ),
        .I3(b_bus[9]),
        .I4(\count_reg_reg[5] ),
        .I5(\bb_reg_reg[31]_5 [9]),
        .O(\bb_reg_reg[31]_4 [9]));
  LUT2 #(
    .INIT(4'h2)) 
    \bb_reg[9]_i_2 
       (.I0(\bb_reg[8]_i_2_n_0 ),
        .I1(b_bus[8]),
        .O(\bb_reg[9]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB8F3B8C0)) 
    \bb_reg[9]_i_3 
       (.I0(opcode[7]),
        .I1(b_source),
        .I2(opcode[9]),
        .I3(\bb_reg[31]_i_6_n_0 ),
        .I4(reg_target[9]),
        .O(b_bus[9]));
  (* SOFT_HLUTNM = "soft_lutpair78" *) 
  LUT4 #(
    .INIT(16'h0004)) 
    bram_bbt_annotations_en_reg_i_1
       (.I0(complete_address_next[30]),
        .I1(complete_address_next[28]),
        .I2(complete_address_next[29]),
        .I3(complete_address_next[31]),
        .O(\bram_bbt_annotations_address[2] ));
  (* SOFT_HLUTNM = "soft_lutpair80" *) 
  LUT4 #(
    .INIT(16'h0400)) 
    \bram_debug_address_reg[19]_i_1 
       (.I0(complete_address_next[30]),
        .I1(complete_address_next[28]),
        .I2(complete_address_next[29]),
        .I3(complete_address_next[31]),
        .O(\bram_debug_bytes_selection[3] ));
  (* SOFT_HLUTNM = "soft_lutpair136" *) 
  LUT2 #(
    .INIT(4'h8)) 
    bram_debug_en_reg_i_1
       (.I0(complete_address_next[28]),
        .I1(complete_address_next[31]),
        .O(bram_debug_en));
  LUT5 #(
    .INIT(32'hF4F1B0E0)) 
    \bram_firmware_code_address_reg[10]_i_1 
       (.I0(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I1(mem_state_reg_reg_n_0),
        .I2(pc_future[10]),
        .I3(pause_in0_out),
        .I4(pc_new[10]),
        .O(\bram_sec_policy_config_address[19] [8]));
  LUT6 #(
    .INIT(64'h0000000033E200E2)) 
    \bram_firmware_code_address_reg[10]_i_10 
       (.I0(b_bus[2]),
        .I1(a_bus[0]),
        .I2(b_bus[1]),
        .I3(a_bus[1]),
        .I4(b_bus[0]),
        .I5(a_bus[2]),
        .O(\bram_firmware_code_address_reg[10]_i_10_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair110" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \bram_firmware_code_address_reg[10]_i_11 
       (.I0(shift2L[6]),
        .I1(a_bus[2]),
        .I2(shift2L[10]),
        .O(shift4L[10]));
  (* SOFT_HLUTNM = "soft_lutpair129" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \bram_firmware_code_address_reg[10]_i_12 
       (.I0(shift2R[14]),
        .I1(a_bus[2]),
        .I2(shift2R[10]),
        .O(shift4R[10]));
  (* SOFT_HLUTNM = "soft_lutpair122" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \bram_firmware_code_address_reg[10]_i_13 
       (.I0(shift2R[22]),
        .I1(a_bus[2]),
        .I2(shift2R[18]),
        .O(shift4R[18]));
  LUT5 #(
    .INIT(32'hFF00B8B8)) 
    \bram_firmware_code_address_reg[10]_i_14 
       (.I0(shift2R[30]),
        .I1(a_bus[2]),
        .I2(shift2R[26]),
        .I3(\bram_firmware_code_address_reg[19]_i_17_n_0 ),
        .I4(a_bus[3]),
        .O(shift8R[26]));
  LUT5 #(
    .INIT(32'h0F000802)) 
    \bram_firmware_code_address_reg[10]_i_16 
       (.I0(\pc_reg_reg[25]_1 ),
        .I1(\upper_reg_reg[8] ),
        .I2(\pc_reg_reg[30]_0 ),
        .I3(\upper_reg_reg[31]_0 [7]),
        .I4(\pc_reg_reg[25] ),
        .O(\bram_firmware_code_address_reg[10]_i_16_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \bram_firmware_code_address_reg[10]_i_17 
       (.I0(b_bus[13]),
        .I1(b_bus[12]),
        .I2(a_bus[1]),
        .I3(b_bus[11]),
        .I4(a_bus[0]),
        .I5(b_bus[10]),
        .O(shift2R[10]));
  LUT4 #(
    .INIT(16'hAAB8)) 
    \bram_firmware_code_address_reg[10]_i_2 
       (.I0(pc_current[10]),
        .I1(pause_in),
        .I2(\pc_reg_reg[10] ),
        .I3(\address_reg_reg[24]_0 ),
        .O(pc_future[10]));
  LUT6 #(
    .INIT(64'hFFFFFFFFAEFFAEAA)) 
    \bram_firmware_code_address_reg[10]_i_3 
       (.I0(c_alu[10]),
        .I1(shift8L[10]),
        .I2(a_bus[4]),
        .I3(\bram_firmware_code_address_reg[15]_i_7_n_0 ),
        .I4(\bram_firmware_code_address_reg[10]_i_6_n_0 ),
        .I5(c_mult[10]),
        .O(pc_new[10]));
  MUXF7 \bram_firmware_code_address_reg[10]_i_4 
       (.I0(\bram_firmware_code_address_reg[10]_i_8_n_0 ),
        .I1(\u6_alu/bv_adder028_out ),
        .O(c_alu[10]),
        .S(\u6_alu/c_alu1127_out ));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \bram_firmware_code_address_reg[10]_i_5 
       (.I0(\bram_firmware_code_address_reg[10]_i_10_n_0 ),
        .I1(a_bus[3]),
        .I2(shift4L[10]),
        .O(shift8L[10]));
  LUT6 #(
    .INIT(64'hAAAAA8080000A808)) 
    \bram_firmware_code_address_reg[10]_i_6 
       (.I0(\bram_firmware_code_address_reg[19]_i_14_n_0 ),
        .I1(shift4R[10]),
        .I2(a_bus[3]),
        .I3(shift4R[18]),
        .I4(a_bus[4]),
        .I5(shift8R[26]),
        .O(\bram_firmware_code_address_reg[10]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF8000000F400)) 
    \bram_firmware_code_address_reg[10]_i_7 
       (.I0(\lower_reg_reg[8] ),
        .I1(\pc_reg_reg[30]_0 ),
        .I2(\bram_firmware_code_address_reg[10]_i_16_n_0 ),
        .I3(\pc_reg_reg[30]_1 ),
        .I4(\bram_firmware_code_address_reg[19]_i_10_n_0 ),
        .I5(\lower_reg_reg[31]_5 [10]),
        .O(c_mult[10]));
  LUT6 #(
    .INIT(64'h0000000011050540)) 
    \bram_firmware_code_address_reg[10]_i_8 
       (.I0(\bram_firmware_code_address_reg[17]_i_9_n_0 ),
        .I1(\bram_firmware_code_address_reg[17]_i_10_n_0 ),
        .I2(\bram_firmware_code_address_reg[17]_i_11_n_0 ),
        .I3(a_bus[10]),
        .I4(b_bus[10]),
        .I5(\u6_alu/c_alu1__0 ),
        .O(\bram_firmware_code_address_reg[10]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'h9969966696996669)) 
    \bram_firmware_code_address_reg[10]_i_9 
       (.I0(b_bus[10]),
        .I1(a_bus[10]),
        .I2(\u6_alu/bv_adder47__3 ),
        .I3(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I4(b_bus[9]),
        .I5(a_bus[9]),
        .O(\u6_alu/bv_adder028_out ));
  LUT5 #(
    .INIT(32'hF4F1B0E0)) 
    \bram_firmware_code_address_reg[11]_i_1 
       (.I0(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I1(mem_state_reg_reg_n_0),
        .I2(pc_future[11]),
        .I3(pause_in0_out),
        .I4(pc_new[11]),
        .O(\bram_sec_policy_config_address[19] [9]));
  (* SOFT_HLUTNM = "soft_lutpair111" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \bram_firmware_code_address_reg[11]_i_10 
       (.I0(shift2L[7]),
        .I1(a_bus[2]),
        .I2(shift2L[11]),
        .O(shift4L[11]));
  (* SOFT_HLUTNM = "soft_lutpair126" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \bram_firmware_code_address_reg[11]_i_11 
       (.I0(shift2R[15]),
        .I1(a_bus[2]),
        .I2(shift2R[11]),
        .O(shift4R[11]));
  (* SOFT_HLUTNM = "soft_lutpair119" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \bram_firmware_code_address_reg[11]_i_12 
       (.I0(shift2R[23]),
        .I1(a_bus[2]),
        .I2(shift2R[19]),
        .O(shift4R[19]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \bram_firmware_code_address_reg[11]_i_13 
       (.I0(\bram_firmware_code_address_reg[19]_i_17_n_0 ),
        .I1(a_bus[3]),
        .I2(shift4R[27]),
        .O(shift8R[27]));
  LUT5 #(
    .INIT(32'h0F000802)) 
    \bram_firmware_code_address_reg[11]_i_15 
       (.I0(\pc_reg_reg[25]_1 ),
        .I1(\upper_reg_reg[9] ),
        .I2(\pc_reg_reg[30]_0 ),
        .I3(\upper_reg_reg[31]_0 [8]),
        .I4(\pc_reg_reg[25] ),
        .O(\bram_firmware_code_address_reg[11]_i_15_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \bram_firmware_code_address_reg[11]_i_16 
       (.I0(b_bus[14]),
        .I1(b_bus[13]),
        .I2(a_bus[1]),
        .I3(b_bus[12]),
        .I4(a_bus[0]),
        .I5(b_bus[11]),
        .O(shift2R[11]));
  LUT4 #(
    .INIT(16'hAAB8)) 
    \bram_firmware_code_address_reg[11]_i_2 
       (.I0(pc_current[11]),
        .I1(pause_in),
        .I2(\pc_reg_reg[11] ),
        .I3(\address_reg_reg[24]_0 ),
        .O(pc_future[11]));
  LUT6 #(
    .INIT(64'hFFFFFFFFAEFFAEAA)) 
    \bram_firmware_code_address_reg[11]_i_3 
       (.I0(c_alu[11]),
        .I1(shift8L[11]),
        .I2(a_bus[4]),
        .I3(\bram_firmware_code_address_reg[15]_i_7_n_0 ),
        .I4(\bram_firmware_code_address_reg[11]_i_6_n_0 ),
        .I5(c_mult[11]),
        .O(pc_new[11]));
  LUT6 #(
    .INIT(64'h9669FFFF96690000)) 
    \bram_firmware_code_address_reg[11]_i_4 
       (.I0(b_bus[11]),
        .I1(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I2(a_bus[11]),
        .I3(\u6_alu/bv_adder43__3 ),
        .I4(\u6_alu/c_alu1127_out ),
        .I5(\bram_firmware_code_address_reg[11]_i_9_n_0 ),
        .O(c_alu[11]));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT4 #(
    .INIT(16'h2F20)) 
    \bram_firmware_code_address_reg[11]_i_5 
       (.I0(shift2L[3]),
        .I1(a_bus[2]),
        .I2(a_bus[3]),
        .I3(shift4L[11]),
        .O(shift8L[11]));
  LUT6 #(
    .INIT(64'hAAAAA8080000A808)) 
    \bram_firmware_code_address_reg[11]_i_6 
       (.I0(\bram_firmware_code_address_reg[19]_i_14_n_0 ),
        .I1(shift4R[11]),
        .I2(a_bus[3]),
        .I3(shift4R[19]),
        .I4(a_bus[4]),
        .I5(shift8R[27]),
        .O(\bram_firmware_code_address_reg[11]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF8000000F400)) 
    \bram_firmware_code_address_reg[11]_i_7 
       (.I0(\lower_reg_reg[9] ),
        .I1(\pc_reg_reg[30]_0 ),
        .I2(\bram_firmware_code_address_reg[11]_i_15_n_0 ),
        .I3(\pc_reg_reg[30]_1 ),
        .I4(\bram_firmware_code_address_reg[19]_i_10_n_0 ),
        .I5(\lower_reg_reg[31]_5 [11]),
        .O(c_mult[11]));
  LUT6 #(
    .INIT(64'hFFB2E8FFE80000B2)) 
    \bram_firmware_code_address_reg[11]_i_8 
       (.I0(\u6_alu/bv_adder47__3 ),
        .I1(b_bus[9]),
        .I2(a_bus[9]),
        .I3(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I4(b_bus[10]),
        .I5(a_bus[10]),
        .O(\u6_alu/bv_adder43__3 ));
  LUT6 #(
    .INIT(64'h0000000011050540)) 
    \bram_firmware_code_address_reg[11]_i_9 
       (.I0(\bram_firmware_code_address_reg[17]_i_9_n_0 ),
        .I1(\bram_firmware_code_address_reg[17]_i_10_n_0 ),
        .I2(\bram_firmware_code_address_reg[17]_i_11_n_0 ),
        .I3(a_bus[11]),
        .I4(b_bus[11]),
        .I5(\u6_alu/c_alu1__0 ),
        .O(\bram_firmware_code_address_reg[11]_i_9_n_0 ));
  LUT5 #(
    .INIT(32'hF4F1B0E0)) 
    \bram_firmware_code_address_reg[12]_i_1 
       (.I0(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I1(mem_state_reg_reg_n_0),
        .I2(pc_future[12]),
        .I3(pause_in0_out),
        .I4(pc_new[12]),
        .O(\bram_sec_policy_config_address[19] [10]));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT5 #(
    .INIT(32'h04FF0400)) 
    \bram_firmware_code_address_reg[12]_i_10 
       (.I0(a_bus[0]),
        .I1(b_bus[0]),
        .I2(a_bus[1]),
        .I3(a_bus[2]),
        .I4(shift2L[4]),
        .O(shift4L[4]));
  (* SOFT_HLUTNM = "soft_lutpair108" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \bram_firmware_code_address_reg[12]_i_11 
       (.I0(shift2L[8]),
        .I1(a_bus[2]),
        .I2(shift2L[12]),
        .O(shift4L[12]));
  LUT3 #(
    .INIT(8'hB8)) 
    \bram_firmware_code_address_reg[12]_i_12 
       (.I0(shift2R[16]),
        .I1(a_bus[2]),
        .I2(shift2R[12]),
        .O(shift4R[12]));
  (* SOFT_HLUTNM = "soft_lutpair125" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \bram_firmware_code_address_reg[12]_i_13 
       (.I0(shift2R[24]),
        .I1(a_bus[2]),
        .I2(shift2R[20]),
        .O(shift4R[20]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT4 #(
    .INIT(16'hCDC8)) 
    \bram_firmware_code_address_reg[12]_i_14 
       (.I0(a_bus[3]),
        .I1(\bram_firmware_code_address_reg[19]_i_17_n_0 ),
        .I2(a_bus[2]),
        .I3(shift2R[28]),
        .O(shift8R[28]));
  LUT6 #(
    .INIT(64'h00FF00000020008A)) 
    \bram_firmware_code_address_reg[12]_i_16 
       (.I0(\pc_reg_reg[25]_1 ),
        .I1(\upper_reg_reg[31]_0 [8]),
        .I2(\upper_reg_reg[9] ),
        .I3(\pc_reg_reg[30]_0 ),
        .I4(\upper_reg_reg[31]_0 [9]),
        .I5(\pc_reg_reg[25] ),
        .O(\bram_firmware_code_address_reg[12]_i_16_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \bram_firmware_code_address_reg[12]_i_17 
       (.I0(b_bus[1]),
        .I1(b_bus[2]),
        .I2(a_bus[1]),
        .I3(b_bus[3]),
        .I4(a_bus[0]),
        .I5(b_bus[4]),
        .O(shift2L[4]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \bram_firmware_code_address_reg[12]_i_18 
       (.I0(b_bus[5]),
        .I1(b_bus[6]),
        .I2(a_bus[1]),
        .I3(b_bus[7]),
        .I4(a_bus[0]),
        .I5(b_bus[8]),
        .O(shift2L[8]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \bram_firmware_code_address_reg[12]_i_19 
       (.I0(b_bus[9]),
        .I1(b_bus[10]),
        .I2(a_bus[1]),
        .I3(b_bus[11]),
        .I4(a_bus[0]),
        .I5(b_bus[12]),
        .O(shift2L[12]));
  LUT4 #(
    .INIT(16'hAAB8)) 
    \bram_firmware_code_address_reg[12]_i_2 
       (.I0(pc_current[12]),
        .I1(pause_in),
        .I2(\pc_reg_reg[12] ),
        .I3(\address_reg_reg[24]_0 ),
        .O(pc_future[12]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \bram_firmware_code_address_reg[12]_i_20 
       (.I0(b_bus[19]),
        .I1(b_bus[18]),
        .I2(a_bus[1]),
        .I3(b_bus[17]),
        .I4(a_bus[0]),
        .I5(b_bus[16]),
        .O(shift2R[16]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \bram_firmware_code_address_reg[12]_i_21 
       (.I0(b_bus[15]),
        .I1(b_bus[14]),
        .I2(a_bus[1]),
        .I3(b_bus[13]),
        .I4(a_bus[0]),
        .I5(b_bus[12]),
        .O(shift2R[12]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \bram_firmware_code_address_reg[12]_i_22 
       (.I0(b_bus[23]),
        .I1(b_bus[22]),
        .I2(a_bus[1]),
        .I3(b_bus[21]),
        .I4(a_bus[0]),
        .I5(b_bus[20]),
        .O(shift2R[20]));
  LUT6 #(
    .INIT(64'hFFFFFFFFAEFFAEAA)) 
    \bram_firmware_code_address_reg[12]_i_3 
       (.I0(c_alu[12]),
        .I1(shift8L[12]),
        .I2(a_bus[4]),
        .I3(\bram_firmware_code_address_reg[15]_i_7_n_0 ),
        .I4(\bram_firmware_code_address_reg[12]_i_6_n_0 ),
        .I5(c_mult[12]),
        .O(pc_new[12]));
  MUXF7 \bram_firmware_code_address_reg[12]_i_4 
       (.I0(\bram_firmware_code_address_reg[12]_i_8_n_0 ),
        .I1(\u6_alu/bv_adder034_out ),
        .O(c_alu[12]),
        .S(\u6_alu/c_alu1127_out ));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \bram_firmware_code_address_reg[12]_i_5 
       (.I0(shift4L[4]),
        .I1(a_bus[3]),
        .I2(shift4L[12]),
        .O(shift8L[12]));
  LUT6 #(
    .INIT(64'hAAAAA8080000A808)) 
    \bram_firmware_code_address_reg[12]_i_6 
       (.I0(\bram_firmware_code_address_reg[19]_i_14_n_0 ),
        .I1(shift4R[12]),
        .I2(a_bus[3]),
        .I3(shift4R[20]),
        .I4(a_bus[4]),
        .I5(shift8R[28]),
        .O(\bram_firmware_code_address_reg[12]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF8000000F400)) 
    \bram_firmware_code_address_reg[12]_i_7 
       (.I0(\lower_reg_reg[11]_0 ),
        .I1(\pc_reg_reg[30]_0 ),
        .I2(\bram_firmware_code_address_reg[12]_i_16_n_0 ),
        .I3(\pc_reg_reg[30]_1 ),
        .I4(\bram_firmware_code_address_reg[19]_i_10_n_0 ),
        .I5(\lower_reg_reg[31]_5 [12]),
        .O(c_mult[12]));
  LUT6 #(
    .INIT(64'h0000000011050540)) 
    \bram_firmware_code_address_reg[12]_i_8 
       (.I0(\bram_firmware_code_address_reg[17]_i_9_n_0 ),
        .I1(\bram_firmware_code_address_reg[17]_i_10_n_0 ),
        .I2(\bram_firmware_code_address_reg[17]_i_11_n_0 ),
        .I3(a_bus[12]),
        .I4(b_bus[12]),
        .I5(\u6_alu/c_alu1__0 ),
        .O(\bram_firmware_code_address_reg[12]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'h9969966696996669)) 
    \bram_firmware_code_address_reg[12]_i_9 
       (.I0(b_bus[12]),
        .I1(a_bus[12]),
        .I2(\u6_alu/bv_adder43__3 ),
        .I3(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I4(b_bus[11]),
        .I5(a_bus[11]),
        .O(\u6_alu/bv_adder034_out ));
  LUT5 #(
    .INIT(32'hF4F1B0E0)) 
    \bram_firmware_code_address_reg[13]_i_1 
       (.I0(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I1(mem_state_reg_reg_n_0),
        .I2(pc_future[13]),
        .I3(pause_in0_out),
        .I4(pc_new[13]),
        .O(\bram_sec_policy_config_address[19] [11]));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \bram_firmware_code_address_reg[13]_i_10 
       (.I0(b_bus[1]),
        .I1(a_bus[0]),
        .I2(b_bus[0]),
        .I3(a_bus[1]),
        .I4(a_bus[2]),
        .I5(shift2L[5]),
        .O(shift4L[5]));
  (* SOFT_HLUTNM = "soft_lutpair109" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \bram_firmware_code_address_reg[13]_i_11 
       (.I0(shift2L[9]),
        .I1(a_bus[2]),
        .I2(shift2L[13]),
        .O(shift4L[13]));
  (* SOFT_HLUTNM = "soft_lutpair130" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \bram_firmware_code_address_reg[13]_i_12 
       (.I0(shift2R[17]),
        .I1(a_bus[2]),
        .I2(shift2R[13]),
        .O(shift4R[13]));
  (* SOFT_HLUTNM = "soft_lutpair124" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \bram_firmware_code_address_reg[13]_i_13 
       (.I0(shift2R[25]),
        .I1(a_bus[2]),
        .I2(shift2R[21]),
        .O(shift4R[21]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT4 #(
    .INIT(16'hCDC8)) 
    \bram_firmware_code_address_reg[13]_i_14 
       (.I0(a_bus[3]),
        .I1(\bram_firmware_code_address_reg[19]_i_17_n_0 ),
        .I2(a_bus[2]),
        .I3(shift2R[29]),
        .O(shift8R[29]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT5 #(
    .INIT(32'h0F000802)) 
    \bram_firmware_code_address_reg[13]_i_16 
       (.I0(\pc_reg_reg[25]_1 ),
        .I1(\upper_reg_reg[11] ),
        .I2(\pc_reg_reg[30]_0 ),
        .I3(\upper_reg_reg[31]_0 [10]),
        .I4(\pc_reg_reg[25] ),
        .O(\bram_firmware_code_address_reg[13]_i_16_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \bram_firmware_code_address_reg[13]_i_17 
       (.I0(b_bus[2]),
        .I1(b_bus[3]),
        .I2(a_bus[1]),
        .I3(b_bus[4]),
        .I4(a_bus[0]),
        .I5(b_bus[5]),
        .O(shift2L[5]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \bram_firmware_code_address_reg[13]_i_18 
       (.I0(b_bus[6]),
        .I1(b_bus[7]),
        .I2(a_bus[1]),
        .I3(b_bus[8]),
        .I4(a_bus[0]),
        .I5(b_bus[9]),
        .O(shift2L[9]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \bram_firmware_code_address_reg[13]_i_19 
       (.I0(b_bus[10]),
        .I1(b_bus[11]),
        .I2(a_bus[1]),
        .I3(b_bus[12]),
        .I4(a_bus[0]),
        .I5(b_bus[13]),
        .O(shift2L[13]));
  LUT4 #(
    .INIT(16'hAAB8)) 
    \bram_firmware_code_address_reg[13]_i_2 
       (.I0(pc_current[13]),
        .I1(pause_in),
        .I2(\pc_reg_reg[13] ),
        .I3(\address_reg_reg[24]_0 ),
        .O(pc_future[13]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \bram_firmware_code_address_reg[13]_i_20 
       (.I0(b_bus[20]),
        .I1(b_bus[19]),
        .I2(a_bus[1]),
        .I3(b_bus[18]),
        .I4(a_bus[0]),
        .I5(b_bus[17]),
        .O(shift2R[17]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \bram_firmware_code_address_reg[13]_i_21 
       (.I0(b_bus[16]),
        .I1(b_bus[15]),
        .I2(a_bus[1]),
        .I3(b_bus[14]),
        .I4(a_bus[0]),
        .I5(b_bus[13]),
        .O(shift2R[13]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \bram_firmware_code_address_reg[13]_i_22 
       (.I0(b_bus[24]),
        .I1(b_bus[23]),
        .I2(a_bus[1]),
        .I3(b_bus[22]),
        .I4(a_bus[0]),
        .I5(b_bus[21]),
        .O(shift2R[21]));
  LUT6 #(
    .INIT(64'hFFFFFFFFAEFFAEAA)) 
    \bram_firmware_code_address_reg[13]_i_3 
       (.I0(c_alu[13]),
        .I1(shift8L[13]),
        .I2(a_bus[4]),
        .I3(\bram_firmware_code_address_reg[15]_i_7_n_0 ),
        .I4(\bram_firmware_code_address_reg[13]_i_6_n_0 ),
        .I5(c_mult[13]),
        .O(pc_new[13]));
  MUXF7 \bram_firmware_code_address_reg[13]_i_4 
       (.I0(\bram_firmware_code_address_reg[13]_i_8_n_0 ),
        .I1(\u6_alu/bv_adder037_out ),
        .O(c_alu[13]),
        .S(\u6_alu/c_alu1127_out ));
  (* SOFT_HLUTNM = "soft_lutpair89" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \bram_firmware_code_address_reg[13]_i_5 
       (.I0(shift4L[5]),
        .I1(a_bus[3]),
        .I2(shift4L[13]),
        .O(shift8L[13]));
  LUT6 #(
    .INIT(64'hAAAAA8080000A808)) 
    \bram_firmware_code_address_reg[13]_i_6 
       (.I0(\bram_firmware_code_address_reg[19]_i_14_n_0 ),
        .I1(shift4R[13]),
        .I2(a_bus[3]),
        .I3(shift4R[21]),
        .I4(a_bus[4]),
        .I5(shift8R[29]),
        .O(\bram_firmware_code_address_reg[13]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF8000000F400)) 
    \bram_firmware_code_address_reg[13]_i_7 
       (.I0(\lower_reg_reg[11] ),
        .I1(\pc_reg_reg[30]_0 ),
        .I2(\bram_firmware_code_address_reg[13]_i_16_n_0 ),
        .I3(\pc_reg_reg[30]_1 ),
        .I4(\bram_firmware_code_address_reg[19]_i_10_n_0 ),
        .I5(\lower_reg_reg[31]_5 [13]),
        .O(c_mult[13]));
  LUT6 #(
    .INIT(64'h0000000011050540)) 
    \bram_firmware_code_address_reg[13]_i_8 
       (.I0(\bram_firmware_code_address_reg[17]_i_9_n_0 ),
        .I1(\bram_firmware_code_address_reg[17]_i_10_n_0 ),
        .I2(\bram_firmware_code_address_reg[17]_i_11_n_0 ),
        .I3(a_bus[13]),
        .I4(b_bus[13]),
        .I5(\u6_alu/c_alu1__0 ),
        .O(\bram_firmware_code_address_reg[13]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'h9969966696996669)) 
    \bram_firmware_code_address_reg[13]_i_9 
       (.I0(b_bus[13]),
        .I1(a_bus[13]),
        .I2(\u6_alu/bv_adder41__3 ),
        .I3(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I4(b_bus[12]),
        .I5(a_bus[12]),
        .O(\u6_alu/bv_adder037_out ));
  LUT5 #(
    .INIT(32'hF4F1B0E0)) 
    \bram_firmware_code_address_reg[14]_i_1 
       (.I0(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I1(mem_state_reg_reg_n_0),
        .I2(pc_future[14]),
        .I3(pause_in0_out),
        .I4(pc_new[14]),
        .O(\bram_sec_policy_config_address[19] [12]));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \bram_firmware_code_address_reg[14]_i_10 
       (.I0(b_bus[0]),
        .I1(a_bus[1]),
        .I2(b_bus[1]),
        .I3(a_bus[0]),
        .I4(b_bus[2]),
        .O(shift2L[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \bram_firmware_code_address_reg[14]_i_11 
       (.I0(b_bus[3]),
        .I1(b_bus[4]),
        .I2(a_bus[1]),
        .I3(b_bus[5]),
        .I4(a_bus[0]),
        .I5(b_bus[6]),
        .O(shift2L[6]));
  (* SOFT_HLUTNM = "soft_lutpair97" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \bram_firmware_code_address_reg[14]_i_12 
       (.I0(shift2L[10]),
        .I1(a_bus[2]),
        .I2(shift2L[14]),
        .O(shift4L[14]));
  (* SOFT_HLUTNM = "soft_lutpair129" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \bram_firmware_code_address_reg[14]_i_13 
       (.I0(shift2R[18]),
        .I1(a_bus[2]),
        .I2(shift2R[14]),
        .O(shift4R[14]));
  (* SOFT_HLUTNM = "soft_lutpair122" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \bram_firmware_code_address_reg[14]_i_14 
       (.I0(shift2R[26]),
        .I1(a_bus[2]),
        .I2(shift2R[22]),
        .O(shift4R[22]));
  LUT5 #(
    .INIT(32'hF0F1F0E0)) 
    \bram_firmware_code_address_reg[14]_i_15 
       (.I0(a_bus[3]),
        .I1(a_bus[2]),
        .I2(\bram_firmware_code_address_reg[19]_i_17_n_0 ),
        .I3(a_bus[1]),
        .I4(shift1R),
        .O(shift8R[30]));
  LUT5 #(
    .INIT(32'h0F000802)) 
    \bram_firmware_code_address_reg[14]_i_17 
       (.I0(\pc_reg_reg[25]_1 ),
        .I1(\upper_reg_reg[12] ),
        .I2(\pc_reg_reg[30]_0 ),
        .I3(\upper_reg_reg[31]_0 [11]),
        .I4(\pc_reg_reg[25] ),
        .O(\bram_firmware_code_address_reg[14]_i_17_n_0 ));
  LUT6 #(
    .INIT(64'hFFB2E8FFE80000B2)) 
    \bram_firmware_code_address_reg[14]_i_18 
       (.I0(\u6_alu/bv_adder45__3 ),
        .I1(b_bus[10]),
        .I2(a_bus[10]),
        .I3(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I4(b_bus[11]),
        .I5(a_bus[11]),
        .O(\u6_alu/bv_adder41__3 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \bram_firmware_code_address_reg[14]_i_19 
       (.I0(b_bus[7]),
        .I1(b_bus[8]),
        .I2(a_bus[1]),
        .I3(b_bus[9]),
        .I4(a_bus[0]),
        .I5(b_bus[10]),
        .O(shift2L[10]));
  LUT4 #(
    .INIT(16'hAAB8)) 
    \bram_firmware_code_address_reg[14]_i_2 
       (.I0(pc_current[14]),
        .I1(pause_in),
        .I2(\pc_reg_reg[14]_0 ),
        .I3(\address_reg_reg[24]_0 ),
        .O(pc_future[14]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \bram_firmware_code_address_reg[14]_i_20 
       (.I0(b_bus[11]),
        .I1(b_bus[12]),
        .I2(a_bus[1]),
        .I3(b_bus[13]),
        .I4(a_bus[0]),
        .I5(b_bus[14]),
        .O(shift2L[14]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \bram_firmware_code_address_reg[14]_i_21 
       (.I0(b_bus[21]),
        .I1(b_bus[20]),
        .I2(a_bus[1]),
        .I3(b_bus[19]),
        .I4(a_bus[0]),
        .I5(b_bus[18]),
        .O(shift2R[18]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \bram_firmware_code_address_reg[14]_i_22 
       (.I0(b_bus[17]),
        .I1(b_bus[16]),
        .I2(a_bus[1]),
        .I3(b_bus[15]),
        .I4(a_bus[0]),
        .I5(b_bus[14]),
        .O(shift2R[14]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \bram_firmware_code_address_reg[14]_i_23 
       (.I0(b_bus[25]),
        .I1(b_bus[24]),
        .I2(a_bus[1]),
        .I3(b_bus[23]),
        .I4(a_bus[0]),
        .I5(b_bus[22]),
        .O(shift2R[22]));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \bram_firmware_code_address_reg[14]_i_24 
       (.I0(b_bus[31]),
        .I1(a_bus[0]),
        .I2(b_bus[30]),
        .O(shift1R));
  LUT6 #(
    .INIT(64'hFFB2E8FFE80000B2)) 
    \bram_firmware_code_address_reg[14]_i_26 
       (.I0(\u6_alu/bv_adder49__3 ),
        .I1(b_bus[8]),
        .I2(a_bus[8]),
        .I3(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I4(b_bus[9]),
        .I5(a_bus[9]),
        .O(\u6_alu/bv_adder45__3 ));
  LUT6 #(
    .INIT(64'hFFFFFEE0FEE00000)) 
    \bram_firmware_code_address_reg[14]_i_27 
       (.I0(bv_adder5414_out),
        .I1(bv_adder54__0),
        .I2(\bram_firmware_code_address_reg[14]_i_30_n_0 ),
        .I3(a_bus[6]),
        .I4(\bram_firmware_code_address_reg[14]_i_31_n_0 ),
        .I5(a_bus[7]),
        .O(\u6_alu/bv_adder49__3 ));
  LUT6 #(
    .INIT(64'hEEE8E88800000000)) 
    \bram_firmware_code_address_reg[14]_i_28 
       (.I0(a_bus[4]),
        .I1(\bram_firmware_code_address_reg[14]_i_32_n_0 ),
        .I2(a_bus[3]),
        .I3(\bram_firmware_code_address_reg[14]_i_33_n_0 ),
        .I4(\u6_alu/bv_adder59__3 ),
        .I5(bv_adder5518_out),
        .O(bv_adder5414_out));
  (* SOFT_HLUTNM = "soft_lutpair115" *) 
  LUT3 #(
    .INIT(8'h82)) 
    \bram_firmware_code_address_reg[14]_i_29 
       (.I0(a_bus[5]),
        .I1(b_bus[5]),
        .I2(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .O(bv_adder54__0));
  LUT6 #(
    .INIT(64'hFFFFFFFFAEFFAEAA)) 
    \bram_firmware_code_address_reg[14]_i_3 
       (.I0(c_alu[14]),
        .I1(shift8L[14]),
        .I2(a_bus[4]),
        .I3(\bram_firmware_code_address_reg[15]_i_7_n_0 ),
        .I4(\bram_firmware_code_address_reg[14]_i_6_n_0 ),
        .I5(c_mult[14]),
        .O(pc_new[14]));
  (* SOFT_HLUTNM = "soft_lutpair145" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \bram_firmware_code_address_reg[14]_i_30 
       (.I0(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I1(b_bus[6]),
        .O(\bram_firmware_code_address_reg[14]_i_30_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair146" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \bram_firmware_code_address_reg[14]_i_31 
       (.I0(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I1(b_bus[7]),
        .O(\bram_firmware_code_address_reg[14]_i_31_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair144" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \bram_firmware_code_address_reg[14]_i_32 
       (.I0(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I1(b_bus[4]),
        .O(\bram_firmware_code_address_reg[14]_i_32_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair123" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \bram_firmware_code_address_reg[14]_i_33 
       (.I0(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I1(b_bus[3]),
        .O(\bram_firmware_code_address_reg[14]_i_33_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair115" *) 
  LUT3 #(
    .INIT(8'hEB)) 
    \bram_firmware_code_address_reg[14]_i_34 
       (.I0(a_bus[5]),
        .I1(b_bus[5]),
        .I2(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .O(bv_adder5518_out));
  LUT6 #(
    .INIT(64'h9669FFFF96690000)) 
    \bram_firmware_code_address_reg[14]_i_4 
       (.I0(b_bus[14]),
        .I1(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I2(a_bus[14]),
        .I3(\u6_alu/bv_adder37__3 ),
        .I4(\u6_alu/c_alu1127_out ),
        .I5(\bram_firmware_code_address_reg[14]_i_9_n_0 ),
        .O(c_alu[14]));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \bram_firmware_code_address_reg[14]_i_5 
       (.I0(shift2L[2]),
        .I1(a_bus[2]),
        .I2(shift2L[6]),
        .I3(a_bus[3]),
        .I4(shift4L[14]),
        .O(shift8L[14]));
  LUT6 #(
    .INIT(64'hAAAAA8080000A808)) 
    \bram_firmware_code_address_reg[14]_i_6 
       (.I0(\bram_firmware_code_address_reg[19]_i_14_n_0 ),
        .I1(shift4R[14]),
        .I2(a_bus[3]),
        .I3(shift4R[22]),
        .I4(a_bus[4]),
        .I5(shift8R[30]),
        .O(\bram_firmware_code_address_reg[14]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF8000000F400)) 
    \bram_firmware_code_address_reg[14]_i_7 
       (.I0(\lower_reg_reg[12] ),
        .I1(\pc_reg_reg[30]_0 ),
        .I2(\bram_firmware_code_address_reg[14]_i_17_n_0 ),
        .I3(\pc_reg_reg[30]_1 ),
        .I4(\bram_firmware_code_address_reg[19]_i_10_n_0 ),
        .I5(\lower_reg_reg[31]_5 [14]),
        .O(c_mult[14]));
  LUT6 #(
    .INIT(64'hFFB2E8FFE80000B2)) 
    \bram_firmware_code_address_reg[14]_i_8 
       (.I0(\u6_alu/bv_adder41__3 ),
        .I1(b_bus[12]),
        .I2(a_bus[12]),
        .I3(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I4(b_bus[13]),
        .I5(a_bus[13]),
        .O(\u6_alu/bv_adder37__3 ));
  LUT6 #(
    .INIT(64'h0000000011050540)) 
    \bram_firmware_code_address_reg[14]_i_9 
       (.I0(\bram_firmware_code_address_reg[17]_i_9_n_0 ),
        .I1(\bram_firmware_code_address_reg[17]_i_10_n_0 ),
        .I2(\bram_firmware_code_address_reg[17]_i_11_n_0 ),
        .I3(a_bus[14]),
        .I4(b_bus[14]),
        .I5(\u6_alu/c_alu1__0 ),
        .O(\bram_firmware_code_address_reg[14]_i_9_n_0 ));
  LUT5 #(
    .INIT(32'hF4F1B0E0)) 
    \bram_firmware_code_address_reg[15]_i_1 
       (.I0(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I1(mem_state_reg_reg_n_0),
        .I2(pc_future[15]),
        .I3(pause_in0_out),
        .I4(pc_new[15]),
        .O(\bram_sec_policy_config_address[19] [13]));
  LUT6 #(
    .INIT(64'h0000000011050540)) 
    \bram_firmware_code_address_reg[15]_i_10 
       (.I0(\bram_firmware_code_address_reg[17]_i_9_n_0 ),
        .I1(\bram_firmware_code_address_reg[17]_i_10_n_0 ),
        .I2(\bram_firmware_code_address_reg[17]_i_11_n_0 ),
        .I3(a_bus[15]),
        .I4(b_bus[15]),
        .I5(\u6_alu/c_alu1__0 ),
        .O(\bram_firmware_code_address_reg[15]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'h9969966696996669)) 
    \bram_firmware_code_address_reg[15]_i_11 
       (.I0(b_bus[15]),
        .I1(a_bus[15]),
        .I2(\u6_alu/bv_adder37__3 ),
        .I3(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I4(b_bus[14]),
        .I5(a_bus[14]),
        .O(\u6_alu/bv_adder043_out ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \bram_firmware_code_address_reg[15]_i_12 
       (.I0(b_bus[0]),
        .I1(b_bus[1]),
        .I2(a_bus[1]),
        .I3(b_bus[2]),
        .I4(a_bus[0]),
        .I5(b_bus[3]),
        .O(shift2L[3]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \bram_firmware_code_address_reg[15]_i_13 
       (.I0(b_bus[4]),
        .I1(b_bus[5]),
        .I2(a_bus[1]),
        .I3(b_bus[6]),
        .I4(a_bus[0]),
        .I5(b_bus[7]),
        .O(shift2L[7]));
  (* SOFT_HLUTNM = "soft_lutpair111" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \bram_firmware_code_address_reg[15]_i_14 
       (.I0(shift2L[11]),
        .I1(a_bus[2]),
        .I2(shift2L[15]),
        .O(shift4L[15]));
  LUT6 #(
    .INIT(64'h0000000000010000)) 
    \bram_firmware_code_address_reg[15]_i_15 
       (.I0(\xilinx_16x1d.reg_loop[0].reg_bit2a_i_7_n_0 ),
        .I1(opcode[3]),
        .I2(\bram_firmware_code_address_reg[15]_i_23_n_0 ),
        .I3(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_43_n_0 ),
        .I4(\xilinx_16x1d.reg_loop[0].reg_bit2a_i_9_n_0 ),
        .I5(\u3_control/is_syscall__2 ),
        .O(\bram_firmware_code_address_reg[15]_i_15_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair126" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \bram_firmware_code_address_reg[15]_i_16 
       (.I0(shift2R[19]),
        .I1(a_bus[2]),
        .I2(shift2R[15]),
        .O(shift4R[15]));
  (* SOFT_HLUTNM = "soft_lutpair119" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \bram_firmware_code_address_reg[15]_i_17 
       (.I0(shift2R[27]),
        .I1(a_bus[2]),
        .I2(shift2R[23]),
        .O(shift4R[23]));
  LUT6 #(
    .INIT(64'hFF00FF01FF00FE00)) 
    \bram_firmware_code_address_reg[15]_i_18 
       (.I0(a_bus[3]),
        .I1(a_bus[2]),
        .I2(a_bus[1]),
        .I3(\bram_firmware_code_address_reg[19]_i_17_n_0 ),
        .I4(a_bus[0]),
        .I5(b_bus[31]),
        .O(shift8R[31]));
  LUT4 #(
    .INIT(16'hAAB8)) 
    \bram_firmware_code_address_reg[15]_i_2 
       (.I0(pc_current[15]),
        .I1(pause_in),
        .I2(\pc_reg_reg[15] ),
        .I3(\address_reg_reg[24]_0 ),
        .O(pc_future[15]));
  LUT5 #(
    .INIT(32'h0F000802)) 
    \bram_firmware_code_address_reg[15]_i_20 
       (.I0(\pc_reg_reg[25]_1 ),
        .I1(\upper_reg_reg[13] ),
        .I2(\pc_reg_reg[30]_0 ),
        .I3(\upper_reg_reg[31]_0 [12]),
        .I4(\pc_reg_reg[25] ),
        .O(\bram_firmware_code_address_reg[15]_i_20_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \bram_firmware_code_address_reg[15]_i_21 
       (.I0(b_bus[8]),
        .I1(b_bus[9]),
        .I2(a_bus[1]),
        .I3(b_bus[10]),
        .I4(a_bus[0]),
        .I5(b_bus[11]),
        .O(shift2L[11]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \bram_firmware_code_address_reg[15]_i_22 
       (.I0(b_bus[12]),
        .I1(b_bus[13]),
        .I2(a_bus[1]),
        .I3(b_bus[14]),
        .I4(a_bus[0]),
        .I5(b_bus[15]),
        .O(shift2L[15]));
  (* SOFT_HLUTNM = "soft_lutpair75" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \bram_firmware_code_address_reg[15]_i_23 
       (.I0(opcode[0]),
        .I1(opcode[1]),
        .O(\bram_firmware_code_address_reg[15]_i_23_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \bram_firmware_code_address_reg[15]_i_24 
       (.I0(b_bus[22]),
        .I1(b_bus[21]),
        .I2(a_bus[1]),
        .I3(b_bus[20]),
        .I4(a_bus[0]),
        .I5(b_bus[19]),
        .O(shift2R[19]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \bram_firmware_code_address_reg[15]_i_25 
       (.I0(b_bus[18]),
        .I1(b_bus[17]),
        .I2(a_bus[1]),
        .I3(b_bus[16]),
        .I4(a_bus[0]),
        .I5(b_bus[15]),
        .O(shift2R[15]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \bram_firmware_code_address_reg[15]_i_26 
       (.I0(b_bus[30]),
        .I1(b_bus[29]),
        .I2(a_bus[1]),
        .I3(b_bus[28]),
        .I4(a_bus[0]),
        .I5(b_bus[27]),
        .O(shift2R[27]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \bram_firmware_code_address_reg[15]_i_27 
       (.I0(b_bus[26]),
        .I1(b_bus[25]),
        .I2(a_bus[1]),
        .I3(b_bus[24]),
        .I4(a_bus[0]),
        .I5(b_bus[23]),
        .O(shift2R[23]));
  LUT6 #(
    .INIT(64'hFFFFFFFFAEFFAEAA)) 
    \bram_firmware_code_address_reg[15]_i_3 
       (.I0(c_alu[15]),
        .I1(shift8L[15]),
        .I2(a_bus[4]),
        .I3(\bram_firmware_code_address_reg[15]_i_7_n_0 ),
        .I4(\bram_firmware_code_address_reg[15]_i_8_n_0 ),
        .I5(c_mult[15]),
        .O(pc_new[15]));
  MUXF7 \bram_firmware_code_address_reg[15]_i_4 
       (.I0(\bram_firmware_code_address_reg[15]_i_10_n_0 ),
        .I1(\u6_alu/bv_adder043_out ),
        .O(c_alu[15]),
        .S(\u6_alu/c_alu1127_out ));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \bram_firmware_code_address_reg[15]_i_5 
       (.I0(shift2L[3]),
        .I1(a_bus[2]),
        .I2(shift2L[7]),
        .I3(a_bus[3]),
        .I4(shift4L[15]),
        .O(shift8L[15]));
  LUT5 #(
    .INIT(32'hAAAACFC0)) 
    \bram_firmware_code_address_reg[15]_i_6 
       (.I0(pc_current[4]),
        .I1(opcode[10]),
        .I2(\upper_reg[0]_i_4_n_0 ),
        .I3(reg_source[4]),
        .I4(\upper_reg[0]_i_5_n_0 ),
        .O(a_bus[4]));
  LUT2 #(
    .INIT(4'h2)) 
    \bram_firmware_code_address_reg[15]_i_7 
       (.I0(\bram_firmware_code_address_reg[15]_i_15_n_0 ),
        .I1(\bram_firmware_code_address_reg[19]_i_14_n_0 ),
        .O(\bram_firmware_code_address_reg[15]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAA8080000A808)) 
    \bram_firmware_code_address_reg[15]_i_8 
       (.I0(\bram_firmware_code_address_reg[19]_i_14_n_0 ),
        .I1(shift4R[15]),
        .I2(a_bus[3]),
        .I3(shift4R[23]),
        .I4(a_bus[4]),
        .I5(shift8R[31]),
        .O(\bram_firmware_code_address_reg[15]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF8000000F400)) 
    \bram_firmware_code_address_reg[15]_i_9 
       (.I0(\lower_reg_reg[13] ),
        .I1(\pc_reg_reg[30]_0 ),
        .I2(\bram_firmware_code_address_reg[15]_i_20_n_0 ),
        .I3(\pc_reg_reg[30]_1 ),
        .I4(\bram_firmware_code_address_reg[19]_i_10_n_0 ),
        .I5(\lower_reg_reg[31]_5 [15]),
        .O(c_mult[15]));
  LUT5 #(
    .INIT(32'hF4F1B0E0)) 
    \bram_firmware_code_address_reg[16]_i_1 
       (.I0(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I1(mem_state_reg_reg_n_0),
        .I2(pc_future[16]),
        .I3(pause_in0_out),
        .I4(pc_new[16]),
        .O(\bram_sec_policy_config_address[19] [14]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT4 #(
    .INIT(16'h0004)) 
    \bram_firmware_code_address_reg[16]_i_12 
       (.I0(a_bus[1]),
        .I1(b_bus[0]),
        .I2(a_bus[0]),
        .I3(a_bus[2]),
        .O(\bram_firmware_code_address_reg[16]_i_12_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair116" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \bram_firmware_code_address_reg[16]_i_13 
       (.I0(shift2L[12]),
        .I1(a_bus[2]),
        .I2(shift2L[16]),
        .O(shift4L[16]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \bram_firmware_code_address_reg[16]_i_14 
       (.I0(b_bus[27]),
        .I1(b_bus[26]),
        .I2(a_bus[1]),
        .I3(b_bus[25]),
        .I4(a_bus[0]),
        .I5(b_bus[24]),
        .O(shift2R[24]));
  LUT4 #(
    .INIT(16'hAAB8)) 
    \bram_firmware_code_address_reg[16]_i_2 
       (.I0(pc_current[16]),
        .I1(pause_in),
        .I2(\pc_reg_reg[16] ),
        .I3(\address_reg_reg[24]_0 ),
        .O(pc_future[16]));
  LUT5 #(
    .INIT(32'hFFFEEEFE)) 
    \bram_firmware_code_address_reg[16]_i_3 
       (.I0(c_alu[16]),
        .I1(c_shift[16]),
        .I2(\upper_reg_reg[16] ),
        .I3(\bram_firmware_code_address_reg[19]_i_10_n_0 ),
        .I4(\lower_reg_reg[31]_5 [16]),
        .O(pc_new[16]));
  LUT6 #(
    .INIT(64'h9669FFFF96690000)) 
    \bram_firmware_code_address_reg[16]_i_4 
       (.I0(b_bus[16]),
        .I1(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I2(a_bus[16]),
        .I3(\u6_alu/bv_adder33__3 ),
        .I4(\u6_alu/c_alu1127_out ),
        .I5(\bram_firmware_code_address_reg[16]_i_7_n_0 ),
        .O(c_alu[16]));
  LUT6 #(
    .INIT(64'hCACACAC0C0C0CAC0)) 
    \bram_firmware_code_address_reg[16]_i_5 
       (.I0(\bram_firmware_code_address_reg[19]_i_14_n_0 ),
        .I1(shift16L[16]),
        .I2(\bram_firmware_code_address_reg[15]_i_7_n_0 ),
        .I3(shift8R[16]),
        .I4(a_bus[4]),
        .I5(\bram_firmware_code_address_reg[19]_i_17_n_0 ),
        .O(c_shift[16]));
  LUT6 #(
    .INIT(64'h0000000011050540)) 
    \bram_firmware_code_address_reg[16]_i_7 
       (.I0(\bram_firmware_code_address_reg[17]_i_9_n_0 ),
        .I1(\bram_firmware_code_address_reg[17]_i_10_n_0 ),
        .I2(\bram_firmware_code_address_reg[17]_i_11_n_0 ),
        .I3(a_bus[16]),
        .I4(b_bus[16]),
        .I5(\u6_alu/c_alu1__0 ),
        .O(\bram_firmware_code_address_reg[16]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \bram_firmware_code_address_reg[16]_i_8 
       (.I0(\bram_firmware_code_address_reg[16]_i_12_n_0 ),
        .I1(a_bus[4]),
        .I2(shift4L[8]),
        .I3(a_bus[3]),
        .I4(shift4L[16]),
        .O(shift16L[16]));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \bram_firmware_code_address_reg[16]_i_9 
       (.I0(shift2R[28]),
        .I1(a_bus[2]),
        .I2(shift2R[24]),
        .I3(a_bus[3]),
        .I4(shift4R[16]),
        .O(shift8R[16]));
  LUT5 #(
    .INIT(32'hF4F1B0E0)) 
    \bram_firmware_code_address_reg[17]_i_1 
       (.I0(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I1(mem_state_reg_reg_n_0),
        .I2(pc_future[17]),
        .I3(pause_in0_out),
        .I4(pc_new[17]),
        .O(\bram_sec_policy_config_address[19] [15]));
  LUT4 #(
    .INIT(16'h4002)) 
    \bram_firmware_code_address_reg[17]_i_10 
       (.I0(\bram_firmware_code_address_reg[17]_i_13_n_0 ),
        .I1(alu_funcD[2]),
        .I2(\bram_firmware_code_address_reg[17]_i_15_n_0 ),
        .I3(alu_funcD[0]),
        .O(\bram_firmware_code_address_reg[17]_i_10_n_0 ));
  LUT4 #(
    .INIT(16'h1004)) 
    \bram_firmware_code_address_reg[17]_i_11 
       (.I0(alu_funcD[0]),
        .I1(\bram_firmware_code_address_reg[17]_i_13_n_0 ),
        .I2(alu_funcD[2]),
        .I3(\bram_firmware_code_address_reg[17]_i_15_n_0 ),
        .O(\bram_firmware_code_address_reg[17]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hAAAEAEAEAAAEAEAA)) 
    \bram_firmware_code_address_reg[17]_i_12 
       (.I0(\u3_control/is_syscall__2 ),
        .I1(\bram_firmware_code_address_reg[17]_i_22_n_0 ),
        .I2(opcode[31]),
        .I3(opcode[29]),
        .I4(opcode[30]),
        .I5(\u3_control/alu_function__23 [2]),
        .O(alu_funcD[2]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT5 #(
    .INIT(32'h00000004)) 
    \bram_firmware_code_address_reg[17]_i_13 
       (.I0(\xilinx_16x1d.reg_loop[0].reg_bit2a_i_7_n_0 ),
        .I1(\u3_control/alu_function__23 [3]),
        .I2(opcode[26]),
        .I3(opcode[31]),
        .I4(\u3_control/is_syscall__2 ),
        .O(\bram_firmware_code_address_reg[17]_i_13_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFEFEA)) 
    \bram_firmware_code_address_reg[17]_i_14 
       (.I0(\u3_control/is_syscall__2 ),
        .I1(\bram_firmware_code_address_reg[17]_i_25_n_0 ),
        .I2(opcode[27]),
        .I3(\bram_firmware_code_address_reg[17]_i_26_n_0 ),
        .I4(\bram_firmware_code_address_reg[17]_i_27_n_0 ),
        .O(alu_funcD[0]));
  LUT6 #(
    .INIT(64'h00000000000000A8)) 
    \bram_firmware_code_address_reg[17]_i_15 
       (.I0(\bram_firmware_code_address_reg[17]_i_28_n_0 ),
        .I1(\u3_control/alu_function__23 [1]),
        .I2(opcode[29]),
        .I3(opcode[31]),
        .I4(opcode[30]),
        .I5(\u3_control/is_syscall__2 ),
        .O(\bram_firmware_code_address_reg[17]_i_15_n_0 ));
  LUT6 #(
    .INIT(64'hFFB2E8FFE80000B2)) 
    \bram_firmware_code_address_reg[17]_i_16 
       (.I0(\u6_alu/bv_adder37__3 ),
        .I1(b_bus[14]),
        .I2(a_bus[14]),
        .I3(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I4(b_bus[15]),
        .I5(a_bus[15]),
        .O(\u6_alu/bv_adder33__3 ));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \bram_firmware_code_address_reg[17]_i_17 
       (.I0(\bram_firmware_code_address_reg[9]_i_10_n_0 ),
        .I1(a_bus[4]),
        .I2(shift4L[9]),
        .I3(a_bus[3]),
        .I4(shift4L[17]),
        .O(shift16L[17]));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \bram_firmware_code_address_reg[17]_i_18 
       (.I0(shift2R[29]),
        .I1(a_bus[2]),
        .I2(shift2R[25]),
        .I3(a_bus[3]),
        .I4(shift4R[17]),
        .O(shift8R[17]));
  LUT4 #(
    .INIT(16'hAAB8)) 
    \bram_firmware_code_address_reg[17]_i_2 
       (.I0(pc_current[17]),
        .I1(pause_in),
        .I2(\pc_reg_reg[17] ),
        .I3(\address_reg_reg[24]_0 ),
        .O(pc_future[17]));
  LUT6 #(
    .INIT(64'h00FF00000020008A)) 
    \bram_firmware_code_address_reg[17]_i_20 
       (.I0(\pc_reg_reg[25]_1 ),
        .I1(\upper_reg_reg[31]_0 [13]),
        .I2(\upper_reg_reg[14] ),
        .I3(\pc_reg_reg[30]_0 ),
        .I4(\upper_reg_reg[31]_0 [14]),
        .I5(\pc_reg_reg[25] ),
        .O(\bram_firmware_code_address_reg[17]_i_20_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \bram_firmware_code_address_reg[17]_i_21 
       (.I0(\pc_reg_reg[25] ),
        .I1(\pc_reg_reg[25]_1 ),
        .I2(\pc_reg_reg[30]_0 ),
        .O(\pc_reg_reg[30]_1 ));
  (* SOFT_HLUTNM = "soft_lutpair74" *) 
  LUT4 #(
    .INIT(16'h4E01)) 
    \bram_firmware_code_address_reg[17]_i_22 
       (.I0(opcode[27]),
        .I1(opcode[28]),
        .I2(opcode[26]),
        .I3(opcode[29]),
        .O(\bram_firmware_code_address_reg[17]_i_22_n_0 ));
  LUT6 #(
    .INIT(64'h0000040004440000)) 
    \bram_firmware_code_address_reg[17]_i_23 
       (.I0(opcode[4]),
        .I1(opcode[5]),
        .I2(opcode[0]),
        .I3(opcode[1]),
        .I4(opcode[2]),
        .I5(opcode[3]),
        .O(\u3_control/alu_function__23 [2]));
  LUT6 #(
    .INIT(64'h1000000000000000)) 
    \bram_firmware_code_address_reg[17]_i_24 
       (.I0(opcode[3]),
        .I1(opcode[4]),
        .I2(opcode[1]),
        .I3(opcode[0]),
        .I4(opcode[5]),
        .I5(opcode[2]),
        .O(\u3_control/alu_function__23 [3]));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT4 #(
    .INIT(16'h0010)) 
    \bram_firmware_code_address_reg[17]_i_25 
       (.I0(opcode[31]),
        .I1(opcode[30]),
        .I2(opcode[28]),
        .I3(opcode[26]),
        .O(\bram_firmware_code_address_reg[17]_i_25_n_0 ));
  LUT6 #(
    .INIT(64'h002301FF002301FA)) 
    \bram_firmware_code_address_reg[17]_i_26 
       (.I0(opcode[26]),
        .I1(opcode[31]),
        .I2(opcode[28]),
        .I3(opcode[30]),
        .I4(opcode[29]),
        .I5(\u3_control/alu_function__23 [0]),
        .O(\bram_firmware_code_address_reg[17]_i_26_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT5 #(
    .INIT(32'h00115444)) 
    \bram_firmware_code_address_reg[17]_i_27 
       (.I0(opcode[30]),
        .I1(opcode[31]),
        .I2(opcode[26]),
        .I3(opcode[29]),
        .I4(opcode[28]),
        .O(\bram_firmware_code_address_reg[17]_i_27_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT4 #(
    .INIT(16'h6041)) 
    \bram_firmware_code_address_reg[17]_i_28 
       (.I0(opcode[26]),
        .I1(opcode[28]),
        .I2(opcode[29]),
        .I3(opcode[27]),
        .O(\bram_firmware_code_address_reg[17]_i_28_n_0 ));
  LUT6 #(
    .INIT(64'h0040004000004440)) 
    \bram_firmware_code_address_reg[17]_i_29 
       (.I0(opcode[4]),
        .I1(opcode[5]),
        .I2(opcode[1]),
        .I3(opcode[2]),
        .I4(opcode[3]),
        .I5(opcode[0]),
        .O(\u3_control/alu_function__23 [1]));
  LUT5 #(
    .INIT(32'hFFFFFFE2)) 
    \bram_firmware_code_address_reg[17]_i_3 
       (.I0(\bram_firmware_code_address_reg[17]_i_4_n_0 ),
        .I1(\u6_alu/c_alu1127_out ),
        .I2(\u6_alu/bv_adder049_out ),
        .I3(c_shift[17]),
        .I4(c_mult[17]),
        .O(pc_new[17]));
  (* SOFT_HLUTNM = "soft_lutpair114" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \bram_firmware_code_address_reg[17]_i_30 
       (.I0(shift2L[13]),
        .I1(a_bus[2]),
        .I2(shift2L[17]),
        .O(shift4L[17]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \bram_firmware_code_address_reg[17]_i_31 
       (.I0(\bram_firmware_code_address_reg[19]_i_17_n_0 ),
        .I1(b_bus[31]),
        .I2(a_bus[1]),
        .I3(b_bus[30]),
        .I4(a_bus[0]),
        .I5(b_bus[29]),
        .O(shift2R[29]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \bram_firmware_code_address_reg[17]_i_32 
       (.I0(b_bus[28]),
        .I1(b_bus[27]),
        .I2(a_bus[1]),
        .I3(b_bus[26]),
        .I4(a_bus[0]),
        .I5(b_bus[25]),
        .O(shift2R[25]));
  LUT6 #(
    .INIT(64'h1045001004010010)) 
    \bram_firmware_code_address_reg[17]_i_33 
       (.I0(opcode[4]),
        .I1(opcode[2]),
        .I2(opcode[3]),
        .I3(opcode[1]),
        .I4(opcode[5]),
        .I5(opcode[0]),
        .O(\u3_control/alu_function__23 [0]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \bram_firmware_code_address_reg[17]_i_34 
       (.I0(b_bus[14]),
        .I1(b_bus[15]),
        .I2(a_bus[1]),
        .I3(b_bus[16]),
        .I4(a_bus[0]),
        .I5(b_bus[17]),
        .O(shift2L[17]));
  LUT6 #(
    .INIT(64'h0000000011050540)) 
    \bram_firmware_code_address_reg[17]_i_4 
       (.I0(\bram_firmware_code_address_reg[17]_i_9_n_0 ),
        .I1(\bram_firmware_code_address_reg[17]_i_10_n_0 ),
        .I2(\bram_firmware_code_address_reg[17]_i_11_n_0 ),
        .I3(a_bus[17]),
        .I4(b_bus[17]),
        .I5(\u6_alu/c_alu1__0 ),
        .O(\bram_firmware_code_address_reg[17]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h0110)) 
    \bram_firmware_code_address_reg[17]_i_5 
       (.I0(alu_funcD[2]),
        .I1(\bram_firmware_code_address_reg[17]_i_13_n_0 ),
        .I2(alu_funcD[0]),
        .I3(\bram_firmware_code_address_reg[17]_i_15_n_0 ),
        .O(\u6_alu/c_alu1127_out ));
  LUT6 #(
    .INIT(64'h9969966696996669)) 
    \bram_firmware_code_address_reg[17]_i_6 
       (.I0(b_bus[17]),
        .I1(a_bus[17]),
        .I2(\u6_alu/bv_adder33__3 ),
        .I3(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I4(b_bus[16]),
        .I5(a_bus[16]),
        .O(\u6_alu/bv_adder049_out ));
  LUT6 #(
    .INIT(64'hCACACAC0C0C0CAC0)) 
    \bram_firmware_code_address_reg[17]_i_7 
       (.I0(\bram_firmware_code_address_reg[19]_i_14_n_0 ),
        .I1(shift16L[17]),
        .I2(\bram_firmware_code_address_reg[15]_i_7_n_0 ),
        .I3(shift8R[17]),
        .I4(a_bus[4]),
        .I5(\bram_firmware_code_address_reg[19]_i_17_n_0 ),
        .O(c_shift[17]));
  LUT6 #(
    .INIT(64'hFFFFF8000000F400)) 
    \bram_firmware_code_address_reg[17]_i_8 
       (.I0(\lower_reg_reg[16] ),
        .I1(\pc_reg_reg[30]_0 ),
        .I2(\bram_firmware_code_address_reg[17]_i_20_n_0 ),
        .I3(\pc_reg_reg[30]_1 ),
        .I4(\bram_firmware_code_address_reg[19]_i_10_n_0 ),
        .I5(\lower_reg_reg[31]_5 [17]),
        .O(c_mult[17]));
  LUT4 #(
    .INIT(16'hBBBD)) 
    \bram_firmware_code_address_reg[17]_i_9 
       (.I0(\bram_firmware_code_address_reg[17]_i_13_n_0 ),
        .I1(alu_funcD[2]),
        .I2(\bram_firmware_code_address_reg[17]_i_15_n_0 ),
        .I3(alu_funcD[0]),
        .O(\bram_firmware_code_address_reg[17]_i_9_n_0 ));
  LUT5 #(
    .INIT(32'hF4F1B0E0)) 
    \bram_firmware_code_address_reg[18]_i_1 
       (.I0(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I1(mem_state_reg_reg_n_0),
        .I2(pc_future[18]),
        .I3(pause_in0_out),
        .I4(pc_new[18]),
        .O(\bram_sec_policy_config_address[19] [16]));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \bram_firmware_code_address_reg[18]_i_10 
       (.I0(shift2R[30]),
        .I1(a_bus[2]),
        .I2(shift2R[26]),
        .I3(a_bus[3]),
        .I4(shift4R[18]),
        .O(shift8R[18]));
  (* SOFT_HLUTNM = "soft_lutpair97" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \bram_firmware_code_address_reg[18]_i_13 
       (.I0(shift2L[14]),
        .I1(a_bus[2]),
        .I2(shift2L[18]),
        .O(shift4L[18]));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \bram_firmware_code_address_reg[18]_i_14 
       (.I0(\bram_firmware_code_address_reg[19]_i_17_n_0 ),
        .I1(a_bus[1]),
        .I2(b_bus[31]),
        .I3(a_bus[0]),
        .I4(b_bus[30]),
        .O(shift2R[30]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \bram_firmware_code_address_reg[18]_i_15 
       (.I0(b_bus[29]),
        .I1(b_bus[28]),
        .I2(a_bus[1]),
        .I3(b_bus[27]),
        .I4(a_bus[0]),
        .I5(b_bus[26]),
        .O(shift2R[26]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \bram_firmware_code_address_reg[18]_i_16 
       (.I0(b_bus[15]),
        .I1(b_bus[16]),
        .I2(a_bus[1]),
        .I3(b_bus[17]),
        .I4(a_bus[0]),
        .I5(b_bus[18]),
        .O(shift2L[18]));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT4 #(
    .INIT(16'hAAB8)) 
    \bram_firmware_code_address_reg[18]_i_2 
       (.I0(pc_current[18]),
        .I1(pause_in),
        .I2(\pc_reg_reg[18] ),
        .I3(\address_reg_reg[24]_0 ),
        .O(pc_future[18]));
  LUT5 #(
    .INIT(32'hFFFEEEFE)) 
    \bram_firmware_code_address_reg[18]_i_3 
       (.I0(c_alu[18]),
        .I1(c_shift[18]),
        .I2(\upper_reg_reg[18] ),
        .I3(\bram_firmware_code_address_reg[19]_i_10_n_0 ),
        .I4(\lower_reg_reg[31]_5 [18]),
        .O(pc_new[18]));
  MUXF7 \bram_firmware_code_address_reg[18]_i_4 
       (.I0(\bram_firmware_code_address_reg[18]_i_7_n_0 ),
        .I1(\u6_alu/bv_adder052_out ),
        .O(c_alu[18]),
        .S(\u6_alu/c_alu1127_out ));
  LUT6 #(
    .INIT(64'hCACACAC0C0C0CAC0)) 
    \bram_firmware_code_address_reg[18]_i_5 
       (.I0(\bram_firmware_code_address_reg[19]_i_14_n_0 ),
        .I1(shift16L[18]),
        .I2(\bram_firmware_code_address_reg[15]_i_7_n_0 ),
        .I3(shift8R[18]),
        .I4(a_bus[4]),
        .I5(\bram_firmware_code_address_reg[19]_i_17_n_0 ),
        .O(c_shift[18]));
  LUT6 #(
    .INIT(64'h0000000011050540)) 
    \bram_firmware_code_address_reg[18]_i_7 
       (.I0(\bram_firmware_code_address_reg[17]_i_9_n_0 ),
        .I1(\bram_firmware_code_address_reg[17]_i_10_n_0 ),
        .I2(\bram_firmware_code_address_reg[17]_i_11_n_0 ),
        .I3(a_bus[18]),
        .I4(b_bus[18]),
        .I5(\u6_alu/c_alu1__0 ),
        .O(\bram_firmware_code_address_reg[18]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'h9969966696996669)) 
    \bram_firmware_code_address_reg[18]_i_8 
       (.I0(b_bus[18]),
        .I1(a_bus[18]),
        .I2(\u6_alu/bv_adder31__3 ),
        .I3(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I4(b_bus[17]),
        .I5(a_bus[17]),
        .O(\u6_alu/bv_adder052_out ));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \bram_firmware_code_address_reg[18]_i_9 
       (.I0(\bram_firmware_code_address_reg[10]_i_10_n_0 ),
        .I1(a_bus[4]),
        .I2(shift4L[10]),
        .I3(a_bus[3]),
        .I4(shift4L[18]),
        .O(shift16L[18]));
  LUT5 #(
    .INIT(32'hF4F1B0E0)) 
    \bram_firmware_code_address_reg[19]_i_1 
       (.I0(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I1(mem_state_reg_reg_n_0),
        .I2(pc_future[19]),
        .I3(pause_in0_out),
        .I4(pc_new[19]),
        .O(\bram_sec_policy_config_address[19] [17]));
  LUT5 #(
    .INIT(32'h00000004)) 
    \bram_firmware_code_address_reg[19]_i_10 
       (.I0(\lower_reg_reg[31] ),
        .I1(\lower_reg_reg[31]_0 ),
        .I2(\lower_reg_reg[31]_1 ),
        .I3(\lower_reg_reg[31]_2 ),
        .I4(negate_reg_reg_0),
        .O(\bram_firmware_code_address_reg[19]_i_10_n_0 ));
  LUT4 #(
    .INIT(16'h0010)) 
    \bram_firmware_code_address_reg[19]_i_11 
       (.I0(\bram_firmware_code_address_reg[17]_i_13_n_0 ),
        .I1(alu_funcD[2]),
        .I2(alu_funcD[0]),
        .I3(\bram_firmware_code_address_reg[17]_i_15_n_0 ),
        .O(\bram_firmware_code_address_reg[19]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hFFB2E8FFE80000B2)) 
    \bram_firmware_code_address_reg[19]_i_12 
       (.I0(\u6_alu/bv_adder31__3 ),
        .I1(b_bus[17]),
        .I2(a_bus[17]),
        .I3(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I4(b_bus[18]),
        .I5(a_bus[18]),
        .O(\u6_alu/bv_adder27__3 ));
  LUT6 #(
    .INIT(64'h0000000011050540)) 
    \bram_firmware_code_address_reg[19]_i_13 
       (.I0(\bram_firmware_code_address_reg[17]_i_9_n_0 ),
        .I1(\bram_firmware_code_address_reg[17]_i_10_n_0 ),
        .I2(\bram_firmware_code_address_reg[17]_i_11_n_0 ),
        .I3(a_bus[19]),
        .I4(b_bus[19]),
        .I5(\u6_alu/c_alu1__0 ),
        .O(\bram_firmware_code_address_reg[19]_i_13_n_0 ));
  LUT5 #(
    .INIT(32'h01000000)) 
    \bram_firmware_code_address_reg[19]_i_14 
       (.I0(\xilinx_16x1d.reg_loop[0].reg_bit2a_i_7_n_0 ),
        .I1(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_43_n_0 ),
        .I2(opcode[3]),
        .I3(opcode[1]),
        .I4(\xilinx_16x1d.reg_loop[0].reg_bit2a_i_9_n_0 ),
        .O(\bram_firmware_code_address_reg[19]_i_14_n_0 ));
  LUT6 #(
    .INIT(64'h0F004F4F0F004040)) 
    \bram_firmware_code_address_reg[19]_i_15 
       (.I0(a_bus[2]),
        .I1(shift2L[3]),
        .I2(a_bus[4]),
        .I3(shift4L[11]),
        .I4(a_bus[3]),
        .I5(shift4L[19]),
        .O(shift16L[19]));
  LUT3 #(
    .INIT(8'hB8)) 
    \bram_firmware_code_address_reg[19]_i_16 
       (.I0(shift4R[27]),
        .I1(a_bus[3]),
        .I2(shift4R[19]),
        .O(shift8R[19]));
  LUT3 #(
    .INIT(8'h80)) 
    \bram_firmware_code_address_reg[19]_i_17 
       (.I0(\bram_firmware_code_address_reg[15]_i_15_n_0 ),
        .I1(\bram_firmware_code_address_reg[19]_i_14_n_0 ),
        .I2(b_bus[31]),
        .O(\bram_firmware_code_address_reg[19]_i_17_n_0 ));
  LUT4 #(
    .INIT(16'h0001)) 
    \bram_firmware_code_address_reg[19]_i_2 
       (.I0(mem_source[1]),
        .I1(mem_source[0]),
        .I2(mem_source[3]),
        .I3(mem_source[2]),
        .O(\bram_firmware_code_address_reg[19]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00040000)) 
    \bram_firmware_code_address_reg[19]_i_20 
       (.I0(\lower_reg_reg[31] ),
        .I1(\lower_reg_reg[31]_0 ),
        .I2(\lower_reg_reg[31]_1 ),
        .I3(\lower_reg_reg[31]_2 ),
        .I4(negate_reg_reg_0),
        .O(\pc_reg_reg[30]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT5 #(
    .INIT(32'h00040000)) 
    \bram_firmware_code_address_reg[19]_i_21 
       (.I0(\lower_reg_reg[31]_0 ),
        .I1(\lower_reg_reg[31] ),
        .I2(\lower_reg_reg[31]_1 ),
        .I3(\lower_reg_reg[31]_2 ),
        .I4(negate_reg_reg_0),
        .O(\pc_reg_reg[25]_1 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT5 #(
    .INIT(32'h00000004)) 
    \bram_firmware_code_address_reg[19]_i_22 
       (.I0(\lower_reg_reg[31]_0 ),
        .I1(\lower_reg_reg[31] ),
        .I2(\lower_reg_reg[31]_1 ),
        .I3(\lower_reg_reg[31]_2 ),
        .I4(negate_reg_reg_0),
        .O(\pc_reg_reg[25] ));
  LUT6 #(
    .INIT(64'hFFB2E8FFE80000B2)) 
    \bram_firmware_code_address_reg[19]_i_23 
       (.I0(\u6_alu/bv_adder35__3 ),
        .I1(b_bus[15]),
        .I2(a_bus[15]),
        .I3(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I4(b_bus[16]),
        .I5(a_bus[16]),
        .O(\u6_alu/bv_adder31__3 ));
  (* SOFT_HLUTNM = "soft_lutpair112" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \bram_firmware_code_address_reg[19]_i_24 
       (.I0(shift2L[15]),
        .I1(a_bus[2]),
        .I2(shift2L[19]),
        .O(shift4L[19]));
  LUT6 #(
    .INIT(64'hCDC8FFFFCDC80000)) 
    \bram_firmware_code_address_reg[19]_i_25 
       (.I0(a_bus[1]),
        .I1(\bram_firmware_code_address_reg[19]_i_17_n_0 ),
        .I2(a_bus[0]),
        .I3(b_bus[31]),
        .I4(a_bus[2]),
        .I5(shift2R[27]),
        .O(shift4R[27]));
  LUT6 #(
    .INIT(64'hFFB2E8FFE80000B2)) 
    \bram_firmware_code_address_reg[19]_i_27 
       (.I0(\u6_alu/bv_adder39__3 ),
        .I1(b_bus[13]),
        .I2(a_bus[13]),
        .I3(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I4(b_bus[14]),
        .I5(a_bus[14]),
        .O(\u6_alu/bv_adder35__3 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \bram_firmware_code_address_reg[19]_i_28 
       (.I0(b_bus[16]),
        .I1(b_bus[17]),
        .I2(a_bus[1]),
        .I3(b_bus[18]),
        .I4(a_bus[0]),
        .I5(b_bus[19]),
        .O(shift2L[19]));
  LUT6 #(
    .INIT(64'hFFFFFEE0FEE00000)) 
    \bram_firmware_code_address_reg[19]_i_29 
       (.I0(bv_adder4429_out),
        .I1(bv_adder44__0),
        .I2(\bram_firmware_code_address_reg[19]_i_32_n_0 ),
        .I3(a_bus[11]),
        .I4(\bram_firmware_code_address_reg[19]_i_33_n_0 ),
        .I5(a_bus[12]),
        .O(\u6_alu/bv_adder39__3 ));
  LUT4 #(
    .INIT(16'hAAB8)) 
    \bram_firmware_code_address_reg[19]_i_3 
       (.I0(pc_current[19]),
        .I1(pause_in),
        .I2(\pc_reg_reg[19] ),
        .I3(\address_reg_reg[24]_0 ),
        .O(pc_future[19]));
  LUT6 #(
    .INIT(64'hEEE8E88800000000)) 
    \bram_firmware_code_address_reg[19]_i_30 
       (.I0(a_bus[9]),
        .I1(\bram_firmware_code_address_reg[19]_i_34_n_0 ),
        .I2(a_bus[8]),
        .I3(\bram_firmware_code_address_reg[19]_i_35_n_0 ),
        .I4(\u6_alu/bv_adder49__3 ),
        .I5(bv_adder4538_out),
        .O(bv_adder4429_out));
  (* SOFT_HLUTNM = "soft_lutpair128" *) 
  LUT3 #(
    .INIT(8'h82)) 
    \bram_firmware_code_address_reg[19]_i_31 
       (.I0(a_bus[10]),
        .I1(b_bus[10]),
        .I2(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .O(bv_adder44__0));
  (* SOFT_HLUTNM = "soft_lutpair150" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \bram_firmware_code_address_reg[19]_i_32 
       (.I0(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I1(b_bus[11]),
        .O(\bram_firmware_code_address_reg[19]_i_32_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair148" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \bram_firmware_code_address_reg[19]_i_33 
       (.I0(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I1(b_bus[12]),
        .O(\bram_firmware_code_address_reg[19]_i_33_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair148" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \bram_firmware_code_address_reg[19]_i_34 
       (.I0(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I1(b_bus[9]),
        .O(\bram_firmware_code_address_reg[19]_i_34_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair147" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \bram_firmware_code_address_reg[19]_i_35 
       (.I0(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I1(b_bus[8]),
        .O(\bram_firmware_code_address_reg[19]_i_35_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair128" *) 
  LUT3 #(
    .INIT(8'hEB)) 
    \bram_firmware_code_address_reg[19]_i_36 
       (.I0(a_bus[10]),
        .I1(b_bus[10]),
        .I2(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .O(bv_adder4538_out));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFF6000)) 
    \bram_firmware_code_address_reg[19]_i_4 
       (.I0(\lower_reg_reg[31]_0 ),
        .I1(\lower_reg_reg[31] ),
        .I2(\bram_firmware_code_address_reg[19]_i_6_n_0 ),
        .I3(neqOp0_in),
        .I4(fifo_ptm_empty),
        .I5(fifo_monitor_to_kernel_full),
        .O(pause_in0_out));
  LUT5 #(
    .INIT(32'hFFFEEEFE)) 
    \bram_firmware_code_address_reg[19]_i_5 
       (.I0(c_alu[19]),
        .I1(c_shift[19]),
        .I2(\upper_reg_reg[19] ),
        .I3(\bram_firmware_code_address_reg[19]_i_10_n_0 ),
        .I4(\lower_reg_reg[31]_5 [19]),
        .O(pc_new[19]));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \bram_firmware_code_address_reg[19]_i_6 
       (.I0(\lower_reg_reg[31]_1 ),
        .I1(\lower_reg_reg[31]_2 ),
        .O(\bram_firmware_code_address_reg[19]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h9669FFFF96690000)) 
    \bram_firmware_code_address_reg[19]_i_7 
       (.I0(b_bus[19]),
        .I1(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I2(a_bus[19]),
        .I3(\u6_alu/bv_adder27__3 ),
        .I4(\u6_alu/c_alu1127_out ),
        .I5(\bram_firmware_code_address_reg[19]_i_13_n_0 ),
        .O(c_alu[19]));
  LUT6 #(
    .INIT(64'hCACACAC0C0C0CAC0)) 
    \bram_firmware_code_address_reg[19]_i_8 
       (.I0(\bram_firmware_code_address_reg[19]_i_14_n_0 ),
        .I1(shift16L[19]),
        .I2(\bram_firmware_code_address_reg[15]_i_7_n_0 ),
        .I3(shift8R[19]),
        .I4(a_bus[4]),
        .I5(\bram_firmware_code_address_reg[19]_i_17_n_0 ),
        .O(c_shift[19]));
  LUT5 #(
    .INIT(32'hF4F1B0E0)) 
    \bram_firmware_code_address_reg[2]_i_1 
       (.I0(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I1(mem_state_reg_reg_n_0),
        .I2(pc_future[2]),
        .I3(pause_in0_out),
        .I4(pc_new[2]),
        .O(\bram_sec_policy_config_address[19] [0]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \bram_firmware_code_address_reg[2]_i_10 
       (.I0(shift4R[10]),
        .I1(a_bus[3]),
        .I2(shift2R[6]),
        .I3(a_bus[2]),
        .I4(shift2R[2]),
        .O(shift8R[2]));
  LUT6 #(
    .INIT(64'h00FF0000000200A8)) 
    \bram_firmware_code_address_reg[2]_i_11 
       (.I0(\pc_reg_reg[25]_1 ),
        .I1(\upper_reg_reg[31]_0 [1]),
        .I2(\upper_reg_reg[31]_0 [0]),
        .I3(\pc_reg_reg[30]_0 ),
        .I4(\upper_reg_reg[31]_0 [2]),
        .I5(\pc_reg_reg[25] ),
        .O(\pc_reg_reg[2]_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \bram_firmware_code_address_reg[2]_i_12 
       (.I0(b_bus[5]),
        .I1(b_bus[4]),
        .I2(a_bus[1]),
        .I3(b_bus[3]),
        .I4(a_bus[0]),
        .I5(b_bus[2]),
        .O(shift2R[2]));
  LUT4 #(
    .INIT(16'hAAB8)) 
    \bram_firmware_code_address_reg[2]_i_2 
       (.I0(pc_current[2]),
        .I1(pause_in),
        .I2(\pc_reg_reg[2] ),
        .I3(\address_reg_reg[24]_0 ),
        .O(pc_future[2]));
  LUT5 #(
    .INIT(32'hFFFEEEFE)) 
    \bram_firmware_code_address_reg[2]_i_3 
       (.I0(c_alu[2]),
        .I1(c_shift[2]),
        .I2(\lower_reg_reg[2] ),
        .I3(\bram_firmware_code_address_reg[19]_i_10_n_0 ),
        .I4(\lower_reg_reg[31]_5 [2]),
        .O(pc_new[2]));
  LUT6 #(
    .INIT(64'h9669FFFF96690000)) 
    \bram_firmware_code_address_reg[2]_i_4 
       (.I0(b_bus[2]),
        .I1(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I2(a_bus[2]),
        .I3(\u6_alu/bv_adder61__3 ),
        .I4(\u6_alu/c_alu1127_out ),
        .I5(\bram_firmware_code_address_reg[2]_i_8_n_0 ),
        .O(c_alu[2]));
  LUT6 #(
    .INIT(64'h0A0ACAC00000CAC0)) 
    \bram_firmware_code_address_reg[2]_i_5 
       (.I0(\bram_firmware_code_address_reg[19]_i_14_n_0 ),
        .I1(\bram_firmware_code_address_reg[2]_i_9_n_0 ),
        .I2(\bram_firmware_code_address_reg[15]_i_7_n_0 ),
        .I3(shift8R[2]),
        .I4(a_bus[4]),
        .I5(shift8R[18]),
        .O(c_shift[2]));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT5 #(
    .INIT(32'hFD8F800D)) 
    \bram_firmware_code_address_reg[2]_i_7 
       (.I0(b_bus[0]),
        .I1(a_bus[0]),
        .I2(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I3(b_bus[1]),
        .I4(a_bus[1]),
        .O(\u6_alu/bv_adder61__3 ));
  LUT6 #(
    .INIT(64'h0000000011050540)) 
    \bram_firmware_code_address_reg[2]_i_8 
       (.I0(\bram_firmware_code_address_reg[17]_i_9_n_0 ),
        .I1(\bram_firmware_code_address_reg[17]_i_10_n_0 ),
        .I2(\bram_firmware_code_address_reg[17]_i_11_n_0 ),
        .I3(a_bus[2]),
        .I4(b_bus[2]),
        .I5(\u6_alu/c_alu1__0 ),
        .O(\bram_firmware_code_address_reg[2]_i_8_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair103" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \bram_firmware_code_address_reg[2]_i_9 
       (.I0(\bram_firmware_code_address_reg[10]_i_10_n_0 ),
        .I1(a_bus[3]),
        .O(\bram_firmware_code_address_reg[2]_i_9_n_0 ));
  LUT5 #(
    .INIT(32'hF4F1B0E0)) 
    \bram_firmware_code_address_reg[3]_i_1 
       (.I0(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I1(mem_state_reg_reg_n_0),
        .I2(pc_future[3]),
        .I3(pause_in0_out),
        .I4(pc_new[3]),
        .O(\bram_sec_policy_config_address[19] [1]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \bram_firmware_code_address_reg[3]_i_10 
       (.I0(shift4R[11]),
        .I1(a_bus[3]),
        .I2(shift2R[7]),
        .I3(a_bus[2]),
        .I4(shift2R[3]),
        .O(shift8R[3]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \bram_firmware_code_address_reg[3]_i_13 
       (.I0(b_bus[6]),
        .I1(b_bus[5]),
        .I2(a_bus[1]),
        .I3(b_bus[4]),
        .I4(a_bus[0]),
        .I5(b_bus[3]),
        .O(shift2R[3]));
  LUT4 #(
    .INIT(16'hAAB8)) 
    \bram_firmware_code_address_reg[3]_i_2 
       (.I0(pc_current[3]),
        .I1(pause_in),
        .I2(\pc_reg_reg[3] ),
        .I3(\address_reg_reg[24]_0 ),
        .O(pc_future[3]));
  LUT5 #(
    .INIT(32'hFFFEEEFE)) 
    \bram_firmware_code_address_reg[3]_i_3 
       (.I0(c_alu[3]),
        .I1(c_shift[3]),
        .I2(\upper_reg_reg[3] ),
        .I3(\bram_firmware_code_address_reg[19]_i_10_n_0 ),
        .I4(\lower_reg_reg[31]_5 [3]),
        .O(pc_new[3]));
  MUXF7 \bram_firmware_code_address_reg[3]_i_4 
       (.I0(\bram_firmware_code_address_reg[3]_i_7_n_0 ),
        .I1(\u6_alu/bv_adder07_out ),
        .O(c_alu[3]),
        .S(\u6_alu/c_alu1127_out ));
  LUT6 #(
    .INIT(64'h0A0ACAC00000CAC0)) 
    \bram_firmware_code_address_reg[3]_i_5 
       (.I0(\bram_firmware_code_address_reg[19]_i_14_n_0 ),
        .I1(\bram_firmware_code_address_reg[3]_i_9_n_0 ),
        .I2(\bram_firmware_code_address_reg[15]_i_7_n_0 ),
        .I3(shift8R[3]),
        .I4(a_bus[4]),
        .I5(shift8R[19]),
        .O(c_shift[3]));
  LUT6 #(
    .INIT(64'h0000000011050540)) 
    \bram_firmware_code_address_reg[3]_i_7 
       (.I0(\bram_firmware_code_address_reg[17]_i_9_n_0 ),
        .I1(\bram_firmware_code_address_reg[17]_i_10_n_0 ),
        .I2(\bram_firmware_code_address_reg[17]_i_11_n_0 ),
        .I3(a_bus[3]),
        .I4(b_bus[3]),
        .I5(\u6_alu/c_alu1__0 ),
        .O(\bram_firmware_code_address_reg[3]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'h9969966696996669)) 
    \bram_firmware_code_address_reg[3]_i_8 
       (.I0(b_bus[3]),
        .I1(a_bus[3]),
        .I2(\u6_alu/bv_adder61__3 ),
        .I3(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I4(b_bus[2]),
        .I5(a_bus[2]),
        .O(\u6_alu/bv_adder07_out ));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT3 #(
    .INIT(8'h04)) 
    \bram_firmware_code_address_reg[3]_i_9 
       (.I0(a_bus[2]),
        .I1(shift2L[3]),
        .I2(a_bus[3]),
        .O(\bram_firmware_code_address_reg[3]_i_9_n_0 ));
  LUT5 #(
    .INIT(32'hF4F1B0E0)) 
    \bram_firmware_code_address_reg[4]_i_1 
       (.I0(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I1(mem_state_reg_reg_n_0),
        .I2(pc_future[4]),
        .I3(pause_in0_out),
        .I4(pc_new[4]),
        .O(\bram_sec_policy_config_address[19] [2]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \bram_firmware_code_address_reg[4]_i_10 
       (.I0(shift4R[12]),
        .I1(a_bus[3]),
        .I2(shift2R[8]),
        .I3(a_bus[2]),
        .I4(shift2R[4]),
        .O(shift8R[4]));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \bram_firmware_code_address_reg[4]_i_11 
       (.I0(\bram_firmware_code_address_reg[19]_i_17_n_0 ),
        .I1(a_bus[2]),
        .I2(shift2R[28]),
        .I3(a_bus[3]),
        .I4(shift4R[20]),
        .O(shift8R[20]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \bram_firmware_code_address_reg[4]_i_14 
       (.I0(b_bus[7]),
        .I1(b_bus[6]),
        .I2(a_bus[1]),
        .I3(b_bus[5]),
        .I4(a_bus[0]),
        .I5(b_bus[4]),
        .O(shift2R[4]));
  LUT4 #(
    .INIT(16'hAAB8)) 
    \bram_firmware_code_address_reg[4]_i_2 
       (.I0(pc_current[4]),
        .I1(pause_in),
        .I2(\pc_reg_reg[4] ),
        .I3(\address_reg_reg[24]_0 ),
        .O(pc_future[4]));
  LUT5 #(
    .INIT(32'hFFFEEEFE)) 
    \bram_firmware_code_address_reg[4]_i_3 
       (.I0(c_alu[4]),
        .I1(c_shift[4]),
        .I2(\upper_reg_reg[4] ),
        .I3(\bram_firmware_code_address_reg[19]_i_10_n_0 ),
        .I4(\lower_reg_reg[31]_5 [4]),
        .O(pc_new[4]));
  LUT6 #(
    .INIT(64'h9669FFFF96690000)) 
    \bram_firmware_code_address_reg[4]_i_4 
       (.I0(b_bus[4]),
        .I1(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I2(a_bus[4]),
        .I3(\u6_alu/bv_adder57__3 ),
        .I4(\u6_alu/c_alu1127_out ),
        .I5(\bram_firmware_code_address_reg[4]_i_8_n_0 ),
        .O(c_alu[4]));
  LUT6 #(
    .INIT(64'h0A0ACAC00000CAC0)) 
    \bram_firmware_code_address_reg[4]_i_5 
       (.I0(\bram_firmware_code_address_reg[19]_i_14_n_0 ),
        .I1(\bram_firmware_code_address_reg[4]_i_9_n_0 ),
        .I2(\bram_firmware_code_address_reg[15]_i_7_n_0 ),
        .I3(shift8R[4]),
        .I4(a_bus[4]),
        .I5(shift8R[20]),
        .O(c_shift[4]));
  LUT6 #(
    .INIT(64'hFFB2E8FFE80000B2)) 
    \bram_firmware_code_address_reg[4]_i_7 
       (.I0(\u6_alu/bv_adder61__3 ),
        .I1(b_bus[2]),
        .I2(a_bus[2]),
        .I3(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I4(b_bus[3]),
        .I5(a_bus[3]),
        .O(\u6_alu/bv_adder57__3 ));
  LUT6 #(
    .INIT(64'h0000000011050540)) 
    \bram_firmware_code_address_reg[4]_i_8 
       (.I0(\bram_firmware_code_address_reg[17]_i_9_n_0 ),
        .I1(\bram_firmware_code_address_reg[17]_i_10_n_0 ),
        .I2(\bram_firmware_code_address_reg[17]_i_11_n_0 ),
        .I3(a_bus[4]),
        .I4(b_bus[4]),
        .I5(\u6_alu/c_alu1__0 ),
        .O(\bram_firmware_code_address_reg[4]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'h0000000022222E22)) 
    \bram_firmware_code_address_reg[4]_i_9 
       (.I0(shift2L[4]),
        .I1(a_bus[2]),
        .I2(a_bus[1]),
        .I3(b_bus[0]),
        .I4(a_bus[0]),
        .I5(a_bus[3]),
        .O(\bram_firmware_code_address_reg[4]_i_9_n_0 ));
  LUT5 #(
    .INIT(32'hF4F1B0E0)) 
    \bram_firmware_code_address_reg[5]_i_1 
       (.I0(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I1(mem_state_reg_reg_n_0),
        .I2(pc_future[5]),
        .I3(pause_in0_out),
        .I4(pc_new[5]),
        .O(\bram_sec_policy_config_address[19] [3]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \bram_firmware_code_address_reg[5]_i_10 
       (.I0(shift4R[13]),
        .I1(a_bus[3]),
        .I2(shift2R[9]),
        .I3(a_bus[2]),
        .I4(shift2R[5]),
        .O(shift8R[5]));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \bram_firmware_code_address_reg[5]_i_11 
       (.I0(\bram_firmware_code_address_reg[19]_i_17_n_0 ),
        .I1(a_bus[2]),
        .I2(shift2R[29]),
        .I3(a_bus[3]),
        .I4(shift4R[21]),
        .O(shift8R[21]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \bram_firmware_code_address_reg[5]_i_14 
       (.I0(b_bus[8]),
        .I1(b_bus[7]),
        .I2(a_bus[1]),
        .I3(b_bus[6]),
        .I4(a_bus[0]),
        .I5(b_bus[5]),
        .O(shift2R[5]));
  LUT4 #(
    .INIT(16'hAAB8)) 
    \bram_firmware_code_address_reg[5]_i_2 
       (.I0(pc_current[5]),
        .I1(pause_in),
        .I2(\pc_reg_reg[5] ),
        .I3(\address_reg_reg[24]_0 ),
        .O(pc_future[5]));
  LUT5 #(
    .INIT(32'hFFFEEEFE)) 
    \bram_firmware_code_address_reg[5]_i_3 
       (.I0(c_alu[5]),
        .I1(c_shift[5]),
        .I2(\upper_reg_reg[5] ),
        .I3(\bram_firmware_code_address_reg[19]_i_10_n_0 ),
        .I4(\lower_reg_reg[31]_5 [5]),
        .O(pc_new[5]));
  MUXF7 \bram_firmware_code_address_reg[5]_i_4 
       (.I0(\bram_firmware_code_address_reg[5]_i_7_n_0 ),
        .I1(\u6_alu/bv_adder013_out ),
        .O(c_alu[5]),
        .S(\u6_alu/c_alu1127_out ));
  LUT6 #(
    .INIT(64'h0A0ACAC00000CAC0)) 
    \bram_firmware_code_address_reg[5]_i_5 
       (.I0(\bram_firmware_code_address_reg[19]_i_14_n_0 ),
        .I1(\bram_firmware_code_address_reg[5]_i_9_n_0 ),
        .I2(\bram_firmware_code_address_reg[15]_i_7_n_0 ),
        .I3(shift8R[5]),
        .I4(a_bus[4]),
        .I5(shift8R[21]),
        .O(c_shift[5]));
  LUT6 #(
    .INIT(64'h0000000011050540)) 
    \bram_firmware_code_address_reg[5]_i_7 
       (.I0(\bram_firmware_code_address_reg[17]_i_9_n_0 ),
        .I1(\bram_firmware_code_address_reg[17]_i_10_n_0 ),
        .I2(\bram_firmware_code_address_reg[17]_i_11_n_0 ),
        .I3(a_bus[5]),
        .I4(b_bus[5]),
        .I5(\u6_alu/c_alu1__0 ),
        .O(\bram_firmware_code_address_reg[5]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'h9969966696996669)) 
    \bram_firmware_code_address_reg[5]_i_8 
       (.I0(b_bus[5]),
        .I1(a_bus[5]),
        .I2(\u6_alu/bv_adder57__3 ),
        .I3(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I4(b_bus[4]),
        .I5(a_bus[4]),
        .O(\u6_alu/bv_adder013_out ));
  (* SOFT_HLUTNM = "soft_lutpair89" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \bram_firmware_code_address_reg[5]_i_9 
       (.I0(shift4L[5]),
        .I1(a_bus[3]),
        .O(\bram_firmware_code_address_reg[5]_i_9_n_0 ));
  LUT5 #(
    .INIT(32'hF4F1B0E0)) 
    \bram_firmware_code_address_reg[6]_i_1 
       (.I0(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I1(mem_state_reg_reg_n_0),
        .I2(pc_future[6]),
        .I3(pause_in0_out),
        .I4(pc_new[6]),
        .O(\bram_sec_policy_config_address[19] [4]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \bram_firmware_code_address_reg[6]_i_10 
       (.I0(shift4R[14]),
        .I1(a_bus[3]),
        .I2(shift2R[10]),
        .I3(a_bus[2]),
        .I4(shift2R[6]),
        .O(shift8R[6]));
  (* SOFT_HLUTNM = "soft_lutpair104" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \bram_firmware_code_address_reg[6]_i_11 
       (.I0(shift4R[30]),
        .I1(a_bus[3]),
        .I2(shift4R[22]),
        .O(shift8R[22]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \bram_firmware_code_address_reg[6]_i_14 
       (.I0(b_bus[9]),
        .I1(b_bus[8]),
        .I2(a_bus[1]),
        .I3(b_bus[7]),
        .I4(a_bus[0]),
        .I5(b_bus[6]),
        .O(shift2R[6]));
  LUT6 #(
    .INIT(64'hCDC8CDCDCDC8C8C8)) 
    \bram_firmware_code_address_reg[6]_i_15 
       (.I0(a_bus[2]),
        .I1(\bram_firmware_code_address_reg[19]_i_17_n_0 ),
        .I2(a_bus[1]),
        .I3(b_bus[31]),
        .I4(a_bus[0]),
        .I5(b_bus[30]),
        .O(shift4R[30]));
  LUT4 #(
    .INIT(16'hAAB8)) 
    \bram_firmware_code_address_reg[6]_i_2 
       (.I0(pc_current[6]),
        .I1(pause_in),
        .I2(\pc_reg_reg[6] ),
        .I3(\address_reg_reg[24]_0 ),
        .O(pc_future[6]));
  LUT5 #(
    .INIT(32'hFFFEEEFE)) 
    \bram_firmware_code_address_reg[6]_i_3 
       (.I0(c_alu[6]),
        .I1(c_shift[6]),
        .I2(\upper_reg_reg[6] ),
        .I3(\bram_firmware_code_address_reg[19]_i_10_n_0 ),
        .I4(\lower_reg_reg[31]_5 [6]),
        .O(pc_new[6]));
  LUT6 #(
    .INIT(64'h9669FFFF96690000)) 
    \bram_firmware_code_address_reg[6]_i_4 
       (.I0(b_bus[6]),
        .I1(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I2(a_bus[6]),
        .I3(\u6_alu/bv_adder53__3 ),
        .I4(\u6_alu/c_alu1127_out ),
        .I5(\bram_firmware_code_address_reg[6]_i_8_n_0 ),
        .O(c_alu[6]));
  LUT6 #(
    .INIT(64'h0A0ACAC00000CAC0)) 
    \bram_firmware_code_address_reg[6]_i_5 
       (.I0(\bram_firmware_code_address_reg[19]_i_14_n_0 ),
        .I1(\bram_firmware_code_address_reg[6]_i_9_n_0 ),
        .I2(\bram_firmware_code_address_reg[15]_i_7_n_0 ),
        .I3(shift8R[6]),
        .I4(a_bus[4]),
        .I5(shift8R[22]),
        .O(c_shift[6]));
  LUT6 #(
    .INIT(64'hFFB2E8FFE80000B2)) 
    \bram_firmware_code_address_reg[6]_i_7 
       (.I0(\u6_alu/bv_adder57__3 ),
        .I1(b_bus[4]),
        .I2(a_bus[4]),
        .I3(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I4(b_bus[5]),
        .I5(a_bus[5]),
        .O(\u6_alu/bv_adder53__3 ));
  LUT6 #(
    .INIT(64'h0000000011050540)) 
    \bram_firmware_code_address_reg[6]_i_8 
       (.I0(\bram_firmware_code_address_reg[17]_i_9_n_0 ),
        .I1(\bram_firmware_code_address_reg[17]_i_10_n_0 ),
        .I2(\bram_firmware_code_address_reg[17]_i_11_n_0 ),
        .I3(a_bus[6]),
        .I4(b_bus[6]),
        .I5(\u6_alu/c_alu1__0 ),
        .O(\bram_firmware_code_address_reg[6]_i_8_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT4 #(
    .INIT(16'h00B8)) 
    \bram_firmware_code_address_reg[6]_i_9 
       (.I0(shift2L[2]),
        .I1(a_bus[2]),
        .I2(shift2L[6]),
        .I3(a_bus[3]),
        .O(\bram_firmware_code_address_reg[6]_i_9_n_0 ));
  LUT5 #(
    .INIT(32'hF4F1B0E0)) 
    \bram_firmware_code_address_reg[7]_i_1 
       (.I0(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I1(mem_state_reg_reg_n_0),
        .I2(pc_future[7]),
        .I3(pause_in0_out),
        .I4(pc_new[7]),
        .O(\bram_sec_policy_config_address[19] [5]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \bram_firmware_code_address_reg[7]_i_10 
       (.I0(shift4R[15]),
        .I1(a_bus[3]),
        .I2(shift2R[11]),
        .I3(a_bus[2]),
        .I4(shift2R[7]),
        .O(shift8R[7]));
  (* SOFT_HLUTNM = "soft_lutpair103" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \bram_firmware_code_address_reg[7]_i_11 
       (.I0(shift4R[31]),
        .I1(a_bus[3]),
        .I2(shift4R[23]),
        .O(shift8R[23]));
  LUT6 #(
    .INIT(64'h00FF00000020008A)) 
    \bram_firmware_code_address_reg[7]_i_13 
       (.I0(\pc_reg_reg[25]_1 ),
        .I1(\upper_reg_reg[31]_0 [3]),
        .I2(\upper_reg_reg[4]_0 ),
        .I3(\pc_reg_reg[30]_0 ),
        .I4(\upper_reg_reg[31]_0 [4]),
        .I5(\pc_reg_reg[25] ),
        .O(\pc_reg_reg[7]_0 ));
  LUT6 #(
    .INIT(64'hFFB2E8FFE80000B2)) 
    \bram_firmware_code_address_reg[7]_i_14 
       (.I0(\u6_alu/bv_adder59__3 ),
        .I1(b_bus[3]),
        .I2(a_bus[3]),
        .I3(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I4(b_bus[4]),
        .I5(a_bus[4]),
        .O(\u6_alu/bv_adder55__3 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \bram_firmware_code_address_reg[7]_i_15 
       (.I0(b_bus[10]),
        .I1(b_bus[9]),
        .I2(a_bus[1]),
        .I3(b_bus[8]),
        .I4(a_bus[0]),
        .I5(b_bus[7]),
        .O(shift2R[7]));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT5 #(
    .INIT(32'hF0F1F0E0)) 
    \bram_firmware_code_address_reg[7]_i_16 
       (.I0(a_bus[2]),
        .I1(a_bus[1]),
        .I2(\bram_firmware_code_address_reg[19]_i_17_n_0 ),
        .I3(a_bus[0]),
        .I4(b_bus[31]),
        .O(shift4R[31]));
  LUT6 #(
    .INIT(64'hFFB2E8FFE80000B2)) 
    \bram_firmware_code_address_reg[7]_i_17 
       (.I0(\u6_alu/bv_adder63__3 ),
        .I1(b_bus[1]),
        .I2(a_bus[1]),
        .I3(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I4(b_bus[2]),
        .I5(a_bus[2]),
        .O(\u6_alu/bv_adder59__3 ));
  LUT6 #(
    .INIT(64'h11111111DDD111D1)) 
    \bram_firmware_code_address_reg[7]_i_18 
       (.I0(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I1(b_bus[0]),
        .I2(reg_source[0]),
        .I3(\upper_reg[0]_i_4_n_0 ),
        .I4(opcode[6]),
        .I5(\upper_reg[0]_i_5_n_0 ),
        .O(\u6_alu/bv_adder63__3 ));
  LUT4 #(
    .INIT(16'hAAB8)) 
    \bram_firmware_code_address_reg[7]_i_2 
       (.I0(pc_current[7]),
        .I1(pause_in),
        .I2(\pc_reg_reg[7] ),
        .I3(\address_reg_reg[24]_0 ),
        .O(pc_future[7]));
  LUT5 #(
    .INIT(32'hFFFEEEFE)) 
    \bram_firmware_code_address_reg[7]_i_3 
       (.I0(c_alu[7]),
        .I1(c_shift[7]),
        .I2(\lower_reg_reg[7]_0 ),
        .I3(\bram_firmware_code_address_reg[19]_i_10_n_0 ),
        .I4(\lower_reg_reg[31]_5 [7]),
        .O(pc_new[7]));
  LUT6 #(
    .INIT(64'h9669FFFF96690000)) 
    \bram_firmware_code_address_reg[7]_i_4 
       (.I0(b_bus[7]),
        .I1(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I2(a_bus[7]),
        .I3(\u6_alu/bv_adder51__3 ),
        .I4(\u6_alu/c_alu1127_out ),
        .I5(\bram_firmware_code_address_reg[7]_i_8_n_0 ),
        .O(c_alu[7]));
  LUT6 #(
    .INIT(64'h0A0ACAC00000CAC0)) 
    \bram_firmware_code_address_reg[7]_i_5 
       (.I0(\bram_firmware_code_address_reg[19]_i_14_n_0 ),
        .I1(\bram_firmware_code_address_reg[7]_i_9_n_0 ),
        .I2(\bram_firmware_code_address_reg[15]_i_7_n_0 ),
        .I3(shift8R[7]),
        .I4(a_bus[4]),
        .I5(shift8R[23]),
        .O(c_shift[7]));
  LUT6 #(
    .INIT(64'hFFB2E8FFE80000B2)) 
    \bram_firmware_code_address_reg[7]_i_7 
       (.I0(\u6_alu/bv_adder55__3 ),
        .I1(b_bus[5]),
        .I2(a_bus[5]),
        .I3(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I4(b_bus[6]),
        .I5(a_bus[6]),
        .O(\u6_alu/bv_adder51__3 ));
  LUT6 #(
    .INIT(64'h0000000011050540)) 
    \bram_firmware_code_address_reg[7]_i_8 
       (.I0(\bram_firmware_code_address_reg[17]_i_9_n_0 ),
        .I1(\bram_firmware_code_address_reg[17]_i_10_n_0 ),
        .I2(\bram_firmware_code_address_reg[17]_i_11_n_0 ),
        .I3(a_bus[7]),
        .I4(b_bus[7]),
        .I5(\u6_alu/c_alu1__0 ),
        .O(\bram_firmware_code_address_reg[7]_i_8_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT4 #(
    .INIT(16'h00B8)) 
    \bram_firmware_code_address_reg[7]_i_9 
       (.I0(shift2L[3]),
        .I1(a_bus[2]),
        .I2(shift2L[7]),
        .I3(a_bus[3]),
        .O(\bram_firmware_code_address_reg[7]_i_9_n_0 ));
  LUT5 #(
    .INIT(32'hF4F1B0E0)) 
    \bram_firmware_code_address_reg[8]_i_1 
       (.I0(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I1(mem_state_reg_reg_n_0),
        .I2(pc_future[8]),
        .I3(pause_in0_out),
        .I4(pc_new[8]),
        .O(\bram_sec_policy_config_address[19] [6]));
  (* SOFT_HLUTNM = "soft_lutpair108" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \bram_firmware_code_address_reg[8]_i_10 
       (.I0(shift2L[4]),
        .I1(a_bus[2]),
        .I2(shift2L[8]),
        .O(shift4L[8]));
  (* SOFT_HLUTNM = "soft_lutpair121" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \bram_firmware_code_address_reg[8]_i_11 
       (.I0(shift2R[12]),
        .I1(a_bus[2]),
        .I2(shift2R[8]),
        .O(shift4R[8]));
  (* SOFT_HLUTNM = "soft_lutpair125" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \bram_firmware_code_address_reg[8]_i_12 
       (.I0(shift2R[20]),
        .I1(a_bus[2]),
        .I2(shift2R[16]),
        .O(shift4R[16]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT5 #(
    .INIT(32'hFF00B8B8)) 
    \bram_firmware_code_address_reg[8]_i_13 
       (.I0(shift2R[28]),
        .I1(a_bus[2]),
        .I2(shift2R[24]),
        .I3(\bram_firmware_code_address_reg[19]_i_17_n_0 ),
        .I4(a_bus[3]),
        .O(shift8R[24]));
  LUT5 #(
    .INIT(32'h0F000802)) 
    \bram_firmware_code_address_reg[8]_i_15 
       (.I0(\pc_reg_reg[25]_1 ),
        .I1(\upper_reg_reg[6]_0 ),
        .I2(\pc_reg_reg[30]_0 ),
        .I3(\upper_reg_reg[31]_0 [5]),
        .I4(\pc_reg_reg[25] ),
        .O(\bram_firmware_code_address_reg[8]_i_15_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \bram_firmware_code_address_reg[8]_i_16 
       (.I0(b_bus[11]),
        .I1(b_bus[10]),
        .I2(a_bus[1]),
        .I3(b_bus[9]),
        .I4(a_bus[0]),
        .I5(b_bus[8]),
        .O(shift2R[8]));
  LUT4 #(
    .INIT(16'hAAB8)) 
    \bram_firmware_code_address_reg[8]_i_2 
       (.I0(pc_current[8]),
        .I1(pause_in),
        .I2(\pc_reg_reg[8] ),
        .I3(\address_reg_reg[24]_0 ),
        .O(pc_future[8]));
  LUT6 #(
    .INIT(64'hFFFFFFFFAEFFAEAA)) 
    \bram_firmware_code_address_reg[8]_i_3 
       (.I0(c_alu[8]),
        .I1(shift8L[8]),
        .I2(a_bus[4]),
        .I3(\bram_firmware_code_address_reg[15]_i_7_n_0 ),
        .I4(\bram_firmware_code_address_reg[8]_i_6_n_0 ),
        .I5(c_mult[8]),
        .O(pc_new[8]));
  MUXF7 \bram_firmware_code_address_reg[8]_i_4 
       (.I0(\bram_firmware_code_address_reg[8]_i_8_n_0 ),
        .I1(\u6_alu/bv_adder022_out ),
        .O(c_alu[8]),
        .S(\u6_alu/c_alu1127_out ));
  LUT6 #(
    .INIT(64'h0004FFFF00040000)) 
    \bram_firmware_code_address_reg[8]_i_5 
       (.I0(a_bus[1]),
        .I1(b_bus[0]),
        .I2(a_bus[0]),
        .I3(a_bus[2]),
        .I4(a_bus[3]),
        .I5(shift4L[8]),
        .O(shift8L[8]));
  LUT6 #(
    .INIT(64'hAAAAA8080000A808)) 
    \bram_firmware_code_address_reg[8]_i_6 
       (.I0(\bram_firmware_code_address_reg[19]_i_14_n_0 ),
        .I1(shift4R[8]),
        .I2(a_bus[3]),
        .I3(shift4R[16]),
        .I4(a_bus[4]),
        .I5(shift8R[24]),
        .O(\bram_firmware_code_address_reg[8]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF8000000F400)) 
    \bram_firmware_code_address_reg[8]_i_7 
       (.I0(\lower_reg_reg[6] ),
        .I1(\pc_reg_reg[30]_0 ),
        .I2(\bram_firmware_code_address_reg[8]_i_15_n_0 ),
        .I3(\pc_reg_reg[30]_1 ),
        .I4(\bram_firmware_code_address_reg[19]_i_10_n_0 ),
        .I5(\lower_reg_reg[31]_5 [8]),
        .O(c_mult[8]));
  LUT6 #(
    .INIT(64'h0000000011050540)) 
    \bram_firmware_code_address_reg[8]_i_8 
       (.I0(\bram_firmware_code_address_reg[17]_i_9_n_0 ),
        .I1(\bram_firmware_code_address_reg[17]_i_10_n_0 ),
        .I2(\bram_firmware_code_address_reg[17]_i_11_n_0 ),
        .I3(a_bus[8]),
        .I4(b_bus[8]),
        .I5(\u6_alu/c_alu1__0 ),
        .O(\bram_firmware_code_address_reg[8]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'h9969966696996669)) 
    \bram_firmware_code_address_reg[8]_i_9 
       (.I0(b_bus[8]),
        .I1(a_bus[8]),
        .I2(\u6_alu/bv_adder51__3 ),
        .I3(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I4(b_bus[7]),
        .I5(a_bus[7]),
        .O(\u6_alu/bv_adder022_out ));
  LUT5 #(
    .INIT(32'hF4F1B0E0)) 
    \bram_firmware_code_address_reg[9]_i_1 
       (.I0(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I1(mem_state_reg_reg_n_0),
        .I2(pc_future[9]),
        .I3(pause_in0_out),
        .I4(pc_new[9]),
        .O(\bram_sec_policy_config_address[19] [7]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT5 #(
    .INIT(32'h00004540)) 
    \bram_firmware_code_address_reg[9]_i_10 
       (.I0(a_bus[1]),
        .I1(b_bus[0]),
        .I2(a_bus[0]),
        .I3(b_bus[1]),
        .I4(a_bus[2]),
        .O(\bram_firmware_code_address_reg[9]_i_10_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair109" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \bram_firmware_code_address_reg[9]_i_11 
       (.I0(shift2L[5]),
        .I1(a_bus[2]),
        .I2(shift2L[9]),
        .O(shift4L[9]));
  (* SOFT_HLUTNM = "soft_lutpair130" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \bram_firmware_code_address_reg[9]_i_12 
       (.I0(shift2R[13]),
        .I1(a_bus[2]),
        .I2(shift2R[9]),
        .O(shift4R[9]));
  (* SOFT_HLUTNM = "soft_lutpair124" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \bram_firmware_code_address_reg[9]_i_13 
       (.I0(shift2R[21]),
        .I1(a_bus[2]),
        .I2(shift2R[17]),
        .O(shift4R[17]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT5 #(
    .INIT(32'hFF00B8B8)) 
    \bram_firmware_code_address_reg[9]_i_14 
       (.I0(shift2R[29]),
        .I1(a_bus[2]),
        .I2(shift2R[25]),
        .I3(\bram_firmware_code_address_reg[19]_i_17_n_0 ),
        .I4(a_bus[3]),
        .O(shift8R[25]));
  LUT5 #(
    .INIT(32'h0F000802)) 
    \bram_firmware_code_address_reg[9]_i_16 
       (.I0(\pc_reg_reg[25]_1 ),
        .I1(\upper_reg_reg[7] ),
        .I2(\pc_reg_reg[30]_0 ),
        .I3(\upper_reg_reg[31]_0 [6]),
        .I4(\pc_reg_reg[25] ),
        .O(\bram_firmware_code_address_reg[9]_i_16_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \bram_firmware_code_address_reg[9]_i_17 
       (.I0(b_bus[12]),
        .I1(b_bus[11]),
        .I2(a_bus[1]),
        .I3(b_bus[10]),
        .I4(a_bus[0]),
        .I5(b_bus[9]),
        .O(shift2R[9]));
  LUT4 #(
    .INIT(16'hAAB8)) 
    \bram_firmware_code_address_reg[9]_i_2 
       (.I0(pc_current[9]),
        .I1(pause_in),
        .I2(\pc_reg_reg[9] ),
        .I3(\address_reg_reg[24]_0 ),
        .O(pc_future[9]));
  LUT6 #(
    .INIT(64'hFFFFFFFFAEFFAEAA)) 
    \bram_firmware_code_address_reg[9]_i_3 
       (.I0(c_alu[9]),
        .I1(shift8L[9]),
        .I2(a_bus[4]),
        .I3(\bram_firmware_code_address_reg[15]_i_7_n_0 ),
        .I4(\bram_firmware_code_address_reg[9]_i_6_n_0 ),
        .I5(c_mult[9]),
        .O(pc_new[9]));
  LUT6 #(
    .INIT(64'h9669FFFF96690000)) 
    \bram_firmware_code_address_reg[9]_i_4 
       (.I0(b_bus[9]),
        .I1(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I2(a_bus[9]),
        .I3(\u6_alu/bv_adder47__3 ),
        .I4(\u6_alu/c_alu1127_out ),
        .I5(\bram_firmware_code_address_reg[9]_i_9_n_0 ),
        .O(c_alu[9]));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \bram_firmware_code_address_reg[9]_i_5 
       (.I0(\bram_firmware_code_address_reg[9]_i_10_n_0 ),
        .I1(a_bus[3]),
        .I2(shift4L[9]),
        .O(shift8L[9]));
  LUT6 #(
    .INIT(64'hAAAAA8080000A808)) 
    \bram_firmware_code_address_reg[9]_i_6 
       (.I0(\bram_firmware_code_address_reg[19]_i_14_n_0 ),
        .I1(shift4R[9]),
        .I2(a_bus[3]),
        .I3(shift4R[17]),
        .I4(a_bus[4]),
        .I5(shift8R[25]),
        .O(\bram_firmware_code_address_reg[9]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF8000000F400)) 
    \bram_firmware_code_address_reg[9]_i_7 
       (.I0(\lower_reg_reg[7] ),
        .I1(\pc_reg_reg[30]_0 ),
        .I2(\bram_firmware_code_address_reg[9]_i_16_n_0 ),
        .I3(\pc_reg_reg[30]_1 ),
        .I4(\bram_firmware_code_address_reg[19]_i_10_n_0 ),
        .I5(\lower_reg_reg[31]_5 [9]),
        .O(c_mult[9]));
  LUT6 #(
    .INIT(64'hFFB2E8FFE80000B2)) 
    \bram_firmware_code_address_reg[9]_i_8 
       (.I0(\u6_alu/bv_adder51__3 ),
        .I1(b_bus[7]),
        .I2(a_bus[7]),
        .I3(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I4(b_bus[8]),
        .I5(a_bus[8]),
        .O(\u6_alu/bv_adder47__3 ));
  LUT6 #(
    .INIT(64'h0000000011050540)) 
    \bram_firmware_code_address_reg[9]_i_9 
       (.I0(\bram_firmware_code_address_reg[17]_i_9_n_0 ),
        .I1(\bram_firmware_code_address_reg[17]_i_10_n_0 ),
        .I2(\bram_firmware_code_address_reg[17]_i_11_n_0 ),
        .I3(a_bus[9]),
        .I4(b_bus[9]),
        .I5(\u6_alu/c_alu1__0 ),
        .O(\bram_firmware_code_address_reg[9]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'h4444444444444440)) 
    \bram_firmware_code_bytes_selection_reg[0]_i_1 
       (.I0(mem_state_reg_reg_n_0),
        .I1(\bram_firmware_code_bytes_selection_reg[0]_i_2_n_0 ),
        .I2(mem_source[1]),
        .I3(mem_source[0]),
        .I4(mem_source[2]),
        .I5(mem_source[3]),
        .O(\bram_firmware_code_bytes_selection[3] [0]));
  LUT6 #(
    .INIT(64'h00000000BC0C0000)) 
    \bram_firmware_code_bytes_selection_reg[0]_i_2 
       (.I0(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_15_n_0 ),
        .I1(mem_source[2]),
        .I2(mem_source[3]),
        .I3(\xilinx_16x1d.reg_loop[1].reg_bit1a_i_3_n_0 ),
        .I4(\bram_firmware_code_data_write_reg[31]_i_2_n_0 ),
        .I5(pause_in0_out),
        .O(\bram_firmware_code_bytes_selection_reg[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h4444444444444440)) 
    \bram_firmware_code_bytes_selection_reg[1]_i_1 
       (.I0(mem_state_reg_reg_n_0),
        .I1(\bram_firmware_code_bytes_selection_reg[1]_i_2_n_0 ),
        .I2(mem_source[1]),
        .I3(mem_source[0]),
        .I4(mem_source[2]),
        .I5(mem_source[3]),
        .O(\bram_firmware_code_bytes_selection[3] [1]));
  LUT6 #(
    .INIT(64'h000000004CF00000)) 
    \bram_firmware_code_bytes_selection_reg[1]_i_2 
       (.I0(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_15_n_0 ),
        .I1(\xilinx_16x1d.reg_loop[1].reg_bit1a_i_3_n_0 ),
        .I2(mem_source[2]),
        .I3(mem_source[3]),
        .I4(\bram_firmware_code_data_write_reg[31]_i_2_n_0 ),
        .I5(pause_in0_out),
        .O(\bram_firmware_code_bytes_selection_reg[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h4444444444444440)) 
    \bram_firmware_code_bytes_selection_reg[2]_i_1 
       (.I0(mem_state_reg_reg_n_0),
        .I1(\bram_firmware_code_bytes_selection_reg[2]_i_2_n_0 ),
        .I2(mem_source[1]),
        .I3(mem_source[0]),
        .I4(mem_source[2]),
        .I5(mem_source[3]),
        .O(\bram_firmware_code_bytes_selection[3] [2]));
  LUT6 #(
    .INIT(64'h0000000045F00000)) 
    \bram_firmware_code_bytes_selection_reg[2]_i_2 
       (.I0(\xilinx_16x1d.reg_loop[1].reg_bit1a_i_3_n_0 ),
        .I1(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_15_n_0 ),
        .I2(mem_source[2]),
        .I3(mem_source[3]),
        .I4(\bram_firmware_code_data_write_reg[31]_i_2_n_0 ),
        .I5(pause_in0_out),
        .O(\bram_firmware_code_bytes_selection_reg[2]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h4444444444444440)) 
    \bram_firmware_code_bytes_selection_reg[3]_i_1 
       (.I0(mem_state_reg_reg_n_0),
        .I1(\bram_firmware_code_bytes_selection_reg[3]_i_2_n_0 ),
        .I2(mem_source[1]),
        .I3(mem_source[0]),
        .I4(mem_source[2]),
        .I5(mem_source[3]),
        .O(\bram_firmware_code_bytes_selection[3] [3]));
  LUT6 #(
    .INIT(64'h0000000013F00000)) 
    \bram_firmware_code_bytes_selection_reg[3]_i_2 
       (.I0(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_15_n_0 ),
        .I1(\xilinx_16x1d.reg_loop[1].reg_bit1a_i_3_n_0 ),
        .I2(mem_source[2]),
        .I3(mem_source[3]),
        .I4(\bram_firmware_code_data_write_reg[31]_i_2_n_0 ),
        .I5(pause_in0_out),
        .O(\bram_firmware_code_bytes_selection_reg[3]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000100000000)) 
    \bram_firmware_code_bytes_selection_reg[3]_i_3 
       (.I0(\u3_control/is_syscall__2 ),
        .I1(opcode[27]),
        .I2(opcode[30]),
        .I3(opcode[29]),
        .I4(opcode[28]),
        .I5(opcode[31]),
        .O(mem_source[1]));
  LUT5 #(
    .INIT(32'h00000020)) 
    \bram_firmware_code_bytes_selection_reg[3]_i_4 
       (.I0(opcode[29]),
        .I1(opcode[30]),
        .I2(opcode[31]),
        .I3(opcode[28]),
        .I4(\u3_control/is_syscall__2 ),
        .O(mem_source[0]));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT4 #(
    .INIT(16'h8880)) 
    \bram_firmware_code_data_write_reg[0]_i_1 
       (.I0(\bram_firmware_code_data_write_reg[31]_i_2_n_0 ),
        .I1(reg_target[0]),
        .I2(mem_source[3]),
        .I3(mem_source[2]),
        .O(\bram_firmware_code_data_write[31] [0]));
  LUT5 #(
    .INIT(32'hA8800880)) 
    \bram_firmware_code_data_write_reg[10]_i_1 
       (.I0(\bram_firmware_code_data_write_reg[31]_i_2_n_0 ),
        .I1(reg_target[10]),
        .I2(mem_source[3]),
        .I3(mem_source[2]),
        .I4(reg_target[2]),
        .O(\bram_firmware_code_data_write[31] [10]));
  LUT5 #(
    .INIT(32'hA8800880)) 
    \bram_firmware_code_data_write_reg[11]_i_1 
       (.I0(\bram_firmware_code_data_write_reg[31]_i_2_n_0 ),
        .I1(reg_target[11]),
        .I2(mem_source[3]),
        .I3(mem_source[2]),
        .I4(reg_target[3]),
        .O(\bram_firmware_code_data_write[31] [11]));
  LUT5 #(
    .INIT(32'hA8800880)) 
    \bram_firmware_code_data_write_reg[12]_i_1 
       (.I0(\bram_firmware_code_data_write_reg[31]_i_2_n_0 ),
        .I1(reg_target[12]),
        .I2(mem_source[3]),
        .I3(mem_source[2]),
        .I4(reg_target[4]),
        .O(\bram_firmware_code_data_write[31] [12]));
  LUT5 #(
    .INIT(32'hA8800880)) 
    \bram_firmware_code_data_write_reg[13]_i_1 
       (.I0(\bram_firmware_code_data_write_reg[31]_i_2_n_0 ),
        .I1(reg_target[13]),
        .I2(mem_source[3]),
        .I3(mem_source[2]),
        .I4(reg_target[5]),
        .O(\bram_firmware_code_data_write[31] [13]));
  LUT5 #(
    .INIT(32'hA8800880)) 
    \bram_firmware_code_data_write_reg[14]_i_1 
       (.I0(\bram_firmware_code_data_write_reg[31]_i_2_n_0 ),
        .I1(reg_target[14]),
        .I2(mem_source[3]),
        .I3(mem_source[2]),
        .I4(reg_target[6]),
        .O(\bram_firmware_code_data_write[31] [14]));
  LUT5 #(
    .INIT(32'hA8800880)) 
    \bram_firmware_code_data_write_reg[15]_i_1 
       (.I0(\bram_firmware_code_data_write_reg[31]_i_2_n_0 ),
        .I1(reg_target[15]),
        .I2(mem_source[3]),
        .I3(mem_source[2]),
        .I4(reg_target[7]),
        .O(\bram_firmware_code_data_write[31] [15]));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT5 #(
    .INIT(32'h8A808080)) 
    \bram_firmware_code_data_write_reg[16]_i_1 
       (.I0(\bram_firmware_code_data_write_reg[31]_i_2_n_0 ),
        .I1(reg_target[0]),
        .I2(mem_source[3]),
        .I3(mem_source[2]),
        .I4(reg_target[16]),
        .O(\bram_firmware_code_data_write[31] [16]));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT5 #(
    .INIT(32'h8A808080)) 
    \bram_firmware_code_data_write_reg[17]_i_1 
       (.I0(\bram_firmware_code_data_write_reg[31]_i_2_n_0 ),
        .I1(reg_target[1]),
        .I2(mem_source[3]),
        .I3(mem_source[2]),
        .I4(reg_target[17]),
        .O(\bram_firmware_code_data_write[31] [17]));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT5 #(
    .INIT(32'h8A808080)) 
    \bram_firmware_code_data_write_reg[18]_i_1 
       (.I0(\bram_firmware_code_data_write_reg[31]_i_2_n_0 ),
        .I1(reg_target[2]),
        .I2(mem_source[3]),
        .I3(mem_source[2]),
        .I4(reg_target[18]),
        .O(\bram_firmware_code_data_write[31] [18]));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT5 #(
    .INIT(32'h8A808080)) 
    \bram_firmware_code_data_write_reg[19]_i_1 
       (.I0(\bram_firmware_code_data_write_reg[31]_i_2_n_0 ),
        .I1(reg_target[3]),
        .I2(mem_source[3]),
        .I3(mem_source[2]),
        .I4(reg_target[19]),
        .O(\bram_firmware_code_data_write[31] [19]));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT4 #(
    .INIT(16'h8880)) 
    \bram_firmware_code_data_write_reg[1]_i_1 
       (.I0(\bram_firmware_code_data_write_reg[31]_i_2_n_0 ),
        .I1(reg_target[1]),
        .I2(mem_source[3]),
        .I3(mem_source[2]),
        .O(\bram_firmware_code_data_write[31] [1]));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT5 #(
    .INIT(32'h8A808080)) 
    \bram_firmware_code_data_write_reg[20]_i_1 
       (.I0(\bram_firmware_code_data_write_reg[31]_i_2_n_0 ),
        .I1(reg_target[4]),
        .I2(mem_source[3]),
        .I3(mem_source[2]),
        .I4(reg_target[20]),
        .O(\bram_firmware_code_data_write[31] [20]));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT5 #(
    .INIT(32'h8A808080)) 
    \bram_firmware_code_data_write_reg[21]_i_1 
       (.I0(\bram_firmware_code_data_write_reg[31]_i_2_n_0 ),
        .I1(reg_target[5]),
        .I2(mem_source[3]),
        .I3(mem_source[2]),
        .I4(reg_target[21]),
        .O(\bram_firmware_code_data_write[31] [21]));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT5 #(
    .INIT(32'h8A808080)) 
    \bram_firmware_code_data_write_reg[22]_i_1 
       (.I0(\bram_firmware_code_data_write_reg[31]_i_2_n_0 ),
        .I1(reg_target[6]),
        .I2(mem_source[3]),
        .I3(mem_source[2]),
        .I4(reg_target[22]),
        .O(\bram_firmware_code_data_write[31] [22]));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT5 #(
    .INIT(32'h8A808080)) 
    \bram_firmware_code_data_write_reg[23]_i_1 
       (.I0(\bram_firmware_code_data_write_reg[31]_i_2_n_0 ),
        .I1(reg_target[7]),
        .I2(mem_source[3]),
        .I3(mem_source[2]),
        .I4(reg_target[23]),
        .O(\bram_firmware_code_data_write[31] [23]));
  LUT6 #(
    .INIT(64'hAA80A0800A800080)) 
    \bram_firmware_code_data_write_reg[24]_i_1 
       (.I0(\bram_firmware_code_data_write_reg[31]_i_2_n_0 ),
        .I1(reg_target[8]),
        .I2(mem_source[3]),
        .I3(mem_source[2]),
        .I4(reg_target[24]),
        .I5(reg_target[0]),
        .O(\bram_firmware_code_data_write[31] [24]));
  LUT6 #(
    .INIT(64'hAA80A0800A800080)) 
    \bram_firmware_code_data_write_reg[25]_i_1 
       (.I0(\bram_firmware_code_data_write_reg[31]_i_2_n_0 ),
        .I1(reg_target[9]),
        .I2(mem_source[3]),
        .I3(mem_source[2]),
        .I4(reg_target[25]),
        .I5(reg_target[1]),
        .O(\bram_firmware_code_data_write[31] [25]));
  LUT6 #(
    .INIT(64'hAA80A0800A800080)) 
    \bram_firmware_code_data_write_reg[26]_i_1 
       (.I0(\bram_firmware_code_data_write_reg[31]_i_2_n_0 ),
        .I1(reg_target[10]),
        .I2(mem_source[3]),
        .I3(mem_source[2]),
        .I4(reg_target[26]),
        .I5(reg_target[2]),
        .O(\bram_firmware_code_data_write[31] [26]));
  LUT6 #(
    .INIT(64'hAA80A0800A800080)) 
    \bram_firmware_code_data_write_reg[27]_i_1 
       (.I0(\bram_firmware_code_data_write_reg[31]_i_2_n_0 ),
        .I1(reg_target[11]),
        .I2(mem_source[3]),
        .I3(mem_source[2]),
        .I4(reg_target[27]),
        .I5(reg_target[3]),
        .O(\bram_firmware_code_data_write[31] [27]));
  LUT6 #(
    .INIT(64'hAA80A0800A800080)) 
    \bram_firmware_code_data_write_reg[28]_i_1 
       (.I0(\bram_firmware_code_data_write_reg[31]_i_2_n_0 ),
        .I1(reg_target[12]),
        .I2(mem_source[3]),
        .I3(mem_source[2]),
        .I4(reg_target[28]),
        .I5(reg_target[4]),
        .O(\bram_firmware_code_data_write[31] [28]));
  LUT6 #(
    .INIT(64'hAA80A0800A800080)) 
    \bram_firmware_code_data_write_reg[29]_i_1 
       (.I0(\bram_firmware_code_data_write_reg[31]_i_2_n_0 ),
        .I1(reg_target[13]),
        .I2(mem_source[3]),
        .I3(mem_source[2]),
        .I4(reg_target[29]),
        .I5(reg_target[5]),
        .O(\bram_firmware_code_data_write[31] [29]));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT4 #(
    .INIT(16'h8880)) 
    \bram_firmware_code_data_write_reg[2]_i_1 
       (.I0(\bram_firmware_code_data_write_reg[31]_i_2_n_0 ),
        .I1(reg_target[2]),
        .I2(mem_source[3]),
        .I3(mem_source[2]),
        .O(\bram_firmware_code_data_write[31] [2]));
  LUT6 #(
    .INIT(64'hAA80A0800A800080)) 
    \bram_firmware_code_data_write_reg[30]_i_1 
       (.I0(\bram_firmware_code_data_write_reg[31]_i_2_n_0 ),
        .I1(reg_target[14]),
        .I2(mem_source[3]),
        .I3(mem_source[2]),
        .I4(reg_target[30]),
        .I5(reg_target[6]),
        .O(\bram_firmware_code_data_write[31] [30]));
  LUT6 #(
    .INIT(64'hAA80A0800A800080)) 
    \bram_firmware_code_data_write_reg[31]_i_1 
       (.I0(\bram_firmware_code_data_write_reg[31]_i_2_n_0 ),
        .I1(reg_target[15]),
        .I2(mem_source[3]),
        .I3(mem_source[2]),
        .I4(reg_target[31]),
        .I5(reg_target[7]),
        .O(\bram_firmware_code_data_write[31] [31]));
  (* SOFT_HLUTNM = "soft_lutpair142" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \bram_firmware_code_data_write_reg[31]_i_10 
       (.I0(opcode[30]),
        .I1(\u3_control/is_syscall__2 ),
        .O(\bram_firmware_code_data_write_reg[31]_i_10_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair106" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \bram_firmware_code_data_write_reg[31]_i_11 
       (.I0(opcode[20]),
        .I1(opcode[23]),
        .I2(opcode[26]),
        .O(\bram_firmware_code_data_write_reg[31]_i_11_n_0 ));
  LUT5 #(
    .INIT(32'h00000020)) 
    \bram_firmware_code_data_write_reg[31]_i_2 
       (.I0(opcode[31]),
        .I1(opcode[28]),
        .I2(opcode[29]),
        .I3(opcode[30]),
        .I4(\u3_control/is_syscall__2 ),
        .O(\bram_firmware_code_data_write_reg[31]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000222)) 
    \bram_firmware_code_data_write_reg[31]_i_4 
       (.I0(opcode[31]),
        .I1(opcode[30]),
        .I2(opcode[29]),
        .I3(opcode[28]),
        .I4(opcode[27]),
        .I5(\u3_control/is_syscall__2 ),
        .O(mem_source[3]));
  LUT6 #(
    .INIT(64'h000000040C0C000C)) 
    \bram_firmware_code_data_write_reg[31]_i_5 
       (.I0(opcode[29]),
        .I1(opcode[31]),
        .I2(\bram_firmware_code_data_write_reg[31]_i_10_n_0 ),
        .I3(opcode[26]),
        .I4(opcode[27]),
        .I5(opcode[28]),
        .O(mem_source[2]));
  LUT6 #(
    .INIT(64'hE200E2E2E2E2E2E2)) 
    \bram_firmware_code_data_write_reg[31]_i_8 
       (.I0(\bram_firmware_code_data_write_reg[31]_i_11_n_0 ),
        .I1(\xilinx_16x1d.reg_loop[0].reg_bit2a_i_6_n_0 ),
        .I2(opcode[20]),
        .I3(\xilinx_16x1d.reg_loop[0].reg_bit2a_i_7_n_0 ),
        .I4(\xilinx_16x1d.reg_loop[0].reg_bit2a_i_8_n_0 ),
        .I5(\xilinx_16x1d.reg_loop[0].reg_bit2a_i_9_n_0 ),
        .O(\bb_reg_reg[1] ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \bram_firmware_code_data_write_reg[31]_i_9 
       (.I0(\bb_reg_reg[1] ),
        .I1(\bb_reg_reg[1]_2 ),
        .I2(\bb_reg_reg[1]_3 ),
        .I3(\bb_reg_reg[1]_0 ),
        .I4(\bb_reg_reg[1]_1 ),
        .O(\bb_reg_reg[1]_4 ));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT4 #(
    .INIT(16'h8880)) 
    \bram_firmware_code_data_write_reg[3]_i_1 
       (.I0(\bram_firmware_code_data_write_reg[31]_i_2_n_0 ),
        .I1(reg_target[3]),
        .I2(mem_source[3]),
        .I3(mem_source[2]),
        .O(\bram_firmware_code_data_write[31] [3]));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT4 #(
    .INIT(16'h8880)) 
    \bram_firmware_code_data_write_reg[4]_i_1 
       (.I0(\bram_firmware_code_data_write_reg[31]_i_2_n_0 ),
        .I1(reg_target[4]),
        .I2(mem_source[3]),
        .I3(mem_source[2]),
        .O(\bram_firmware_code_data_write[31] [4]));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT4 #(
    .INIT(16'h8880)) 
    \bram_firmware_code_data_write_reg[5]_i_1 
       (.I0(\bram_firmware_code_data_write_reg[31]_i_2_n_0 ),
        .I1(reg_target[5]),
        .I2(mem_source[3]),
        .I3(mem_source[2]),
        .O(\bram_firmware_code_data_write[31] [5]));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT4 #(
    .INIT(16'h8880)) 
    \bram_firmware_code_data_write_reg[6]_i_1 
       (.I0(\bram_firmware_code_data_write_reg[31]_i_2_n_0 ),
        .I1(reg_target[6]),
        .I2(mem_source[3]),
        .I3(mem_source[2]),
        .O(\bram_firmware_code_data_write[31] [6]));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT4 #(
    .INIT(16'h8880)) 
    \bram_firmware_code_data_write_reg[7]_i_1 
       (.I0(\bram_firmware_code_data_write_reg[31]_i_2_n_0 ),
        .I1(reg_target[7]),
        .I2(mem_source[3]),
        .I3(mem_source[2]),
        .O(\bram_firmware_code_data_write[31] [7]));
  LUT5 #(
    .INIT(32'hA8800880)) 
    \bram_firmware_code_data_write_reg[8]_i_1 
       (.I0(\bram_firmware_code_data_write_reg[31]_i_2_n_0 ),
        .I1(reg_target[8]),
        .I2(mem_source[3]),
        .I3(mem_source[2]),
        .I4(reg_target[0]),
        .O(\bram_firmware_code_data_write[31] [8]));
  LUT5 #(
    .INIT(32'hA8800880)) 
    \bram_firmware_code_data_write_reg[9]_i_1 
       (.I0(\bram_firmware_code_data_write_reg[31]_i_2_n_0 ),
        .I1(reg_target[9]),
        .I2(mem_source[3]),
        .I3(mem_source[2]),
        .I4(reg_target[1]),
        .O(\bram_firmware_code_data_write[31] [9]));
  (* SOFT_HLUTNM = "soft_lutpair78" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    bram_firmware_code_en_reg_i_1
       (.I0(complete_address_next[30]),
        .I1(complete_address_next[28]),
        .I2(complete_address_next[29]),
        .I3(complete_address_next[31]),
        .O(\bram_firmware_code_address[2] ));
  (* SOFT_HLUTNM = "soft_lutpair80" *) 
  LUT4 #(
    .INIT(16'h15FF)) 
    bram_firmware_code_en_reg_i_2
       (.I0(complete_address_next[30]),
        .I1(complete_address_next[28]),
        .I2(complete_address_next[29]),
        .I3(complete_address_next[31]),
        .O(bram_sec_policy_config_en_0));
  (* SOFT_HLUTNM = "soft_lutpair86" *) 
  LUT4 #(
    .INIT(16'h0400)) 
    \bram_sec_policy_config_address_reg[19]_i_1 
       (.I0(complete_address_next[30]),
        .I1(complete_address_next[29]),
        .I2(complete_address_next[28]),
        .I3(complete_address_next[31]),
        .O(\bram_sec_policy_config_bytes_selection[3] ));
  LUT2 #(
    .INIT(4'h8)) 
    bram_sec_policy_config_en_reg_i_1
       (.I0(complete_address_next[29]),
        .I1(complete_address_next[31]),
        .O(bram_sec_policy_config_en));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT5 #(
    .INIT(32'h0000BBB9)) 
    \count_reg[0]_i_1 
       (.I0(\lower_reg_reg[31]_1 ),
        .I1(\lower_reg_reg[31]_2 ),
        .I2(\lower_reg_reg[31]_0 ),
        .I3(\lower_reg_reg[31] ),
        .I4(Q[0]),
        .O(D[0]));
  LUT6 #(
    .INIT(64'hBBB900000000BBB9)) 
    \count_reg[1]_i_1 
       (.I0(\lower_reg_reg[31]_1 ),
        .I1(\lower_reg_reg[31]_2 ),
        .I2(\lower_reg_reg[31]_0 ),
        .I3(\lower_reg_reg[31] ),
        .I4(Q[1]),
        .I5(Q[0]),
        .O(D[1]));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT4 #(
    .INIT(16'hFE0F)) 
    \count_reg[4]_i_2 
       (.I0(\lower_reg_reg[31] ),
        .I1(\lower_reg_reg[31]_0 ),
        .I2(\lower_reg_reg[31]_2 ),
        .I3(\lower_reg_reg[31]_1 ),
        .O(\count_reg_reg[4] ));
  LUT5 #(
    .INIT(32'hFFFFD000)) 
    \count_reg[5]_i_1 
       (.I0(\lower_reg_reg[31]_2 ),
        .I1(\lower_reg_reg[31]_1 ),
        .I2(\lower_reg_reg[0] ),
        .I3(neqOp0_in),
        .I4(\count_reg_reg[5] ),
        .O(\count_reg_reg[5]_0 ));
  LUT6 #(
    .INIT(64'h0000000000000100)) 
    \count_reg[5]_i_11 
       (.I0(opcode[30]),
        .I1(opcode[31]),
        .I2(\count_reg[5]_i_12_n_0 ),
        .I3(\count_reg[5]_i_16_n_0 ),
        .I4(opcode[28]),
        .I5(\u3_control/is_syscall__2 ),
        .O(\lower_reg_reg[31] ));
  (* SOFT_HLUTNM = "soft_lutpair131" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \count_reg[5]_i_12 
       (.I0(opcode[27]),
        .I1(opcode[29]),
        .O(\count_reg[5]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'h000000000000B200)) 
    \count_reg[5]_i_13 
       (.I0(opcode[3]),
        .I1(opcode[1]),
        .I2(opcode[0]),
        .I3(opcode[4]),
        .I4(\count_reg[5]_i_17_n_0 ),
        .I5(\count_reg[5]_i_18_n_0 ),
        .O(\count_reg[5]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000002000)) 
    \count_reg[5]_i_14 
       (.I0(opcode[4]),
        .I1(opcode[0]),
        .I2(opcode[3]),
        .I3(opcode[1]),
        .I4(\count_reg[5]_i_17_n_0 ),
        .I5(\count_reg[5]_i_18_n_0 ),
        .O(\count_reg[5]_i_14_n_0 ));
  LUT6 #(
    .INIT(64'h000000000000B800)) 
    \count_reg[5]_i_15 
       (.I0(opcode[0]),
        .I1(opcode[3]),
        .I2(opcode[1]),
        .I3(opcode[4]),
        .I4(\count_reg[5]_i_17_n_0 ),
        .I5(\count_reg[5]_i_18_n_0 ),
        .O(\count_reg[5]_i_15_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000090)) 
    \count_reg[5]_i_16 
       (.I0(opcode[1]),
        .I1(opcode[0]),
        .I2(opcode[4]),
        .I3(opcode[2]),
        .I4(opcode[5]),
        .I5(\count_reg[5]_i_18_n_0 ),
        .O(\count_reg[5]_i_16_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair96" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \count_reg[5]_i_17 
       (.I0(opcode[2]),
        .I1(opcode[5]),
        .O(\count_reg[5]_i_17_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair131" *) 
  LUT3 #(
    .INIT(8'hBA)) 
    \count_reg[5]_i_18 
       (.I0(opcode[29]),
        .I1(opcode[27]),
        .I2(opcode[26]),
        .O(\count_reg[5]_i_18_n_0 ));
  LUT5 #(
    .INIT(32'h33F337F3)) 
    \count_reg[5]_i_2 
       (.I0(\lower_reg_reg[31]_0 ),
        .I1(\count_reg_reg[4]_0 ),
        .I2(\lower_reg_reg[31]_2 ),
        .I3(\lower_reg_reg[31]_1 ),
        .I4(\lower_reg_reg[31] ),
        .O(D[2]));
  LUT6 #(
    .INIT(64'h0000000000000100)) 
    \count_reg[5]_i_4 
       (.I0(opcode[30]),
        .I1(opcode[31]),
        .I2(\count_reg[5]_i_12_n_0 ),
        .I3(\count_reg[5]_i_13_n_0 ),
        .I4(opcode[28]),
        .I5(\u3_control/is_syscall__2 ),
        .O(\lower_reg_reg[31]_2 ));
  LUT6 #(
    .INIT(64'h0000000000000100)) 
    \count_reg[5]_i_5 
       (.I0(opcode[30]),
        .I1(opcode[31]),
        .I2(\count_reg[5]_i_12_n_0 ),
        .I3(\count_reg[5]_i_14_n_0 ),
        .I4(opcode[28]),
        .I5(\u3_control/is_syscall__2 ),
        .O(\lower_reg_reg[31]_1 ));
  LUT3 #(
    .INIT(8'hBF)) 
    \count_reg[5]_i_6 
       (.I0(\lower_reg_reg[31]_1 ),
        .I1(\lower_reg_reg[31]_0 ),
        .I2(\lower_reg_reg[31] ),
        .O(\lower_reg_reg[0] ));
  LUT4 #(
    .INIT(16'h0E10)) 
    \count_reg[5]_i_8 
       (.I0(\lower_reg_reg[31]_0 ),
        .I1(\lower_reg_reg[31] ),
        .I2(\lower_reg_reg[31]_1 ),
        .I3(\lower_reg_reg[31]_2 ),
        .O(\count_reg_reg[5] ));
  LUT6 #(
    .INIT(64'h0000000000000100)) 
    \count_reg[5]_i_9 
       (.I0(opcode[30]),
        .I1(opcode[31]),
        .I2(\count_reg[5]_i_12_n_0 ),
        .I3(\count_reg[5]_i_15_n_0 ),
        .I4(opcode[28]),
        .I5(\u3_control/is_syscall__2 ),
        .O(\lower_reg_reg[31]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair133" *) 
  LUT3 #(
    .INIT(8'h10)) 
    fifo_kernel_to_monitor_en_reg_i_1
       (.I0(complete_address_next[29]),
        .I1(complete_address_next[28]),
        .I2(complete_address_next[30]),
        .O(fifo_kernel_to_monitor_en));
  (* SOFT_HLUTNM = "soft_lutpair81" *) 
  LUT4 #(
    .INIT(16'h0008)) 
    \fifo_monitor_to_kernel_data_write_reg[31]_i_1 
       (.I0(complete_address_next[30]),
        .I1(complete_address_next[28]),
        .I2(complete_address_next[29]),
        .I3(complete_address_next[31]),
        .O(\fifo_monitor_to_kernel_data_write[31] ));
  (* SOFT_HLUTNM = "soft_lutpair86" *) 
  LUT3 #(
    .INIT(8'h40)) 
    fifo_monitor_to_kernel_en_reg_i_1
       (.I0(complete_address_next[29]),
        .I1(complete_address_next[28]),
        .I2(complete_address_next[30]),
        .O(fifo_monitor_to_kernel_en));
  LUT6 #(
    .INIT(64'h0000000000100000)) 
    fifo_ptm_en_reg_i_1
       (.I0(complete_address_next[30]),
        .I1(complete_address_next[28]),
        .I2(fifo_ptm_en_reg_i_2_n_0),
        .I3(\bram_firmware_code_bytes_selection[3] [0]),
        .I4(complete_address_next[29]),
        .I5(complete_address_next[31]),
        .O(fifo_ptm_en));
  LUT6 #(
    .INIT(64'hFFFFFFFFFF0FFF1F)) 
    fifo_ptm_en_reg_i_2
       (.I0(fifo_ptm_en_reg_i_3_n_0),
        .I1(fifo_ptm_en_reg_i_4_n_0),
        .I2(\opcode_reg[31]_i_3_n_0 ),
        .I3(pause_in0_out),
        .I4(fifo_ptm_en_reg_i_5_n_0),
        .I5(mem_state_reg_reg_n_0),
        .O(fifo_ptm_en_reg_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT5 #(
    .INIT(32'h40F05000)) 
    fifo_ptm_en_reg_i_3
       (.I0(\xilinx_16x1d.reg_loop[1].reg_bit1a_i_3_n_0 ),
        .I1(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_15_n_0 ),
        .I2(\bram_firmware_code_data_write_reg[31]_i_2_n_0 ),
        .I3(mem_source[3]),
        .I4(mem_source[2]),
        .O(fifo_ptm_en_reg_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT5 #(
    .INIT(32'h10F03000)) 
    fifo_ptm_en_reg_i_4
       (.I0(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_15_n_0 ),
        .I1(\xilinx_16x1d.reg_loop[1].reg_bit1a_i_3_n_0 ),
        .I2(\bram_firmware_code_data_write_reg[31]_i_2_n_0 ),
        .I3(mem_source[3]),
        .I4(mem_source[2]),
        .O(fifo_ptm_en_reg_i_4_n_0));
  LUT5 #(
    .INIT(32'h40F0C000)) 
    fifo_ptm_en_reg_i_5
       (.I0(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_15_n_0 ),
        .I1(\xilinx_16x1d.reg_loop[1].reg_bit1a_i_3_n_0 ),
        .I2(\bram_firmware_code_data_write_reg[31]_i_2_n_0 ),
        .I3(mem_source[3]),
        .I4(mem_source[2]),
        .O(fifo_ptm_en_reg_i_5_n_0));
  (* SOFT_HLUTNM = "soft_lutpair133" *) 
  LUT3 #(
    .INIT(8'h08)) 
    fifo_read_instrumentation_request_read_en_reg_i_1
       (.I0(complete_address_next[28]),
        .I1(complete_address_next[29]),
        .I2(complete_address_next[30]),
        .O(fifo_read_instrumentation_request_read_en));
  LUT5 #(
    .INIT(32'h10000000)) 
    intr_enable_reg_i_2
       (.I0(rd_indexD[4]),
        .I1(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_38_n_0 ),
        .I2(rd_indexD[5]),
        .I3(rd_indexD[2]),
        .I4(rd_indexD[3]),
        .O(intr_enable_reg_reg_2));
  LUT6 #(
    .INIT(64'hF111F1F1F1111111)) 
    \lower_reg[0]_i_1 
       (.I0(\lower_reg[31]_i_3_n_0 ),
        .I1(\lower_reg[0]_i_2_n_0 ),
        .I2(\lower_reg_reg[0] ),
        .I3(\lower_reg_reg[31]_5 [1]),
        .I4(mode_reg_reg),
        .I5(lower_reg0__62),
        .O(\lower_reg_reg[31]_3 [0]));
  (* SOFT_HLUTNM = "soft_lutpair79" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \lower_reg[0]_i_2 
       (.I0(\lower_reg_reg[31]_0 ),
        .I1(a_bus[0]),
        .O(\lower_reg[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hF111F1F1F1111111)) 
    \lower_reg[10]_i_1 
       (.I0(\lower_reg[31]_i_3_n_0 ),
        .I1(\lower_reg[10]_i_2_n_0 ),
        .I2(\lower_reg_reg[0] ),
        .I3(\lower_reg_reg[31]_5 [11]),
        .I4(mode_reg_reg),
        .I5(\lower_reg_reg[31]_5 [9]),
        .O(\lower_reg_reg[31]_3 [10]));
  (* SOFT_HLUTNM = "soft_lutpair162" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \lower_reg[10]_i_2 
       (.I0(\lower_reg_reg[31]_0 ),
        .I1(a_bus[10]),
        .O(\lower_reg[10]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hF111F1F1F1111111)) 
    \lower_reg[11]_i_1 
       (.I0(\lower_reg[31]_i_3_n_0 ),
        .I1(\lower_reg[11]_i_2_n_0 ),
        .I2(\lower_reg_reg[0] ),
        .I3(\lower_reg_reg[31]_5 [12]),
        .I4(mode_reg_reg),
        .I5(\lower_reg_reg[31]_5 [10]),
        .O(\lower_reg_reg[31]_3 [11]));
  (* SOFT_HLUTNM = "soft_lutpair152" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \lower_reg[11]_i_2 
       (.I0(\lower_reg_reg[31]_0 ),
        .I1(a_bus[11]),
        .O(\lower_reg[11]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hF111F1F1F1111111)) 
    \lower_reg[12]_i_1 
       (.I0(\lower_reg[31]_i_3_n_0 ),
        .I1(\lower_reg[12]_i_2_n_0 ),
        .I2(\lower_reg_reg[0] ),
        .I3(\lower_reg_reg[31]_5 [13]),
        .I4(mode_reg_reg),
        .I5(\lower_reg_reg[31]_5 [11]),
        .O(\lower_reg_reg[31]_3 [12]));
  (* SOFT_HLUTNM = "soft_lutpair151" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \lower_reg[12]_i_2 
       (.I0(\lower_reg_reg[31]_0 ),
        .I1(a_bus[12]),
        .O(\lower_reg[12]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hF111F1F1F1111111)) 
    \lower_reg[13]_i_1 
       (.I0(\lower_reg[31]_i_3_n_0 ),
        .I1(\lower_reg[13]_i_2_n_0 ),
        .I2(\lower_reg_reg[0] ),
        .I3(\lower_reg_reg[31]_5 [14]),
        .I4(mode_reg_reg),
        .I5(\lower_reg_reg[31]_5 [12]),
        .O(\lower_reg_reg[31]_3 [13]));
  (* SOFT_HLUTNM = "soft_lutpair160" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \lower_reg[13]_i_2 
       (.I0(\lower_reg_reg[31]_0 ),
        .I1(a_bus[13]),
        .O(\lower_reg[13]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hF111F1F1F1111111)) 
    \lower_reg[14]_i_1 
       (.I0(\lower_reg[31]_i_3_n_0 ),
        .I1(\lower_reg[14]_i_2_n_0 ),
        .I2(\lower_reg_reg[0] ),
        .I3(\lower_reg_reg[31]_5 [15]),
        .I4(mode_reg_reg),
        .I5(\lower_reg_reg[31]_5 [13]),
        .O(\lower_reg_reg[31]_3 [14]));
  (* SOFT_HLUTNM = "soft_lutpair163" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \lower_reg[14]_i_2 
       (.I0(\lower_reg_reg[31]_0 ),
        .I1(a_bus[14]),
        .O(\lower_reg[14]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hF111F1F1F1111111)) 
    \lower_reg[15]_i_1 
       (.I0(\lower_reg[31]_i_3_n_0 ),
        .I1(\lower_reg[15]_i_2_n_0 ),
        .I2(\lower_reg_reg[0] ),
        .I3(\lower_reg_reg[31]_5 [16]),
        .I4(mode_reg_reg),
        .I5(\lower_reg_reg[31]_5 [14]),
        .O(\lower_reg_reg[31]_3 [15]));
  (* SOFT_HLUTNM = "soft_lutpair162" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \lower_reg[15]_i_2 
       (.I0(\lower_reg_reg[31]_0 ),
        .I1(a_bus[15]),
        .O(\lower_reg[15]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hF111F1F1F1111111)) 
    \lower_reg[16]_i_1 
       (.I0(\lower_reg[31]_i_3_n_0 ),
        .I1(\lower_reg[16]_i_2_n_0 ),
        .I2(\lower_reg_reg[0] ),
        .I3(\lower_reg_reg[31]_5 [17]),
        .I4(mode_reg_reg),
        .I5(\lower_reg_reg[31]_5 [15]),
        .O(\lower_reg_reg[31]_3 [16]));
  (* SOFT_HLUTNM = "soft_lutpair160" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \lower_reg[16]_i_2 
       (.I0(\lower_reg_reg[31]_0 ),
        .I1(a_bus[16]),
        .O(\lower_reg[16]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hF111F1F1F1111111)) 
    \lower_reg[17]_i_1 
       (.I0(\lower_reg[31]_i_3_n_0 ),
        .I1(\lower_reg[17]_i_2_n_0 ),
        .I2(\lower_reg_reg[0] ),
        .I3(\lower_reg_reg[31]_5 [18]),
        .I4(mode_reg_reg),
        .I5(\lower_reg_reg[31]_5 [16]),
        .O(\lower_reg_reg[31]_3 [17]));
  (* SOFT_HLUTNM = "soft_lutpair159" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \lower_reg[17]_i_2 
       (.I0(\lower_reg_reg[31]_0 ),
        .I1(a_bus[17]),
        .O(\lower_reg[17]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hF111F1F1F1111111)) 
    \lower_reg[18]_i_1 
       (.I0(\lower_reg[31]_i_3_n_0 ),
        .I1(\lower_reg[18]_i_2_n_0 ),
        .I2(\lower_reg_reg[0] ),
        .I3(\lower_reg_reg[31]_5 [19]),
        .I4(mode_reg_reg),
        .I5(\lower_reg_reg[31]_5 [17]),
        .O(\lower_reg_reg[31]_3 [18]));
  (* SOFT_HLUTNM = "soft_lutpair157" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \lower_reg[18]_i_2 
       (.I0(\lower_reg_reg[31]_0 ),
        .I1(a_bus[18]),
        .O(\lower_reg[18]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hF111F1F1F1111111)) 
    \lower_reg[19]_i_1 
       (.I0(\lower_reg[31]_i_3_n_0 ),
        .I1(\lower_reg[19]_i_2_n_0 ),
        .I2(\lower_reg_reg[0] ),
        .I3(\lower_reg_reg[31]_5 [20]),
        .I4(mode_reg_reg),
        .I5(\lower_reg_reg[31]_5 [18]),
        .O(\lower_reg_reg[31]_3 [19]));
  (* SOFT_HLUTNM = "soft_lutpair159" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \lower_reg[19]_i_2 
       (.I0(\lower_reg_reg[31]_0 ),
        .I1(a_bus[19]),
        .O(\lower_reg[19]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hF111F1F1F1111111)) 
    \lower_reg[1]_i_1 
       (.I0(\lower_reg[31]_i_3_n_0 ),
        .I1(\lower_reg[1]_i_2_n_0 ),
        .I2(\lower_reg_reg[0] ),
        .I3(\lower_reg_reg[31]_5 [2]),
        .I4(mode_reg_reg),
        .I5(\lower_reg_reg[31]_5 [0]),
        .O(\lower_reg_reg[31]_3 [1]));
  (* SOFT_HLUTNM = "soft_lutpair165" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \lower_reg[1]_i_2 
       (.I0(\lower_reg_reg[31]_0 ),
        .I1(a_bus[1]),
        .O(\lower_reg[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hF111F1F1F1111111)) 
    \lower_reg[20]_i_1 
       (.I0(\lower_reg[31]_i_3_n_0 ),
        .I1(\lower_reg[20]_i_2_n_0 ),
        .I2(\lower_reg_reg[0] ),
        .I3(\lower_reg_reg[31]_5 [21]),
        .I4(mode_reg_reg),
        .I5(\lower_reg_reg[31]_5 [19]),
        .O(\lower_reg_reg[31]_3 [20]));
  (* SOFT_HLUTNM = "soft_lutpair149" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \lower_reg[20]_i_2 
       (.I0(\lower_reg_reg[31]_0 ),
        .I1(a_bus[20]),
        .O(\lower_reg[20]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hF111F1F1F1111111)) 
    \lower_reg[21]_i_1 
       (.I0(\lower_reg[31]_i_3_n_0 ),
        .I1(\lower_reg[21]_i_2_n_0 ),
        .I2(\lower_reg_reg[0] ),
        .I3(\lower_reg_reg[31]_5 [22]),
        .I4(mode_reg_reg),
        .I5(\lower_reg_reg[31]_5 [20]),
        .O(\lower_reg_reg[31]_3 [21]));
  (* SOFT_HLUTNM = "soft_lutpair154" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \lower_reg[21]_i_2 
       (.I0(\lower_reg_reg[31]_0 ),
        .I1(a_bus[21]),
        .O(\lower_reg[21]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hF111F1F1F1111111)) 
    \lower_reg[22]_i_1 
       (.I0(\lower_reg[31]_i_3_n_0 ),
        .I1(\lower_reg[22]_i_2_n_0 ),
        .I2(\lower_reg_reg[0] ),
        .I3(\lower_reg_reg[31]_5 [23]),
        .I4(mode_reg_reg),
        .I5(\lower_reg_reg[31]_5 [21]),
        .O(\lower_reg_reg[31]_3 [22]));
  (* SOFT_HLUTNM = "soft_lutpair158" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \lower_reg[22]_i_2 
       (.I0(\lower_reg_reg[31]_0 ),
        .I1(a_bus[22]),
        .O(\lower_reg[22]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hF111F1F1F1111111)) 
    \lower_reg[23]_i_1 
       (.I0(\lower_reg[31]_i_3_n_0 ),
        .I1(\lower_reg[23]_i_2_n_0 ),
        .I2(\lower_reg_reg[0] ),
        .I3(\lower_reg_reg[31]_5 [24]),
        .I4(mode_reg_reg),
        .I5(\lower_reg_reg[31]_5 [22]),
        .O(\lower_reg_reg[31]_3 [23]));
  (* SOFT_HLUTNM = "soft_lutpair157" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \lower_reg[23]_i_2 
       (.I0(\lower_reg_reg[31]_0 ),
        .I1(a_bus[23]),
        .O(\lower_reg[23]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hF111F1F1F1111111)) 
    \lower_reg[24]_i_1 
       (.I0(\lower_reg[31]_i_3_n_0 ),
        .I1(\lower_reg[24]_i_2_n_0 ),
        .I2(\lower_reg_reg[0] ),
        .I3(\lower_reg_reg[31]_5 [25]),
        .I4(mode_reg_reg),
        .I5(\lower_reg_reg[31]_5 [23]),
        .O(\lower_reg_reg[31]_3 [24]));
  (* SOFT_HLUTNM = "soft_lutpair154" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \lower_reg[24]_i_2 
       (.I0(\lower_reg_reg[31]_0 ),
        .I1(a_bus[24]),
        .O(\lower_reg[24]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hF111F1F1F1111111)) 
    \lower_reg[25]_i_1 
       (.I0(\lower_reg[31]_i_3_n_0 ),
        .I1(\lower_reg[25]_i_2_n_0 ),
        .I2(\lower_reg_reg[0] ),
        .I3(\lower_reg_reg[31]_5 [26]),
        .I4(mode_reg_reg),
        .I5(\lower_reg_reg[31]_5 [24]),
        .O(\lower_reg_reg[31]_3 [25]));
  (* SOFT_HLUTNM = "soft_lutpair127" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \lower_reg[25]_i_2 
       (.I0(\lower_reg_reg[31]_0 ),
        .I1(a_bus[25]),
        .O(\lower_reg[25]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hF111F1F1F1111111)) 
    \lower_reg[26]_i_1 
       (.I0(\lower_reg[31]_i_3_n_0 ),
        .I1(\lower_reg[26]_i_2_n_0 ),
        .I2(\lower_reg_reg[0] ),
        .I3(\lower_reg_reg[31]_5 [27]),
        .I4(mode_reg_reg),
        .I5(\lower_reg_reg[31]_5 [25]),
        .O(\lower_reg_reg[31]_3 [26]));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \lower_reg[26]_i_2 
       (.I0(\lower_reg_reg[31]_0 ),
        .I1(a_bus[26]),
        .O(\lower_reg[26]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hF111F1F1F1111111)) 
    \lower_reg[27]_i_1 
       (.I0(\lower_reg[31]_i_3_n_0 ),
        .I1(\lower_reg[27]_i_2_n_0 ),
        .I2(\lower_reg_reg[0] ),
        .I3(\lower_reg_reg[31]_5 [28]),
        .I4(mode_reg_reg),
        .I5(\lower_reg_reg[31]_5 [26]),
        .O(\lower_reg_reg[31]_3 [27]));
  (* SOFT_HLUTNM = "soft_lutpair152" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \lower_reg[27]_i_2 
       (.I0(\lower_reg_reg[31]_0 ),
        .I1(a_bus[27]),
        .O(\lower_reg[27]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hF111F1F1F1111111)) 
    \lower_reg[28]_i_1 
       (.I0(\lower_reg[31]_i_3_n_0 ),
        .I1(\lower_reg[28]_i_2_n_0 ),
        .I2(\lower_reg_reg[0] ),
        .I3(\lower_reg_reg[31]_5 [29]),
        .I4(mode_reg_reg),
        .I5(\lower_reg_reg[31]_5 [27]),
        .O(\lower_reg_reg[31]_3 [28]));
  (* SOFT_HLUTNM = "soft_lutpair151" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \lower_reg[28]_i_2 
       (.I0(\lower_reg_reg[31]_0 ),
        .I1(a_bus[28]),
        .O(\lower_reg[28]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hF111F1F1F1111111)) 
    \lower_reg[29]_i_1 
       (.I0(\lower_reg[31]_i_3_n_0 ),
        .I1(\lower_reg[29]_i_2_n_0 ),
        .I2(\lower_reg_reg[0] ),
        .I3(\lower_reg_reg[31]_5 [30]),
        .I4(mode_reg_reg),
        .I5(\lower_reg_reg[31]_5 [28]),
        .O(\lower_reg_reg[31]_3 [29]));
  (* SOFT_HLUTNM = "soft_lutpair149" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \lower_reg[29]_i_2 
       (.I0(\lower_reg_reg[31]_0 ),
        .I1(a_bus[29]),
        .O(\lower_reg[29]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hF111F1F1F1111111)) 
    \lower_reg[2]_i_1 
       (.I0(\lower_reg[31]_i_3_n_0 ),
        .I1(\lower_reg[2]_i_2_n_0 ),
        .I2(\lower_reg_reg[0] ),
        .I3(\lower_reg_reg[31]_5 [3]),
        .I4(mode_reg_reg),
        .I5(\lower_reg_reg[31]_5 [1]),
        .O(\lower_reg_reg[31]_3 [2]));
  (* SOFT_HLUTNM = "soft_lutpair110" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \lower_reg[2]_i_2 
       (.I0(\lower_reg_reg[31]_0 ),
        .I1(a_bus[2]),
        .O(\lower_reg[2]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hF111F1F1F1111111)) 
    \lower_reg[30]_i_1 
       (.I0(\lower_reg[31]_i_3_n_0 ),
        .I1(\lower_reg[30]_i_2_n_0 ),
        .I2(\lower_reg_reg[0] ),
        .I3(\lower_reg_reg[31]_5 [31]),
        .I4(mode_reg_reg),
        .I5(\lower_reg_reg[31]_5 [29]),
        .O(\lower_reg_reg[31]_3 [30]));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \lower_reg[30]_i_2 
       (.I0(\lower_reg_reg[31]_0 ),
        .I1(a_bus[30]),
        .O(\lower_reg[30]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT5 #(
    .INIT(32'hC5C4C484)) 
    \lower_reg[31]_i_1 
       (.I0(\lower_reg_reg[31]_2 ),
        .I1(neqOp0_in),
        .I2(\lower_reg_reg[31]_1 ),
        .I3(\lower_reg_reg[31] ),
        .I4(\lower_reg_reg[31]_0 ),
        .O(\lower_reg_reg[31]_4 ));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT4 #(
    .INIT(16'hFF40)) 
    \lower_reg[31]_i_2 
       (.I0(\lower_reg[31]_i_3_n_0 ),
        .I1(\lower_reg_reg[31]_0 ),
        .I2(a_bus[31]),
        .I3(\lower_reg_reg[30] ),
        .O(\lower_reg_reg[31]_3 [31]));
  LUT2 #(
    .INIT(4'hB)) 
    \lower_reg[31]_i_3 
       (.I0(\lower_reg_reg[31]_1 ),
        .I1(\lower_reg_reg[31] ),
        .O(\lower_reg[31]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h88B8)) 
    \lower_reg[31]_i_4 
       (.I0(pc_current[31]),
        .I1(\upper_reg[0]_i_5_n_0 ),
        .I2(reg_source[31]),
        .I3(\upper_reg[0]_i_4_n_0 ),
        .O(a_bus[31]));
  LUT6 #(
    .INIT(64'hF111F1F1F1111111)) 
    \lower_reg[3]_i_1 
       (.I0(\lower_reg[31]_i_3_n_0 ),
        .I1(\lower_reg[3]_i_2_n_0 ),
        .I2(\lower_reg_reg[0] ),
        .I3(\lower_reg_reg[31]_5 [4]),
        .I4(mode_reg_reg),
        .I5(\lower_reg_reg[31]_5 [2]),
        .O(\lower_reg_reg[31]_3 [3]));
  (* SOFT_HLUTNM = "soft_lutpair104" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \lower_reg[3]_i_2 
       (.I0(\lower_reg_reg[31]_0 ),
        .I1(a_bus[3]),
        .O(\lower_reg[3]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hF111F1F1F1111111)) 
    \lower_reg[4]_i_1 
       (.I0(\lower_reg[31]_i_3_n_0 ),
        .I1(\lower_reg[4]_i_2_n_0 ),
        .I2(\lower_reg_reg[0] ),
        .I3(\lower_reg_reg[31]_5 [5]),
        .I4(mode_reg_reg),
        .I5(\lower_reg_reg[31]_5 [3]),
        .O(\lower_reg_reg[31]_3 [4]));
  (* SOFT_HLUTNM = "soft_lutpair88" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \lower_reg[4]_i_2 
       (.I0(\lower_reg_reg[31]_0 ),
        .I1(a_bus[4]),
        .O(\lower_reg[4]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hF111F1F1F1111111)) 
    \lower_reg[5]_i_1 
       (.I0(\lower_reg[31]_i_3_n_0 ),
        .I1(\lower_reg[5]_i_2_n_0 ),
        .I2(\lower_reg_reg[0] ),
        .I3(\lower_reg_reg[31]_5 [6]),
        .I4(mode_reg_reg),
        .I5(\lower_reg_reg[31]_5 [4]),
        .O(\lower_reg_reg[31]_3 [5]));
  (* SOFT_HLUTNM = "soft_lutpair163" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \lower_reg[5]_i_2 
       (.I0(\lower_reg_reg[31]_0 ),
        .I1(a_bus[5]),
        .O(\lower_reg[5]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hF111F1F1F1111111)) 
    \lower_reg[6]_i_1 
       (.I0(\lower_reg[31]_i_3_n_0 ),
        .I1(\lower_reg[6]_i_2_n_0 ),
        .I2(\lower_reg_reg[0] ),
        .I3(\lower_reg_reg[31]_5 [7]),
        .I4(mode_reg_reg),
        .I5(\lower_reg_reg[31]_5 [5]),
        .O(\lower_reg_reg[31]_3 [6]));
  (* SOFT_HLUTNM = "soft_lutpair158" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \lower_reg[6]_i_2 
       (.I0(\lower_reg_reg[31]_0 ),
        .I1(a_bus[6]),
        .O(\lower_reg[6]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hF111F1F1F1111111)) 
    \lower_reg[7]_i_1 
       (.I0(\lower_reg[31]_i_3_n_0 ),
        .I1(\lower_reg[7]_i_2_n_0 ),
        .I2(\lower_reg_reg[0] ),
        .I3(\lower_reg_reg[31]_5 [8]),
        .I4(mode_reg_reg),
        .I5(\lower_reg_reg[31]_5 [6]),
        .O(\lower_reg_reg[31]_3 [7]));
  (* SOFT_HLUTNM = "soft_lutpair164" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \lower_reg[7]_i_2 
       (.I0(\lower_reg_reg[31]_0 ),
        .I1(a_bus[7]),
        .O(\lower_reg[7]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hF111F1F1F1111111)) 
    \lower_reg[8]_i_1 
       (.I0(\lower_reg[31]_i_3_n_0 ),
        .I1(\lower_reg[8]_i_2_n_0 ),
        .I2(\lower_reg_reg[0] ),
        .I3(\lower_reg_reg[31]_5 [9]),
        .I4(mode_reg_reg),
        .I5(\lower_reg_reg[31]_5 [7]),
        .O(\lower_reg_reg[31]_3 [8]));
  (* SOFT_HLUTNM = "soft_lutpair165" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \lower_reg[8]_i_2 
       (.I0(\lower_reg_reg[31]_0 ),
        .I1(a_bus[8]),
        .O(\lower_reg[8]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hF111F1F1F1111111)) 
    \lower_reg[9]_i_1 
       (.I0(\lower_reg[31]_i_3_n_0 ),
        .I1(\lower_reg[9]_i_2_n_0 ),
        .I2(\lower_reg_reg[0] ),
        .I3(\lower_reg_reg[31]_5 [10]),
        .I4(mode_reg_reg),
        .I5(\lower_reg_reg[31]_5 [8]),
        .O(\lower_reg_reg[31]_3 [9]));
  (* SOFT_HLUTNM = "soft_lutpair164" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \lower_reg[9]_i_2 
       (.I0(\lower_reg_reg[31]_0 ),
        .I1(a_bus[9]),
        .O(\lower_reg[9]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    mem_state_reg_i_1
       (.I0(pause_in0_out),
        .O(mem_state_reg));
  LUT5 #(
    .INIT(32'h55555554)) 
    mem_state_reg_i_2
       (.I0(mem_state_reg_reg_n_0),
        .I1(mem_source[1]),
        .I2(mem_source[0]),
        .I3(mem_source[2]),
        .I4(mem_source[3]),
        .O(mem_state_reg_i_2_n_0));
  FDCE mem_state_reg_reg
       (.C(clk),
        .CE(mem_state_reg),
        .CLR(\address_reg_reg[24]_0 ),
        .D(mem_state_reg_i_2_n_0),
        .Q(mem_state_reg_reg_n_0));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT2 #(
    .INIT(4'h6)) 
    negate_reg_i_2
       (.I0(b_bus[31]),
        .I1(a_bus[31]),
        .O(negate_reg_reg));
  LUT5 #(
    .INIT(32'h0DFF0D00)) 
    \next_opcode_reg[0]_i_1 
       (.I0(\next_opcode_reg[0]_i_2_n_0 ),
        .I1(\next_opcode_reg[0]_i_3_n_0 ),
        .I2(cpu_address_selection_of_memory_area[2]),
        .I3(cpu_address_selection_of_memory_area[3]),
        .I4(\next_opcode_reg[0]_i_4_n_0 ),
        .O(cpu_data_r[0]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \next_opcode_reg[0]_i_10 
       (.I0(fifo_read_instrumentation_data_read[0]),
        .I1(fifo_ptm_data_read[0]),
        .I2(cpu_address_selection_of_memory_area[1]),
        .I3(bram_bbt_annotations_data_read[0]),
        .I4(cpu_address_selection_of_memory_area[0]),
        .I5(bram_firmware_code_data_read[0]),
        .O(\next_opcode_reg[0]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'h00000000F8A85808)) 
    \next_opcode_reg[0]_i_11 
       (.I0(cpu_address_misc_signals_selection_of_memory[0]),
        .I1(fifo_ptm_empty),
        .I2(cpu_address_misc_signals_selection_of_memory[1]),
        .I3(fifo_ptm_almost_empty),
        .I4(fifo_ptm_full),
        .I5(cpu_address_misc_signals_selection_of_memory[2]),
        .O(\next_opcode_reg[0]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hAABFAFBFFABFFFBF)) 
    \next_opcode_reg[0]_i_12 
       (.I0(cpu_address_misc_signals_selection_of_memory[2]),
        .I1(fifo_kernel_to_monitor_empty),
        .I2(cpu_address_misc_signals_selection_of_memory[0]),
        .I3(cpu_address_misc_signals_selection_of_memory[1]),
        .I4(fifo_kernel_to_monitor_almost_empty),
        .I5(fifo_kernel_to_monitor_full),
        .O(\next_opcode_reg[0]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAABFAFBFFABFFFBF)) 
    \next_opcode_reg[0]_i_13 
       (.I0(cpu_address_misc_signals_selection_of_memory[2]),
        .I1(fifo_instrumentation_empty),
        .I2(cpu_address_misc_signals_selection_of_memory[0]),
        .I3(cpu_address_misc_signals_selection_of_memory[1]),
        .I4(fifo_instrumentation_almost_empty),
        .I5(fifo_instrumentation_full),
        .O(\next_opcode_reg[0]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hAABFAFBFFABFFFBF)) 
    \next_opcode_reg[0]_i_14 
       (.I0(cpu_address_misc_signals_selection_of_memory[2]),
        .I1(fifo_monitor_to_kernel_empty),
        .I2(cpu_address_misc_signals_selection_of_memory[0]),
        .I3(cpu_address_misc_signals_selection_of_memory[1]),
        .I4(fifo_monitor_to_kernel_almost_empty),
        .I5(fifo_monitor_to_kernel_full),
        .O(\next_opcode_reg[0]_i_14_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFD0DFDFD)) 
    \next_opcode_reg[0]_i_2 
       (.I0(\next_opcode_reg[0]_i_5_n_0 ),
        .I1(\next_opcode_reg[0]_i_6_n_0 ),
        .I2(cpu_address_misc_ip_selection_of_memory[0]),
        .I3(\next_opcode_reg[0]_i_7_n_0 ),
        .I4(\next_opcode_reg[0]_i_8_n_0 ),
        .I5(\next_opcode_reg[0]_i_9_n_0 ),
        .O(\next_opcode_reg[0]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h3808)) 
    \next_opcode_reg[0]_i_3 
       (.I0(bram_sec_policy_config_data_read[0]),
        .I1(cpu_address_selection_of_memory_area[1]),
        .I2(cpu_address_selection_of_memory_area[0]),
        .I3(bram_debug_data_read[0]),
        .O(\next_opcode_reg[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \next_opcode_reg[0]_i_4 
       (.I0(PLtoPS_readed_data[0]),
        .I1(cpu_address_selection_of_memory_area[1]),
        .I2(fifo_kernel_to_monitor_data_read[0]),
        .I3(cpu_address_selection_of_memory_area[0]),
        .I4(cpu_address_selection_of_memory_area[2]),
        .I5(\next_opcode_reg[0]_i_10_n_0 ),
        .O(\next_opcode_reg[0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hBBBBBBBBBFBBBBBB)) 
    \next_opcode_reg[0]_i_5 
       (.I0(\next_opcode_reg[0]_i_11_n_0 ),
        .I1(cpu_address_misc_ip_selection_of_memory[1]),
        .I2(cpu_address_misc_signals_selection_of_memory[1]),
        .I3(cpu_address_misc_signals_selection_of_memory[2]),
        .I4(fifo_ptm_almost_full),
        .I5(cpu_address_misc_signals_selection_of_memory[0]),
        .O(\next_opcode_reg[0]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000AAAA8AAA)) 
    \next_opcode_reg[0]_i_6 
       (.I0(\next_opcode_reg[0]_i_12_n_0 ),
        .I1(cpu_address_misc_signals_selection_of_memory[1]),
        .I2(cpu_address_misc_signals_selection_of_memory[2]),
        .I3(fifo_kernel_to_monitor_almost_full),
        .I4(cpu_address_misc_signals_selection_of_memory[0]),
        .I5(cpu_address_misc_ip_selection_of_memory[1]),
        .O(\next_opcode_reg[0]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h8888888880888888)) 
    \next_opcode_reg[0]_i_7 
       (.I0(\next_opcode_reg[0]_i_13_n_0 ),
        .I1(cpu_address_misc_ip_selection_of_memory[1]),
        .I2(cpu_address_misc_signals_selection_of_memory[1]),
        .I3(cpu_address_misc_signals_selection_of_memory[2]),
        .I4(fifo_instrumentation_almost_full),
        .I5(cpu_address_misc_signals_selection_of_memory[0]),
        .O(\next_opcode_reg[0]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF0040FFFFFFFF)) 
    \next_opcode_reg[0]_i_8 
       (.I0(cpu_address_misc_signals_selection_of_memory[1]),
        .I1(cpu_address_misc_signals_selection_of_memory[2]),
        .I2(fifo_monitor_to_kernel_almost_full),
        .I3(cpu_address_misc_signals_selection_of_memory[0]),
        .I4(cpu_address_misc_ip_selection_of_memory[1]),
        .I5(\next_opcode_reg[0]_i_14_n_0 ),
        .O(\next_opcode_reg[0]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFF9)) 
    \next_opcode_reg[0]_i_9 
       (.I0(cpu_address_misc_ip_selection_of_memory[1]),
        .I1(cpu_address_misc_ip_selection_of_memory[2]),
        .I2(cpu_address_misc_ip_selection_of_memory[3]),
        .I3(cpu_address_selection_of_memory_area[0]),
        .I4(cpu_address_misc_signals_selection_of_memory[3]),
        .I5(cpu_address_selection_of_memory_area[1]),
        .O(\next_opcode_reg[0]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \next_opcode_reg[10]_i_2 
       (.I0(PLtoPS_readed_data[10]),
        .I1(cpu_address_selection_of_memory_area[1]),
        .I2(fifo_kernel_to_monitor_data_read[10]),
        .I3(cpu_address_selection_of_memory_area[0]),
        .I4(cpu_address_selection_of_memory_area[2]),
        .I5(\next_opcode_reg[10]_i_4_n_0 ),
        .O(\next_opcode_reg[10]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h05400040)) 
    \next_opcode_reg[10]_i_3 
       (.I0(cpu_address_selection_of_memory_area[2]),
        .I1(bram_debug_data_read[10]),
        .I2(cpu_address_selection_of_memory_area[0]),
        .I3(cpu_address_selection_of_memory_area[1]),
        .I4(bram_sec_policy_config_data_read[10]),
        .O(\next_opcode_reg[10]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \next_opcode_reg[10]_i_4 
       (.I0(fifo_read_instrumentation_data_read[10]),
        .I1(fifo_ptm_data_read[10]),
        .I2(cpu_address_selection_of_memory_area[1]),
        .I3(bram_bbt_annotations_data_read[10]),
        .I4(cpu_address_selection_of_memory_area[0]),
        .I5(bram_firmware_code_data_read[10]),
        .O(\next_opcode_reg[10]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \next_opcode_reg[11]_i_2 
       (.I0(PLtoPS_readed_data[11]),
        .I1(cpu_address_selection_of_memory_area[1]),
        .I2(fifo_kernel_to_monitor_data_read[11]),
        .I3(cpu_address_selection_of_memory_area[0]),
        .I4(cpu_address_selection_of_memory_area[2]),
        .I5(\next_opcode_reg[11]_i_4_n_0 ),
        .O(\next_opcode_reg[11]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h05400040)) 
    \next_opcode_reg[11]_i_3 
       (.I0(cpu_address_selection_of_memory_area[2]),
        .I1(bram_debug_data_read[11]),
        .I2(cpu_address_selection_of_memory_area[0]),
        .I3(cpu_address_selection_of_memory_area[1]),
        .I4(bram_sec_policy_config_data_read[11]),
        .O(\next_opcode_reg[11]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \next_opcode_reg[11]_i_4 
       (.I0(fifo_read_instrumentation_data_read[11]),
        .I1(fifo_ptm_data_read[11]),
        .I2(cpu_address_selection_of_memory_area[1]),
        .I3(bram_bbt_annotations_data_read[11]),
        .I4(cpu_address_selection_of_memory_area[0]),
        .I5(bram_firmware_code_data_read[11]),
        .O(\next_opcode_reg[11]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \next_opcode_reg[12]_i_2 
       (.I0(PLtoPS_readed_data[12]),
        .I1(cpu_address_selection_of_memory_area[1]),
        .I2(fifo_kernel_to_monitor_data_read[12]),
        .I3(cpu_address_selection_of_memory_area[0]),
        .I4(cpu_address_selection_of_memory_area[2]),
        .I5(\next_opcode_reg[12]_i_4_n_0 ),
        .O(\next_opcode_reg[12]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h05400040)) 
    \next_opcode_reg[12]_i_3 
       (.I0(cpu_address_selection_of_memory_area[2]),
        .I1(bram_debug_data_read[12]),
        .I2(cpu_address_selection_of_memory_area[0]),
        .I3(cpu_address_selection_of_memory_area[1]),
        .I4(bram_sec_policy_config_data_read[12]),
        .O(\next_opcode_reg[12]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \next_opcode_reg[12]_i_4 
       (.I0(fifo_read_instrumentation_data_read[12]),
        .I1(fifo_ptm_data_read[12]),
        .I2(cpu_address_selection_of_memory_area[1]),
        .I3(bram_bbt_annotations_data_read[12]),
        .I4(cpu_address_selection_of_memory_area[0]),
        .I5(bram_firmware_code_data_read[12]),
        .O(\next_opcode_reg[12]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \next_opcode_reg[13]_i_2 
       (.I0(PLtoPS_readed_data[13]),
        .I1(cpu_address_selection_of_memory_area[1]),
        .I2(fifo_kernel_to_monitor_data_read[13]),
        .I3(cpu_address_selection_of_memory_area[0]),
        .I4(cpu_address_selection_of_memory_area[2]),
        .I5(\next_opcode_reg[13]_i_4_n_0 ),
        .O(\next_opcode_reg[13]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h05400040)) 
    \next_opcode_reg[13]_i_3 
       (.I0(cpu_address_selection_of_memory_area[2]),
        .I1(bram_debug_data_read[13]),
        .I2(cpu_address_selection_of_memory_area[0]),
        .I3(cpu_address_selection_of_memory_area[1]),
        .I4(bram_sec_policy_config_data_read[13]),
        .O(\next_opcode_reg[13]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \next_opcode_reg[13]_i_4 
       (.I0(fifo_read_instrumentation_data_read[13]),
        .I1(fifo_ptm_data_read[13]),
        .I2(cpu_address_selection_of_memory_area[1]),
        .I3(bram_bbt_annotations_data_read[13]),
        .I4(cpu_address_selection_of_memory_area[0]),
        .I5(bram_firmware_code_data_read[13]),
        .O(\next_opcode_reg[13]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \next_opcode_reg[14]_i_2 
       (.I0(PLtoPS_readed_data[14]),
        .I1(cpu_address_selection_of_memory_area[1]),
        .I2(fifo_kernel_to_monitor_data_read[14]),
        .I3(cpu_address_selection_of_memory_area[0]),
        .I4(cpu_address_selection_of_memory_area[2]),
        .I5(\next_opcode_reg[14]_i_4_n_0 ),
        .O(\next_opcode_reg[14]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h05400040)) 
    \next_opcode_reg[14]_i_3 
       (.I0(cpu_address_selection_of_memory_area[2]),
        .I1(bram_debug_data_read[14]),
        .I2(cpu_address_selection_of_memory_area[0]),
        .I3(cpu_address_selection_of_memory_area[1]),
        .I4(bram_sec_policy_config_data_read[14]),
        .O(\next_opcode_reg[14]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \next_opcode_reg[14]_i_4 
       (.I0(fifo_read_instrumentation_data_read[14]),
        .I1(fifo_ptm_data_read[14]),
        .I2(cpu_address_selection_of_memory_area[1]),
        .I3(bram_bbt_annotations_data_read[14]),
        .I4(cpu_address_selection_of_memory_area[0]),
        .I5(bram_firmware_code_data_read[14]),
        .O(\next_opcode_reg[14]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \next_opcode_reg[15]_i_2 
       (.I0(PLtoPS_readed_data[15]),
        .I1(cpu_address_selection_of_memory_area[1]),
        .I2(fifo_kernel_to_monitor_data_read[15]),
        .I3(cpu_address_selection_of_memory_area[0]),
        .I4(cpu_address_selection_of_memory_area[2]),
        .I5(\next_opcode_reg[15]_i_4_n_0 ),
        .O(\next_opcode_reg[15]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h05400040)) 
    \next_opcode_reg[15]_i_3 
       (.I0(cpu_address_selection_of_memory_area[2]),
        .I1(bram_debug_data_read[15]),
        .I2(cpu_address_selection_of_memory_area[0]),
        .I3(cpu_address_selection_of_memory_area[1]),
        .I4(bram_sec_policy_config_data_read[15]),
        .O(\next_opcode_reg[15]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \next_opcode_reg[15]_i_4 
       (.I0(fifo_read_instrumentation_data_read[15]),
        .I1(fifo_ptm_data_read[15]),
        .I2(cpu_address_selection_of_memory_area[1]),
        .I3(bram_bbt_annotations_data_read[15]),
        .I4(cpu_address_selection_of_memory_area[0]),
        .I5(bram_firmware_code_data_read[15]),
        .O(\next_opcode_reg[15]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \next_opcode_reg[16]_i_2 
       (.I0(PLtoPS_readed_data[16]),
        .I1(cpu_address_selection_of_memory_area[1]),
        .I2(fifo_kernel_to_monitor_data_read[16]),
        .I3(cpu_address_selection_of_memory_area[0]),
        .I4(cpu_address_selection_of_memory_area[2]),
        .I5(\next_opcode_reg[16]_i_4_n_0 ),
        .O(\next_opcode_reg[16]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h05400040)) 
    \next_opcode_reg[16]_i_3 
       (.I0(cpu_address_selection_of_memory_area[2]),
        .I1(bram_debug_data_read[16]),
        .I2(cpu_address_selection_of_memory_area[0]),
        .I3(cpu_address_selection_of_memory_area[1]),
        .I4(bram_sec_policy_config_data_read[16]),
        .O(\next_opcode_reg[16]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \next_opcode_reg[16]_i_4 
       (.I0(fifo_read_instrumentation_data_read[16]),
        .I1(fifo_ptm_data_read[16]),
        .I2(cpu_address_selection_of_memory_area[1]),
        .I3(bram_bbt_annotations_data_read[16]),
        .I4(cpu_address_selection_of_memory_area[0]),
        .I5(bram_firmware_code_data_read[16]),
        .O(\next_opcode_reg[16]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \next_opcode_reg[17]_i_2 
       (.I0(PLtoPS_readed_data[17]),
        .I1(cpu_address_selection_of_memory_area[1]),
        .I2(fifo_kernel_to_monitor_data_read[17]),
        .I3(cpu_address_selection_of_memory_area[0]),
        .I4(cpu_address_selection_of_memory_area[2]),
        .I5(\next_opcode_reg[17]_i_4_n_0 ),
        .O(\next_opcode_reg[17]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h05400040)) 
    \next_opcode_reg[17]_i_3 
       (.I0(cpu_address_selection_of_memory_area[2]),
        .I1(bram_debug_data_read[17]),
        .I2(cpu_address_selection_of_memory_area[0]),
        .I3(cpu_address_selection_of_memory_area[1]),
        .I4(bram_sec_policy_config_data_read[17]),
        .O(\next_opcode_reg[17]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \next_opcode_reg[17]_i_4 
       (.I0(fifo_read_instrumentation_data_read[17]),
        .I1(fifo_ptm_data_read[17]),
        .I2(cpu_address_selection_of_memory_area[1]),
        .I3(bram_bbt_annotations_data_read[17]),
        .I4(cpu_address_selection_of_memory_area[0]),
        .I5(bram_firmware_code_data_read[17]),
        .O(\next_opcode_reg[17]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \next_opcode_reg[18]_i_2 
       (.I0(PLtoPS_readed_data[18]),
        .I1(cpu_address_selection_of_memory_area[1]),
        .I2(fifo_kernel_to_monitor_data_read[18]),
        .I3(cpu_address_selection_of_memory_area[0]),
        .I4(cpu_address_selection_of_memory_area[2]),
        .I5(\next_opcode_reg[18]_i_4_n_0 ),
        .O(\next_opcode_reg[18]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h05400040)) 
    \next_opcode_reg[18]_i_3 
       (.I0(cpu_address_selection_of_memory_area[2]),
        .I1(bram_debug_data_read[18]),
        .I2(cpu_address_selection_of_memory_area[0]),
        .I3(cpu_address_selection_of_memory_area[1]),
        .I4(bram_sec_policy_config_data_read[18]),
        .O(\next_opcode_reg[18]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \next_opcode_reg[18]_i_4 
       (.I0(fifo_read_instrumentation_data_read[18]),
        .I1(fifo_ptm_data_read[18]),
        .I2(cpu_address_selection_of_memory_area[1]),
        .I3(bram_bbt_annotations_data_read[18]),
        .I4(cpu_address_selection_of_memory_area[0]),
        .I5(bram_firmware_code_data_read[18]),
        .O(\next_opcode_reg[18]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \next_opcode_reg[19]_i_2 
       (.I0(PLtoPS_readed_data[19]),
        .I1(cpu_address_selection_of_memory_area[1]),
        .I2(fifo_kernel_to_monitor_data_read[19]),
        .I3(cpu_address_selection_of_memory_area[0]),
        .I4(cpu_address_selection_of_memory_area[2]),
        .I5(\next_opcode_reg[19]_i_4_n_0 ),
        .O(\next_opcode_reg[19]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h05400040)) 
    \next_opcode_reg[19]_i_3 
       (.I0(cpu_address_selection_of_memory_area[2]),
        .I1(bram_debug_data_read[19]),
        .I2(cpu_address_selection_of_memory_area[0]),
        .I3(cpu_address_selection_of_memory_area[1]),
        .I4(bram_sec_policy_config_data_read[19]),
        .O(\next_opcode_reg[19]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \next_opcode_reg[19]_i_4 
       (.I0(fifo_read_instrumentation_data_read[19]),
        .I1(fifo_ptm_data_read[19]),
        .I2(cpu_address_selection_of_memory_area[1]),
        .I3(bram_bbt_annotations_data_read[19]),
        .I4(cpu_address_selection_of_memory_area[0]),
        .I5(bram_firmware_code_data_read[19]),
        .O(\next_opcode_reg[19]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \next_opcode_reg[1]_i_2 
       (.I0(PLtoPS_readed_data[1]),
        .I1(cpu_address_selection_of_memory_area[1]),
        .I2(fifo_kernel_to_monitor_data_read[1]),
        .I3(cpu_address_selection_of_memory_area[0]),
        .I4(cpu_address_selection_of_memory_area[2]),
        .I5(\next_opcode_reg[1]_i_4_n_0 ),
        .O(\next_opcode_reg[1]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h05400040)) 
    \next_opcode_reg[1]_i_3 
       (.I0(cpu_address_selection_of_memory_area[2]),
        .I1(bram_debug_data_read[1]),
        .I2(cpu_address_selection_of_memory_area[0]),
        .I3(cpu_address_selection_of_memory_area[1]),
        .I4(bram_sec_policy_config_data_read[1]),
        .O(\next_opcode_reg[1]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \next_opcode_reg[1]_i_4 
       (.I0(fifo_read_instrumentation_data_read[1]),
        .I1(fifo_ptm_data_read[1]),
        .I2(cpu_address_selection_of_memory_area[1]),
        .I3(bram_bbt_annotations_data_read[1]),
        .I4(cpu_address_selection_of_memory_area[0]),
        .I5(bram_firmware_code_data_read[1]),
        .O(\next_opcode_reg[1]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \next_opcode_reg[20]_i_2 
       (.I0(PLtoPS_readed_data[20]),
        .I1(cpu_address_selection_of_memory_area[1]),
        .I2(fifo_kernel_to_monitor_data_read[20]),
        .I3(cpu_address_selection_of_memory_area[0]),
        .I4(cpu_address_selection_of_memory_area[2]),
        .I5(\next_opcode_reg[20]_i_4_n_0 ),
        .O(\next_opcode_reg[20]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h05400040)) 
    \next_opcode_reg[20]_i_3 
       (.I0(cpu_address_selection_of_memory_area[2]),
        .I1(bram_debug_data_read[20]),
        .I2(cpu_address_selection_of_memory_area[0]),
        .I3(cpu_address_selection_of_memory_area[1]),
        .I4(bram_sec_policy_config_data_read[20]),
        .O(\next_opcode_reg[20]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \next_opcode_reg[20]_i_4 
       (.I0(fifo_read_instrumentation_data_read[20]),
        .I1(fifo_ptm_data_read[20]),
        .I2(cpu_address_selection_of_memory_area[1]),
        .I3(bram_bbt_annotations_data_read[20]),
        .I4(cpu_address_selection_of_memory_area[0]),
        .I5(bram_firmware_code_data_read[20]),
        .O(\next_opcode_reg[20]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \next_opcode_reg[21]_i_2 
       (.I0(PLtoPS_readed_data[21]),
        .I1(cpu_address_selection_of_memory_area[1]),
        .I2(fifo_kernel_to_monitor_data_read[21]),
        .I3(cpu_address_selection_of_memory_area[0]),
        .I4(cpu_address_selection_of_memory_area[2]),
        .I5(\next_opcode_reg[21]_i_4_n_0 ),
        .O(\next_opcode_reg[21]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h05400040)) 
    \next_opcode_reg[21]_i_3 
       (.I0(cpu_address_selection_of_memory_area[2]),
        .I1(bram_debug_data_read[21]),
        .I2(cpu_address_selection_of_memory_area[0]),
        .I3(cpu_address_selection_of_memory_area[1]),
        .I4(bram_sec_policy_config_data_read[21]),
        .O(\next_opcode_reg[21]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \next_opcode_reg[21]_i_4 
       (.I0(fifo_read_instrumentation_data_read[21]),
        .I1(fifo_ptm_data_read[21]),
        .I2(cpu_address_selection_of_memory_area[1]),
        .I3(bram_bbt_annotations_data_read[21]),
        .I4(cpu_address_selection_of_memory_area[0]),
        .I5(bram_firmware_code_data_read[21]),
        .O(\next_opcode_reg[21]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \next_opcode_reg[22]_i_2 
       (.I0(PLtoPS_readed_data[22]),
        .I1(cpu_address_selection_of_memory_area[1]),
        .I2(fifo_kernel_to_monitor_data_read[22]),
        .I3(cpu_address_selection_of_memory_area[0]),
        .I4(cpu_address_selection_of_memory_area[2]),
        .I5(\next_opcode_reg[22]_i_4_n_0 ),
        .O(\next_opcode_reg[22]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h05400040)) 
    \next_opcode_reg[22]_i_3 
       (.I0(cpu_address_selection_of_memory_area[2]),
        .I1(bram_debug_data_read[22]),
        .I2(cpu_address_selection_of_memory_area[0]),
        .I3(cpu_address_selection_of_memory_area[1]),
        .I4(bram_sec_policy_config_data_read[22]),
        .O(\next_opcode_reg[22]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \next_opcode_reg[22]_i_4 
       (.I0(fifo_read_instrumentation_data_read[22]),
        .I1(fifo_ptm_data_read[22]),
        .I2(cpu_address_selection_of_memory_area[1]),
        .I3(bram_bbt_annotations_data_read[22]),
        .I4(cpu_address_selection_of_memory_area[0]),
        .I5(bram_firmware_code_data_read[22]),
        .O(\next_opcode_reg[22]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \next_opcode_reg[23]_i_2 
       (.I0(PLtoPS_readed_data[23]),
        .I1(cpu_address_selection_of_memory_area[1]),
        .I2(fifo_kernel_to_monitor_data_read[23]),
        .I3(cpu_address_selection_of_memory_area[0]),
        .I4(cpu_address_selection_of_memory_area[2]),
        .I5(\next_opcode_reg[23]_i_4_n_0 ),
        .O(\next_opcode_reg[23]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h05400040)) 
    \next_opcode_reg[23]_i_3 
       (.I0(cpu_address_selection_of_memory_area[2]),
        .I1(bram_debug_data_read[23]),
        .I2(cpu_address_selection_of_memory_area[0]),
        .I3(cpu_address_selection_of_memory_area[1]),
        .I4(bram_sec_policy_config_data_read[23]),
        .O(\next_opcode_reg[23]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \next_opcode_reg[23]_i_4 
       (.I0(fifo_read_instrumentation_data_read[23]),
        .I1(fifo_ptm_data_read[23]),
        .I2(cpu_address_selection_of_memory_area[1]),
        .I3(bram_bbt_annotations_data_read[23]),
        .I4(cpu_address_selection_of_memory_area[0]),
        .I5(bram_firmware_code_data_read[23]),
        .O(\next_opcode_reg[23]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \next_opcode_reg[24]_i_2 
       (.I0(PLtoPS_readed_data[24]),
        .I1(cpu_address_selection_of_memory_area[1]),
        .I2(fifo_kernel_to_monitor_data_read[24]),
        .I3(cpu_address_selection_of_memory_area[0]),
        .I4(cpu_address_selection_of_memory_area[2]),
        .I5(\next_opcode_reg[24]_i_4_n_0 ),
        .O(\next_opcode_reg[24]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h05400040)) 
    \next_opcode_reg[24]_i_3 
       (.I0(cpu_address_selection_of_memory_area[2]),
        .I1(bram_debug_data_read[24]),
        .I2(cpu_address_selection_of_memory_area[0]),
        .I3(cpu_address_selection_of_memory_area[1]),
        .I4(bram_sec_policy_config_data_read[24]),
        .O(\next_opcode_reg[24]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \next_opcode_reg[24]_i_4 
       (.I0(fifo_read_instrumentation_data_read[24]),
        .I1(fifo_ptm_data_read[24]),
        .I2(cpu_address_selection_of_memory_area[1]),
        .I3(bram_bbt_annotations_data_read[24]),
        .I4(cpu_address_selection_of_memory_area[0]),
        .I5(bram_firmware_code_data_read[24]),
        .O(\next_opcode_reg[24]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \next_opcode_reg[25]_i_2 
       (.I0(PLtoPS_readed_data[25]),
        .I1(cpu_address_selection_of_memory_area[1]),
        .I2(fifo_kernel_to_monitor_data_read[25]),
        .I3(cpu_address_selection_of_memory_area[0]),
        .I4(cpu_address_selection_of_memory_area[2]),
        .I5(\next_opcode_reg[25]_i_4_n_0 ),
        .O(\next_opcode_reg[25]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h05400040)) 
    \next_opcode_reg[25]_i_3 
       (.I0(cpu_address_selection_of_memory_area[2]),
        .I1(bram_debug_data_read[25]),
        .I2(cpu_address_selection_of_memory_area[0]),
        .I3(cpu_address_selection_of_memory_area[1]),
        .I4(bram_sec_policy_config_data_read[25]),
        .O(\next_opcode_reg[25]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \next_opcode_reg[25]_i_4 
       (.I0(fifo_read_instrumentation_data_read[25]),
        .I1(fifo_ptm_data_read[25]),
        .I2(cpu_address_selection_of_memory_area[1]),
        .I3(bram_bbt_annotations_data_read[25]),
        .I4(cpu_address_selection_of_memory_area[0]),
        .I5(bram_firmware_code_data_read[25]),
        .O(\next_opcode_reg[25]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \next_opcode_reg[26]_i_2 
       (.I0(PLtoPS_readed_data[26]),
        .I1(cpu_address_selection_of_memory_area[1]),
        .I2(fifo_kernel_to_monitor_data_read[26]),
        .I3(cpu_address_selection_of_memory_area[0]),
        .I4(cpu_address_selection_of_memory_area[2]),
        .I5(\next_opcode_reg[26]_i_4_n_0 ),
        .O(\next_opcode_reg[26]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h05400040)) 
    \next_opcode_reg[26]_i_3 
       (.I0(cpu_address_selection_of_memory_area[2]),
        .I1(bram_debug_data_read[26]),
        .I2(cpu_address_selection_of_memory_area[0]),
        .I3(cpu_address_selection_of_memory_area[1]),
        .I4(bram_sec_policy_config_data_read[26]),
        .O(\next_opcode_reg[26]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \next_opcode_reg[26]_i_4 
       (.I0(fifo_read_instrumentation_data_read[26]),
        .I1(fifo_ptm_data_read[26]),
        .I2(cpu_address_selection_of_memory_area[1]),
        .I3(bram_bbt_annotations_data_read[26]),
        .I4(cpu_address_selection_of_memory_area[0]),
        .I5(bram_firmware_code_data_read[26]),
        .O(\next_opcode_reg[26]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \next_opcode_reg[27]_i_2 
       (.I0(PLtoPS_readed_data[27]),
        .I1(cpu_address_selection_of_memory_area[1]),
        .I2(fifo_kernel_to_monitor_data_read[27]),
        .I3(cpu_address_selection_of_memory_area[0]),
        .I4(cpu_address_selection_of_memory_area[2]),
        .I5(\next_opcode_reg[27]_i_4_n_0 ),
        .O(\next_opcode_reg[27]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h05400040)) 
    \next_opcode_reg[27]_i_3 
       (.I0(cpu_address_selection_of_memory_area[2]),
        .I1(bram_debug_data_read[27]),
        .I2(cpu_address_selection_of_memory_area[0]),
        .I3(cpu_address_selection_of_memory_area[1]),
        .I4(bram_sec_policy_config_data_read[27]),
        .O(\next_opcode_reg[27]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \next_opcode_reg[27]_i_4 
       (.I0(fifo_read_instrumentation_data_read[27]),
        .I1(fifo_ptm_data_read[27]),
        .I2(cpu_address_selection_of_memory_area[1]),
        .I3(bram_bbt_annotations_data_read[27]),
        .I4(cpu_address_selection_of_memory_area[0]),
        .I5(bram_firmware_code_data_read[27]),
        .O(\next_opcode_reg[27]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \next_opcode_reg[28]_i_2 
       (.I0(PLtoPS_readed_data[28]),
        .I1(cpu_address_selection_of_memory_area[1]),
        .I2(fifo_kernel_to_monitor_data_read[28]),
        .I3(cpu_address_selection_of_memory_area[0]),
        .I4(cpu_address_selection_of_memory_area[2]),
        .I5(\next_opcode_reg[28]_i_4_n_0 ),
        .O(\next_opcode_reg[28]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h05400040)) 
    \next_opcode_reg[28]_i_3 
       (.I0(cpu_address_selection_of_memory_area[2]),
        .I1(bram_debug_data_read[28]),
        .I2(cpu_address_selection_of_memory_area[0]),
        .I3(cpu_address_selection_of_memory_area[1]),
        .I4(bram_sec_policy_config_data_read[28]),
        .O(\next_opcode_reg[28]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \next_opcode_reg[28]_i_4 
       (.I0(fifo_read_instrumentation_data_read[28]),
        .I1(fifo_ptm_data_read[28]),
        .I2(cpu_address_selection_of_memory_area[1]),
        .I3(bram_bbt_annotations_data_read[28]),
        .I4(cpu_address_selection_of_memory_area[0]),
        .I5(bram_firmware_code_data_read[28]),
        .O(\next_opcode_reg[28]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \next_opcode_reg[29]_i_2 
       (.I0(PLtoPS_readed_data[29]),
        .I1(cpu_address_selection_of_memory_area[1]),
        .I2(fifo_kernel_to_monitor_data_read[29]),
        .I3(cpu_address_selection_of_memory_area[0]),
        .I4(cpu_address_selection_of_memory_area[2]),
        .I5(\next_opcode_reg[29]_i_4_n_0 ),
        .O(\next_opcode_reg[29]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h05400040)) 
    \next_opcode_reg[29]_i_3 
       (.I0(cpu_address_selection_of_memory_area[2]),
        .I1(bram_debug_data_read[29]),
        .I2(cpu_address_selection_of_memory_area[0]),
        .I3(cpu_address_selection_of_memory_area[1]),
        .I4(bram_sec_policy_config_data_read[29]),
        .O(\next_opcode_reg[29]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \next_opcode_reg[29]_i_4 
       (.I0(fifo_read_instrumentation_data_read[29]),
        .I1(fifo_ptm_data_read[29]),
        .I2(cpu_address_selection_of_memory_area[1]),
        .I3(bram_bbt_annotations_data_read[29]),
        .I4(cpu_address_selection_of_memory_area[0]),
        .I5(bram_firmware_code_data_read[29]),
        .O(\next_opcode_reg[29]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \next_opcode_reg[2]_i_2 
       (.I0(PLtoPS_readed_data[2]),
        .I1(cpu_address_selection_of_memory_area[1]),
        .I2(fifo_kernel_to_monitor_data_read[2]),
        .I3(cpu_address_selection_of_memory_area[0]),
        .I4(cpu_address_selection_of_memory_area[2]),
        .I5(\next_opcode_reg[2]_i_4_n_0 ),
        .O(\next_opcode_reg[2]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h05400040)) 
    \next_opcode_reg[2]_i_3 
       (.I0(cpu_address_selection_of_memory_area[2]),
        .I1(bram_debug_data_read[2]),
        .I2(cpu_address_selection_of_memory_area[0]),
        .I3(cpu_address_selection_of_memory_area[1]),
        .I4(bram_sec_policy_config_data_read[2]),
        .O(\next_opcode_reg[2]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \next_opcode_reg[2]_i_4 
       (.I0(fifo_read_instrumentation_data_read[2]),
        .I1(fifo_ptm_data_read[2]),
        .I2(cpu_address_selection_of_memory_area[1]),
        .I3(bram_bbt_annotations_data_read[2]),
        .I4(cpu_address_selection_of_memory_area[0]),
        .I5(bram_firmware_code_data_read[2]),
        .O(\next_opcode_reg[2]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \next_opcode_reg[30]_i_2 
       (.I0(PLtoPS_readed_data[30]),
        .I1(cpu_address_selection_of_memory_area[1]),
        .I2(fifo_kernel_to_monitor_data_read[30]),
        .I3(cpu_address_selection_of_memory_area[0]),
        .I4(cpu_address_selection_of_memory_area[2]),
        .I5(\next_opcode_reg[30]_i_4_n_0 ),
        .O(\next_opcode_reg[30]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h05400040)) 
    \next_opcode_reg[30]_i_3 
       (.I0(cpu_address_selection_of_memory_area[2]),
        .I1(bram_debug_data_read[30]),
        .I2(cpu_address_selection_of_memory_area[0]),
        .I3(cpu_address_selection_of_memory_area[1]),
        .I4(bram_sec_policy_config_data_read[30]),
        .O(\next_opcode_reg[30]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \next_opcode_reg[30]_i_4 
       (.I0(fifo_read_instrumentation_data_read[30]),
        .I1(fifo_ptm_data_read[30]),
        .I2(cpu_address_selection_of_memory_area[1]),
        .I3(bram_bbt_annotations_data_read[30]),
        .I4(cpu_address_selection_of_memory_area[0]),
        .I5(bram_firmware_code_data_read[30]),
        .O(\next_opcode_reg[30]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \next_opcode_reg[31]_i_1 
       (.I0(pause_in0_out),
        .I1(mem_state_reg_reg_n_0),
        .O(\next_opcode_reg[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \next_opcode_reg[31]_i_3 
       (.I0(PLtoPS_readed_data[31]),
        .I1(cpu_address_selection_of_memory_area[1]),
        .I2(fifo_kernel_to_monitor_data_read[31]),
        .I3(cpu_address_selection_of_memory_area[0]),
        .I4(cpu_address_selection_of_memory_area[2]),
        .I5(\next_opcode_reg[31]_i_5_n_0 ),
        .O(\next_opcode_reg[31]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h05400040)) 
    \next_opcode_reg[31]_i_4 
       (.I0(cpu_address_selection_of_memory_area[2]),
        .I1(bram_debug_data_read[31]),
        .I2(cpu_address_selection_of_memory_area[0]),
        .I3(cpu_address_selection_of_memory_area[1]),
        .I4(bram_sec_policy_config_data_read[31]),
        .O(\next_opcode_reg[31]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \next_opcode_reg[31]_i_5 
       (.I0(fifo_read_instrumentation_data_read[31]),
        .I1(fifo_ptm_data_read[31]),
        .I2(cpu_address_selection_of_memory_area[1]),
        .I3(bram_bbt_annotations_data_read[31]),
        .I4(cpu_address_selection_of_memory_area[0]),
        .I5(bram_firmware_code_data_read[31]),
        .O(\next_opcode_reg[31]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \next_opcode_reg[3]_i_2 
       (.I0(PLtoPS_readed_data[3]),
        .I1(cpu_address_selection_of_memory_area[1]),
        .I2(fifo_kernel_to_monitor_data_read[3]),
        .I3(cpu_address_selection_of_memory_area[0]),
        .I4(cpu_address_selection_of_memory_area[2]),
        .I5(\next_opcode_reg[3]_i_4_n_0 ),
        .O(\next_opcode_reg[3]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h05400040)) 
    \next_opcode_reg[3]_i_3 
       (.I0(cpu_address_selection_of_memory_area[2]),
        .I1(bram_debug_data_read[3]),
        .I2(cpu_address_selection_of_memory_area[0]),
        .I3(cpu_address_selection_of_memory_area[1]),
        .I4(bram_sec_policy_config_data_read[3]),
        .O(\next_opcode_reg[3]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \next_opcode_reg[3]_i_4 
       (.I0(fifo_read_instrumentation_data_read[3]),
        .I1(fifo_ptm_data_read[3]),
        .I2(cpu_address_selection_of_memory_area[1]),
        .I3(bram_bbt_annotations_data_read[3]),
        .I4(cpu_address_selection_of_memory_area[0]),
        .I5(bram_firmware_code_data_read[3]),
        .O(\next_opcode_reg[3]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \next_opcode_reg[4]_i_2 
       (.I0(PLtoPS_readed_data[4]),
        .I1(cpu_address_selection_of_memory_area[1]),
        .I2(fifo_kernel_to_monitor_data_read[4]),
        .I3(cpu_address_selection_of_memory_area[0]),
        .I4(cpu_address_selection_of_memory_area[2]),
        .I5(\next_opcode_reg[4]_i_4_n_0 ),
        .O(\next_opcode_reg[4]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h05400040)) 
    \next_opcode_reg[4]_i_3 
       (.I0(cpu_address_selection_of_memory_area[2]),
        .I1(bram_debug_data_read[4]),
        .I2(cpu_address_selection_of_memory_area[0]),
        .I3(cpu_address_selection_of_memory_area[1]),
        .I4(bram_sec_policy_config_data_read[4]),
        .O(\next_opcode_reg[4]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \next_opcode_reg[4]_i_4 
       (.I0(fifo_read_instrumentation_data_read[4]),
        .I1(fifo_ptm_data_read[4]),
        .I2(cpu_address_selection_of_memory_area[1]),
        .I3(bram_bbt_annotations_data_read[4]),
        .I4(cpu_address_selection_of_memory_area[0]),
        .I5(bram_firmware_code_data_read[4]),
        .O(\next_opcode_reg[4]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \next_opcode_reg[5]_i_2 
       (.I0(PLtoPS_readed_data[5]),
        .I1(cpu_address_selection_of_memory_area[1]),
        .I2(fifo_kernel_to_monitor_data_read[5]),
        .I3(cpu_address_selection_of_memory_area[0]),
        .I4(cpu_address_selection_of_memory_area[2]),
        .I5(\next_opcode_reg[5]_i_4_n_0 ),
        .O(\next_opcode_reg[5]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h05400040)) 
    \next_opcode_reg[5]_i_3 
       (.I0(cpu_address_selection_of_memory_area[2]),
        .I1(bram_debug_data_read[5]),
        .I2(cpu_address_selection_of_memory_area[0]),
        .I3(cpu_address_selection_of_memory_area[1]),
        .I4(bram_sec_policy_config_data_read[5]),
        .O(\next_opcode_reg[5]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \next_opcode_reg[5]_i_4 
       (.I0(fifo_read_instrumentation_data_read[5]),
        .I1(fifo_ptm_data_read[5]),
        .I2(cpu_address_selection_of_memory_area[1]),
        .I3(bram_bbt_annotations_data_read[5]),
        .I4(cpu_address_selection_of_memory_area[0]),
        .I5(bram_firmware_code_data_read[5]),
        .O(\next_opcode_reg[5]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \next_opcode_reg[6]_i_2 
       (.I0(PLtoPS_readed_data[6]),
        .I1(cpu_address_selection_of_memory_area[1]),
        .I2(fifo_kernel_to_monitor_data_read[6]),
        .I3(cpu_address_selection_of_memory_area[0]),
        .I4(cpu_address_selection_of_memory_area[2]),
        .I5(\next_opcode_reg[6]_i_4_n_0 ),
        .O(\next_opcode_reg[6]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h05400040)) 
    \next_opcode_reg[6]_i_3 
       (.I0(cpu_address_selection_of_memory_area[2]),
        .I1(bram_debug_data_read[6]),
        .I2(cpu_address_selection_of_memory_area[0]),
        .I3(cpu_address_selection_of_memory_area[1]),
        .I4(bram_sec_policy_config_data_read[6]),
        .O(\next_opcode_reg[6]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \next_opcode_reg[6]_i_4 
       (.I0(fifo_read_instrumentation_data_read[6]),
        .I1(fifo_ptm_data_read[6]),
        .I2(cpu_address_selection_of_memory_area[1]),
        .I3(bram_bbt_annotations_data_read[6]),
        .I4(cpu_address_selection_of_memory_area[0]),
        .I5(bram_firmware_code_data_read[6]),
        .O(\next_opcode_reg[6]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \next_opcode_reg[7]_i_2 
       (.I0(PLtoPS_readed_data[7]),
        .I1(cpu_address_selection_of_memory_area[1]),
        .I2(fifo_kernel_to_monitor_data_read[7]),
        .I3(cpu_address_selection_of_memory_area[0]),
        .I4(cpu_address_selection_of_memory_area[2]),
        .I5(\next_opcode_reg[7]_i_4_n_0 ),
        .O(\next_opcode_reg[7]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h05400040)) 
    \next_opcode_reg[7]_i_3 
       (.I0(cpu_address_selection_of_memory_area[2]),
        .I1(bram_debug_data_read[7]),
        .I2(cpu_address_selection_of_memory_area[0]),
        .I3(cpu_address_selection_of_memory_area[1]),
        .I4(bram_sec_policy_config_data_read[7]),
        .O(\next_opcode_reg[7]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \next_opcode_reg[7]_i_4 
       (.I0(fifo_read_instrumentation_data_read[7]),
        .I1(fifo_ptm_data_read[7]),
        .I2(cpu_address_selection_of_memory_area[1]),
        .I3(bram_bbt_annotations_data_read[7]),
        .I4(cpu_address_selection_of_memory_area[0]),
        .I5(bram_firmware_code_data_read[7]),
        .O(\next_opcode_reg[7]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \next_opcode_reg[8]_i_2 
       (.I0(PLtoPS_readed_data[8]),
        .I1(cpu_address_selection_of_memory_area[1]),
        .I2(fifo_kernel_to_monitor_data_read[8]),
        .I3(cpu_address_selection_of_memory_area[0]),
        .I4(cpu_address_selection_of_memory_area[2]),
        .I5(\next_opcode_reg[8]_i_4_n_0 ),
        .O(\next_opcode_reg[8]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h05400040)) 
    \next_opcode_reg[8]_i_3 
       (.I0(cpu_address_selection_of_memory_area[2]),
        .I1(bram_debug_data_read[8]),
        .I2(cpu_address_selection_of_memory_area[0]),
        .I3(cpu_address_selection_of_memory_area[1]),
        .I4(bram_sec_policy_config_data_read[8]),
        .O(\next_opcode_reg[8]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \next_opcode_reg[8]_i_4 
       (.I0(fifo_read_instrumentation_data_read[8]),
        .I1(fifo_ptm_data_read[8]),
        .I2(cpu_address_selection_of_memory_area[1]),
        .I3(bram_bbt_annotations_data_read[8]),
        .I4(cpu_address_selection_of_memory_area[0]),
        .I5(bram_firmware_code_data_read[8]),
        .O(\next_opcode_reg[8]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h8830FFFF88300000)) 
    \next_opcode_reg[9]_i_2 
       (.I0(PLtoPS_readed_data[9]),
        .I1(cpu_address_selection_of_memory_area[1]),
        .I2(fifo_kernel_to_monitor_data_read[9]),
        .I3(cpu_address_selection_of_memory_area[0]),
        .I4(cpu_address_selection_of_memory_area[2]),
        .I5(\next_opcode_reg[9]_i_4_n_0 ),
        .O(\next_opcode_reg[9]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h05400040)) 
    \next_opcode_reg[9]_i_3 
       (.I0(cpu_address_selection_of_memory_area[2]),
        .I1(bram_debug_data_read[9]),
        .I2(cpu_address_selection_of_memory_area[0]),
        .I3(cpu_address_selection_of_memory_area[1]),
        .I4(bram_sec_policy_config_data_read[9]),
        .O(\next_opcode_reg[9]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \next_opcode_reg[9]_i_4 
       (.I0(fifo_read_instrumentation_data_read[9]),
        .I1(fifo_ptm_data_read[9]),
        .I2(cpu_address_selection_of_memory_area[1]),
        .I3(bram_bbt_annotations_data_read[9]),
        .I4(cpu_address_selection_of_memory_area[0]),
        .I5(bram_firmware_code_data_read[9]),
        .O(\next_opcode_reg[9]_i_4_n_0 ));
  FDCE \next_opcode_reg_reg[0] 
       (.C(clk),
        .CE(\next_opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(cpu_data_r[0]),
        .Q(\next_opcode_reg_reg_n_0_[0] ));
  FDCE \next_opcode_reg_reg[10] 
       (.C(clk),
        .CE(\next_opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(cpu_data_r[10]),
        .Q(\next_opcode_reg_reg_n_0_[10] ));
  MUXF7 \next_opcode_reg_reg[10]_i_1 
       (.I0(\next_opcode_reg[10]_i_2_n_0 ),
        .I1(\next_opcode_reg[10]_i_3_n_0 ),
        .O(cpu_data_r[10]),
        .S(cpu_address_selection_of_memory_area[3]));
  FDCE \next_opcode_reg_reg[11] 
       (.C(clk),
        .CE(\next_opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(cpu_data_r[11]),
        .Q(\next_opcode_reg_reg_n_0_[11] ));
  MUXF7 \next_opcode_reg_reg[11]_i_1 
       (.I0(\next_opcode_reg[11]_i_2_n_0 ),
        .I1(\next_opcode_reg[11]_i_3_n_0 ),
        .O(cpu_data_r[11]),
        .S(cpu_address_selection_of_memory_area[3]));
  FDCE \next_opcode_reg_reg[12] 
       (.C(clk),
        .CE(\next_opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(cpu_data_r[12]),
        .Q(\next_opcode_reg_reg_n_0_[12] ));
  MUXF7 \next_opcode_reg_reg[12]_i_1 
       (.I0(\next_opcode_reg[12]_i_2_n_0 ),
        .I1(\next_opcode_reg[12]_i_3_n_0 ),
        .O(cpu_data_r[12]),
        .S(cpu_address_selection_of_memory_area[3]));
  FDCE \next_opcode_reg_reg[13] 
       (.C(clk),
        .CE(\next_opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(cpu_data_r[13]),
        .Q(\next_opcode_reg_reg_n_0_[13] ));
  MUXF7 \next_opcode_reg_reg[13]_i_1 
       (.I0(\next_opcode_reg[13]_i_2_n_0 ),
        .I1(\next_opcode_reg[13]_i_3_n_0 ),
        .O(cpu_data_r[13]),
        .S(cpu_address_selection_of_memory_area[3]));
  FDCE \next_opcode_reg_reg[14] 
       (.C(clk),
        .CE(\next_opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(cpu_data_r[14]),
        .Q(\next_opcode_reg_reg_n_0_[14] ));
  MUXF7 \next_opcode_reg_reg[14]_i_1 
       (.I0(\next_opcode_reg[14]_i_2_n_0 ),
        .I1(\next_opcode_reg[14]_i_3_n_0 ),
        .O(cpu_data_r[14]),
        .S(cpu_address_selection_of_memory_area[3]));
  FDCE \next_opcode_reg_reg[15] 
       (.C(clk),
        .CE(\next_opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(cpu_data_r[15]),
        .Q(\next_opcode_reg_reg_n_0_[15] ));
  MUXF7 \next_opcode_reg_reg[15]_i_1 
       (.I0(\next_opcode_reg[15]_i_2_n_0 ),
        .I1(\next_opcode_reg[15]_i_3_n_0 ),
        .O(cpu_data_r[15]),
        .S(cpu_address_selection_of_memory_area[3]));
  FDCE \next_opcode_reg_reg[16] 
       (.C(clk),
        .CE(\next_opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(cpu_data_r[16]),
        .Q(\next_opcode_reg_reg_n_0_[16] ));
  MUXF7 \next_opcode_reg_reg[16]_i_1 
       (.I0(\next_opcode_reg[16]_i_2_n_0 ),
        .I1(\next_opcode_reg[16]_i_3_n_0 ),
        .O(cpu_data_r[16]),
        .S(cpu_address_selection_of_memory_area[3]));
  FDCE \next_opcode_reg_reg[17] 
       (.C(clk),
        .CE(\next_opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(cpu_data_r[17]),
        .Q(\next_opcode_reg_reg_n_0_[17] ));
  MUXF7 \next_opcode_reg_reg[17]_i_1 
       (.I0(\next_opcode_reg[17]_i_2_n_0 ),
        .I1(\next_opcode_reg[17]_i_3_n_0 ),
        .O(cpu_data_r[17]),
        .S(cpu_address_selection_of_memory_area[3]));
  FDCE \next_opcode_reg_reg[18] 
       (.C(clk),
        .CE(\next_opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(cpu_data_r[18]),
        .Q(\next_opcode_reg_reg_n_0_[18] ));
  MUXF7 \next_opcode_reg_reg[18]_i_1 
       (.I0(\next_opcode_reg[18]_i_2_n_0 ),
        .I1(\next_opcode_reg[18]_i_3_n_0 ),
        .O(cpu_data_r[18]),
        .S(cpu_address_selection_of_memory_area[3]));
  FDCE \next_opcode_reg_reg[19] 
       (.C(clk),
        .CE(\next_opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(cpu_data_r[19]),
        .Q(\next_opcode_reg_reg_n_0_[19] ));
  MUXF7 \next_opcode_reg_reg[19]_i_1 
       (.I0(\next_opcode_reg[19]_i_2_n_0 ),
        .I1(\next_opcode_reg[19]_i_3_n_0 ),
        .O(cpu_data_r[19]),
        .S(cpu_address_selection_of_memory_area[3]));
  FDCE \next_opcode_reg_reg[1] 
       (.C(clk),
        .CE(\next_opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(cpu_data_r[1]),
        .Q(\next_opcode_reg_reg_n_0_[1] ));
  MUXF7 \next_opcode_reg_reg[1]_i_1 
       (.I0(\next_opcode_reg[1]_i_2_n_0 ),
        .I1(\next_opcode_reg[1]_i_3_n_0 ),
        .O(cpu_data_r[1]),
        .S(cpu_address_selection_of_memory_area[3]));
  FDCE \next_opcode_reg_reg[20] 
       (.C(clk),
        .CE(\next_opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(cpu_data_r[20]),
        .Q(\next_opcode_reg_reg_n_0_[20] ));
  MUXF7 \next_opcode_reg_reg[20]_i_1 
       (.I0(\next_opcode_reg[20]_i_2_n_0 ),
        .I1(\next_opcode_reg[20]_i_3_n_0 ),
        .O(cpu_data_r[20]),
        .S(cpu_address_selection_of_memory_area[3]));
  FDCE \next_opcode_reg_reg[21] 
       (.C(clk),
        .CE(\next_opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(cpu_data_r[21]),
        .Q(\next_opcode_reg_reg_n_0_[21] ));
  MUXF7 \next_opcode_reg_reg[21]_i_1 
       (.I0(\next_opcode_reg[21]_i_2_n_0 ),
        .I1(\next_opcode_reg[21]_i_3_n_0 ),
        .O(cpu_data_r[21]),
        .S(cpu_address_selection_of_memory_area[3]));
  FDCE \next_opcode_reg_reg[22] 
       (.C(clk),
        .CE(\next_opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(cpu_data_r[22]),
        .Q(\next_opcode_reg_reg_n_0_[22] ));
  MUXF7 \next_opcode_reg_reg[22]_i_1 
       (.I0(\next_opcode_reg[22]_i_2_n_0 ),
        .I1(\next_opcode_reg[22]_i_3_n_0 ),
        .O(cpu_data_r[22]),
        .S(cpu_address_selection_of_memory_area[3]));
  FDCE \next_opcode_reg_reg[23] 
       (.C(clk),
        .CE(\next_opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(cpu_data_r[23]),
        .Q(\next_opcode_reg_reg_n_0_[23] ));
  MUXF7 \next_opcode_reg_reg[23]_i_1 
       (.I0(\next_opcode_reg[23]_i_2_n_0 ),
        .I1(\next_opcode_reg[23]_i_3_n_0 ),
        .O(cpu_data_r[23]),
        .S(cpu_address_selection_of_memory_area[3]));
  FDCE \next_opcode_reg_reg[24] 
       (.C(clk),
        .CE(\next_opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(cpu_data_r[24]),
        .Q(\next_opcode_reg_reg_n_0_[24] ));
  MUXF7 \next_opcode_reg_reg[24]_i_1 
       (.I0(\next_opcode_reg[24]_i_2_n_0 ),
        .I1(\next_opcode_reg[24]_i_3_n_0 ),
        .O(cpu_data_r[24]),
        .S(cpu_address_selection_of_memory_area[3]));
  FDCE \next_opcode_reg_reg[25] 
       (.C(clk),
        .CE(\next_opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(cpu_data_r[25]),
        .Q(\next_opcode_reg_reg_n_0_[25] ));
  MUXF7 \next_opcode_reg_reg[25]_i_1 
       (.I0(\next_opcode_reg[25]_i_2_n_0 ),
        .I1(\next_opcode_reg[25]_i_3_n_0 ),
        .O(cpu_data_r[25]),
        .S(cpu_address_selection_of_memory_area[3]));
  FDCE \next_opcode_reg_reg[26] 
       (.C(clk),
        .CE(\next_opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(cpu_data_r[26]),
        .Q(\next_opcode_reg_reg_n_0_[26] ));
  MUXF7 \next_opcode_reg_reg[26]_i_1 
       (.I0(\next_opcode_reg[26]_i_2_n_0 ),
        .I1(\next_opcode_reg[26]_i_3_n_0 ),
        .O(cpu_data_r[26]),
        .S(cpu_address_selection_of_memory_area[3]));
  FDCE \next_opcode_reg_reg[27] 
       (.C(clk),
        .CE(\next_opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(cpu_data_r[27]),
        .Q(\next_opcode_reg_reg_n_0_[27] ));
  MUXF7 \next_opcode_reg_reg[27]_i_1 
       (.I0(\next_opcode_reg[27]_i_2_n_0 ),
        .I1(\next_opcode_reg[27]_i_3_n_0 ),
        .O(cpu_data_r[27]),
        .S(cpu_address_selection_of_memory_area[3]));
  FDCE \next_opcode_reg_reg[28] 
       (.C(clk),
        .CE(\next_opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(cpu_data_r[28]),
        .Q(\next_opcode_reg_reg_n_0_[28] ));
  MUXF7 \next_opcode_reg_reg[28]_i_1 
       (.I0(\next_opcode_reg[28]_i_2_n_0 ),
        .I1(\next_opcode_reg[28]_i_3_n_0 ),
        .O(cpu_data_r[28]),
        .S(cpu_address_selection_of_memory_area[3]));
  FDCE \next_opcode_reg_reg[29] 
       (.C(clk),
        .CE(\next_opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(cpu_data_r[29]),
        .Q(\next_opcode_reg_reg_n_0_[29] ));
  MUXF7 \next_opcode_reg_reg[29]_i_1 
       (.I0(\next_opcode_reg[29]_i_2_n_0 ),
        .I1(\next_opcode_reg[29]_i_3_n_0 ),
        .O(cpu_data_r[29]),
        .S(cpu_address_selection_of_memory_area[3]));
  FDCE \next_opcode_reg_reg[2] 
       (.C(clk),
        .CE(\next_opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(cpu_data_r[2]),
        .Q(\next_opcode_reg_reg_n_0_[2] ));
  MUXF7 \next_opcode_reg_reg[2]_i_1 
       (.I0(\next_opcode_reg[2]_i_2_n_0 ),
        .I1(\next_opcode_reg[2]_i_3_n_0 ),
        .O(cpu_data_r[2]),
        .S(cpu_address_selection_of_memory_area[3]));
  FDCE \next_opcode_reg_reg[30] 
       (.C(clk),
        .CE(\next_opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(cpu_data_r[30]),
        .Q(\next_opcode_reg_reg_n_0_[30] ));
  MUXF7 \next_opcode_reg_reg[30]_i_1 
       (.I0(\next_opcode_reg[30]_i_2_n_0 ),
        .I1(\next_opcode_reg[30]_i_3_n_0 ),
        .O(cpu_data_r[30]),
        .S(cpu_address_selection_of_memory_area[3]));
  FDCE \next_opcode_reg_reg[31] 
       (.C(clk),
        .CE(\next_opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(cpu_data_r[31]),
        .Q(\next_opcode_reg_reg_n_0_[31] ));
  MUXF7 \next_opcode_reg_reg[31]_i_2 
       (.I0(\next_opcode_reg[31]_i_3_n_0 ),
        .I1(\next_opcode_reg[31]_i_4_n_0 ),
        .O(cpu_data_r[31]),
        .S(cpu_address_selection_of_memory_area[3]));
  FDCE \next_opcode_reg_reg[3] 
       (.C(clk),
        .CE(\next_opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(cpu_data_r[3]),
        .Q(\next_opcode_reg_reg_n_0_[3] ));
  MUXF7 \next_opcode_reg_reg[3]_i_1 
       (.I0(\next_opcode_reg[3]_i_2_n_0 ),
        .I1(\next_opcode_reg[3]_i_3_n_0 ),
        .O(cpu_data_r[3]),
        .S(cpu_address_selection_of_memory_area[3]));
  FDCE \next_opcode_reg_reg[4] 
       (.C(clk),
        .CE(\next_opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(cpu_data_r[4]),
        .Q(\next_opcode_reg_reg_n_0_[4] ));
  MUXF7 \next_opcode_reg_reg[4]_i_1 
       (.I0(\next_opcode_reg[4]_i_2_n_0 ),
        .I1(\next_opcode_reg[4]_i_3_n_0 ),
        .O(cpu_data_r[4]),
        .S(cpu_address_selection_of_memory_area[3]));
  FDCE \next_opcode_reg_reg[5] 
       (.C(clk),
        .CE(\next_opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(cpu_data_r[5]),
        .Q(\next_opcode_reg_reg_n_0_[5] ));
  MUXF7 \next_opcode_reg_reg[5]_i_1 
       (.I0(\next_opcode_reg[5]_i_2_n_0 ),
        .I1(\next_opcode_reg[5]_i_3_n_0 ),
        .O(cpu_data_r[5]),
        .S(cpu_address_selection_of_memory_area[3]));
  FDCE \next_opcode_reg_reg[6] 
       (.C(clk),
        .CE(\next_opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(cpu_data_r[6]),
        .Q(\next_opcode_reg_reg_n_0_[6] ));
  MUXF7 \next_opcode_reg_reg[6]_i_1 
       (.I0(\next_opcode_reg[6]_i_2_n_0 ),
        .I1(\next_opcode_reg[6]_i_3_n_0 ),
        .O(cpu_data_r[6]),
        .S(cpu_address_selection_of_memory_area[3]));
  FDCE \next_opcode_reg_reg[7] 
       (.C(clk),
        .CE(\next_opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(cpu_data_r[7]),
        .Q(\next_opcode_reg_reg_n_0_[7] ));
  MUXF7 \next_opcode_reg_reg[7]_i_1 
       (.I0(\next_opcode_reg[7]_i_2_n_0 ),
        .I1(\next_opcode_reg[7]_i_3_n_0 ),
        .O(cpu_data_r[7]),
        .S(cpu_address_selection_of_memory_area[3]));
  FDCE \next_opcode_reg_reg[8] 
       (.C(clk),
        .CE(\next_opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(cpu_data_r[8]),
        .Q(\next_opcode_reg_reg_n_0_[8] ));
  MUXF7 \next_opcode_reg_reg[8]_i_1 
       (.I0(\next_opcode_reg[8]_i_2_n_0 ),
        .I1(\next_opcode_reg[8]_i_3_n_0 ),
        .O(cpu_data_r[8]),
        .S(cpu_address_selection_of_memory_area[3]));
  FDCE \next_opcode_reg_reg[9] 
       (.C(clk),
        .CE(\next_opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(cpu_data_r[9]),
        .Q(\next_opcode_reg_reg_n_0_[9] ));
  MUXF7 \next_opcode_reg_reg[9]_i_1 
       (.I0(\next_opcode_reg[9]_i_2_n_0 ),
        .I1(\next_opcode_reg[9]_i_3_n_0 ),
        .O(cpu_data_r[9]),
        .S(cpu_address_selection_of_memory_area[3]));
  LUT4 #(
    .INIT(16'h00E2)) 
    \opcode_reg[0]_i_1 
       (.I0(\next_opcode_reg_reg_n_0_[0] ),
        .I1(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I2(cpu_data_r[0]),
        .I3(opcode_next1__0),
        .O(\opcode_reg[0]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h00E2)) 
    \opcode_reg[10]_i_1 
       (.I0(\next_opcode_reg_reg_n_0_[10] ),
        .I1(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I2(cpu_data_r[10]),
        .I3(opcode_next1__0),
        .O(\opcode_reg[10]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h00E2)) 
    \opcode_reg[11]_i_1 
       (.I0(\next_opcode_reg_reg_n_0_[11] ),
        .I1(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I2(cpu_data_r[11]),
        .I3(opcode_next1__0),
        .O(\opcode_reg[11]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h00E2)) 
    \opcode_reg[12]_i_1 
       (.I0(\next_opcode_reg_reg_n_0_[12] ),
        .I1(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I2(cpu_data_r[12]),
        .I3(opcode_next1__0),
        .O(\opcode_reg[12]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h00E2)) 
    \opcode_reg[13]_i_1 
       (.I0(\next_opcode_reg_reg_n_0_[13] ),
        .I1(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I2(cpu_data_r[13]),
        .I3(opcode_next1__0),
        .O(\opcode_reg[13]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h00E2)) 
    \opcode_reg[14]_i_1 
       (.I0(\next_opcode_reg_reg_n_0_[14] ),
        .I1(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I2(cpu_data_r[14]),
        .I3(opcode_next1__0),
        .O(\opcode_reg[14]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h00E2)) 
    \opcode_reg[15]_i_1 
       (.I0(\next_opcode_reg_reg_n_0_[15] ),
        .I1(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I2(cpu_data_r[15]),
        .I3(opcode_next1__0),
        .O(\opcode_reg[15]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h00E2)) 
    \opcode_reg[16]_i_1 
       (.I0(\next_opcode_reg_reg_n_0_[16] ),
        .I1(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I2(cpu_data_r[16]),
        .I3(opcode_next1__0),
        .O(\opcode_reg[16]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h00E2)) 
    \opcode_reg[17]_i_1 
       (.I0(\next_opcode_reg_reg_n_0_[17] ),
        .I1(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I2(cpu_data_r[17]),
        .I3(opcode_next1__0),
        .O(\opcode_reg[17]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h00E2)) 
    \opcode_reg[18]_i_1 
       (.I0(\next_opcode_reg_reg_n_0_[18] ),
        .I1(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I2(cpu_data_r[18]),
        .I3(opcode_next1__0),
        .O(\opcode_reg[18]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h00E2)) 
    \opcode_reg[19]_i_1 
       (.I0(\next_opcode_reg_reg_n_0_[19] ),
        .I1(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I2(cpu_data_r[19]),
        .I3(opcode_next1__0),
        .O(\opcode_reg[19]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h00E2)) 
    \opcode_reg[1]_i_1 
       (.I0(\next_opcode_reg_reg_n_0_[1] ),
        .I1(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I2(cpu_data_r[1]),
        .I3(opcode_next1__0),
        .O(\opcode_reg[1]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h00E2)) 
    \opcode_reg[20]_i_1 
       (.I0(\next_opcode_reg_reg_n_0_[20] ),
        .I1(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I2(cpu_data_r[20]),
        .I3(opcode_next1__0),
        .O(\opcode_reg[20]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h00E2)) 
    \opcode_reg[21]_i_1 
       (.I0(\next_opcode_reg_reg_n_0_[21] ),
        .I1(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I2(cpu_data_r[21]),
        .I3(opcode_next1__0),
        .O(\opcode_reg[21]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h00E2)) 
    \opcode_reg[22]_i_1 
       (.I0(\next_opcode_reg_reg_n_0_[22] ),
        .I1(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I2(cpu_data_r[22]),
        .I3(opcode_next1__0),
        .O(\opcode_reg[22]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h00E2)) 
    \opcode_reg[23]_i_1 
       (.I0(\next_opcode_reg_reg_n_0_[23] ),
        .I1(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I2(cpu_data_r[23]),
        .I3(opcode_next1__0),
        .O(\opcode_reg[23]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h00E2)) 
    \opcode_reg[24]_i_1 
       (.I0(\next_opcode_reg_reg_n_0_[24] ),
        .I1(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I2(cpu_data_r[24]),
        .I3(opcode_next1__0),
        .O(\opcode_reg[24]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h00E2)) 
    \opcode_reg[25]_i_1 
       (.I0(\next_opcode_reg_reg_n_0_[25] ),
        .I1(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I2(cpu_data_r[25]),
        .I3(opcode_next1__0),
        .O(\opcode_reg[25]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h00E2)) 
    \opcode_reg[26]_i_1 
       (.I0(\next_opcode_reg_reg_n_0_[26] ),
        .I1(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I2(cpu_data_r[26]),
        .I3(opcode_next1__0),
        .O(\opcode_reg[26]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h00E2)) 
    \opcode_reg[27]_i_1 
       (.I0(\next_opcode_reg_reg_n_0_[27] ),
        .I1(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I2(cpu_data_r[27]),
        .I3(opcode_next1__0),
        .O(\opcode_reg[27]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h00E2)) 
    \opcode_reg[28]_i_1 
       (.I0(\next_opcode_reg_reg_n_0_[28] ),
        .I1(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I2(cpu_data_r[28]),
        .I3(opcode_next1__0),
        .O(\opcode_reg[28]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h00E2)) 
    \opcode_reg[29]_i_1 
       (.I0(\next_opcode_reg_reg_n_0_[29] ),
        .I1(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I2(cpu_data_r[29]),
        .I3(opcode_next1__0),
        .O(\opcode_reg[29]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair83" *) 
  LUT4 #(
    .INIT(16'h00E2)) 
    \opcode_reg[2]_i_1 
       (.I0(\next_opcode_reg_reg_n_0_[2] ),
        .I1(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I2(cpu_data_r[2]),
        .I3(opcode_next1__0),
        .O(\opcode_reg[2]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h00E2)) 
    \opcode_reg[30]_i_1 
       (.I0(\next_opcode_reg_reg_n_0_[30] ),
        .I1(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I2(cpu_data_r[30]),
        .I3(opcode_next1__0),
        .O(\opcode_reg[30]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h00FB)) 
    \opcode_reg[31]_i_1 
       (.I0(mem_state_reg_reg_n_0),
        .I1(\opcode_reg[31]_i_3_n_0 ),
        .I2(opcode_next1__0),
        .I3(pause_in0_out),
        .O(\opcode_reg[31]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h00E2)) 
    \opcode_reg[31]_i_2 
       (.I0(\next_opcode_reg_reg_n_0_[31] ),
        .I1(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I2(cpu_data_r[31]),
        .I3(opcode_next1__0),
        .O(\opcode_reg[31]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \opcode_reg[31]_i_3 
       (.I0(mem_source[3]),
        .I1(mem_source[2]),
        .I2(mem_source[0]),
        .I3(mem_source[1]),
        .O(\opcode_reg[31]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h00AE)) 
    \opcode_reg[31]_i_4 
       (.I0(\u3_control/is_syscall__2 ),
        .I1(\opcode_reg[31]_i_5_n_0 ),
        .I2(take_branch),
        .I3(pause_in0_out),
        .O(opcode_next1__0));
  LUT6 #(
    .INIT(64'hFFFF000200000000)) 
    \opcode_reg[31]_i_5 
       (.I0(opcode[27]),
        .I1(opcode[29]),
        .I2(opcode[28]),
        .I3(\pc_reg[3]_i_5_n_0 ),
        .I4(\u3_control/is_syscall__2 ),
        .I5(pc_source[1]),
        .O(\opcode_reg[31]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'h00E2)) 
    \opcode_reg[3]_i_1 
       (.I0(\next_opcode_reg_reg_n_0_[3] ),
        .I1(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I2(cpu_data_r[3]),
        .I3(opcode_next1__0),
        .O(\opcode_reg[3]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h00E2)) 
    \opcode_reg[4]_i_1 
       (.I0(\next_opcode_reg_reg_n_0_[4] ),
        .I1(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I2(cpu_data_r[4]),
        .I3(opcode_next1__0),
        .O(\opcode_reg[4]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h00E2)) 
    \opcode_reg[5]_i_1 
       (.I0(\next_opcode_reg_reg_n_0_[5] ),
        .I1(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I2(cpu_data_r[5]),
        .I3(opcode_next1__0),
        .O(\opcode_reg[5]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h00E2)) 
    \opcode_reg[6]_i_1 
       (.I0(\next_opcode_reg_reg_n_0_[6] ),
        .I1(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I2(cpu_data_r[6]),
        .I3(opcode_next1__0),
        .O(\opcode_reg[6]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h00E2)) 
    \opcode_reg[7]_i_1 
       (.I0(\next_opcode_reg_reg_n_0_[7] ),
        .I1(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I2(cpu_data_r[7]),
        .I3(opcode_next1__0),
        .O(\opcode_reg[7]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h00E2)) 
    \opcode_reg[8]_i_1 
       (.I0(\next_opcode_reg_reg_n_0_[8] ),
        .I1(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I2(cpu_data_r[8]),
        .I3(opcode_next1__0),
        .O(\opcode_reg[8]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h00E2)) 
    \opcode_reg[9]_i_1 
       (.I0(\next_opcode_reg_reg_n_0_[9] ),
        .I1(\bram_firmware_code_address_reg[19]_i_2_n_0 ),
        .I2(cpu_data_r[9]),
        .I3(opcode_next1__0),
        .O(\opcode_reg[9]_i_1_n_0 ));
  FDCE \opcode_reg_reg[0] 
       (.C(clk),
        .CE(\opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(\opcode_reg[0]_i_1_n_0 ),
        .Q(opcode[0]));
  FDCE \opcode_reg_reg[10] 
       (.C(clk),
        .CE(\opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(\opcode_reg[10]_i_1_n_0 ),
        .Q(opcode[10]));
  FDCE \opcode_reg_reg[11] 
       (.C(clk),
        .CE(\opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(\opcode_reg[11]_i_1_n_0 ),
        .Q(opcode[11]));
  FDCE \opcode_reg_reg[12] 
       (.C(clk),
        .CE(\opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(\opcode_reg[12]_i_1_n_0 ),
        .Q(\pc_reg_reg[14] ));
  FDCE \opcode_reg_reg[13] 
       (.C(clk),
        .CE(\opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(\opcode_reg[13]_i_1_n_0 ),
        .Q(opcode[13]));
  FDCE \opcode_reg_reg[14] 
       (.C(clk),
        .CE(\opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(\opcode_reg[14]_i_1_n_0 ),
        .Q(opcode[14]));
  FDCE \opcode_reg_reg[15] 
       (.C(clk),
        .CE(\opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(\opcode_reg[15]_i_1_n_0 ),
        .Q(opcode[15]));
  FDCE \opcode_reg_reg[16] 
       (.C(clk),
        .CE(\opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(\opcode_reg[16]_i_1_n_0 ),
        .Q(opcode[16]));
  FDCE \opcode_reg_reg[17] 
       (.C(clk),
        .CE(\opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(\opcode_reg[17]_i_1_n_0 ),
        .Q(opcode[17]));
  FDCE \opcode_reg_reg[18] 
       (.C(clk),
        .CE(\opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(\opcode_reg[18]_i_1_n_0 ),
        .Q(opcode[18]));
  FDCE \opcode_reg_reg[19] 
       (.C(clk),
        .CE(\opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(\opcode_reg[19]_i_1_n_0 ),
        .Q(opcode[19]));
  FDCE \opcode_reg_reg[1] 
       (.C(clk),
        .CE(\opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(\opcode_reg[1]_i_1_n_0 ),
        .Q(opcode[1]));
  FDCE \opcode_reg_reg[20] 
       (.C(clk),
        .CE(\opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(\opcode_reg[20]_i_1_n_0 ),
        .Q(opcode[20]));
  FDCE \opcode_reg_reg[21] 
       (.C(clk),
        .CE(\opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(\opcode_reg[21]_i_1_n_0 ),
        .Q(opcode[21]));
  FDCE \opcode_reg_reg[22] 
       (.C(clk),
        .CE(\opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(\opcode_reg[22]_i_1_n_0 ),
        .Q(opcode[22]));
  FDCE \opcode_reg_reg[23] 
       (.C(clk),
        .CE(\opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(\opcode_reg[23]_i_1_n_0 ),
        .Q(opcode[23]));
  FDCE \opcode_reg_reg[24] 
       (.C(clk),
        .CE(\opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(\opcode_reg[24]_i_1_n_0 ),
        .Q(opcode[24]));
  FDCE \opcode_reg_reg[25] 
       (.C(clk),
        .CE(\opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(\opcode_reg[25]_i_1_n_0 ),
        .Q(opcode[25]));
  FDCE \opcode_reg_reg[26] 
       (.C(clk),
        .CE(\opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(\opcode_reg[26]_i_1_n_0 ),
        .Q(opcode[26]));
  FDCE \opcode_reg_reg[27] 
       (.C(clk),
        .CE(\opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(\opcode_reg[27]_i_1_n_0 ),
        .Q(opcode[27]));
  FDCE \opcode_reg_reg[28] 
       (.C(clk),
        .CE(\opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(\opcode_reg[28]_i_1_n_0 ),
        .Q(opcode[28]));
  FDCE \opcode_reg_reg[29] 
       (.C(clk),
        .CE(\opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(\opcode_reg[29]_i_1_n_0 ),
        .Q(opcode[29]));
  FDCE \opcode_reg_reg[2] 
       (.C(clk),
        .CE(\opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(\opcode_reg[2]_i_1_n_0 ),
        .Q(opcode[2]));
  FDCE \opcode_reg_reg[30] 
       (.C(clk),
        .CE(\opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(\opcode_reg[30]_i_1_n_0 ),
        .Q(opcode[30]));
  FDCE \opcode_reg_reg[31] 
       (.C(clk),
        .CE(\opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(\opcode_reg[31]_i_2_n_0 ),
        .Q(opcode[31]));
  FDCE \opcode_reg_reg[3] 
       (.C(clk),
        .CE(\opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(\opcode_reg[3]_i_1_n_0 ),
        .Q(opcode[3]));
  FDCE \opcode_reg_reg[4] 
       (.C(clk),
        .CE(\opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(\opcode_reg[4]_i_1_n_0 ),
        .Q(opcode[4]));
  FDCE \opcode_reg_reg[5] 
       (.C(clk),
        .CE(\opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(\opcode_reg[5]_i_1_n_0 ),
        .Q(opcode[5]));
  FDCE \opcode_reg_reg[6] 
       (.C(clk),
        .CE(\opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(\opcode_reg[6]_i_1_n_0 ),
        .Q(opcode[6]));
  FDCE \opcode_reg_reg[7] 
       (.C(clk),
        .CE(\opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(\opcode_reg[7]_i_1_n_0 ),
        .Q(opcode[7]));
  FDCE \opcode_reg_reg[8] 
       (.C(clk),
        .CE(\opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(\opcode_reg[8]_i_1_n_0 ),
        .Q(opcode[8]));
  FDCE \opcode_reg_reg[9] 
       (.C(clk),
        .CE(\opcode_reg[31]_i_1_n_0 ),
        .CLR(\address_reg_reg[24]_0 ),
        .D(\opcode_reg[9]_i_1_n_0 ),
        .Q(opcode[9]));
  LUT6 #(
    .INIT(64'hE2E2FFAAE2E200AA)) 
    \pc_reg[10]_i_1 
       (.I0(pc_plus4[7]),
        .I1(take_branch),
        .I2(pc_new[10]),
        .I3(pc_source[0]),
        .I4(pc_source[1]),
        .I5(opcode[8]),
        .O(\pc_reg_reg[10] ));
  LUT6 #(
    .INIT(64'hE2E2FFAAE2E200AA)) 
    \pc_reg[11]_i_1 
       (.I0(pc_plus4[8]),
        .I1(take_branch),
        .I2(pc_new[11]),
        .I3(pc_source[0]),
        .I4(pc_source[1]),
        .I5(opcode[9]),
        .O(\pc_reg_reg[11] ));
  LUT6 #(
    .INIT(64'hE2E2FFAAE2E200AA)) 
    \pc_reg[12]_i_1 
       (.I0(pc_plus4[9]),
        .I1(take_branch),
        .I2(pc_new[12]),
        .I3(pc_source[0]),
        .I4(pc_source[1]),
        .I5(opcode[10]),
        .O(\pc_reg_reg[12] ));
  LUT6 #(
    .INIT(64'hE2E2FFAAE2E200AA)) 
    \pc_reg[13]_i_1 
       (.I0(pc_plus4[10]),
        .I1(take_branch),
        .I2(pc_new[13]),
        .I3(pc_source[0]),
        .I4(pc_source[1]),
        .I5(opcode[11]),
        .O(\pc_reg_reg[13] ));
  LUT6 #(
    .INIT(64'hE2E2FFAAE2E200AA)) 
    \pc_reg[14]_i_1 
       (.I0(pc_plus4[11]),
        .I1(take_branch),
        .I2(pc_new[14]),
        .I3(pc_source[0]),
        .I4(pc_source[1]),
        .I5(\pc_reg_reg[14] ),
        .O(\pc_reg_reg[14]_0 ));
  LUT6 #(
    .INIT(64'hE2E2FFAAE2E200AA)) 
    \pc_reg[15]_i_1 
       (.I0(pc_plus4[12]),
        .I1(take_branch),
        .I2(pc_new[15]),
        .I3(pc_source[0]),
        .I4(pc_source[1]),
        .I5(opcode[13]),
        .O(\pc_reg_reg[15] ));
  LUT6 #(
    .INIT(64'hE2E2FFAAE2E200AA)) 
    \pc_reg[16]_i_1 
       (.I0(pc_plus4[13]),
        .I1(take_branch),
        .I2(pc_new[16]),
        .I3(pc_source[0]),
        .I4(pc_source[1]),
        .I5(opcode[14]),
        .O(\pc_reg_reg[16] ));
  LUT6 #(
    .INIT(64'hE2E2FFAAE2E200AA)) 
    \pc_reg[17]_i_1 
       (.I0(pc_plus4[14]),
        .I1(take_branch),
        .I2(pc_new[17]),
        .I3(pc_source[0]),
        .I4(pc_source[1]),
        .I5(opcode[15]),
        .O(\pc_reg_reg[17] ));
  LUT6 #(
    .INIT(64'hE2E2FFAAE2E200AA)) 
    \pc_reg[18]_i_1 
       (.I0(pc_plus4[15]),
        .I1(take_branch),
        .I2(pc_new[18]),
        .I3(pc_source[0]),
        .I4(pc_source[1]),
        .I5(opcode[16]),
        .O(\pc_reg_reg[18] ));
  LUT6 #(
    .INIT(64'hE2E2FFAAE2E200AA)) 
    \pc_reg[19]_i_1 
       (.I0(pc_plus4[16]),
        .I1(take_branch),
        .I2(pc_new[19]),
        .I3(pc_source[0]),
        .I4(pc_source[1]),
        .I5(opcode[17]),
        .O(\pc_reg_reg[19] ));
  LUT6 #(
    .INIT(64'hE2E2FFAAE2E200AA)) 
    \pc_reg[20]_i_1 
       (.I0(pc_plus4[17]),
        .I1(take_branch),
        .I2(pc_new[20]),
        .I3(pc_source[0]),
        .I4(pc_source[1]),
        .I5(opcode[18]),
        .O(\pc_reg_reg[20] ));
  LUT5 #(
    .INIT(32'hFFFEEEFE)) 
    \pc_reg[20]_i_3 
       (.I0(c_alu[20]),
        .I1(c_shift[20]),
        .I2(\upper_reg_reg[20] ),
        .I3(\bram_firmware_code_address_reg[19]_i_10_n_0 ),
        .I4(\lower_reg_reg[31]_5 [20]),
        .O(pc_new[20]));
  LUT6 #(
    .INIT(64'hCACACAC0C0C0CAC0)) 
    \pc_reg[20]_i_5 
       (.I0(\bram_firmware_code_address_reg[19]_i_14_n_0 ),
        .I1(shift16L[20]),
        .I2(\bram_firmware_code_address_reg[15]_i_7_n_0 ),
        .I3(shift8R[20]),
        .I4(a_bus[4]),
        .I5(\bram_firmware_code_address_reg[19]_i_17_n_0 ),
        .O(c_shift[20]));
  LUT6 #(
    .INIT(64'h0000000011050540)) 
    \pc_reg[20]_i_7 
       (.I0(\bram_firmware_code_address_reg[17]_i_9_n_0 ),
        .I1(\bram_firmware_code_address_reg[17]_i_10_n_0 ),
        .I2(\bram_firmware_code_address_reg[17]_i_11_n_0 ),
        .I3(a_bus[20]),
        .I4(b_bus[20]),
        .I5(\u6_alu/c_alu1__0 ),
        .O(\pc_reg[20]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'h9969966696996669)) 
    \pc_reg[20]_i_8 
       (.I0(b_bus[20]),
        .I1(a_bus[20]),
        .I2(\u6_alu/bv_adder27__3 ),
        .I3(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I4(b_bus[19]),
        .I5(a_bus[19]),
        .O(\u6_alu/bv_adder058_out ));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \pc_reg[20]_i_9 
       (.I0(shift4L[4]),
        .I1(a_bus[4]),
        .I2(shift4L[12]),
        .I3(a_bus[3]),
        .I4(shift4L[20]),
        .O(shift16L[20]));
  LUT6 #(
    .INIT(64'hE2E2FFAAE2E200AA)) 
    \pc_reg[21]_i_1 
       (.I0(pc_plus4[18]),
        .I1(take_branch),
        .I2(pc_new[21]),
        .I3(pc_source[0]),
        .I4(pc_source[1]),
        .I5(opcode[19]),
        .O(\pc_reg_reg[21] ));
  LUT5 #(
    .INIT(32'hFFFEEEFE)) 
    \pc_reg[21]_i_3 
       (.I0(c_alu[21]),
        .I1(c_shift[21]),
        .I2(\upper_reg_reg[21] ),
        .I3(\bram_firmware_code_address_reg[19]_i_10_n_0 ),
        .I4(\lower_reg_reg[31]_5 [21]),
        .O(pc_new[21]));
  LUT6 #(
    .INIT(64'h9669FFFF96690000)) 
    \pc_reg[21]_i_4 
       (.I0(b_bus[21]),
        .I1(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I2(a_bus[21]),
        .I3(\u6_alu/bv_adder23__3 ),
        .I4(\u6_alu/c_alu1127_out ),
        .I5(\pc_reg[21]_i_7_n_0 ),
        .O(c_alu[21]));
  LUT6 #(
    .INIT(64'hCACACAC0C0C0CAC0)) 
    \pc_reg[21]_i_5 
       (.I0(\bram_firmware_code_address_reg[19]_i_14_n_0 ),
        .I1(shift16L[21]),
        .I2(\bram_firmware_code_address_reg[15]_i_7_n_0 ),
        .I3(shift8R[21]),
        .I4(a_bus[4]),
        .I5(\bram_firmware_code_address_reg[19]_i_17_n_0 ),
        .O(c_shift[21]));
  LUT6 #(
    .INIT(64'h0000000011050540)) 
    \pc_reg[21]_i_7 
       (.I0(\bram_firmware_code_address_reg[17]_i_9_n_0 ),
        .I1(\bram_firmware_code_address_reg[17]_i_10_n_0 ),
        .I2(\bram_firmware_code_address_reg[17]_i_11_n_0 ),
        .I3(a_bus[21]),
        .I4(b_bus[21]),
        .I5(\u6_alu/c_alu1__0 ),
        .O(\pc_reg[21]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \pc_reg[21]_i_8 
       (.I0(\bram_firmware_code_address_reg[5]_i_9_n_0 ),
        .I1(a_bus[4]),
        .I2(shift4L[13]),
        .I3(a_bus[3]),
        .I4(shift4L[21]),
        .O(shift16L[21]));
  LUT6 #(
    .INIT(64'hE2E2FFAAE2E200AA)) 
    \pc_reg[22]_i_1 
       (.I0(pc_plus4[19]),
        .I1(take_branch),
        .I2(pc_new[22]),
        .I3(pc_source[0]),
        .I4(pc_source[1]),
        .I5(opcode[20]),
        .O(\pc_reg_reg[22] ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \pc_reg[22]_i_10 
       (.I0(\bram_firmware_code_address_reg[6]_i_9_n_0 ),
        .I1(a_bus[4]),
        .I2(shift4L[14]),
        .I3(a_bus[3]),
        .I4(shift4L[22]),
        .O(shift16L[22]));
  LUT6 #(
    .INIT(64'h00FF00000020008A)) 
    \pc_reg[22]_i_12 
       (.I0(\pc_reg_reg[25]_1 ),
        .I1(\upper_reg_reg[31]_0 [15]),
        .I2(\upper_reg_reg[19]_0 ),
        .I3(\pc_reg_reg[30]_0 ),
        .I4(\upper_reg_reg[31]_0 [16]),
        .I5(\pc_reg_reg[25] ),
        .O(\pc_reg[22]_i_12_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFE2)) 
    \pc_reg[22]_i_3 
       (.I0(\pc_reg[22]_i_5_n_0 ),
        .I1(\u6_alu/c_alu1127_out ),
        .I2(\u6_alu/bv_adder064_out ),
        .I3(c_shift[22]),
        .I4(c_mult[22]),
        .O(pc_new[22]));
  LUT6 #(
    .INIT(64'h0000000011050540)) 
    \pc_reg[22]_i_5 
       (.I0(\bram_firmware_code_address_reg[17]_i_9_n_0 ),
        .I1(\bram_firmware_code_address_reg[17]_i_10_n_0 ),
        .I2(\bram_firmware_code_address_reg[17]_i_11_n_0 ),
        .I3(a_bus[22]),
        .I4(b_bus[22]),
        .I5(\u6_alu/c_alu1__0 ),
        .O(\pc_reg[22]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h9969966696996669)) 
    \pc_reg[22]_i_6 
       (.I0(b_bus[22]),
        .I1(a_bus[22]),
        .I2(\u6_alu/bv_adder23__3 ),
        .I3(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I4(b_bus[21]),
        .I5(a_bus[21]),
        .O(\u6_alu/bv_adder064_out ));
  LUT6 #(
    .INIT(64'hCACACAC0C0C0CAC0)) 
    \pc_reg[22]_i_7 
       (.I0(\bram_firmware_code_address_reg[19]_i_14_n_0 ),
        .I1(shift16L[22]),
        .I2(\bram_firmware_code_address_reg[15]_i_7_n_0 ),
        .I3(shift8R[22]),
        .I4(a_bus[4]),
        .I5(\bram_firmware_code_address_reg[19]_i_17_n_0 ),
        .O(c_shift[22]));
  LUT6 #(
    .INIT(64'hFFFFF8000000F400)) 
    \pc_reg[22]_i_8 
       (.I0(\lower_reg_reg[21] ),
        .I1(\pc_reg_reg[30]_0 ),
        .I2(\pc_reg[22]_i_12_n_0 ),
        .I3(\pc_reg_reg[30]_1 ),
        .I4(\bram_firmware_code_address_reg[19]_i_10_n_0 ),
        .I5(\lower_reg_reg[31]_5 [22]),
        .O(c_mult[22]));
  LUT6 #(
    .INIT(64'hFFB2E8FFE80000B2)) 
    \pc_reg[22]_i_9 
       (.I0(\u6_alu/bv_adder27__3 ),
        .I1(b_bus[19]),
        .I2(a_bus[19]),
        .I3(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I4(b_bus[20]),
        .I5(a_bus[20]),
        .O(\u6_alu/bv_adder23__3 ));
  LUT6 #(
    .INIT(64'hE2E2FFAAE2E200AA)) 
    \pc_reg[23]_i_1 
       (.I0(pc_plus4[20]),
        .I1(take_branch),
        .I2(pc_new[23]),
        .I3(pc_source[0]),
        .I4(pc_source[1]),
        .I5(opcode[21]),
        .O(\pc_reg_reg[23] ));
  LUT5 #(
    .INIT(32'hFFFEEEFE)) 
    \pc_reg[23]_i_3 
       (.I0(c_alu[23]),
        .I1(c_shift[23]),
        .I2(\upper_reg_reg[23] ),
        .I3(\bram_firmware_code_address_reg[19]_i_10_n_0 ),
        .I4(\lower_reg_reg[31]_5 [23]),
        .O(pc_new[23]));
  LUT6 #(
    .INIT(64'hCACACAC0C0C0CAC0)) 
    \pc_reg[23]_i_5 
       (.I0(\bram_firmware_code_address_reg[19]_i_14_n_0 ),
        .I1(shift16L[23]),
        .I2(\bram_firmware_code_address_reg[15]_i_7_n_0 ),
        .I3(shift8R[23]),
        .I4(a_bus[4]),
        .I5(\bram_firmware_code_address_reg[19]_i_17_n_0 ),
        .O(c_shift[23]));
  LUT6 #(
    .INIT(64'h0000000011050540)) 
    \pc_reg[23]_i_7 
       (.I0(\bram_firmware_code_address_reg[17]_i_9_n_0 ),
        .I1(\bram_firmware_code_address_reg[17]_i_10_n_0 ),
        .I2(\bram_firmware_code_address_reg[17]_i_11_n_0 ),
        .I3(a_bus[23]),
        .I4(b_bus[23]),
        .I5(\u6_alu/c_alu1__0 ),
        .O(\pc_reg[23]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'h9969966696996669)) 
    \pc_reg[23]_i_8 
       (.I0(b_bus[23]),
        .I1(a_bus[23]),
        .I2(\u6_alu/bv_adder21__3 ),
        .I3(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I4(b_bus[22]),
        .I5(a_bus[22]),
        .O(\u6_alu/bv_adder067_out ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \pc_reg[23]_i_9 
       (.I0(\bram_firmware_code_address_reg[7]_i_9_n_0 ),
        .I1(a_bus[4]),
        .I2(shift4L[15]),
        .I3(a_bus[3]),
        .I4(shift4L[23]),
        .O(shift16L[23]));
  LUT6 #(
    .INIT(64'hE2E2FFAAE2E200AA)) 
    \pc_reg[24]_i_1 
       (.I0(pc_plus4[21]),
        .I1(take_branch),
        .I2(pc_new[24]),
        .I3(pc_source[0]),
        .I4(pc_source[1]),
        .I5(opcode[22]),
        .O(\pc_reg_reg[24] ));
  LUT6 #(
    .INIT(64'hFFFFF8000000F400)) 
    \pc_reg[24]_i_10 
       (.I0(\lower_reg_reg[22] ),
        .I1(\pc_reg_reg[30]_0 ),
        .I2(\pc_reg[24]_i_15_n_0 ),
        .I3(\pc_reg_reg[30]_1 ),
        .I4(\bram_firmware_code_address_reg[19]_i_10_n_0 ),
        .I5(\lower_reg_reg[31]_5 [24]),
        .O(c_mult[24]));
  (* SOFT_HLUTNM = "soft_lutpair87" *) 
  LUT4 #(
    .INIT(16'hA808)) 
    \pc_reg[24]_i_11 
       (.I0(\bram_firmware_code_address_reg[19]_i_14_n_0 ),
        .I1(shift8R[24]),
        .I2(a_bus[4]),
        .I3(\bram_firmware_code_address_reg[19]_i_17_n_0 ),
        .O(\pc_reg[24]_i_11_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \pc_reg[24]_i_12 
       (.I0(shift4L[16]),
        .I1(a_bus[3]),
        .I2(shift2L[20]),
        .I3(a_bus[2]),
        .I4(shift2L[24]),
        .O(shift8L[24]));
  LUT6 #(
    .INIT(64'hFFB2E8FFE80000B2)) 
    \pc_reg[24]_i_13 
       (.I0(\u6_alu/bv_adder29__3 ),
        .I1(b_bus[18]),
        .I2(a_bus[18]),
        .I3(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I4(b_bus[19]),
        .I5(a_bus[19]),
        .O(\u6_alu/bv_adder25__3 ));
  LUT5 #(
    .INIT(32'h0F000802)) 
    \pc_reg[24]_i_15 
       (.I0(\pc_reg_reg[25]_1 ),
        .I1(\upper_reg_reg[22] ),
        .I2(\pc_reg_reg[30]_0 ),
        .I3(\upper_reg_reg[31]_0 [17]),
        .I4(\pc_reg_reg[25] ),
        .O(\pc_reg[24]_i_15_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFEE0FEE00000)) 
    \pc_reg[24]_i_16 
       (.I0(bv_adder3444_out),
        .I1(bv_adder34__0),
        .I2(\pc_reg[24]_i_20_n_0 ),
        .I3(a_bus[16]),
        .I4(\pc_reg[24]_i_21_n_0 ),
        .I5(a_bus[17]),
        .O(\u6_alu/bv_adder29__3 ));
  LUT6 #(
    .INIT(64'hEEE8E88800000000)) 
    \pc_reg[24]_i_18 
       (.I0(a_bus[14]),
        .I1(\pc_reg[24]_i_22_n_0 ),
        .I2(a_bus[13]),
        .I3(\pc_reg[24]_i_23_n_0 ),
        .I4(\u6_alu/bv_adder39__3 ),
        .I5(bv_adder3558_out),
        .O(bv_adder3444_out));
  (* SOFT_HLUTNM = "soft_lutpair137" *) 
  LUT3 #(
    .INIT(8'h82)) 
    \pc_reg[24]_i_19 
       (.I0(a_bus[15]),
        .I1(b_bus[15]),
        .I2(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .O(bv_adder34__0));
  (* SOFT_HLUTNM = "soft_lutpair150" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \pc_reg[24]_i_20 
       (.I0(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I1(b_bus[16]),
        .O(\pc_reg[24]_i_20_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair155" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \pc_reg[24]_i_21 
       (.I0(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I1(b_bus[17]),
        .O(\pc_reg[24]_i_21_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair153" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \pc_reg[24]_i_22 
       (.I0(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I1(b_bus[14]),
        .O(\pc_reg[24]_i_22_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair147" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \pc_reg[24]_i_23 
       (.I0(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I1(b_bus[13]),
        .O(\pc_reg[24]_i_23_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair137" *) 
  LUT3 #(
    .INIT(8'hEB)) 
    \pc_reg[24]_i_24 
       (.I0(a_bus[15]),
        .I1(b_bus[15]),
        .I2(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .O(bv_adder3558_out));
  LUT6 #(
    .INIT(64'hFFFFFFFFE22E2EE2)) 
    \pc_reg[24]_i_3 
       (.I0(\pc_reg[24]_i_4_n_0 ),
        .I1(\u6_alu/c_alu1127_out ),
        .I2(\u6_alu/bv_adder17__3 ),
        .I3(a_bus[24]),
        .I4(\pc_reg[24]_i_7_n_0 ),
        .I5(\pc_reg[24]_i_8_n_0 ),
        .O(pc_new[24]));
  LUT6 #(
    .INIT(64'h0000000011050540)) 
    \pc_reg[24]_i_4 
       (.I0(\bram_firmware_code_address_reg[17]_i_9_n_0 ),
        .I1(\bram_firmware_code_address_reg[17]_i_10_n_0 ),
        .I2(\bram_firmware_code_address_reg[17]_i_11_n_0 ),
        .I3(a_bus[24]),
        .I4(b_bus[24]),
        .I5(\u6_alu/c_alu1__0 ),
        .O(\pc_reg[24]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFB2E8FFE80000B2)) 
    \pc_reg[24]_i_5 
       (.I0(\u6_alu/bv_adder21__3 ),
        .I1(b_bus[22]),
        .I2(a_bus[22]),
        .I3(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I4(b_bus[23]),
        .I5(a_bus[23]),
        .O(\u6_alu/bv_adder17__3 ));
  LUT4 #(
    .INIT(16'h88B8)) 
    \pc_reg[24]_i_6 
       (.I0(pc_current[24]),
        .I1(\upper_reg[0]_i_5_n_0 ),
        .I2(reg_source[24]),
        .I3(\upper_reg[0]_i_4_n_0 ),
        .O(a_bus[24]));
  (* SOFT_HLUTNM = "soft_lutpair156" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \pc_reg[24]_i_7 
       (.I0(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I1(b_bus[24]),
        .O(\pc_reg[24]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hFEFEFEAEAEAEFEAE)) 
    \pc_reg[24]_i_8 
       (.I0(c_mult[24]),
        .I1(\pc_reg[24]_i_11_n_0 ),
        .I2(\bram_firmware_code_address_reg[15]_i_7_n_0 ),
        .I3(shift8L[24]),
        .I4(a_bus[4]),
        .I5(shift8L[8]),
        .O(\pc_reg[24]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hFFB2E8FFE80000B2)) 
    \pc_reg[24]_i_9 
       (.I0(\u6_alu/bv_adder25__3 ),
        .I1(b_bus[20]),
        .I2(a_bus[20]),
        .I3(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I4(b_bus[21]),
        .I5(a_bus[21]),
        .O(\u6_alu/bv_adder21__3 ));
  LUT6 #(
    .INIT(64'hE2E2FFAAE2E200AA)) 
    \pc_reg[25]_i_1 
       (.I0(pc_plus4[22]),
        .I1(take_branch),
        .I2(pc_new[25]),
        .I3(pc_source[0]),
        .I4(pc_source[1]),
        .I5(opcode[23]),
        .O(\pc_reg_reg[25]_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \pc_reg[25]_i_12 
       (.I0(shift4L[17]),
        .I1(a_bus[3]),
        .I2(shift2L[21]),
        .I3(a_bus[2]),
        .I4(shift2L[25]),
        .O(shift8L[25]));
  LUT5 #(
    .INIT(32'hFFFEEEFE)) 
    \pc_reg[25]_i_3 
       (.I0(c_alu[25]),
        .I1(c_shift[25]),
        .I2(\upper_reg_reg[25] ),
        .I3(\bram_firmware_code_address_reg[19]_i_10_n_0 ),
        .I4(\lower_reg_reg[31]_5 [25]),
        .O(pc_new[25]));
  LUT6 #(
    .INIT(64'hCACACAC0C0C0CAC0)) 
    \pc_reg[25]_i_5 
       (.I0(\bram_firmware_code_address_reg[19]_i_14_n_0 ),
        .I1(shift16L[25]),
        .I2(\bram_firmware_code_address_reg[15]_i_7_n_0 ),
        .I3(shift8R[25]),
        .I4(a_bus[4]),
        .I5(\bram_firmware_code_address_reg[19]_i_17_n_0 ),
        .O(c_shift[25]));
  LUT6 #(
    .INIT(64'h0000000011050540)) 
    \pc_reg[25]_i_7 
       (.I0(\bram_firmware_code_address_reg[17]_i_9_n_0 ),
        .I1(\bram_firmware_code_address_reg[17]_i_10_n_0 ),
        .I2(\bram_firmware_code_address_reg[17]_i_11_n_0 ),
        .I3(a_bus[25]),
        .I4(b_bus[25]),
        .I5(\u6_alu/c_alu1__0 ),
        .O(\pc_reg[25]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'h9969966696996669)) 
    \pc_reg[25]_i_8 
       (.I0(b_bus[25]),
        .I1(a_bus[25]),
        .I2(\u6_alu/bv_adder17__3 ),
        .I3(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I4(b_bus[24]),
        .I5(a_bus[24]),
        .O(\u6_alu/bv_adder073_out ));
  LUT3 #(
    .INIT(8'hB8)) 
    \pc_reg[25]_i_9 
       (.I0(shift8L[9]),
        .I1(a_bus[4]),
        .I2(shift8L[25]),
        .O(shift16L[25]));
  LUT6 #(
    .INIT(64'hE2E2FFAAE2E200AA)) 
    \pc_reg[26]_i_1 
       (.I0(pc_plus4[23]),
        .I1(take_branch),
        .I2(pc_new[26]),
        .I3(pc_source[0]),
        .I4(pc_source[1]),
        .I5(opcode[24]),
        .O(\pc_reg_reg[26] ));
  (* SOFT_HLUTNM = "soft_lutpair87" *) 
  LUT4 #(
    .INIT(16'hA808)) 
    \pc_reg[26]_i_10 
       (.I0(\bram_firmware_code_address_reg[19]_i_14_n_0 ),
        .I1(shift8R[26]),
        .I2(a_bus[4]),
        .I3(\bram_firmware_code_address_reg[19]_i_17_n_0 ),
        .O(\pc_reg[26]_i_10_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \pc_reg[26]_i_11 
       (.I0(shift4L[18]),
        .I1(a_bus[3]),
        .I2(shift2L[22]),
        .I3(a_bus[2]),
        .I4(shift2L[26]),
        .O(shift8L[26]));
  LUT5 #(
    .INIT(32'h0F000802)) 
    \pc_reg[26]_i_12 
       (.I0(\pc_reg_reg[25]_1 ),
        .I1(\upper_reg_reg[24] ),
        .I2(\pc_reg_reg[30]_0 ),
        .I3(\upper_reg_reg[31]_0 [18]),
        .I4(\pc_reg_reg[25] ),
        .O(\pc_reg[26]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pc_reg[26]_i_13 
       (.I0(b_bus[19]),
        .I1(b_bus[20]),
        .I2(a_bus[1]),
        .I3(b_bus[21]),
        .I4(a_bus[0]),
        .I5(b_bus[22]),
        .O(shift2L[22]));
  LUT6 #(
    .INIT(64'hFFFFFFFFE22E2EE2)) 
    \pc_reg[26]_i_3 
       (.I0(\pc_reg[26]_i_4_n_0 ),
        .I1(\u6_alu/c_alu1127_out ),
        .I2(\u6_alu/bv_adder13__3 ),
        .I3(a_bus[26]),
        .I4(\pc_reg[26]_i_7_n_0 ),
        .I5(\pc_reg[26]_i_8_n_0 ),
        .O(pc_new[26]));
  LUT6 #(
    .INIT(64'h0000000011050540)) 
    \pc_reg[26]_i_4 
       (.I0(\bram_firmware_code_address_reg[17]_i_9_n_0 ),
        .I1(\bram_firmware_code_address_reg[17]_i_10_n_0 ),
        .I2(\bram_firmware_code_address_reg[17]_i_11_n_0 ),
        .I3(a_bus[26]),
        .I4(b_bus[26]),
        .I5(\u6_alu/c_alu1__0 ),
        .O(\pc_reg[26]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFB2E8FFE80000B2)) 
    \pc_reg[26]_i_5 
       (.I0(\u6_alu/bv_adder17__3 ),
        .I1(b_bus[24]),
        .I2(a_bus[24]),
        .I3(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I4(b_bus[25]),
        .I5(a_bus[25]),
        .O(\u6_alu/bv_adder13__3 ));
  LUT4 #(
    .INIT(16'h88B8)) 
    \pc_reg[26]_i_6 
       (.I0(pc_current[26]),
        .I1(\upper_reg[0]_i_5_n_0 ),
        .I2(reg_source[26]),
        .I3(\upper_reg[0]_i_4_n_0 ),
        .O(a_bus[26]));
  (* SOFT_HLUTNM = "soft_lutpair156" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \pc_reg[26]_i_7 
       (.I0(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I1(b_bus[26]),
        .O(\pc_reg[26]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hFEFEFEAEAEAEFEAE)) 
    \pc_reg[26]_i_8 
       (.I0(c_mult[26]),
        .I1(\pc_reg[26]_i_10_n_0 ),
        .I2(\bram_firmware_code_address_reg[15]_i_7_n_0 ),
        .I3(shift8L[26]),
        .I4(a_bus[4]),
        .I5(shift8L[10]),
        .O(\pc_reg[26]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF8000000F400)) 
    \pc_reg[26]_i_9 
       (.I0(\lower_reg_reg[24] ),
        .I1(\pc_reg_reg[30]_0 ),
        .I2(\pc_reg[26]_i_12_n_0 ),
        .I3(\pc_reg_reg[30]_1 ),
        .I4(\bram_firmware_code_address_reg[19]_i_10_n_0 ),
        .I5(\lower_reg_reg[31]_5 [26]),
        .O(c_mult[26]));
  LUT6 #(
    .INIT(64'hE2E2FFAAE2E200AA)) 
    \pc_reg[27]_i_1 
       (.I0(pc_plus4[24]),
        .I1(take_branch),
        .I2(pc_new[27]),
        .I3(pc_source[0]),
        .I4(pc_source[1]),
        .I5(opcode[25]),
        .O(\pc_reg_reg[27] ));
  LUT6 #(
    .INIT(64'hFFFFF8000000F400)) 
    \pc_reg[27]_i_10 
       (.I0(\lower_reg_reg[26]_0 ),
        .I1(\pc_reg_reg[30]_0 ),
        .I2(\pc_reg[27]_i_15_n_0 ),
        .I3(\pc_reg_reg[30]_1 ),
        .I4(\bram_firmware_code_address_reg[19]_i_10_n_0 ),
        .I5(\lower_reg_reg[31]_5 [27]),
        .O(c_mult[27]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT5 #(
    .INIT(32'hAA00A808)) 
    \pc_reg[27]_i_11 
       (.I0(\bram_firmware_code_address_reg[19]_i_14_n_0 ),
        .I1(shift4R[27]),
        .I2(a_bus[3]),
        .I3(\bram_firmware_code_address_reg[19]_i_17_n_0 ),
        .I4(a_bus[4]),
        .O(\pc_reg[27]_i_11_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \pc_reg[27]_i_12 
       (.I0(shift4L[19]),
        .I1(a_bus[3]),
        .I2(shift2L[23]),
        .I3(a_bus[2]),
        .I4(shift2L[27]),
        .O(shift8L[27]));
  LUT6 #(
    .INIT(64'hFFFFFEE0FEE00000)) 
    \pc_reg[27]_i_13 
       (.I0(bv_adder2459_out),
        .I1(bv_adder24__0),
        .I2(\pc_reg[27]_i_19_n_0 ),
        .I3(a_bus[21]),
        .I4(\pc_reg[27]_i_20_n_0 ),
        .I5(a_bus[22]),
        .O(\u6_alu/bv_adder19__3 ));
  LUT6 #(
    .INIT(64'h00FF00000020008A)) 
    \pc_reg[27]_i_15 
       (.I0(\pc_reg_reg[25]_1 ),
        .I1(\upper_reg_reg[31]_0 [18]),
        .I2(\upper_reg_reg[24] ),
        .I3(\pc_reg_reg[30]_0 ),
        .I4(\upper_reg_reg[31]_0 [19]),
        .I5(\pc_reg_reg[25] ),
        .O(\pc_reg[27]_i_15_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pc_reg[27]_i_16 
       (.I0(b_bus[20]),
        .I1(b_bus[21]),
        .I2(a_bus[1]),
        .I3(b_bus[22]),
        .I4(a_bus[0]),
        .I5(b_bus[23]),
        .O(shift2L[23]));
  LUT6 #(
    .INIT(64'hEEE8E88800000000)) 
    \pc_reg[27]_i_17 
       (.I0(a_bus[19]),
        .I1(\pc_reg[27]_i_21_n_0 ),
        .I2(a_bus[18]),
        .I3(\pc_reg[27]_i_22_n_0 ),
        .I4(\u6_alu/bv_adder29__3 ),
        .I5(bv_adder2578_out),
        .O(bv_adder2459_out));
  (* SOFT_HLUTNM = "soft_lutpair134" *) 
  LUT3 #(
    .INIT(8'h82)) 
    \pc_reg[27]_i_18 
       (.I0(a_bus[20]),
        .I1(b_bus[20]),
        .I2(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .O(bv_adder24__0));
  (* SOFT_HLUTNM = "soft_lutpair146" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \pc_reg[27]_i_19 
       (.I0(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I1(b_bus[21]),
        .O(\pc_reg[27]_i_19_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair153" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \pc_reg[27]_i_20 
       (.I0(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I1(b_bus[22]),
        .O(\pc_reg[27]_i_20_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair145" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \pc_reg[27]_i_21 
       (.I0(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I1(b_bus[19]),
        .O(\pc_reg[27]_i_21_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair144" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \pc_reg[27]_i_22 
       (.I0(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I1(b_bus[18]),
        .O(\pc_reg[27]_i_22_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair134" *) 
  LUT3 #(
    .INIT(8'hEB)) 
    \pc_reg[27]_i_23 
       (.I0(a_bus[20]),
        .I1(b_bus[20]),
        .I2(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .O(bv_adder2578_out));
  LUT6 #(
    .INIT(64'hFFFFFFFFE22E2EE2)) 
    \pc_reg[27]_i_3 
       (.I0(\pc_reg[27]_i_4_n_0 ),
        .I1(\u6_alu/c_alu1127_out ),
        .I2(\u6_alu/bv_adder11__3 ),
        .I3(a_bus[27]),
        .I4(\pc_reg[27]_i_7_n_0 ),
        .I5(\pc_reg[27]_i_8_n_0 ),
        .O(pc_new[27]));
  LUT6 #(
    .INIT(64'h0000000011050540)) 
    \pc_reg[27]_i_4 
       (.I0(\bram_firmware_code_address_reg[17]_i_9_n_0 ),
        .I1(\bram_firmware_code_address_reg[17]_i_10_n_0 ),
        .I2(\bram_firmware_code_address_reg[17]_i_11_n_0 ),
        .I3(a_bus[27]),
        .I4(b_bus[27]),
        .I5(\u6_alu/c_alu1__0 ),
        .O(\pc_reg[27]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFB2E8FFE80000B2)) 
    \pc_reg[27]_i_5 
       (.I0(\u6_alu/bv_adder15__3 ),
        .I1(b_bus[25]),
        .I2(a_bus[25]),
        .I3(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I4(b_bus[26]),
        .I5(a_bus[26]),
        .O(\u6_alu/bv_adder11__3 ));
  LUT4 #(
    .INIT(16'h88B8)) 
    \pc_reg[27]_i_6 
       (.I0(pc_current[27]),
        .I1(\upper_reg[0]_i_5_n_0 ),
        .I2(reg_source[27]),
        .I3(\upper_reg[0]_i_4_n_0 ),
        .O(a_bus[27]));
  (* SOFT_HLUTNM = "soft_lutpair161" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \pc_reg[27]_i_7 
       (.I0(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I1(b_bus[27]),
        .O(\pc_reg[27]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hFEFEFEAEAEAEFEAE)) 
    \pc_reg[27]_i_8 
       (.I0(c_mult[27]),
        .I1(\pc_reg[27]_i_11_n_0 ),
        .I2(\bram_firmware_code_address_reg[15]_i_7_n_0 ),
        .I3(shift8L[27]),
        .I4(a_bus[4]),
        .I5(shift8L[11]),
        .O(\pc_reg[27]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hFFB2E8FFE80000B2)) 
    \pc_reg[27]_i_9 
       (.I0(\u6_alu/bv_adder19__3 ),
        .I1(b_bus[23]),
        .I2(a_bus[23]),
        .I3(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I4(b_bus[24]),
        .I5(a_bus[24]),
        .O(\u6_alu/bv_adder15__3 ));
  LUT6 #(
    .INIT(64'hF606F606CCCC6666)) 
    \pc_reg[28]_i_1 
       (.I0(p_48_in),
        .I1(pc_current[28]),
        .I2(take_branch),
        .I3(pc_new[28]),
        .I4(pc_source[0]),
        .I5(pc_source[1]),
        .O(\pc_reg_reg[28] ));
  LUT6 #(
    .INIT(64'h9969966696996669)) 
    \pc_reg[28]_i_10 
       (.I0(b_bus[28]),
        .I1(a_bus[28]),
        .I2(\u6_alu/bv_adder11__3 ),
        .I3(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I4(b_bus[27]),
        .I5(a_bus[27]),
        .O(\u6_alu/bv_adder082_out ));
  (* SOFT_HLUTNM = "soft_lutpair116" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \pc_reg[28]_i_11 
       (.I0(shift2L[16]),
        .I1(a_bus[2]),
        .I2(shift2L[20]),
        .O(shift4L[20]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \pc_reg[28]_i_12 
       (.I0(shift2L[24]),
        .I1(a_bus[2]),
        .I2(shift1L[26]),
        .I3(a_bus[1]),
        .I4(shift1L[28]),
        .O(shift4L[28]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pc_reg[28]_i_13 
       (.I0(b_bus[31]),
        .I1(b_bus[30]),
        .I2(a_bus[1]),
        .I3(b_bus[29]),
        .I4(a_bus[0]),
        .I5(b_bus[28]),
        .O(shift2R[28]));
  LUT5 #(
    .INIT(32'h0F000802)) 
    \pc_reg[28]_i_15 
       (.I0(\pc_reg_reg[25]_1 ),
        .I1(\upper_reg_reg[26] ),
        .I2(\pc_reg_reg[30]_0 ),
        .I3(\upper_reg_reg[31]_0 [20]),
        .I4(\pc_reg_reg[25] ),
        .O(\pc_reg[28]_i_15_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pc_reg[28]_i_16 
       (.I0(b_bus[13]),
        .I1(b_bus[14]),
        .I2(a_bus[1]),
        .I3(b_bus[15]),
        .I4(a_bus[0]),
        .I5(b_bus[16]),
        .O(shift2L[16]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pc_reg[28]_i_17 
       (.I0(b_bus[17]),
        .I1(b_bus[18]),
        .I2(a_bus[1]),
        .I3(b_bus[19]),
        .I4(a_bus[0]),
        .I5(b_bus[20]),
        .O(shift2L[20]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pc_reg[28]_i_18 
       (.I0(b_bus[21]),
        .I1(b_bus[22]),
        .I2(a_bus[1]),
        .I3(b_bus[23]),
        .I4(a_bus[0]),
        .I5(b_bus[24]),
        .O(shift2L[24]));
  (* SOFT_HLUTNM = "soft_lutpair100" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \pc_reg[28]_i_19 
       (.I0(b_bus[25]),
        .I1(a_bus[0]),
        .I2(b_bus[26]),
        .O(shift1L[26]));
  (* SOFT_HLUTNM = "soft_lutpair90" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \pc_reg[28]_i_20 
       (.I0(b_bus[27]),
        .I1(a_bus[0]),
        .I2(b_bus[28]),
        .O(shift1L[28]));
  LUT5 #(
    .INIT(32'hFFFFEFEA)) 
    \pc_reg[28]_i_3 
       (.I0(c_alu[28]),
        .I1(shift16L[28]),
        .I2(\bram_firmware_code_address_reg[15]_i_7_n_0 ),
        .I3(\pc_reg[28]_i_7_n_0 ),
        .I4(c_mult[28]),
        .O(pc_new[28]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \pc_reg[28]_i_6 
       (.I0(shift8L[12]),
        .I1(a_bus[4]),
        .I2(shift4L[20]),
        .I3(a_bus[3]),
        .I4(shift4L[28]),
        .O(shift16L[28]));
  LUT6 #(
    .INIT(64'hAA00AA00AA00A808)) 
    \pc_reg[28]_i_7 
       (.I0(\bram_firmware_code_address_reg[19]_i_14_n_0 ),
        .I1(shift2R[28]),
        .I2(a_bus[2]),
        .I3(\bram_firmware_code_address_reg[19]_i_17_n_0 ),
        .I4(a_bus[3]),
        .I5(a_bus[4]),
        .O(\pc_reg[28]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF8000000F400)) 
    \pc_reg[28]_i_8 
       (.I0(\lower_reg_reg[26] ),
        .I1(\pc_reg_reg[30]_0 ),
        .I2(\pc_reg[28]_i_15_n_0 ),
        .I3(\pc_reg_reg[30]_1 ),
        .I4(\bram_firmware_code_address_reg[19]_i_10_n_0 ),
        .I5(\lower_reg_reg[31]_5 [28]),
        .O(c_mult[28]));
  LUT6 #(
    .INIT(64'h0000000011050540)) 
    \pc_reg[28]_i_9 
       (.I0(\bram_firmware_code_address_reg[17]_i_9_n_0 ),
        .I1(\bram_firmware_code_address_reg[17]_i_10_n_0 ),
        .I2(\bram_firmware_code_address_reg[17]_i_11_n_0 ),
        .I3(a_bus[28]),
        .I4(b_bus[28]),
        .I5(\u6_alu/c_alu1__0 ),
        .O(\pc_reg[28]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hE2E2FFAAE2E200AA)) 
    \pc_reg[29]_i_1 
       (.I0(pc_plus4[25]),
        .I1(take_branch),
        .I2(pc_new[29]),
        .I3(pc_source[0]),
        .I4(pc_source[1]),
        .I5(pc_current[29]),
        .O(\pc_reg_reg[29] ));
  LUT6 #(
    .INIT(64'hAA00AA00AA00A808)) 
    \pc_reg[29]_i_10 
       (.I0(\bram_firmware_code_address_reg[19]_i_14_n_0 ),
        .I1(shift2R[29]),
        .I2(a_bus[2]),
        .I3(\bram_firmware_code_address_reg[19]_i_17_n_0 ),
        .I4(a_bus[3]),
        .I5(a_bus[4]),
        .O(\pc_reg[29]_i_10_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \pc_reg[29]_i_11 
       (.I0(shift4L[21]),
        .I1(a_bus[3]),
        .I2(shift2L[25]),
        .I3(a_bus[2]),
        .I4(shift2L[29]),
        .O(shift8L[29]));
  LUT5 #(
    .INIT(32'h0F000802)) 
    \pc_reg[29]_i_13 
       (.I0(\pc_reg_reg[25]_1 ),
        .I1(\upper_reg_reg[27] ),
        .I2(\pc_reg_reg[30]_0 ),
        .I3(\upper_reg_reg[31]_0 [21]),
        .I4(\pc_reg_reg[25] ),
        .O(\pc_reg[29]_i_13_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair114" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \pc_reg[29]_i_14 
       (.I0(shift2L[17]),
        .I1(a_bus[2]),
        .I2(shift2L[21]),
        .O(shift4L[21]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pc_reg[29]_i_15 
       (.I0(b_bus[22]),
        .I1(b_bus[23]),
        .I2(a_bus[1]),
        .I3(b_bus[24]),
        .I4(a_bus[0]),
        .I5(b_bus[25]),
        .O(shift2L[25]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pc_reg[29]_i_16 
       (.I0(b_bus[26]),
        .I1(b_bus[27]),
        .I2(a_bus[1]),
        .I3(b_bus[28]),
        .I4(a_bus[0]),
        .I5(b_bus[29]),
        .O(shift2L[29]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pc_reg[29]_i_18 
       (.I0(b_bus[18]),
        .I1(b_bus[19]),
        .I2(a_bus[1]),
        .I3(b_bus[20]),
        .I4(a_bus[0]),
        .I5(b_bus[21]),
        .O(shift2L[21]));
  LUT6 #(
    .INIT(64'hFFFFFFFFE22E2EE2)) 
    \pc_reg[29]_i_3 
       (.I0(\pc_reg[29]_i_4_n_0 ),
        .I1(\u6_alu/c_alu1127_out ),
        .I2(\u6_alu/bv_adder7__3 ),
        .I3(a_bus[29]),
        .I4(\pc_reg[29]_i_7_n_0 ),
        .I5(\pc_reg[29]_i_8_n_0 ),
        .O(pc_new[29]));
  LUT6 #(
    .INIT(64'h0000000011050540)) 
    \pc_reg[29]_i_4 
       (.I0(\bram_firmware_code_address_reg[17]_i_9_n_0 ),
        .I1(\bram_firmware_code_address_reg[17]_i_10_n_0 ),
        .I2(\bram_firmware_code_address_reg[17]_i_11_n_0 ),
        .I3(a_bus[29]),
        .I4(b_bus[29]),
        .I5(\u6_alu/c_alu1__0 ),
        .O(\pc_reg[29]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFB2E8FFE80000B2)) 
    \pc_reg[29]_i_5 
       (.I0(\u6_alu/bv_adder11__3 ),
        .I1(b_bus[27]),
        .I2(a_bus[27]),
        .I3(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I4(b_bus[28]),
        .I5(a_bus[28]),
        .O(\u6_alu/bv_adder7__3 ));
  LUT4 #(
    .INIT(16'h88B8)) 
    \pc_reg[29]_i_6 
       (.I0(pc_current[29]),
        .I1(\upper_reg[0]_i_5_n_0 ),
        .I2(reg_source[29]),
        .I3(\upper_reg[0]_i_4_n_0 ),
        .O(a_bus[29]));
  LUT2 #(
    .INIT(4'h9)) 
    \pc_reg[29]_i_7 
       (.I0(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I1(b_bus[29]),
        .O(\pc_reg[29]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hFEFEFEAEAEAEFEAE)) 
    \pc_reg[29]_i_8 
       (.I0(c_mult[29]),
        .I1(\pc_reg[29]_i_10_n_0 ),
        .I2(\bram_firmware_code_address_reg[15]_i_7_n_0 ),
        .I3(shift8L[29]),
        .I4(a_bus[4]),
        .I5(shift8L[13]),
        .O(\pc_reg[29]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF8000000F400)) 
    \pc_reg[29]_i_9 
       (.I0(\lower_reg_reg[27] ),
        .I1(\pc_reg_reg[30]_0 ),
        .I2(\pc_reg[29]_i_13_n_0 ),
        .I3(\pc_reg_reg[30]_1 ),
        .I4(\bram_firmware_code_address_reg[19]_i_10_n_0 ),
        .I5(\lower_reg_reg[31]_5 [29]),
        .O(c_mult[29]));
  LUT6 #(
    .INIT(64'hD1D1FF55D1D10055)) 
    \pc_reg[2]_i_1 
       (.I0(pc_current[2]),
        .I1(take_branch),
        .I2(pc_new[2]),
        .I3(pc_source[0]),
        .I4(pc_source[1]),
        .I5(opcode[0]),
        .O(\pc_reg_reg[2] ));
  LUT6 #(
    .INIT(64'hE2E2FFAAE2E200AA)) 
    \pc_reg[30]_i_1 
       (.I0(pc_plus4[26]),
        .I1(take_branch),
        .I2(pc_new[30]),
        .I3(pc_source[0]),
        .I4(pc_source[1]),
        .I5(pc_current[30]),
        .O(\pc_reg_reg[30] ));
  LUT5 #(
    .INIT(32'h0F000802)) 
    \pc_reg[30]_i_10 
       (.I0(\pc_reg_reg[25]_1 ),
        .I1(\upper_reg_reg[28] ),
        .I2(\pc_reg_reg[30]_0 ),
        .I3(\upper_reg_reg[31]_0 [22]),
        .I4(\pc_reg_reg[25] ),
        .O(\pc_reg[30]_i_10_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair121" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \pc_reg[30]_i_11 
       (.I0(shift2L[18]),
        .I1(a_bus[2]),
        .I2(shift2L[22]),
        .O(shift4L[22]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pc_reg[30]_i_12 
       (.I0(b_bus[23]),
        .I1(b_bus[24]),
        .I2(a_bus[1]),
        .I3(b_bus[25]),
        .I4(a_bus[0]),
        .I5(b_bus[26]),
        .O(shift2L[26]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pc_reg[30]_i_13 
       (.I0(b_bus[27]),
        .I1(b_bus[28]),
        .I2(a_bus[1]),
        .I3(b_bus[29]),
        .I4(a_bus[0]),
        .I5(b_bus[30]),
        .O(shift2L[30]));
  LUT5 #(
    .INIT(32'hFFFFFFE2)) 
    \pc_reg[30]_i_3 
       (.I0(\pc_reg[30]_i_4_n_0 ),
        .I1(\u6_alu/c_alu1127_out ),
        .I2(\u6_alu/bv_adder088_out ),
        .I3(c_shift[30]),
        .I4(c_mult[30]),
        .O(pc_new[30]));
  LUT6 #(
    .INIT(64'h0000000011050540)) 
    \pc_reg[30]_i_4 
       (.I0(\bram_firmware_code_address_reg[17]_i_9_n_0 ),
        .I1(\bram_firmware_code_address_reg[17]_i_10_n_0 ),
        .I2(\bram_firmware_code_address_reg[17]_i_11_n_0 ),
        .I3(a_bus[30]),
        .I4(b_bus[30]),
        .I5(\u6_alu/c_alu1__0 ),
        .O(\pc_reg[30]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h9969966696996669)) 
    \pc_reg[30]_i_5 
       (.I0(b_bus[30]),
        .I1(a_bus[30]),
        .I2(\u6_alu/bv_adder7__3 ),
        .I3(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I4(b_bus[29]),
        .I5(a_bus[29]),
        .O(\u6_alu/bv_adder088_out ));
  LUT6 #(
    .INIT(64'hCFC0AAAACFC00000)) 
    \pc_reg[30]_i_6 
       (.I0(\bram_firmware_code_address_reg[19]_i_14_n_0 ),
        .I1(shift8L[14]),
        .I2(a_bus[4]),
        .I3(shift8L[30]),
        .I4(\bram_firmware_code_address_reg[15]_i_7_n_0 ),
        .I5(shift16R),
        .O(c_shift[30]));
  LUT6 #(
    .INIT(64'hFFFFF8000000F400)) 
    \pc_reg[30]_i_7 
       (.I0(\lower_reg_reg[28] ),
        .I1(\pc_reg_reg[30]_0 ),
        .I2(\pc_reg[30]_i_10_n_0 ),
        .I3(\pc_reg_reg[30]_1 ),
        .I4(\bram_firmware_code_address_reg[19]_i_10_n_0 ),
        .I5(\lower_reg_reg[31]_5 [30]),
        .O(c_mult[30]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \pc_reg[30]_i_8 
       (.I0(shift4L[22]),
        .I1(a_bus[3]),
        .I2(shift2L[26]),
        .I3(a_bus[2]),
        .I4(shift2L[30]),
        .O(shift8L[30]));
  LUT6 #(
    .INIT(64'hFF00FF01FF00FE00)) 
    \pc_reg[30]_i_9 
       (.I0(a_bus[4]),
        .I1(a_bus[3]),
        .I2(a_bus[2]),
        .I3(\bram_firmware_code_address_reg[19]_i_17_n_0 ),
        .I4(a_bus[1]),
        .I5(shift1R),
        .O(shift16R));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \pc_reg[31]_i_1 
       (.I0(pause_in),
        .O(\pc_reg_reg[31]_0 ));
  LUT6 #(
    .INIT(64'h0000000000002E22)) 
    \pc_reg[31]_i_10 
       (.I0(\pc_reg[31]_i_17_n_0 ),
        .I1(opcode[26]),
        .I2(opcode[30]),
        .I3(\pc_reg[31]_i_18_n_0 ),
        .I4(opcode[31]),
        .I5(opcode[29]),
        .O(\pc_reg[31]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFAFFFFFFFFB)) 
    \pc_reg[31]_i_11 
       (.I0(\u3_control/is_syscall__2 ),
        .I1(opcode[26]),
        .I2(opcode[27]),
        .I3(opcode[30]),
        .I4(\pc_reg[31]_i_19_n_0 ),
        .I5(opcode[28]),
        .O(branch_func));
  LUT6 #(
    .INIT(64'h0000000010111000)) 
    \pc_reg[31]_i_12 
       (.I0(opcode[29]),
        .I1(opcode[31]),
        .I2(\pc_reg[31]_i_20_n_0 ),
        .I3(opcode[30]),
        .I4(\pc_reg[31]_i_21_n_0 ),
        .I5(\u3_control/is_syscall__2 ),
        .O(\pc_reg[31]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'h9669FFFF96690000)) 
    \pc_reg[31]_i_13 
       (.I0(b_bus[31]),
        .I1(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I2(a_bus[31]),
        .I3(\u6_alu/bv_adder3__3 ),
        .I4(\u6_alu/c_alu1127_out ),
        .I5(\pc_reg[31]_i_23_n_0 ),
        .O(c_alu[31]));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \pc_reg[31]_i_14 
       (.I0(shift8L[15]),
        .I1(a_bus[4]),
        .I2(shift8L[31]),
        .I3(\bram_firmware_code_address_reg[15]_i_7_n_0 ),
        .I4(\pc_reg[31]_i_25_n_0 ),
        .O(c_shift[31]));
  LUT6 #(
    .INIT(64'h3333333303030B08)) 
    \pc_reg[31]_i_16 
       (.I0(opcode[23]),
        .I1(opcode[30]),
        .I2(opcode[27]),
        .I3(\pc_reg[31]_i_28_n_0 ),
        .I4(opcode[26]),
        .I5(opcode[28]),
        .O(\pc_reg[31]_i_16_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT5 #(
    .INIT(32'h11100010)) 
    \pc_reg[31]_i_17 
       (.I0(opcode[27]),
        .I1(opcode[28]),
        .I2(\pc_reg[31]_i_28_n_0 ),
        .I3(opcode[30]),
        .I4(opcode[23]),
        .O(\pc_reg[31]_i_17_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAAAA55555554)) 
    \pc_reg[31]_i_18 
       (.I0(opcode[27]),
        .I1(opcode[17]),
        .I2(opcode[16]),
        .I3(opcode[18]),
        .I4(opcode[19]),
        .I5(opcode[28]),
        .O(\pc_reg[31]_i_18_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \pc_reg[31]_i_19 
       (.I0(opcode[29]),
        .I1(opcode[31]),
        .O(\pc_reg[31]_i_19_n_0 ));
  LUT6 #(
    .INIT(64'hE2E2FFAAE2E200AA)) 
    \pc_reg[31]_i_2 
       (.I0(pc_plus4[27]),
        .I1(take_branch),
        .I2(pc_new[31]),
        .I3(pc_source[0]),
        .I4(pc_source[1]),
        .I5(pc_current[31]),
        .O(\pc_reg_reg[31] ));
  (* SOFT_HLUTNM = "soft_lutpair85" *) 
  LUT4 #(
    .INIT(16'h0010)) 
    \pc_reg[31]_i_20 
       (.I0(opcode[26]),
        .I1(opcode[28]),
        .I2(opcode[23]),
        .I3(opcode[27]),
        .O(\pc_reg[31]_i_20_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFAAAA0000AAA8)) 
    \pc_reg[31]_i_21 
       (.I0(opcode[26]),
        .I1(opcode[18]),
        .I2(opcode[17]),
        .I3(opcode[19]),
        .I4(opcode[27]),
        .I5(opcode[28]),
        .O(\pc_reg[31]_i_21_n_0 ));
  LUT6 #(
    .INIT(64'hFFB2E8FFE80000B2)) 
    \pc_reg[31]_i_22 
       (.I0(\u6_alu/bv_adder7__3 ),
        .I1(b_bus[29]),
        .I2(a_bus[29]),
        .I3(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I4(b_bus[30]),
        .I5(a_bus[30]),
        .O(\u6_alu/bv_adder3__3 ));
  LUT6 #(
    .INIT(64'h0000000011050540)) 
    \pc_reg[31]_i_23 
       (.I0(\bram_firmware_code_address_reg[17]_i_9_n_0 ),
        .I1(\bram_firmware_code_address_reg[17]_i_10_n_0 ),
        .I2(\bram_firmware_code_address_reg[17]_i_11_n_0 ),
        .I3(a_bus[31]),
        .I4(b_bus[31]),
        .I5(\u6_alu/c_alu1__0 ),
        .O(\pc_reg[31]_i_23_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \pc_reg[31]_i_24 
       (.I0(shift4L[23]),
        .I1(a_bus[3]),
        .I2(shift2L[27]),
        .I3(a_bus[2]),
        .I4(shift2L[31]),
        .O(shift8L[31]));
  (* SOFT_HLUTNM = "soft_lutpair88" *) 
  LUT4 #(
    .INIT(16'hA808)) 
    \pc_reg[31]_i_25 
       (.I0(\bram_firmware_code_address_reg[19]_i_14_n_0 ),
        .I1(shift8R[31]),
        .I2(a_bus[4]),
        .I3(\bram_firmware_code_address_reg[19]_i_17_n_0 ),
        .O(\pc_reg[31]_i_25_n_0 ));
  LUT6 #(
    .INIT(64'h00FF00000020008A)) 
    \pc_reg[31]_i_27 
       (.I0(\pc_reg_reg[25]_1 ),
        .I1(\upper_reg_reg[31]_0 [22]),
        .I2(\upper_reg_reg[28] ),
        .I3(\pc_reg_reg[30]_0 ),
        .I4(\upper_reg_reg[31]_0 [23]),
        .I5(\pc_reg_reg[25] ),
        .O(\pc_reg_reg[31]_1 ));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT5 #(
    .INIT(32'h00000002)) 
    \pc_reg[31]_i_28 
       (.I0(opcode[3]),
        .I1(opcode[4]),
        .I2(opcode[5]),
        .I3(opcode[1]),
        .I4(opcode[2]),
        .O(\pc_reg[31]_i_28_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair112" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \pc_reg[31]_i_29 
       (.I0(shift2L[19]),
        .I1(a_bus[2]),
        .I2(shift2L[23]),
        .O(shift4L[23]));
  LUT5 #(
    .INIT(32'hBFFFFFFF)) 
    \pc_reg[31]_i_3 
       (.I0(reset),
        .I1(\reset_reg_reg[3] [3]),
        .I2(\reset_reg_reg[3] [2]),
        .I3(\reset_reg_reg[3] [0]),
        .I4(\reset_reg_reg[3] [1]),
        .O(\address_reg_reg[24]_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pc_reg[31]_i_30 
       (.I0(b_bus[24]),
        .I1(b_bus[25]),
        .I2(a_bus[1]),
        .I3(b_bus[26]),
        .I4(a_bus[0]),
        .I5(b_bus[27]),
        .O(shift2L[27]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pc_reg[31]_i_31 
       (.I0(b_bus[28]),
        .I1(b_bus[29]),
        .I2(a_bus[1]),
        .I3(b_bus[30]),
        .I4(a_bus[0]),
        .I5(b_bus[31]),
        .O(shift2L[31]));
  LUT6 #(
    .INIT(64'hFFFFFFFF0000FFFE)) 
    \pc_reg[31]_i_4 
       (.I0(mem_source[3]),
        .I1(mem_source[2]),
        .I2(mem_source[0]),
        .I3(mem_source[1]),
        .I4(mem_state_reg_reg_n_0),
        .I5(pause_in0_out),
        .O(pause_in));
  LUT6 #(
    .INIT(64'h0111F1E1011EFEEE)) 
    \pc_reg[31]_i_6 
       (.I0(\u3_control/is_syscall__2 ),
        .I1(\pc_reg[31]_i_10_n_0 ),
        .I2(branch_func),
        .I3(CO),
        .I4(\pc_reg[31]_i_12_n_0 ),
        .I5(reg_source[31]),
        .O(take_branch));
  LUT5 #(
    .INIT(32'hFFFEEEFE)) 
    \pc_reg[31]_i_7 
       (.I0(c_alu[31]),
        .I1(c_shift[31]),
        .I2(\lower_reg_reg[31]_6 ),
        .I3(\bram_firmware_code_address_reg[19]_i_10_n_0 ),
        .I4(\lower_reg_reg[31]_5 [31]),
        .O(pc_new[31]));
  LUT6 #(
    .INIT(64'hAAAAAAABAAAAAAAA)) 
    \pc_reg[31]_i_8 
       (.I0(\u3_control/is_syscall__2 ),
        .I1(opcode[31]),
        .I2(opcode[30]),
        .I3(opcode[28]),
        .I4(opcode[29]),
        .I5(opcode[27]),
        .O(pc_source[0]));
  LUT4 #(
    .INIT(16'hABAA)) 
    \pc_reg[31]_i_9 
       (.I0(\u3_control/is_syscall__2 ),
        .I1(opcode[29]),
        .I2(opcode[31]),
        .I3(\pc_reg[31]_i_16_n_0 ),
        .O(pc_source[1]));
  LUT6 #(
    .INIT(64'hFFFFFF28FF28FF28)) 
    \pc_reg[3]_i_1 
       (.I0(\pc_reg_reg[3]_0 ),
        .I1(pc_current[2]),
        .I2(pc_current[3]),
        .I3(\pc_reg[3]_i_3_n_0 ),
        .I4(opcode[1]),
        .I5(\pc_reg[3]_i_4_n_0 ),
        .O(\pc_reg_reg[3] ));
  LUT6 #(
    .INIT(64'h000000000000FFFD)) 
    \pc_reg[3]_i_2 
       (.I0(opcode[27]),
        .I1(opcode[29]),
        .I2(opcode[28]),
        .I3(\pc_reg[3]_i_5_n_0 ),
        .I4(\u3_control/is_syscall__2 ),
        .I5(pc_source[1]),
        .O(\pc_reg_reg[3]_0 ));
  LUT5 #(
    .INIT(32'hAA280028)) 
    \pc_reg[3]_i_3 
       (.I0(pc_source[1]),
        .I1(pc_current[3]),
        .I2(pc_current[2]),
        .I3(take_branch),
        .I4(pc_new[3]),
        .O(\pc_reg[3]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h00000000FFFF0002)) 
    \pc_reg[3]_i_4 
       (.I0(opcode[27]),
        .I1(opcode[29]),
        .I2(opcode[28]),
        .I3(\pc_reg[3]_i_5_n_0 ),
        .I4(\u3_control/is_syscall__2 ),
        .I5(pc_source[1]),
        .O(\pc_reg[3]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair143" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \pc_reg[3]_i_5 
       (.I0(opcode[30]),
        .I1(opcode[31]),
        .O(\pc_reg[3]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hE2E2FFAAE2E200AA)) 
    \pc_reg[4]_i_1 
       (.I0(pc_plus4[1]),
        .I1(take_branch),
        .I2(pc_new[4]),
        .I3(pc_source[0]),
        .I4(pc_source[1]),
        .I5(opcode[2]),
        .O(\pc_reg_reg[4] ));
  LUT6 #(
    .INIT(64'hE2E2FFAAE2E200AA)) 
    \pc_reg[5]_i_1 
       (.I0(pc_plus4[2]),
        .I1(take_branch),
        .I2(pc_new[5]),
        .I3(pc_source[0]),
        .I4(pc_source[1]),
        .I5(opcode[3]),
        .O(\pc_reg_reg[5] ));
  LUT6 #(
    .INIT(64'hE2E2FFAAE2E200AA)) 
    \pc_reg[6]_i_1 
       (.I0(pc_plus4[3]),
        .I1(take_branch),
        .I2(pc_new[6]),
        .I3(pc_source[0]),
        .I4(pc_source[1]),
        .I5(opcode[4]),
        .O(\pc_reg_reg[6] ));
  LUT6 #(
    .INIT(64'hE2E2FFAAE2E200AA)) 
    \pc_reg[7]_i_1 
       (.I0(pc_plus4[4]),
        .I1(take_branch),
        .I2(pc_new[7]),
        .I3(pc_source[0]),
        .I4(pc_source[1]),
        .I5(opcode[5]),
        .O(\pc_reg_reg[7] ));
  LUT6 #(
    .INIT(64'hE2E2FFAAE2E200AA)) 
    \pc_reg[8]_i_1 
       (.I0(pc_plus4[5]),
        .I1(take_branch),
        .I2(pc_new[8]),
        .I3(pc_source[0]),
        .I4(pc_source[1]),
        .I5(opcode[6]),
        .O(\pc_reg_reg[8] ));
  LUT6 #(
    .INIT(64'hE2E2FFAAE2E200AA)) 
    \pc_reg[9]_i_1 
       (.I0(pc_plus4[6]),
        .I1(take_branch),
        .I2(pc_new[9]),
        .I3(pc_source[0]),
        .I4(pc_source[1]),
        .I5(opcode[7]),
        .O(\pc_reg_reg[9] ));
  MUXF7 \pc_reg_reg[20]_i_4 
       (.I0(\pc_reg[20]_i_7_n_0 ),
        .I1(\u6_alu/bv_adder058_out ),
        .O(c_alu[20]),
        .S(\u6_alu/c_alu1127_out ));
  MUXF7 \pc_reg_reg[23]_i_4 
       (.I0(\pc_reg[23]_i_7_n_0 ),
        .I1(\u6_alu/bv_adder067_out ),
        .O(c_alu[23]),
        .S(\u6_alu/c_alu1127_out ));
  MUXF7 \pc_reg_reg[25]_i_4 
       (.I0(\pc_reg[25]_i_7_n_0 ),
        .I1(\u6_alu/bv_adder073_out ),
        .O(c_alu[25]),
        .S(\u6_alu/c_alu1127_out ));
  MUXF7 \pc_reg_reg[28]_i_5 
       (.I0(\pc_reg[28]_i_9_n_0 ),
        .I1(\u6_alu/bv_adder082_out ),
        .O(c_alu[28]),
        .S(\u6_alu/c_alu1127_out ));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    sign_reg_i_10
       (.I0(a_bus[2]),
        .I1(a_bus[3]),
        .I2(a_bus[0]),
        .I3(a_bus[1]),
        .O(sign_reg_i_10_n_0));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    sign_reg_i_11
       (.I0(a_bus[26]),
        .I1(a_bus[27]),
        .I2(a_bus[24]),
        .I3(a_bus[25]),
        .O(sign_reg_i_11_n_0));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    sign_reg_i_12
       (.I0(a_bus[18]),
        .I1(a_bus[19]),
        .I2(a_bus[16]),
        .I3(a_bus[17]),
        .O(sign_reg_i_12_n_0));
  LUT6 #(
    .INIT(64'h0000FFFEFFFE0000)) 
    sign_reg_i_2
       (.I0(sign_reg_i_4_n_0),
        .I1(sign_reg_i_5_n_0),
        .I2(sign_reg_i_6_n_0),
        .I3(sign_reg_i_7_n_0),
        .I4(a_bus[31]),
        .I5(b_bus[31]),
        .O(sign_reg__6));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT5 #(
    .INIT(32'hAA32A8CA)) 
    sign_reg_i_3
       (.I0(sign_reg1_out),
        .I1(\lower_reg_reg[31] ),
        .I2(\lower_reg_reg[31]_2 ),
        .I3(\lower_reg_reg[31]_1 ),
        .I4(\lower_reg_reg[31]_0 ),
        .O(sign2_reg));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    sign_reg_i_4
       (.I0(a_bus[13]),
        .I1(a_bus[12]),
        .I2(a_bus[15]),
        .I3(a_bus[14]),
        .I4(sign_reg_i_9_n_0),
        .O(sign_reg_i_4_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    sign_reg_i_5
       (.I0(a_bus[5]),
        .I1(a_bus[4]),
        .I2(a_bus[7]),
        .I3(a_bus[6]),
        .I4(sign_reg_i_10_n_0),
        .O(sign_reg_i_5_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    sign_reg_i_6
       (.I0(a_bus[29]),
        .I1(a_bus[28]),
        .I2(a_bus[30]),
        .I3(a_bus[31]),
        .I4(sign_reg_i_11_n_0),
        .O(sign_reg_i_6_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    sign_reg_i_7
       (.I0(a_bus[21]),
        .I1(a_bus[20]),
        .I2(a_bus[23]),
        .I3(a_bus[22]),
        .I4(sign_reg_i_12_n_0),
        .O(sign_reg_i_7_n_0));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    sign_reg_i_9
       (.I0(a_bus[10]),
        .I1(a_bus[11]),
        .I2(a_bus[8]),
        .I3(a_bus[9]),
        .O(sign_reg_i_9_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    take_branch1_carry__0_i_1
       (.I0(reg_source[21]),
        .I1(reg_target[21]),
        .I2(reg_target[23]),
        .I3(reg_source[23]),
        .I4(reg_target[22]),
        .I5(reg_source[22]),
        .O(\pc_reg_reg[28]_0 [3]));
  LUT5 #(
    .INIT(32'hC0408000)) 
    take_branch1_carry__0_i_10
       (.I0(rs_index[4]),
        .I1(take_branch1_carry_i_17_n_0),
        .I2(take_branch1_carry_i_18_n_0),
        .I3(data_out1B[19]),
        .I4(DPO50_out),
        .O(reg_source[19]));
  LUT5 #(
    .INIT(32'hC0408000)) 
    take_branch1_carry__0_i_11
       (.I0(rs_index[4]),
        .I1(take_branch1_carry_i_17_n_0),
        .I2(take_branch1_carry_i_18_n_0),
        .I3(data_out1B[15]),
        .I4(DPO66_out),
        .O(reg_source[15]));
  LUT5 #(
    .INIT(32'hC0408000)) 
    take_branch1_carry__0_i_12
       (.I0(rs_index[4]),
        .I1(take_branch1_carry_i_17_n_0),
        .I2(take_branch1_carry_i_18_n_0),
        .I3(data_out1B[17]),
        .I4(DPO58_out),
        .O(reg_source[17]));
  LUT5 #(
    .INIT(32'hC0408000)) 
    take_branch1_carry__0_i_13
       (.I0(rs_index[4]),
        .I1(take_branch1_carry_i_17_n_0),
        .I2(take_branch1_carry_i_18_n_0),
        .I3(data_out1B[16]),
        .I4(DPO62_out),
        .O(reg_source[16]));
  LUT5 #(
    .INIT(32'hC0408000)) 
    take_branch1_carry__0_i_14
       (.I0(rs_index[4]),
        .I1(take_branch1_carry_i_17_n_0),
        .I2(take_branch1_carry_i_18_n_0),
        .I3(data_out1B[12]),
        .I4(DPO78_out),
        .O(reg_source[12]));
  LUT5 #(
    .INIT(32'hC0408000)) 
    take_branch1_carry__0_i_15
       (.I0(rs_index[4]),
        .I1(take_branch1_carry_i_17_n_0),
        .I2(take_branch1_carry_i_18_n_0),
        .I3(data_out1B[14]),
        .I4(DPO70_out),
        .O(reg_source[14]));
  LUT5 #(
    .INIT(32'hC0408000)) 
    take_branch1_carry__0_i_16
       (.I0(rs_index[4]),
        .I1(take_branch1_carry_i_17_n_0),
        .I2(take_branch1_carry_i_18_n_0),
        .I3(data_out1B[13]),
        .I4(DPO74_out),
        .O(reg_source[13]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    take_branch1_carry__0_i_2
       (.I0(reg_source[18]),
        .I1(reg_target[18]),
        .I2(reg_target[20]),
        .I3(reg_source[20]),
        .I4(reg_target[19]),
        .I5(reg_source[19]),
        .O(\pc_reg_reg[28]_0 [2]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    take_branch1_carry__0_i_3
       (.I0(reg_source[15]),
        .I1(reg_target[15]),
        .I2(reg_target[17]),
        .I3(reg_source[17]),
        .I4(reg_target[16]),
        .I5(reg_source[16]),
        .O(\pc_reg_reg[28]_0 [1]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    take_branch1_carry__0_i_4
       (.I0(reg_source[12]),
        .I1(reg_target[12]),
        .I2(reg_target[14]),
        .I3(reg_source[14]),
        .I4(reg_target[13]),
        .I5(reg_source[13]),
        .O(\pc_reg_reg[28]_0 [0]));
  LUT5 #(
    .INIT(32'hC0408000)) 
    take_branch1_carry__0_i_5
       (.I0(rs_index[4]),
        .I1(take_branch1_carry_i_17_n_0),
        .I2(take_branch1_carry_i_18_n_0),
        .I3(data_out1B[21]),
        .I4(DPO42_out),
        .O(reg_source[21]));
  LUT5 #(
    .INIT(32'hC0408000)) 
    take_branch1_carry__0_i_6
       (.I0(rs_index[4]),
        .I1(take_branch1_carry_i_17_n_0),
        .I2(take_branch1_carry_i_18_n_0),
        .I3(data_out1B[23]),
        .I4(DPO34_out),
        .O(reg_source[23]));
  LUT5 #(
    .INIT(32'hC0408000)) 
    take_branch1_carry__0_i_7
       (.I0(rs_index[4]),
        .I1(take_branch1_carry_i_17_n_0),
        .I2(take_branch1_carry_i_18_n_0),
        .I3(data_out1B[22]),
        .I4(DPO38_out),
        .O(reg_source[22]));
  LUT5 #(
    .INIT(32'hC0408000)) 
    take_branch1_carry__0_i_8
       (.I0(rs_index[4]),
        .I1(take_branch1_carry_i_17_n_0),
        .I2(take_branch1_carry_i_18_n_0),
        .I3(data_out1B[18]),
        .I4(DPO54_out),
        .O(reg_source[18]));
  LUT5 #(
    .INIT(32'hC0408000)) 
    take_branch1_carry__0_i_9
       (.I0(rs_index[4]),
        .I1(take_branch1_carry_i_17_n_0),
        .I2(take_branch1_carry_i_18_n_0),
        .I3(data_out1B[20]),
        .I4(DPO46_out),
        .O(reg_source[20]));
  LUT4 #(
    .INIT(16'h9009)) 
    take_branch1_carry__1_i_1
       (.I0(reg_source[30]),
        .I1(reg_target[30]),
        .I2(reg_source[31]),
        .I3(reg_target[31]),
        .O(\pc_reg_reg[28]_1 [2]));
  LUT5 #(
    .INIT(32'hC0408000)) 
    take_branch1_carry__1_i_10
       (.I0(rs_index[4]),
        .I1(take_branch1_carry_i_17_n_0),
        .I2(take_branch1_carry_i_18_n_0),
        .I3(data_out1B[26]),
        .I4(DPO22_out),
        .O(reg_source[26]));
  LUT5 #(
    .INIT(32'hC0408000)) 
    take_branch1_carry__1_i_11
       (.I0(rs_index[4]),
        .I1(take_branch1_carry_i_17_n_0),
        .I2(take_branch1_carry_i_18_n_0),
        .I3(data_out1B[25]),
        .I4(DPO26_out),
        .O(reg_source[25]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    take_branch1_carry__1_i_2
       (.I0(reg_source[27]),
        .I1(reg_target[27]),
        .I2(reg_target[29]),
        .I3(reg_source[29]),
        .I4(reg_target[28]),
        .I5(reg_source[28]),
        .O(\pc_reg_reg[28]_1 [1]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    take_branch1_carry__1_i_3
       (.I0(reg_source[24]),
        .I1(reg_target[24]),
        .I2(reg_target[26]),
        .I3(reg_source[26]),
        .I4(reg_target[25]),
        .I5(reg_source[25]),
        .O(\pc_reg_reg[28]_1 [0]));
  LUT5 #(
    .INIT(32'hC0408000)) 
    take_branch1_carry__1_i_4
       (.I0(rs_index[4]),
        .I1(take_branch1_carry_i_17_n_0),
        .I2(take_branch1_carry_i_18_n_0),
        .I3(data_out1B[30]),
        .I4(DPO6_out),
        .O(reg_source[30]));
  LUT5 #(
    .INIT(32'hC0408000)) 
    take_branch1_carry__1_i_5
       (.I0(rs_index[4]),
        .I1(take_branch1_carry_i_17_n_0),
        .I2(take_branch1_carry_i_18_n_0),
        .I3(data_out1B[31]),
        .I4(DPO2_out),
        .O(reg_source[31]));
  LUT5 #(
    .INIT(32'hC0408000)) 
    take_branch1_carry__1_i_6
       (.I0(rs_index[4]),
        .I1(take_branch1_carry_i_17_n_0),
        .I2(take_branch1_carry_i_18_n_0),
        .I3(data_out1B[27]),
        .I4(DPO18_out),
        .O(reg_source[27]));
  LUT5 #(
    .INIT(32'hC0408000)) 
    take_branch1_carry__1_i_7
       (.I0(rs_index[4]),
        .I1(take_branch1_carry_i_17_n_0),
        .I2(take_branch1_carry_i_18_n_0),
        .I3(data_out1B[29]),
        .I4(DPO10_out),
        .O(reg_source[29]));
  LUT5 #(
    .INIT(32'hC0408000)) 
    take_branch1_carry__1_i_8
       (.I0(rs_index[4]),
        .I1(take_branch1_carry_i_17_n_0),
        .I2(take_branch1_carry_i_18_n_0),
        .I3(data_out1B[28]),
        .I4(DPO14_out),
        .O(reg_source[28]));
  LUT5 #(
    .INIT(32'hC0408000)) 
    take_branch1_carry__1_i_9
       (.I0(rs_index[4]),
        .I1(take_branch1_carry_i_17_n_0),
        .I2(take_branch1_carry_i_18_n_0),
        .I3(data_out1B[24]),
        .I4(DPO30_out),
        .O(reg_source[24]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    take_branch1_carry_i_1
       (.I0(reg_source[9]),
        .I1(reg_target[9]),
        .I2(reg_target[11]),
        .I3(reg_source[11]),
        .I4(reg_target[10]),
        .I5(reg_source[10]),
        .O(S[3]));
  LUT5 #(
    .INIT(32'hC0408000)) 
    take_branch1_carry_i_10
       (.I0(rs_index[4]),
        .I1(take_branch1_carry_i_17_n_0),
        .I2(take_branch1_carry_i_18_n_0),
        .I3(data_out1B[7]),
        .I4(DPO98_out),
        .O(reg_source[7]));
  LUT5 #(
    .INIT(32'hCC4C8C0C)) 
    take_branch1_carry_i_11
       (.I0(rs_index[4]),
        .I1(take_branch1_carry_i_17_n_0),
        .I2(take_branch1_carry_i_18_n_0),
        .I3(data_out1B[3]),
        .I4(DPO114_out),
        .O(reg_source[3]));
  LUT5 #(
    .INIT(32'hCC4C8C0C)) 
    take_branch1_carry_i_12
       (.I0(rs_index[4]),
        .I1(take_branch1_carry_i_17_n_0),
        .I2(take_branch1_carry_i_18_n_0),
        .I3(data_out1B[5]),
        .I4(DPO106_out),
        .O(reg_source[5]));
  LUT5 #(
    .INIT(32'hCC4C8C0C)) 
    take_branch1_carry_i_13
       (.I0(rs_index[4]),
        .I1(take_branch1_carry_i_17_n_0),
        .I2(take_branch1_carry_i_18_n_0),
        .I3(data_out1B[4]),
        .I4(DPO110_out),
        .O(reg_source[4]));
  LUT6 #(
    .INIT(64'hCCC088C044C000C0)) 
    take_branch1_carry_i_14
       (.I0(rs_index[4]),
        .I1(take_branch1_carry_i_18_n_0),
        .I2(intr_enable),
        .I3(take_branch1_carry_i_17_n_0),
        .I4(DPO),
        .I5(data_out1B[0]),
        .O(reg_source[0]));
  LUT5 #(
    .INIT(32'hCC4C8C0C)) 
    take_branch1_carry_i_15
       (.I0(rs_index[4]),
        .I1(take_branch1_carry_i_17_n_0),
        .I2(take_branch1_carry_i_18_n_0),
        .I3(data_out1B[2]),
        .I4(DPO118_out),
        .O(reg_source[2]));
  LUT5 #(
    .INIT(32'hC0408000)) 
    take_branch1_carry_i_16
       (.I0(rs_index[4]),
        .I1(take_branch1_carry_i_17_n_0),
        .I2(take_branch1_carry_i_18_n_0),
        .I3(data_out1B[1]),
        .I4(DPO122_out),
        .O(reg_source[1]));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFBFFE)) 
    take_branch1_carry_i_17
       (.I0(rs_index[1]),
        .I1(rs_index[5]),
        .I2(rs_index[3]),
        .I3(rs_index[2]),
        .I4(rs_index[4]),
        .I5(rs_index[0]),
        .O(take_branch1_carry_i_17_n_0));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFE)) 
    take_branch1_carry_i_18
       (.I0(rs_index[2]),
        .I1(rs_index[1]),
        .I2(rs_index[0]),
        .I3(rs_index[5]),
        .I4(rs_index[4]),
        .I5(rs_index[3]),
        .O(take_branch1_carry_i_18_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    take_branch1_carry_i_2
       (.I0(reg_source[6]),
        .I1(reg_target[6]),
        .I2(reg_target[8]),
        .I3(reg_source[8]),
        .I4(reg_target[7]),
        .I5(reg_source[7]),
        .O(S[2]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    take_branch1_carry_i_3
       (.I0(reg_source[3]),
        .I1(reg_target[3]),
        .I2(reg_target[5]),
        .I3(reg_source[5]),
        .I4(reg_target[4]),
        .I5(reg_source[4]),
        .O(S[1]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    take_branch1_carry_i_4
       (.I0(reg_source[0]),
        .I1(reg_target[0]),
        .I2(reg_target[2]),
        .I3(reg_source[2]),
        .I4(reg_target[1]),
        .I5(reg_source[1]),
        .O(S[0]));
  LUT5 #(
    .INIT(32'hC0408000)) 
    take_branch1_carry_i_5
       (.I0(rs_index[4]),
        .I1(take_branch1_carry_i_17_n_0),
        .I2(take_branch1_carry_i_18_n_0),
        .I3(data_out1B[9]),
        .I4(DPO90_out),
        .O(reg_source[9]));
  LUT5 #(
    .INIT(32'hC0408000)) 
    take_branch1_carry_i_6
       (.I0(rs_index[4]),
        .I1(take_branch1_carry_i_17_n_0),
        .I2(take_branch1_carry_i_18_n_0),
        .I3(data_out1B[11]),
        .I4(DPO82_out),
        .O(reg_source[11]));
  LUT5 #(
    .INIT(32'hC0408000)) 
    take_branch1_carry_i_7
       (.I0(rs_index[4]),
        .I1(take_branch1_carry_i_17_n_0),
        .I2(take_branch1_carry_i_18_n_0),
        .I3(data_out1B[10]),
        .I4(DPO86_out),
        .O(reg_source[10]));
  LUT5 #(
    .INIT(32'hC0408000)) 
    take_branch1_carry_i_8
       (.I0(rs_index[4]),
        .I1(take_branch1_carry_i_17_n_0),
        .I2(take_branch1_carry_i_18_n_0),
        .I3(data_out1B[6]),
        .I4(DPO102_out),
        .O(reg_source[6]));
  LUT5 #(
    .INIT(32'hC0408000)) 
    take_branch1_carry_i_9
       (.I0(rs_index[4]),
        .I1(take_branch1_carry_i_17_n_0),
        .I2(take_branch1_carry_i_18_n_0),
        .I3(data_out1B[8]),
        .I4(DPO94_out),
        .O(reg_source[8]));
  LUT6 #(
    .INIT(64'hEABAFFFD40100020)) 
    \upper_reg[0]_i_1 
       (.I0(\lower_reg_reg[31]_1 ),
        .I1(\lower_reg_reg[31] ),
        .I2(a_bus[0]),
        .I3(\lower_reg_reg[31]_0 ),
        .I4(\lower_reg_reg[31]_2 ),
        .I5(\aa_reg_reg[1] ),
        .O(\upper_reg_reg[31] [0]));
  LUT4 #(
    .INIT(16'h00E2)) 
    \upper_reg[0]_i_2 
       (.I0(reg_source[0]),
        .I1(\upper_reg[0]_i_4_n_0 ),
        .I2(opcode[6]),
        .I3(\upper_reg[0]_i_5_n_0 ),
        .O(a_bus[0]));
  LUT5 #(
    .INIT(32'h00000004)) 
    \upper_reg[0]_i_4 
       (.I0(\xilinx_16x1d.reg_loop[0].reg_bit2a_i_7_n_0 ),
        .I1(\upper_reg[0]_i_6_n_0 ),
        .I2(opcode[26]),
        .I3(opcode[31]),
        .I4(\u3_control/is_syscall__2 ),
        .O(\upper_reg[0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000005504)) 
    \upper_reg[0]_i_5 
       (.I0(opcode[29]),
        .I1(opcode[26]),
        .I2(opcode[27]),
        .I3(opcode[28]),
        .I4(\pc_reg[3]_i_5_n_0 ),
        .I5(\u3_control/is_syscall__2 ),
        .O(\upper_reg[0]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h000000000000000D)) 
    \upper_reg[0]_i_6 
       (.I0(opcode[0]),
        .I1(opcode[1]),
        .I2(opcode[3]),
        .I3(opcode[2]),
        .I4(opcode[4]),
        .I5(opcode[5]),
        .O(\upper_reg[0]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'h84FF8400)) 
    \upper_reg[10]_i_2 
       (.I0(\lower_reg_reg[31] ),
        .I1(a_bus[10]),
        .I2(\lower_reg_reg[31]_0 ),
        .I3(\lower_reg_reg[31]_2 ),
        .I4(\bb_reg_reg[0]_9 ),
        .O(\upper_reg[10]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hCDC8CDCDC8CDC8C8)) 
    \upper_reg[10]_i_3 
       (.I0(\aa_reg[29]_i_2_n_0 ),
        .I1(\bb_reg_reg[0]_9 ),
        .I2(\aa_reg[29]_i_3_n_0 ),
        .I3(\aa_reg[10]_i_4_n_0 ),
        .I4(a_bus[31]),
        .I5(a_bus[10]),
        .O(\upper_reg[10]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h84FF8400)) 
    \upper_reg[11]_i_2 
       (.I0(\lower_reg_reg[31] ),
        .I1(a_bus[11]),
        .I2(\lower_reg_reg[31]_0 ),
        .I3(\lower_reg_reg[31]_2 ),
        .I4(\aa_reg_reg[12] ),
        .O(\upper_reg[11]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hCDC8CDCDC8CDC8C8)) 
    \upper_reg[11]_i_3 
       (.I0(\aa_reg[29]_i_2_n_0 ),
        .I1(\aa_reg_reg[12] ),
        .I2(\aa_reg[29]_i_3_n_0 ),
        .I3(\aa_reg[11]_i_4_n_0 ),
        .I4(a_bus[31]),
        .I5(a_bus[11]),
        .O(\upper_reg[11]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h84FF8400)) 
    \upper_reg[12]_i_2 
       (.I0(\lower_reg_reg[31] ),
        .I1(a_bus[12]),
        .I2(\lower_reg_reg[31]_0 ),
        .I3(\lower_reg_reg[31]_2 ),
        .I4(\bb_reg_reg[0]_8 ),
        .O(\upper_reg[12]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hCDC8CDCDC8CDC8C8)) 
    \upper_reg[12]_i_3 
       (.I0(\aa_reg[29]_i_2_n_0 ),
        .I1(\bb_reg_reg[0]_8 ),
        .I2(\aa_reg[29]_i_3_n_0 ),
        .I3(\aa_reg[12]_i_4_n_0 ),
        .I4(a_bus[31]),
        .I5(a_bus[12]),
        .O(\upper_reg[12]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h84FF8400)) 
    \upper_reg[13]_i_2 
       (.I0(\lower_reg_reg[31] ),
        .I1(a_bus[13]),
        .I2(\lower_reg_reg[31]_0 ),
        .I3(\lower_reg_reg[31]_2 ),
        .I4(\aa_reg_reg[14] ),
        .O(\upper_reg[13]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hCDC8CDCDC8CDC8C8)) 
    \upper_reg[13]_i_3 
       (.I0(\aa_reg[29]_i_2_n_0 ),
        .I1(\aa_reg_reg[14] ),
        .I2(\aa_reg[29]_i_3_n_0 ),
        .I3(\aa_reg[13]_i_4_n_0 ),
        .I4(a_bus[31]),
        .I5(a_bus[13]),
        .O(\upper_reg[13]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h84FF8400)) 
    \upper_reg[14]_i_2 
       (.I0(\lower_reg_reg[31] ),
        .I1(a_bus[14]),
        .I2(\lower_reg_reg[31]_0 ),
        .I3(\lower_reg_reg[31]_2 ),
        .I4(\bb_reg_reg[0]_7 ),
        .O(\upper_reg[14]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hCDC8CDCDC8CDC8C8)) 
    \upper_reg[14]_i_3 
       (.I0(\aa_reg[29]_i_2_n_0 ),
        .I1(\bb_reg_reg[0]_7 ),
        .I2(\aa_reg[29]_i_3_n_0 ),
        .I3(\aa_reg[14]_i_4_n_0 ),
        .I4(a_bus[31]),
        .I5(a_bus[14]),
        .O(\upper_reg[14]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h84FF8400)) 
    \upper_reg[15]_i_2 
       (.I0(\lower_reg_reg[31] ),
        .I1(a_bus[15]),
        .I2(\lower_reg_reg[31]_0 ),
        .I3(\lower_reg_reg[31]_2 ),
        .I4(\aa_reg_reg[16] ),
        .O(\upper_reg[15]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hCDC8CDCDC8CDC8C8)) 
    \upper_reg[15]_i_3 
       (.I0(\aa_reg[29]_i_2_n_0 ),
        .I1(\aa_reg_reg[16] ),
        .I2(\aa_reg[29]_i_3_n_0 ),
        .I3(\aa_reg[15]_i_4_n_0 ),
        .I4(a_bus[31]),
        .I5(a_bus[15]),
        .O(\upper_reg[15]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h84FF8400)) 
    \upper_reg[16]_i_2 
       (.I0(\lower_reg_reg[31] ),
        .I1(a_bus[16]),
        .I2(\lower_reg_reg[31]_0 ),
        .I3(\lower_reg_reg[31]_2 ),
        .I4(\bb_reg_reg[0]_6 ),
        .O(\upper_reg[16]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hCDC8CDCDC8CDC8C8)) 
    \upper_reg[16]_i_3 
       (.I0(\aa_reg[29]_i_2_n_0 ),
        .I1(\bb_reg_reg[0]_6 ),
        .I2(\aa_reg[29]_i_3_n_0 ),
        .I3(\aa_reg[16]_i_4_n_0 ),
        .I4(a_bus[31]),
        .I5(a_bus[16]),
        .O(\upper_reg[16]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h84FF8400)) 
    \upper_reg[17]_i_2 
       (.I0(\lower_reg_reg[31] ),
        .I1(a_bus[17]),
        .I2(\lower_reg_reg[31]_0 ),
        .I3(\lower_reg_reg[31]_2 ),
        .I4(\aa_reg_reg[18] ),
        .O(\upper_reg[17]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hCDC8CDCDC8CDC8C8)) 
    \upper_reg[17]_i_3 
       (.I0(\aa_reg[29]_i_2_n_0 ),
        .I1(\aa_reg_reg[18] ),
        .I2(\aa_reg[29]_i_3_n_0 ),
        .I3(\aa_reg[17]_i_4_n_0 ),
        .I4(a_bus[31]),
        .I5(a_bus[17]),
        .O(\upper_reg[17]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h84FF8400)) 
    \upper_reg[18]_i_2 
       (.I0(\lower_reg_reg[31] ),
        .I1(a_bus[18]),
        .I2(\lower_reg_reg[31]_0 ),
        .I3(\lower_reg_reg[31]_2 ),
        .I4(\bb_reg_reg[0]_5 ),
        .O(\upper_reg[18]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hCDC8CDCDC8CDC8C8)) 
    \upper_reg[18]_i_3 
       (.I0(\aa_reg[29]_i_2_n_0 ),
        .I1(\bb_reg_reg[0]_5 ),
        .I2(\aa_reg[29]_i_3_n_0 ),
        .I3(\aa_reg[18]_i_4_n_0 ),
        .I4(a_bus[31]),
        .I5(a_bus[18]),
        .O(\upper_reg[18]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h84FF8400)) 
    \upper_reg[19]_i_2 
       (.I0(\lower_reg_reg[31] ),
        .I1(a_bus[19]),
        .I2(\lower_reg_reg[31]_0 ),
        .I3(\lower_reg_reg[31]_2 ),
        .I4(\aa_reg_reg[20] ),
        .O(\upper_reg[19]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hCDC8CDCDC8CDC8C8)) 
    \upper_reg[19]_i_3 
       (.I0(\aa_reg[29]_i_2_n_0 ),
        .I1(\aa_reg_reg[20] ),
        .I2(\aa_reg[29]_i_3_n_0 ),
        .I3(\aa_reg[19]_i_4_n_0 ),
        .I4(a_bus[31]),
        .I5(a_bus[19]),
        .O(\upper_reg[19]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h84FF8400)) 
    \upper_reg[1]_i_2 
       (.I0(\lower_reg_reg[31] ),
        .I1(a_bus[1]),
        .I2(\lower_reg_reg[31]_0 ),
        .I3(\lower_reg_reg[31]_2 ),
        .I4(\aa_reg_reg[2] ),
        .O(\upper_reg[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hC8CDCDCDCDC8C8C8)) 
    \upper_reg[1]_i_3 
       (.I0(\aa_reg[29]_i_2_n_0 ),
        .I1(\aa_reg_reg[2] ),
        .I2(\aa_reg[29]_i_3_n_0 ),
        .I3(a_bus[0]),
        .I4(a_bus[31]),
        .I5(a_bus[1]),
        .O(\upper_reg[1]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h84FF8400)) 
    \upper_reg[20]_i_2 
       (.I0(\lower_reg_reg[31] ),
        .I1(a_bus[20]),
        .I2(\lower_reg_reg[31]_0 ),
        .I3(\lower_reg_reg[31]_2 ),
        .I4(\bb_reg_reg[0]_4 ),
        .O(\upper_reg[20]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hCDC8CDCDC8CDC8C8)) 
    \upper_reg[20]_i_3 
       (.I0(\aa_reg[29]_i_2_n_0 ),
        .I1(\bb_reg_reg[0]_4 ),
        .I2(\aa_reg[29]_i_3_n_0 ),
        .I3(\aa_reg[20]_i_4_n_0 ),
        .I4(a_bus[31]),
        .I5(a_bus[20]),
        .O(\upper_reg[20]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h84FF8400)) 
    \upper_reg[21]_i_2 
       (.I0(\lower_reg_reg[31] ),
        .I1(a_bus[21]),
        .I2(\lower_reg_reg[31]_0 ),
        .I3(\lower_reg_reg[31]_2 ),
        .I4(\aa_reg_reg[22] ),
        .O(\upper_reg[21]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hCDC8CDCDC8CDC8C8)) 
    \upper_reg[21]_i_3 
       (.I0(\aa_reg[29]_i_2_n_0 ),
        .I1(\aa_reg_reg[22] ),
        .I2(\aa_reg[29]_i_3_n_0 ),
        .I3(\aa_reg[21]_i_4_n_0 ),
        .I4(a_bus[31]),
        .I5(a_bus[21]),
        .O(\upper_reg[21]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h84FF8400)) 
    \upper_reg[22]_i_2 
       (.I0(\lower_reg_reg[31] ),
        .I1(a_bus[22]),
        .I2(\lower_reg_reg[31]_0 ),
        .I3(\lower_reg_reg[31]_2 ),
        .I4(\bb_reg_reg[0]_1 ),
        .O(\upper_reg[22]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFF00F909FF00FC0C)) 
    \upper_reg[22]_i_3 
       (.I0(\aa_reg[22]_i_3_n_0 ),
        .I1(a_bus[22]),
        .I2(\aa_reg[29]_i_2_n_0 ),
        .I3(\bb_reg_reg[0]_1 ),
        .I4(\aa_reg[29]_i_3_n_0 ),
        .I5(a_bus[31]),
        .O(\upper_reg[22]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h84FF8400)) 
    \upper_reg[23]_i_2 
       (.I0(\lower_reg_reg[31] ),
        .I1(a_bus[23]),
        .I2(\lower_reg_reg[31]_0 ),
        .I3(\lower_reg_reg[31]_2 ),
        .I4(\aa_reg_reg[24] ),
        .O(\upper_reg[23]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFF00F909FF00FC0C)) 
    \upper_reg[23]_i_3 
       (.I0(\aa_reg[23]_i_3_n_0 ),
        .I1(a_bus[23]),
        .I2(\aa_reg[29]_i_2_n_0 ),
        .I3(\aa_reg_reg[24] ),
        .I4(\aa_reg[29]_i_3_n_0 ),
        .I5(a_bus[31]),
        .O(\upper_reg[23]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h84FF8400)) 
    \upper_reg[24]_i_2 
       (.I0(\lower_reg_reg[31] ),
        .I1(a_bus[24]),
        .I2(\lower_reg_reg[31]_0 ),
        .I3(\lower_reg_reg[31]_2 ),
        .I4(\bb_reg_reg[0]_0 ),
        .O(\upper_reg[24]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFF00F909FF00FC0C)) 
    \upper_reg[24]_i_3 
       (.I0(\aa_reg[24]_i_3_n_0 ),
        .I1(a_bus[24]),
        .I2(\aa_reg[29]_i_2_n_0 ),
        .I3(\bb_reg_reg[0]_0 ),
        .I4(\aa_reg[29]_i_3_n_0 ),
        .I5(a_bus[31]),
        .O(\upper_reg[24]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h84FF8400)) 
    \upper_reg[25]_i_2 
       (.I0(\lower_reg_reg[31] ),
        .I1(a_bus[25]),
        .I2(\lower_reg_reg[31]_0 ),
        .I3(\lower_reg_reg[31]_2 ),
        .I4(\aa_reg_reg[26] ),
        .O(\upper_reg[25]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFF00F909FF00FC0C)) 
    \upper_reg[25]_i_3 
       (.I0(\aa_reg[25]_i_3_n_0 ),
        .I1(a_bus[25]),
        .I2(\aa_reg[29]_i_2_n_0 ),
        .I3(\aa_reg_reg[26] ),
        .I4(\aa_reg[29]_i_3_n_0 ),
        .I5(a_bus[31]),
        .O(\upper_reg[25]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h84FF8400)) 
    \upper_reg[26]_i_2 
       (.I0(\lower_reg_reg[31] ),
        .I1(a_bus[26]),
        .I2(\lower_reg_reg[31]_0 ),
        .I3(\lower_reg_reg[31]_2 ),
        .I4(\bb_reg_reg[0] ),
        .O(\upper_reg[26]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFF00F909FF00FC0C)) 
    \upper_reg[26]_i_3 
       (.I0(\aa_reg[26]_i_3_n_0 ),
        .I1(a_bus[26]),
        .I2(\aa_reg[29]_i_2_n_0 ),
        .I3(\bb_reg_reg[0] ),
        .I4(\aa_reg[29]_i_3_n_0 ),
        .I5(a_bus[31]),
        .O(\upper_reg[26]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h84FF8400)) 
    \upper_reg[27]_i_2 
       (.I0(\lower_reg_reg[31] ),
        .I1(a_bus[27]),
        .I2(\lower_reg_reg[31]_0 ),
        .I3(\lower_reg_reg[31]_2 ),
        .I4(\aa_reg_reg[28] ),
        .O(\upper_reg[27]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFF00F909FF00FC0C)) 
    \upper_reg[27]_i_3 
       (.I0(\aa_reg[27]_i_3_n_0 ),
        .I1(a_bus[27]),
        .I2(\aa_reg[29]_i_2_n_0 ),
        .I3(\aa_reg_reg[28] ),
        .I4(\aa_reg[29]_i_3_n_0 ),
        .I5(a_bus[31]),
        .O(\upper_reg[27]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h84FF8400)) 
    \upper_reg[28]_i_2 
       (.I0(\lower_reg_reg[31] ),
        .I1(a_bus[28]),
        .I2(\lower_reg_reg[31]_0 ),
        .I3(\lower_reg_reg[31]_2 ),
        .I4(\bb_reg_reg[0]_3 ),
        .O(\upper_reg[28]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hCDC8CDCDC8CDC8C8)) 
    \upper_reg[28]_i_3 
       (.I0(\aa_reg[29]_i_2_n_0 ),
        .I1(\bb_reg_reg[0]_3 ),
        .I2(\aa_reg[29]_i_3_n_0 ),
        .I3(\aa_reg[28]_i_6_n_0 ),
        .I4(a_bus[31]),
        .I5(a_bus[28]),
        .O(\upper_reg[28]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h84FF8400)) 
    \upper_reg[29]_i_2 
       (.I0(\lower_reg_reg[31] ),
        .I1(a_bus[29]),
        .I2(\lower_reg_reg[31]_0 ),
        .I3(\lower_reg_reg[31]_2 ),
        .I4(\aa_reg_reg[30]_0 ),
        .O(\upper_reg[29]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hCDC8CDCDC8CDC8C8)) 
    \upper_reg[29]_i_3 
       (.I0(\aa_reg[29]_i_2_n_0 ),
        .I1(\aa_reg_reg[30]_0 ),
        .I2(\aa_reg[29]_i_3_n_0 ),
        .I3(\aa_reg[29]_i_5_n_0 ),
        .I4(a_bus[31]),
        .I5(a_bus[29]),
        .O(\upper_reg[29]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h84FF8400)) 
    \upper_reg[2]_i_2 
       (.I0(\lower_reg_reg[31] ),
        .I1(a_bus[2]),
        .I2(\lower_reg_reg[31]_0 ),
        .I3(\lower_reg_reg[31]_2 ),
        .I4(\bb_reg_reg[0]_13 ),
        .O(\upper_reg[2]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hCDC8CDCDC8CDC8C8)) 
    \upper_reg[2]_i_3 
       (.I0(\aa_reg[29]_i_2_n_0 ),
        .I1(\bb_reg_reg[0]_13 ),
        .I2(\aa_reg[29]_i_3_n_0 ),
        .I3(\aa_reg[2]_i_4_n_0 ),
        .I4(a_bus[31]),
        .I5(a_bus[2]),
        .O(\upper_reg[2]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h84FF8400)) 
    \upper_reg[30]_i_2 
       (.I0(\lower_reg_reg[31] ),
        .I1(a_bus[30]),
        .I2(\lower_reg_reg[31]_0 ),
        .I3(\lower_reg_reg[31]_2 ),
        .I4(\bb_reg_reg[0]_2 ),
        .O(\upper_reg[30]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hCDC8CDCDC8CDC8C8)) 
    \upper_reg[30]_i_3 
       (.I0(\aa_reg[29]_i_2_n_0 ),
        .I1(\bb_reg_reg[0]_2 ),
        .I2(\aa_reg[29]_i_3_n_0 ),
        .I3(\aa_reg[30]_i_7_n_0 ),
        .I4(a_bus[31]),
        .I5(a_bus[30]),
        .O(\upper_reg[30]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT5 #(
    .INIT(32'hEFFF2226)) 
    \upper_reg[31]_i_1 
       (.I0(\lower_reg_reg[31]_2 ),
        .I1(\lower_reg_reg[31]_1 ),
        .I2(\lower_reg_reg[31] ),
        .I3(\lower_reg_reg[31]_0 ),
        .I4(mode_reg_reg_0),
        .O(E));
  LUT5 #(
    .INIT(32'h84FF8400)) 
    \upper_reg[31]_i_4 
       (.I0(\lower_reg_reg[31] ),
        .I1(a_bus[31]),
        .I2(\lower_reg_reg[31]_0 ),
        .I3(\lower_reg_reg[31]_2 ),
        .I4(sign_reg_reg),
        .O(\upper_reg[31]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hCDC8C8C8)) 
    \upper_reg[31]_i_5 
       (.I0(\aa_reg[29]_i_2_n_0 ),
        .I1(sign_reg_reg),
        .I2(\aa_reg[29]_i_3_n_0 ),
        .I3(a_bus[31]),
        .I4(\aa_reg[31]_i_7_n_0 ),
        .O(\upper_reg[31]_i_5_n_0 ));
  LUT5 #(
    .INIT(32'h84FF8400)) 
    \upper_reg[3]_i_2 
       (.I0(\lower_reg_reg[31] ),
        .I1(a_bus[3]),
        .I2(\lower_reg_reg[31]_0 ),
        .I3(\lower_reg_reg[31]_2 ),
        .I4(\aa_reg_reg[4] ),
        .O(\upper_reg[3]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hCDC8CDCDC8CDC8C8)) 
    \upper_reg[3]_i_3 
       (.I0(\aa_reg[29]_i_2_n_0 ),
        .I1(\aa_reg_reg[4] ),
        .I2(\aa_reg[29]_i_3_n_0 ),
        .I3(\aa_reg[3]_i_4_n_0 ),
        .I4(a_bus[31]),
        .I5(a_bus[3]),
        .O(\upper_reg[3]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h84FF8400)) 
    \upper_reg[4]_i_2 
       (.I0(\lower_reg_reg[31] ),
        .I1(a_bus[4]),
        .I2(\lower_reg_reg[31]_0 ),
        .I3(\lower_reg_reg[31]_2 ),
        .I4(\bb_reg_reg[0]_12 ),
        .O(\upper_reg[4]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hCDC8CDCDC8CDC8C8)) 
    \upper_reg[4]_i_3 
       (.I0(\aa_reg[29]_i_2_n_0 ),
        .I1(\bb_reg_reg[0]_12 ),
        .I2(\aa_reg[29]_i_3_n_0 ),
        .I3(\aa_reg[4]_i_3_n_0 ),
        .I4(a_bus[31]),
        .I5(a_bus[4]),
        .O(\upper_reg[4]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h84FF8400)) 
    \upper_reg[5]_i_2 
       (.I0(\lower_reg_reg[31] ),
        .I1(a_bus[5]),
        .I2(\lower_reg_reg[31]_0 ),
        .I3(\lower_reg_reg[31]_2 ),
        .I4(\aa_reg_reg[6] ),
        .O(\upper_reg[5]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hCDC8CDCDC8CDC8C8)) 
    \upper_reg[5]_i_3 
       (.I0(\aa_reg[29]_i_2_n_0 ),
        .I1(\aa_reg_reg[6] ),
        .I2(\aa_reg[29]_i_3_n_0 ),
        .I3(\aa_reg[5]_i_4_n_0 ),
        .I4(a_bus[31]),
        .I5(a_bus[5]),
        .O(\upper_reg[5]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h84FF8400)) 
    \upper_reg[6]_i_2 
       (.I0(\lower_reg_reg[31] ),
        .I1(a_bus[6]),
        .I2(\lower_reg_reg[31]_0 ),
        .I3(\lower_reg_reg[31]_2 ),
        .I4(\bb_reg_reg[0]_11 ),
        .O(\upper_reg[6]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hCDC8CDCDC8CDC8C8)) 
    \upper_reg[6]_i_3 
       (.I0(\aa_reg[29]_i_2_n_0 ),
        .I1(\bb_reg_reg[0]_11 ),
        .I2(\aa_reg[29]_i_3_n_0 ),
        .I3(\aa_reg[6]_i_4_n_0 ),
        .I4(a_bus[31]),
        .I5(a_bus[6]),
        .O(\upper_reg[6]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h84FF8400)) 
    \upper_reg[7]_i_2 
       (.I0(\lower_reg_reg[31] ),
        .I1(a_bus[7]),
        .I2(\lower_reg_reg[31]_0 ),
        .I3(\lower_reg_reg[31]_2 ),
        .I4(\aa_reg_reg[8] ),
        .O(\upper_reg[7]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hCDC8CDCDC8CDC8C8)) 
    \upper_reg[7]_i_3 
       (.I0(\aa_reg[29]_i_2_n_0 ),
        .I1(\aa_reg_reg[8] ),
        .I2(\aa_reg[29]_i_3_n_0 ),
        .I3(\aa_reg[7]_i_4_n_0 ),
        .I4(a_bus[31]),
        .I5(a_bus[7]),
        .O(\upper_reg[7]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h84FF8400)) 
    \upper_reg[8]_i_2 
       (.I0(\lower_reg_reg[31] ),
        .I1(a_bus[8]),
        .I2(\lower_reg_reg[31]_0 ),
        .I3(\lower_reg_reg[31]_2 ),
        .I4(\bb_reg_reg[0]_10 ),
        .O(\upper_reg[8]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hCDC8CDCDC8CDC8C8)) 
    \upper_reg[8]_i_3 
       (.I0(\aa_reg[29]_i_2_n_0 ),
        .I1(\bb_reg_reg[0]_10 ),
        .I2(\aa_reg[29]_i_3_n_0 ),
        .I3(\aa_reg[8]_i_4_n_0 ),
        .I4(a_bus[31]),
        .I5(a_bus[8]),
        .O(\upper_reg[8]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h84FF8400)) 
    \upper_reg[9]_i_2 
       (.I0(\lower_reg_reg[31] ),
        .I1(a_bus[9]),
        .I2(\lower_reg_reg[31]_0 ),
        .I3(\lower_reg_reg[31]_2 ),
        .I4(\aa_reg_reg[10] ),
        .O(\upper_reg[9]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hCDC8CDCDC8CDC8C8)) 
    \upper_reg[9]_i_3 
       (.I0(\aa_reg[29]_i_2_n_0 ),
        .I1(\aa_reg_reg[10] ),
        .I2(\aa_reg[29]_i_3_n_0 ),
        .I3(\aa_reg[9]_i_4_n_0 ),
        .I4(a_bus[31]),
        .I5(a_bus[9]),
        .O(\upper_reg[9]_i_3_n_0 ));
  MUXF7 \upper_reg_reg[10]_i_1 
       (.I0(\upper_reg[10]_i_2_n_0 ),
        .I1(\upper_reg[10]_i_3_n_0 ),
        .O(\upper_reg_reg[31] [10]),
        .S(\lower_reg_reg[31]_1 ));
  MUXF7 \upper_reg_reg[11]_i_1 
       (.I0(\upper_reg[11]_i_2_n_0 ),
        .I1(\upper_reg[11]_i_3_n_0 ),
        .O(\upper_reg_reg[31] [11]),
        .S(\lower_reg_reg[31]_1 ));
  MUXF7 \upper_reg_reg[12]_i_1 
       (.I0(\upper_reg[12]_i_2_n_0 ),
        .I1(\upper_reg[12]_i_3_n_0 ),
        .O(\upper_reg_reg[31] [12]),
        .S(\lower_reg_reg[31]_1 ));
  MUXF7 \upper_reg_reg[13]_i_1 
       (.I0(\upper_reg[13]_i_2_n_0 ),
        .I1(\upper_reg[13]_i_3_n_0 ),
        .O(\upper_reg_reg[31] [13]),
        .S(\lower_reg_reg[31]_1 ));
  MUXF7 \upper_reg_reg[14]_i_1 
       (.I0(\upper_reg[14]_i_2_n_0 ),
        .I1(\upper_reg[14]_i_3_n_0 ),
        .O(\upper_reg_reg[31] [14]),
        .S(\lower_reg_reg[31]_1 ));
  MUXF7 \upper_reg_reg[15]_i_1 
       (.I0(\upper_reg[15]_i_2_n_0 ),
        .I1(\upper_reg[15]_i_3_n_0 ),
        .O(\upper_reg_reg[31] [15]),
        .S(\lower_reg_reg[31]_1 ));
  MUXF7 \upper_reg_reg[16]_i_1 
       (.I0(\upper_reg[16]_i_2_n_0 ),
        .I1(\upper_reg[16]_i_3_n_0 ),
        .O(\upper_reg_reg[31] [16]),
        .S(\lower_reg_reg[31]_1 ));
  MUXF7 \upper_reg_reg[17]_i_1 
       (.I0(\upper_reg[17]_i_2_n_0 ),
        .I1(\upper_reg[17]_i_3_n_0 ),
        .O(\upper_reg_reg[31] [17]),
        .S(\lower_reg_reg[31]_1 ));
  MUXF7 \upper_reg_reg[18]_i_1 
       (.I0(\upper_reg[18]_i_2_n_0 ),
        .I1(\upper_reg[18]_i_3_n_0 ),
        .O(\upper_reg_reg[31] [18]),
        .S(\lower_reg_reg[31]_1 ));
  MUXF7 \upper_reg_reg[19]_i_1 
       (.I0(\upper_reg[19]_i_2_n_0 ),
        .I1(\upper_reg[19]_i_3_n_0 ),
        .O(\upper_reg_reg[31] [19]),
        .S(\lower_reg_reg[31]_1 ));
  MUXF7 \upper_reg_reg[1]_i_1 
       (.I0(\upper_reg[1]_i_2_n_0 ),
        .I1(\upper_reg[1]_i_3_n_0 ),
        .O(\upper_reg_reg[31] [1]),
        .S(\lower_reg_reg[31]_1 ));
  MUXF7 \upper_reg_reg[20]_i_1 
       (.I0(\upper_reg[20]_i_2_n_0 ),
        .I1(\upper_reg[20]_i_3_n_0 ),
        .O(\upper_reg_reg[31] [20]),
        .S(\lower_reg_reg[31]_1 ));
  MUXF7 \upper_reg_reg[21]_i_1 
       (.I0(\upper_reg[21]_i_2_n_0 ),
        .I1(\upper_reg[21]_i_3_n_0 ),
        .O(\upper_reg_reg[31] [21]),
        .S(\lower_reg_reg[31]_1 ));
  MUXF7 \upper_reg_reg[22]_i_1 
       (.I0(\upper_reg[22]_i_2_n_0 ),
        .I1(\upper_reg[22]_i_3_n_0 ),
        .O(\upper_reg_reg[31] [22]),
        .S(\lower_reg_reg[31]_1 ));
  MUXF7 \upper_reg_reg[23]_i_1 
       (.I0(\upper_reg[23]_i_2_n_0 ),
        .I1(\upper_reg[23]_i_3_n_0 ),
        .O(\upper_reg_reg[31] [23]),
        .S(\lower_reg_reg[31]_1 ));
  MUXF7 \upper_reg_reg[24]_i_1 
       (.I0(\upper_reg[24]_i_2_n_0 ),
        .I1(\upper_reg[24]_i_3_n_0 ),
        .O(\upper_reg_reg[31] [24]),
        .S(\lower_reg_reg[31]_1 ));
  MUXF7 \upper_reg_reg[25]_i_1 
       (.I0(\upper_reg[25]_i_2_n_0 ),
        .I1(\upper_reg[25]_i_3_n_0 ),
        .O(\upper_reg_reg[31] [25]),
        .S(\lower_reg_reg[31]_1 ));
  MUXF7 \upper_reg_reg[26]_i_1 
       (.I0(\upper_reg[26]_i_2_n_0 ),
        .I1(\upper_reg[26]_i_3_n_0 ),
        .O(\upper_reg_reg[31] [26]),
        .S(\lower_reg_reg[31]_1 ));
  MUXF7 \upper_reg_reg[27]_i_1 
       (.I0(\upper_reg[27]_i_2_n_0 ),
        .I1(\upper_reg[27]_i_3_n_0 ),
        .O(\upper_reg_reg[31] [27]),
        .S(\lower_reg_reg[31]_1 ));
  MUXF7 \upper_reg_reg[28]_i_1 
       (.I0(\upper_reg[28]_i_2_n_0 ),
        .I1(\upper_reg[28]_i_3_n_0 ),
        .O(\upper_reg_reg[31] [28]),
        .S(\lower_reg_reg[31]_1 ));
  MUXF7 \upper_reg_reg[29]_i_1 
       (.I0(\upper_reg[29]_i_2_n_0 ),
        .I1(\upper_reg[29]_i_3_n_0 ),
        .O(\upper_reg_reg[31] [29]),
        .S(\lower_reg_reg[31]_1 ));
  MUXF7 \upper_reg_reg[2]_i_1 
       (.I0(\upper_reg[2]_i_2_n_0 ),
        .I1(\upper_reg[2]_i_3_n_0 ),
        .O(\upper_reg_reg[31] [2]),
        .S(\lower_reg_reg[31]_1 ));
  MUXF7 \upper_reg_reg[30]_i_1 
       (.I0(\upper_reg[30]_i_2_n_0 ),
        .I1(\upper_reg[30]_i_3_n_0 ),
        .O(\upper_reg_reg[31] [30]),
        .S(\lower_reg_reg[31]_1 ));
  MUXF7 \upper_reg_reg[31]_i_2 
       (.I0(\upper_reg[31]_i_4_n_0 ),
        .I1(\upper_reg[31]_i_5_n_0 ),
        .O(\upper_reg_reg[31] [31]),
        .S(\lower_reg_reg[31]_1 ));
  MUXF7 \upper_reg_reg[3]_i_1 
       (.I0(\upper_reg[3]_i_2_n_0 ),
        .I1(\upper_reg[3]_i_3_n_0 ),
        .O(\upper_reg_reg[31] [3]),
        .S(\lower_reg_reg[31]_1 ));
  MUXF7 \upper_reg_reg[4]_i_1 
       (.I0(\upper_reg[4]_i_2_n_0 ),
        .I1(\upper_reg[4]_i_3_n_0 ),
        .O(\upper_reg_reg[31] [4]),
        .S(\lower_reg_reg[31]_1 ));
  MUXF7 \upper_reg_reg[5]_i_1 
       (.I0(\upper_reg[5]_i_2_n_0 ),
        .I1(\upper_reg[5]_i_3_n_0 ),
        .O(\upper_reg_reg[31] [5]),
        .S(\lower_reg_reg[31]_1 ));
  MUXF7 \upper_reg_reg[6]_i_1 
       (.I0(\upper_reg[6]_i_2_n_0 ),
        .I1(\upper_reg[6]_i_3_n_0 ),
        .O(\upper_reg_reg[31] [6]),
        .S(\lower_reg_reg[31]_1 ));
  MUXF7 \upper_reg_reg[7]_i_1 
       (.I0(\upper_reg[7]_i_2_n_0 ),
        .I1(\upper_reg[7]_i_3_n_0 ),
        .O(\upper_reg_reg[31] [7]),
        .S(\lower_reg_reg[31]_1 ));
  MUXF7 \upper_reg_reg[8]_i_1 
       (.I0(\upper_reg[8]_i_2_n_0 ),
        .I1(\upper_reg[8]_i_3_n_0 ),
        .O(\upper_reg_reg[31] [8]),
        .S(\lower_reg_reg[31]_1 ));
  MUXF7 \upper_reg_reg[9]_i_1 
       (.I0(\upper_reg[9]_i_2_n_0 ),
        .I1(\upper_reg[9]_i_3_n_0 ),
        .O(\upper_reg_reg[31] [9]),
        .S(\lower_reg_reg[31]_1 ));
  LUT6 #(
    .INIT(64'hAABA555500100000)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_1 
       (.I0(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_11_n_0 ),
        .I1(mem_source[0]),
        .I2(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_12_n_0 ),
        .I3(intr_enable_reg_reg_0),
        .I4(c_source),
        .I5(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_15_n_0 ),
        .O(intr_enable_reg_reg_1));
  LUT6 #(
    .INIT(64'h00000000FFFF88B8)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_10 
       (.I0(opcode[24]),
        .I1(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_23_n_0 ),
        .I2(opcode[14]),
        .I3(opcode[23]),
        .I4(\u3_control/is_syscall__2 ),
        .I5(eqOp),
        .O(DPRA3));
  LUT2 #(
    .INIT(4'h2)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_11 
       (.I0(\u3_control/c_source__14 [2]),
        .I1(\u3_control/is_syscall__2 ),
        .O(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hAAFF0C00AA000C00)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_12 
       (.I0(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_27_n_0 ),
        .I1(cpu_data_r[0]),
        .I2(mem_source[1]),
        .I3(mem_source[2]),
        .I4(mem_source[3]),
        .I5(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_28_n_0 ),
        .O(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_12_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_13 
       (.I0(\u3_control/is_syscall__2 ),
        .I1(\u3_control/c_source__14 [0]),
        .O(intr_enable_reg_reg_0));
  LUT6 #(
    .INIT(64'hAAAAAAAAAAAAAEEE)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_14 
       (.I0(\u3_control/is_syscall__2 ),
        .I1(opcode[31]),
        .I2(opcode[27]),
        .I3(opcode[28]),
        .I4(opcode[30]),
        .I5(opcode[29]),
        .O(c_source));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFE200E2)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_15 
       (.I0(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_30_n_0 ),
        .I1(\u6_alu/c_alu1__0 ),
        .I2(p_0_in),
        .I3(\u6_alu/c_alu1127_out ),
        .I4(\u6_alu/bv_adder0__1 ),
        .I5(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_34_n_0 ),
        .O(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_15_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair141" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_16 
       (.I0(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_35_n_0 ),
        .I1(\u3_control/rd__4 [4]),
        .I2(\u3_control/is_syscall__2 ),
        .O(rd_indexD[4]));
  LUT6 #(
    .INIT(64'h0400000000000000)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_17 
       (.I0(rd_indexD[4]),
        .I1(rd_indexD[5]),
        .I2(rd_indexD[0]),
        .I3(rd_indexD[3]),
        .I4(rd_indexD[1]),
        .I5(rd_indexD[2]),
        .O(intr_enable_reg_reg));
  LUT6 #(
    .INIT(64'h5555551555555554)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_18 
       (.I0(pause_in),
        .I1(rd_indexD[5]),
        .I2(rd_indexD[2]),
        .I3(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_38_n_0 ),
        .I4(rd_indexD[4]),
        .I5(rd_indexD[3]),
        .O(\u4_reg_bank/write_enable ));
  (* SOFT_HLUTNM = "soft_lutpair142" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_19 
       (.I0(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_35_n_0 ),
        .I1(\u3_control/rd__4 [0]),
        .I2(\u3_control/is_syscall__2 ),
        .O(rd_indexD[0]));
  LUT3 #(
    .INIT(8'hD0)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_2 
       (.I0(rd_indexD[4]),
        .I1(intr_enable_reg_reg),
        .I2(\u4_reg_bank/write_enable ),
        .O(WE));
  (* SOFT_HLUTNM = "soft_lutpair140" *) 
  LUT3 #(
    .INIT(8'hEA)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_20 
       (.I0(\u3_control/is_syscall__2 ),
        .I1(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_35_n_0 ),
        .I2(\u3_control/rd__4 [1]),
        .O(rd_indexD[1]));
  (* SOFT_HLUTNM = "soft_lutpair140" *) 
  LUT3 #(
    .INIT(8'hEA)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_21 
       (.I0(\u3_control/is_syscall__2 ),
        .I1(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_35_n_0 ),
        .I2(\u3_control/rd__4 [2]),
        .O(rd_indexD[2]));
  (* SOFT_HLUTNM = "soft_lutpair141" *) 
  LUT3 #(
    .INIT(8'hEA)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_22 
       (.I0(\u3_control/is_syscall__2 ),
        .I1(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_35_n_0 ),
        .I2(\u3_control/rd__4 [3]),
        .O(rd_indexD[3]));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFD)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_23 
       (.I0(opcode[30]),
        .I1(opcode[31]),
        .I2(opcode[27]),
        .I3(opcode[26]),
        .I4(opcode[29]),
        .I5(opcode[28]),
        .O(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_23_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000800)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_24 
       (.I0(\xilinx_16x1d.reg_loop[0].reg_bit2a_i_9_n_0 ),
        .I1(opcode[2]),
        .I2(opcode[1]),
        .I3(opcode[3]),
        .I4(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_43_n_0 ),
        .I5(\xilinx_16x1d.reg_loop[0].reg_bit2a_i_7_n_0 ),
        .O(\u3_control/is_syscall__2 ));
  LUT6 #(
    .INIT(64'h0008000000000000)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_25 
       (.I0(rs_index[3]),
        .I1(rs_index[1]),
        .I2(rs_index[4]),
        .I3(rs_index[0]),
        .I4(rs_index[5]),
        .I5(rs_index[2]),
        .O(eqOp));
  LUT6 #(
    .INIT(64'h0000000010000011)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_26 
       (.I0(opcode[31]),
        .I1(opcode[30]),
        .I2(opcode[27]),
        .I3(opcode[28]),
        .I4(opcode[29]),
        .I5(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_50_n_0 ),
        .O(\u3_control/c_source__14 [2]));
  LUT6 #(
    .INIT(64'hF0FFAACCF000AACC)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_27 
       (.I0(cpu_data_r[16]),
        .I1(cpu_data_r[24]),
        .I2(cpu_data_r[0]),
        .I3(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_15_n_0 ),
        .I4(\xilinx_16x1d.reg_loop[1].reg_bit1a_i_3_n_0 ),
        .I5(cpu_data_r[8]),
        .O(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_27_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_28 
       (.I0(cpu_data_r[0]),
        .I1(\xilinx_16x1d.reg_loop[1].reg_bit1a_i_3_n_0 ),
        .I2(cpu_data_r[16]),
        .O(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_28_n_0 ));
  LUT6 #(
    .INIT(64'h0505050500005400)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_29 
       (.I0(opcode[31]),
        .I1(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_51_n_0 ),
        .I2(opcode[30]),
        .I3(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_52_n_0 ),
        .I4(opcode[26]),
        .I5(opcode[29]),
        .O(\u3_control/c_source__14 [0]));
  LUT2 #(
    .INIT(4'h2)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_3 
       (.I0(rd_indexD[0]),
        .I1(intr_enable_reg_reg),
        .O(\bb_reg_reg[31]_2 ));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT5 #(
    .INIT(32'h0000168E)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_30 
       (.I0(b_bus[0]),
        .I1(a_bus[0]),
        .I2(\bram_firmware_code_address_reg[17]_i_11_n_0 ),
        .I3(\bram_firmware_code_address_reg[17]_i_10_n_0 ),
        .I4(\bram_firmware_code_address_reg[17]_i_9_n_0 ),
        .O(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_30_n_0 ));
  LUT4 #(
    .INIT(16'h0024)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_31 
       (.I0(\bram_firmware_code_address_reg[17]_i_15_n_0 ),
        .I1(alu_funcD[2]),
        .I2(alu_funcD[0]),
        .I3(\bram_firmware_code_address_reg[17]_i_13_n_0 ),
        .O(\u6_alu/c_alu1__0 ));
  LUT6 #(
    .INIT(64'hABABFD00A8FF0101)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_32 
       (.I0(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I1(bv_adder4__0),
        .I2(bv_adder489_out),
        .I3(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_55_n_0 ),
        .I4(a_bus[31]),
        .I5(b_bus[31]),
        .O(p_0_in));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_33 
       (.I0(b_bus[0]),
        .I1(a_bus[0]),
        .O(\u6_alu/bv_adder0__1 ));
  LUT5 #(
    .INIT(32'hFFFFB888)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_34 
       (.I0(\lower_reg_reg[31]_5 [0]),
        .I1(\u8_mult/eqOp92_in ),
        .I2(\u8_mult/eqOp__1 ),
        .I3(\upper_reg_reg[31]_0 [0]),
        .I4(c_shift[0]),
        .O(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_34_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFF0008)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_35 
       (.I0(opcode[31]),
        .I1(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_59_n_0 ),
        .I2(opcode[30]),
        .I3(opcode[29]),
        .I4(\u3_control/c_source__14 [0]),
        .I5(\u3_control/c_source__14 [2]),
        .O(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_35_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT5 #(
    .INIT(32'hF0AAFFB8)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_36 
       (.I0(opcode[15]),
        .I1(opcode[23]),
        .I2(opcode[20]),
        .I3(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_60_n_0 ),
        .I4(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_61_n_0 ),
        .O(\u3_control/rd__4 [4]));
  LUT5 #(
    .INIT(32'hAAAAAAEA)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_37 
       (.I0(\u3_control/is_syscall__2 ),
        .I1(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_35_n_0 ),
        .I2(opcode[23]),
        .I3(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_61_n_0 ),
        .I4(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_60_n_0 ),
        .O(rd_indexD[5]));
  LUT2 #(
    .INIT(4'hE)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_38 
       (.I0(rd_indexD[1]),
        .I1(rd_indexD[0]),
        .O(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_38_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT5 #(
    .INIT(32'hF0AAFFB8)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_39 
       (.I0(opcode[11]),
        .I1(opcode[23]),
        .I2(opcode[16]),
        .I3(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_60_n_0 ),
        .I4(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_61_n_0 ),
        .O(\u3_control/rd__4 [0]));
  LUT2 #(
    .INIT(4'h2)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_4 
       (.I0(rd_indexD[1]),
        .I1(intr_enable_reg_reg),
        .O(\bb_reg_reg[31]_1 ));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT5 #(
    .INIT(32'hF0AAFFB8)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_40 
       (.I0(\pc_reg_reg[14] ),
        .I1(opcode[23]),
        .I2(opcode[17]),
        .I3(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_60_n_0 ),
        .I4(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_61_n_0 ),
        .O(\u3_control/rd__4 [1]));
  LUT5 #(
    .INIT(32'hF0AAFFB8)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_41 
       (.I0(opcode[13]),
        .I1(opcode[23]),
        .I2(opcode[18]),
        .I3(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_60_n_0 ),
        .I4(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_61_n_0 ),
        .O(\u3_control/rd__4 [2]));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT5 #(
    .INIT(32'hF0AAFFB8)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_42 
       (.I0(opcode[14]),
        .I1(opcode[23]),
        .I2(opcode[19]),
        .I3(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_60_n_0 ),
        .I4(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_61_n_0 ),
        .O(\u3_control/rd__4 [3]));
  (* SOFT_HLUTNM = "soft_lutpair92" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_43 
       (.I0(opcode[4]),
        .I1(opcode[5]),
        .O(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_43_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFF404040FF40)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_44 
       (.I0(\xilinx_16x1d.reg_loop[0].reg_bit2a_i_7_n_0 ),
        .I1(\xilinx_16x1d.reg_loop[0].reg_bit2a_i_8_n_0 ),
        .I2(\xilinx_16x1d.reg_loop[0].reg_bit2a_i_9_n_0 ),
        .I3(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_62_n_0 ),
        .I4(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_23_n_0 ),
        .I5(opcode[24]),
        .O(rs_index[3]));
  LUT6 #(
    .INIT(64'hFFFFFF404040FF40)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_45 
       (.I0(\xilinx_16x1d.reg_loop[0].reg_bit2a_i_7_n_0 ),
        .I1(\xilinx_16x1d.reg_loop[0].reg_bit2a_i_8_n_0 ),
        .I2(\xilinx_16x1d.reg_loop[0].reg_bit2a_i_9_n_0 ),
        .I3(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_63_n_0 ),
        .I4(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_23_n_0 ),
        .I5(opcode[22]),
        .O(rs_index[1]));
  LUT6 #(
    .INIT(64'hFFFFFF404040FF40)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_46 
       (.I0(\xilinx_16x1d.reg_loop[0].reg_bit2a_i_7_n_0 ),
        .I1(\xilinx_16x1d.reg_loop[0].reg_bit2a_i_8_n_0 ),
        .I2(\xilinx_16x1d.reg_loop[0].reg_bit2a_i_9_n_0 ),
        .I3(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_64_n_0 ),
        .I4(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_23_n_0 ),
        .I5(opcode[25]),
        .O(rs_index[4]));
  LUT6 #(
    .INIT(64'hFFFFFF404040FF40)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_47 
       (.I0(\xilinx_16x1d.reg_loop[0].reg_bit2a_i_7_n_0 ),
        .I1(\xilinx_16x1d.reg_loop[0].reg_bit2a_i_8_n_0 ),
        .I2(\xilinx_16x1d.reg_loop[0].reg_bit2a_i_9_n_0 ),
        .I3(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_65_n_0 ),
        .I4(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_23_n_0 ),
        .I5(opcode[21]),
        .O(rs_index[0]));
  LUT6 #(
    .INIT(64'h000400040004FFFF)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_48 
       (.I0(\xilinx_16x1d.reg_loop[0].reg_bit2a_i_7_n_0 ),
        .I1(\xilinx_16x1d.reg_loop[0].reg_bit2a_i_8_n_0 ),
        .I2(opcode[26]),
        .I3(opcode[31]),
        .I4(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_66_n_0 ),
        .I5(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_67_n_0 ),
        .O(rs_index[5]));
  LUT6 #(
    .INIT(64'hFF40FF4040FF4040)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_49 
       (.I0(\xilinx_16x1d.reg_loop[0].reg_bit2a_i_7_n_0 ),
        .I1(\xilinx_16x1d.reg_loop[0].reg_bit2a_i_8_n_0 ),
        .I2(\xilinx_16x1d.reg_loop[0].reg_bit2a_i_9_n_0 ),
        .I3(opcode[23]),
        .I4(opcode[13]),
        .I5(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_23_n_0 ),
        .O(rs_index[2]));
  LUT2 #(
    .INIT(4'h2)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_5 
       (.I0(rd_indexD[2]),
        .I1(intr_enable_reg_reg),
        .O(\bb_reg_reg[31]_0 ));
  MUXF7 \xilinx_16x1d.reg_loop[0].reg_bit1a_i_50 
       (.I0(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_68_n_0 ),
        .I1(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_69_n_0 ),
        .O(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_50_n_0 ),
        .S(opcode[26]));
  LUT6 #(
    .INIT(64'h0000000575DD4545)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_51 
       (.I0(opcode[3]),
        .I1(opcode[1]),
        .I2(opcode[0]),
        .I3(opcode[2]),
        .I4(opcode[5]),
        .I5(opcode[4]),
        .O(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_51_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_52 
       (.I0(opcode[28]),
        .I1(opcode[27]),
        .O(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_52_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair123" *) 
  LUT3 #(
    .INIT(8'h82)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_53 
       (.I0(a_bus[30]),
        .I1(b_bus[30]),
        .I2(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .O(bv_adder4__0));
  LUT6 #(
    .INIT(64'hEEE8E88800000000)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_54 
       (.I0(a_bus[29]),
        .I1(\pc_reg[29]_i_7_n_0 ),
        .I2(a_bus[28]),
        .I3(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_70_n_0 ),
        .I4(\u6_alu/bv_adder9__3 ),
        .I5(bv_adder5118_out),
        .O(bv_adder489_out));
  LUT4 #(
    .INIT(16'h1000)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_55 
       (.I0(alu_funcD[2]),
        .I1(\bram_firmware_code_address_reg[17]_i_13_n_0 ),
        .I2(alu_funcD[0]),
        .I3(\bram_firmware_code_address_reg[17]_i_15_n_0 ),
        .O(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_55_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT4 #(
    .INIT(16'h0010)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_56 
       (.I0(\lower_reg_reg[31]_2 ),
        .I1(\lower_reg_reg[31]_1 ),
        .I2(\lower_reg_reg[31]_0 ),
        .I3(\lower_reg_reg[31] ),
        .O(\u8_mult/eqOp92_in ));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT4 #(
    .INIT(16'h0010)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_57 
       (.I0(\lower_reg_reg[31]_2 ),
        .I1(\lower_reg_reg[31]_1 ),
        .I2(\lower_reg_reg[31] ),
        .I3(\lower_reg_reg[31]_0 ),
        .O(\u8_mult/eqOp__1 ));
  LUT6 #(
    .INIT(64'h0A0ACAC00000CAC0)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_58 
       (.I0(\bram_firmware_code_address_reg[19]_i_14_n_0 ),
        .I1(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_73_n_0 ),
        .I2(\bram_firmware_code_address_reg[15]_i_7_n_0 ),
        .I3(shift8R[0]),
        .I4(a_bus[4]),
        .I5(shift8R[16]),
        .O(c_shift[0]));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_59 
       (.I0(opcode[28]),
        .I1(opcode[27]),
        .O(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_59_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_6 
       (.I0(rd_indexD[3]),
        .I1(intr_enable_reg_reg),
        .O(\bb_reg_reg[31] ));
  LUT6 #(
    .INIT(64'h1111111104550444)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_60 
       (.I0(opcode[30]),
        .I1(opcode[31]),
        .I2(opcode[27]),
        .I3(opcode[28]),
        .I4(opcode[26]),
        .I5(opcode[29]),
        .O(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_60_n_0 ));
  LUT6 #(
    .INIT(64'hFFFEFFFEFFFFFEFF)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_61 
       (.I0(opcode[31]),
        .I1(opcode[28]),
        .I2(opcode[29]),
        .I3(opcode[30]),
        .I4(opcode[27]),
        .I5(opcode[26]),
        .O(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_61_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_62 
       (.I0(opcode[14]),
        .I1(opcode[23]),
        .O(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_62_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_63 
       (.I0(\pc_reg_reg[14] ),
        .I1(opcode[23]),
        .O(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_63_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_64 
       (.I0(opcode[15]),
        .I1(opcode[23]),
        .O(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_64_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_65 
       (.I0(opcode[11]),
        .I1(opcode[23]),
        .O(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_65_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair74" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_66 
       (.I0(opcode[29]),
        .I1(opcode[26]),
        .I2(opcode[23]),
        .I3(opcode[28]),
        .O(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_66_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair143" *) 
  LUT3 #(
    .INIT(8'hFD)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_67 
       (.I0(opcode[30]),
        .I1(opcode[27]),
        .I2(opcode[31]),
        .O(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_67_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFBF)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_68 
       (.I0(opcode[27]),
        .I1(opcode[0]),
        .I2(opcode[3]),
        .I3(opcode[2]),
        .I4(opcode[1]),
        .I5(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_43_n_0 ),
        .O(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_68_n_0 ));
  LUT5 #(
    .INIT(32'h55555545)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_69 
       (.I0(opcode[27]),
        .I1(opcode[18]),
        .I2(opcode[20]),
        .I3(opcode[19]),
        .I4(opcode[17]),
        .O(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_69_n_0 ));
  LUT6 #(
    .INIT(64'h00000000FFFF88B8)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_7 
       (.I0(opcode[21]),
        .I1(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_23_n_0 ),
        .I2(opcode[11]),
        .I3(opcode[23]),
        .I4(\u3_control/is_syscall__2 ),
        .I5(eqOp),
        .O(DPRA0));
  (* SOFT_HLUTNM = "soft_lutpair161" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_70 
       (.I0(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I1(b_bus[28]),
        .O(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_70_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFEE0FEE00000)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_71 
       (.I0(bv_adder1474_out),
        .I1(bv_adder14__0),
        .I2(\pc_reg[26]_i_7_n_0 ),
        .I3(a_bus[26]),
        .I4(\pc_reg[27]_i_7_n_0 ),
        .I5(a_bus[27]),
        .O(\u6_alu/bv_adder9__3 ));
  (* SOFT_HLUTNM = "soft_lutpair118" *) 
  LUT3 #(
    .INIT(8'hEB)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_72 
       (.I0(a_bus[30]),
        .I1(b_bus[30]),
        .I2(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .O(bv_adder5118_out));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT5 #(
    .INIT(32'h00000010)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_73 
       (.I0(a_bus[2]),
        .I1(a_bus[0]),
        .I2(b_bus[0]),
        .I3(a_bus[1]),
        .I4(a_bus[3]),
        .O(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_73_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_74 
       (.I0(shift4R[8]),
        .I1(a_bus[3]),
        .I2(shift2R[4]),
        .I3(a_bus[2]),
        .I4(shift2R[0]),
        .O(shift8R[0]));
  LUT6 #(
    .INIT(64'hEEE8E88800000000)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_75 
       (.I0(a_bus[24]),
        .I1(\pc_reg[24]_i_7_n_0 ),
        .I2(a_bus[23]),
        .I3(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_78_n_0 ),
        .I4(\u6_alu/bv_adder19__3 ),
        .I5(bv_adder1598_out),
        .O(bv_adder1474_out));
  (* SOFT_HLUTNM = "soft_lutpair127" *) 
  LUT3 #(
    .INIT(8'h82)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_76 
       (.I0(a_bus[25]),
        .I1(b_bus[25]),
        .I2(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .O(bv_adder14__0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_77 
       (.I0(b_bus[3]),
        .I1(b_bus[2]),
        .I2(a_bus[1]),
        .I3(b_bus[1]),
        .I4(a_bus[0]),
        .I5(b_bus[0]),
        .O(shift2R[0]));
  (* SOFT_HLUTNM = "soft_lutpair155" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_78 
       (.I0(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I1(b_bus[23]),
        .O(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_78_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair100" *) 
  LUT3 #(
    .INIT(8'hEB)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_79 
       (.I0(a_bus[25]),
        .I1(b_bus[25]),
        .I2(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .O(bv_adder1598_out));
  LUT6 #(
    .INIT(64'h00000000FFFF88B8)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_8 
       (.I0(opcode[22]),
        .I1(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_23_n_0 ),
        .I2(\pc_reg_reg[14] ),
        .I3(opcode[23]),
        .I4(\u3_control/is_syscall__2 ),
        .I5(eqOp),
        .O(DPRA1));
  LUT5 #(
    .INIT(32'h0000FFA4)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a_i_9 
       (.I0(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_23_n_0 ),
        .I1(opcode[13]),
        .I2(opcode[23]),
        .I3(\u3_control/is_syscall__2 ),
        .I4(eqOp),
        .O(DPRA2));
  LUT3 #(
    .INIT(8'h20)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1b_i_1 
       (.I0(rd_indexD[4]),
        .I1(intr_enable_reg_reg),
        .I2(\u4_reg_bank/write_enable ),
        .O(\bb_reg_reg[31]_3 ));
  LUT6 #(
    .INIT(64'hE200E2E2E2E2E2E2)) 
    \xilinx_16x1d.reg_loop[0].reg_bit2a_i_1 
       (.I0(\xilinx_16x1d.reg_loop[0].reg_bit2a_i_5_n_0 ),
        .I1(\xilinx_16x1d.reg_loop[0].reg_bit2a_i_6_n_0 ),
        .I2(opcode[16]),
        .I3(\xilinx_16x1d.reg_loop[0].reg_bit2a_i_7_n_0 ),
        .I4(\xilinx_16x1d.reg_loop[0].reg_bit2a_i_8_n_0 ),
        .I5(\xilinx_16x1d.reg_loop[0].reg_bit2a_i_9_n_0 ),
        .O(\bb_reg_reg[1]_3 ));
  (* SOFT_HLUTNM = "soft_lutpair106" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \xilinx_16x1d.reg_loop[0].reg_bit2a_i_10 
       (.I0(opcode[17]),
        .I1(opcode[23]),
        .I2(opcode[26]),
        .O(\xilinx_16x1d.reg_loop[0].reg_bit2a_i_10_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair120" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \xilinx_16x1d.reg_loop[0].reg_bit2a_i_11 
       (.I0(opcode[18]),
        .I1(opcode[23]),
        .I2(opcode[26]),
        .O(\xilinx_16x1d.reg_loop[0].reg_bit2a_i_11_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair120" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \xilinx_16x1d.reg_loop[0].reg_bit2a_i_12 
       (.I0(opcode[19]),
        .I1(opcode[23]),
        .I2(opcode[26]),
        .O(\xilinx_16x1d.reg_loop[0].reg_bit2a_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hE200E2E2E2E2E2E2)) 
    \xilinx_16x1d.reg_loop[0].reg_bit2a_i_2 
       (.I0(\xilinx_16x1d.reg_loop[0].reg_bit2a_i_10_n_0 ),
        .I1(\xilinx_16x1d.reg_loop[0].reg_bit2a_i_6_n_0 ),
        .I2(opcode[17]),
        .I3(\xilinx_16x1d.reg_loop[0].reg_bit2a_i_7_n_0 ),
        .I4(\xilinx_16x1d.reg_loop[0].reg_bit2a_i_8_n_0 ),
        .I5(\xilinx_16x1d.reg_loop[0].reg_bit2a_i_9_n_0 ),
        .O(\bb_reg_reg[1]_2 ));
  LUT6 #(
    .INIT(64'hE200E2E2E2E2E2E2)) 
    \xilinx_16x1d.reg_loop[0].reg_bit2a_i_3 
       (.I0(\xilinx_16x1d.reg_loop[0].reg_bit2a_i_11_n_0 ),
        .I1(\xilinx_16x1d.reg_loop[0].reg_bit2a_i_6_n_0 ),
        .I2(opcode[18]),
        .I3(\xilinx_16x1d.reg_loop[0].reg_bit2a_i_7_n_0 ),
        .I4(\xilinx_16x1d.reg_loop[0].reg_bit2a_i_8_n_0 ),
        .I5(\xilinx_16x1d.reg_loop[0].reg_bit2a_i_9_n_0 ),
        .O(\bb_reg_reg[1]_1 ));
  LUT6 #(
    .INIT(64'hE200E2E2E2E2E2E2)) 
    \xilinx_16x1d.reg_loop[0].reg_bit2a_i_4 
       (.I0(\xilinx_16x1d.reg_loop[0].reg_bit2a_i_12_n_0 ),
        .I1(\xilinx_16x1d.reg_loop[0].reg_bit2a_i_6_n_0 ),
        .I2(opcode[19]),
        .I3(\xilinx_16x1d.reg_loop[0].reg_bit2a_i_7_n_0 ),
        .I4(\xilinx_16x1d.reg_loop[0].reg_bit2a_i_8_n_0 ),
        .I5(\xilinx_16x1d.reg_loop[0].reg_bit2a_i_9_n_0 ),
        .O(\bb_reg_reg[1]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair85" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \xilinx_16x1d.reg_loop[0].reg_bit2a_i_5 
       (.I0(opcode[16]),
        .I1(opcode[23]),
        .I2(opcode[26]),
        .O(\xilinx_16x1d.reg_loop[0].reg_bit2a_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFEEF)) 
    \xilinx_16x1d.reg_loop[0].reg_bit2a_i_6 
       (.I0(opcode[29]),
        .I1(opcode[27]),
        .I2(opcode[30]),
        .I3(opcode[26]),
        .I4(opcode[31]),
        .I5(opcode[28]),
        .O(\xilinx_16x1d.reg_loop[0].reg_bit2a_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair77" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \xilinx_16x1d.reg_loop[0].reg_bit2a_i_7 
       (.I0(opcode[29]),
        .I1(opcode[30]),
        .I2(opcode[27]),
        .I3(opcode[28]),
        .O(\xilinx_16x1d.reg_loop[0].reg_bit2a_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT5 #(
    .INIT(32'h00000020)) 
    \xilinx_16x1d.reg_loop[0].reg_bit2a_i_8 
       (.I0(opcode[2]),
        .I1(opcode[1]),
        .I2(opcode[3]),
        .I3(opcode[4]),
        .I4(opcode[5]),
        .O(\xilinx_16x1d.reg_loop[0].reg_bit2a_i_8_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \xilinx_16x1d.reg_loop[0].reg_bit2a_i_9 
       (.I0(opcode[31]),
        .I1(opcode[26]),
        .O(\xilinx_16x1d.reg_loop[0].reg_bit2a_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hFFF02F2F0F002020)) 
    \xilinx_16x1d.reg_loop[10].reg_bit1a_i_1 
       (.I0(pc_plus4[7]),
        .I1(intr_enable_reg_reg_0),
        .I2(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_11_n_0 ),
        .I3(\xilinx_16x1d.reg_loop[10].reg_bit1a_i_2_n_0 ),
        .I4(c_source),
        .I5(pc_new[10]),
        .O(reg_dest[9]));
  LUT6 #(
    .INIT(64'h88888888BBB888B8)) 
    \xilinx_16x1d.reg_loop[10].reg_bit1a_i_2 
       (.I0(pc_current[10]),
        .I1(intr_enable_reg_reg_0),
        .I2(\xilinx_16x1d.reg_loop[10].reg_bit1a_i_3_n_0 ),
        .I3(mem_source[2]),
        .I4(\xilinx_16x1d.reg_loop[10].reg_bit1a_i_4_n_0 ),
        .I5(mem_source[0]),
        .O(\xilinx_16x1d.reg_loop[10].reg_bit1a_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hA808)) 
    \xilinx_16x1d.reg_loop[10].reg_bit1a_i_3 
       (.I0(mem_source[3]),
        .I1(cpu_data_r[26]),
        .I2(\xilinx_16x1d.reg_loop[1].reg_bit1a_i_3_n_0 ),
        .I3(cpu_data_r[10]),
        .O(\xilinx_16x1d.reg_loop[10].reg_bit1a_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAA00AA00A2FFA200)) 
    \xilinx_16x1d.reg_loop[10].reg_bit1a_i_4 
       (.I0(\xilinx_16x1d.reg_loop[7].reg_bit1a_i_4_n_0 ),
        .I1(mem_source[2]),
        .I2(mem_source[0]),
        .I3(mem_source[3]),
        .I4(cpu_data_r[10]),
        .I5(mem_source[1]),
        .O(\xilinx_16x1d.reg_loop[10].reg_bit1a_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFF02F2F0F002020)) 
    \xilinx_16x1d.reg_loop[11].reg_bit1a_i_1 
       (.I0(pc_plus4[8]),
        .I1(intr_enable_reg_reg_0),
        .I2(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_11_n_0 ),
        .I3(\xilinx_16x1d.reg_loop[11].reg_bit1a_i_2_n_0 ),
        .I4(c_source),
        .I5(pc_new[11]),
        .O(reg_dest[10]));
  LUT6 #(
    .INIT(64'h88888888BBB888B8)) 
    \xilinx_16x1d.reg_loop[11].reg_bit1a_i_2 
       (.I0(pc_current[11]),
        .I1(intr_enable_reg_reg_0),
        .I2(\xilinx_16x1d.reg_loop[11].reg_bit1a_i_3_n_0 ),
        .I3(mem_source[2]),
        .I4(\xilinx_16x1d.reg_loop[11].reg_bit1a_i_4_n_0 ),
        .I5(mem_source[0]),
        .O(\xilinx_16x1d.reg_loop[11].reg_bit1a_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hA808)) 
    \xilinx_16x1d.reg_loop[11].reg_bit1a_i_3 
       (.I0(mem_source[3]),
        .I1(cpu_data_r[27]),
        .I2(\xilinx_16x1d.reg_loop[1].reg_bit1a_i_3_n_0 ),
        .I3(cpu_data_r[11]),
        .O(\xilinx_16x1d.reg_loop[11].reg_bit1a_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAA00AA00A2FFA200)) 
    \xilinx_16x1d.reg_loop[11].reg_bit1a_i_4 
       (.I0(\xilinx_16x1d.reg_loop[7].reg_bit1a_i_4_n_0 ),
        .I1(mem_source[2]),
        .I2(mem_source[0]),
        .I3(mem_source[3]),
        .I4(cpu_data_r[11]),
        .I5(mem_source[1]),
        .O(\xilinx_16x1d.reg_loop[11].reg_bit1a_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFF02F2F0F002020)) 
    \xilinx_16x1d.reg_loop[12].reg_bit1a_i_1 
       (.I0(pc_plus4[9]),
        .I1(intr_enable_reg_reg_0),
        .I2(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_11_n_0 ),
        .I3(\xilinx_16x1d.reg_loop[12].reg_bit1a_i_2_n_0 ),
        .I4(c_source),
        .I5(pc_new[12]),
        .O(reg_dest[11]));
  LUT6 #(
    .INIT(64'h88888888BBB888B8)) 
    \xilinx_16x1d.reg_loop[12].reg_bit1a_i_2 
       (.I0(pc_current[12]),
        .I1(intr_enable_reg_reg_0),
        .I2(\xilinx_16x1d.reg_loop[12].reg_bit1a_i_3_n_0 ),
        .I3(mem_source[2]),
        .I4(\xilinx_16x1d.reg_loop[12].reg_bit1a_i_4_n_0 ),
        .I5(mem_source[0]),
        .O(\xilinx_16x1d.reg_loop[12].reg_bit1a_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hA808)) 
    \xilinx_16x1d.reg_loop[12].reg_bit1a_i_3 
       (.I0(mem_source[3]),
        .I1(cpu_data_r[28]),
        .I2(\xilinx_16x1d.reg_loop[1].reg_bit1a_i_3_n_0 ),
        .I3(cpu_data_r[12]),
        .O(\xilinx_16x1d.reg_loop[12].reg_bit1a_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAA00AA00A2FFA200)) 
    \xilinx_16x1d.reg_loop[12].reg_bit1a_i_4 
       (.I0(\xilinx_16x1d.reg_loop[7].reg_bit1a_i_4_n_0 ),
        .I1(mem_source[2]),
        .I2(mem_source[0]),
        .I3(mem_source[3]),
        .I4(cpu_data_r[12]),
        .I5(mem_source[1]),
        .O(\xilinx_16x1d.reg_loop[12].reg_bit1a_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFF02F2F0F002020)) 
    \xilinx_16x1d.reg_loop[13].reg_bit1a_i_1 
       (.I0(pc_plus4[10]),
        .I1(intr_enable_reg_reg_0),
        .I2(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_11_n_0 ),
        .I3(\xilinx_16x1d.reg_loop[13].reg_bit1a_i_2_n_0 ),
        .I4(c_source),
        .I5(pc_new[13]),
        .O(reg_dest[12]));
  LUT6 #(
    .INIT(64'h88888888BBB888B8)) 
    \xilinx_16x1d.reg_loop[13].reg_bit1a_i_2 
       (.I0(pc_current[13]),
        .I1(intr_enable_reg_reg_0),
        .I2(\xilinx_16x1d.reg_loop[13].reg_bit1a_i_3_n_0 ),
        .I3(mem_source[2]),
        .I4(\xilinx_16x1d.reg_loop[13].reg_bit1a_i_4_n_0 ),
        .I5(mem_source[0]),
        .O(\xilinx_16x1d.reg_loop[13].reg_bit1a_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hA808)) 
    \xilinx_16x1d.reg_loop[13].reg_bit1a_i_3 
       (.I0(mem_source[3]),
        .I1(cpu_data_r[29]),
        .I2(\xilinx_16x1d.reg_loop[1].reg_bit1a_i_3_n_0 ),
        .I3(cpu_data_r[13]),
        .O(\xilinx_16x1d.reg_loop[13].reg_bit1a_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAA00AA00A2FFA200)) 
    \xilinx_16x1d.reg_loop[13].reg_bit1a_i_4 
       (.I0(\xilinx_16x1d.reg_loop[7].reg_bit1a_i_4_n_0 ),
        .I1(mem_source[2]),
        .I2(mem_source[0]),
        .I3(mem_source[3]),
        .I4(cpu_data_r[13]),
        .I5(mem_source[1]),
        .O(\xilinx_16x1d.reg_loop[13].reg_bit1a_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFF02F2F0F002020)) 
    \xilinx_16x1d.reg_loop[14].reg_bit1a_i_1 
       (.I0(pc_plus4[11]),
        .I1(intr_enable_reg_reg_0),
        .I2(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_11_n_0 ),
        .I3(\xilinx_16x1d.reg_loop[14].reg_bit1a_i_2_n_0 ),
        .I4(c_source),
        .I5(pc_new[14]),
        .O(reg_dest[13]));
  LUT6 #(
    .INIT(64'h88888888BBB888B8)) 
    \xilinx_16x1d.reg_loop[14].reg_bit1a_i_2 
       (.I0(pc_current[14]),
        .I1(intr_enable_reg_reg_0),
        .I2(\xilinx_16x1d.reg_loop[14].reg_bit1a_i_3_n_0 ),
        .I3(mem_source[2]),
        .I4(\xilinx_16x1d.reg_loop[14].reg_bit1a_i_4_n_0 ),
        .I5(mem_source[0]),
        .O(\xilinx_16x1d.reg_loop[14].reg_bit1a_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hA808)) 
    \xilinx_16x1d.reg_loop[14].reg_bit1a_i_3 
       (.I0(mem_source[3]),
        .I1(cpu_data_r[30]),
        .I2(\xilinx_16x1d.reg_loop[1].reg_bit1a_i_3_n_0 ),
        .I3(cpu_data_r[14]),
        .O(\xilinx_16x1d.reg_loop[14].reg_bit1a_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAA00AA00A2FFA200)) 
    \xilinx_16x1d.reg_loop[14].reg_bit1a_i_4 
       (.I0(\xilinx_16x1d.reg_loop[7].reg_bit1a_i_4_n_0 ),
        .I1(mem_source[2]),
        .I2(mem_source[0]),
        .I3(mem_source[3]),
        .I4(cpu_data_r[14]),
        .I5(mem_source[1]),
        .O(\xilinx_16x1d.reg_loop[14].reg_bit1a_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFF02F2F0F002020)) 
    \xilinx_16x1d.reg_loop[15].reg_bit1a_i_1 
       (.I0(pc_plus4[12]),
        .I1(intr_enable_reg_reg_0),
        .I2(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_11_n_0 ),
        .I3(\xilinx_16x1d.reg_loop[15].reg_bit1a_i_2_n_0 ),
        .I4(c_source),
        .I5(pc_new[15]),
        .O(reg_dest[14]));
  LUT6 #(
    .INIT(64'h88888888BBB888B8)) 
    \xilinx_16x1d.reg_loop[15].reg_bit1a_i_2 
       (.I0(pc_current[15]),
        .I1(intr_enable_reg_reg_0),
        .I2(\xilinx_16x1d.reg_loop[15].reg_bit1a_i_3_n_0 ),
        .I3(mem_source[2]),
        .I4(\xilinx_16x1d.reg_loop[15].reg_bit1a_i_4_n_0 ),
        .I5(mem_source[0]),
        .O(\xilinx_16x1d.reg_loop[15].reg_bit1a_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair76" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \xilinx_16x1d.reg_loop[15].reg_bit1a_i_3 
       (.I0(mem_source[3]),
        .I1(data_read_var__15),
        .O(\xilinx_16x1d.reg_loop[15].reg_bit1a_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAA00AA00A2FFA200)) 
    \xilinx_16x1d.reg_loop[15].reg_bit1a_i_4 
       (.I0(\xilinx_16x1d.reg_loop[7].reg_bit1a_i_4_n_0 ),
        .I1(mem_source[2]),
        .I2(mem_source[0]),
        .I3(mem_source[3]),
        .I4(cpu_data_r[15]),
        .I5(mem_source[1]),
        .O(\xilinx_16x1d.reg_loop[15].reg_bit1a_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair101" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \xilinx_16x1d.reg_loop[15].reg_bit1a_i_5 
       (.I0(cpu_data_r[15]),
        .I1(\xilinx_16x1d.reg_loop[1].reg_bit1a_i_3_n_0 ),
        .I2(cpu_data_r[31]),
        .O(data_read_var__15));
  LUT5 #(
    .INIT(32'hFCBB3088)) 
    \xilinx_16x1d.reg_loop[16].reg_bit1a_i_1 
       (.I0(\xilinx_16x1d.reg_loop[16].reg_bit1a_i_2_n_0 ),
        .I1(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_11_n_0 ),
        .I2(\xilinx_16x1d.reg_loop[16].reg_bit1a_i_3_n_0 ),
        .I3(c_source),
        .I4(pc_new[16]),
        .O(reg_dest[15]));
  (* SOFT_HLUTNM = "soft_lutpair117" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \xilinx_16x1d.reg_loop[16].reg_bit1a_i_2 
       (.I0(opcode[0]),
        .I1(intr_enable_reg_reg_0),
        .I2(pc_plus4[13]),
        .O(\xilinx_16x1d.reg_loop[16].reg_bit1a_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h88888888BBB888B8)) 
    \xilinx_16x1d.reg_loop[16].reg_bit1a_i_3 
       (.I0(pc_current[16]),
        .I1(intr_enable_reg_reg_0),
        .I2(\xilinx_16x1d.reg_loop[16].reg_bit1a_i_4_n_0 ),
        .I3(mem_source[2]),
        .I4(\xilinx_16x1d.reg_loop[16].reg_bit1a_i_5_n_0 ),
        .I5(mem_source[0]),
        .O(\xilinx_16x1d.reg_loop[16].reg_bit1a_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT5 #(
    .INIT(32'hAA00A800)) 
    \xilinx_16x1d.reg_loop[16].reg_bit1a_i_4 
       (.I0(data_read_var__15),
        .I1(mem_source[0]),
        .I2(mem_source[2]),
        .I3(mem_source[3]),
        .I4(mem_source[1]),
        .O(\xilinx_16x1d.reg_loop[16].reg_bit1a_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAA00AA00A2FFA200)) 
    \xilinx_16x1d.reg_loop[16].reg_bit1a_i_5 
       (.I0(\xilinx_16x1d.reg_loop[7].reg_bit1a_i_4_n_0 ),
        .I1(mem_source[2]),
        .I2(mem_source[0]),
        .I3(mem_source[3]),
        .I4(cpu_data_r[16]),
        .I5(mem_source[1]),
        .O(\xilinx_16x1d.reg_loop[16].reg_bit1a_i_5_n_0 ));
  LUT5 #(
    .INIT(32'hFCBB3088)) 
    \xilinx_16x1d.reg_loop[17].reg_bit1a_i_1 
       (.I0(\xilinx_16x1d.reg_loop[17].reg_bit1a_i_2_n_0 ),
        .I1(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_11_n_0 ),
        .I2(\xilinx_16x1d.reg_loop[17].reg_bit1a_i_3_n_0 ),
        .I3(c_source),
        .I4(pc_new[17]),
        .O(reg_dest[16]));
  LUT3 #(
    .INIT(8'hB8)) 
    \xilinx_16x1d.reg_loop[17].reg_bit1a_i_2 
       (.I0(opcode[1]),
        .I1(intr_enable_reg_reg_0),
        .I2(pc_plus4[14]),
        .O(\xilinx_16x1d.reg_loop[17].reg_bit1a_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h88888888BBB888B8)) 
    \xilinx_16x1d.reg_loop[17].reg_bit1a_i_3 
       (.I0(pc_current[17]),
        .I1(intr_enable_reg_reg_0),
        .I2(\xilinx_16x1d.reg_loop[16].reg_bit1a_i_4_n_0 ),
        .I3(mem_source[2]),
        .I4(\xilinx_16x1d.reg_loop[17].reg_bit1a_i_4_n_0 ),
        .I5(mem_source[0]),
        .O(\xilinx_16x1d.reg_loop[17].reg_bit1a_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAA00AA00A2FFA200)) 
    \xilinx_16x1d.reg_loop[17].reg_bit1a_i_4 
       (.I0(\xilinx_16x1d.reg_loop[7].reg_bit1a_i_4_n_0 ),
        .I1(mem_source[2]),
        .I2(mem_source[0]),
        .I3(mem_source[3]),
        .I4(cpu_data_r[17]),
        .I5(mem_source[1]),
        .O(\xilinx_16x1d.reg_loop[17].reg_bit1a_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFCBB3088)) 
    \xilinx_16x1d.reg_loop[18].reg_bit1a_i_1 
       (.I0(\xilinx_16x1d.reg_loop[18].reg_bit1a_i_2_n_0 ),
        .I1(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_11_n_0 ),
        .I2(\xilinx_16x1d.reg_loop[18].reg_bit1a_i_3_n_0 ),
        .I3(c_source),
        .I4(pc_new[18]),
        .O(reg_dest[17]));
  (* SOFT_HLUTNM = "soft_lutpair117" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \xilinx_16x1d.reg_loop[18].reg_bit1a_i_2 
       (.I0(opcode[2]),
        .I1(intr_enable_reg_reg_0),
        .I2(pc_plus4[15]),
        .O(\xilinx_16x1d.reg_loop[18].reg_bit1a_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h88888888BBB888B8)) 
    \xilinx_16x1d.reg_loop[18].reg_bit1a_i_3 
       (.I0(pc_current[18]),
        .I1(intr_enable_reg_reg_0),
        .I2(\xilinx_16x1d.reg_loop[16].reg_bit1a_i_4_n_0 ),
        .I3(mem_source[2]),
        .I4(\xilinx_16x1d.reg_loop[18].reg_bit1a_i_4_n_0 ),
        .I5(mem_source[0]),
        .O(\xilinx_16x1d.reg_loop[18].reg_bit1a_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAA00AA00A2FFA200)) 
    \xilinx_16x1d.reg_loop[18].reg_bit1a_i_4 
       (.I0(\xilinx_16x1d.reg_loop[7].reg_bit1a_i_4_n_0 ),
        .I1(mem_source[2]),
        .I2(mem_source[0]),
        .I3(mem_source[3]),
        .I4(cpu_data_r[18]),
        .I5(mem_source[1]),
        .O(\xilinx_16x1d.reg_loop[18].reg_bit1a_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFCBB3088)) 
    \xilinx_16x1d.reg_loop[19].reg_bit1a_i_1 
       (.I0(\xilinx_16x1d.reg_loop[19].reg_bit1a_i_2_n_0 ),
        .I1(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_11_n_0 ),
        .I2(\xilinx_16x1d.reg_loop[19].reg_bit1a_i_3_n_0 ),
        .I3(c_source),
        .I4(pc_new[19]),
        .O(reg_dest[18]));
  (* SOFT_HLUTNM = "soft_lutpair99" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \xilinx_16x1d.reg_loop[19].reg_bit1a_i_2 
       (.I0(opcode[3]),
        .I1(intr_enable_reg_reg_0),
        .I2(pc_plus4[16]),
        .O(\xilinx_16x1d.reg_loop[19].reg_bit1a_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h88888888BBB888B8)) 
    \xilinx_16x1d.reg_loop[19].reg_bit1a_i_3 
       (.I0(pc_current[19]),
        .I1(intr_enable_reg_reg_0),
        .I2(\xilinx_16x1d.reg_loop[16].reg_bit1a_i_4_n_0 ),
        .I3(mem_source[2]),
        .I4(\xilinx_16x1d.reg_loop[19].reg_bit1a_i_4_n_0 ),
        .I5(mem_source[0]),
        .O(\xilinx_16x1d.reg_loop[19].reg_bit1a_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAA00AA00A2FFA200)) 
    \xilinx_16x1d.reg_loop[19].reg_bit1a_i_4 
       (.I0(\xilinx_16x1d.reg_loop[7].reg_bit1a_i_4_n_0 ),
        .I1(mem_source[2]),
        .I2(mem_source[0]),
        .I3(mem_source[3]),
        .I4(cpu_data_r[19]),
        .I5(mem_source[1]),
        .O(\xilinx_16x1d.reg_loop[19].reg_bit1a_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAABA555500100000)) 
    \xilinx_16x1d.reg_loop[1].reg_bit1a_i_1 
       (.I0(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_11_n_0 ),
        .I1(mem_source[0]),
        .I2(\xilinx_16x1d.reg_loop[1].reg_bit1a_i_2_n_0 ),
        .I3(intr_enable_reg_reg_0),
        .I4(c_source),
        .I5(\xilinx_16x1d.reg_loop[1].reg_bit1a_i_3_n_0 ),
        .O(reg_dest[0]));
  LUT6 #(
    .INIT(64'h0000000000005404)) 
    \xilinx_16x1d.reg_loop[1].reg_bit1a_i_10 
       (.I0(a_bus[2]),
        .I1(b_bus[1]),
        .I2(a_bus[0]),
        .I3(b_bus[0]),
        .I4(a_bus[1]),
        .I5(a_bus[3]),
        .O(\xilinx_16x1d.reg_loop[1].reg_bit1a_i_10_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \xilinx_16x1d.reg_loop[1].reg_bit1a_i_11 
       (.I0(shift4R[9]),
        .I1(a_bus[3]),
        .I2(shift2R[5]),
        .I3(a_bus[2]),
        .I4(shift2R[1]),
        .O(shift8R[1]));
  LUT5 #(
    .INIT(32'h0F000208)) 
    \xilinx_16x1d.reg_loop[1].reg_bit1a_i_12 
       (.I0(\pc_reg_reg[25]_1 ),
        .I1(\upper_reg_reg[31]_0 [0]),
        .I2(\pc_reg_reg[30]_0 ),
        .I3(\upper_reg_reg[31]_0 [1]),
        .I4(\pc_reg_reg[25] ),
        .O(\xilinx_16x1d.reg_loop[1].reg_bit1a_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \xilinx_16x1d.reg_loop[1].reg_bit1a_i_13 
       (.I0(b_bus[4]),
        .I1(b_bus[3]),
        .I2(a_bus[1]),
        .I3(b_bus[2]),
        .I4(a_bus[0]),
        .I5(b_bus[1]),
        .O(shift2R[1]));
  LUT6 #(
    .INIT(64'hAAFF0C00AA000C00)) 
    \xilinx_16x1d.reg_loop[1].reg_bit1a_i_2 
       (.I0(\xilinx_16x1d.reg_loop[1].reg_bit1a_i_4_n_0 ),
        .I1(cpu_data_r[1]),
        .I2(mem_source[1]),
        .I3(mem_source[2]),
        .I4(mem_source[3]),
        .I5(\xilinx_16x1d.reg_loop[1].reg_bit1a_i_5_n_0 ),
        .O(\xilinx_16x1d.reg_loop[1].reg_bit1a_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFE2)) 
    \xilinx_16x1d.reg_loop[1].reg_bit1a_i_3 
       (.I0(\xilinx_16x1d.reg_loop[1].reg_bit1a_i_6_n_0 ),
        .I1(\u6_alu/c_alu1127_out ),
        .I2(\u6_alu/bv_adder01_out ),
        .I3(c_shift[1]),
        .I4(c_mult[1]),
        .O(\xilinx_16x1d.reg_loop[1].reg_bit1a_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hF0FFAACCF000AACC)) 
    \xilinx_16x1d.reg_loop[1].reg_bit1a_i_4 
       (.I0(cpu_data_r[17]),
        .I1(cpu_data_r[25]),
        .I2(cpu_data_r[1]),
        .I3(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_15_n_0 ),
        .I4(\xilinx_16x1d.reg_loop[1].reg_bit1a_i_3_n_0 ),
        .I5(cpu_data_r[9]),
        .O(\xilinx_16x1d.reg_loop[1].reg_bit1a_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair113" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \xilinx_16x1d.reg_loop[1].reg_bit1a_i_5 
       (.I0(cpu_data_r[1]),
        .I1(\xilinx_16x1d.reg_loop[1].reg_bit1a_i_3_n_0 ),
        .I2(cpu_data_r[17]),
        .O(\xilinx_16x1d.reg_loop[1].reg_bit1a_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h0000000011050540)) 
    \xilinx_16x1d.reg_loop[1].reg_bit1a_i_6 
       (.I0(\bram_firmware_code_address_reg[17]_i_9_n_0 ),
        .I1(\bram_firmware_code_address_reg[17]_i_10_n_0 ),
        .I2(\bram_firmware_code_address_reg[17]_i_11_n_0 ),
        .I3(a_bus[1]),
        .I4(b_bus[1]),
        .I5(\u6_alu/c_alu1__0 ),
        .O(\xilinx_16x1d.reg_loop[1].reg_bit1a_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT5 #(
    .INIT(32'h96666966)) 
    \xilinx_16x1d.reg_loop[1].reg_bit1a_i_7 
       (.I0(b_bus[1]),
        .I1(a_bus[1]),
        .I2(\bram_firmware_code_address_reg[19]_i_11_n_0 ),
        .I3(b_bus[0]),
        .I4(a_bus[0]),
        .O(\u6_alu/bv_adder01_out ));
  LUT6 #(
    .INIT(64'h0A0ACAC00000CAC0)) 
    \xilinx_16x1d.reg_loop[1].reg_bit1a_i_8 
       (.I0(\bram_firmware_code_address_reg[19]_i_14_n_0 ),
        .I1(\xilinx_16x1d.reg_loop[1].reg_bit1a_i_10_n_0 ),
        .I2(\bram_firmware_code_address_reg[15]_i_7_n_0 ),
        .I3(shift8R[1]),
        .I4(a_bus[4]),
        .I5(shift8R[17]),
        .O(c_shift[1]));
  LUT6 #(
    .INIT(64'hFFFFF4000000F800)) 
    \xilinx_16x1d.reg_loop[1].reg_bit1a_i_9 
       (.I0(\lower_reg_reg[31]_5 [0]),
        .I1(\pc_reg_reg[30]_0 ),
        .I2(\xilinx_16x1d.reg_loop[1].reg_bit1a_i_12_n_0 ),
        .I3(\pc_reg_reg[30]_1 ),
        .I4(\bram_firmware_code_address_reg[19]_i_10_n_0 ),
        .I5(\lower_reg_reg[31]_5 [1]),
        .O(c_mult[1]));
  LUT5 #(
    .INIT(32'hFCBB3088)) 
    \xilinx_16x1d.reg_loop[20].reg_bit1a_i_1 
       (.I0(\xilinx_16x1d.reg_loop[20].reg_bit1a_i_2_n_0 ),
        .I1(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_11_n_0 ),
        .I2(\xilinx_16x1d.reg_loop[20].reg_bit1a_i_3_n_0 ),
        .I3(c_source),
        .I4(pc_new[20]),
        .O(reg_dest[19]));
  (* SOFT_HLUTNM = "soft_lutpair92" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \xilinx_16x1d.reg_loop[20].reg_bit1a_i_2 
       (.I0(opcode[4]),
        .I1(intr_enable_reg_reg_0),
        .I2(pc_plus4[17]),
        .O(\xilinx_16x1d.reg_loop[20].reg_bit1a_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h88888888BBB888B8)) 
    \xilinx_16x1d.reg_loop[20].reg_bit1a_i_3 
       (.I0(pc_current[20]),
        .I1(intr_enable_reg_reg_0),
        .I2(\xilinx_16x1d.reg_loop[16].reg_bit1a_i_4_n_0 ),
        .I3(mem_source[2]),
        .I4(\xilinx_16x1d.reg_loop[20].reg_bit1a_i_4_n_0 ),
        .I5(mem_source[0]),
        .O(\xilinx_16x1d.reg_loop[20].reg_bit1a_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAA00AA00A2FFA200)) 
    \xilinx_16x1d.reg_loop[20].reg_bit1a_i_4 
       (.I0(\xilinx_16x1d.reg_loop[7].reg_bit1a_i_4_n_0 ),
        .I1(mem_source[2]),
        .I2(mem_source[0]),
        .I3(mem_source[3]),
        .I4(cpu_data_r[20]),
        .I5(mem_source[1]),
        .O(\xilinx_16x1d.reg_loop[20].reg_bit1a_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFCBB3088)) 
    \xilinx_16x1d.reg_loop[21].reg_bit1a_i_1 
       (.I0(\xilinx_16x1d.reg_loop[21].reg_bit1a_i_2_n_0 ),
        .I1(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_11_n_0 ),
        .I2(\xilinx_16x1d.reg_loop[21].reg_bit1a_i_3_n_0 ),
        .I3(c_source),
        .I4(pc_new[21]),
        .O(reg_dest[20]));
  (* SOFT_HLUTNM = "soft_lutpair96" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \xilinx_16x1d.reg_loop[21].reg_bit1a_i_2 
       (.I0(opcode[5]),
        .I1(intr_enable_reg_reg_0),
        .I2(pc_plus4[18]),
        .O(\xilinx_16x1d.reg_loop[21].reg_bit1a_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h88888888BBB888B8)) 
    \xilinx_16x1d.reg_loop[21].reg_bit1a_i_3 
       (.I0(pc_current[21]),
        .I1(intr_enable_reg_reg_0),
        .I2(\xilinx_16x1d.reg_loop[16].reg_bit1a_i_4_n_0 ),
        .I3(mem_source[2]),
        .I4(\xilinx_16x1d.reg_loop[21].reg_bit1a_i_4_n_0 ),
        .I5(mem_source[0]),
        .O(\xilinx_16x1d.reg_loop[21].reg_bit1a_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAA00AA00A2FFA200)) 
    \xilinx_16x1d.reg_loop[21].reg_bit1a_i_4 
       (.I0(\xilinx_16x1d.reg_loop[7].reg_bit1a_i_4_n_0 ),
        .I1(mem_source[2]),
        .I2(mem_source[0]),
        .I3(mem_source[3]),
        .I4(cpu_data_r[21]),
        .I5(mem_source[1]),
        .O(\xilinx_16x1d.reg_loop[21].reg_bit1a_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFCBB3088)) 
    \xilinx_16x1d.reg_loop[22].reg_bit1a_i_1 
       (.I0(\xilinx_16x1d.reg_loop[22].reg_bit1a_i_2_n_0 ),
        .I1(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_11_n_0 ),
        .I2(\xilinx_16x1d.reg_loop[22].reg_bit1a_i_3_n_0 ),
        .I3(c_source),
        .I4(pc_new[22]),
        .O(reg_dest[21]));
  (* SOFT_HLUTNM = "soft_lutpair94" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \xilinx_16x1d.reg_loop[22].reg_bit1a_i_2 
       (.I0(opcode[6]),
        .I1(intr_enable_reg_reg_0),
        .I2(pc_plus4[19]),
        .O(\xilinx_16x1d.reg_loop[22].reg_bit1a_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h88888888BBB888B8)) 
    \xilinx_16x1d.reg_loop[22].reg_bit1a_i_3 
       (.I0(pc_current[22]),
        .I1(intr_enable_reg_reg_0),
        .I2(\xilinx_16x1d.reg_loop[16].reg_bit1a_i_4_n_0 ),
        .I3(mem_source[2]),
        .I4(\xilinx_16x1d.reg_loop[22].reg_bit1a_i_4_n_0 ),
        .I5(mem_source[0]),
        .O(\xilinx_16x1d.reg_loop[22].reg_bit1a_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAA00AA00A2FFA200)) 
    \xilinx_16x1d.reg_loop[22].reg_bit1a_i_4 
       (.I0(\xilinx_16x1d.reg_loop[7].reg_bit1a_i_4_n_0 ),
        .I1(mem_source[2]),
        .I2(mem_source[0]),
        .I3(mem_source[3]),
        .I4(cpu_data_r[22]),
        .I5(mem_source[1]),
        .O(\xilinx_16x1d.reg_loop[22].reg_bit1a_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFCBB3088)) 
    \xilinx_16x1d.reg_loop[23].reg_bit1a_i_1 
       (.I0(\xilinx_16x1d.reg_loop[23].reg_bit1a_i_2_n_0 ),
        .I1(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_11_n_0 ),
        .I2(\xilinx_16x1d.reg_loop[23].reg_bit1a_i_3_n_0 ),
        .I3(c_source),
        .I4(pc_new[23]),
        .O(reg_dest[22]));
  (* SOFT_HLUTNM = "soft_lutpair95" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \xilinx_16x1d.reg_loop[23].reg_bit1a_i_2 
       (.I0(opcode[7]),
        .I1(intr_enable_reg_reg_0),
        .I2(pc_plus4[20]),
        .O(\xilinx_16x1d.reg_loop[23].reg_bit1a_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h88888888BBB888B8)) 
    \xilinx_16x1d.reg_loop[23].reg_bit1a_i_3 
       (.I0(pc_current[23]),
        .I1(intr_enable_reg_reg_0),
        .I2(\xilinx_16x1d.reg_loop[16].reg_bit1a_i_4_n_0 ),
        .I3(mem_source[2]),
        .I4(\xilinx_16x1d.reg_loop[23].reg_bit1a_i_4_n_0 ),
        .I5(mem_source[0]),
        .O(\xilinx_16x1d.reg_loop[23].reg_bit1a_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAA00AA00A2FFA200)) 
    \xilinx_16x1d.reg_loop[23].reg_bit1a_i_4 
       (.I0(\xilinx_16x1d.reg_loop[7].reg_bit1a_i_4_n_0 ),
        .I1(mem_source[2]),
        .I2(mem_source[0]),
        .I3(mem_source[3]),
        .I4(cpu_data_r[23]),
        .I5(mem_source[1]),
        .O(\xilinx_16x1d.reg_loop[23].reg_bit1a_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFCBB3088)) 
    \xilinx_16x1d.reg_loop[24].reg_bit1a_i_1 
       (.I0(\xilinx_16x1d.reg_loop[24].reg_bit1a_i_2_n_0 ),
        .I1(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_11_n_0 ),
        .I2(\xilinx_16x1d.reg_loop[24].reg_bit1a_i_3_n_0 ),
        .I3(c_source),
        .I4(pc_new[24]),
        .O(reg_dest[23]));
  (* SOFT_HLUTNM = "soft_lutpair99" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \xilinx_16x1d.reg_loop[24].reg_bit1a_i_2 
       (.I0(opcode[8]),
        .I1(intr_enable_reg_reg_0),
        .I2(pc_plus4[21]),
        .O(\xilinx_16x1d.reg_loop[24].reg_bit1a_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h88888888BBB888B8)) 
    \xilinx_16x1d.reg_loop[24].reg_bit1a_i_3 
       (.I0(pc_current[24]),
        .I1(intr_enable_reg_reg_0),
        .I2(\xilinx_16x1d.reg_loop[16].reg_bit1a_i_4_n_0 ),
        .I3(mem_source[2]),
        .I4(\xilinx_16x1d.reg_loop[24].reg_bit1a_i_4_n_0 ),
        .I5(mem_source[0]),
        .O(\xilinx_16x1d.reg_loop[24].reg_bit1a_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAA00AA00A2FFA200)) 
    \xilinx_16x1d.reg_loop[24].reg_bit1a_i_4 
       (.I0(\xilinx_16x1d.reg_loop[7].reg_bit1a_i_4_n_0 ),
        .I1(mem_source[2]),
        .I2(mem_source[0]),
        .I3(mem_source[3]),
        .I4(cpu_data_r[24]),
        .I5(mem_source[1]),
        .O(\xilinx_16x1d.reg_loop[24].reg_bit1a_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFCBB3088)) 
    \xilinx_16x1d.reg_loop[25].reg_bit1a_i_1 
       (.I0(\xilinx_16x1d.reg_loop[25].reg_bit1a_i_2_n_0 ),
        .I1(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_11_n_0 ),
        .I2(\xilinx_16x1d.reg_loop[25].reg_bit1a_i_3_n_0 ),
        .I3(c_source),
        .I4(pc_new[25]),
        .O(reg_dest[24]));
  (* SOFT_HLUTNM = "soft_lutpair95" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \xilinx_16x1d.reg_loop[25].reg_bit1a_i_2 
       (.I0(opcode[9]),
        .I1(intr_enable_reg_reg_0),
        .I2(pc_plus4[22]),
        .O(\xilinx_16x1d.reg_loop[25].reg_bit1a_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h88888888BBB888B8)) 
    \xilinx_16x1d.reg_loop[25].reg_bit1a_i_3 
       (.I0(pc_current[25]),
        .I1(intr_enable_reg_reg_0),
        .I2(\xilinx_16x1d.reg_loop[16].reg_bit1a_i_4_n_0 ),
        .I3(mem_source[2]),
        .I4(\xilinx_16x1d.reg_loop[25].reg_bit1a_i_4_n_0 ),
        .I5(mem_source[0]),
        .O(\xilinx_16x1d.reg_loop[25].reg_bit1a_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAA00AA00A2FFA200)) 
    \xilinx_16x1d.reg_loop[25].reg_bit1a_i_4 
       (.I0(\xilinx_16x1d.reg_loop[7].reg_bit1a_i_4_n_0 ),
        .I1(mem_source[2]),
        .I2(mem_source[0]),
        .I3(mem_source[3]),
        .I4(cpu_data_r[25]),
        .I5(mem_source[1]),
        .O(\xilinx_16x1d.reg_loop[25].reg_bit1a_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFCBB3088)) 
    \xilinx_16x1d.reg_loop[26].reg_bit1a_i_1 
       (.I0(\xilinx_16x1d.reg_loop[26].reg_bit1a_i_2_n_0 ),
        .I1(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_11_n_0 ),
        .I2(\xilinx_16x1d.reg_loop[26].reg_bit1a_i_3_n_0 ),
        .I3(c_source),
        .I4(pc_new[26]),
        .O(reg_dest[25]));
  (* SOFT_HLUTNM = "soft_lutpair94" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \xilinx_16x1d.reg_loop[26].reg_bit1a_i_2 
       (.I0(opcode[10]),
        .I1(intr_enable_reg_reg_0),
        .I2(pc_plus4[23]),
        .O(\xilinx_16x1d.reg_loop[26].reg_bit1a_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h88888888BBB888B8)) 
    \xilinx_16x1d.reg_loop[26].reg_bit1a_i_3 
       (.I0(pc_current[26]),
        .I1(intr_enable_reg_reg_0),
        .I2(\xilinx_16x1d.reg_loop[16].reg_bit1a_i_4_n_0 ),
        .I3(mem_source[2]),
        .I4(\xilinx_16x1d.reg_loop[26].reg_bit1a_i_4_n_0 ),
        .I5(mem_source[0]),
        .O(\xilinx_16x1d.reg_loop[26].reg_bit1a_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAA00AA00A2FFA200)) 
    \xilinx_16x1d.reg_loop[26].reg_bit1a_i_4 
       (.I0(\xilinx_16x1d.reg_loop[7].reg_bit1a_i_4_n_0 ),
        .I1(mem_source[2]),
        .I2(mem_source[0]),
        .I3(mem_source[3]),
        .I4(cpu_data_r[26]),
        .I5(mem_source[1]),
        .O(\xilinx_16x1d.reg_loop[26].reg_bit1a_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFCBB3088)) 
    \xilinx_16x1d.reg_loop[27].reg_bit1a_i_1 
       (.I0(\xilinx_16x1d.reg_loop[27].reg_bit1a_i_2_n_0 ),
        .I1(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_11_n_0 ),
        .I2(\xilinx_16x1d.reg_loop[27].reg_bit1a_i_3_n_0 ),
        .I3(c_source),
        .I4(pc_new[27]),
        .O(reg_dest[26]));
  (* SOFT_HLUTNM = "soft_lutpair91" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \xilinx_16x1d.reg_loop[27].reg_bit1a_i_2 
       (.I0(opcode[11]),
        .I1(intr_enable_reg_reg_0),
        .I2(pc_plus4[24]),
        .O(\xilinx_16x1d.reg_loop[27].reg_bit1a_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h88888888BBB888B8)) 
    \xilinx_16x1d.reg_loop[27].reg_bit1a_i_3 
       (.I0(pc_current[27]),
        .I1(intr_enable_reg_reg_0),
        .I2(\xilinx_16x1d.reg_loop[16].reg_bit1a_i_4_n_0 ),
        .I3(mem_source[2]),
        .I4(\xilinx_16x1d.reg_loop[27].reg_bit1a_i_4_n_0 ),
        .I5(mem_source[0]),
        .O(\xilinx_16x1d.reg_loop[27].reg_bit1a_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAA00AA00A2FFA200)) 
    \xilinx_16x1d.reg_loop[27].reg_bit1a_i_4 
       (.I0(\xilinx_16x1d.reg_loop[7].reg_bit1a_i_4_n_0 ),
        .I1(mem_source[2]),
        .I2(mem_source[0]),
        .I3(mem_source[3]),
        .I4(cpu_data_r[27]),
        .I5(mem_source[1]),
        .O(\xilinx_16x1d.reg_loop[27].reg_bit1a_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFCBB3088)) 
    \xilinx_16x1d.reg_loop[28].reg_bit1a_i_1 
       (.I0(\pc_reg_reg[28]_2 ),
        .I1(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_11_n_0 ),
        .I2(\xilinx_16x1d.reg_loop[28].reg_bit1a_i_3_n_0 ),
        .I3(c_source),
        .I4(pc_new[28]),
        .O(reg_dest[27]));
  LUT6 #(
    .INIT(64'h88888888BBB888B8)) 
    \xilinx_16x1d.reg_loop[28].reg_bit1a_i_3 
       (.I0(pc_current[28]),
        .I1(intr_enable_reg_reg_0),
        .I2(\xilinx_16x1d.reg_loop[16].reg_bit1a_i_4_n_0 ),
        .I3(mem_source[2]),
        .I4(\xilinx_16x1d.reg_loop[28].reg_bit1a_i_4_n_0 ),
        .I5(mem_source[0]),
        .O(\xilinx_16x1d.reg_loop[28].reg_bit1a_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAA00AA00A2FFA200)) 
    \xilinx_16x1d.reg_loop[28].reg_bit1a_i_4 
       (.I0(\xilinx_16x1d.reg_loop[7].reg_bit1a_i_4_n_0 ),
        .I1(mem_source[2]),
        .I2(mem_source[0]),
        .I3(mem_source[3]),
        .I4(cpu_data_r[28]),
        .I5(mem_source[1]),
        .O(\xilinx_16x1d.reg_loop[28].reg_bit1a_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFCBB3088)) 
    \xilinx_16x1d.reg_loop[29].reg_bit1a_i_1 
       (.I0(\xilinx_16x1d.reg_loop[29].reg_bit1a_i_2_n_0 ),
        .I1(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_11_n_0 ),
        .I2(\xilinx_16x1d.reg_loop[29].reg_bit1a_i_3_n_0 ),
        .I3(c_source),
        .I4(pc_new[29]),
        .O(reg_dest[28]));
  (* SOFT_HLUTNM = "soft_lutpair93" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \xilinx_16x1d.reg_loop[29].reg_bit1a_i_2 
       (.I0(opcode[13]),
        .I1(intr_enable_reg_reg_0),
        .I2(pc_plus4[25]),
        .O(\xilinx_16x1d.reg_loop[29].reg_bit1a_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h88888888BBB888B8)) 
    \xilinx_16x1d.reg_loop[29].reg_bit1a_i_3 
       (.I0(pc_current[29]),
        .I1(intr_enable_reg_reg_0),
        .I2(\xilinx_16x1d.reg_loop[16].reg_bit1a_i_4_n_0 ),
        .I3(mem_source[2]),
        .I4(\xilinx_16x1d.reg_loop[29].reg_bit1a_i_4_n_0 ),
        .I5(mem_source[0]),
        .O(\xilinx_16x1d.reg_loop[29].reg_bit1a_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAA00AA00A2FFA200)) 
    \xilinx_16x1d.reg_loop[29].reg_bit1a_i_4 
       (.I0(\xilinx_16x1d.reg_loop[7].reg_bit1a_i_4_n_0 ),
        .I1(mem_source[2]),
        .I2(mem_source[0]),
        .I3(mem_source[3]),
        .I4(cpu_data_r[29]),
        .I5(mem_source[1]),
        .O(\xilinx_16x1d.reg_loop[29].reg_bit1a_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hEFEA575745400202)) 
    \xilinx_16x1d.reg_loop[2].reg_bit1a_i_1 
       (.I0(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_11_n_0 ),
        .I1(pc_current[2]),
        .I2(intr_enable_reg_reg_0),
        .I3(\xilinx_16x1d.reg_loop[2].reg_bit1a_i_2_n_0 ),
        .I4(c_source),
        .I5(pc_new[2]),
        .O(reg_dest[1]));
  LUT6 #(
    .INIT(64'h00000000F8C83808)) 
    \xilinx_16x1d.reg_loop[2].reg_bit1a_i_2 
       (.I0(\xilinx_16x1d.reg_loop[2].reg_bit1a_i_3_n_0 ),
        .I1(mem_source[3]),
        .I2(mem_source[2]),
        .I3(\xilinx_16x1d.reg_loop[2].reg_bit1a_i_4_n_0 ),
        .I4(\xilinx_16x1d.reg_loop[2].reg_bit1a_i_5_n_0 ),
        .I5(mem_source[0]),
        .O(\xilinx_16x1d.reg_loop[2].reg_bit1a_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair113" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \xilinx_16x1d.reg_loop[2].reg_bit1a_i_3 
       (.I0(cpu_data_r[2]),
        .I1(\xilinx_16x1d.reg_loop[1].reg_bit1a_i_3_n_0 ),
        .I2(cpu_data_r[18]),
        .O(\xilinx_16x1d.reg_loop[2].reg_bit1a_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair83" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \xilinx_16x1d.reg_loop[2].reg_bit1a_i_4 
       (.I0(cpu_data_r[2]),
        .I1(mem_source[1]),
        .O(\xilinx_16x1d.reg_loop[2].reg_bit1a_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hF0FFAACCF000AACC)) 
    \xilinx_16x1d.reg_loop[2].reg_bit1a_i_5 
       (.I0(cpu_data_r[18]),
        .I1(cpu_data_r[26]),
        .I2(cpu_data_r[2]),
        .I3(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_15_n_0 ),
        .I4(\xilinx_16x1d.reg_loop[1].reg_bit1a_i_3_n_0 ),
        .I5(cpu_data_r[10]),
        .O(\xilinx_16x1d.reg_loop[2].reg_bit1a_i_5_n_0 ));
  LUT5 #(
    .INIT(32'hFCBB3088)) 
    \xilinx_16x1d.reg_loop[30].reg_bit1a_i_1 
       (.I0(\xilinx_16x1d.reg_loop[30].reg_bit1a_i_2_n_0 ),
        .I1(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_11_n_0 ),
        .I2(\xilinx_16x1d.reg_loop[30].reg_bit1a_i_3_n_0 ),
        .I3(c_source),
        .I4(pc_new[30]),
        .O(reg_dest[29]));
  (* SOFT_HLUTNM = "soft_lutpair93" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \xilinx_16x1d.reg_loop[30].reg_bit1a_i_2 
       (.I0(opcode[14]),
        .I1(intr_enable_reg_reg_0),
        .I2(pc_plus4[26]),
        .O(\xilinx_16x1d.reg_loop[30].reg_bit1a_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h88888888BBB888B8)) 
    \xilinx_16x1d.reg_loop[30].reg_bit1a_i_3 
       (.I0(pc_current[30]),
        .I1(intr_enable_reg_reg_0),
        .I2(\xilinx_16x1d.reg_loop[16].reg_bit1a_i_4_n_0 ),
        .I3(mem_source[2]),
        .I4(\xilinx_16x1d.reg_loop[30].reg_bit1a_i_4_n_0 ),
        .I5(mem_source[0]),
        .O(\xilinx_16x1d.reg_loop[30].reg_bit1a_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAA00AA00A2FFA200)) 
    \xilinx_16x1d.reg_loop[30].reg_bit1a_i_4 
       (.I0(\xilinx_16x1d.reg_loop[7].reg_bit1a_i_4_n_0 ),
        .I1(mem_source[2]),
        .I2(mem_source[0]),
        .I3(mem_source[3]),
        .I4(cpu_data_r[30]),
        .I5(mem_source[1]),
        .O(\xilinx_16x1d.reg_loop[30].reg_bit1a_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFCBB3088)) 
    \xilinx_16x1d.reg_loop[31].reg_bit1a_i_1 
       (.I0(\xilinx_16x1d.reg_loop[31].reg_bit1a_i_2_n_0 ),
        .I1(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_11_n_0 ),
        .I2(\xilinx_16x1d.reg_loop[31].reg_bit1a_i_3_n_0 ),
        .I3(c_source),
        .I4(pc_new[31]),
        .O(reg_dest[30]));
  (* SOFT_HLUTNM = "soft_lutpair91" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \xilinx_16x1d.reg_loop[31].reg_bit1a_i_2 
       (.I0(opcode[15]),
        .I1(intr_enable_reg_reg_0),
        .I2(pc_plus4[27]),
        .O(\xilinx_16x1d.reg_loop[31].reg_bit1a_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h88888888BBB888B8)) 
    \xilinx_16x1d.reg_loop[31].reg_bit1a_i_3 
       (.I0(pc_current[31]),
        .I1(intr_enable_reg_reg_0),
        .I2(\xilinx_16x1d.reg_loop[16].reg_bit1a_i_4_n_0 ),
        .I3(mem_source[2]),
        .I4(\xilinx_16x1d.reg_loop[31].reg_bit1a_i_4_n_0 ),
        .I5(mem_source[0]),
        .O(\xilinx_16x1d.reg_loop[31].reg_bit1a_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAA00AA00A2FFA200)) 
    \xilinx_16x1d.reg_loop[31].reg_bit1a_i_4 
       (.I0(\xilinx_16x1d.reg_loop[7].reg_bit1a_i_4_n_0 ),
        .I1(mem_source[2]),
        .I2(mem_source[0]),
        .I3(mem_source[3]),
        .I4(cpu_data_r[31]),
        .I5(mem_source[1]),
        .O(\xilinx_16x1d.reg_loop[31].reg_bit1a_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFF02F2F0F002020)) 
    \xilinx_16x1d.reg_loop[3].reg_bit1a_i_1 
       (.I0(pc_plus4[0]),
        .I1(intr_enable_reg_reg_0),
        .I2(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_11_n_0 ),
        .I3(\xilinx_16x1d.reg_loop[3].reg_bit1a_i_3_n_0 ),
        .I4(c_source),
        .I5(pc_new[3]),
        .O(reg_dest[2]));
  LUT4 #(
    .INIT(16'h88B8)) 
    \xilinx_16x1d.reg_loop[3].reg_bit1a_i_3 
       (.I0(pc_current[3]),
        .I1(intr_enable_reg_reg_0),
        .I2(\xilinx_16x1d.reg_loop[3].reg_bit1a_i_4_n_0 ),
        .I3(mem_source[0]),
        .O(\xilinx_16x1d.reg_loop[3].reg_bit1a_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAAFF0C00AA000C00)) 
    \xilinx_16x1d.reg_loop[3].reg_bit1a_i_4 
       (.I0(\xilinx_16x1d.reg_loop[3].reg_bit1a_i_5_n_0 ),
        .I1(cpu_data_r[3]),
        .I2(mem_source[1]),
        .I3(mem_source[2]),
        .I4(mem_source[3]),
        .I5(\xilinx_16x1d.reg_loop[3].reg_bit1a_i_6_n_0 ),
        .O(\xilinx_16x1d.reg_loop[3].reg_bit1a_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hF0FFAACCF000AACC)) 
    \xilinx_16x1d.reg_loop[3].reg_bit1a_i_5 
       (.I0(cpu_data_r[19]),
        .I1(cpu_data_r[27]),
        .I2(cpu_data_r[3]),
        .I3(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_15_n_0 ),
        .I4(\xilinx_16x1d.reg_loop[1].reg_bit1a_i_3_n_0 ),
        .I5(cpu_data_r[11]),
        .O(\xilinx_16x1d.reg_loop[3].reg_bit1a_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair107" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \xilinx_16x1d.reg_loop[3].reg_bit1a_i_6 
       (.I0(cpu_data_r[3]),
        .I1(\xilinx_16x1d.reg_loop[1].reg_bit1a_i_3_n_0 ),
        .I2(cpu_data_r[19]),
        .O(\xilinx_16x1d.reg_loop[3].reg_bit1a_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hFFF02F2F0F002020)) 
    \xilinx_16x1d.reg_loop[4].reg_bit1a_i_1 
       (.I0(pc_plus4[1]),
        .I1(intr_enable_reg_reg_0),
        .I2(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_11_n_0 ),
        .I3(\xilinx_16x1d.reg_loop[4].reg_bit1a_i_2_n_0 ),
        .I4(c_source),
        .I5(pc_new[4]),
        .O(reg_dest[3]));
  LUT4 #(
    .INIT(16'h88B8)) 
    \xilinx_16x1d.reg_loop[4].reg_bit1a_i_2 
       (.I0(pc_current[4]),
        .I1(intr_enable_reg_reg_0),
        .I2(\xilinx_16x1d.reg_loop[4].reg_bit1a_i_3_n_0 ),
        .I3(mem_source[0]),
        .O(\xilinx_16x1d.reg_loop[4].reg_bit1a_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAAFF0C00AA000C00)) 
    \xilinx_16x1d.reg_loop[4].reg_bit1a_i_3 
       (.I0(\xilinx_16x1d.reg_loop[4].reg_bit1a_i_4_n_0 ),
        .I1(cpu_data_r[4]),
        .I2(mem_source[1]),
        .I3(mem_source[2]),
        .I4(mem_source[3]),
        .I5(\xilinx_16x1d.reg_loop[4].reg_bit1a_i_5_n_0 ),
        .O(\xilinx_16x1d.reg_loop[4].reg_bit1a_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hF0FFAACCF000AACC)) 
    \xilinx_16x1d.reg_loop[4].reg_bit1a_i_4 
       (.I0(cpu_data_r[20]),
        .I1(cpu_data_r[28]),
        .I2(cpu_data_r[4]),
        .I3(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_15_n_0 ),
        .I4(\xilinx_16x1d.reg_loop[1].reg_bit1a_i_3_n_0 ),
        .I5(cpu_data_r[12]),
        .O(\xilinx_16x1d.reg_loop[4].reg_bit1a_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair107" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \xilinx_16x1d.reg_loop[4].reg_bit1a_i_5 
       (.I0(cpu_data_r[4]),
        .I1(\xilinx_16x1d.reg_loop[1].reg_bit1a_i_3_n_0 ),
        .I2(cpu_data_r[20]),
        .O(\xilinx_16x1d.reg_loop[4].reg_bit1a_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFFF02F2F0F002020)) 
    \xilinx_16x1d.reg_loop[5].reg_bit1a_i_1 
       (.I0(pc_plus4[2]),
        .I1(intr_enable_reg_reg_0),
        .I2(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_11_n_0 ),
        .I3(\xilinx_16x1d.reg_loop[5].reg_bit1a_i_2_n_0 ),
        .I4(c_source),
        .I5(pc_new[5]),
        .O(reg_dest[4]));
  LUT4 #(
    .INIT(16'h88B8)) 
    \xilinx_16x1d.reg_loop[5].reg_bit1a_i_2 
       (.I0(pc_current[5]),
        .I1(intr_enable_reg_reg_0),
        .I2(\xilinx_16x1d.reg_loop[5].reg_bit1a_i_3_n_0 ),
        .I3(mem_source[0]),
        .O(\xilinx_16x1d.reg_loop[5].reg_bit1a_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAAFF0C00AA000C00)) 
    \xilinx_16x1d.reg_loop[5].reg_bit1a_i_3 
       (.I0(\xilinx_16x1d.reg_loop[5].reg_bit1a_i_4_n_0 ),
        .I1(cpu_data_r[5]),
        .I2(mem_source[1]),
        .I3(mem_source[2]),
        .I4(mem_source[3]),
        .I5(\xilinx_16x1d.reg_loop[5].reg_bit1a_i_5_n_0 ),
        .O(\xilinx_16x1d.reg_loop[5].reg_bit1a_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hF0FFAACCF000AACC)) 
    \xilinx_16x1d.reg_loop[5].reg_bit1a_i_4 
       (.I0(cpu_data_r[21]),
        .I1(cpu_data_r[29]),
        .I2(cpu_data_r[5]),
        .I3(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_15_n_0 ),
        .I4(\xilinx_16x1d.reg_loop[1].reg_bit1a_i_3_n_0 ),
        .I5(cpu_data_r[13]),
        .O(\xilinx_16x1d.reg_loop[5].reg_bit1a_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair102" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \xilinx_16x1d.reg_loop[5].reg_bit1a_i_5 
       (.I0(cpu_data_r[5]),
        .I1(\xilinx_16x1d.reg_loop[1].reg_bit1a_i_3_n_0 ),
        .I2(cpu_data_r[21]),
        .O(\xilinx_16x1d.reg_loop[5].reg_bit1a_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFFF02F2F0F002020)) 
    \xilinx_16x1d.reg_loop[6].reg_bit1a_i_1 
       (.I0(pc_plus4[3]),
        .I1(intr_enable_reg_reg_0),
        .I2(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_11_n_0 ),
        .I3(\xilinx_16x1d.reg_loop[6].reg_bit1a_i_2_n_0 ),
        .I4(c_source),
        .I5(pc_new[6]),
        .O(reg_dest[5]));
  LUT4 #(
    .INIT(16'h88B8)) 
    \xilinx_16x1d.reg_loop[6].reg_bit1a_i_2 
       (.I0(pc_current[6]),
        .I1(intr_enable_reg_reg_0),
        .I2(\xilinx_16x1d.reg_loop[6].reg_bit1a_i_3_n_0 ),
        .I3(mem_source[0]),
        .O(\xilinx_16x1d.reg_loop[6].reg_bit1a_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAAFF0C00AA000C00)) 
    \xilinx_16x1d.reg_loop[6].reg_bit1a_i_3 
       (.I0(\xilinx_16x1d.reg_loop[6].reg_bit1a_i_4_n_0 ),
        .I1(cpu_data_r[6]),
        .I2(mem_source[1]),
        .I3(mem_source[2]),
        .I4(mem_source[3]),
        .I5(\xilinx_16x1d.reg_loop[6].reg_bit1a_i_5_n_0 ),
        .O(\xilinx_16x1d.reg_loop[6].reg_bit1a_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hF0FFAACCF000AACC)) 
    \xilinx_16x1d.reg_loop[6].reg_bit1a_i_4 
       (.I0(cpu_data_r[22]),
        .I1(cpu_data_r[30]),
        .I2(cpu_data_r[6]),
        .I3(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_15_n_0 ),
        .I4(\xilinx_16x1d.reg_loop[1].reg_bit1a_i_3_n_0 ),
        .I5(cpu_data_r[14]),
        .O(\xilinx_16x1d.reg_loop[6].reg_bit1a_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair101" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \xilinx_16x1d.reg_loop[6].reg_bit1a_i_5 
       (.I0(cpu_data_r[6]),
        .I1(\xilinx_16x1d.reg_loop[1].reg_bit1a_i_3_n_0 ),
        .I2(cpu_data_r[22]),
        .O(\xilinx_16x1d.reg_loop[6].reg_bit1a_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFFF02F2F0F002020)) 
    \xilinx_16x1d.reg_loop[7].reg_bit1a_i_1 
       (.I0(pc_plus4[4]),
        .I1(intr_enable_reg_reg_0),
        .I2(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_11_n_0 ),
        .I3(\xilinx_16x1d.reg_loop[7].reg_bit1a_i_2_n_0 ),
        .I4(c_source),
        .I5(pc_new[7]),
        .O(reg_dest[6]));
  LUT4 #(
    .INIT(16'h88B8)) 
    \xilinx_16x1d.reg_loop[7].reg_bit1a_i_2 
       (.I0(pc_current[7]),
        .I1(intr_enable_reg_reg_0),
        .I2(\xilinx_16x1d.reg_loop[7].reg_bit1a_i_3_n_0 ),
        .I3(mem_source[0]),
        .O(\xilinx_16x1d.reg_loop[7].reg_bit1a_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAAFF0C00AA000C00)) 
    \xilinx_16x1d.reg_loop[7].reg_bit1a_i_3 
       (.I0(\xilinx_16x1d.reg_loop[7].reg_bit1a_i_4_n_0 ),
        .I1(cpu_data_r[7]),
        .I2(mem_source[1]),
        .I3(mem_source[2]),
        .I4(mem_source[3]),
        .I5(\xilinx_16x1d.reg_loop[7].reg_bit1a_i_5_n_0 ),
        .O(\xilinx_16x1d.reg_loop[7].reg_bit1a_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hF0FFAACCF000AACC)) 
    \xilinx_16x1d.reg_loop[7].reg_bit1a_i_4 
       (.I0(cpu_data_r[23]),
        .I1(cpu_data_r[31]),
        .I2(cpu_data_r[7]),
        .I3(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_15_n_0 ),
        .I4(\xilinx_16x1d.reg_loop[1].reg_bit1a_i_3_n_0 ),
        .I5(cpu_data_r[15]),
        .O(\xilinx_16x1d.reg_loop[7].reg_bit1a_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair102" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \xilinx_16x1d.reg_loop[7].reg_bit1a_i_5 
       (.I0(cpu_data_r[7]),
        .I1(\xilinx_16x1d.reg_loop[1].reg_bit1a_i_3_n_0 ),
        .I2(cpu_data_r[23]),
        .O(\xilinx_16x1d.reg_loop[7].reg_bit1a_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFFF02F2F0F002020)) 
    \xilinx_16x1d.reg_loop[8].reg_bit1a_i_1 
       (.I0(pc_plus4[5]),
        .I1(intr_enable_reg_reg_0),
        .I2(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_11_n_0 ),
        .I3(\xilinx_16x1d.reg_loop[8].reg_bit1a_i_2_n_0 ),
        .I4(c_source),
        .I5(pc_new[8]),
        .O(reg_dest[7]));
  LUT6 #(
    .INIT(64'h88888888BBB888B8)) 
    \xilinx_16x1d.reg_loop[8].reg_bit1a_i_2 
       (.I0(pc_current[8]),
        .I1(intr_enable_reg_reg_0),
        .I2(\xilinx_16x1d.reg_loop[8].reg_bit1a_i_3_n_0 ),
        .I3(mem_source[2]),
        .I4(\xilinx_16x1d.reg_loop[8].reg_bit1a_i_4_n_0 ),
        .I5(mem_source[0]),
        .O(\xilinx_16x1d.reg_loop[8].reg_bit1a_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair76" *) 
  LUT4 #(
    .INIT(16'hA808)) 
    \xilinx_16x1d.reg_loop[8].reg_bit1a_i_3 
       (.I0(mem_source[3]),
        .I1(cpu_data_r[24]),
        .I2(\xilinx_16x1d.reg_loop[1].reg_bit1a_i_3_n_0 ),
        .I3(cpu_data_r[8]),
        .O(\xilinx_16x1d.reg_loop[8].reg_bit1a_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAA00AA00A2FFA200)) 
    \xilinx_16x1d.reg_loop[8].reg_bit1a_i_4 
       (.I0(\xilinx_16x1d.reg_loop[7].reg_bit1a_i_4_n_0 ),
        .I1(mem_source[2]),
        .I2(mem_source[0]),
        .I3(mem_source[3]),
        .I4(cpu_data_r[8]),
        .I5(mem_source[1]),
        .O(\xilinx_16x1d.reg_loop[8].reg_bit1a_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFF02F2F0F002020)) 
    \xilinx_16x1d.reg_loop[9].reg_bit1a_i_1 
       (.I0(pc_plus4[6]),
        .I1(intr_enable_reg_reg_0),
        .I2(\xilinx_16x1d.reg_loop[0].reg_bit1a_i_11_n_0 ),
        .I3(\xilinx_16x1d.reg_loop[9].reg_bit1a_i_2_n_0 ),
        .I4(c_source),
        .I5(pc_new[9]),
        .O(reg_dest[8]));
  LUT6 #(
    .INIT(64'h88888888BBB888B8)) 
    \xilinx_16x1d.reg_loop[9].reg_bit1a_i_2 
       (.I0(pc_current[9]),
        .I1(intr_enable_reg_reg_0),
        .I2(\xilinx_16x1d.reg_loop[9].reg_bit1a_i_3_n_0 ),
        .I3(mem_source[2]),
        .I4(\xilinx_16x1d.reg_loop[9].reg_bit1a_i_4_n_0 ),
        .I5(mem_source[0]),
        .O(\xilinx_16x1d.reg_loop[9].reg_bit1a_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hA808)) 
    \xilinx_16x1d.reg_loop[9].reg_bit1a_i_3 
       (.I0(mem_source[3]),
        .I1(cpu_data_r[25]),
        .I2(\xilinx_16x1d.reg_loop[1].reg_bit1a_i_3_n_0 ),
        .I3(cpu_data_r[9]),
        .O(\xilinx_16x1d.reg_loop[9].reg_bit1a_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAA00AA00A2FFA200)) 
    \xilinx_16x1d.reg_loop[9].reg_bit1a_i_4 
       (.I0(\xilinx_16x1d.reg_loop[7].reg_bit1a_i_4_n_0 ),
        .I1(mem_source[2]),
        .I2(mem_source[0]),
        .I3(mem_source[3]),
        .I4(cpu_data_r[9]),
        .I5(mem_source[1]),
        .O(\xilinx_16x1d.reg_loop[9].reg_bit1a_i_4_n_0 ));
endmodule

module design_1_Dispatcher_HardBlare_0_0_mlite_cpu
   (intr_enable_reg_reg,
    intr_enable,
    mode_reg,
    sign_reg_reg,
    sign2_reg_reg,
    negate_reg,
    \lower_reg_reg[31] ,
    \lower_reg_reg[31]_0 ,
    \lower_reg_reg[31]_1 ,
    \lower_reg_reg[31]_2 ,
    eqOp__4,
    \aa_reg_reg[30] ,
    D,
    bram_debug_en,
    bram_sec_policy_config_en,
    E,
    \bram_bbt_annotations_address[2] ,
    bram_sec_policy_config_en_0,
    \fifo_monitor_to_kernel_data_write[31] ,
    \bram_debug_bytes_selection[3] ,
    \PLtoPS_bytes_selection[3] ,
    \PLtoPS_write_data[31] ,
    \bram_firmware_code_bytes_selection[3] ,
    \bram_sec_policy_config_bytes_selection[3] ,
    fifo_ptm_en,
    PLtoPS_generate_interrupt,
    fifo_monitor_to_kernel_en,
    fifo_kernel_to_monitor_en,
    fifo_read_instrumentation_request_read_en,
    PLtoPS_enable,
    sign_reg__6,
    negate_reg_reg,
    \count_reg_reg[5] ,
    \bram_firmware_code_data_write[31] ,
    intr_enable_reg_reg_0,
    sign2_reg,
    clk,
    intr_enable_reg_reg_1,
    mode_reg_reg,
    sign_reg_reg_0,
    sign_reg_reg_1,
    negate_reg_reg_0,
    bram_debug_data_read,
    bram_sec_policy_config_data_read,
    fifo_kernel_to_monitor_empty,
    fifo_kernel_to_monitor_almost_empty,
    fifo_kernel_to_monitor_full,
    fifo_monitor_to_kernel_empty,
    fifo_monitor_to_kernel_almost_empty,
    fifo_monitor_to_kernel_full,
    fifo_instrumentation_empty,
    fifo_instrumentation_almost_empty,
    fifo_instrumentation_full,
    PLtoPS_readed_data,
    fifo_kernel_to_monitor_data_read,
    fifo_read_instrumentation_data_read,
    fifo_ptm_data_read,
    bram_bbt_annotations_data_read,
    bram_firmware_code_data_read,
    fifo_kernel_to_monitor_almost_full,
    fifo_instrumentation_almost_full,
    fifo_monitor_to_kernel_almost_full,
    fifo_ptm_almost_full,
    fifo_ptm_empty,
    fifo_ptm_almost_empty,
    fifo_ptm_full,
    reset);
  output [0:0]intr_enable_reg_reg;
  output intr_enable;
  output mode_reg;
  output sign_reg_reg;
  output sign2_reg_reg;
  output negate_reg;
  output \lower_reg_reg[31] ;
  output \lower_reg_reg[31]_0 ;
  output \lower_reg_reg[31]_1 ;
  output \lower_reg_reg[31]_2 ;
  output eqOp__4;
  output \aa_reg_reg[30] ;
  output [17:0]D;
  output bram_debug_en;
  output bram_sec_policy_config_en;
  output [0:0]E;
  output [0:0]\bram_bbt_annotations_address[2] ;
  output bram_sec_policy_config_en_0;
  output [0:0]\fifo_monitor_to_kernel_data_write[31] ;
  output [0:0]\bram_debug_bytes_selection[3] ;
  output [0:0]\PLtoPS_bytes_selection[3] ;
  output [0:0]\PLtoPS_write_data[31] ;
  output [3:0]\bram_firmware_code_bytes_selection[3] ;
  output [0:0]\bram_sec_policy_config_bytes_selection[3] ;
  output fifo_ptm_en;
  output PLtoPS_generate_interrupt;
  output fifo_monitor_to_kernel_en;
  output fifo_kernel_to_monitor_en;
  output fifo_read_instrumentation_request_read_en;
  output PLtoPS_enable;
  output sign_reg__6;
  output negate_reg_reg;
  output \count_reg_reg[5] ;
  output [31:0]\bram_firmware_code_data_write[31] ;
  output intr_enable_reg_reg_0;
  output sign2_reg;
  input clk;
  input intr_enable_reg_reg_1;
  input mode_reg_reg;
  input sign_reg_reg_0;
  input sign_reg_reg_1;
  input negate_reg_reg_0;
  input [31:0]bram_debug_data_read;
  input [31:0]bram_sec_policy_config_data_read;
  input fifo_kernel_to_monitor_empty;
  input fifo_kernel_to_monitor_almost_empty;
  input fifo_kernel_to_monitor_full;
  input fifo_monitor_to_kernel_empty;
  input fifo_monitor_to_kernel_almost_empty;
  input fifo_monitor_to_kernel_full;
  input fifo_instrumentation_empty;
  input fifo_instrumentation_almost_empty;
  input fifo_instrumentation_full;
  input [31:0]PLtoPS_readed_data;
  input [31:0]fifo_kernel_to_monitor_data_read;
  input [31:0]fifo_read_instrumentation_data_read;
  input [31:0]fifo_ptm_data_read;
  input [31:0]bram_bbt_annotations_data_read;
  input [31:0]bram_firmware_code_data_read;
  input fifo_kernel_to_monitor_almost_full;
  input fifo_instrumentation_almost_full;
  input fifo_monitor_to_kernel_almost_full;
  input fifo_ptm_almost_full;
  input fifo_ptm_empty;
  input fifo_ptm_almost_empty;
  input fifo_ptm_full;
  input reset;

  wire [17:0]D;
  wire DPO;
  wire DPO102_out;
  wire DPO106_out;
  wire DPO10_out;
  wire DPO110_out;
  wire DPO114_out;
  wire DPO118_out;
  wire DPO122_out;
  wire DPO14_out;
  wire DPO18_out;
  wire DPO22_out;
  wire DPO26_out;
  wire DPO2_out;
  wire DPO30_out;
  wire DPO34_out;
  wire DPO38_out;
  wire DPO42_out;
  wire DPO46_out;
  wire DPO50_out;
  wire DPO54_out;
  wire DPO58_out;
  wire DPO62_out;
  wire DPO66_out;
  wire DPO6_out;
  wire DPO70_out;
  wire DPO74_out;
  wire DPO78_out;
  wire DPO82_out;
  wire DPO86_out;
  wire DPO90_out;
  wire DPO94_out;
  wire DPO98_out;
  wire DPRA0;
  wire DPRA1;
  wire DPRA2;
  wire DPRA3;
  wire [0:0]E;
  wire [0:0]\PLtoPS_bytes_selection[3] ;
  wire PLtoPS_enable;
  wire PLtoPS_generate_interrupt;
  wire [31:0]PLtoPS_readed_data;
  wire [0:0]\PLtoPS_write_data[31] ;
  wire WE;
  wire [31:0]aa_reg;
  wire \aa_reg_reg[30] ;
  wire [31:0]bb_reg;
  wire [0:0]\bram_bbt_annotations_address[2] ;
  wire [31:0]bram_bbt_annotations_data_read;
  wire [0:0]\bram_debug_bytes_selection[3] ;
  wire [31:0]bram_debug_data_read;
  wire bram_debug_en;
  wire [3:0]\bram_firmware_code_bytes_selection[3] ;
  wire [31:0]bram_firmware_code_data_read;
  wire [31:0]\bram_firmware_code_data_write[31] ;
  wire [0:0]\bram_sec_policy_config_bytes_selection[3] ;
  wire [31:0]bram_sec_policy_config_data_read;
  wire bram_sec_policy_config_en;
  wire bram_sec_policy_config_en_0;
  wire [0:0]c_source;
  wire clk;
  wire count_reg;
  wire \count_reg_reg[5] ;
  wire [0:0]data0;
  wire [31:0]data_out1B;
  wire eqOp__4;
  wire fifo_instrumentation_almost_empty;
  wire fifo_instrumentation_almost_full;
  wire fifo_instrumentation_empty;
  wire fifo_instrumentation_full;
  wire fifo_kernel_to_monitor_almost_empty;
  wire fifo_kernel_to_monitor_almost_full;
  wire [31:0]fifo_kernel_to_monitor_data_read;
  wire fifo_kernel_to_monitor_empty;
  wire fifo_kernel_to_monitor_en;
  wire fifo_kernel_to_monitor_full;
  wire fifo_monitor_to_kernel_almost_empty;
  wire fifo_monitor_to_kernel_almost_full;
  wire [0:0]\fifo_monitor_to_kernel_data_write[31] ;
  wire fifo_monitor_to_kernel_empty;
  wire fifo_monitor_to_kernel_en;
  wire fifo_monitor_to_kernel_full;
  wire fifo_ptm_almost_empty;
  wire fifo_ptm_almost_full;
  wire [31:0]fifo_ptm_data_read;
  wire fifo_ptm_empty;
  wire fifo_ptm_en;
  wire fifo_ptm_full;
  wire [31:0]fifo_read_instrumentation_data_read;
  wire fifo_read_instrumentation_request_read_en;
  wire intr_enable;
  wire [0:0]intr_enable_reg_reg;
  wire intr_enable_reg_reg_0;
  wire intr_enable_reg_reg_1;
  wire [31:0]lower_reg;
  wire lower_reg0__62;
  wire \lower_reg_reg[31] ;
  wire \lower_reg_reg[31]_0 ;
  wire \lower_reg_reg[31]_1 ;
  wire \lower_reg_reg[31]_2 ;
  wire mode_reg;
  wire mode_reg_reg;
  wire negate_reg;
  wire negate_reg_reg;
  wire negate_reg_reg_0;
  wire neqOp;
  wire neqOp0_in;
  wire [12:12]opcode;
  wire [30:0]p_0_in;
  wire p_1_in;
  wire p_1_in15_in;
  wire p_1_in18_in;
  wire p_1_in21_in;
  wire p_1_in24_in;
  wire p_1_in27_in;
  wire p_1_in30_in;
  wire p_1_in33_in;
  wire p_1_in36_in;
  wire p_1_in39_in;
  wire p_1_in3_in;
  wire p_1_in42_in;
  wire p_1_in45_in;
  wire p_1_in48_in;
  wire p_1_in60_in;
  wire p_1_in63_in;
  wire p_1_in69_in;
  wire p_1_in75_in;
  wire p_1_in78_in;
  wire p_1_in81_in;
  wire p_1_in84_in;
  wire p_1_in87_in;
  wire p_1_in90_in;
  wire p_48_in;
  wire [31:2]pc_current;
  wire [31:3]pc_plus4;
  wire [3:0]plusOp;
  wire [31:1]reg_dest;
  wire [31:0]reg_target;
  wire reset;
  wire \reset_reg[2]_i_1_n_0 ;
  wire [3:0]reset_reg_reg__0;
  wire sign2_reg;
  wire sign2_reg_reg;
  wire sign_reg1_out;
  wire sign_reg__6;
  wire sign_reg_reg;
  wire sign_reg_reg_0;
  wire sign_reg_reg_1;
  wire u1_pc_next_n_0;
  wire u1_pc_next_n_60;
  wire u2_mem_ctrl_n_0;
  wire u2_mem_ctrl_n_10;
  wire u2_mem_ctrl_n_100;
  wire u2_mem_ctrl_n_101;
  wire u2_mem_ctrl_n_102;
  wire u2_mem_ctrl_n_103;
  wire u2_mem_ctrl_n_11;
  wire u2_mem_ctrl_n_12;
  wire u2_mem_ctrl_n_13;
  wire u2_mem_ctrl_n_136;
  wire u2_mem_ctrl_n_137;
  wire u2_mem_ctrl_n_138;
  wire u2_mem_ctrl_n_139;
  wire u2_mem_ctrl_n_140;
  wire u2_mem_ctrl_n_141;
  wire u2_mem_ctrl_n_142;
  wire u2_mem_ctrl_n_143;
  wire u2_mem_ctrl_n_144;
  wire u2_mem_ctrl_n_145;
  wire u2_mem_ctrl_n_146;
  wire u2_mem_ctrl_n_147;
  wire u2_mem_ctrl_n_148;
  wire u2_mem_ctrl_n_149;
  wire u2_mem_ctrl_n_15;
  wire u2_mem_ctrl_n_150;
  wire u2_mem_ctrl_n_151;
  wire u2_mem_ctrl_n_152;
  wire u2_mem_ctrl_n_153;
  wire u2_mem_ctrl_n_154;
  wire u2_mem_ctrl_n_155;
  wire u2_mem_ctrl_n_156;
  wire u2_mem_ctrl_n_157;
  wire u2_mem_ctrl_n_158;
  wire u2_mem_ctrl_n_159;
  wire u2_mem_ctrl_n_16;
  wire u2_mem_ctrl_n_160;
  wire u2_mem_ctrl_n_161;
  wire u2_mem_ctrl_n_162;
  wire u2_mem_ctrl_n_163;
  wire u2_mem_ctrl_n_164;
  wire u2_mem_ctrl_n_165;
  wire u2_mem_ctrl_n_166;
  wire u2_mem_ctrl_n_167;
  wire u2_mem_ctrl_n_168;
  wire u2_mem_ctrl_n_169;
  wire u2_mem_ctrl_n_17;
  wire u2_mem_ctrl_n_170;
  wire u2_mem_ctrl_n_175;
  wire u2_mem_ctrl_n_176;
  wire u2_mem_ctrl_n_177;
  wire u2_mem_ctrl_n_178;
  wire u2_mem_ctrl_n_179;
  wire u2_mem_ctrl_n_18;
  wire u2_mem_ctrl_n_180;
  wire u2_mem_ctrl_n_181;
  wire u2_mem_ctrl_n_182;
  wire u2_mem_ctrl_n_183;
  wire u2_mem_ctrl_n_184;
  wire u2_mem_ctrl_n_185;
  wire u2_mem_ctrl_n_186;
  wire u2_mem_ctrl_n_187;
  wire u2_mem_ctrl_n_188;
  wire u2_mem_ctrl_n_19;
  wire u2_mem_ctrl_n_190;
  wire u2_mem_ctrl_n_191;
  wire u2_mem_ctrl_n_192;
  wire u2_mem_ctrl_n_20;
  wire u2_mem_ctrl_n_23;
  wire u2_mem_ctrl_n_264;
  wire u2_mem_ctrl_n_299;
  wire u2_mem_ctrl_n_300;
  wire u2_mem_ctrl_n_301;
  wire u2_mem_ctrl_n_5;
  wire u2_mem_ctrl_n_6;
  wire u2_mem_ctrl_n_74;
  wire u2_mem_ctrl_n_75;
  wire u2_mem_ctrl_n_76;
  wire u2_mem_ctrl_n_77;
  wire u2_mem_ctrl_n_78;
  wire u2_mem_ctrl_n_79;
  wire u2_mem_ctrl_n_8;
  wire u2_mem_ctrl_n_80;
  wire u2_mem_ctrl_n_81;
  wire u2_mem_ctrl_n_82;
  wire u2_mem_ctrl_n_83;
  wire u2_mem_ctrl_n_84;
  wire u2_mem_ctrl_n_85;
  wire u2_mem_ctrl_n_86;
  wire u2_mem_ctrl_n_87;
  wire u2_mem_ctrl_n_88;
  wire u2_mem_ctrl_n_89;
  wire u2_mem_ctrl_n_9;
  wire u2_mem_ctrl_n_90;
  wire u2_mem_ctrl_n_91;
  wire u2_mem_ctrl_n_92;
  wire u2_mem_ctrl_n_93;
  wire u2_mem_ctrl_n_94;
  wire u2_mem_ctrl_n_95;
  wire u2_mem_ctrl_n_96;
  wire u2_mem_ctrl_n_97;
  wire u2_mem_ctrl_n_98;
  wire u2_mem_ctrl_n_99;
  wire u5_bus_mux_n_0;
  wire u8_mult_n_100;
  wire u8_mult_n_101;
  wire u8_mult_n_102;
  wire u8_mult_n_103;
  wire u8_mult_n_104;
  wire u8_mult_n_105;
  wire u8_mult_n_106;
  wire u8_mult_n_107;
  wire u8_mult_n_108;
  wire u8_mult_n_109;
  wire u8_mult_n_110;
  wire u8_mult_n_111;
  wire u8_mult_n_112;
  wire u8_mult_n_113;
  wire u8_mult_n_114;
  wire u8_mult_n_115;
  wire u8_mult_n_116;
  wire u8_mult_n_117;
  wire u8_mult_n_118;
  wire u8_mult_n_119;
  wire u8_mult_n_120;
  wire u8_mult_n_121;
  wire u8_mult_n_122;
  wire u8_mult_n_123;
  wire u8_mult_n_124;
  wire u8_mult_n_125;
  wire u8_mult_n_126;
  wire u8_mult_n_127;
  wire u8_mult_n_128;
  wire u8_mult_n_129;
  wire u8_mult_n_130;
  wire u8_mult_n_131;
  wire u8_mult_n_132;
  wire u8_mult_n_133;
  wire u8_mult_n_134;
  wire u8_mult_n_135;
  wire u8_mult_n_136;
  wire u8_mult_n_137;
  wire u8_mult_n_138;
  wire u8_mult_n_139;
  wire u8_mult_n_140;
  wire u8_mult_n_141;
  wire u8_mult_n_142;
  wire u8_mult_n_143;
  wire u8_mult_n_144;
  wire u8_mult_n_145;
  wire u8_mult_n_146;
  wire u8_mult_n_147;
  wire u8_mult_n_148;
  wire u8_mult_n_149;
  wire u8_mult_n_150;
  wire u8_mult_n_151;
  wire u8_mult_n_152;
  wire u8_mult_n_153;
  wire u8_mult_n_154;
  wire u8_mult_n_155;
  wire u8_mult_n_156;
  wire u8_mult_n_157;
  wire u8_mult_n_158;
  wire u8_mult_n_159;
  wire u8_mult_n_160;
  wire u8_mult_n_161;
  wire u8_mult_n_162;
  wire u8_mult_n_163;
  wire u8_mult_n_164;
  wire u8_mult_n_165;
  wire u8_mult_n_166;
  wire u8_mult_n_167;
  wire u8_mult_n_168;
  wire u8_mult_n_169;
  wire u8_mult_n_170;
  wire u8_mult_n_171;
  wire u8_mult_n_172;
  wire u8_mult_n_173;
  wire u8_mult_n_174;
  wire u8_mult_n_207;
  wire u8_mult_n_39;
  wire u8_mult_n_4;
  wire u8_mult_n_40;
  wire u8_mult_n_41;
  wire u8_mult_n_42;
  wire u8_mult_n_43;
  wire u8_mult_n_44;
  wire u8_mult_n_45;
  wire u8_mult_n_46;
  wire u8_mult_n_47;
  wire u8_mult_n_48;
  wire u8_mult_n_49;
  wire u8_mult_n_5;
  wire u8_mult_n_50;
  wire u8_mult_n_51;
  wire u8_mult_n_52;
  wire u8_mult_n_53;
  wire u8_mult_n_54;
  wire u8_mult_n_55;
  wire u8_mult_n_6;
  wire u8_mult_n_79;
  wire u8_mult_n_80;
  wire u8_mult_n_81;
  wire u8_mult_n_82;
  wire u8_mult_n_83;
  wire u8_mult_n_84;
  wire u8_mult_n_85;
  wire u8_mult_n_86;
  wire u8_mult_n_87;
  wire u8_mult_n_88;
  wire u8_mult_n_89;
  wire u8_mult_n_90;
  wire u8_mult_n_91;
  wire u8_mult_n_92;
  wire u8_mult_n_93;
  wire u8_mult_n_94;
  wire u8_mult_n_95;
  wire u8_mult_n_98;
  wire u8_mult_n_99;
  wire [31:0]upper_reg;

  (* SOFT_HLUTNM = "soft_lutpair188" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \reset_reg[0]_i_1 
       (.I0(reset_reg_reg__0[0]),
        .O(plusOp[0]));
  (* SOFT_HLUTNM = "soft_lutpair188" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \reset_reg[1]_i_1 
       (.I0(reset_reg_reg__0[0]),
        .I1(reset_reg_reg__0[1]),
        .O(plusOp[1]));
  (* SOFT_HLUTNM = "soft_lutpair187" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \reset_reg[2]_i_1 
       (.I0(reset_reg_reg__0[1]),
        .I1(reset_reg_reg__0[0]),
        .I2(reset_reg_reg__0[2]),
        .O(\reset_reg[2]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h7FFF)) 
    \reset_reg[3]_i_1 
       (.I0(reset_reg_reg__0[1]),
        .I1(reset_reg_reg__0[0]),
        .I2(reset_reg_reg__0[2]),
        .I3(reset_reg_reg__0[3]),
        .O(neqOp));
  (* SOFT_HLUTNM = "soft_lutpair187" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \reset_reg[3]_i_2 
       (.I0(reset_reg_reg__0[0]),
        .I1(reset_reg_reg__0[1]),
        .I2(reset_reg_reg__0[2]),
        .I3(reset_reg_reg__0[3]),
        .O(plusOp[3]));
  FDCE \reset_reg_reg[0] 
       (.C(clk),
        .CE(neqOp),
        .CLR(reset),
        .D(plusOp[0]),
        .Q(reset_reg_reg__0[0]));
  FDCE \reset_reg_reg[1] 
       (.C(clk),
        .CE(neqOp),
        .CLR(reset),
        .D(plusOp[1]),
        .Q(reset_reg_reg__0[1]));
  FDCE \reset_reg_reg[2] 
       (.C(clk),
        .CE(neqOp),
        .CLR(reset),
        .D(\reset_reg[2]_i_1_n_0 ),
        .Q(reset_reg_reg__0[2]));
  FDCE \reset_reg_reg[3] 
       (.C(clk),
        .CE(neqOp),
        .CLR(reset),
        .D(plusOp[3]),
        .Q(reset_reg_reg__0[3]));
  design_1_Dispatcher_HardBlare_0_0_pc_next u1_pc_next
       (.Q(opcode),
        .\address_reg_reg[31] (u1_pc_next_n_60),
        .\bb_reg_reg[28] (u1_pc_next_n_0),
        .c_source(c_source),
        .clk(clk),
        .mem_state_reg_reg(u2_mem_ctrl_n_170),
        .\opcode_reg_reg[10] (u2_mem_ctrl_n_94),
        .\opcode_reg_reg[11] (u2_mem_ctrl_n_93),
        .\opcode_reg_reg[12] (u2_mem_ctrl_n_92),
        .\opcode_reg_reg[13] (u2_mem_ctrl_n_91),
        .\opcode_reg_reg[14] (u2_mem_ctrl_n_90),
        .\opcode_reg_reg[15] (u2_mem_ctrl_n_89),
        .\opcode_reg_reg[16] (u2_mem_ctrl_n_88),
        .\opcode_reg_reg[17] (u2_mem_ctrl_n_87),
        .\opcode_reg_reg[18] (u2_mem_ctrl_n_86),
        .\opcode_reg_reg[19] (u2_mem_ctrl_n_85),
        .\opcode_reg_reg[20] (u2_mem_ctrl_n_84),
        .\opcode_reg_reg[21] (u2_mem_ctrl_n_83),
        .\opcode_reg_reg[22] (u2_mem_ctrl_n_82),
        .\opcode_reg_reg[23] (u2_mem_ctrl_n_81),
        .\opcode_reg_reg[24] (u2_mem_ctrl_n_80),
        .\opcode_reg_reg[25] (u2_mem_ctrl_n_79),
        .\opcode_reg_reg[27] (u2_mem_ctrl_n_75),
        .\opcode_reg_reg[2] (u2_mem_ctrl_n_102),
        .\opcode_reg_reg[3] (u2_mem_ctrl_n_101),
        .\opcode_reg_reg[4] (u2_mem_ctrl_n_100),
        .\opcode_reg_reg[5] (u2_mem_ctrl_n_99),
        .\opcode_reg_reg[6] (u2_mem_ctrl_n_98),
        .\opcode_reg_reg[7] (u2_mem_ctrl_n_97),
        .\opcode_reg_reg[8] (u2_mem_ctrl_n_96),
        .\opcode_reg_reg[9] (u2_mem_ctrl_n_95),
        .p_48_in(p_48_in),
        .pc_current(pc_current),
        .pc_plus4({pc_plus4[31:29],pc_plus4[27:3]}),
        .\pc_reg_reg[28]_0 (u2_mem_ctrl_n_20),
        .\pc_reg_reg[29]_0 (u2_mem_ctrl_n_78),
        .\pc_reg_reg[2]_0 (u2_mem_ctrl_n_74),
        .\pc_reg_reg[2]_1 (u2_mem_ctrl_n_103),
        .\pc_reg_reg[30]_0 (u2_mem_ctrl_n_77),
        .\pc_reg_reg[31]_0 (u2_mem_ctrl_n_76),
        .\reset_reg_reg[3] (u2_mem_ctrl_n_0));
  design_1_Dispatcher_HardBlare_0_0_mem_ctrl u2_mem_ctrl
       (.CO(u5_bus_mux_n_0),
        .D({u2_mem_ctrl_n_11,u2_mem_ctrl_n_12,u2_mem_ctrl_n_13}),
        .DPO(DPO),
        .DPO102_out(DPO102_out),
        .DPO106_out(DPO106_out),
        .DPO10_out(DPO10_out),
        .DPO110_out(DPO110_out),
        .DPO114_out(DPO114_out),
        .DPO118_out(DPO118_out),
        .DPO122_out(DPO122_out),
        .DPO14_out(DPO14_out),
        .DPO18_out(DPO18_out),
        .DPO22_out(DPO22_out),
        .DPO26_out(DPO26_out),
        .DPO2_out(DPO2_out),
        .DPO30_out(DPO30_out),
        .DPO34_out(DPO34_out),
        .DPO38_out(DPO38_out),
        .DPO42_out(DPO42_out),
        .DPO46_out(DPO46_out),
        .DPO50_out(DPO50_out),
        .DPO54_out(DPO54_out),
        .DPO58_out(DPO58_out),
        .DPO62_out(DPO62_out),
        .DPO66_out(DPO66_out),
        .DPO6_out(DPO6_out),
        .DPO70_out(DPO70_out),
        .DPO74_out(DPO74_out),
        .DPO78_out(DPO78_out),
        .DPO82_out(DPO82_out),
        .DPO86_out(DPO86_out),
        .DPO90_out(DPO90_out),
        .DPO94_out(DPO94_out),
        .DPO98_out(DPO98_out),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .E(u2_mem_ctrl_n_169),
        .\PLtoPS_bytes_selection[3] (\PLtoPS_bytes_selection[3] ),
        .PLtoPS_enable(PLtoPS_enable),
        .PLtoPS_generate_interrupt(PLtoPS_generate_interrupt),
        .PLtoPS_readed_data(PLtoPS_readed_data),
        .\PLtoPS_write_data[31] (\PLtoPS_write_data[31] ),
        .Q({u8_mult_n_4,u8_mult_n_5}),
        .S({u2_mem_ctrl_n_175,u2_mem_ctrl_n_176,u2_mem_ctrl_n_177,u2_mem_ctrl_n_178}),
        .WE(WE),
        .\aa_reg_reg[10] (u8_mult_n_166),
        .\aa_reg_reg[12] (u8_mult_n_164),
        .\aa_reg_reg[14] (u8_mult_n_162),
        .\aa_reg_reg[16] (u8_mult_n_160),
        .\aa_reg_reg[18] (u8_mult_n_158),
        .\aa_reg_reg[1] (u8_mult_n_144),
        .\aa_reg_reg[20] (u8_mult_n_156),
        .\aa_reg_reg[22] (u8_mult_n_154),
        .\aa_reg_reg[24] (u8_mult_n_152),
        .\aa_reg_reg[26] (u8_mult_n_150),
        .\aa_reg_reg[28] (u8_mult_n_148),
        .\aa_reg_reg[2] (u8_mult_n_174),
        .\aa_reg_reg[30] (\aa_reg_reg[30] ),
        .\aa_reg_reg[30]_0 (u8_mult_n_146),
        .\aa_reg_reg[31] (aa_reg),
        .\aa_reg_reg[31]_0 (u2_mem_ctrl_n_264),
        .\aa_reg_reg[31]_1 ({u8_mult_n_98,u8_mult_n_99,u8_mult_n_100,u8_mult_n_101,u8_mult_n_102,u8_mult_n_103,u8_mult_n_104,u8_mult_n_105,u8_mult_n_106,u8_mult_n_107,u8_mult_n_108,u8_mult_n_109,u8_mult_n_110,u8_mult_n_111,u8_mult_n_112,u8_mult_n_113,u8_mult_n_114,u8_mult_n_115,u8_mult_n_116,u8_mult_n_117,u8_mult_n_118,u8_mult_n_119,u8_mult_n_120,u8_mult_n_121,u8_mult_n_122,u8_mult_n_123,u8_mult_n_124,u8_mult_n_125,u8_mult_n_126,u8_mult_n_127,u8_mult_n_128}),
        .\aa_reg_reg[4] (u8_mult_n_172),
        .\aa_reg_reg[6] (u8_mult_n_170),
        .\aa_reg_reg[8] (u8_mult_n_168),
        .\address_reg_reg[24]_0 (u2_mem_ctrl_n_0),
        .\bb_reg_reg[0] (u8_mult_n_149),
        .\bb_reg_reg[0]_0 (u8_mult_n_151),
        .\bb_reg_reg[0]_1 (u8_mult_n_153),
        .\bb_reg_reg[0]_10 (u8_mult_n_167),
        .\bb_reg_reg[0]_11 (u8_mult_n_169),
        .\bb_reg_reg[0]_12 (u8_mult_n_171),
        .\bb_reg_reg[0]_13 (u8_mult_n_173),
        .\bb_reg_reg[0]_2 (u8_mult_n_145),
        .\bb_reg_reg[0]_3 (u8_mult_n_147),
        .\bb_reg_reg[0]_4 (u8_mult_n_155),
        .\bb_reg_reg[0]_5 (u8_mult_n_157),
        .\bb_reg_reg[0]_6 (u8_mult_n_159),
        .\bb_reg_reg[0]_7 (u8_mult_n_161),
        .\bb_reg_reg[0]_8 (u8_mult_n_163),
        .\bb_reg_reg[0]_9 (u8_mult_n_165),
        .\bb_reg_reg[1] (u2_mem_ctrl_n_15),
        .\bb_reg_reg[1]_0 (u2_mem_ctrl_n_16),
        .\bb_reg_reg[1]_1 (u2_mem_ctrl_n_17),
        .\bb_reg_reg[1]_2 (u2_mem_ctrl_n_18),
        .\bb_reg_reg[1]_3 (u2_mem_ctrl_n_19),
        .\bb_reg_reg[1]_4 (u2_mem_ctrl_n_299),
        .\bb_reg_reg[31] (u2_mem_ctrl_n_6),
        .\bb_reg_reg[31]_0 (u2_mem_ctrl_n_8),
        .\bb_reg_reg[31]_1 (u2_mem_ctrl_n_9),
        .\bb_reg_reg[31]_2 (u2_mem_ctrl_n_10),
        .\bb_reg_reg[31]_3 (u2_mem_ctrl_n_23),
        .\bb_reg_reg[31]_4 (bb_reg),
        .\bb_reg_reg[31]_5 (p_0_in),
        .\bram_bbt_annotations_address[2] (\bram_bbt_annotations_address[2] ),
        .bram_bbt_annotations_data_read(bram_bbt_annotations_data_read),
        .\bram_debug_bytes_selection[3] (\bram_debug_bytes_selection[3] ),
        .bram_debug_data_read(bram_debug_data_read),
        .bram_debug_en(bram_debug_en),
        .\bram_firmware_code_address[2] (E),
        .\bram_firmware_code_bytes_selection[3] (\bram_firmware_code_bytes_selection[3] ),
        .bram_firmware_code_data_read(bram_firmware_code_data_read),
        .\bram_firmware_code_data_write[31] (\bram_firmware_code_data_write[31] ),
        .\bram_sec_policy_config_address[19] (D),
        .\bram_sec_policy_config_bytes_selection[3] (\bram_sec_policy_config_bytes_selection[3] ),
        .bram_sec_policy_config_data_read(bram_sec_policy_config_data_read),
        .bram_sec_policy_config_en(bram_sec_policy_config_en),
        .bram_sec_policy_config_en_0(bram_sec_policy_config_en_0),
        .clk(clk),
        .\count_reg_reg[4] (u2_mem_ctrl_n_300),
        .\count_reg_reg[4]_0 (u8_mult_n_207),
        .\count_reg_reg[5] (\count_reg_reg[5] ),
        .\count_reg_reg[5]_0 (count_reg),
        .data0(data0),
        .data_out1B(data_out1B),
        .fifo_instrumentation_almost_empty(fifo_instrumentation_almost_empty),
        .fifo_instrumentation_almost_full(fifo_instrumentation_almost_full),
        .fifo_instrumentation_empty(fifo_instrumentation_empty),
        .fifo_instrumentation_full(fifo_instrumentation_full),
        .fifo_kernel_to_monitor_almost_empty(fifo_kernel_to_monitor_almost_empty),
        .fifo_kernel_to_monitor_almost_full(fifo_kernel_to_monitor_almost_full),
        .fifo_kernel_to_monitor_data_read(fifo_kernel_to_monitor_data_read),
        .fifo_kernel_to_monitor_empty(fifo_kernel_to_monitor_empty),
        .fifo_kernel_to_monitor_en(fifo_kernel_to_monitor_en),
        .fifo_kernel_to_monitor_full(fifo_kernel_to_monitor_full),
        .fifo_monitor_to_kernel_almost_empty(fifo_monitor_to_kernel_almost_empty),
        .fifo_monitor_to_kernel_almost_full(fifo_monitor_to_kernel_almost_full),
        .\fifo_monitor_to_kernel_data_write[31] (\fifo_monitor_to_kernel_data_write[31] ),
        .fifo_monitor_to_kernel_empty(fifo_monitor_to_kernel_empty),
        .fifo_monitor_to_kernel_en(fifo_monitor_to_kernel_en),
        .fifo_monitor_to_kernel_full(fifo_monitor_to_kernel_full),
        .fifo_ptm_almost_empty(fifo_ptm_almost_empty),
        .fifo_ptm_almost_full(fifo_ptm_almost_full),
        .fifo_ptm_data_read(fifo_ptm_data_read),
        .fifo_ptm_empty(fifo_ptm_empty),
        .fifo_ptm_en(fifo_ptm_en),
        .fifo_ptm_full(fifo_ptm_full),
        .fifo_read_instrumentation_data_read(fifo_read_instrumentation_data_read),
        .fifo_read_instrumentation_request_read_en(fifo_read_instrumentation_request_read_en),
        .intr_enable(intr_enable),
        .intr_enable_reg_reg(eqOp__4),
        .intr_enable_reg_reg_0(c_source),
        .intr_enable_reg_reg_1(intr_enable_reg_reg),
        .intr_enable_reg_reg_2(intr_enable_reg_reg_0),
        .lower_reg0__62(lower_reg0__62),
        .\lower_reg_reg[0] (u2_mem_ctrl_n_168),
        .\lower_reg_reg[11] (u8_mult_n_50),
        .\lower_reg_reg[11]_0 (u8_mult_n_51),
        .\lower_reg_reg[12] (u8_mult_n_49),
        .\lower_reg_reg[13] (u8_mult_n_48),
        .\lower_reg_reg[16] (u8_mult_n_46),
        .\lower_reg_reg[21] (u8_mult_n_45),
        .\lower_reg_reg[22] (u8_mult_n_44),
        .\lower_reg_reg[24] (u8_mult_n_39),
        .\lower_reg_reg[26] (u8_mult_n_42),
        .\lower_reg_reg[26]_0 (u8_mult_n_43),
        .\lower_reg_reg[27] (u8_mult_n_41),
        .\lower_reg_reg[28] (u8_mult_n_40),
        .\lower_reg_reg[29] (u8_mult_n_6),
        .\lower_reg_reg[2] (u8_mult_n_142),
        .\lower_reg_reg[30] (u8_mult_n_143),
        .\lower_reg_reg[31] (\lower_reg_reg[31] ),
        .\lower_reg_reg[31]_0 (\lower_reg_reg[31]_0 ),
        .\lower_reg_reg[31]_1 (\lower_reg_reg[31]_1 ),
        .\lower_reg_reg[31]_2 (\lower_reg_reg[31]_2 ),
        .\lower_reg_reg[31]_3 ({u2_mem_ctrl_n_136,u2_mem_ctrl_n_137,u2_mem_ctrl_n_138,u2_mem_ctrl_n_139,u2_mem_ctrl_n_140,u2_mem_ctrl_n_141,u2_mem_ctrl_n_142,u2_mem_ctrl_n_143,u2_mem_ctrl_n_144,u2_mem_ctrl_n_145,u2_mem_ctrl_n_146,u2_mem_ctrl_n_147,u2_mem_ctrl_n_148,u2_mem_ctrl_n_149,u2_mem_ctrl_n_150,u2_mem_ctrl_n_151,u2_mem_ctrl_n_152,u2_mem_ctrl_n_153,u2_mem_ctrl_n_154,u2_mem_ctrl_n_155,u2_mem_ctrl_n_156,u2_mem_ctrl_n_157,u2_mem_ctrl_n_158,u2_mem_ctrl_n_159,u2_mem_ctrl_n_160,u2_mem_ctrl_n_161,u2_mem_ctrl_n_162,u2_mem_ctrl_n_163,u2_mem_ctrl_n_164,u2_mem_ctrl_n_165,u2_mem_ctrl_n_166,u2_mem_ctrl_n_167}),
        .\lower_reg_reg[31]_4 (u2_mem_ctrl_n_301),
        .\lower_reg_reg[31]_5 (lower_reg),
        .\lower_reg_reg[31]_6 (u8_mult_n_129),
        .\lower_reg_reg[6] (u8_mult_n_54),
        .\lower_reg_reg[7] (u8_mult_n_53),
        .\lower_reg_reg[7]_0 (u8_mult_n_137),
        .\lower_reg_reg[8] (u8_mult_n_52),
        .\lower_reg_reg[9] (u8_mult_n_47),
        .mode_reg_reg(mode_reg),
        .mode_reg_reg_0(u8_mult_n_95),
        .negate_reg_reg(negate_reg_reg),
        .negate_reg_reg_0(negate_reg),
        .neqOp0_in(neqOp0_in),
        .p_48_in(p_48_in),
        .pc_current(pc_current),
        .pc_plus4({pc_plus4[31:29],pc_plus4[27:3]}),
        .\pc_reg_reg[10] (u2_mem_ctrl_n_96),
        .\pc_reg_reg[11] (u2_mem_ctrl_n_95),
        .\pc_reg_reg[12] (u2_mem_ctrl_n_94),
        .\pc_reg_reg[13] (u2_mem_ctrl_n_93),
        .\pc_reg_reg[14] (opcode),
        .\pc_reg_reg[14]_0 (u2_mem_ctrl_n_92),
        .\pc_reg_reg[15] (u2_mem_ctrl_n_91),
        .\pc_reg_reg[16] (u2_mem_ctrl_n_90),
        .\pc_reg_reg[17] (u2_mem_ctrl_n_89),
        .\pc_reg_reg[18] (u2_mem_ctrl_n_88),
        .\pc_reg_reg[19] (u2_mem_ctrl_n_87),
        .\pc_reg_reg[20] (u2_mem_ctrl_n_86),
        .\pc_reg_reg[21] (u2_mem_ctrl_n_85),
        .\pc_reg_reg[22] (u2_mem_ctrl_n_84),
        .\pc_reg_reg[23] (u2_mem_ctrl_n_83),
        .\pc_reg_reg[24] (u2_mem_ctrl_n_82),
        .\pc_reg_reg[25] (u2_mem_ctrl_n_5),
        .\pc_reg_reg[25]_0 (u2_mem_ctrl_n_81),
        .\pc_reg_reg[25]_1 (u2_mem_ctrl_n_190),
        .\pc_reg_reg[26] (u2_mem_ctrl_n_80),
        .\pc_reg_reg[27] (u2_mem_ctrl_n_79),
        .\pc_reg_reg[28] (u2_mem_ctrl_n_20),
        .\pc_reg_reg[28]_0 ({u2_mem_ctrl_n_179,u2_mem_ctrl_n_180,u2_mem_ctrl_n_181,u2_mem_ctrl_n_182}),
        .\pc_reg_reg[28]_1 ({u2_mem_ctrl_n_183,u2_mem_ctrl_n_184,u2_mem_ctrl_n_185}),
        .\pc_reg_reg[28]_2 (u1_pc_next_n_0),
        .\pc_reg_reg[29] (u2_mem_ctrl_n_78),
        .\pc_reg_reg[29]_0 (u1_pc_next_n_60),
        .\pc_reg_reg[2] (u2_mem_ctrl_n_103),
        .\pc_reg_reg[2]_0 (u2_mem_ctrl_n_192),
        .\pc_reg_reg[30] (u2_mem_ctrl_n_77),
        .\pc_reg_reg[30]_0 (u2_mem_ctrl_n_186),
        .\pc_reg_reg[30]_1 (u2_mem_ctrl_n_188),
        .\pc_reg_reg[31] (u2_mem_ctrl_n_76),
        .\pc_reg_reg[31]_0 (u2_mem_ctrl_n_170),
        .\pc_reg_reg[31]_1 (u2_mem_ctrl_n_187),
        .\pc_reg_reg[3] (u2_mem_ctrl_n_74),
        .\pc_reg_reg[3]_0 (u2_mem_ctrl_n_75),
        .\pc_reg_reg[4] (u2_mem_ctrl_n_102),
        .\pc_reg_reg[5] (u2_mem_ctrl_n_101),
        .\pc_reg_reg[6] (u2_mem_ctrl_n_100),
        .\pc_reg_reg[7] (u2_mem_ctrl_n_99),
        .\pc_reg_reg[7]_0 (u2_mem_ctrl_n_191),
        .\pc_reg_reg[8] (u2_mem_ctrl_n_98),
        .\pc_reg_reg[9] (u2_mem_ctrl_n_97),
        .reg_dest(reg_dest),
        .reg_target(reg_target),
        .reset(reset),
        .\reset_reg_reg[3] (reset_reg_reg__0),
        .sign2_reg(sign2_reg),
        .sign_reg1_out(sign_reg1_out),
        .sign_reg__6(sign_reg__6),
        .sign_reg_reg(u8_mult_n_94),
        .\upper_reg_reg[11] (u8_mult_n_89),
        .\upper_reg_reg[12] (u8_mult_n_88),
        .\upper_reg_reg[13] (u8_mult_n_87),
        .\upper_reg_reg[14] (u8_mult_n_85),
        .\upper_reg_reg[16] (u8_mult_n_136),
        .\upper_reg_reg[18] (u8_mult_n_135),
        .\upper_reg_reg[19] (u8_mult_n_134),
        .\upper_reg_reg[19]_0 (u8_mult_n_83),
        .\upper_reg_reg[20] (u8_mult_n_133),
        .\upper_reg_reg[21] (u8_mult_n_132),
        .\upper_reg_reg[22] (u8_mult_n_84),
        .\upper_reg_reg[23] (u8_mult_n_131),
        .\upper_reg_reg[24] (u8_mult_n_80),
        .\upper_reg_reg[25] (u8_mult_n_130),
        .\upper_reg_reg[26] (u8_mult_n_82),
        .\upper_reg_reg[27] (u8_mult_n_81),
        .\upper_reg_reg[28] (u8_mult_n_55),
        .\upper_reg_reg[31] (upper_reg),
        .\upper_reg_reg[31]_0 ({p_1_in90_in,p_1_in87_in,p_1_in84_in,p_1_in81_in,p_1_in78_in,p_1_in75_in,p_1_in69_in,p_1_in63_in,p_1_in60_in,p_1_in48_in,p_1_in45_in,p_1_in42_in,p_1_in39_in,p_1_in36_in,p_1_in33_in,p_1_in30_in,p_1_in27_in,p_1_in24_in,p_1_in21_in,p_1_in18_in,p_1_in15_in,p_1_in3_in,p_1_in,u8_mult_n_79}),
        .\upper_reg_reg[3] (u8_mult_n_141),
        .\upper_reg_reg[4] (u8_mult_n_140),
        .\upper_reg_reg[4]_0 (u8_mult_n_90),
        .\upper_reg_reg[5] (u8_mult_n_139),
        .\upper_reg_reg[6] (u8_mult_n_138),
        .\upper_reg_reg[6]_0 (u8_mult_n_93),
        .\upper_reg_reg[7] (u8_mult_n_92),
        .\upper_reg_reg[8] (u8_mult_n_91),
        .\upper_reg_reg[9] (u8_mult_n_86));
  design_1_Dispatcher_HardBlare_0_0_reg_bank u4_reg_bank
       (.DPO(DPO),
        .DPO102_out(DPO102_out),
        .DPO106_out(DPO106_out),
        .DPO10_out(DPO10_out),
        .DPO110_out(DPO110_out),
        .DPO114_out(DPO114_out),
        .DPO118_out(DPO118_out),
        .DPO122_out(DPO122_out),
        .DPO14_out(DPO14_out),
        .DPO18_out(DPO18_out),
        .DPO22_out(DPO22_out),
        .DPO26_out(DPO26_out),
        .DPO2_out(DPO2_out),
        .DPO30_out(DPO30_out),
        .DPO34_out(DPO34_out),
        .DPO38_out(DPO38_out),
        .DPO42_out(DPO42_out),
        .DPO46_out(DPO46_out),
        .DPO50_out(DPO50_out),
        .DPO54_out(DPO54_out),
        .DPO58_out(DPO58_out),
        .DPO62_out(DPO62_out),
        .DPO66_out(DPO66_out),
        .DPO6_out(DPO6_out),
        .DPO70_out(DPO70_out),
        .DPO74_out(DPO74_out),
        .DPO78_out(DPO78_out),
        .DPO82_out(DPO82_out),
        .DPO86_out(DPO86_out),
        .DPO90_out(DPO90_out),
        .DPO94_out(DPO94_out),
        .DPO98_out(DPO98_out),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .WE(WE),
        .clk(clk),
        .data0(data0),
        .data_out1B(data_out1B),
        .intr_enable(intr_enable),
        .intr_enable_reg_reg_0(intr_enable_reg_reg_1),
        .\opcode_reg_reg[16] (u2_mem_ctrl_n_19),
        .\opcode_reg_reg[17] (u2_mem_ctrl_n_18),
        .\opcode_reg_reg[18] (u2_mem_ctrl_n_17),
        .\opcode_reg_reg[19] (u2_mem_ctrl_n_16),
        .\opcode_reg_reg[20] (u2_mem_ctrl_n_15),
        .\opcode_reg_reg[20]_0 (u2_mem_ctrl_n_299),
        .\opcode_reg_reg[29] (intr_enable_reg_reg),
        .\opcode_reg_reg[2] (u2_mem_ctrl_n_9),
        .\opcode_reg_reg[2]_0 (u2_mem_ctrl_n_8),
        .\opcode_reg_reg[2]_1 (u2_mem_ctrl_n_6),
        .\opcode_reg_reg[31] (u2_mem_ctrl_n_10),
        .\opcode_reg_reg[31]_0 (u2_mem_ctrl_n_23),
        .reg_dest(reg_dest),
        .reg_target(reg_target),
        .\reset_reg_reg[3] (u2_mem_ctrl_n_0));
  design_1_Dispatcher_HardBlare_0_0_bus_mux u5_bus_mux
       (.CO(u5_bus_mux_n_0),
        .S({u2_mem_ctrl_n_175,u2_mem_ctrl_n_176,u2_mem_ctrl_n_177,u2_mem_ctrl_n_178}),
        .\opcode_reg_reg[25] ({u2_mem_ctrl_n_179,u2_mem_ctrl_n_180,u2_mem_ctrl_n_181,u2_mem_ctrl_n_182}),
        .\opcode_reg_reg[25]_0 ({u2_mem_ctrl_n_183,u2_mem_ctrl_n_184,u2_mem_ctrl_n_185}));
  design_1_Dispatcher_HardBlare_0_0_mult u8_mult
       (.D({u2_mem_ctrl_n_11,u2_mem_ctrl_n_12,u2_mem_ctrl_n_13}),
        .E(count_reg),
        .Q({u8_mult_n_4,u8_mult_n_5}),
        .\aa_reg_reg[30]_0 ({u8_mult_n_98,u8_mult_n_99,u8_mult_n_100,u8_mult_n_101,u8_mult_n_102,u8_mult_n_103,u8_mult_n_104,u8_mult_n_105,u8_mult_n_106,u8_mult_n_107,u8_mult_n_108,u8_mult_n_109,u8_mult_n_110,u8_mult_n_111,u8_mult_n_112,u8_mult_n_113,u8_mult_n_114,u8_mult_n_115,u8_mult_n_116,u8_mult_n_117,u8_mult_n_118,u8_mult_n_119,u8_mult_n_120,u8_mult_n_121,u8_mult_n_122,u8_mult_n_123,u8_mult_n_124,u8_mult_n_125,u8_mult_n_126,u8_mult_n_127,u8_mult_n_128}),
        .\aa_reg_reg[31]_0 (mode_reg),
        .\address_reg_reg[31] (u8_mult_n_6),
        .\bb_reg_reg[1]_0 (aa_reg),
        .\bb_reg_reg[30]_0 (p_0_in),
        .clk(clk),
        .\count_reg_reg[5]_0 (u8_mult_n_207),
        .lower_reg0__62(lower_reg0__62),
        .\lower_reg_reg[0]_0 ({p_1_in90_in,p_1_in87_in,p_1_in84_in,p_1_in81_in,p_1_in78_in,p_1_in75_in,p_1_in69_in,p_1_in63_in,p_1_in60_in,p_1_in48_in,p_1_in45_in,p_1_in42_in,p_1_in39_in,p_1_in36_in,p_1_in33_in,p_1_in30_in,p_1_in27_in,p_1_in24_in,p_1_in21_in,p_1_in18_in,p_1_in15_in,p_1_in3_in,p_1_in,u8_mult_n_79}),
        .\lower_reg_reg[30]_0 (lower_reg),
        .\lower_reg_reg[31]_0 (u8_mult_n_143),
        .mode_reg_reg_0(mode_reg_reg),
        .mode_reg_reg_1(u2_mem_ctrl_n_264),
        .negate_reg(negate_reg),
        .negate_reg_reg_0(negate_reg_reg_0),
        .negate_reg_reg_1(u2_mem_ctrl_n_186),
        .negate_reg_reg_2(u2_mem_ctrl_n_188),
        .negate_reg_reg_3(u2_mem_ctrl_n_190),
        .negate_reg_reg_4(u2_mem_ctrl_n_5),
        .neqOp0_in(neqOp0_in),
        .\opcode_reg_reg[15] (bb_reg),
        .\opcode_reg_reg[30] (u2_mem_ctrl_n_300),
        .\opcode_reg_reg[30]_0 (u2_mem_ctrl_n_168),
        .\opcode_reg_reg[30]_1 (u2_mem_ctrl_n_169),
        .\opcode_reg_reg[30]_2 (upper_reg),
        .\opcode_reg_reg[30]_3 (u2_mem_ctrl_n_301),
        .\opcode_reg_reg[30]_4 ({u2_mem_ctrl_n_136,u2_mem_ctrl_n_137,u2_mem_ctrl_n_138,u2_mem_ctrl_n_139,u2_mem_ctrl_n_140,u2_mem_ctrl_n_141,u2_mem_ctrl_n_142,u2_mem_ctrl_n_143,u2_mem_ctrl_n_144,u2_mem_ctrl_n_145,u2_mem_ctrl_n_146,u2_mem_ctrl_n_147,u2_mem_ctrl_n_148,u2_mem_ctrl_n_149,u2_mem_ctrl_n_150,u2_mem_ctrl_n_151,u2_mem_ctrl_n_152,u2_mem_ctrl_n_153,u2_mem_ctrl_n_154,u2_mem_ctrl_n_155,u2_mem_ctrl_n_156,u2_mem_ctrl_n_157,u2_mem_ctrl_n_158,u2_mem_ctrl_n_159,u2_mem_ctrl_n_160,u2_mem_ctrl_n_161,u2_mem_ctrl_n_162,u2_mem_ctrl_n_163,u2_mem_ctrl_n_164,u2_mem_ctrl_n_165,u2_mem_ctrl_n_166,u2_mem_ctrl_n_167}),
        .\pc_reg_reg[10] (u8_mult_n_52),
        .\pc_reg_reg[10]_0 (u8_mult_n_91),
        .\pc_reg_reg[11] (u8_mult_n_47),
        .\pc_reg_reg[12] (u8_mult_n_51),
        .\pc_reg_reg[13] (u8_mult_n_50),
        .\pc_reg_reg[13]_0 (u8_mult_n_89),
        .\pc_reg_reg[14] (u8_mult_n_49),
        .\pc_reg_reg[14]_0 (u8_mult_n_88),
        .\pc_reg_reg[15] (u8_mult_n_48),
        .\pc_reg_reg[15]_0 (u8_mult_n_87),
        .\pc_reg_reg[16] (u8_mult_n_85),
        .\pc_reg_reg[16]_0 (u8_mult_n_86),
        .\pc_reg_reg[16]_1 (u8_mult_n_136),
        .\pc_reg_reg[17] (u8_mult_n_46),
        .\pc_reg_reg[18] (u8_mult_n_135),
        .\pc_reg_reg[19] (u8_mult_n_134),
        .\pc_reg_reg[20] (u8_mult_n_133),
        .\pc_reg_reg[21] (u8_mult_n_83),
        .\pc_reg_reg[21]_0 (u8_mult_n_132),
        .\pc_reg_reg[22] (u8_mult_n_45),
        .\pc_reg_reg[23] (u8_mult_n_131),
        .\pc_reg_reg[24] (u8_mult_n_44),
        .\pc_reg_reg[24]_0 (u8_mult_n_84),
        .\pc_reg_reg[25] (u8_mult_n_130),
        .\pc_reg_reg[27] (u8_mult_n_43),
        .\pc_reg_reg[28] (u8_mult_n_42),
        .\pc_reg_reg[28]_0 (u8_mult_n_82),
        .\pc_reg_reg[29] (u8_mult_n_41),
        .\pc_reg_reg[29]_0 (u8_mult_n_81),
        .\pc_reg_reg[2] (u8_mult_n_142),
        .\pc_reg_reg[31] (u8_mult_n_39),
        .\pc_reg_reg[31]_0 (u8_mult_n_40),
        .\pc_reg_reg[31]_1 (u8_mult_n_55),
        .\pc_reg_reg[31]_2 (u8_mult_n_80),
        .\pc_reg_reg[31]_3 (u8_mult_n_129),
        .\pc_reg_reg[3] (u8_mult_n_141),
        .\pc_reg_reg[4] (u8_mult_n_140),
        .\pc_reg_reg[5] (u8_mult_n_139),
        .\pc_reg_reg[6] (u8_mult_n_90),
        .\pc_reg_reg[6]_0 (u8_mult_n_138),
        .\pc_reg_reg[7] (u8_mult_n_137),
        .\pc_reg_reg[8] (u8_mult_n_54),
        .\pc_reg_reg[8]_0 (u8_mult_n_93),
        .\pc_reg_reg[9] (u8_mult_n_53),
        .\pc_reg_reg[9]_0 (u8_mult_n_92),
        .reset(reset),
        .\reset_reg_reg[3] (reset_reg_reg__0),
        .sign2_reg_reg_0(sign2_reg_reg),
        .sign_reg1_out(sign_reg1_out),
        .sign_reg_reg_0(sign_reg_reg),
        .sign_reg_reg_1(sign_reg_reg_0),
        .sign_reg_reg_2(sign_reg_reg_1),
        .\upper_reg_reg[0]_0 (u8_mult_n_144),
        .\upper_reg_reg[10]_0 (u8_mult_n_165),
        .\upper_reg_reg[11]_0 (u8_mult_n_164),
        .\upper_reg_reg[12]_0 (u8_mult_n_163),
        .\upper_reg_reg[13]_0 (u8_mult_n_162),
        .\upper_reg_reg[14]_0 (u8_mult_n_161),
        .\upper_reg_reg[15]_0 (u8_mult_n_160),
        .\upper_reg_reg[16]_0 (u8_mult_n_159),
        .\upper_reg_reg[17]_0 (u8_mult_n_158),
        .\upper_reg_reg[18]_0 (u8_mult_n_157),
        .\upper_reg_reg[19]_0 (u8_mult_n_156),
        .\upper_reg_reg[1]_0 (u8_mult_n_174),
        .\upper_reg_reg[1]_1 (u2_mem_ctrl_n_192),
        .\upper_reg_reg[20]_0 (u8_mult_n_155),
        .\upper_reg_reg[21]_0 (u8_mult_n_154),
        .\upper_reg_reg[22]_0 (u8_mult_n_153),
        .\upper_reg_reg[23]_0 (u8_mult_n_152),
        .\upper_reg_reg[24]_0 (u8_mult_n_151),
        .\upper_reg_reg[25]_0 (u8_mult_n_150),
        .\upper_reg_reg[26]_0 (u8_mult_n_149),
        .\upper_reg_reg[27]_0 (u8_mult_n_148),
        .\upper_reg_reg[28]_0 (u8_mult_n_147),
        .\upper_reg_reg[29]_0 (u8_mult_n_146),
        .\upper_reg_reg[2]_0 (u8_mult_n_173),
        .\upper_reg_reg[30]_0 (u8_mult_n_145),
        .\upper_reg_reg[30]_1 (u2_mem_ctrl_n_187),
        .\upper_reg_reg[31]_0 (u8_mult_n_94),
        .\upper_reg_reg[31]_1 (u8_mult_n_95),
        .\upper_reg_reg[3]_0 (u8_mult_n_172),
        .\upper_reg_reg[4]_0 (u8_mult_n_171),
        .\upper_reg_reg[5]_0 (u8_mult_n_170),
        .\upper_reg_reg[6]_0 (u8_mult_n_169),
        .\upper_reg_reg[6]_1 (u2_mem_ctrl_n_191),
        .\upper_reg_reg[7]_0 (u8_mult_n_168),
        .\upper_reg_reg[8]_0 (u8_mult_n_167),
        .\upper_reg_reg[9]_0 (u8_mult_n_166));
endmodule

module design_1_Dispatcher_HardBlare_0_0_mult
   (\aa_reg_reg[31]_0 ,
    sign_reg_reg_0,
    sign2_reg_reg_0,
    negate_reg,
    Q,
    \address_reg_reg[31] ,
    \lower_reg_reg[30]_0 ,
    \pc_reg_reg[31] ,
    \pc_reg_reg[31]_0 ,
    \pc_reg_reg[29] ,
    \pc_reg_reg[28] ,
    \pc_reg_reg[27] ,
    \pc_reg_reg[24] ,
    \pc_reg_reg[22] ,
    \pc_reg_reg[17] ,
    \pc_reg_reg[11] ,
    \pc_reg_reg[15] ,
    \pc_reg_reg[14] ,
    \pc_reg_reg[13] ,
    \pc_reg_reg[12] ,
    \pc_reg_reg[10] ,
    \pc_reg_reg[9] ,
    \pc_reg_reg[8] ,
    \pc_reg_reg[31]_1 ,
    \lower_reg_reg[0]_0 ,
    \pc_reg_reg[31]_2 ,
    \pc_reg_reg[29]_0 ,
    \pc_reg_reg[28]_0 ,
    \pc_reg_reg[21] ,
    \pc_reg_reg[24]_0 ,
    \pc_reg_reg[16] ,
    \pc_reg_reg[16]_0 ,
    \pc_reg_reg[15]_0 ,
    \pc_reg_reg[14]_0 ,
    \pc_reg_reg[13]_0 ,
    \pc_reg_reg[6] ,
    \pc_reg_reg[10]_0 ,
    \pc_reg_reg[9]_0 ,
    \pc_reg_reg[8]_0 ,
    \upper_reg_reg[31]_0 ,
    \upper_reg_reg[31]_1 ,
    neqOp0_in,
    lower_reg0__62,
    \aa_reg_reg[30]_0 ,
    \pc_reg_reg[31]_3 ,
    \pc_reg_reg[25] ,
    \pc_reg_reg[23] ,
    \pc_reg_reg[21]_0 ,
    \pc_reg_reg[20] ,
    \pc_reg_reg[19] ,
    \pc_reg_reg[18] ,
    \pc_reg_reg[16]_1 ,
    \pc_reg_reg[7] ,
    \pc_reg_reg[6]_0 ,
    \pc_reg_reg[5] ,
    \pc_reg_reg[4] ,
    \pc_reg_reg[3] ,
    \pc_reg_reg[2] ,
    \lower_reg_reg[31]_0 ,
    \upper_reg_reg[0]_0 ,
    \upper_reg_reg[30]_0 ,
    \upper_reg_reg[29]_0 ,
    \upper_reg_reg[28]_0 ,
    \upper_reg_reg[27]_0 ,
    \upper_reg_reg[26]_0 ,
    \upper_reg_reg[25]_0 ,
    \upper_reg_reg[24]_0 ,
    \upper_reg_reg[23]_0 ,
    \upper_reg_reg[22]_0 ,
    \upper_reg_reg[21]_0 ,
    \upper_reg_reg[20]_0 ,
    \upper_reg_reg[19]_0 ,
    \upper_reg_reg[18]_0 ,
    \upper_reg_reg[17]_0 ,
    \upper_reg_reg[16]_0 ,
    \upper_reg_reg[15]_0 ,
    \upper_reg_reg[14]_0 ,
    \upper_reg_reg[13]_0 ,
    \upper_reg_reg[12]_0 ,
    \upper_reg_reg[11]_0 ,
    \upper_reg_reg[10]_0 ,
    \upper_reg_reg[9]_0 ,
    \upper_reg_reg[8]_0 ,
    \upper_reg_reg[7]_0 ,
    \upper_reg_reg[6]_0 ,
    \upper_reg_reg[5]_0 ,
    \upper_reg_reg[4]_0 ,
    \upper_reg_reg[3]_0 ,
    \upper_reg_reg[2]_0 ,
    \upper_reg_reg[1]_0 ,
    \bb_reg_reg[30]_0 ,
    sign_reg1_out,
    \count_reg_reg[5]_0 ,
    mode_reg_reg_0,
    clk,
    sign_reg_reg_1,
    sign_reg_reg_2,
    negate_reg_reg_0,
    \opcode_reg_reg[30] ,
    reset,
    \reset_reg_reg[3] ,
    negate_reg_reg_1,
    \upper_reg_reg[30]_1 ,
    negate_reg_reg_2,
    negate_reg_reg_3,
    negate_reg_reg_4,
    \upper_reg_reg[6]_1 ,
    \upper_reg_reg[1]_1 ,
    \opcode_reg_reg[30]_0 ,
    E,
    D,
    \opcode_reg_reg[15] ,
    mode_reg_reg_1,
    \bb_reg_reg[1]_0 ,
    \opcode_reg_reg[30]_1 ,
    \opcode_reg_reg[30]_2 ,
    \opcode_reg_reg[30]_3 ,
    \opcode_reg_reg[30]_4 );
  output \aa_reg_reg[31]_0 ;
  output sign_reg_reg_0;
  output sign2_reg_reg_0;
  output negate_reg;
  output [1:0]Q;
  output \address_reg_reg[31] ;
  output [31:0]\lower_reg_reg[30]_0 ;
  output \pc_reg_reg[31] ;
  output \pc_reg_reg[31]_0 ;
  output \pc_reg_reg[29] ;
  output \pc_reg_reg[28] ;
  output \pc_reg_reg[27] ;
  output \pc_reg_reg[24] ;
  output \pc_reg_reg[22] ;
  output \pc_reg_reg[17] ;
  output \pc_reg_reg[11] ;
  output \pc_reg_reg[15] ;
  output \pc_reg_reg[14] ;
  output \pc_reg_reg[13] ;
  output \pc_reg_reg[12] ;
  output \pc_reg_reg[10] ;
  output \pc_reg_reg[9] ;
  output \pc_reg_reg[8] ;
  output \pc_reg_reg[31]_1 ;
  output [23:0]\lower_reg_reg[0]_0 ;
  output \pc_reg_reg[31]_2 ;
  output \pc_reg_reg[29]_0 ;
  output \pc_reg_reg[28]_0 ;
  output \pc_reg_reg[21] ;
  output \pc_reg_reg[24]_0 ;
  output \pc_reg_reg[16] ;
  output \pc_reg_reg[16]_0 ;
  output \pc_reg_reg[15]_0 ;
  output \pc_reg_reg[14]_0 ;
  output \pc_reg_reg[13]_0 ;
  output \pc_reg_reg[6] ;
  output \pc_reg_reg[10]_0 ;
  output \pc_reg_reg[9]_0 ;
  output \pc_reg_reg[8]_0 ;
  output \upper_reg_reg[31]_0 ;
  output \upper_reg_reg[31]_1 ;
  output neqOp0_in;
  output lower_reg0__62;
  output [30:0]\aa_reg_reg[30]_0 ;
  output \pc_reg_reg[31]_3 ;
  output \pc_reg_reg[25] ;
  output \pc_reg_reg[23] ;
  output \pc_reg_reg[21]_0 ;
  output \pc_reg_reg[20] ;
  output \pc_reg_reg[19] ;
  output \pc_reg_reg[18] ;
  output \pc_reg_reg[16]_1 ;
  output \pc_reg_reg[7] ;
  output \pc_reg_reg[6]_0 ;
  output \pc_reg_reg[5] ;
  output \pc_reg_reg[4] ;
  output \pc_reg_reg[3] ;
  output \pc_reg_reg[2] ;
  output \lower_reg_reg[31]_0 ;
  output \upper_reg_reg[0]_0 ;
  output \upper_reg_reg[30]_0 ;
  output \upper_reg_reg[29]_0 ;
  output \upper_reg_reg[28]_0 ;
  output \upper_reg_reg[27]_0 ;
  output \upper_reg_reg[26]_0 ;
  output \upper_reg_reg[25]_0 ;
  output \upper_reg_reg[24]_0 ;
  output \upper_reg_reg[23]_0 ;
  output \upper_reg_reg[22]_0 ;
  output \upper_reg_reg[21]_0 ;
  output \upper_reg_reg[20]_0 ;
  output \upper_reg_reg[19]_0 ;
  output \upper_reg_reg[18]_0 ;
  output \upper_reg_reg[17]_0 ;
  output \upper_reg_reg[16]_0 ;
  output \upper_reg_reg[15]_0 ;
  output \upper_reg_reg[14]_0 ;
  output \upper_reg_reg[13]_0 ;
  output \upper_reg_reg[12]_0 ;
  output \upper_reg_reg[11]_0 ;
  output \upper_reg_reg[10]_0 ;
  output \upper_reg_reg[9]_0 ;
  output \upper_reg_reg[8]_0 ;
  output \upper_reg_reg[7]_0 ;
  output \upper_reg_reg[6]_0 ;
  output \upper_reg_reg[5]_0 ;
  output \upper_reg_reg[4]_0 ;
  output \upper_reg_reg[3]_0 ;
  output \upper_reg_reg[2]_0 ;
  output \upper_reg_reg[1]_0 ;
  output [30:0]\bb_reg_reg[30]_0 ;
  output sign_reg1_out;
  output \count_reg_reg[5]_0 ;
  input mode_reg_reg_0;
  input clk;
  input sign_reg_reg_1;
  input sign_reg_reg_2;
  input negate_reg_reg_0;
  input \opcode_reg_reg[30] ;
  input reset;
  input [3:0]\reset_reg_reg[3] ;
  input negate_reg_reg_1;
  input \upper_reg_reg[30]_1 ;
  input negate_reg_reg_2;
  input negate_reg_reg_3;
  input negate_reg_reg_4;
  input \upper_reg_reg[6]_1 ;
  input \upper_reg_reg[1]_1 ;
  input \opcode_reg_reg[30]_0 ;
  input [0:0]E;
  input [2:0]D;
  input [31:0]\opcode_reg_reg[15] ;
  input [0:0]mode_reg_reg_1;
  input [31:0]\bb_reg_reg[1]_0 ;
  input [0:0]\opcode_reg_reg[30]_1 ;
  input [31:0]\opcode_reg_reg[30]_2 ;
  input [0:0]\opcode_reg_reg[30]_3 ;
  input [31:0]\opcode_reg_reg[30]_4 ;

  wire [2:0]D;
  wire [0:0]E;
  wire [1:0]Q;
  wire [30:0]\aa_reg_reg[30]_0 ;
  wire \aa_reg_reg[31]_0 ;
  wire \aa_reg_reg_n_0_[0] ;
  wire \address_reg_reg[31] ;
  wire [31:0]\bb_reg_reg[1]_0 ;
  wire [30:0]\bb_reg_reg[30]_0 ;
  wire \bb_reg_reg_n_0_[0] ;
  wire \bram_firmware_code_address_reg[16]_i_10_n_0 ;
  wire \bram_firmware_code_address_reg[18]_i_11_n_0 ;
  wire \bram_firmware_code_address_reg[18]_i_12_n_0 ;
  wire \bram_firmware_code_address_reg[19]_i_18_n_0 ;
  wire \bram_firmware_code_address_reg[19]_i_19_n_0 ;
  wire \bram_firmware_code_address_reg[19]_i_26_n_0 ;
  wire \bram_firmware_code_address_reg[3]_i_11_n_0 ;
  wire \bram_firmware_code_address_reg[3]_i_12_n_0 ;
  wire \bram_firmware_code_address_reg[4]_i_12_n_0 ;
  wire \bram_firmware_code_address_reg[4]_i_13_n_0 ;
  wire \bram_firmware_code_address_reg[5]_i_12_n_0 ;
  wire \bram_firmware_code_address_reg[5]_i_13_n_0 ;
  wire \bram_firmware_code_address_reg[6]_i_12_n_0 ;
  wire \bram_firmware_code_address_reg[7]_i_12_n_0 ;
  wire bv_adder010_out;
  wire bv_adder013_out;
  wire bv_adder016_out;
  wire bv_adder019_out;
  wire bv_adder01_out;
  wire bv_adder022_out;
  wire bv_adder025_out;
  wire bv_adder028_out;
  wire bv_adder031_out;
  wire bv_adder034_out;
  wire bv_adder037_out;
  wire bv_adder040_out;
  wire bv_adder043_out;
  wire bv_adder046_out;
  wire bv_adder049_out;
  wire bv_adder04_out;
  wire bv_adder052_out;
  wire bv_adder055_out;
  wire bv_adder058_out;
  wire bv_adder061_out;
  wire bv_adder064_out;
  wire bv_adder067_out;
  wire bv_adder070_out;
  wire bv_adder073_out;
  wire bv_adder076_out;
  wire bv_adder079_out;
  wire bv_adder07_out;
  wire bv_adder082_out;
  wire bv_adder085_out;
  wire bv_adder088_out;
  wire bv_adder091_out;
  wire bv_adder13__3;
  wire bv_adder17__3;
  wire bv_adder21__3;
  wire bv_adder25__3;
  wire bv_adder29__3;
  wire bv_adder33__3;
  wire bv_adder37__3;
  wire bv_adder41__3;
  wire bv_adder45__3;
  wire bv_adder49__3;
  wire bv_adder53__3;
  wire bv_adder57__3;
  wire bv_adder5__3;
  wire bv_adder61__3;
  wire bv_adder9__3;
  wire clk;
  wire \count_reg[2]_i_1_n_0 ;
  wire \count_reg[3]_i_1_n_0 ;
  wire \count_reg[4]_i_1_n_0 ;
  wire \count_reg_reg[5]_0 ;
  wire \count_reg_reg_n_0_[2] ;
  wire \count_reg_reg_n_0_[3] ;
  wire \count_reg_reg_n_0_[4] ;
  wire \count_reg_reg_n_0_[5] ;
  wire eqOp__0;
  wire lower_reg0__62;
  wire \lower_reg[0]_i_10_n_0 ;
  wire \lower_reg[0]_i_11_n_0 ;
  wire \lower_reg[0]_i_12_n_0 ;
  wire \lower_reg[0]_i_13_n_0 ;
  wire \lower_reg[0]_i_14_n_0 ;
  wire \lower_reg[0]_i_15_n_0 ;
  wire \lower_reg[0]_i_16_n_0 ;
  wire \lower_reg[0]_i_17_n_0 ;
  wire \lower_reg[0]_i_18_n_0 ;
  wire \lower_reg[0]_i_19_n_0 ;
  wire \lower_reg[0]_i_20_n_0 ;
  wire \lower_reg[0]_i_4_n_0 ;
  wire \lower_reg[0]_i_5_n_0 ;
  wire \lower_reg[0]_i_8_n_0 ;
  wire \lower_reg[0]_i_9_n_0 ;
  wire [23:0]\lower_reg_reg[0]_0 ;
  wire [31:0]\lower_reg_reg[30]_0 ;
  wire \lower_reg_reg[31]_0 ;
  wire mode_reg_reg_0;
  wire [0:0]mode_reg_reg_1;
  wire negate_reg;
  wire negate_reg_reg_0;
  wire negate_reg_reg_1;
  wire negate_reg_reg_2;
  wire negate_reg_reg_3;
  wire negate_reg_reg_4;
  wire neqOp0_in;
  wire [31:0]\opcode_reg_reg[15] ;
  wire \opcode_reg_reg[30] ;
  wire \opcode_reg_reg[30]_0 ;
  wire [0:0]\opcode_reg_reg[30]_1 ;
  wire [31:0]\opcode_reg_reg[30]_2 ;
  wire [0:0]\opcode_reg_reg[30]_3 ;
  wire [31:0]\opcode_reg_reg[30]_4 ;
  wire p_0_in342_in;
  wire p_1_in12_in;
  wire p_1_in51_in;
  wire p_1_in54_in;
  wire p_1_in57_in;
  wire p_1_in66_in;
  wire p_1_in6_in;
  wire p_1_in72_in;
  wire p_1_in9_in;
  wire \pc_reg[20]_i_10_n_0 ;
  wire \pc_reg[20]_i_11_n_0 ;
  wire \pc_reg[21]_i_9_n_0 ;
  wire \pc_reg[23]_i_10_n_0 ;
  wire \pc_reg[23]_i_11_n_0 ;
  wire \pc_reg[25]_i_10_n_0 ;
  wire \pc_reg[25]_i_11_n_0 ;
  wire \pc_reg[25]_i_13_n_0 ;
  wire \pc_reg_reg[10] ;
  wire \pc_reg_reg[10]_0 ;
  wire \pc_reg_reg[11] ;
  wire \pc_reg_reg[12] ;
  wire \pc_reg_reg[13] ;
  wire \pc_reg_reg[13]_0 ;
  wire \pc_reg_reg[14] ;
  wire \pc_reg_reg[14]_0 ;
  wire \pc_reg_reg[15] ;
  wire \pc_reg_reg[15]_0 ;
  wire \pc_reg_reg[16] ;
  wire \pc_reg_reg[16]_0 ;
  wire \pc_reg_reg[16]_1 ;
  wire \pc_reg_reg[17] ;
  wire \pc_reg_reg[18] ;
  wire \pc_reg_reg[19] ;
  wire \pc_reg_reg[20] ;
  wire \pc_reg_reg[21] ;
  wire \pc_reg_reg[21]_0 ;
  wire \pc_reg_reg[22] ;
  wire \pc_reg_reg[23] ;
  wire \pc_reg_reg[24] ;
  wire \pc_reg_reg[24]_0 ;
  wire \pc_reg_reg[25] ;
  wire \pc_reg_reg[27] ;
  wire \pc_reg_reg[28] ;
  wire \pc_reg_reg[28]_0 ;
  wire \pc_reg_reg[29] ;
  wire \pc_reg_reg[29]_0 ;
  wire \pc_reg_reg[2] ;
  wire \pc_reg_reg[31] ;
  wire \pc_reg_reg[31]_0 ;
  wire \pc_reg_reg[31]_1 ;
  wire \pc_reg_reg[31]_2 ;
  wire \pc_reg_reg[31]_3 ;
  wire \pc_reg_reg[3] ;
  wire \pc_reg_reg[4] ;
  wire \pc_reg_reg[5] ;
  wire \pc_reg_reg[6] ;
  wire \pc_reg_reg[6]_0 ;
  wire \pc_reg_reg[7] ;
  wire \pc_reg_reg[8] ;
  wire \pc_reg_reg[8]_0 ;
  wire \pc_reg_reg[9] ;
  wire \pc_reg_reg[9]_0 ;
  wire reset;
  wire reset_in1_out;
  wire [3:0]\reset_reg_reg[3] ;
  wire sign2_reg_reg_0;
  wire sign_reg1_out;
  wire sign_reg_reg_0;
  wire sign_reg_reg_1;
  wire sign_reg_reg_2;
  wire \upper_reg_reg[0]_0 ;
  wire \upper_reg_reg[10]_0 ;
  wire \upper_reg_reg[11]_0 ;
  wire \upper_reg_reg[12]_0 ;
  wire \upper_reg_reg[13]_0 ;
  wire \upper_reg_reg[14]_0 ;
  wire \upper_reg_reg[15]_0 ;
  wire \upper_reg_reg[16]_0 ;
  wire \upper_reg_reg[17]_0 ;
  wire \upper_reg_reg[18]_0 ;
  wire \upper_reg_reg[19]_0 ;
  wire \upper_reg_reg[1]_0 ;
  wire \upper_reg_reg[1]_1 ;
  wire \upper_reg_reg[20]_0 ;
  wire \upper_reg_reg[21]_0 ;
  wire \upper_reg_reg[22]_0 ;
  wire \upper_reg_reg[23]_0 ;
  wire \upper_reg_reg[24]_0 ;
  wire \upper_reg_reg[25]_0 ;
  wire \upper_reg_reg[26]_0 ;
  wire \upper_reg_reg[27]_0 ;
  wire \upper_reg_reg[28]_0 ;
  wire \upper_reg_reg[29]_0 ;
  wire \upper_reg_reg[2]_0 ;
  wire \upper_reg_reg[30]_0 ;
  wire \upper_reg_reg[30]_1 ;
  wire \upper_reg_reg[31]_0 ;
  wire \upper_reg_reg[31]_1 ;
  wire \upper_reg_reg[3]_0 ;
  wire \upper_reg_reg[4]_0 ;
  wire \upper_reg_reg[5]_0 ;
  wire \upper_reg_reg[6]_0 ;
  wire \upper_reg_reg[6]_1 ;
  wire \upper_reg_reg[7]_0 ;
  wire \upper_reg_reg[8]_0 ;
  wire \upper_reg_reg[9]_0 ;

  FDCE \aa_reg_reg[0] 
       (.C(clk),
        .CE(mode_reg_reg_1),
        .CLR(reset_in1_out),
        .D(\bb_reg_reg[1]_0 [0]),
        .Q(\aa_reg_reg_n_0_[0] ));
  FDCE \aa_reg_reg[10] 
       (.C(clk),
        .CE(mode_reg_reg_1),
        .CLR(reset_in1_out),
        .D(\bb_reg_reg[1]_0 [10]),
        .Q(\aa_reg_reg[30]_0 [9]));
  FDCE \aa_reg_reg[11] 
       (.C(clk),
        .CE(mode_reg_reg_1),
        .CLR(reset_in1_out),
        .D(\bb_reg_reg[1]_0 [11]),
        .Q(\aa_reg_reg[30]_0 [10]));
  FDCE \aa_reg_reg[12] 
       (.C(clk),
        .CE(mode_reg_reg_1),
        .CLR(reset_in1_out),
        .D(\bb_reg_reg[1]_0 [12]),
        .Q(\aa_reg_reg[30]_0 [11]));
  FDCE \aa_reg_reg[13] 
       (.C(clk),
        .CE(mode_reg_reg_1),
        .CLR(reset_in1_out),
        .D(\bb_reg_reg[1]_0 [13]),
        .Q(\aa_reg_reg[30]_0 [12]));
  FDCE \aa_reg_reg[14] 
       (.C(clk),
        .CE(mode_reg_reg_1),
        .CLR(reset_in1_out),
        .D(\bb_reg_reg[1]_0 [14]),
        .Q(\aa_reg_reg[30]_0 [13]));
  FDCE \aa_reg_reg[15] 
       (.C(clk),
        .CE(mode_reg_reg_1),
        .CLR(reset_in1_out),
        .D(\bb_reg_reg[1]_0 [15]),
        .Q(\aa_reg_reg[30]_0 [14]));
  FDCE \aa_reg_reg[16] 
       (.C(clk),
        .CE(mode_reg_reg_1),
        .CLR(reset_in1_out),
        .D(\bb_reg_reg[1]_0 [16]),
        .Q(\aa_reg_reg[30]_0 [15]));
  FDCE \aa_reg_reg[17] 
       (.C(clk),
        .CE(mode_reg_reg_1),
        .CLR(reset_in1_out),
        .D(\bb_reg_reg[1]_0 [17]),
        .Q(\aa_reg_reg[30]_0 [16]));
  FDCE \aa_reg_reg[18] 
       (.C(clk),
        .CE(mode_reg_reg_1),
        .CLR(reset_in1_out),
        .D(\bb_reg_reg[1]_0 [18]),
        .Q(\aa_reg_reg[30]_0 [17]));
  FDCE \aa_reg_reg[19] 
       (.C(clk),
        .CE(mode_reg_reg_1),
        .CLR(reset_in1_out),
        .D(\bb_reg_reg[1]_0 [19]),
        .Q(\aa_reg_reg[30]_0 [18]));
  FDCE \aa_reg_reg[1] 
       (.C(clk),
        .CE(mode_reg_reg_1),
        .CLR(reset_in1_out),
        .D(\bb_reg_reg[1]_0 [1]),
        .Q(\aa_reg_reg[30]_0 [0]));
  FDCE \aa_reg_reg[20] 
       (.C(clk),
        .CE(mode_reg_reg_1),
        .CLR(reset_in1_out),
        .D(\bb_reg_reg[1]_0 [20]),
        .Q(\aa_reg_reg[30]_0 [19]));
  FDCE \aa_reg_reg[21] 
       (.C(clk),
        .CE(mode_reg_reg_1),
        .CLR(reset_in1_out),
        .D(\bb_reg_reg[1]_0 [21]),
        .Q(\aa_reg_reg[30]_0 [20]));
  FDCE \aa_reg_reg[22] 
       (.C(clk),
        .CE(mode_reg_reg_1),
        .CLR(reset_in1_out),
        .D(\bb_reg_reg[1]_0 [22]),
        .Q(\aa_reg_reg[30]_0 [21]));
  FDCE \aa_reg_reg[23] 
       (.C(clk),
        .CE(mode_reg_reg_1),
        .CLR(reset_in1_out),
        .D(\bb_reg_reg[1]_0 [23]),
        .Q(\aa_reg_reg[30]_0 [22]));
  FDCE \aa_reg_reg[24] 
       (.C(clk),
        .CE(mode_reg_reg_1),
        .CLR(reset_in1_out),
        .D(\bb_reg_reg[1]_0 [24]),
        .Q(\aa_reg_reg[30]_0 [23]));
  FDCE \aa_reg_reg[25] 
       (.C(clk),
        .CE(mode_reg_reg_1),
        .CLR(reset_in1_out),
        .D(\bb_reg_reg[1]_0 [25]),
        .Q(\aa_reg_reg[30]_0 [24]));
  FDCE \aa_reg_reg[26] 
       (.C(clk),
        .CE(mode_reg_reg_1),
        .CLR(reset_in1_out),
        .D(\bb_reg_reg[1]_0 [26]),
        .Q(\aa_reg_reg[30]_0 [25]));
  FDCE \aa_reg_reg[27] 
       (.C(clk),
        .CE(mode_reg_reg_1),
        .CLR(reset_in1_out),
        .D(\bb_reg_reg[1]_0 [27]),
        .Q(\aa_reg_reg[30]_0 [26]));
  FDCE \aa_reg_reg[28] 
       (.C(clk),
        .CE(mode_reg_reg_1),
        .CLR(reset_in1_out),
        .D(\bb_reg_reg[1]_0 [28]),
        .Q(\aa_reg_reg[30]_0 [27]));
  FDCE \aa_reg_reg[29] 
       (.C(clk),
        .CE(mode_reg_reg_1),
        .CLR(reset_in1_out),
        .D(\bb_reg_reg[1]_0 [29]),
        .Q(\aa_reg_reg[30]_0 [28]));
  FDCE \aa_reg_reg[2] 
       (.C(clk),
        .CE(mode_reg_reg_1),
        .CLR(reset_in1_out),
        .D(\bb_reg_reg[1]_0 [2]),
        .Q(\aa_reg_reg[30]_0 [1]));
  FDCE \aa_reg_reg[30] 
       (.C(clk),
        .CE(mode_reg_reg_1),
        .CLR(reset_in1_out),
        .D(\bb_reg_reg[1]_0 [30]),
        .Q(\aa_reg_reg[30]_0 [29]));
  FDCE \aa_reg_reg[31] 
       (.C(clk),
        .CE(mode_reg_reg_1),
        .CLR(reset_in1_out),
        .D(\bb_reg_reg[1]_0 [31]),
        .Q(\aa_reg_reg[30]_0 [30]));
  FDCE \aa_reg_reg[3] 
       (.C(clk),
        .CE(mode_reg_reg_1),
        .CLR(reset_in1_out),
        .D(\bb_reg_reg[1]_0 [3]),
        .Q(\aa_reg_reg[30]_0 [2]));
  FDCE \aa_reg_reg[4] 
       (.C(clk),
        .CE(mode_reg_reg_1),
        .CLR(reset_in1_out),
        .D(\bb_reg_reg[1]_0 [4]),
        .Q(\aa_reg_reg[30]_0 [3]));
  FDCE \aa_reg_reg[5] 
       (.C(clk),
        .CE(mode_reg_reg_1),
        .CLR(reset_in1_out),
        .D(\bb_reg_reg[1]_0 [5]),
        .Q(\aa_reg_reg[30]_0 [4]));
  FDCE \aa_reg_reg[6] 
       (.C(clk),
        .CE(mode_reg_reg_1),
        .CLR(reset_in1_out),
        .D(\bb_reg_reg[1]_0 [6]),
        .Q(\aa_reg_reg[30]_0 [5]));
  FDCE \aa_reg_reg[7] 
       (.C(clk),
        .CE(mode_reg_reg_1),
        .CLR(reset_in1_out),
        .D(\bb_reg_reg[1]_0 [7]),
        .Q(\aa_reg_reg[30]_0 [6]));
  FDCE \aa_reg_reg[8] 
       (.C(clk),
        .CE(mode_reg_reg_1),
        .CLR(reset_in1_out),
        .D(\bb_reg_reg[1]_0 [8]),
        .Q(\aa_reg_reg[30]_0 [7]));
  FDCE \aa_reg_reg[9] 
       (.C(clk),
        .CE(mode_reg_reg_1),
        .CLR(reset_in1_out),
        .D(\bb_reg_reg[1]_0 [9]),
        .Q(\aa_reg_reg[30]_0 [8]));
  LUT6 #(
    .INIT(64'h0000000000000010)) 
    \address_reg[31]_i_7 
       (.I0(\lower_reg_reg[30]_0 [29]),
        .I1(\lower_reg_reg[30]_0 [27]),
        .I2(\pc_reg_reg[31] ),
        .I3(\lower_reg_reg[30]_0 [26]),
        .I4(\lower_reg_reg[30]_0 [28]),
        .I5(\lower_reg_reg[30]_0 [30]),
        .O(\address_reg_reg[31] ));
  FDCE \bb_reg_reg[0] 
       (.C(clk),
        .CE(E),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[15] [0]),
        .Q(\bb_reg_reg_n_0_[0] ));
  FDCE \bb_reg_reg[10] 
       (.C(clk),
        .CE(E),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[15] [10]),
        .Q(\bb_reg_reg[30]_0 [9]));
  FDCE \bb_reg_reg[11] 
       (.C(clk),
        .CE(E),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[15] [11]),
        .Q(\bb_reg_reg[30]_0 [10]));
  FDCE \bb_reg_reg[12] 
       (.C(clk),
        .CE(E),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[15] [12]),
        .Q(\bb_reg_reg[30]_0 [11]));
  FDCE \bb_reg_reg[13] 
       (.C(clk),
        .CE(E),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[15] [13]),
        .Q(\bb_reg_reg[30]_0 [12]));
  FDCE \bb_reg_reg[14] 
       (.C(clk),
        .CE(E),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[15] [14]),
        .Q(\bb_reg_reg[30]_0 [13]));
  FDCE \bb_reg_reg[15] 
       (.C(clk),
        .CE(E),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[15] [15]),
        .Q(\bb_reg_reg[30]_0 [14]));
  FDCE \bb_reg_reg[16] 
       (.C(clk),
        .CE(E),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[15] [16]),
        .Q(\bb_reg_reg[30]_0 [15]));
  FDCE \bb_reg_reg[17] 
       (.C(clk),
        .CE(E),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[15] [17]),
        .Q(\bb_reg_reg[30]_0 [16]));
  FDCE \bb_reg_reg[18] 
       (.C(clk),
        .CE(E),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[15] [18]),
        .Q(\bb_reg_reg[30]_0 [17]));
  FDCE \bb_reg_reg[19] 
       (.C(clk),
        .CE(E),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[15] [19]),
        .Q(\bb_reg_reg[30]_0 [18]));
  FDCE \bb_reg_reg[1] 
       (.C(clk),
        .CE(E),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[15] [1]),
        .Q(\bb_reg_reg[30]_0 [0]));
  FDCE \bb_reg_reg[20] 
       (.C(clk),
        .CE(E),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[15] [20]),
        .Q(\bb_reg_reg[30]_0 [19]));
  FDCE \bb_reg_reg[21] 
       (.C(clk),
        .CE(E),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[15] [21]),
        .Q(\bb_reg_reg[30]_0 [20]));
  FDCE \bb_reg_reg[22] 
       (.C(clk),
        .CE(E),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[15] [22]),
        .Q(\bb_reg_reg[30]_0 [21]));
  FDCE \bb_reg_reg[23] 
       (.C(clk),
        .CE(E),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[15] [23]),
        .Q(\bb_reg_reg[30]_0 [22]));
  FDCE \bb_reg_reg[24] 
       (.C(clk),
        .CE(E),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[15] [24]),
        .Q(\bb_reg_reg[30]_0 [23]));
  FDCE \bb_reg_reg[25] 
       (.C(clk),
        .CE(E),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[15] [25]),
        .Q(\bb_reg_reg[30]_0 [24]));
  FDCE \bb_reg_reg[26] 
       (.C(clk),
        .CE(E),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[15] [26]),
        .Q(\bb_reg_reg[30]_0 [25]));
  FDCE \bb_reg_reg[27] 
       (.C(clk),
        .CE(E),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[15] [27]),
        .Q(\bb_reg_reg[30]_0 [26]));
  FDCE \bb_reg_reg[28] 
       (.C(clk),
        .CE(E),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[15] [28]),
        .Q(\bb_reg_reg[30]_0 [27]));
  FDCE \bb_reg_reg[29] 
       (.C(clk),
        .CE(E),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[15] [29]),
        .Q(\bb_reg_reg[30]_0 [28]));
  FDCE \bb_reg_reg[2] 
       (.C(clk),
        .CE(E),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[15] [2]),
        .Q(\bb_reg_reg[30]_0 [1]));
  FDCE \bb_reg_reg[30] 
       (.C(clk),
        .CE(E),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[15] [30]),
        .Q(\bb_reg_reg[30]_0 [29]));
  FDCE \bb_reg_reg[31] 
       (.C(clk),
        .CE(E),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[15] [31]),
        .Q(\bb_reg_reg[30]_0 [30]));
  FDCE \bb_reg_reg[3] 
       (.C(clk),
        .CE(E),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[15] [3]),
        .Q(\bb_reg_reg[30]_0 [2]));
  FDCE \bb_reg_reg[4] 
       (.C(clk),
        .CE(E),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[15] [4]),
        .Q(\bb_reg_reg[30]_0 [3]));
  FDCE \bb_reg_reg[5] 
       (.C(clk),
        .CE(E),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[15] [5]),
        .Q(\bb_reg_reg[30]_0 [4]));
  FDCE \bb_reg_reg[6] 
       (.C(clk),
        .CE(E),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[15] [6]),
        .Q(\bb_reg_reg[30]_0 [5]));
  FDCE \bb_reg_reg[7] 
       (.C(clk),
        .CE(E),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[15] [7]),
        .Q(\bb_reg_reg[30]_0 [6]));
  FDCE \bb_reg_reg[8] 
       (.C(clk),
        .CE(E),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[15] [8]),
        .Q(\bb_reg_reg[30]_0 [7]));
  FDCE \bb_reg_reg[9] 
       (.C(clk),
        .CE(E),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[15] [9]),
        .Q(\bb_reg_reg[30]_0 [8]));
  (* SOFT_HLUTNM = "soft_lutpair172" *) 
  LUT5 #(
    .INIT(32'h00000010)) 
    \bram_firmware_code_address_reg[10]_i_15 
       (.I0(\lower_reg_reg[30]_0 [8]),
        .I1(\lower_reg_reg[30]_0 [6]),
        .I2(\bram_firmware_code_address_reg[7]_i_12_n_0 ),
        .I3(\lower_reg_reg[30]_0 [7]),
        .I4(\lower_reg_reg[30]_0 [9]),
        .O(\pc_reg_reg[10] ));
  (* SOFT_HLUTNM = "soft_lutpair173" *) 
  LUT5 #(
    .INIT(32'h00000010)) 
    \bram_firmware_code_address_reg[10]_i_18 
       (.I0(\lower_reg_reg[0]_0 [5]),
        .I1(\lower_reg_reg[0]_0 [3]),
        .I2(\pc_reg_reg[6] ),
        .I3(\lower_reg_reg[0]_0 [4]),
        .I4(\lower_reg_reg[0]_0 [6]),
        .O(\pc_reg_reg[10]_0 ));
  LUT6 #(
    .INIT(64'h0000000000000010)) 
    \bram_firmware_code_address_reg[11]_i_14 
       (.I0(\lower_reg_reg[30]_0 [9]),
        .I1(\lower_reg_reg[30]_0 [7]),
        .I2(\bram_firmware_code_address_reg[7]_i_12_n_0 ),
        .I3(\lower_reg_reg[30]_0 [6]),
        .I4(\lower_reg_reg[30]_0 [8]),
        .I5(\lower_reg_reg[30]_0 [10]),
        .O(\pc_reg_reg[11] ));
  (* SOFT_HLUTNM = "soft_lutpair181" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \bram_firmware_code_address_reg[12]_i_15 
       (.I0(\pc_reg_reg[11] ),
        .I1(\lower_reg_reg[30]_0 [11]),
        .O(\pc_reg_reg[12] ));
  (* SOFT_HLUTNM = "soft_lutpair181" *) 
  LUT3 #(
    .INIT(8'h04)) 
    \bram_firmware_code_address_reg[13]_i_15 
       (.I0(\lower_reg_reg[30]_0 [11]),
        .I1(\pc_reg_reg[11] ),
        .I2(\lower_reg_reg[30]_0 [12]),
        .O(\pc_reg_reg[13] ));
  LUT3 #(
    .INIT(8'h04)) 
    \bram_firmware_code_address_reg[13]_i_23 
       (.I0(\lower_reg_reg[0]_0 [8]),
        .I1(\pc_reg_reg[16]_0 ),
        .I2(\lower_reg_reg[0]_0 [9]),
        .O(\pc_reg_reg[13]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair174" *) 
  LUT4 #(
    .INIT(16'h0004)) 
    \bram_firmware_code_address_reg[14]_i_16 
       (.I0(\lower_reg_reg[30]_0 [12]),
        .I1(\pc_reg_reg[11] ),
        .I2(\lower_reg_reg[30]_0 [11]),
        .I3(\lower_reg_reg[30]_0 [13]),
        .O(\pc_reg_reg[14] ));
  (* SOFT_HLUTNM = "soft_lutpair167" *) 
  LUT4 #(
    .INIT(16'h0004)) 
    \bram_firmware_code_address_reg[14]_i_25 
       (.I0(\lower_reg_reg[0]_0 [9]),
        .I1(\pc_reg_reg[16]_0 ),
        .I2(\lower_reg_reg[0]_0 [8]),
        .I3(\lower_reg_reg[0]_0 [10]),
        .O(\pc_reg_reg[14]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair174" *) 
  LUT5 #(
    .INIT(32'h00000010)) 
    \bram_firmware_code_address_reg[15]_i_19 
       (.I0(\lower_reg_reg[30]_0 [13]),
        .I1(\lower_reg_reg[30]_0 [11]),
        .I2(\pc_reg_reg[11] ),
        .I3(\lower_reg_reg[30]_0 [12]),
        .I4(\lower_reg_reg[30]_0 [14]),
        .O(\pc_reg_reg[15] ));
  (* SOFT_HLUTNM = "soft_lutpair167" *) 
  LUT5 #(
    .INIT(32'h00000010)) 
    \bram_firmware_code_address_reg[15]_i_28 
       (.I0(\lower_reg_reg[0]_0 [10]),
        .I1(\lower_reg_reg[0]_0 [8]),
        .I2(\pc_reg_reg[16]_0 ),
        .I3(\lower_reg_reg[0]_0 [9]),
        .I4(\lower_reg_reg[0]_0 [11]),
        .O(\pc_reg_reg[15]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair186" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \bram_firmware_code_address_reg[16]_i_10 
       (.I0(\bram_firmware_code_address_reg[19]_i_26_n_0 ),
        .I1(\lower_reg_reg[30]_0 [16]),
        .O(\bram_firmware_code_address_reg[16]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000010)) 
    \bram_firmware_code_address_reg[16]_i_11 
       (.I0(\lower_reg_reg[0]_0 [11]),
        .I1(\lower_reg_reg[0]_0 [9]),
        .I2(\pc_reg_reg[16]_0 ),
        .I3(\lower_reg_reg[0]_0 [8]),
        .I4(\lower_reg_reg[0]_0 [10]),
        .I5(\lower_reg_reg[0]_0 [12]),
        .O(\pc_reg_reg[16] ));
  LUT6 #(
    .INIT(64'h0000000000000010)) 
    \bram_firmware_code_address_reg[16]_i_15 
       (.I0(\lower_reg_reg[0]_0 [6]),
        .I1(\lower_reg_reg[0]_0 [4]),
        .I2(\pc_reg_reg[6] ),
        .I3(\lower_reg_reg[0]_0 [3]),
        .I4(\lower_reg_reg[0]_0 [5]),
        .I5(\lower_reg_reg[0]_0 [7]),
        .O(\pc_reg_reg[16]_0 ));
  LUT6 #(
    .INIT(64'hAAF0AAF0AAC3AA00)) 
    \bram_firmware_code_address_reg[16]_i_6 
       (.I0(\bram_firmware_code_address_reg[16]_i_10_n_0 ),
        .I1(\pc_reg_reg[16] ),
        .I2(\lower_reg_reg[0]_0 [13]),
        .I3(negate_reg_reg_1),
        .I4(negate_reg_reg_3),
        .I5(negate_reg_reg_4),
        .O(\pc_reg_reg[16]_1 ));
  (* SOFT_HLUTNM = "soft_lutpair186" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \bram_firmware_code_address_reg[17]_i_19 
       (.I0(\bram_firmware_code_address_reg[19]_i_26_n_0 ),
        .I1(\lower_reg_reg[30]_0 [16]),
        .O(\pc_reg_reg[17] ));
  (* SOFT_HLUTNM = "soft_lutpair178" *) 
  LUT4 #(
    .INIT(16'h04FB)) 
    \bram_firmware_code_address_reg[18]_i_11 
       (.I0(\lower_reg_reg[30]_0 [17]),
        .I1(\bram_firmware_code_address_reg[19]_i_26_n_0 ),
        .I2(\lower_reg_reg[30]_0 [16]),
        .I3(\lower_reg_reg[30]_0 [18]),
        .O(\bram_firmware_code_address_reg[18]_i_11_n_0 ));
  LUT3 #(
    .INIT(8'h04)) 
    \bram_firmware_code_address_reg[18]_i_12 
       (.I0(\lower_reg_reg[0]_0 [13]),
        .I1(\pc_reg_reg[16] ),
        .I2(\lower_reg_reg[0]_0 [14]),
        .O(\bram_firmware_code_address_reg[18]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAAF0AAF0AAC3AA00)) 
    \bram_firmware_code_address_reg[18]_i_6 
       (.I0(\bram_firmware_code_address_reg[18]_i_11_n_0 ),
        .I1(\bram_firmware_code_address_reg[18]_i_12_n_0 ),
        .I2(p_1_in51_in),
        .I3(negate_reg_reg_1),
        .I4(negate_reg_reg_3),
        .I5(negate_reg_reg_4),
        .O(\pc_reg_reg[18] ));
  (* SOFT_HLUTNM = "soft_lutpair178" *) 
  LUT5 #(
    .INIT(32'h0010FFEF)) 
    \bram_firmware_code_address_reg[19]_i_18 
       (.I0(\lower_reg_reg[30]_0 [18]),
        .I1(\lower_reg_reg[30]_0 [16]),
        .I2(\bram_firmware_code_address_reg[19]_i_26_n_0 ),
        .I3(\lower_reg_reg[30]_0 [17]),
        .I4(\lower_reg_reg[30]_0 [19]),
        .O(\bram_firmware_code_address_reg[19]_i_18_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair176" *) 
  LUT4 #(
    .INIT(16'h0004)) 
    \bram_firmware_code_address_reg[19]_i_19 
       (.I0(\lower_reg_reg[0]_0 [14]),
        .I1(\pc_reg_reg[16] ),
        .I2(\lower_reg_reg[0]_0 [13]),
        .I3(p_1_in51_in),
        .O(\bram_firmware_code_address_reg[19]_i_19_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000010)) 
    \bram_firmware_code_address_reg[19]_i_26 
       (.I0(\lower_reg_reg[30]_0 [14]),
        .I1(\lower_reg_reg[30]_0 [12]),
        .I2(\pc_reg_reg[11] ),
        .I3(\lower_reg_reg[30]_0 [11]),
        .I4(\lower_reg_reg[30]_0 [13]),
        .I5(\lower_reg_reg[30]_0 [15]),
        .O(\bram_firmware_code_address_reg[19]_i_26_n_0 ));
  LUT6 #(
    .INIT(64'hAAF0AAF0AAC3AA00)) 
    \bram_firmware_code_address_reg[19]_i_9 
       (.I0(\bram_firmware_code_address_reg[19]_i_18_n_0 ),
        .I1(\bram_firmware_code_address_reg[19]_i_19_n_0 ),
        .I2(p_1_in54_in),
        .I3(negate_reg_reg_1),
        .I4(negate_reg_reg_3),
        .I5(negate_reg_reg_4),
        .O(\pc_reg_reg[19] ));
  LUT6 #(
    .INIT(64'hFFFF560000000000)) 
    \bram_firmware_code_address_reg[2]_i_6 
       (.I0(\lower_reg_reg[30]_0 [2]),
        .I1(\lower_reg_reg[30]_0 [0]),
        .I2(\lower_reg_reg[30]_0 [1]),
        .I3(negate_reg_reg_1),
        .I4(\upper_reg_reg[1]_1 ),
        .I5(negate_reg_reg_2),
        .O(\pc_reg_reg[2] ));
  (* SOFT_HLUTNM = "soft_lutpair171" *) 
  LUT4 #(
    .INIT(16'h01FE)) 
    \bram_firmware_code_address_reg[3]_i_11 
       (.I0(\lower_reg_reg[30]_0 [2]),
        .I1(\lower_reg_reg[30]_0 [0]),
        .I2(\lower_reg_reg[30]_0 [1]),
        .I3(\lower_reg_reg[30]_0 [3]),
        .O(\bram_firmware_code_address_reg[3]_i_11_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \bram_firmware_code_address_reg[3]_i_12 
       (.I0(\lower_reg_reg[0]_0 [1]),
        .I1(\lower_reg_reg[0]_0 [0]),
        .I2(\lower_reg_reg[0]_0 [2]),
        .O(\bram_firmware_code_address_reg[3]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hAAF0AAF0AAC3AA00)) 
    \bram_firmware_code_address_reg[3]_i_6 
       (.I0(\bram_firmware_code_address_reg[3]_i_11_n_0 ),
        .I1(\bram_firmware_code_address_reg[3]_i_12_n_0 ),
        .I2(p_1_in6_in),
        .I3(negate_reg_reg_1),
        .I4(negate_reg_reg_3),
        .I5(negate_reg_reg_4),
        .O(\pc_reg_reg[3] ));
  (* SOFT_HLUTNM = "soft_lutpair171" *) 
  LUT5 #(
    .INIT(32'h0001FFFE)) 
    \bram_firmware_code_address_reg[4]_i_12 
       (.I0(\lower_reg_reg[30]_0 [3]),
        .I1(\lower_reg_reg[30]_0 [1]),
        .I2(\lower_reg_reg[30]_0 [0]),
        .I3(\lower_reg_reg[30]_0 [2]),
        .I4(\lower_reg_reg[30]_0 [4]),
        .O(\bram_firmware_code_address_reg[4]_i_12_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair175" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    \bram_firmware_code_address_reg[4]_i_13 
       (.I0(\lower_reg_reg[0]_0 [2]),
        .I1(\lower_reg_reg[0]_0 [0]),
        .I2(\lower_reg_reg[0]_0 [1]),
        .I3(p_1_in6_in),
        .O(\bram_firmware_code_address_reg[4]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hAAF0AAF0AAC3AA00)) 
    \bram_firmware_code_address_reg[4]_i_6 
       (.I0(\bram_firmware_code_address_reg[4]_i_12_n_0 ),
        .I1(\bram_firmware_code_address_reg[4]_i_13_n_0 ),
        .I2(p_1_in9_in),
        .I3(negate_reg_reg_1),
        .I4(negate_reg_reg_3),
        .I5(negate_reg_reg_4),
        .O(\pc_reg_reg[4] ));
  LUT6 #(
    .INIT(64'h00000001FFFFFFFE)) 
    \bram_firmware_code_address_reg[5]_i_12 
       (.I0(\lower_reg_reg[30]_0 [4]),
        .I1(\lower_reg_reg[30]_0 [2]),
        .I2(\lower_reg_reg[30]_0 [0]),
        .I3(\lower_reg_reg[30]_0 [1]),
        .I4(\lower_reg_reg[30]_0 [3]),
        .I5(\lower_reg_reg[30]_0 [5]),
        .O(\bram_firmware_code_address_reg[5]_i_12_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair175" *) 
  LUT5 #(
    .INIT(32'h00000001)) 
    \bram_firmware_code_address_reg[5]_i_13 
       (.I0(p_1_in6_in),
        .I1(\lower_reg_reg[0]_0 [1]),
        .I2(\lower_reg_reg[0]_0 [0]),
        .I3(\lower_reg_reg[0]_0 [2]),
        .I4(p_1_in9_in),
        .O(\bram_firmware_code_address_reg[5]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hAAF0AAF0AAC3AA00)) 
    \bram_firmware_code_address_reg[5]_i_6 
       (.I0(\bram_firmware_code_address_reg[5]_i_12_n_0 ),
        .I1(\bram_firmware_code_address_reg[5]_i_13_n_0 ),
        .I2(p_1_in12_in),
        .I3(negate_reg_reg_1),
        .I4(negate_reg_reg_3),
        .I5(negate_reg_reg_4),
        .O(\pc_reg_reg[5] ));
  (* SOFT_HLUTNM = "soft_lutpair182" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \bram_firmware_code_address_reg[6]_i_12 
       (.I0(\bram_firmware_code_address_reg[7]_i_12_n_0 ),
        .I1(\lower_reg_reg[30]_0 [6]),
        .O(\bram_firmware_code_address_reg[6]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \bram_firmware_code_address_reg[6]_i_13 
       (.I0(p_1_in9_in),
        .I1(\lower_reg_reg[0]_0 [2]),
        .I2(\lower_reg_reg[0]_0 [0]),
        .I3(\lower_reg_reg[0]_0 [1]),
        .I4(p_1_in6_in),
        .I5(p_1_in12_in),
        .O(\pc_reg_reg[6] ));
  LUT6 #(
    .INIT(64'hAAF0AAF0AAC3AA00)) 
    \bram_firmware_code_address_reg[6]_i_6 
       (.I0(\bram_firmware_code_address_reg[6]_i_12_n_0 ),
        .I1(\pc_reg_reg[6] ),
        .I2(\lower_reg_reg[0]_0 [3]),
        .I3(negate_reg_reg_1),
        .I4(negate_reg_reg_3),
        .I5(negate_reg_reg_4),
        .O(\pc_reg_reg[6]_0 ));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \bram_firmware_code_address_reg[7]_i_12 
       (.I0(\lower_reg_reg[30]_0 [4]),
        .I1(\lower_reg_reg[30]_0 [2]),
        .I2(\lower_reg_reg[30]_0 [0]),
        .I3(\lower_reg_reg[30]_0 [1]),
        .I4(\lower_reg_reg[30]_0 [3]),
        .I5(\lower_reg_reg[30]_0 [5]),
        .O(\bram_firmware_code_address_reg[7]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF590000000000)) 
    \bram_firmware_code_address_reg[7]_i_6 
       (.I0(\lower_reg_reg[30]_0 [7]),
        .I1(\bram_firmware_code_address_reg[7]_i_12_n_0 ),
        .I2(\lower_reg_reg[30]_0 [6]),
        .I3(negate_reg_reg_1),
        .I4(\upper_reg_reg[6]_1 ),
        .I5(negate_reg_reg_2),
        .O(\pc_reg_reg[7] ));
  (* SOFT_HLUTNM = "soft_lutpair182" *) 
  LUT3 #(
    .INIT(8'h04)) 
    \bram_firmware_code_address_reg[8]_i_14 
       (.I0(\lower_reg_reg[30]_0 [6]),
        .I1(\bram_firmware_code_address_reg[7]_i_12_n_0 ),
        .I2(\lower_reg_reg[30]_0 [7]),
        .O(\pc_reg_reg[8] ));
  LUT3 #(
    .INIT(8'h04)) 
    \bram_firmware_code_address_reg[8]_i_17 
       (.I0(\lower_reg_reg[0]_0 [3]),
        .I1(\pc_reg_reg[6] ),
        .I2(\lower_reg_reg[0]_0 [4]),
        .O(\pc_reg_reg[8]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair172" *) 
  LUT4 #(
    .INIT(16'h0004)) 
    \bram_firmware_code_address_reg[9]_i_15 
       (.I0(\lower_reg_reg[30]_0 [7]),
        .I1(\bram_firmware_code_address_reg[7]_i_12_n_0 ),
        .I2(\lower_reg_reg[30]_0 [6]),
        .I3(\lower_reg_reg[30]_0 [8]),
        .O(\pc_reg_reg[9] ));
  (* SOFT_HLUTNM = "soft_lutpair173" *) 
  LUT4 #(
    .INIT(16'h0004)) 
    \bram_firmware_code_address_reg[9]_i_18 
       (.I0(\lower_reg_reg[0]_0 [4]),
        .I1(\pc_reg_reg[6] ),
        .I2(\lower_reg_reg[0]_0 [3]),
        .I3(\lower_reg_reg[0]_0 [5]),
        .O(\pc_reg_reg[9]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair168" *) 
  LUT4 #(
    .INIT(16'h8882)) 
    \count_reg[2]_i_1 
       (.I0(\opcode_reg_reg[30] ),
        .I1(\count_reg_reg_n_0_[2] ),
        .I2(Q[0]),
        .I3(Q[1]),
        .O(\count_reg[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair168" *) 
  LUT5 #(
    .INIT(32'h88888882)) 
    \count_reg[3]_i_1 
       (.I0(\opcode_reg_reg[30] ),
        .I1(\count_reg_reg_n_0_[3] ),
        .I2(\count_reg_reg_n_0_[2] ),
        .I3(Q[0]),
        .I4(Q[1]),
        .O(\count_reg[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8888888888888882)) 
    \count_reg[4]_i_1 
       (.I0(\opcode_reg_reg[30] ),
        .I1(\count_reg_reg_n_0_[4] ),
        .I2(Q[1]),
        .I3(Q[0]),
        .I4(\count_reg_reg_n_0_[2] ),
        .I5(\count_reg_reg_n_0_[3] ),
        .O(\count_reg[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h00000001FFFFFFFE)) 
    \count_reg[5]_i_10 
       (.I0(\count_reg_reg_n_0_[4] ),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(\count_reg_reg_n_0_[2] ),
        .I4(\count_reg_reg_n_0_[3] ),
        .I5(\count_reg_reg_n_0_[5] ),
        .O(\count_reg_reg[5]_0 ));
  LUT5 #(
    .INIT(32'hBFFFFFFF)) 
    \count_reg[5]_i_3 
       (.I0(reset),
        .I1(\reset_reg_reg[3] [3]),
        .I2(\reset_reg_reg[3] [2]),
        .I3(\reset_reg_reg[3] [0]),
        .I4(\reset_reg_reg[3] [1]),
        .O(reset_in1_out));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \count_reg[5]_i_7 
       (.I0(\count_reg_reg_n_0_[2] ),
        .I1(Q[0]),
        .I2(Q[1]),
        .I3(\count_reg_reg_n_0_[4] ),
        .I4(\count_reg_reg_n_0_[3] ),
        .I5(\count_reg_reg_n_0_[5] ),
        .O(neqOp0_in));
  FDCE \count_reg_reg[0] 
       (.C(clk),
        .CE(E),
        .CLR(reset_in1_out),
        .D(D[0]),
        .Q(Q[0]));
  FDCE \count_reg_reg[1] 
       (.C(clk),
        .CE(E),
        .CLR(reset_in1_out),
        .D(D[1]),
        .Q(Q[1]));
  FDCE \count_reg_reg[2] 
       (.C(clk),
        .CE(E),
        .CLR(reset_in1_out),
        .D(\count_reg[2]_i_1_n_0 ),
        .Q(\count_reg_reg_n_0_[2] ));
  FDCE \count_reg_reg[3] 
       (.C(clk),
        .CE(E),
        .CLR(reset_in1_out),
        .D(\count_reg[3]_i_1_n_0 ),
        .Q(\count_reg_reg_n_0_[3] ));
  FDCE \count_reg_reg[4] 
       (.C(clk),
        .CE(E),
        .CLR(reset_in1_out),
        .D(\count_reg[4]_i_1_n_0 ),
        .Q(\count_reg_reg_n_0_[4] ));
  FDCE \count_reg_reg[5] 
       (.C(clk),
        .CE(E),
        .CLR(reset_in1_out),
        .D(D[2]),
        .Q(\count_reg_reg_n_0_[5] ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \lower_reg[0]_i_10 
       (.I0(\aa_reg_reg[30]_0 [9]),
        .I1(\aa_reg_reg[30]_0 [10]),
        .I2(\aa_reg_reg[30]_0 [7]),
        .I3(\aa_reg_reg[30]_0 [8]),
        .O(\lower_reg[0]_i_10_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \lower_reg[0]_i_11 
       (.I0(\aa_reg_reg[30]_0 [4]),
        .I1(\aa_reg_reg[30]_0 [3]),
        .I2(\aa_reg_reg[30]_0 [6]),
        .I3(\aa_reg_reg[30]_0 [5]),
        .I4(\lower_reg[0]_i_16_n_0 ),
        .O(\lower_reg[0]_i_11_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \lower_reg[0]_i_12 
       (.I0(\bb_reg_reg[30]_0 [21]),
        .I1(\bb_reg_reg[30]_0 [20]),
        .I2(\bb_reg_reg[30]_0 [23]),
        .I3(\bb_reg_reg[30]_0 [22]),
        .I4(\lower_reg[0]_i_17_n_0 ),
        .O(\lower_reg[0]_i_12_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \lower_reg[0]_i_13 
       (.I0(\bb_reg_reg[30]_0 [26]),
        .I1(\bb_reg_reg[30]_0 [27]),
        .I2(\bb_reg_reg[30]_0 [24]),
        .I3(\bb_reg_reg[30]_0 [25]),
        .O(\lower_reg[0]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \lower_reg[0]_i_14 
       (.I0(\lower_reg[0]_i_18_n_0 ),
        .I1(\bb_reg_reg[30]_0 [14]),
        .I2(\bb_reg_reg[30]_0 [15]),
        .I3(\bb_reg_reg[30]_0 [12]),
        .I4(\bb_reg_reg[30]_0 [13]),
        .I5(\lower_reg[0]_i_19_n_0 ),
        .O(\lower_reg[0]_i_14_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \lower_reg[0]_i_15 
       (.I0(\aa_reg_reg[30]_0 [17]),
        .I1(\aa_reg_reg[30]_0 [18]),
        .I2(\aa_reg_reg[30]_0 [15]),
        .I3(\aa_reg_reg[30]_0 [16]),
        .O(\lower_reg[0]_i_15_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \lower_reg[0]_i_16 
       (.I0(\aa_reg_reg[30]_0 [1]),
        .I1(\aa_reg_reg[30]_0 [2]),
        .I2(\aa_reg_reg_n_0_[0] ),
        .I3(\aa_reg_reg[30]_0 [0]),
        .O(\lower_reg[0]_i_16_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \lower_reg[0]_i_17 
       (.I0(\bb_reg_reg[30]_0 [18]),
        .I1(\bb_reg_reg[30]_0 [19]),
        .I2(\bb_reg_reg[30]_0 [16]),
        .I3(\bb_reg_reg[30]_0 [17]),
        .O(\lower_reg[0]_i_17_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \lower_reg[0]_i_18 
       (.I0(\bb_reg_reg[30]_0 [10]),
        .I1(\bb_reg_reg[30]_0 [11]),
        .I2(\bb_reg_reg[30]_0 [8]),
        .I3(\bb_reg_reg[30]_0 [9]),
        .O(\lower_reg[0]_i_18_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \lower_reg[0]_i_19 
       (.I0(\bb_reg_reg[30]_0 [5]),
        .I1(\bb_reg_reg[30]_0 [4]),
        .I2(\bb_reg_reg[30]_0 [7]),
        .I3(\bb_reg_reg[30]_0 [6]),
        .I4(\lower_reg[0]_i_20_n_0 ),
        .O(\lower_reg[0]_i_19_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \lower_reg[0]_i_20 
       (.I0(\bb_reg_reg[30]_0 [2]),
        .I1(\bb_reg_reg[30]_0 [3]),
        .I2(\bb_reg_reg[30]_0 [0]),
        .I3(\bb_reg_reg[30]_0 [1]),
        .O(\lower_reg[0]_i_20_n_0 ));
  LUT4 #(
    .INIT(16'h00E0)) 
    \lower_reg[0]_i_3 
       (.I0(\lower_reg[0]_i_4_n_0 ),
        .I1(\lower_reg[0]_i_5_n_0 ),
        .I2(eqOp__0),
        .I3(p_0_in342_in),
        .O(lower_reg0__62));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \lower_reg[0]_i_4 
       (.I0(\lower_reg[0]_i_8_n_0 ),
        .I1(\aa_reg_reg[30]_0 [30]),
        .I2(\aa_reg_reg[30]_0 [29]),
        .I3(\aa_reg_reg[30]_0 [27]),
        .I4(\aa_reg_reg[30]_0 [28]),
        .I5(\lower_reg[0]_i_9_n_0 ),
        .O(\lower_reg[0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \lower_reg[0]_i_5 
       (.I0(\lower_reg[0]_i_10_n_0 ),
        .I1(\aa_reg_reg[30]_0 [13]),
        .I2(\aa_reg_reg[30]_0 [14]),
        .I3(\aa_reg_reg[30]_0 [11]),
        .I4(\aa_reg_reg[30]_0 [12]),
        .I5(\lower_reg[0]_i_11_n_0 ),
        .O(\lower_reg[0]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \lower_reg[0]_i_6 
       (.I0(\lower_reg[0]_i_12_n_0 ),
        .I1(\bb_reg_reg[30]_0 [30]),
        .I2(\bb_reg_reg[30]_0 [28]),
        .I3(\bb_reg_reg[30]_0 [29]),
        .I4(\lower_reg[0]_i_13_n_0 ),
        .I5(\lower_reg[0]_i_14_n_0 ),
        .O(eqOp__0));
  LUT6 #(
    .INIT(64'hE4EDE484ED8D848D)) 
    \lower_reg[0]_i_7 
       (.I0(\lower_reg_reg[0]_0 [23]),
        .I1(\aa_reg_reg[30]_0 [30]),
        .I2(\aa_reg_reg[31]_0 ),
        .I3(\lower_reg_reg[0]_0 [22]),
        .I4(\aa_reg_reg[30]_0 [29]),
        .I5(bv_adder5__3),
        .O(p_0_in342_in));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \lower_reg[0]_i_8 
       (.I0(\aa_reg_reg[30]_0 [25]),
        .I1(\aa_reg_reg[30]_0 [26]),
        .I2(\aa_reg_reg[30]_0 [23]),
        .I3(\aa_reg_reg[30]_0 [24]),
        .O(\lower_reg[0]_i_8_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \lower_reg[0]_i_9 
       (.I0(\aa_reg_reg[30]_0 [20]),
        .I1(\aa_reg_reg[30]_0 [19]),
        .I2(\aa_reg_reg[30]_0 [22]),
        .I3(\aa_reg_reg[30]_0 [21]),
        .I4(\lower_reg[0]_i_15_n_0 ),
        .O(\lower_reg[0]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'h2EE2E2E200000000)) 
    \lower_reg[31]_i_5 
       (.I0(\lower_reg_reg[30]_0 [30]),
        .I1(\aa_reg_reg[31]_0 ),
        .I2(\lower_reg_reg[0]_0 [0]),
        .I3(\bb_reg_reg_n_0_[0] ),
        .I4(\aa_reg_reg_n_0_[0] ),
        .I5(\opcode_reg_reg[30]_0 ),
        .O(\lower_reg_reg[31]_0 ));
  FDCE \lower_reg_reg[0] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_3 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_4 [0]),
        .Q(\lower_reg_reg[30]_0 [0]));
  FDCE \lower_reg_reg[10] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_3 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_4 [10]),
        .Q(\lower_reg_reg[30]_0 [10]));
  FDCE \lower_reg_reg[11] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_3 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_4 [11]),
        .Q(\lower_reg_reg[30]_0 [11]));
  FDCE \lower_reg_reg[12] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_3 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_4 [12]),
        .Q(\lower_reg_reg[30]_0 [12]));
  FDCE \lower_reg_reg[13] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_3 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_4 [13]),
        .Q(\lower_reg_reg[30]_0 [13]));
  FDCE \lower_reg_reg[14] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_3 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_4 [14]),
        .Q(\lower_reg_reg[30]_0 [14]));
  FDCE \lower_reg_reg[15] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_3 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_4 [15]),
        .Q(\lower_reg_reg[30]_0 [15]));
  FDCE \lower_reg_reg[16] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_3 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_4 [16]),
        .Q(\lower_reg_reg[30]_0 [16]));
  FDCE \lower_reg_reg[17] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_3 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_4 [17]),
        .Q(\lower_reg_reg[30]_0 [17]));
  FDCE \lower_reg_reg[18] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_3 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_4 [18]),
        .Q(\lower_reg_reg[30]_0 [18]));
  FDCE \lower_reg_reg[19] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_3 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_4 [19]),
        .Q(\lower_reg_reg[30]_0 [19]));
  FDCE \lower_reg_reg[1] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_3 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_4 [1]),
        .Q(\lower_reg_reg[30]_0 [1]));
  FDCE \lower_reg_reg[20] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_3 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_4 [20]),
        .Q(\lower_reg_reg[30]_0 [20]));
  FDCE \lower_reg_reg[21] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_3 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_4 [21]),
        .Q(\lower_reg_reg[30]_0 [21]));
  FDCE \lower_reg_reg[22] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_3 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_4 [22]),
        .Q(\lower_reg_reg[30]_0 [22]));
  FDCE \lower_reg_reg[23] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_3 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_4 [23]),
        .Q(\lower_reg_reg[30]_0 [23]));
  FDCE \lower_reg_reg[24] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_3 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_4 [24]),
        .Q(\lower_reg_reg[30]_0 [24]));
  FDCE \lower_reg_reg[25] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_3 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_4 [25]),
        .Q(\lower_reg_reg[30]_0 [25]));
  FDCE \lower_reg_reg[26] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_3 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_4 [26]),
        .Q(\lower_reg_reg[30]_0 [26]));
  FDCE \lower_reg_reg[27] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_3 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_4 [27]),
        .Q(\lower_reg_reg[30]_0 [27]));
  FDCE \lower_reg_reg[28] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_3 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_4 [28]),
        .Q(\lower_reg_reg[30]_0 [28]));
  FDCE \lower_reg_reg[29] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_3 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_4 [29]),
        .Q(\lower_reg_reg[30]_0 [29]));
  FDCE \lower_reg_reg[2] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_3 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_4 [2]),
        .Q(\lower_reg_reg[30]_0 [2]));
  FDCE \lower_reg_reg[30] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_3 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_4 [30]),
        .Q(\lower_reg_reg[30]_0 [30]));
  FDCE \lower_reg_reg[31] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_3 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_4 [31]),
        .Q(\lower_reg_reg[30]_0 [31]));
  FDCE \lower_reg_reg[3] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_3 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_4 [3]),
        .Q(\lower_reg_reg[30]_0 [3]));
  FDCE \lower_reg_reg[4] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_3 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_4 [4]),
        .Q(\lower_reg_reg[30]_0 [4]));
  FDCE \lower_reg_reg[5] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_3 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_4 [5]),
        .Q(\lower_reg_reg[30]_0 [5]));
  FDCE \lower_reg_reg[6] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_3 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_4 [6]),
        .Q(\lower_reg_reg[30]_0 [6]));
  FDCE \lower_reg_reg[7] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_3 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_4 [7]),
        .Q(\lower_reg_reg[30]_0 [7]));
  FDCE \lower_reg_reg[8] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_3 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_4 [8]),
        .Q(\lower_reg_reg[30]_0 [8]));
  FDCE \lower_reg_reg[9] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_3 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_4 [9]),
        .Q(\lower_reg_reg[30]_0 [9]));
  FDCE mode_reg_reg
       (.C(clk),
        .CE(1'b1),
        .CLR(reset_in1_out),
        .D(mode_reg_reg_0),
        .Q(\aa_reg_reg[31]_0 ));
  FDCE negate_reg_reg
       (.C(clk),
        .CE(1'b1),
        .CLR(reset_in1_out),
        .D(negate_reg_reg_0),
        .Q(negate_reg));
  LUT6 #(
    .INIT(64'h00000010FFFFFFEF)) 
    \pc_reg[20]_i_10 
       (.I0(\lower_reg_reg[30]_0 [19]),
        .I1(\lower_reg_reg[30]_0 [17]),
        .I2(\bram_firmware_code_address_reg[19]_i_26_n_0 ),
        .I3(\lower_reg_reg[30]_0 [16]),
        .I4(\lower_reg_reg[30]_0 [18]),
        .I5(\lower_reg_reg[30]_0 [20]),
        .O(\pc_reg[20]_i_10_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair176" *) 
  LUT5 #(
    .INIT(32'h00000010)) 
    \pc_reg[20]_i_11 
       (.I0(p_1_in51_in),
        .I1(\lower_reg_reg[0]_0 [13]),
        .I2(\pc_reg_reg[16] ),
        .I3(\lower_reg_reg[0]_0 [14]),
        .I4(p_1_in54_in),
        .O(\pc_reg[20]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hAAF0AAF0AAC3AA00)) 
    \pc_reg[20]_i_6 
       (.I0(\pc_reg[20]_i_10_n_0 ),
        .I1(\pc_reg[20]_i_11_n_0 ),
        .I2(p_1_in57_in),
        .I3(negate_reg_reg_1),
        .I4(negate_reg_reg_3),
        .I5(negate_reg_reg_4),
        .O(\pc_reg_reg[20] ));
  LUT6 #(
    .INIT(64'h0000000000000010)) 
    \pc_reg[21]_i_10 
       (.I0(p_1_in54_in),
        .I1(\lower_reg_reg[0]_0 [14]),
        .I2(\pc_reg_reg[16] ),
        .I3(\lower_reg_reg[0]_0 [13]),
        .I4(p_1_in51_in),
        .I5(p_1_in57_in),
        .O(\pc_reg_reg[21] ));
  LUT6 #(
    .INIT(64'hAAF0AAF0AAC3AA00)) 
    \pc_reg[21]_i_6 
       (.I0(\pc_reg[21]_i_9_n_0 ),
        .I1(\pc_reg_reg[21] ),
        .I2(\lower_reg_reg[0]_0 [15]),
        .I3(negate_reg_reg_1),
        .I4(negate_reg_reg_3),
        .I5(negate_reg_reg_4),
        .O(\pc_reg_reg[21]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair185" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \pc_reg[21]_i_9 
       (.I0(\pc_reg[25]_i_13_n_0 ),
        .I1(\lower_reg_reg[30]_0 [21]),
        .O(\pc_reg[21]_i_9_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair185" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \pc_reg[22]_i_11 
       (.I0(\pc_reg[25]_i_13_n_0 ),
        .I1(\lower_reg_reg[30]_0 [21]),
        .O(\pc_reg_reg[22] ));
  (* SOFT_HLUTNM = "soft_lutpair180" *) 
  LUT4 #(
    .INIT(16'h04FB)) 
    \pc_reg[23]_i_10 
       (.I0(\lower_reg_reg[30]_0 [22]),
        .I1(\pc_reg[25]_i_13_n_0 ),
        .I2(\lower_reg_reg[30]_0 [21]),
        .I3(\lower_reg_reg[30]_0 [23]),
        .O(\pc_reg[23]_i_10_n_0 ));
  LUT3 #(
    .INIT(8'h04)) 
    \pc_reg[23]_i_11 
       (.I0(\lower_reg_reg[0]_0 [15]),
        .I1(\pc_reg_reg[21] ),
        .I2(\lower_reg_reg[0]_0 [16]),
        .O(\pc_reg[23]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hAAF0AAF0AAC3AA00)) 
    \pc_reg[23]_i_6 
       (.I0(\pc_reg[23]_i_10_n_0 ),
        .I1(\pc_reg[23]_i_11_n_0 ),
        .I2(p_1_in66_in),
        .I3(negate_reg_reg_1),
        .I4(negate_reg_reg_3),
        .I5(negate_reg_reg_4),
        .O(\pc_reg_reg[23] ));
  (* SOFT_HLUTNM = "soft_lutpair180" *) 
  LUT4 #(
    .INIT(16'h0004)) 
    \pc_reg[24]_i_14 
       (.I0(\lower_reg_reg[30]_0 [22]),
        .I1(\pc_reg[25]_i_13_n_0 ),
        .I2(\lower_reg_reg[30]_0 [21]),
        .I3(\lower_reg_reg[30]_0 [23]),
        .O(\pc_reg_reg[24] ));
  (* SOFT_HLUTNM = "soft_lutpair169" *) 
  LUT4 #(
    .INIT(16'h0004)) 
    \pc_reg[24]_i_17 
       (.I0(\lower_reg_reg[0]_0 [16]),
        .I1(\pc_reg_reg[21] ),
        .I2(\lower_reg_reg[0]_0 [15]),
        .I3(p_1_in66_in),
        .O(\pc_reg_reg[24]_0 ));
  LUT6 #(
    .INIT(64'h00000010FFFFFFEF)) 
    \pc_reg[25]_i_10 
       (.I0(\lower_reg_reg[30]_0 [24]),
        .I1(\lower_reg_reg[30]_0 [22]),
        .I2(\pc_reg[25]_i_13_n_0 ),
        .I3(\lower_reg_reg[30]_0 [21]),
        .I4(\lower_reg_reg[30]_0 [23]),
        .I5(\lower_reg_reg[30]_0 [25]),
        .O(\pc_reg[25]_i_10_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair169" *) 
  LUT5 #(
    .INIT(32'h00000010)) 
    \pc_reg[25]_i_11 
       (.I0(p_1_in66_in),
        .I1(\lower_reg_reg[0]_0 [15]),
        .I2(\pc_reg_reg[21] ),
        .I3(\lower_reg_reg[0]_0 [16]),
        .I4(\lower_reg_reg[0]_0 [17]),
        .O(\pc_reg[25]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000010)) 
    \pc_reg[25]_i_13 
       (.I0(\lower_reg_reg[30]_0 [19]),
        .I1(\lower_reg_reg[30]_0 [17]),
        .I2(\bram_firmware_code_address_reg[19]_i_26_n_0 ),
        .I3(\lower_reg_reg[30]_0 [16]),
        .I4(\lower_reg_reg[30]_0 [18]),
        .I5(\lower_reg_reg[30]_0 [20]),
        .O(\pc_reg[25]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hAAF0AAF0AAC3AA00)) 
    \pc_reg[25]_i_6 
       (.I0(\pc_reg[25]_i_10_n_0 ),
        .I1(\pc_reg[25]_i_11_n_0 ),
        .I2(p_1_in72_in),
        .I3(negate_reg_reg_1),
        .I4(negate_reg_reg_3),
        .I5(negate_reg_reg_4),
        .O(\pc_reg_reg[25] ));
  (* SOFT_HLUTNM = "soft_lutpair184" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \pc_reg[27]_i_14 
       (.I0(\pc_reg_reg[31] ),
        .I1(\lower_reg_reg[30]_0 [26]),
        .O(\pc_reg_reg[27] ));
  (* SOFT_HLUTNM = "soft_lutpair184" *) 
  LUT3 #(
    .INIT(8'h04)) 
    \pc_reg[28]_i_14 
       (.I0(\lower_reg_reg[30]_0 [26]),
        .I1(\pc_reg_reg[31] ),
        .I2(\lower_reg_reg[30]_0 [27]),
        .O(\pc_reg_reg[28] ));
  LUT3 #(
    .INIT(8'h04)) 
    \pc_reg[28]_i_21 
       (.I0(\lower_reg_reg[0]_0 [18]),
        .I1(\pc_reg_reg[31]_2 ),
        .I2(\lower_reg_reg[0]_0 [19]),
        .O(\pc_reg_reg[28]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair177" *) 
  LUT4 #(
    .INIT(16'h0004)) 
    \pc_reg[29]_i_12 
       (.I0(\lower_reg_reg[30]_0 [27]),
        .I1(\pc_reg_reg[31] ),
        .I2(\lower_reg_reg[30]_0 [26]),
        .I3(\lower_reg_reg[30]_0 [28]),
        .O(\pc_reg_reg[29] ));
  (* SOFT_HLUTNM = "soft_lutpair170" *) 
  LUT4 #(
    .INIT(16'h0004)) 
    \pc_reg[29]_i_17 
       (.I0(\lower_reg_reg[0]_0 [19]),
        .I1(\pc_reg_reg[31]_2 ),
        .I2(\lower_reg_reg[0]_0 [18]),
        .I3(\lower_reg_reg[0]_0 [20]),
        .O(\pc_reg_reg[29]_0 ));
  LUT6 #(
    .INIT(64'hFFFF590000000000)) 
    \pc_reg[31]_i_15 
       (.I0(\lower_reg_reg[30]_0 [31]),
        .I1(\pc_reg_reg[31]_0 ),
        .I2(\lower_reg_reg[30]_0 [30]),
        .I3(negate_reg_reg_1),
        .I4(\upper_reg_reg[30]_1 ),
        .I5(negate_reg_reg_2),
        .O(\pc_reg_reg[31]_3 ));
  (* SOFT_HLUTNM = "soft_lutpair177" *) 
  LUT5 #(
    .INIT(32'h00000010)) 
    \pc_reg[31]_i_26 
       (.I0(\lower_reg_reg[30]_0 [28]),
        .I1(\lower_reg_reg[30]_0 [26]),
        .I2(\pc_reg_reg[31] ),
        .I3(\lower_reg_reg[30]_0 [27]),
        .I4(\lower_reg_reg[30]_0 [29]),
        .O(\pc_reg_reg[31]_0 ));
  LUT6 #(
    .INIT(64'h0000000000000010)) 
    \pc_reg[31]_i_32 
       (.I0(\lower_reg_reg[30]_0 [24]),
        .I1(\lower_reg_reg[30]_0 [22]),
        .I2(\pc_reg[25]_i_13_n_0 ),
        .I3(\lower_reg_reg[30]_0 [21]),
        .I4(\lower_reg_reg[30]_0 [23]),
        .I5(\lower_reg_reg[30]_0 [25]),
        .O(\pc_reg_reg[31] ));
  (* SOFT_HLUTNM = "soft_lutpair170" *) 
  LUT5 #(
    .INIT(32'h00000010)) 
    \pc_reg[31]_i_33 
       (.I0(\lower_reg_reg[0]_0 [20]),
        .I1(\lower_reg_reg[0]_0 [18]),
        .I2(\pc_reg_reg[31]_2 ),
        .I3(\lower_reg_reg[0]_0 [19]),
        .I4(\lower_reg_reg[0]_0 [21]),
        .O(\pc_reg_reg[31]_1 ));
  LUT6 #(
    .INIT(64'h0000000000000010)) 
    \pc_reg[31]_i_34 
       (.I0(\lower_reg_reg[0]_0 [17]),
        .I1(\lower_reg_reg[0]_0 [16]),
        .I2(\pc_reg_reg[21] ),
        .I3(\lower_reg_reg[0]_0 [15]),
        .I4(p_1_in66_in),
        .I5(p_1_in72_in),
        .O(\pc_reg_reg[31]_2 ));
  FDCE sign2_reg_reg
       (.C(clk),
        .CE(1'b1),
        .CLR(reset_in1_out),
        .D(sign_reg_reg_2),
        .Q(sign2_reg_reg_0));
  (* SOFT_HLUTNM = "soft_lutpair183" *) 
  LUT3 #(
    .INIT(8'h80)) 
    sign_reg_i_8
       (.I0(neqOp0_in),
        .I1(\bb_reg_reg_n_0_[0] ),
        .I2(\aa_reg_reg[31]_0 ),
        .O(sign_reg1_out));
  FDCE sign_reg_reg
       (.C(clk),
        .CE(1'b1),
        .CLR(reset_in1_out),
        .D(sign_reg_reg_1),
        .Q(sign_reg_reg_0));
  LUT6 #(
    .INIT(64'hB40078FF78FF7800)) 
    \upper_reg[0]_i_3 
       (.I0(\aa_reg_reg[30]_0 [0]),
        .I1(\bb_reg_reg_n_0_[0] ),
        .I2(\lower_reg_reg[0]_0 [1]),
        .I3(\aa_reg_reg[31]_0 ),
        .I4(\aa_reg_reg_n_0_[0] ),
        .I5(\lower_reg_reg[0]_0 [0]),
        .O(\upper_reg_reg[0]_0 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \upper_reg[10]_i_4 
       (.I0(bv_adder031_out),
        .I1(\bb_reg_reg_n_0_[0] ),
        .I2(\lower_reg_reg[0]_0 [8]),
        .I3(\aa_reg_reg[31]_0 ),
        .I4(bv_adder028_out),
        .O(\upper_reg_reg[10]_0 ));
  LUT4 #(
    .INIT(16'h9669)) 
    \upper_reg[10]_i_5 
       (.I0(\aa_reg_reg[30]_0 [9]),
        .I1(\aa_reg_reg[31]_0 ),
        .I2(\lower_reg_reg[0]_0 [7]),
        .I3(bv_adder45__3),
        .O(bv_adder028_out));
  LUT6 #(
    .INIT(64'h9F60FFFF9F600000)) 
    \upper_reg[11]_i_4 
       (.I0(\aa_reg_reg[30]_0 [11]),
        .I1(bv_adder41__3),
        .I2(\bb_reg_reg_n_0_[0] ),
        .I3(\lower_reg_reg[0]_0 [9]),
        .I4(\aa_reg_reg[31]_0 ),
        .I5(bv_adder031_out),
        .O(\upper_reg_reg[11]_0 ));
  LUT6 #(
    .INIT(64'hFFB2E8FFE80000B2)) 
    \upper_reg[11]_i_5 
       (.I0(bv_adder45__3),
        .I1(\aa_reg_reg[30]_0 [9]),
        .I2(\lower_reg_reg[0]_0 [7]),
        .I3(\aa_reg_reg[31]_0 ),
        .I4(\aa_reg_reg[30]_0 [10]),
        .I5(\lower_reg_reg[0]_0 [8]),
        .O(bv_adder41__3));
  LUT6 #(
    .INIT(64'h9969966696996669)) 
    \upper_reg[11]_i_6 
       (.I0(\aa_reg_reg[30]_0 [10]),
        .I1(\lower_reg_reg[0]_0 [8]),
        .I2(bv_adder45__3),
        .I3(\aa_reg_reg[31]_0 ),
        .I4(\aa_reg_reg[30]_0 [9]),
        .I5(\lower_reg_reg[0]_0 [7]),
        .O(bv_adder031_out));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \upper_reg[12]_i_4 
       (.I0(bv_adder037_out),
        .I1(\bb_reg_reg_n_0_[0] ),
        .I2(\lower_reg_reg[0]_0 [10]),
        .I3(\aa_reg_reg[31]_0 ),
        .I4(bv_adder034_out),
        .O(\upper_reg_reg[12]_0 ));
  LUT4 #(
    .INIT(16'h9669)) 
    \upper_reg[12]_i_5 
       (.I0(\aa_reg_reg[30]_0 [11]),
        .I1(\aa_reg_reg[31]_0 ),
        .I2(\lower_reg_reg[0]_0 [9]),
        .I3(bv_adder41__3),
        .O(bv_adder034_out));
  LUT6 #(
    .INIT(64'h9F60FFFF9F600000)) 
    \upper_reg[13]_i_4 
       (.I0(\aa_reg_reg[30]_0 [13]),
        .I1(bv_adder37__3),
        .I2(\bb_reg_reg_n_0_[0] ),
        .I3(\lower_reg_reg[0]_0 [11]),
        .I4(\aa_reg_reg[31]_0 ),
        .I5(bv_adder037_out),
        .O(\upper_reg_reg[13]_0 ));
  LUT6 #(
    .INIT(64'hFFB2E8FFE80000B2)) 
    \upper_reg[13]_i_5 
       (.I0(bv_adder41__3),
        .I1(\aa_reg_reg[30]_0 [11]),
        .I2(\lower_reg_reg[0]_0 [9]),
        .I3(\aa_reg_reg[31]_0 ),
        .I4(\aa_reg_reg[30]_0 [12]),
        .I5(\lower_reg_reg[0]_0 [10]),
        .O(bv_adder37__3));
  LUT6 #(
    .INIT(64'h9969966696996669)) 
    \upper_reg[13]_i_6 
       (.I0(\aa_reg_reg[30]_0 [12]),
        .I1(\lower_reg_reg[0]_0 [10]),
        .I2(bv_adder41__3),
        .I3(\aa_reg_reg[31]_0 ),
        .I4(\aa_reg_reg[30]_0 [11]),
        .I5(\lower_reg_reg[0]_0 [9]),
        .O(bv_adder037_out));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \upper_reg[14]_i_4 
       (.I0(bv_adder043_out),
        .I1(\bb_reg_reg_n_0_[0] ),
        .I2(\lower_reg_reg[0]_0 [12]),
        .I3(\aa_reg_reg[31]_0 ),
        .I4(bv_adder040_out),
        .O(\upper_reg_reg[14]_0 ));
  LUT4 #(
    .INIT(16'h9669)) 
    \upper_reg[14]_i_5 
       (.I0(\aa_reg_reg[30]_0 [13]),
        .I1(\aa_reg_reg[31]_0 ),
        .I2(\lower_reg_reg[0]_0 [11]),
        .I3(bv_adder37__3),
        .O(bv_adder040_out));
  LUT6 #(
    .INIT(64'h9F60FFFF9F600000)) 
    \upper_reg[15]_i_4 
       (.I0(\aa_reg_reg[30]_0 [15]),
        .I1(bv_adder33__3),
        .I2(\bb_reg_reg_n_0_[0] ),
        .I3(\lower_reg_reg[0]_0 [13]),
        .I4(\aa_reg_reg[31]_0 ),
        .I5(bv_adder043_out),
        .O(\upper_reg_reg[15]_0 ));
  LUT6 #(
    .INIT(64'hFFB2E8FFE80000B2)) 
    \upper_reg[15]_i_5 
       (.I0(bv_adder37__3),
        .I1(\aa_reg_reg[30]_0 [13]),
        .I2(\lower_reg_reg[0]_0 [11]),
        .I3(\aa_reg_reg[31]_0 ),
        .I4(\aa_reg_reg[30]_0 [14]),
        .I5(\lower_reg_reg[0]_0 [12]),
        .O(bv_adder33__3));
  LUT6 #(
    .INIT(64'h9969966696996669)) 
    \upper_reg[15]_i_6 
       (.I0(\aa_reg_reg[30]_0 [14]),
        .I1(\lower_reg_reg[0]_0 [12]),
        .I2(bv_adder37__3),
        .I3(\aa_reg_reg[31]_0 ),
        .I4(\aa_reg_reg[30]_0 [13]),
        .I5(\lower_reg_reg[0]_0 [11]),
        .O(bv_adder043_out));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \upper_reg[16]_i_4 
       (.I0(bv_adder049_out),
        .I1(\bb_reg_reg_n_0_[0] ),
        .I2(\lower_reg_reg[0]_0 [14]),
        .I3(\aa_reg_reg[31]_0 ),
        .I4(bv_adder046_out),
        .O(\upper_reg_reg[16]_0 ));
  LUT4 #(
    .INIT(16'h9669)) 
    \upper_reg[16]_i_5 
       (.I0(\aa_reg_reg[30]_0 [15]),
        .I1(\aa_reg_reg[31]_0 ),
        .I2(\lower_reg_reg[0]_0 [13]),
        .I3(bv_adder33__3),
        .O(bv_adder046_out));
  LUT6 #(
    .INIT(64'h9F60FFFF9F600000)) 
    \upper_reg[17]_i_4 
       (.I0(\aa_reg_reg[30]_0 [17]),
        .I1(bv_adder29__3),
        .I2(\bb_reg_reg_n_0_[0] ),
        .I3(p_1_in51_in),
        .I4(\aa_reg_reg[31]_0 ),
        .I5(bv_adder049_out),
        .O(\upper_reg_reg[17]_0 ));
  LUT6 #(
    .INIT(64'hFFB2E8FFE80000B2)) 
    \upper_reg[17]_i_5 
       (.I0(bv_adder33__3),
        .I1(\aa_reg_reg[30]_0 [15]),
        .I2(\lower_reg_reg[0]_0 [13]),
        .I3(\aa_reg_reg[31]_0 ),
        .I4(\aa_reg_reg[30]_0 [16]),
        .I5(\lower_reg_reg[0]_0 [14]),
        .O(bv_adder29__3));
  LUT6 #(
    .INIT(64'h9969966696996669)) 
    \upper_reg[17]_i_6 
       (.I0(\aa_reg_reg[30]_0 [16]),
        .I1(\lower_reg_reg[0]_0 [14]),
        .I2(bv_adder33__3),
        .I3(\aa_reg_reg[31]_0 ),
        .I4(\aa_reg_reg[30]_0 [15]),
        .I5(\lower_reg_reg[0]_0 [13]),
        .O(bv_adder049_out));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \upper_reg[18]_i_4 
       (.I0(bv_adder055_out),
        .I1(\bb_reg_reg_n_0_[0] ),
        .I2(p_1_in54_in),
        .I3(\aa_reg_reg[31]_0 ),
        .I4(bv_adder052_out),
        .O(\upper_reg_reg[18]_0 ));
  LUT4 #(
    .INIT(16'h9669)) 
    \upper_reg[18]_i_5 
       (.I0(\aa_reg_reg[30]_0 [17]),
        .I1(\aa_reg_reg[31]_0 ),
        .I2(p_1_in51_in),
        .I3(bv_adder29__3),
        .O(bv_adder052_out));
  LUT6 #(
    .INIT(64'h9F60FFFF9F600000)) 
    \upper_reg[19]_i_4 
       (.I0(\aa_reg_reg[30]_0 [19]),
        .I1(bv_adder25__3),
        .I2(\bb_reg_reg_n_0_[0] ),
        .I3(p_1_in57_in),
        .I4(\aa_reg_reg[31]_0 ),
        .I5(bv_adder055_out),
        .O(\upper_reg_reg[19]_0 ));
  LUT6 #(
    .INIT(64'hFFB2E8FFE80000B2)) 
    \upper_reg[19]_i_5 
       (.I0(bv_adder29__3),
        .I1(\aa_reg_reg[30]_0 [17]),
        .I2(p_1_in51_in),
        .I3(\aa_reg_reg[31]_0 ),
        .I4(\aa_reg_reg[30]_0 [18]),
        .I5(p_1_in54_in),
        .O(bv_adder25__3));
  LUT6 #(
    .INIT(64'h9969966696996669)) 
    \upper_reg[19]_i_6 
       (.I0(\aa_reg_reg[30]_0 [18]),
        .I1(p_1_in54_in),
        .I2(bv_adder29__3),
        .I3(\aa_reg_reg[31]_0 ),
        .I4(\aa_reg_reg[30]_0 [17]),
        .I5(p_1_in51_in),
        .O(bv_adder055_out));
  LUT6 #(
    .INIT(64'h9F60FFFF9F600000)) 
    \upper_reg[1]_i_4 
       (.I0(\aa_reg_reg[30]_0 [1]),
        .I1(bv_adder61__3),
        .I2(\bb_reg_reg_n_0_[0] ),
        .I3(\lower_reg_reg[0]_0 [2]),
        .I4(\aa_reg_reg[31]_0 ),
        .I5(bv_adder01_out),
        .O(\upper_reg_reg[1]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair179" *) 
  LUT5 #(
    .INIT(32'hFD8F800D)) 
    \upper_reg[1]_i_5 
       (.I0(\aa_reg_reg_n_0_[0] ),
        .I1(\lower_reg_reg[0]_0 [0]),
        .I2(\aa_reg_reg[31]_0 ),
        .I3(\aa_reg_reg[30]_0 [0]),
        .I4(\lower_reg_reg[0]_0 [1]),
        .O(bv_adder61__3));
  (* SOFT_HLUTNM = "soft_lutpair179" *) 
  LUT5 #(
    .INIT(32'h96666966)) 
    \upper_reg[1]_i_6 
       (.I0(\aa_reg_reg[30]_0 [0]),
        .I1(\lower_reg_reg[0]_0 [1]),
        .I2(\aa_reg_reg[31]_0 ),
        .I3(\aa_reg_reg_n_0_[0] ),
        .I4(\lower_reg_reg[0]_0 [0]),
        .O(bv_adder01_out));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \upper_reg[20]_i_4 
       (.I0(bv_adder061_out),
        .I1(\bb_reg_reg_n_0_[0] ),
        .I2(\lower_reg_reg[0]_0 [15]),
        .I3(\aa_reg_reg[31]_0 ),
        .I4(bv_adder058_out),
        .O(\upper_reg_reg[20]_0 ));
  LUT4 #(
    .INIT(16'h9669)) 
    \upper_reg[20]_i_5 
       (.I0(\aa_reg_reg[30]_0 [19]),
        .I1(\aa_reg_reg[31]_0 ),
        .I2(p_1_in57_in),
        .I3(bv_adder25__3),
        .O(bv_adder058_out));
  LUT6 #(
    .INIT(64'h9F60FFFF9F600000)) 
    \upper_reg[21]_i_4 
       (.I0(\aa_reg_reg[30]_0 [21]),
        .I1(bv_adder21__3),
        .I2(\bb_reg_reg_n_0_[0] ),
        .I3(\lower_reg_reg[0]_0 [16]),
        .I4(\aa_reg_reg[31]_0 ),
        .I5(bv_adder061_out),
        .O(\upper_reg_reg[21]_0 ));
  LUT6 #(
    .INIT(64'hFFB2E8FFE80000B2)) 
    \upper_reg[21]_i_5 
       (.I0(bv_adder25__3),
        .I1(\aa_reg_reg[30]_0 [19]),
        .I2(p_1_in57_in),
        .I3(\aa_reg_reg[31]_0 ),
        .I4(\aa_reg_reg[30]_0 [20]),
        .I5(\lower_reg_reg[0]_0 [15]),
        .O(bv_adder21__3));
  LUT6 #(
    .INIT(64'h9969966696996669)) 
    \upper_reg[21]_i_6 
       (.I0(\aa_reg_reg[30]_0 [20]),
        .I1(\lower_reg_reg[0]_0 [15]),
        .I2(bv_adder25__3),
        .I3(\aa_reg_reg[31]_0 ),
        .I4(\aa_reg_reg[30]_0 [19]),
        .I5(p_1_in57_in),
        .O(bv_adder061_out));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \upper_reg[22]_i_4 
       (.I0(bv_adder067_out),
        .I1(\bb_reg_reg_n_0_[0] ),
        .I2(p_1_in66_in),
        .I3(\aa_reg_reg[31]_0 ),
        .I4(bv_adder064_out),
        .O(\upper_reg_reg[22]_0 ));
  LUT4 #(
    .INIT(16'h9669)) 
    \upper_reg[22]_i_5 
       (.I0(\aa_reg_reg[30]_0 [21]),
        .I1(\aa_reg_reg[31]_0 ),
        .I2(\lower_reg_reg[0]_0 [16]),
        .I3(bv_adder21__3),
        .O(bv_adder064_out));
  LUT6 #(
    .INIT(64'h9F60FFFF9F600000)) 
    \upper_reg[23]_i_4 
       (.I0(\aa_reg_reg[30]_0 [23]),
        .I1(bv_adder17__3),
        .I2(\bb_reg_reg_n_0_[0] ),
        .I3(\lower_reg_reg[0]_0 [17]),
        .I4(\aa_reg_reg[31]_0 ),
        .I5(bv_adder067_out),
        .O(\upper_reg_reg[23]_0 ));
  LUT6 #(
    .INIT(64'hFFB2E8FFE80000B2)) 
    \upper_reg[23]_i_5 
       (.I0(bv_adder21__3),
        .I1(\aa_reg_reg[30]_0 [21]),
        .I2(\lower_reg_reg[0]_0 [16]),
        .I3(\aa_reg_reg[31]_0 ),
        .I4(\aa_reg_reg[30]_0 [22]),
        .I5(p_1_in66_in),
        .O(bv_adder17__3));
  LUT6 #(
    .INIT(64'h9969966696996669)) 
    \upper_reg[23]_i_6 
       (.I0(\aa_reg_reg[30]_0 [22]),
        .I1(p_1_in66_in),
        .I2(bv_adder21__3),
        .I3(\aa_reg_reg[31]_0 ),
        .I4(\aa_reg_reg[30]_0 [21]),
        .I5(\lower_reg_reg[0]_0 [16]),
        .O(bv_adder067_out));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \upper_reg[24]_i_4 
       (.I0(bv_adder073_out),
        .I1(\bb_reg_reg_n_0_[0] ),
        .I2(p_1_in72_in),
        .I3(\aa_reg_reg[31]_0 ),
        .I4(bv_adder070_out),
        .O(\upper_reg_reg[24]_0 ));
  LUT4 #(
    .INIT(16'h9669)) 
    \upper_reg[24]_i_5 
       (.I0(\aa_reg_reg[30]_0 [23]),
        .I1(\aa_reg_reg[31]_0 ),
        .I2(\lower_reg_reg[0]_0 [17]),
        .I3(bv_adder17__3),
        .O(bv_adder070_out));
  LUT6 #(
    .INIT(64'h9F60FFFF9F600000)) 
    \upper_reg[25]_i_4 
       (.I0(\aa_reg_reg[30]_0 [25]),
        .I1(bv_adder13__3),
        .I2(\bb_reg_reg_n_0_[0] ),
        .I3(\lower_reg_reg[0]_0 [18]),
        .I4(\aa_reg_reg[31]_0 ),
        .I5(bv_adder073_out),
        .O(\upper_reg_reg[25]_0 ));
  LUT6 #(
    .INIT(64'hFFB2E8FFE80000B2)) 
    \upper_reg[25]_i_5 
       (.I0(bv_adder17__3),
        .I1(\aa_reg_reg[30]_0 [23]),
        .I2(\lower_reg_reg[0]_0 [17]),
        .I3(\aa_reg_reg[31]_0 ),
        .I4(\aa_reg_reg[30]_0 [24]),
        .I5(p_1_in72_in),
        .O(bv_adder13__3));
  LUT6 #(
    .INIT(64'h9969966696996669)) 
    \upper_reg[25]_i_6 
       (.I0(\aa_reg_reg[30]_0 [24]),
        .I1(p_1_in72_in),
        .I2(bv_adder17__3),
        .I3(\aa_reg_reg[31]_0 ),
        .I4(\aa_reg_reg[30]_0 [23]),
        .I5(\lower_reg_reg[0]_0 [17]),
        .O(bv_adder073_out));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \upper_reg[26]_i_4 
       (.I0(bv_adder079_out),
        .I1(\bb_reg_reg_n_0_[0] ),
        .I2(\lower_reg_reg[0]_0 [19]),
        .I3(\aa_reg_reg[31]_0 ),
        .I4(bv_adder076_out),
        .O(\upper_reg_reg[26]_0 ));
  LUT4 #(
    .INIT(16'h9669)) 
    \upper_reg[26]_i_5 
       (.I0(\aa_reg_reg[30]_0 [25]),
        .I1(\aa_reg_reg[31]_0 ),
        .I2(\lower_reg_reg[0]_0 [18]),
        .I3(bv_adder13__3),
        .O(bv_adder076_out));
  LUT6 #(
    .INIT(64'h9F60FFFF9F600000)) 
    \upper_reg[27]_i_4 
       (.I0(\aa_reg_reg[30]_0 [27]),
        .I1(bv_adder9__3),
        .I2(\bb_reg_reg_n_0_[0] ),
        .I3(\lower_reg_reg[0]_0 [20]),
        .I4(\aa_reg_reg[31]_0 ),
        .I5(bv_adder079_out),
        .O(\upper_reg_reg[27]_0 ));
  LUT6 #(
    .INIT(64'hFFB2E8FFE80000B2)) 
    \upper_reg[27]_i_5 
       (.I0(bv_adder13__3),
        .I1(\aa_reg_reg[30]_0 [25]),
        .I2(\lower_reg_reg[0]_0 [18]),
        .I3(\aa_reg_reg[31]_0 ),
        .I4(\aa_reg_reg[30]_0 [26]),
        .I5(\lower_reg_reg[0]_0 [19]),
        .O(bv_adder9__3));
  LUT6 #(
    .INIT(64'h9969966696996669)) 
    \upper_reg[27]_i_6 
       (.I0(\aa_reg_reg[30]_0 [26]),
        .I1(\lower_reg_reg[0]_0 [19]),
        .I2(bv_adder13__3),
        .I3(\aa_reg_reg[31]_0 ),
        .I4(\aa_reg_reg[30]_0 [25]),
        .I5(\lower_reg_reg[0]_0 [18]),
        .O(bv_adder079_out));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \upper_reg[28]_i_4 
       (.I0(bv_adder085_out),
        .I1(\bb_reg_reg_n_0_[0] ),
        .I2(\lower_reg_reg[0]_0 [21]),
        .I3(\aa_reg_reg[31]_0 ),
        .I4(bv_adder082_out),
        .O(\upper_reg_reg[28]_0 ));
  LUT4 #(
    .INIT(16'h9669)) 
    \upper_reg[28]_i_5 
       (.I0(\aa_reg_reg[30]_0 [27]),
        .I1(\aa_reg_reg[31]_0 ),
        .I2(\lower_reg_reg[0]_0 [20]),
        .I3(bv_adder9__3),
        .O(bv_adder082_out));
  LUT6 #(
    .INIT(64'h9F60FFFF9F600000)) 
    \upper_reg[29]_i_4 
       (.I0(\aa_reg_reg[30]_0 [29]),
        .I1(bv_adder5__3),
        .I2(\bb_reg_reg_n_0_[0] ),
        .I3(\lower_reg_reg[0]_0 [22]),
        .I4(\aa_reg_reg[31]_0 ),
        .I5(bv_adder085_out),
        .O(\upper_reg_reg[29]_0 ));
  LUT6 #(
    .INIT(64'hFFB2E8FFE80000B2)) 
    \upper_reg[29]_i_5 
       (.I0(bv_adder9__3),
        .I1(\aa_reg_reg[30]_0 [27]),
        .I2(\lower_reg_reg[0]_0 [20]),
        .I3(\aa_reg_reg[31]_0 ),
        .I4(\aa_reg_reg[30]_0 [28]),
        .I5(\lower_reg_reg[0]_0 [21]),
        .O(bv_adder5__3));
  LUT6 #(
    .INIT(64'h9969966696996669)) 
    \upper_reg[29]_i_6 
       (.I0(\aa_reg_reg[30]_0 [28]),
        .I1(\lower_reg_reg[0]_0 [21]),
        .I2(bv_adder9__3),
        .I3(\aa_reg_reg[31]_0 ),
        .I4(\aa_reg_reg[30]_0 [27]),
        .I5(\lower_reg_reg[0]_0 [20]),
        .O(bv_adder085_out));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \upper_reg[2]_i_4 
       (.I0(bv_adder07_out),
        .I1(\bb_reg_reg_n_0_[0] ),
        .I2(p_1_in6_in),
        .I3(\aa_reg_reg[31]_0 ),
        .I4(bv_adder04_out),
        .O(\upper_reg_reg[2]_0 ));
  LUT4 #(
    .INIT(16'h9669)) 
    \upper_reg[2]_i_5 
       (.I0(\aa_reg_reg[30]_0 [1]),
        .I1(\aa_reg_reg[31]_0 ),
        .I2(\lower_reg_reg[0]_0 [2]),
        .I3(bv_adder61__3),
        .O(bv_adder04_out));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \upper_reg[30]_i_4 
       (.I0(bv_adder091_out),
        .I1(\bb_reg_reg_n_0_[0] ),
        .I2(\lower_reg_reg[0]_0 [23]),
        .I3(\aa_reg_reg[31]_0 ),
        .I4(bv_adder088_out),
        .O(\upper_reg_reg[30]_0 ));
  LUT4 #(
    .INIT(16'h9669)) 
    \upper_reg[30]_i_5 
       (.I0(\aa_reg_reg[30]_0 [29]),
        .I1(\aa_reg_reg[31]_0 ),
        .I2(\lower_reg_reg[0]_0 [22]),
        .I3(bv_adder5__3),
        .O(bv_adder088_out));
  (* SOFT_HLUTNM = "soft_lutpair183" *) 
  LUT3 #(
    .INIT(8'hA8)) 
    \upper_reg[31]_i_3 
       (.I0(neqOp0_in),
        .I1(lower_reg0__62),
        .I2(\aa_reg_reg[31]_0 ),
        .O(\upper_reg_reg[31]_1 ));
  LUT6 #(
    .INIT(64'h6F60FFFF6F600000)) 
    \upper_reg[31]_i_6 
       (.I0(p_0_in342_in),
        .I1(sign_reg_reg_0),
        .I2(\bb_reg_reg_n_0_[0] ),
        .I3(sign2_reg_reg_0),
        .I4(\aa_reg_reg[31]_0 ),
        .I5(bv_adder091_out),
        .O(\upper_reg_reg[31]_0 ));
  LUT6 #(
    .INIT(64'h9969966696996669)) 
    \upper_reg[31]_i_7 
       (.I0(\aa_reg_reg[30]_0 [30]),
        .I1(\lower_reg_reg[0]_0 [23]),
        .I2(bv_adder5__3),
        .I3(\aa_reg_reg[31]_0 ),
        .I4(\aa_reg_reg[30]_0 [29]),
        .I5(\lower_reg_reg[0]_0 [22]),
        .O(bv_adder091_out));
  LUT6 #(
    .INIT(64'h9F60FFFF9F600000)) 
    \upper_reg[3]_i_4 
       (.I0(\aa_reg_reg[30]_0 [3]),
        .I1(bv_adder57__3),
        .I2(\bb_reg_reg_n_0_[0] ),
        .I3(p_1_in9_in),
        .I4(\aa_reg_reg[31]_0 ),
        .I5(bv_adder07_out),
        .O(\upper_reg_reg[3]_0 ));
  LUT6 #(
    .INIT(64'hFFB2E8FFE80000B2)) 
    \upper_reg[3]_i_5 
       (.I0(bv_adder61__3),
        .I1(\aa_reg_reg[30]_0 [1]),
        .I2(\lower_reg_reg[0]_0 [2]),
        .I3(\aa_reg_reg[31]_0 ),
        .I4(\aa_reg_reg[30]_0 [2]),
        .I5(p_1_in6_in),
        .O(bv_adder57__3));
  LUT6 #(
    .INIT(64'h9969966696996669)) 
    \upper_reg[3]_i_6 
       (.I0(\aa_reg_reg[30]_0 [2]),
        .I1(p_1_in6_in),
        .I2(bv_adder61__3),
        .I3(\aa_reg_reg[31]_0 ),
        .I4(\aa_reg_reg[30]_0 [1]),
        .I5(\lower_reg_reg[0]_0 [2]),
        .O(bv_adder07_out));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \upper_reg[4]_i_4 
       (.I0(bv_adder013_out),
        .I1(\bb_reg_reg_n_0_[0] ),
        .I2(p_1_in12_in),
        .I3(\aa_reg_reg[31]_0 ),
        .I4(bv_adder010_out),
        .O(\upper_reg_reg[4]_0 ));
  LUT4 #(
    .INIT(16'h9669)) 
    \upper_reg[4]_i_5 
       (.I0(\aa_reg_reg[30]_0 [3]),
        .I1(\aa_reg_reg[31]_0 ),
        .I2(p_1_in9_in),
        .I3(bv_adder57__3),
        .O(bv_adder010_out));
  LUT6 #(
    .INIT(64'h9F60FFFF9F600000)) 
    \upper_reg[5]_i_4 
       (.I0(\aa_reg_reg[30]_0 [5]),
        .I1(bv_adder53__3),
        .I2(\bb_reg_reg_n_0_[0] ),
        .I3(\lower_reg_reg[0]_0 [3]),
        .I4(\aa_reg_reg[31]_0 ),
        .I5(bv_adder013_out),
        .O(\upper_reg_reg[5]_0 ));
  LUT6 #(
    .INIT(64'hFFB2E8FFE80000B2)) 
    \upper_reg[5]_i_5 
       (.I0(bv_adder57__3),
        .I1(\aa_reg_reg[30]_0 [3]),
        .I2(p_1_in9_in),
        .I3(\aa_reg_reg[31]_0 ),
        .I4(\aa_reg_reg[30]_0 [4]),
        .I5(p_1_in12_in),
        .O(bv_adder53__3));
  LUT6 #(
    .INIT(64'h9969966696996669)) 
    \upper_reg[5]_i_6 
       (.I0(\aa_reg_reg[30]_0 [4]),
        .I1(p_1_in12_in),
        .I2(bv_adder57__3),
        .I3(\aa_reg_reg[31]_0 ),
        .I4(\aa_reg_reg[30]_0 [3]),
        .I5(p_1_in9_in),
        .O(bv_adder013_out));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \upper_reg[6]_i_4 
       (.I0(bv_adder019_out),
        .I1(\bb_reg_reg_n_0_[0] ),
        .I2(\lower_reg_reg[0]_0 [4]),
        .I3(\aa_reg_reg[31]_0 ),
        .I4(bv_adder016_out),
        .O(\upper_reg_reg[6]_0 ));
  LUT4 #(
    .INIT(16'h9669)) 
    \upper_reg[6]_i_5 
       (.I0(\aa_reg_reg[30]_0 [5]),
        .I1(\aa_reg_reg[31]_0 ),
        .I2(\lower_reg_reg[0]_0 [3]),
        .I3(bv_adder53__3),
        .O(bv_adder016_out));
  LUT6 #(
    .INIT(64'h9F60FFFF9F600000)) 
    \upper_reg[7]_i_4 
       (.I0(\aa_reg_reg[30]_0 [7]),
        .I1(bv_adder49__3),
        .I2(\bb_reg_reg_n_0_[0] ),
        .I3(\lower_reg_reg[0]_0 [5]),
        .I4(\aa_reg_reg[31]_0 ),
        .I5(bv_adder019_out),
        .O(\upper_reg_reg[7]_0 ));
  LUT6 #(
    .INIT(64'hFFB2E8FFE80000B2)) 
    \upper_reg[7]_i_5 
       (.I0(bv_adder53__3),
        .I1(\aa_reg_reg[30]_0 [5]),
        .I2(\lower_reg_reg[0]_0 [3]),
        .I3(\aa_reg_reg[31]_0 ),
        .I4(\aa_reg_reg[30]_0 [6]),
        .I5(\lower_reg_reg[0]_0 [4]),
        .O(bv_adder49__3));
  LUT6 #(
    .INIT(64'h9969966696996669)) 
    \upper_reg[7]_i_6 
       (.I0(\aa_reg_reg[30]_0 [6]),
        .I1(\lower_reg_reg[0]_0 [4]),
        .I2(bv_adder53__3),
        .I3(\aa_reg_reg[31]_0 ),
        .I4(\aa_reg_reg[30]_0 [5]),
        .I5(\lower_reg_reg[0]_0 [3]),
        .O(bv_adder019_out));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \upper_reg[8]_i_4 
       (.I0(bv_adder025_out),
        .I1(\bb_reg_reg_n_0_[0] ),
        .I2(\lower_reg_reg[0]_0 [6]),
        .I3(\aa_reg_reg[31]_0 ),
        .I4(bv_adder022_out),
        .O(\upper_reg_reg[8]_0 ));
  LUT4 #(
    .INIT(16'h9669)) 
    \upper_reg[8]_i_5 
       (.I0(\aa_reg_reg[30]_0 [7]),
        .I1(\aa_reg_reg[31]_0 ),
        .I2(\lower_reg_reg[0]_0 [5]),
        .I3(bv_adder49__3),
        .O(bv_adder022_out));
  LUT6 #(
    .INIT(64'h9F60FFFF9F600000)) 
    \upper_reg[9]_i_4 
       (.I0(\aa_reg_reg[30]_0 [9]),
        .I1(bv_adder45__3),
        .I2(\bb_reg_reg_n_0_[0] ),
        .I3(\lower_reg_reg[0]_0 [7]),
        .I4(\aa_reg_reg[31]_0 ),
        .I5(bv_adder025_out),
        .O(\upper_reg_reg[9]_0 ));
  LUT6 #(
    .INIT(64'hFFB2E8FFE80000B2)) 
    \upper_reg[9]_i_5 
       (.I0(bv_adder49__3),
        .I1(\aa_reg_reg[30]_0 [7]),
        .I2(\lower_reg_reg[0]_0 [5]),
        .I3(\aa_reg_reg[31]_0 ),
        .I4(\aa_reg_reg[30]_0 [8]),
        .I5(\lower_reg_reg[0]_0 [6]),
        .O(bv_adder45__3));
  LUT6 #(
    .INIT(64'h9969966696996669)) 
    \upper_reg[9]_i_6 
       (.I0(\aa_reg_reg[30]_0 [8]),
        .I1(\lower_reg_reg[0]_0 [6]),
        .I2(bv_adder49__3),
        .I3(\aa_reg_reg[31]_0 ),
        .I4(\aa_reg_reg[30]_0 [7]),
        .I5(\lower_reg_reg[0]_0 [5]),
        .O(bv_adder025_out));
  FDCE \upper_reg_reg[0] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_1 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_2 [0]),
        .Q(\lower_reg_reg[0]_0 [0]));
  FDCE \upper_reg_reg[10] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_1 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_2 [10]),
        .Q(\lower_reg_reg[0]_0 [7]));
  FDCE \upper_reg_reg[11] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_1 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_2 [11]),
        .Q(\lower_reg_reg[0]_0 [8]));
  FDCE \upper_reg_reg[12] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_1 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_2 [12]),
        .Q(\lower_reg_reg[0]_0 [9]));
  FDCE \upper_reg_reg[13] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_1 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_2 [13]),
        .Q(\lower_reg_reg[0]_0 [10]));
  FDCE \upper_reg_reg[14] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_1 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_2 [14]),
        .Q(\lower_reg_reg[0]_0 [11]));
  FDCE \upper_reg_reg[15] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_1 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_2 [15]),
        .Q(\lower_reg_reg[0]_0 [12]));
  FDCE \upper_reg_reg[16] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_1 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_2 [16]),
        .Q(\lower_reg_reg[0]_0 [13]));
  FDCE \upper_reg_reg[17] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_1 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_2 [17]),
        .Q(\lower_reg_reg[0]_0 [14]));
  FDCE \upper_reg_reg[18] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_1 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_2 [18]),
        .Q(p_1_in51_in));
  FDCE \upper_reg_reg[19] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_1 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_2 [19]),
        .Q(p_1_in54_in));
  FDCE \upper_reg_reg[1] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_1 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_2 [1]),
        .Q(\lower_reg_reg[0]_0 [1]));
  FDCE \upper_reg_reg[20] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_1 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_2 [20]),
        .Q(p_1_in57_in));
  FDCE \upper_reg_reg[21] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_1 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_2 [21]),
        .Q(\lower_reg_reg[0]_0 [15]));
  FDCE \upper_reg_reg[22] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_1 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_2 [22]),
        .Q(\lower_reg_reg[0]_0 [16]));
  FDCE \upper_reg_reg[23] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_1 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_2 [23]),
        .Q(p_1_in66_in));
  FDCE \upper_reg_reg[24] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_1 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_2 [24]),
        .Q(\lower_reg_reg[0]_0 [17]));
  FDCE \upper_reg_reg[25] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_1 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_2 [25]),
        .Q(p_1_in72_in));
  FDCE \upper_reg_reg[26] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_1 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_2 [26]),
        .Q(\lower_reg_reg[0]_0 [18]));
  FDCE \upper_reg_reg[27] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_1 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_2 [27]),
        .Q(\lower_reg_reg[0]_0 [19]));
  FDCE \upper_reg_reg[28] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_1 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_2 [28]),
        .Q(\lower_reg_reg[0]_0 [20]));
  FDCE \upper_reg_reg[29] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_1 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_2 [29]),
        .Q(\lower_reg_reg[0]_0 [21]));
  FDCE \upper_reg_reg[2] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_1 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_2 [2]),
        .Q(\lower_reg_reg[0]_0 [2]));
  FDCE \upper_reg_reg[30] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_1 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_2 [30]),
        .Q(\lower_reg_reg[0]_0 [22]));
  FDCE \upper_reg_reg[31] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_1 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_2 [31]),
        .Q(\lower_reg_reg[0]_0 [23]));
  FDCE \upper_reg_reg[3] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_1 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_2 [3]),
        .Q(p_1_in6_in));
  FDCE \upper_reg_reg[4] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_1 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_2 [4]),
        .Q(p_1_in9_in));
  FDCE \upper_reg_reg[5] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_1 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_2 [5]),
        .Q(p_1_in12_in));
  FDCE \upper_reg_reg[6] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_1 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_2 [6]),
        .Q(\lower_reg_reg[0]_0 [3]));
  FDCE \upper_reg_reg[7] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_1 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_2 [7]),
        .Q(\lower_reg_reg[0]_0 [4]));
  FDCE \upper_reg_reg[8] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_1 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_2 [8]),
        .Q(\lower_reg_reg[0]_0 [5]));
  FDCE \upper_reg_reg[9] 
       (.C(clk),
        .CE(\opcode_reg_reg[30]_1 ),
        .CLR(reset_in1_out),
        .D(\opcode_reg_reg[30]_2 [9]),
        .Q(\lower_reg_reg[0]_0 [6]));
endmodule

module design_1_Dispatcher_HardBlare_0_0_pc_next
   (\bb_reg_reg[28] ,
    p_48_in,
    pc_current,
    pc_plus4,
    \address_reg_reg[31] ,
    Q,
    c_source,
    \opcode_reg_reg[27] ,
    mem_state_reg_reg,
    \pc_reg_reg[31]_0 ,
    clk,
    \reset_reg_reg[3] ,
    \pc_reg_reg[30]_0 ,
    \pc_reg_reg[29]_0 ,
    \pc_reg_reg[28]_0 ,
    \opcode_reg_reg[25] ,
    \opcode_reg_reg[24] ,
    \opcode_reg_reg[23] ,
    \opcode_reg_reg[22] ,
    \opcode_reg_reg[21] ,
    \opcode_reg_reg[20] ,
    \opcode_reg_reg[19] ,
    \opcode_reg_reg[18] ,
    \opcode_reg_reg[17] ,
    \opcode_reg_reg[16] ,
    \opcode_reg_reg[15] ,
    \opcode_reg_reg[14] ,
    \opcode_reg_reg[13] ,
    \opcode_reg_reg[12] ,
    \opcode_reg_reg[11] ,
    \opcode_reg_reg[10] ,
    \opcode_reg_reg[9] ,
    \opcode_reg_reg[8] ,
    \opcode_reg_reg[7] ,
    \opcode_reg_reg[6] ,
    \opcode_reg_reg[5] ,
    \opcode_reg_reg[4] ,
    \opcode_reg_reg[3] ,
    \opcode_reg_reg[2] ,
    \pc_reg_reg[2]_0 ,
    \pc_reg_reg[2]_1 );
  output \bb_reg_reg[28] ;
  output p_48_in;
  output [31:2]pc_current;
  output [27:0]pc_plus4;
  output \address_reg_reg[31] ;
  input [0:0]Q;
  input [0:0]c_source;
  input \opcode_reg_reg[27] ;
  input mem_state_reg_reg;
  input \pc_reg_reg[31]_0 ;
  input clk;
  input \reset_reg_reg[3] ;
  input \pc_reg_reg[30]_0 ;
  input \pc_reg_reg[29]_0 ;
  input \pc_reg_reg[28]_0 ;
  input \opcode_reg_reg[25] ;
  input \opcode_reg_reg[24] ;
  input \opcode_reg_reg[23] ;
  input \opcode_reg_reg[22] ;
  input \opcode_reg_reg[21] ;
  input \opcode_reg_reg[20] ;
  input \opcode_reg_reg[19] ;
  input \opcode_reg_reg[18] ;
  input \opcode_reg_reg[17] ;
  input \opcode_reg_reg[16] ;
  input \opcode_reg_reg[15] ;
  input \opcode_reg_reg[14] ;
  input \opcode_reg_reg[13] ;
  input \opcode_reg_reg[12] ;
  input \opcode_reg_reg[11] ;
  input \opcode_reg_reg[10] ;
  input \opcode_reg_reg[9] ;
  input \opcode_reg_reg[8] ;
  input \opcode_reg_reg[7] ;
  input \opcode_reg_reg[6] ;
  input \opcode_reg_reg[5] ;
  input \opcode_reg_reg[4] ;
  input \opcode_reg_reg[3] ;
  input \opcode_reg_reg[2] ;
  input \pc_reg_reg[2]_0 ;
  input \pc_reg_reg[2]_1 ;

  wire [0:0]Q;
  wire \address_reg_reg[31] ;
  wire \bb_reg_reg[28] ;
  wire [0:0]c_source;
  wire clk;
  wire mem_state_reg_reg;
  wire \opcode_reg_reg[10] ;
  wire \opcode_reg_reg[11] ;
  wire \opcode_reg_reg[12] ;
  wire \opcode_reg_reg[13] ;
  wire \opcode_reg_reg[14] ;
  wire \opcode_reg_reg[15] ;
  wire \opcode_reg_reg[16] ;
  wire \opcode_reg_reg[17] ;
  wire \opcode_reg_reg[18] ;
  wire \opcode_reg_reg[19] ;
  wire \opcode_reg_reg[20] ;
  wire \opcode_reg_reg[21] ;
  wire \opcode_reg_reg[22] ;
  wire \opcode_reg_reg[23] ;
  wire \opcode_reg_reg[24] ;
  wire \opcode_reg_reg[25] ;
  wire \opcode_reg_reg[27] ;
  wire \opcode_reg_reg[2] ;
  wire \opcode_reg_reg[3] ;
  wire \opcode_reg_reg[4] ;
  wire \opcode_reg_reg[5] ;
  wire \opcode_reg_reg[6] ;
  wire \opcode_reg_reg[7] ;
  wire \opcode_reg_reg[8] ;
  wire \opcode_reg_reg[9] ;
  wire p_18_in;
  wire p_28_in;
  wire p_38_in;
  wire p_48_in;
  wire p_8_in;
  wire [31:2]pc_current;
  wire [27:0]pc_plus4;
  wire \pc_reg_reg[28]_0 ;
  wire \pc_reg_reg[29]_0 ;
  wire \pc_reg_reg[2]_0 ;
  wire \pc_reg_reg[2]_1 ;
  wire \pc_reg_reg[30]_0 ;
  wire \pc_reg_reg[31]_0 ;
  wire \reset_reg_reg[3] ;

  LUT2 #(
    .INIT(4'h8)) 
    \address_reg[31]_i_3 
       (.I0(pc_plus4[27]),
        .I1(\opcode_reg_reg[27] ),
        .O(\address_reg_reg[31] ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \pc_reg[10]_i_2 
       (.I0(pc_current[8]),
        .I1(p_8_in),
        .I2(pc_current[9]),
        .I3(pc_current[10]),
        .O(pc_plus4[7]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \pc_reg[11]_i_2 
       (.I0(pc_current[9]),
        .I1(p_8_in),
        .I2(pc_current[8]),
        .I3(pc_current[10]),
        .I4(pc_current[11]),
        .O(pc_plus4[8]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \pc_reg[12]_i_2 
       (.I0(pc_current[10]),
        .I1(pc_current[8]),
        .I2(p_8_in),
        .I3(pc_current[9]),
        .I4(pc_current[11]),
        .I5(pc_current[12]),
        .O(pc_plus4[9]));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \pc_reg[12]_i_3 
       (.I0(pc_current[7]),
        .I1(pc_current[5]),
        .I2(pc_current[3]),
        .I3(pc_current[2]),
        .I4(pc_current[4]),
        .I5(pc_current[6]),
        .O(p_8_in));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \pc_reg[13]_i_2 
       (.I0(p_18_in),
        .I1(pc_current[13]),
        .O(pc_plus4[10]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \pc_reg[14]_i_2 
       (.I0(p_18_in),
        .I1(pc_current[13]),
        .I2(pc_current[14]),
        .O(pc_plus4[11]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \pc_reg[15]_i_2 
       (.I0(pc_current[13]),
        .I1(p_18_in),
        .I2(pc_current[14]),
        .I3(pc_current[15]),
        .O(pc_plus4[12]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \pc_reg[16]_i_2 
       (.I0(pc_current[14]),
        .I1(p_18_in),
        .I2(pc_current[13]),
        .I3(pc_current[15]),
        .I4(pc_current[16]),
        .O(pc_plus4[13]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \pc_reg[17]_i_2 
       (.I0(pc_current[15]),
        .I1(pc_current[13]),
        .I2(p_18_in),
        .I3(pc_current[14]),
        .I4(pc_current[16]),
        .I5(pc_current[17]),
        .O(pc_plus4[14]));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \pc_reg[17]_i_3 
       (.I0(pc_current[12]),
        .I1(pc_current[10]),
        .I2(pc_current[8]),
        .I3(p_8_in),
        .I4(pc_current[9]),
        .I5(pc_current[11]),
        .O(p_18_in));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \pc_reg[18]_i_2 
       (.I0(p_28_in),
        .I1(pc_current[18]),
        .O(pc_plus4[15]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \pc_reg[19]_i_2 
       (.I0(p_28_in),
        .I1(pc_current[18]),
        .I2(pc_current[19]),
        .O(pc_plus4[16]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \pc_reg[20]_i_2 
       (.I0(pc_current[18]),
        .I1(p_28_in),
        .I2(pc_current[19]),
        .I3(pc_current[20]),
        .O(pc_plus4[17]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \pc_reg[21]_i_2 
       (.I0(pc_current[19]),
        .I1(p_28_in),
        .I2(pc_current[18]),
        .I3(pc_current[20]),
        .I4(pc_current[21]),
        .O(pc_plus4[18]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \pc_reg[22]_i_2 
       (.I0(pc_current[20]),
        .I1(pc_current[18]),
        .I2(p_28_in),
        .I3(pc_current[19]),
        .I4(pc_current[21]),
        .I5(pc_current[22]),
        .O(pc_plus4[19]));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \pc_reg[22]_i_4 
       (.I0(pc_current[17]),
        .I1(pc_current[15]),
        .I2(pc_current[13]),
        .I3(p_18_in),
        .I4(pc_current[14]),
        .I5(pc_current[16]),
        .O(p_28_in));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \pc_reg[23]_i_2 
       (.I0(p_38_in),
        .I1(pc_current[23]),
        .O(pc_plus4[20]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \pc_reg[24]_i_2 
       (.I0(p_38_in),
        .I1(pc_current[23]),
        .I2(pc_current[24]),
        .O(pc_plus4[21]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \pc_reg[25]_i_2 
       (.I0(pc_current[23]),
        .I1(p_38_in),
        .I2(pc_current[24]),
        .I3(pc_current[25]),
        .O(pc_plus4[22]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \pc_reg[26]_i_2 
       (.I0(pc_current[24]),
        .I1(p_38_in),
        .I2(pc_current[23]),
        .I3(pc_current[25]),
        .I4(pc_current[26]),
        .O(pc_plus4[23]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \pc_reg[27]_i_2 
       (.I0(pc_current[25]),
        .I1(pc_current[23]),
        .I2(p_38_in),
        .I3(pc_current[24]),
        .I4(pc_current[26]),
        .I5(pc_current[27]),
        .O(pc_plus4[24]));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \pc_reg[28]_i_2 
       (.I0(pc_current[27]),
        .I1(pc_current[25]),
        .I2(pc_current[23]),
        .I3(p_38_in),
        .I4(pc_current[24]),
        .I5(pc_current[26]),
        .O(p_48_in));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \pc_reg[28]_i_4 
       (.I0(pc_current[22]),
        .I1(pc_current[20]),
        .I2(pc_current[18]),
        .I3(p_28_in),
        .I4(pc_current[19]),
        .I5(pc_current[21]),
        .O(p_38_in));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \pc_reg[29]_i_2 
       (.I0(p_48_in),
        .I1(pc_current[28]),
        .I2(pc_current[29]),
        .O(pc_plus4[25]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \pc_reg[30]_i_2 
       (.I0(pc_current[28]),
        .I1(p_48_in),
        .I2(pc_current[29]),
        .I3(pc_current[30]),
        .O(pc_plus4[26]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \pc_reg[31]_i_5 
       (.I0(pc_current[29]),
        .I1(p_48_in),
        .I2(pc_current[28]),
        .I3(pc_current[30]),
        .I4(pc_current[31]),
        .O(pc_plus4[27]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \pc_reg[4]_i_2 
       (.I0(pc_current[2]),
        .I1(pc_current[3]),
        .I2(pc_current[4]),
        .O(pc_plus4[1]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \pc_reg[5]_i_2 
       (.I0(pc_current[3]),
        .I1(pc_current[2]),
        .I2(pc_current[4]),
        .I3(pc_current[5]),
        .O(pc_plus4[2]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \pc_reg[6]_i_2 
       (.I0(pc_current[4]),
        .I1(pc_current[2]),
        .I2(pc_current[3]),
        .I3(pc_current[5]),
        .I4(pc_current[6]),
        .O(pc_plus4[3]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \pc_reg[7]_i_2 
       (.I0(pc_current[5]),
        .I1(pc_current[3]),
        .I2(pc_current[2]),
        .I3(pc_current[4]),
        .I4(pc_current[6]),
        .I5(pc_current[7]),
        .O(pc_plus4[4]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \pc_reg[8]_i_2 
       (.I0(p_8_in),
        .I1(pc_current[8]),
        .O(pc_plus4[5]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \pc_reg[9]_i_2 
       (.I0(p_8_in),
        .I1(pc_current[8]),
        .I2(pc_current[9]),
        .O(pc_plus4[6]));
  FDCE \pc_reg_reg[10] 
       (.C(clk),
        .CE(mem_state_reg_reg),
        .CLR(\reset_reg_reg[3] ),
        .D(\opcode_reg_reg[8] ),
        .Q(pc_current[10]));
  FDCE \pc_reg_reg[11] 
       (.C(clk),
        .CE(mem_state_reg_reg),
        .CLR(\reset_reg_reg[3] ),
        .D(\opcode_reg_reg[9] ),
        .Q(pc_current[11]));
  FDCE \pc_reg_reg[12] 
       (.C(clk),
        .CE(mem_state_reg_reg),
        .CLR(\reset_reg_reg[3] ),
        .D(\opcode_reg_reg[10] ),
        .Q(pc_current[12]));
  FDCE \pc_reg_reg[13] 
       (.C(clk),
        .CE(mem_state_reg_reg),
        .CLR(\reset_reg_reg[3] ),
        .D(\opcode_reg_reg[11] ),
        .Q(pc_current[13]));
  FDCE \pc_reg_reg[14] 
       (.C(clk),
        .CE(mem_state_reg_reg),
        .CLR(\reset_reg_reg[3] ),
        .D(\opcode_reg_reg[12] ),
        .Q(pc_current[14]));
  FDCE \pc_reg_reg[15] 
       (.C(clk),
        .CE(mem_state_reg_reg),
        .CLR(\reset_reg_reg[3] ),
        .D(\opcode_reg_reg[13] ),
        .Q(pc_current[15]));
  FDCE \pc_reg_reg[16] 
       (.C(clk),
        .CE(mem_state_reg_reg),
        .CLR(\reset_reg_reg[3] ),
        .D(\opcode_reg_reg[14] ),
        .Q(pc_current[16]));
  FDCE \pc_reg_reg[17] 
       (.C(clk),
        .CE(mem_state_reg_reg),
        .CLR(\reset_reg_reg[3] ),
        .D(\opcode_reg_reg[15] ),
        .Q(pc_current[17]));
  FDCE \pc_reg_reg[18] 
       (.C(clk),
        .CE(mem_state_reg_reg),
        .CLR(\reset_reg_reg[3] ),
        .D(\opcode_reg_reg[16] ),
        .Q(pc_current[18]));
  FDCE \pc_reg_reg[19] 
       (.C(clk),
        .CE(mem_state_reg_reg),
        .CLR(\reset_reg_reg[3] ),
        .D(\opcode_reg_reg[17] ),
        .Q(pc_current[19]));
  FDCE \pc_reg_reg[20] 
       (.C(clk),
        .CE(mem_state_reg_reg),
        .CLR(\reset_reg_reg[3] ),
        .D(\opcode_reg_reg[18] ),
        .Q(pc_current[20]));
  FDCE \pc_reg_reg[21] 
       (.C(clk),
        .CE(mem_state_reg_reg),
        .CLR(\reset_reg_reg[3] ),
        .D(\opcode_reg_reg[19] ),
        .Q(pc_current[21]));
  FDCE \pc_reg_reg[22] 
       (.C(clk),
        .CE(mem_state_reg_reg),
        .CLR(\reset_reg_reg[3] ),
        .D(\opcode_reg_reg[20] ),
        .Q(pc_current[22]));
  FDCE \pc_reg_reg[23] 
       (.C(clk),
        .CE(mem_state_reg_reg),
        .CLR(\reset_reg_reg[3] ),
        .D(\opcode_reg_reg[21] ),
        .Q(pc_current[23]));
  FDCE \pc_reg_reg[24] 
       (.C(clk),
        .CE(mem_state_reg_reg),
        .CLR(\reset_reg_reg[3] ),
        .D(\opcode_reg_reg[22] ),
        .Q(pc_current[24]));
  FDCE \pc_reg_reg[25] 
       (.C(clk),
        .CE(mem_state_reg_reg),
        .CLR(\reset_reg_reg[3] ),
        .D(\opcode_reg_reg[23] ),
        .Q(pc_current[25]));
  FDCE \pc_reg_reg[26] 
       (.C(clk),
        .CE(mem_state_reg_reg),
        .CLR(\reset_reg_reg[3] ),
        .D(\opcode_reg_reg[24] ),
        .Q(pc_current[26]));
  FDCE \pc_reg_reg[27] 
       (.C(clk),
        .CE(mem_state_reg_reg),
        .CLR(\reset_reg_reg[3] ),
        .D(\opcode_reg_reg[25] ),
        .Q(pc_current[27]));
  FDCE \pc_reg_reg[28] 
       (.C(clk),
        .CE(mem_state_reg_reg),
        .CLR(\reset_reg_reg[3] ),
        .D(\pc_reg_reg[28]_0 ),
        .Q(pc_current[28]));
  FDCE \pc_reg_reg[29] 
       (.C(clk),
        .CE(mem_state_reg_reg),
        .CLR(\reset_reg_reg[3] ),
        .D(\pc_reg_reg[29]_0 ),
        .Q(pc_current[29]));
  FDCE \pc_reg_reg[2] 
       (.C(clk),
        .CE(mem_state_reg_reg),
        .CLR(\reset_reg_reg[3] ),
        .D(\pc_reg_reg[2]_1 ),
        .Q(pc_current[2]));
  FDCE \pc_reg_reg[30] 
       (.C(clk),
        .CE(mem_state_reg_reg),
        .CLR(\reset_reg_reg[3] ),
        .D(\pc_reg_reg[30]_0 ),
        .Q(pc_current[30]));
  FDCE \pc_reg_reg[31] 
       (.C(clk),
        .CE(mem_state_reg_reg),
        .CLR(\reset_reg_reg[3] ),
        .D(\pc_reg_reg[31]_0 ),
        .Q(pc_current[31]));
  FDCE \pc_reg_reg[3] 
       (.C(clk),
        .CE(mem_state_reg_reg),
        .CLR(\reset_reg_reg[3] ),
        .D(\pc_reg_reg[2]_0 ),
        .Q(pc_current[3]));
  FDCE \pc_reg_reg[4] 
       (.C(clk),
        .CE(mem_state_reg_reg),
        .CLR(\reset_reg_reg[3] ),
        .D(\opcode_reg_reg[2] ),
        .Q(pc_current[4]));
  FDCE \pc_reg_reg[5] 
       (.C(clk),
        .CE(mem_state_reg_reg),
        .CLR(\reset_reg_reg[3] ),
        .D(\opcode_reg_reg[3] ),
        .Q(pc_current[5]));
  FDCE \pc_reg_reg[6] 
       (.C(clk),
        .CE(mem_state_reg_reg),
        .CLR(\reset_reg_reg[3] ),
        .D(\opcode_reg_reg[4] ),
        .Q(pc_current[6]));
  FDCE \pc_reg_reg[7] 
       (.C(clk),
        .CE(mem_state_reg_reg),
        .CLR(\reset_reg_reg[3] ),
        .D(\opcode_reg_reg[5] ),
        .Q(pc_current[7]));
  FDCE \pc_reg_reg[8] 
       (.C(clk),
        .CE(mem_state_reg_reg),
        .CLR(\reset_reg_reg[3] ),
        .D(\opcode_reg_reg[6] ),
        .Q(pc_current[8]));
  FDCE \pc_reg_reg[9] 
       (.C(clk),
        .CE(mem_state_reg_reg),
        .CLR(\reset_reg_reg[3] ),
        .D(\opcode_reg_reg[7] ),
        .Q(pc_current[9]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT4 #(
    .INIT(16'hF066)) 
    \xilinx_16x1d.reg_loop[28].reg_bit1a_i_2 
       (.I0(p_48_in),
        .I1(pc_current[28]),
        .I2(Q),
        .I3(c_source),
        .O(\bb_reg_reg[28] ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \xilinx_16x1d.reg_loop[3].reg_bit1a_i_2 
       (.I0(pc_current[2]),
        .I1(pc_current[3]),
        .O(pc_plus4[0]));
endmodule

module design_1_Dispatcher_HardBlare_0_0_reg_bank
   (DPO,
    data_out1B,
    DPO122_out,
    DPO118_out,
    DPO114_out,
    DPO110_out,
    DPO106_out,
    DPO102_out,
    DPO98_out,
    DPO94_out,
    DPO90_out,
    DPO86_out,
    DPO82_out,
    DPO78_out,
    DPO74_out,
    DPO70_out,
    DPO66_out,
    DPO62_out,
    DPO58_out,
    DPO54_out,
    DPO50_out,
    DPO46_out,
    DPO42_out,
    DPO38_out,
    DPO34_out,
    DPO30_out,
    DPO26_out,
    DPO22_out,
    DPO18_out,
    DPO14_out,
    DPO10_out,
    DPO6_out,
    DPO2_out,
    intr_enable,
    reg_target,
    data0,
    clk,
    \opcode_reg_reg[29] ,
    WE,
    \opcode_reg_reg[31] ,
    \opcode_reg_reg[2] ,
    \opcode_reg_reg[2]_0 ,
    \opcode_reg_reg[2]_1 ,
    DPRA0,
    DPRA1,
    DPRA2,
    DPRA3,
    \opcode_reg_reg[31]_0 ,
    \opcode_reg_reg[16] ,
    \opcode_reg_reg[17] ,
    \opcode_reg_reg[18] ,
    \opcode_reg_reg[19] ,
    reg_dest,
    intr_enable_reg_reg_0,
    \reset_reg_reg[3] ,
    \opcode_reg_reg[20] ,
    \opcode_reg_reg[20]_0 );
  output DPO;
  output [31:0]data_out1B;
  output DPO122_out;
  output DPO118_out;
  output DPO114_out;
  output DPO110_out;
  output DPO106_out;
  output DPO102_out;
  output DPO98_out;
  output DPO94_out;
  output DPO90_out;
  output DPO86_out;
  output DPO82_out;
  output DPO78_out;
  output DPO74_out;
  output DPO70_out;
  output DPO66_out;
  output DPO62_out;
  output DPO58_out;
  output DPO54_out;
  output DPO50_out;
  output DPO46_out;
  output DPO42_out;
  output DPO38_out;
  output DPO34_out;
  output DPO30_out;
  output DPO26_out;
  output DPO22_out;
  output DPO18_out;
  output DPO14_out;
  output DPO10_out;
  output DPO6_out;
  output DPO2_out;
  output intr_enable;
  output [31:0]reg_target;
  output [0:0]data0;
  input clk;
  input \opcode_reg_reg[29] ;
  input WE;
  input \opcode_reg_reg[31] ;
  input \opcode_reg_reg[2] ;
  input \opcode_reg_reg[2]_0 ;
  input \opcode_reg_reg[2]_1 ;
  input DPRA0;
  input DPRA1;
  input DPRA2;
  input DPRA3;
  input \opcode_reg_reg[31]_0 ;
  input \opcode_reg_reg[16] ;
  input \opcode_reg_reg[17] ;
  input \opcode_reg_reg[18] ;
  input \opcode_reg_reg[19] ;
  input [30:0]reg_dest;
  input intr_enable_reg_reg_0;
  input \reset_reg_reg[3] ;
  input \opcode_reg_reg[20] ;
  input \opcode_reg_reg[20]_0 ;

  wire DPO;
  wire DPO0_out;
  wire DPO100_out;
  wire DPO102_out;
  wire DPO104_out;
  wire DPO106_out;
  wire DPO108_out;
  wire DPO10_out;
  wire DPO110_out;
  wire DPO112_out;
  wire DPO114_out;
  wire DPO116_out;
  wire DPO118_out;
  wire DPO120_out;
  wire DPO122_out;
  wire DPO124_out;
  wire DPO12_out;
  wire DPO14_out;
  wire DPO16_out;
  wire DPO18_out;
  wire DPO20_out;
  wire DPO22_out;
  wire DPO24_out;
  wire DPO26_out;
  wire DPO28_out;
  wire DPO2_out;
  wire DPO30_out;
  wire DPO32_out;
  wire DPO34_out;
  wire DPO36_out;
  wire DPO38_out;
  wire DPO40_out;
  wire DPO42_out;
  wire DPO44_out;
  wire DPO46_out;
  wire DPO48_out;
  wire DPO4_out;
  wire DPO50_out;
  wire DPO52_out;
  wire DPO54_out;
  wire DPO56_out;
  wire DPO58_out;
  wire DPO60_out;
  wire DPO62_out;
  wire DPO64_out;
  wire DPO66_out;
  wire DPO68_out;
  wire DPO6_out;
  wire DPO70_out;
  wire DPO72_out;
  wire DPO74_out;
  wire DPO76_out;
  wire DPO78_out;
  wire DPO80_out;
  wire DPO82_out;
  wire DPO84_out;
  wire DPO86_out;
  wire DPO88_out;
  wire DPO8_out;
  wire DPO90_out;
  wire DPO92_out;
  wire DPO94_out;
  wire DPO96_out;
  wire DPO98_out;
  wire DPRA0;
  wire DPRA1;
  wire DPRA2;
  wire DPRA3;
  wire WE;
  wire clk;
  wire [0:0]data0;
  wire [31:0]data_out1B;
  wire [31:0]data_out2B;
  wire intr_enable;
  wire intr_enable_reg_reg_0;
  wire \opcode_reg_reg[16] ;
  wire \opcode_reg_reg[17] ;
  wire \opcode_reg_reg[18] ;
  wire \opcode_reg_reg[19] ;
  wire \opcode_reg_reg[20] ;
  wire \opcode_reg_reg[20]_0 ;
  wire \opcode_reg_reg[29] ;
  wire \opcode_reg_reg[2] ;
  wire \opcode_reg_reg[2]_0 ;
  wire \opcode_reg_reg[2]_1 ;
  wire \opcode_reg_reg[31] ;
  wire \opcode_reg_reg[31]_0 ;
  wire [30:0]reg_dest;
  wire [31:0]reg_target;
  wire \reset_reg_reg[3] ;
  wire \NLW_xilinx_16x1d.reg_loop[0].reg_bit1a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[0].reg_bit1b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[0].reg_bit2a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[0].reg_bit2b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[10].reg_bit1a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[10].reg_bit1b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[10].reg_bit2a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[10].reg_bit2b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[11].reg_bit1a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[11].reg_bit1b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[11].reg_bit2a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[11].reg_bit2b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[12].reg_bit1a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[12].reg_bit1b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[12].reg_bit2a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[12].reg_bit2b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[13].reg_bit1a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[13].reg_bit1b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[13].reg_bit2a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[13].reg_bit2b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[14].reg_bit1a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[14].reg_bit1b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[14].reg_bit2a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[14].reg_bit2b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[15].reg_bit1a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[15].reg_bit1b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[15].reg_bit2a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[15].reg_bit2b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[16].reg_bit1a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[16].reg_bit1b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[16].reg_bit2a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[16].reg_bit2b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[17].reg_bit1a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[17].reg_bit1b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[17].reg_bit2a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[17].reg_bit2b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[18].reg_bit1a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[18].reg_bit1b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[18].reg_bit2a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[18].reg_bit2b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[19].reg_bit1a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[19].reg_bit1b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[19].reg_bit2a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[19].reg_bit2b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[1].reg_bit1a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[1].reg_bit1b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[1].reg_bit2a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[1].reg_bit2b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[20].reg_bit1a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[20].reg_bit1b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[20].reg_bit2a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[20].reg_bit2b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[21].reg_bit1a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[21].reg_bit1b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[21].reg_bit2a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[21].reg_bit2b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[22].reg_bit1a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[22].reg_bit1b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[22].reg_bit2a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[22].reg_bit2b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[23].reg_bit1a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[23].reg_bit1b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[23].reg_bit2a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[23].reg_bit2b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[24].reg_bit1a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[24].reg_bit1b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[24].reg_bit2a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[24].reg_bit2b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[25].reg_bit1a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[25].reg_bit1b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[25].reg_bit2a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[25].reg_bit2b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[26].reg_bit1a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[26].reg_bit1b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[26].reg_bit2a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[26].reg_bit2b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[27].reg_bit1a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[27].reg_bit1b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[27].reg_bit2a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[27].reg_bit2b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[28].reg_bit1a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[28].reg_bit1b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[28].reg_bit2a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[28].reg_bit2b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[29].reg_bit1a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[29].reg_bit1b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[29].reg_bit2a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[29].reg_bit2b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[2].reg_bit1a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[2].reg_bit1b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[2].reg_bit2a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[2].reg_bit2b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[30].reg_bit1a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[30].reg_bit1b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[30].reg_bit2a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[30].reg_bit2b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[31].reg_bit1a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[31].reg_bit1b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[31].reg_bit2a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[31].reg_bit2b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[3].reg_bit1a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[3].reg_bit1b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[3].reg_bit2a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[3].reg_bit2b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[4].reg_bit1a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[4].reg_bit1b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[4].reg_bit2a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[4].reg_bit2b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[5].reg_bit1a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[5].reg_bit1b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[5].reg_bit2a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[5].reg_bit2b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[6].reg_bit1a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[6].reg_bit1b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[6].reg_bit2a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[6].reg_bit2b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[7].reg_bit1a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[7].reg_bit1b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[7].reg_bit2a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[7].reg_bit2b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[8].reg_bit1a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[8].reg_bit1b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[8].reg_bit2a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[8].reg_bit2b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[9].reg_bit1a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[9].reg_bit1b_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[9].reg_bit2a_SPO_UNCONNECTED ;
  wire \NLW_xilinx_16x1d.reg_loop[9].reg_bit2b_SPO_UNCONNECTED ;

  (* SOFT_HLUTNM = "soft_lutpair166" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \bb_reg[1]_i_4 
       (.I0(data_out2B[0]),
        .I1(\opcode_reg_reg[20] ),
        .I2(DPO124_out),
        .O(data0));
  LUT4 #(
    .INIT(16'hE200)) 
    \bram_firmware_code_data_write_reg[16]_i_2 
       (.I0(DPO60_out),
        .I1(\opcode_reg_reg[20] ),
        .I2(data_out2B[16]),
        .I3(\opcode_reg_reg[20]_0 ),
        .O(reg_target[16]));
  LUT4 #(
    .INIT(16'hE200)) 
    \bram_firmware_code_data_write_reg[17]_i_2 
       (.I0(DPO56_out),
        .I1(\opcode_reg_reg[20] ),
        .I2(data_out2B[17]),
        .I3(\opcode_reg_reg[20]_0 ),
        .O(reg_target[17]));
  LUT4 #(
    .INIT(16'hE200)) 
    \bram_firmware_code_data_write_reg[18]_i_2 
       (.I0(DPO52_out),
        .I1(\opcode_reg_reg[20] ),
        .I2(data_out2B[18]),
        .I3(\opcode_reg_reg[20]_0 ),
        .O(reg_target[18]));
  LUT4 #(
    .INIT(16'hE200)) 
    \bram_firmware_code_data_write_reg[19]_i_2 
       (.I0(DPO48_out),
        .I1(\opcode_reg_reg[20] ),
        .I2(data_out2B[19]),
        .I3(\opcode_reg_reg[20]_0 ),
        .O(reg_target[19]));
  LUT4 #(
    .INIT(16'hE200)) 
    \bram_firmware_code_data_write_reg[20]_i_2 
       (.I0(DPO44_out),
        .I1(\opcode_reg_reg[20] ),
        .I2(data_out2B[20]),
        .I3(\opcode_reg_reg[20]_0 ),
        .O(reg_target[20]));
  LUT4 #(
    .INIT(16'hE200)) 
    \bram_firmware_code_data_write_reg[21]_i_2 
       (.I0(DPO40_out),
        .I1(\opcode_reg_reg[20] ),
        .I2(data_out2B[21]),
        .I3(\opcode_reg_reg[20]_0 ),
        .O(reg_target[21]));
  LUT4 #(
    .INIT(16'hE200)) 
    \bram_firmware_code_data_write_reg[22]_i_2 
       (.I0(DPO36_out),
        .I1(\opcode_reg_reg[20] ),
        .I2(data_out2B[22]),
        .I3(\opcode_reg_reg[20]_0 ),
        .O(reg_target[22]));
  LUT4 #(
    .INIT(16'hE200)) 
    \bram_firmware_code_data_write_reg[23]_i_2 
       (.I0(DPO32_out),
        .I1(\opcode_reg_reg[20] ),
        .I2(data_out2B[23]),
        .I3(\opcode_reg_reg[20]_0 ),
        .O(reg_target[23]));
  LUT4 #(
    .INIT(16'hE200)) 
    \bram_firmware_code_data_write_reg[24]_i_2 
       (.I0(DPO92_out),
        .I1(\opcode_reg_reg[20] ),
        .I2(data_out2B[8]),
        .I3(\opcode_reg_reg[20]_0 ),
        .O(reg_target[8]));
  LUT4 #(
    .INIT(16'hE200)) 
    \bram_firmware_code_data_write_reg[24]_i_3 
       (.I0(DPO28_out),
        .I1(\opcode_reg_reg[20] ),
        .I2(data_out2B[24]),
        .I3(\opcode_reg_reg[20]_0 ),
        .O(reg_target[24]));
  (* SOFT_HLUTNM = "soft_lutpair166" *) 
  LUT4 #(
    .INIT(16'hE200)) 
    \bram_firmware_code_data_write_reg[24]_i_4 
       (.I0(DPO124_out),
        .I1(\opcode_reg_reg[20] ),
        .I2(data_out2B[0]),
        .I3(\opcode_reg_reg[20]_0 ),
        .O(reg_target[0]));
  LUT4 #(
    .INIT(16'hE200)) 
    \bram_firmware_code_data_write_reg[25]_i_2 
       (.I0(DPO88_out),
        .I1(\opcode_reg_reg[20] ),
        .I2(data_out2B[9]),
        .I3(\opcode_reg_reg[20]_0 ),
        .O(reg_target[9]));
  LUT4 #(
    .INIT(16'hE200)) 
    \bram_firmware_code_data_write_reg[25]_i_3 
       (.I0(DPO24_out),
        .I1(\opcode_reg_reg[20] ),
        .I2(data_out2B[25]),
        .I3(\opcode_reg_reg[20]_0 ),
        .O(reg_target[25]));
  LUT4 #(
    .INIT(16'hE200)) 
    \bram_firmware_code_data_write_reg[25]_i_4 
       (.I0(DPO120_out),
        .I1(\opcode_reg_reg[20] ),
        .I2(data_out2B[1]),
        .I3(\opcode_reg_reg[20]_0 ),
        .O(reg_target[1]));
  LUT4 #(
    .INIT(16'hE200)) 
    \bram_firmware_code_data_write_reg[26]_i_2 
       (.I0(DPO84_out),
        .I1(\opcode_reg_reg[20] ),
        .I2(data_out2B[10]),
        .I3(\opcode_reg_reg[20]_0 ),
        .O(reg_target[10]));
  LUT4 #(
    .INIT(16'hE200)) 
    \bram_firmware_code_data_write_reg[26]_i_3 
       (.I0(DPO20_out),
        .I1(\opcode_reg_reg[20] ),
        .I2(data_out2B[26]),
        .I3(\opcode_reg_reg[20]_0 ),
        .O(reg_target[26]));
  LUT4 #(
    .INIT(16'hE200)) 
    \bram_firmware_code_data_write_reg[26]_i_4 
       (.I0(DPO116_out),
        .I1(\opcode_reg_reg[20] ),
        .I2(data_out2B[2]),
        .I3(\opcode_reg_reg[20]_0 ),
        .O(reg_target[2]));
  LUT4 #(
    .INIT(16'hE200)) 
    \bram_firmware_code_data_write_reg[27]_i_2 
       (.I0(DPO80_out),
        .I1(\opcode_reg_reg[20] ),
        .I2(data_out2B[11]),
        .I3(\opcode_reg_reg[20]_0 ),
        .O(reg_target[11]));
  LUT4 #(
    .INIT(16'hE200)) 
    \bram_firmware_code_data_write_reg[27]_i_3 
       (.I0(DPO16_out),
        .I1(\opcode_reg_reg[20] ),
        .I2(data_out2B[27]),
        .I3(\opcode_reg_reg[20]_0 ),
        .O(reg_target[27]));
  LUT4 #(
    .INIT(16'hE200)) 
    \bram_firmware_code_data_write_reg[27]_i_4 
       (.I0(DPO112_out),
        .I1(\opcode_reg_reg[20] ),
        .I2(data_out2B[3]),
        .I3(\opcode_reg_reg[20]_0 ),
        .O(reg_target[3]));
  LUT4 #(
    .INIT(16'hE200)) 
    \bram_firmware_code_data_write_reg[28]_i_2 
       (.I0(DPO76_out),
        .I1(\opcode_reg_reg[20] ),
        .I2(data_out2B[12]),
        .I3(\opcode_reg_reg[20]_0 ),
        .O(reg_target[12]));
  LUT4 #(
    .INIT(16'hE200)) 
    \bram_firmware_code_data_write_reg[28]_i_3 
       (.I0(DPO12_out),
        .I1(\opcode_reg_reg[20] ),
        .I2(data_out2B[28]),
        .I3(\opcode_reg_reg[20]_0 ),
        .O(reg_target[28]));
  LUT4 #(
    .INIT(16'hE200)) 
    \bram_firmware_code_data_write_reg[28]_i_4 
       (.I0(DPO108_out),
        .I1(\opcode_reg_reg[20] ),
        .I2(data_out2B[4]),
        .I3(\opcode_reg_reg[20]_0 ),
        .O(reg_target[4]));
  LUT4 #(
    .INIT(16'hE200)) 
    \bram_firmware_code_data_write_reg[29]_i_2 
       (.I0(DPO72_out),
        .I1(\opcode_reg_reg[20] ),
        .I2(data_out2B[13]),
        .I3(\opcode_reg_reg[20]_0 ),
        .O(reg_target[13]));
  LUT4 #(
    .INIT(16'hE200)) 
    \bram_firmware_code_data_write_reg[29]_i_3 
       (.I0(DPO8_out),
        .I1(\opcode_reg_reg[20] ),
        .I2(data_out2B[29]),
        .I3(\opcode_reg_reg[20]_0 ),
        .O(reg_target[29]));
  LUT4 #(
    .INIT(16'hE200)) 
    \bram_firmware_code_data_write_reg[29]_i_4 
       (.I0(DPO104_out),
        .I1(\opcode_reg_reg[20] ),
        .I2(data_out2B[5]),
        .I3(\opcode_reg_reg[20]_0 ),
        .O(reg_target[5]));
  LUT4 #(
    .INIT(16'hE200)) 
    \bram_firmware_code_data_write_reg[30]_i_2 
       (.I0(DPO68_out),
        .I1(\opcode_reg_reg[20] ),
        .I2(data_out2B[14]),
        .I3(\opcode_reg_reg[20]_0 ),
        .O(reg_target[14]));
  LUT4 #(
    .INIT(16'hE200)) 
    \bram_firmware_code_data_write_reg[30]_i_3 
       (.I0(DPO4_out),
        .I1(\opcode_reg_reg[20] ),
        .I2(data_out2B[30]),
        .I3(\opcode_reg_reg[20]_0 ),
        .O(reg_target[30]));
  LUT4 #(
    .INIT(16'hE200)) 
    \bram_firmware_code_data_write_reg[30]_i_4 
       (.I0(DPO100_out),
        .I1(\opcode_reg_reg[20] ),
        .I2(data_out2B[6]),
        .I3(\opcode_reg_reg[20]_0 ),
        .O(reg_target[6]));
  LUT4 #(
    .INIT(16'hE200)) 
    \bram_firmware_code_data_write_reg[31]_i_3 
       (.I0(DPO64_out),
        .I1(\opcode_reg_reg[20] ),
        .I2(data_out2B[15]),
        .I3(\opcode_reg_reg[20]_0 ),
        .O(reg_target[15]));
  LUT4 #(
    .INIT(16'hE200)) 
    \bram_firmware_code_data_write_reg[31]_i_6 
       (.I0(DPO0_out),
        .I1(\opcode_reg_reg[20] ),
        .I2(data_out2B[31]),
        .I3(\opcode_reg_reg[20]_0 ),
        .O(reg_target[31]));
  LUT4 #(
    .INIT(16'hE200)) 
    \bram_firmware_code_data_write_reg[31]_i_7 
       (.I0(DPO96_out),
        .I1(\opcode_reg_reg[20] ),
        .I2(data_out2B[7]),
        .I3(\opcode_reg_reg[20]_0 ),
        .O(reg_target[7]));
  FDCE intr_enable_reg_reg
       (.C(clk),
        .CE(1'b1),
        .CLR(\reset_reg_reg[3] ),
        .D(intr_enable_reg_reg_0),
        .Q(intr_enable));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(\opcode_reg_reg[29] ),
        .DPO(DPO),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[0].reg_bit1a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[0].reg_bit1b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(\opcode_reg_reg[29] ),
        .DPO(data_out1B[0]),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[0].reg_bit1b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[0].reg_bit2a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(\opcode_reg_reg[29] ),
        .DPO(DPO124_out),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[0].reg_bit2a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[0].reg_bit2b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(\opcode_reg_reg[29] ),
        .DPO(data_out2B[0]),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[0].reg_bit2b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[10].reg_bit1a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[9]),
        .DPO(DPO86_out),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[10].reg_bit1a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[10].reg_bit1b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[9]),
        .DPO(data_out1B[10]),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[10].reg_bit1b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[10].reg_bit2a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[9]),
        .DPO(DPO84_out),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[10].reg_bit2a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[10].reg_bit2b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[9]),
        .DPO(data_out2B[10]),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[10].reg_bit2b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[11].reg_bit1a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[10]),
        .DPO(DPO82_out),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[11].reg_bit1a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[11].reg_bit1b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[10]),
        .DPO(data_out1B[11]),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[11].reg_bit1b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[11].reg_bit2a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[10]),
        .DPO(DPO80_out),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[11].reg_bit2a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[11].reg_bit2b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[10]),
        .DPO(data_out2B[11]),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[11].reg_bit2b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[12].reg_bit1a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[11]),
        .DPO(DPO78_out),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[12].reg_bit1a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[12].reg_bit1b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[11]),
        .DPO(data_out1B[12]),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[12].reg_bit1b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[12].reg_bit2a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[11]),
        .DPO(DPO76_out),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[12].reg_bit2a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[12].reg_bit2b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[11]),
        .DPO(data_out2B[12]),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[12].reg_bit2b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[13].reg_bit1a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[12]),
        .DPO(DPO74_out),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[13].reg_bit1a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[13].reg_bit1b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[12]),
        .DPO(data_out1B[13]),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[13].reg_bit1b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[13].reg_bit2a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[12]),
        .DPO(DPO72_out),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[13].reg_bit2a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[13].reg_bit2b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[12]),
        .DPO(data_out2B[13]),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[13].reg_bit2b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[14].reg_bit1a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[13]),
        .DPO(DPO70_out),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[14].reg_bit1a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[14].reg_bit1b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[13]),
        .DPO(data_out1B[14]),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[14].reg_bit1b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[14].reg_bit2a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[13]),
        .DPO(DPO68_out),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[14].reg_bit2a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[14].reg_bit2b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[13]),
        .DPO(data_out2B[14]),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[14].reg_bit2b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[15].reg_bit1a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[14]),
        .DPO(DPO66_out),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[15].reg_bit1a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[15].reg_bit1b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[14]),
        .DPO(data_out1B[15]),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[15].reg_bit1b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[15].reg_bit2a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[14]),
        .DPO(DPO64_out),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[15].reg_bit2a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[15].reg_bit2b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[14]),
        .DPO(data_out2B[15]),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[15].reg_bit2b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[16].reg_bit1a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[15]),
        .DPO(DPO62_out),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[16].reg_bit1a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[16].reg_bit1b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[15]),
        .DPO(data_out1B[16]),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[16].reg_bit1b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[16].reg_bit2a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[15]),
        .DPO(DPO60_out),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[16].reg_bit2a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[16].reg_bit2b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[15]),
        .DPO(data_out2B[16]),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[16].reg_bit2b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[17].reg_bit1a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[16]),
        .DPO(DPO58_out),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[17].reg_bit1a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[17].reg_bit1b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[16]),
        .DPO(data_out1B[17]),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[17].reg_bit1b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[17].reg_bit2a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[16]),
        .DPO(DPO56_out),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[17].reg_bit2a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[17].reg_bit2b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[16]),
        .DPO(data_out2B[17]),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[17].reg_bit2b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[18].reg_bit1a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[17]),
        .DPO(DPO54_out),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[18].reg_bit1a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[18].reg_bit1b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[17]),
        .DPO(data_out1B[18]),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[18].reg_bit1b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[18].reg_bit2a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[17]),
        .DPO(DPO52_out),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[18].reg_bit2a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[18].reg_bit2b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[17]),
        .DPO(data_out2B[18]),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[18].reg_bit2b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[19].reg_bit1a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[18]),
        .DPO(DPO50_out),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[19].reg_bit1a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[19].reg_bit1b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[18]),
        .DPO(data_out1B[19]),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[19].reg_bit1b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[19].reg_bit2a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[18]),
        .DPO(DPO48_out),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[19].reg_bit2a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[19].reg_bit2b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[18]),
        .DPO(data_out2B[19]),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[19].reg_bit2b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[1].reg_bit1a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[0]),
        .DPO(DPO122_out),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[1].reg_bit1a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[1].reg_bit1b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[0]),
        .DPO(data_out1B[1]),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[1].reg_bit1b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[1].reg_bit2a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[0]),
        .DPO(DPO120_out),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[1].reg_bit2a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[1].reg_bit2b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[0]),
        .DPO(data_out2B[1]),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[1].reg_bit2b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[20].reg_bit1a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[19]),
        .DPO(DPO46_out),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[20].reg_bit1a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[20].reg_bit1b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[19]),
        .DPO(data_out1B[20]),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[20].reg_bit1b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[20].reg_bit2a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[19]),
        .DPO(DPO44_out),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[20].reg_bit2a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[20].reg_bit2b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[19]),
        .DPO(data_out2B[20]),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[20].reg_bit2b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[21].reg_bit1a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[20]),
        .DPO(DPO42_out),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[21].reg_bit1a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[21].reg_bit1b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[20]),
        .DPO(data_out1B[21]),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[21].reg_bit1b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[21].reg_bit2a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[20]),
        .DPO(DPO40_out),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[21].reg_bit2a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[21].reg_bit2b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[20]),
        .DPO(data_out2B[21]),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[21].reg_bit2b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[22].reg_bit1a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[21]),
        .DPO(DPO38_out),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[22].reg_bit1a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[22].reg_bit1b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[21]),
        .DPO(data_out1B[22]),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[22].reg_bit1b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[22].reg_bit2a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[21]),
        .DPO(DPO36_out),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[22].reg_bit2a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[22].reg_bit2b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[21]),
        .DPO(data_out2B[22]),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[22].reg_bit2b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[23].reg_bit1a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[22]),
        .DPO(DPO34_out),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[23].reg_bit1a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[23].reg_bit1b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[22]),
        .DPO(data_out1B[23]),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[23].reg_bit1b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[23].reg_bit2a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[22]),
        .DPO(DPO32_out),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[23].reg_bit2a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[23].reg_bit2b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[22]),
        .DPO(data_out2B[23]),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[23].reg_bit2b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[24].reg_bit1a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[23]),
        .DPO(DPO30_out),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[24].reg_bit1a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[24].reg_bit1b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[23]),
        .DPO(data_out1B[24]),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[24].reg_bit1b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[24].reg_bit2a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[23]),
        .DPO(DPO28_out),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[24].reg_bit2a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[24].reg_bit2b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[23]),
        .DPO(data_out2B[24]),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[24].reg_bit2b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[25].reg_bit1a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[24]),
        .DPO(DPO26_out),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[25].reg_bit1a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[25].reg_bit1b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[24]),
        .DPO(data_out1B[25]),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[25].reg_bit1b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[25].reg_bit2a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[24]),
        .DPO(DPO24_out),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[25].reg_bit2a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[25].reg_bit2b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[24]),
        .DPO(data_out2B[25]),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[25].reg_bit2b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[26].reg_bit1a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[25]),
        .DPO(DPO22_out),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[26].reg_bit1a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[26].reg_bit1b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[25]),
        .DPO(data_out1B[26]),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[26].reg_bit1b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[26].reg_bit2a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[25]),
        .DPO(DPO20_out),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[26].reg_bit2a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[26].reg_bit2b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[25]),
        .DPO(data_out2B[26]),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[26].reg_bit2b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[27].reg_bit1a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[26]),
        .DPO(DPO18_out),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[27].reg_bit1a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[27].reg_bit1b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[26]),
        .DPO(data_out1B[27]),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[27].reg_bit1b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[27].reg_bit2a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[26]),
        .DPO(DPO16_out),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[27].reg_bit2a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[27].reg_bit2b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[26]),
        .DPO(data_out2B[27]),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[27].reg_bit2b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[28].reg_bit1a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[27]),
        .DPO(DPO14_out),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[28].reg_bit1a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[28].reg_bit1b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[27]),
        .DPO(data_out1B[28]),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[28].reg_bit1b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[28].reg_bit2a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[27]),
        .DPO(DPO12_out),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[28].reg_bit2a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[28].reg_bit2b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[27]),
        .DPO(data_out2B[28]),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[28].reg_bit2b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[29].reg_bit1a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[28]),
        .DPO(DPO10_out),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[29].reg_bit1a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[29].reg_bit1b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[28]),
        .DPO(data_out1B[29]),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[29].reg_bit1b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[29].reg_bit2a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[28]),
        .DPO(DPO8_out),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[29].reg_bit2a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[29].reg_bit2b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[28]),
        .DPO(data_out2B[29]),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[29].reg_bit2b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[2].reg_bit1a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[1]),
        .DPO(DPO118_out),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[2].reg_bit1a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[2].reg_bit1b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[1]),
        .DPO(data_out1B[2]),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[2].reg_bit1b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[2].reg_bit2a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[1]),
        .DPO(DPO116_out),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[2].reg_bit2a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[2].reg_bit2b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[1]),
        .DPO(data_out2B[2]),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[2].reg_bit2b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[30].reg_bit1a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[29]),
        .DPO(DPO6_out),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[30].reg_bit1a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[30].reg_bit1b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[29]),
        .DPO(data_out1B[30]),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[30].reg_bit1b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[30].reg_bit2a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[29]),
        .DPO(DPO4_out),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[30].reg_bit2a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[30].reg_bit2b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[29]),
        .DPO(data_out2B[30]),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[30].reg_bit2b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[31].reg_bit1a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[30]),
        .DPO(DPO2_out),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[31].reg_bit1a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[31].reg_bit1b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[30]),
        .DPO(data_out1B[31]),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[31].reg_bit1b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[31].reg_bit2a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[30]),
        .DPO(DPO0_out),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[31].reg_bit2a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[31].reg_bit2b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[30]),
        .DPO(data_out2B[31]),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[31].reg_bit2b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[3].reg_bit1a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[2]),
        .DPO(DPO114_out),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[3].reg_bit1a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[3].reg_bit1b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[2]),
        .DPO(data_out1B[3]),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[3].reg_bit1b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[3].reg_bit2a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[2]),
        .DPO(DPO112_out),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[3].reg_bit2a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[3].reg_bit2b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[2]),
        .DPO(data_out2B[3]),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[3].reg_bit2b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[4].reg_bit1a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[3]),
        .DPO(DPO110_out),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[4].reg_bit1a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[4].reg_bit1b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[3]),
        .DPO(data_out1B[4]),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[4].reg_bit1b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[4].reg_bit2a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[3]),
        .DPO(DPO108_out),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[4].reg_bit2a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[4].reg_bit2b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[3]),
        .DPO(data_out2B[4]),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[4].reg_bit2b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[5].reg_bit1a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[4]),
        .DPO(DPO106_out),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[5].reg_bit1a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[5].reg_bit1b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[4]),
        .DPO(data_out1B[5]),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[5].reg_bit1b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[5].reg_bit2a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[4]),
        .DPO(DPO104_out),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[5].reg_bit2a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[5].reg_bit2b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[4]),
        .DPO(data_out2B[5]),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[5].reg_bit2b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[6].reg_bit1a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[5]),
        .DPO(DPO102_out),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[6].reg_bit1a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[6].reg_bit1b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[5]),
        .DPO(data_out1B[6]),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[6].reg_bit1b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[6].reg_bit2a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[5]),
        .DPO(DPO100_out),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[6].reg_bit2a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[6].reg_bit2b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[5]),
        .DPO(data_out2B[6]),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[6].reg_bit2b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[7].reg_bit1a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[6]),
        .DPO(DPO98_out),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[7].reg_bit1a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[7].reg_bit1b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[6]),
        .DPO(data_out1B[7]),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[7].reg_bit1b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[7].reg_bit2a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[6]),
        .DPO(DPO96_out),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[7].reg_bit2a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[7].reg_bit2b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[6]),
        .DPO(data_out2B[7]),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[7].reg_bit2b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[8].reg_bit1a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[7]),
        .DPO(DPO94_out),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[8].reg_bit1a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[8].reg_bit1b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[7]),
        .DPO(data_out1B[8]),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[8].reg_bit1b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[8].reg_bit2a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[7]),
        .DPO(DPO92_out),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[8].reg_bit2a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[8].reg_bit2b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[7]),
        .DPO(data_out2B[8]),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[8].reg_bit2b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[9].reg_bit1a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[8]),
        .DPO(DPO90_out),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[9].reg_bit1a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[9].reg_bit1b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[8]),
        .DPO(data_out1B[9]),
        .DPRA0(DPRA0),
        .DPRA1(DPRA1),
        .DPRA2(DPRA2),
        .DPRA3(DPRA3),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[9].reg_bit1b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[9].reg_bit2a 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[8]),
        .DPO(DPO88_out),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[9].reg_bit2a_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(WE));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  RAM32X1D #(
    .INIT(32'h00000000),
    .IS_WCLK_INVERTED(1'b0)) 
    \xilinx_16x1d.reg_loop[9].reg_bit2b 
       (.A0(\opcode_reg_reg[31] ),
        .A1(\opcode_reg_reg[2] ),
        .A2(\opcode_reg_reg[2]_0 ),
        .A3(\opcode_reg_reg[2]_1 ),
        .A4(1'b0),
        .D(reg_dest[8]),
        .DPO(data_out2B[9]),
        .DPRA0(\opcode_reg_reg[16] ),
        .DPRA1(\opcode_reg_reg[17] ),
        .DPRA2(\opcode_reg_reg[18] ),
        .DPRA3(\opcode_reg_reg[19] ),
        .DPRA4(1'b0),
        .SPO(\NLW_xilinx_16x1d.reg_loop[9].reg_bit2b_SPO_UNCONNECTED ),
        .WCLK(clk),
        .WE(\opcode_reg_reg[31]_0 ));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
