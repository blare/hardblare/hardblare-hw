--Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
--Date        : Tue Dec 10 21:24:50 2019
--Host        : HardBlare-Workstation running 64-bit Ubuntu 16.04.6 LTS
--Command     : generate_target Dispatcher_Plasma_MIPS_HardBlare.bd
--Design      : Dispatcher_Plasma_MIPS_HardBlare
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity Dispatcher_Plasma_MIPS_HardBlare is
  port (
    bram_annotations_address : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_annotations_bytes_selection : out STD_LOGIC_VECTOR ( 3 downto 0 );
    bram_annotations_clk : out STD_LOGIC;
    bram_annotations_data_read : in STD_LOGIC;
    bram_annotations_data_write : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_annotations_en : out STD_LOGIC;
    bram_annotations_reset : out STD_LOGIC;
    bram_bbt_annotations_address : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_bbt_annotations_bytes_selection : out STD_LOGIC_VECTOR ( 3 downto 0 );
    bram_bbt_annotations_clk : out STD_LOGIC;
    bram_bbt_annotations_data_read : in STD_LOGIC;
    bram_bbt_annotations_data_write : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_bbt_annotations_en : out STD_LOGIC;
    bram_bbt_annotations_reset : out STD_LOGIC;
    bram_firmware_code_address : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_firmware_code_bytes_selection : out STD_LOGIC_VECTOR ( 3 downto 0 );
    bram_firmware_code_clk : out STD_LOGIC;
    bram_firmware_code_data_read : in STD_LOGIC;
    bram_firmware_code_data_write : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_firmware_code_en : out STD_LOGIC;
    bram_firmware_code_reset : out STD_LOGIC;
    bram_kernel_to_monitor_address : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_kernel_to_monitor_bytes_selection : out STD_LOGIC_VECTOR ( 3 downto 0 );
    bram_kernel_to_monitor_clk : out STD_LOGIC;
    bram_kernel_to_monitor_data_read : in STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_kernel_to_monitor_data_write : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_kernel_to_monitor_en : out STD_LOGIC;
    bram_kernel_to_monitor_reset : out STD_LOGIC;
    bram_monitor_to_kernel_address : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_monitor_to_kernel_bytes_selection : out STD_LOGIC_VECTOR ( 3 downto 0 );
    bram_monitor_to_kernel_clk : out STD_LOGIC;
    bram_monitor_to_kernel_data_read : in STD_LOGIC;
    bram_monitor_to_kernel_data_write : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_monitor_to_kernel_en : out STD_LOGIC;
    bram_monitor_to_kernel_reset : out STD_LOGIC;
    bram_ptm_traces_address : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_ptm_traces_bytes_selection : out STD_LOGIC_VECTOR ( 3 downto 0 );
    bram_ptm_traces_clk : out STD_LOGIC;
    bram_ptm_traces_data_read : in STD_LOGIC;
    bram_ptm_traces_data_write : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_ptm_traces_en : out STD_LOGIC;
    bram_ptm_traces_reset : out STD_LOGIC;
    clk : in STD_LOGIC;
    reset : in STD_LOGIC
  );
  attribute CORE_GENERATION_INFO : string;
  attribute CORE_GENERATION_INFO of Dispatcher_Plasma_MIPS_HardBlare : entity is "Dispatcher_Plasma_MIPS_HardBlare,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLibrary=BlockDiagram,x_ipName=Dispatcher_Plasma_MIPS_HardBlare,x_ipVersion=1.00.a,x_ipLanguage=VHDL,numBlks=1,numReposBlks=1,numNonXlnxBlks=0,numHierBlks=0,maxHierDepth=0,numSysgenBlks=0,numHlsBlks=0,numHdlrefBlks=1,numPkgbdBlks=0,bdsource=USER,synth_mode=OOC_per_IP}";
  attribute HW_HANDOFF : string;
  attribute HW_HANDOFF of Dispatcher_Plasma_MIPS_HardBlare : entity is "Dispatcher_Plasma_MIPS_HardBlare.hwdef";
end Dispatcher_Plasma_MIPS_HardBlare;

architecture STRUCTURE of Dispatcher_Plasma_MIPS_HardBlare is
  component Dispatcher_Plasma_MIPS_HardBlare_plasma_0_0 is
  port (
    clk : in STD_LOGIC;
    reset : in STD_LOGIC;
    bram_firmware_code_en : out STD_LOGIC;
    bram_firmware_code_reset : out STD_LOGIC;
    bram_firmware_code_clk : out STD_LOGIC;
    bram_firmware_code_address : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_firmware_code_data_write : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_firmware_code_bytes_selection : out STD_LOGIC_VECTOR ( 3 downto 0 );
    bram_firmware_code_data_read : in STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_ptm_traces_en : out STD_LOGIC;
    bram_ptm_traces_reset : out STD_LOGIC;
    bram_ptm_traces_clk : out STD_LOGIC;
    bram_ptm_traces_address : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_ptm_traces_data_write : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_ptm_traces_bytes_selection : out STD_LOGIC_VECTOR ( 3 downto 0 );
    bram_ptm_traces_data_read : in STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_bbt_annotations_en : out STD_LOGIC;
    bram_bbt_annotations_reset : out STD_LOGIC;
    bram_bbt_annotations_clk : out STD_LOGIC;
    bram_bbt_annotations_address : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_bbt_annotations_data_write : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_bbt_annotations_bytes_selection : out STD_LOGIC_VECTOR ( 3 downto 0 );
    bram_bbt_annotations_data_read : in STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_annotations_en : out STD_LOGIC;
    bram_annotations_reset : out STD_LOGIC;
    bram_annotations_clk : out STD_LOGIC;
    bram_annotations_address : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_annotations_data_write : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_annotations_bytes_selection : out STD_LOGIC_VECTOR ( 3 downto 0 );
    bram_annotations_data_read : in STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_monitor_to_kernel_en : out STD_LOGIC;
    bram_monitor_to_kernel_reset : out STD_LOGIC;
    bram_monitor_to_kernel_clk : out STD_LOGIC;
    bram_monitor_to_kernel_address : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_monitor_to_kernel_data_write : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_monitor_to_kernel_bytes_selection : out STD_LOGIC_VECTOR ( 3 downto 0 );
    bram_monitor_to_kernel_data_read : in STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_kernel_to_monitor_en : out STD_LOGIC;
    bram_kernel_to_monitor_reset : out STD_LOGIC;
    bram_kernel_to_monitor_clk : out STD_LOGIC;
    bram_kernel_to_monitor_address : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_kernel_to_monitor_data_write : out STD_LOGIC_VECTOR ( 31 downto 0 );
    bram_kernel_to_monitor_bytes_selection : out STD_LOGIC_VECTOR ( 3 downto 0 );
    bram_kernel_to_monitor_data_read : in STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  end component Dispatcher_Plasma_MIPS_HardBlare_plasma_0_0;
  signal bram_annotations_data_read_1 : STD_LOGIC;
  signal bram_bbt_annotations_data_read_1 : STD_LOGIC;
  signal bram_firmware_code_data_read_1 : STD_LOGIC;
  signal bram_kernel_to_monitor_data_read_1 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal bram_monitor_to_kernel_data_read_1 : STD_LOGIC;
  signal bram_ptm_traces_data_read_1 : STD_LOGIC;
  signal clk_1 : STD_LOGIC;
  signal plasma_0_bram_annotations_address : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal plasma_0_bram_annotations_bytes_selection : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal plasma_0_bram_annotations_clk : STD_LOGIC;
  signal plasma_0_bram_annotations_data_write : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal plasma_0_bram_annotations_en : STD_LOGIC;
  signal plasma_0_bram_annotations_reset : STD_LOGIC;
  signal plasma_0_bram_bbt_annotations_address : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal plasma_0_bram_bbt_annotations_bytes_selection : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal plasma_0_bram_bbt_annotations_clk : STD_LOGIC;
  signal plasma_0_bram_bbt_annotations_data_write : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal plasma_0_bram_bbt_annotations_en : STD_LOGIC;
  signal plasma_0_bram_bbt_annotations_reset : STD_LOGIC;
  signal plasma_0_bram_firmware_code_address : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal plasma_0_bram_firmware_code_bytes_selection : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal plasma_0_bram_firmware_code_clk : STD_LOGIC;
  signal plasma_0_bram_firmware_code_data_write : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal plasma_0_bram_firmware_code_en : STD_LOGIC;
  signal plasma_0_bram_firmware_code_reset : STD_LOGIC;
  signal plasma_0_bram_kernel_to_monitor_address : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal plasma_0_bram_kernel_to_monitor_bytes_selection : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal plasma_0_bram_kernel_to_monitor_clk : STD_LOGIC;
  signal plasma_0_bram_kernel_to_monitor_data_write : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal plasma_0_bram_kernel_to_monitor_en : STD_LOGIC;
  signal plasma_0_bram_kernel_to_monitor_reset : STD_LOGIC;
  signal plasma_0_bram_monitor_to_kernel_address : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal plasma_0_bram_monitor_to_kernel_bytes_selection : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal plasma_0_bram_monitor_to_kernel_clk : STD_LOGIC;
  signal plasma_0_bram_monitor_to_kernel_data_write : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal plasma_0_bram_monitor_to_kernel_en : STD_LOGIC;
  signal plasma_0_bram_monitor_to_kernel_reset : STD_LOGIC;
  signal plasma_0_bram_ptm_traces_address : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal plasma_0_bram_ptm_traces_bytes_selection : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal plasma_0_bram_ptm_traces_clk : STD_LOGIC;
  signal plasma_0_bram_ptm_traces_data_write : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal plasma_0_bram_ptm_traces_en : STD_LOGIC;
  signal plasma_0_bram_ptm_traces_reset : STD_LOGIC;
  signal reset_1 : STD_LOGIC;
begin
  bram_annotations_address(31 downto 0) <= plasma_0_bram_annotations_address(31 downto 0);
  bram_annotations_bytes_selection(3 downto 0) <= plasma_0_bram_annotations_bytes_selection(3 downto 0);
  bram_annotations_clk <= plasma_0_bram_annotations_clk;
  bram_annotations_data_read_1 <= bram_annotations_data_read;
  bram_annotations_data_write(31 downto 0) <= plasma_0_bram_annotations_data_write(31 downto 0);
  bram_annotations_en <= plasma_0_bram_annotations_en;
  bram_annotations_reset <= plasma_0_bram_annotations_reset;
  bram_bbt_annotations_address(31 downto 0) <= plasma_0_bram_bbt_annotations_address(31 downto 0);
  bram_bbt_annotations_bytes_selection(3 downto 0) <= plasma_0_bram_bbt_annotations_bytes_selection(3 downto 0);
  bram_bbt_annotations_clk <= plasma_0_bram_bbt_annotations_clk;
  bram_bbt_annotations_data_read_1 <= bram_bbt_annotations_data_read;
  bram_bbt_annotations_data_write(31 downto 0) <= plasma_0_bram_bbt_annotations_data_write(31 downto 0);
  bram_bbt_annotations_en <= plasma_0_bram_bbt_annotations_en;
  bram_bbt_annotations_reset <= plasma_0_bram_bbt_annotations_reset;
  bram_firmware_code_address(31 downto 0) <= plasma_0_bram_firmware_code_address(31 downto 0);
  bram_firmware_code_bytes_selection(3 downto 0) <= plasma_0_bram_firmware_code_bytes_selection(3 downto 0);
  bram_firmware_code_clk <= plasma_0_bram_firmware_code_clk;
  bram_firmware_code_data_read_1 <= bram_firmware_code_data_read;
  bram_firmware_code_data_write(31 downto 0) <= plasma_0_bram_firmware_code_data_write(31 downto 0);
  bram_firmware_code_en <= plasma_0_bram_firmware_code_en;
  bram_firmware_code_reset <= plasma_0_bram_firmware_code_reset;
  bram_kernel_to_monitor_address(31 downto 0) <= plasma_0_bram_kernel_to_monitor_address(31 downto 0);
  bram_kernel_to_monitor_bytes_selection(3 downto 0) <= plasma_0_bram_kernel_to_monitor_bytes_selection(3 downto 0);
  bram_kernel_to_monitor_clk <= plasma_0_bram_kernel_to_monitor_clk;
  bram_kernel_to_monitor_data_read_1(31 downto 0) <= bram_kernel_to_monitor_data_read(31 downto 0);
  bram_kernel_to_monitor_data_write(31 downto 0) <= plasma_0_bram_kernel_to_monitor_data_write(31 downto 0);
  bram_kernel_to_monitor_en <= plasma_0_bram_kernel_to_monitor_en;
  bram_kernel_to_monitor_reset <= plasma_0_bram_kernel_to_monitor_reset;
  bram_monitor_to_kernel_address(31 downto 0) <= plasma_0_bram_monitor_to_kernel_address(31 downto 0);
  bram_monitor_to_kernel_bytes_selection(3 downto 0) <= plasma_0_bram_monitor_to_kernel_bytes_selection(3 downto 0);
  bram_monitor_to_kernel_clk <= plasma_0_bram_monitor_to_kernel_clk;
  bram_monitor_to_kernel_data_read_1 <= bram_monitor_to_kernel_data_read;
  bram_monitor_to_kernel_data_write(31 downto 0) <= plasma_0_bram_monitor_to_kernel_data_write(31 downto 0);
  bram_monitor_to_kernel_en <= plasma_0_bram_monitor_to_kernel_en;
  bram_monitor_to_kernel_reset <= plasma_0_bram_monitor_to_kernel_reset;
  bram_ptm_traces_address(31 downto 0) <= plasma_0_bram_ptm_traces_address(31 downto 0);
  bram_ptm_traces_bytes_selection(3 downto 0) <= plasma_0_bram_ptm_traces_bytes_selection(3 downto 0);
  bram_ptm_traces_clk <= plasma_0_bram_ptm_traces_clk;
  bram_ptm_traces_data_read_1 <= bram_ptm_traces_data_read;
  bram_ptm_traces_data_write(31 downto 0) <= plasma_0_bram_ptm_traces_data_write(31 downto 0);
  bram_ptm_traces_en <= plasma_0_bram_ptm_traces_en;
  bram_ptm_traces_reset <= plasma_0_bram_ptm_traces_reset;
  clk_1 <= clk;
  reset_1 <= reset;
plasma_0: component Dispatcher_Plasma_MIPS_HardBlare_plasma_0_0
     port map (
      bram_annotations_address(31 downto 0) => plasma_0_bram_annotations_address(31 downto 0),
      bram_annotations_bytes_selection(3 downto 0) => plasma_0_bram_annotations_bytes_selection(3 downto 0),
      bram_annotations_clk => plasma_0_bram_annotations_clk,
      bram_annotations_data_read(31) => bram_annotations_data_read_1,
      bram_annotations_data_read(30) => bram_annotations_data_read_1,
      bram_annotations_data_read(29) => bram_annotations_data_read_1,
      bram_annotations_data_read(28) => bram_annotations_data_read_1,
      bram_annotations_data_read(27) => bram_annotations_data_read_1,
      bram_annotations_data_read(26) => bram_annotations_data_read_1,
      bram_annotations_data_read(25) => bram_annotations_data_read_1,
      bram_annotations_data_read(24) => bram_annotations_data_read_1,
      bram_annotations_data_read(23) => bram_annotations_data_read_1,
      bram_annotations_data_read(22) => bram_annotations_data_read_1,
      bram_annotations_data_read(21) => bram_annotations_data_read_1,
      bram_annotations_data_read(20) => bram_annotations_data_read_1,
      bram_annotations_data_read(19) => bram_annotations_data_read_1,
      bram_annotations_data_read(18) => bram_annotations_data_read_1,
      bram_annotations_data_read(17) => bram_annotations_data_read_1,
      bram_annotations_data_read(16) => bram_annotations_data_read_1,
      bram_annotations_data_read(15) => bram_annotations_data_read_1,
      bram_annotations_data_read(14) => bram_annotations_data_read_1,
      bram_annotations_data_read(13) => bram_annotations_data_read_1,
      bram_annotations_data_read(12) => bram_annotations_data_read_1,
      bram_annotations_data_read(11) => bram_annotations_data_read_1,
      bram_annotations_data_read(10) => bram_annotations_data_read_1,
      bram_annotations_data_read(9) => bram_annotations_data_read_1,
      bram_annotations_data_read(8) => bram_annotations_data_read_1,
      bram_annotations_data_read(7) => bram_annotations_data_read_1,
      bram_annotations_data_read(6) => bram_annotations_data_read_1,
      bram_annotations_data_read(5) => bram_annotations_data_read_1,
      bram_annotations_data_read(4) => bram_annotations_data_read_1,
      bram_annotations_data_read(3) => bram_annotations_data_read_1,
      bram_annotations_data_read(2) => bram_annotations_data_read_1,
      bram_annotations_data_read(1) => bram_annotations_data_read_1,
      bram_annotations_data_read(0) => bram_annotations_data_read_1,
      bram_annotations_data_write(31 downto 0) => plasma_0_bram_annotations_data_write(31 downto 0),
      bram_annotations_en => plasma_0_bram_annotations_en,
      bram_annotations_reset => plasma_0_bram_annotations_reset,
      bram_bbt_annotations_address(31 downto 0) => plasma_0_bram_bbt_annotations_address(31 downto 0),
      bram_bbt_annotations_bytes_selection(3 downto 0) => plasma_0_bram_bbt_annotations_bytes_selection(3 downto 0),
      bram_bbt_annotations_clk => plasma_0_bram_bbt_annotations_clk,
      bram_bbt_annotations_data_read(31) => bram_bbt_annotations_data_read_1,
      bram_bbt_annotations_data_read(30) => bram_bbt_annotations_data_read_1,
      bram_bbt_annotations_data_read(29) => bram_bbt_annotations_data_read_1,
      bram_bbt_annotations_data_read(28) => bram_bbt_annotations_data_read_1,
      bram_bbt_annotations_data_read(27) => bram_bbt_annotations_data_read_1,
      bram_bbt_annotations_data_read(26) => bram_bbt_annotations_data_read_1,
      bram_bbt_annotations_data_read(25) => bram_bbt_annotations_data_read_1,
      bram_bbt_annotations_data_read(24) => bram_bbt_annotations_data_read_1,
      bram_bbt_annotations_data_read(23) => bram_bbt_annotations_data_read_1,
      bram_bbt_annotations_data_read(22) => bram_bbt_annotations_data_read_1,
      bram_bbt_annotations_data_read(21) => bram_bbt_annotations_data_read_1,
      bram_bbt_annotations_data_read(20) => bram_bbt_annotations_data_read_1,
      bram_bbt_annotations_data_read(19) => bram_bbt_annotations_data_read_1,
      bram_bbt_annotations_data_read(18) => bram_bbt_annotations_data_read_1,
      bram_bbt_annotations_data_read(17) => bram_bbt_annotations_data_read_1,
      bram_bbt_annotations_data_read(16) => bram_bbt_annotations_data_read_1,
      bram_bbt_annotations_data_read(15) => bram_bbt_annotations_data_read_1,
      bram_bbt_annotations_data_read(14) => bram_bbt_annotations_data_read_1,
      bram_bbt_annotations_data_read(13) => bram_bbt_annotations_data_read_1,
      bram_bbt_annotations_data_read(12) => bram_bbt_annotations_data_read_1,
      bram_bbt_annotations_data_read(11) => bram_bbt_annotations_data_read_1,
      bram_bbt_annotations_data_read(10) => bram_bbt_annotations_data_read_1,
      bram_bbt_annotations_data_read(9) => bram_bbt_annotations_data_read_1,
      bram_bbt_annotations_data_read(8) => bram_bbt_annotations_data_read_1,
      bram_bbt_annotations_data_read(7) => bram_bbt_annotations_data_read_1,
      bram_bbt_annotations_data_read(6) => bram_bbt_annotations_data_read_1,
      bram_bbt_annotations_data_read(5) => bram_bbt_annotations_data_read_1,
      bram_bbt_annotations_data_read(4) => bram_bbt_annotations_data_read_1,
      bram_bbt_annotations_data_read(3) => bram_bbt_annotations_data_read_1,
      bram_bbt_annotations_data_read(2) => bram_bbt_annotations_data_read_1,
      bram_bbt_annotations_data_read(1) => bram_bbt_annotations_data_read_1,
      bram_bbt_annotations_data_read(0) => bram_bbt_annotations_data_read_1,
      bram_bbt_annotations_data_write(31 downto 0) => plasma_0_bram_bbt_annotations_data_write(31 downto 0),
      bram_bbt_annotations_en => plasma_0_bram_bbt_annotations_en,
      bram_bbt_annotations_reset => plasma_0_bram_bbt_annotations_reset,
      bram_firmware_code_address(31 downto 0) => plasma_0_bram_firmware_code_address(31 downto 0),
      bram_firmware_code_bytes_selection(3 downto 0) => plasma_0_bram_firmware_code_bytes_selection(3 downto 0),
      bram_firmware_code_clk => plasma_0_bram_firmware_code_clk,
      bram_firmware_code_data_read(31) => bram_firmware_code_data_read_1,
      bram_firmware_code_data_read(30) => bram_firmware_code_data_read_1,
      bram_firmware_code_data_read(29) => bram_firmware_code_data_read_1,
      bram_firmware_code_data_read(28) => bram_firmware_code_data_read_1,
      bram_firmware_code_data_read(27) => bram_firmware_code_data_read_1,
      bram_firmware_code_data_read(26) => bram_firmware_code_data_read_1,
      bram_firmware_code_data_read(25) => bram_firmware_code_data_read_1,
      bram_firmware_code_data_read(24) => bram_firmware_code_data_read_1,
      bram_firmware_code_data_read(23) => bram_firmware_code_data_read_1,
      bram_firmware_code_data_read(22) => bram_firmware_code_data_read_1,
      bram_firmware_code_data_read(21) => bram_firmware_code_data_read_1,
      bram_firmware_code_data_read(20) => bram_firmware_code_data_read_1,
      bram_firmware_code_data_read(19) => bram_firmware_code_data_read_1,
      bram_firmware_code_data_read(18) => bram_firmware_code_data_read_1,
      bram_firmware_code_data_read(17) => bram_firmware_code_data_read_1,
      bram_firmware_code_data_read(16) => bram_firmware_code_data_read_1,
      bram_firmware_code_data_read(15) => bram_firmware_code_data_read_1,
      bram_firmware_code_data_read(14) => bram_firmware_code_data_read_1,
      bram_firmware_code_data_read(13) => bram_firmware_code_data_read_1,
      bram_firmware_code_data_read(12) => bram_firmware_code_data_read_1,
      bram_firmware_code_data_read(11) => bram_firmware_code_data_read_1,
      bram_firmware_code_data_read(10) => bram_firmware_code_data_read_1,
      bram_firmware_code_data_read(9) => bram_firmware_code_data_read_1,
      bram_firmware_code_data_read(8) => bram_firmware_code_data_read_1,
      bram_firmware_code_data_read(7) => bram_firmware_code_data_read_1,
      bram_firmware_code_data_read(6) => bram_firmware_code_data_read_1,
      bram_firmware_code_data_read(5) => bram_firmware_code_data_read_1,
      bram_firmware_code_data_read(4) => bram_firmware_code_data_read_1,
      bram_firmware_code_data_read(3) => bram_firmware_code_data_read_1,
      bram_firmware_code_data_read(2) => bram_firmware_code_data_read_1,
      bram_firmware_code_data_read(1) => bram_firmware_code_data_read_1,
      bram_firmware_code_data_read(0) => bram_firmware_code_data_read_1,
      bram_firmware_code_data_write(31 downto 0) => plasma_0_bram_firmware_code_data_write(31 downto 0),
      bram_firmware_code_en => plasma_0_bram_firmware_code_en,
      bram_firmware_code_reset => plasma_0_bram_firmware_code_reset,
      bram_kernel_to_monitor_address(31 downto 0) => plasma_0_bram_kernel_to_monitor_address(31 downto 0),
      bram_kernel_to_monitor_bytes_selection(3 downto 0) => plasma_0_bram_kernel_to_monitor_bytes_selection(3 downto 0),
      bram_kernel_to_monitor_clk => plasma_0_bram_kernel_to_monitor_clk,
      bram_kernel_to_monitor_data_read(31 downto 0) => bram_kernel_to_monitor_data_read_1(31 downto 0),
      bram_kernel_to_monitor_data_write(31 downto 0) => plasma_0_bram_kernel_to_monitor_data_write(31 downto 0),
      bram_kernel_to_monitor_en => plasma_0_bram_kernel_to_monitor_en,
      bram_kernel_to_monitor_reset => plasma_0_bram_kernel_to_monitor_reset,
      bram_monitor_to_kernel_address(31 downto 0) => plasma_0_bram_monitor_to_kernel_address(31 downto 0),
      bram_monitor_to_kernel_bytes_selection(3 downto 0) => plasma_0_bram_monitor_to_kernel_bytes_selection(3 downto 0),
      bram_monitor_to_kernel_clk => plasma_0_bram_monitor_to_kernel_clk,
      bram_monitor_to_kernel_data_read(31) => bram_monitor_to_kernel_data_read_1,
      bram_monitor_to_kernel_data_read(30) => bram_monitor_to_kernel_data_read_1,
      bram_monitor_to_kernel_data_read(29) => bram_monitor_to_kernel_data_read_1,
      bram_monitor_to_kernel_data_read(28) => bram_monitor_to_kernel_data_read_1,
      bram_monitor_to_kernel_data_read(27) => bram_monitor_to_kernel_data_read_1,
      bram_monitor_to_kernel_data_read(26) => bram_monitor_to_kernel_data_read_1,
      bram_monitor_to_kernel_data_read(25) => bram_monitor_to_kernel_data_read_1,
      bram_monitor_to_kernel_data_read(24) => bram_monitor_to_kernel_data_read_1,
      bram_monitor_to_kernel_data_read(23) => bram_monitor_to_kernel_data_read_1,
      bram_monitor_to_kernel_data_read(22) => bram_monitor_to_kernel_data_read_1,
      bram_monitor_to_kernel_data_read(21) => bram_monitor_to_kernel_data_read_1,
      bram_monitor_to_kernel_data_read(20) => bram_monitor_to_kernel_data_read_1,
      bram_monitor_to_kernel_data_read(19) => bram_monitor_to_kernel_data_read_1,
      bram_monitor_to_kernel_data_read(18) => bram_monitor_to_kernel_data_read_1,
      bram_monitor_to_kernel_data_read(17) => bram_monitor_to_kernel_data_read_1,
      bram_monitor_to_kernel_data_read(16) => bram_monitor_to_kernel_data_read_1,
      bram_monitor_to_kernel_data_read(15) => bram_monitor_to_kernel_data_read_1,
      bram_monitor_to_kernel_data_read(14) => bram_monitor_to_kernel_data_read_1,
      bram_monitor_to_kernel_data_read(13) => bram_monitor_to_kernel_data_read_1,
      bram_monitor_to_kernel_data_read(12) => bram_monitor_to_kernel_data_read_1,
      bram_monitor_to_kernel_data_read(11) => bram_monitor_to_kernel_data_read_1,
      bram_monitor_to_kernel_data_read(10) => bram_monitor_to_kernel_data_read_1,
      bram_monitor_to_kernel_data_read(9) => bram_monitor_to_kernel_data_read_1,
      bram_monitor_to_kernel_data_read(8) => bram_monitor_to_kernel_data_read_1,
      bram_monitor_to_kernel_data_read(7) => bram_monitor_to_kernel_data_read_1,
      bram_monitor_to_kernel_data_read(6) => bram_monitor_to_kernel_data_read_1,
      bram_monitor_to_kernel_data_read(5) => bram_monitor_to_kernel_data_read_1,
      bram_monitor_to_kernel_data_read(4) => bram_monitor_to_kernel_data_read_1,
      bram_monitor_to_kernel_data_read(3) => bram_monitor_to_kernel_data_read_1,
      bram_monitor_to_kernel_data_read(2) => bram_monitor_to_kernel_data_read_1,
      bram_monitor_to_kernel_data_read(1) => bram_monitor_to_kernel_data_read_1,
      bram_monitor_to_kernel_data_read(0) => bram_monitor_to_kernel_data_read_1,
      bram_monitor_to_kernel_data_write(31 downto 0) => plasma_0_bram_monitor_to_kernel_data_write(31 downto 0),
      bram_monitor_to_kernel_en => plasma_0_bram_monitor_to_kernel_en,
      bram_monitor_to_kernel_reset => plasma_0_bram_monitor_to_kernel_reset,
      bram_ptm_traces_address(31 downto 0) => plasma_0_bram_ptm_traces_address(31 downto 0),
      bram_ptm_traces_bytes_selection(3 downto 0) => plasma_0_bram_ptm_traces_bytes_selection(3 downto 0),
      bram_ptm_traces_clk => plasma_0_bram_ptm_traces_clk,
      bram_ptm_traces_data_read(31) => bram_ptm_traces_data_read_1,
      bram_ptm_traces_data_read(30) => bram_ptm_traces_data_read_1,
      bram_ptm_traces_data_read(29) => bram_ptm_traces_data_read_1,
      bram_ptm_traces_data_read(28) => bram_ptm_traces_data_read_1,
      bram_ptm_traces_data_read(27) => bram_ptm_traces_data_read_1,
      bram_ptm_traces_data_read(26) => bram_ptm_traces_data_read_1,
      bram_ptm_traces_data_read(25) => bram_ptm_traces_data_read_1,
      bram_ptm_traces_data_read(24) => bram_ptm_traces_data_read_1,
      bram_ptm_traces_data_read(23) => bram_ptm_traces_data_read_1,
      bram_ptm_traces_data_read(22) => bram_ptm_traces_data_read_1,
      bram_ptm_traces_data_read(21) => bram_ptm_traces_data_read_1,
      bram_ptm_traces_data_read(20) => bram_ptm_traces_data_read_1,
      bram_ptm_traces_data_read(19) => bram_ptm_traces_data_read_1,
      bram_ptm_traces_data_read(18) => bram_ptm_traces_data_read_1,
      bram_ptm_traces_data_read(17) => bram_ptm_traces_data_read_1,
      bram_ptm_traces_data_read(16) => bram_ptm_traces_data_read_1,
      bram_ptm_traces_data_read(15) => bram_ptm_traces_data_read_1,
      bram_ptm_traces_data_read(14) => bram_ptm_traces_data_read_1,
      bram_ptm_traces_data_read(13) => bram_ptm_traces_data_read_1,
      bram_ptm_traces_data_read(12) => bram_ptm_traces_data_read_1,
      bram_ptm_traces_data_read(11) => bram_ptm_traces_data_read_1,
      bram_ptm_traces_data_read(10) => bram_ptm_traces_data_read_1,
      bram_ptm_traces_data_read(9) => bram_ptm_traces_data_read_1,
      bram_ptm_traces_data_read(8) => bram_ptm_traces_data_read_1,
      bram_ptm_traces_data_read(7) => bram_ptm_traces_data_read_1,
      bram_ptm_traces_data_read(6) => bram_ptm_traces_data_read_1,
      bram_ptm_traces_data_read(5) => bram_ptm_traces_data_read_1,
      bram_ptm_traces_data_read(4) => bram_ptm_traces_data_read_1,
      bram_ptm_traces_data_read(3) => bram_ptm_traces_data_read_1,
      bram_ptm_traces_data_read(2) => bram_ptm_traces_data_read_1,
      bram_ptm_traces_data_read(1) => bram_ptm_traces_data_read_1,
      bram_ptm_traces_data_read(0) => bram_ptm_traces_data_read_1,
      bram_ptm_traces_data_write(31 downto 0) => plasma_0_bram_ptm_traces_data_write(31 downto 0),
      bram_ptm_traces_en => plasma_0_bram_ptm_traces_en,
      bram_ptm_traces_reset => plasma_0_bram_ptm_traces_reset,
      clk => clk_1,
      reset => reset_1
    );
end STRUCTURE;
