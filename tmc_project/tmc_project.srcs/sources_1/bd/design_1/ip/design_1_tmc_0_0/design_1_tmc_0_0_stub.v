// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
// Date        : Sun Jan 26 02:16:46 2020
// Host        : HardBlare-Workstation running 64-bit Ubuntu 16.04.6 LTS
// Command     : write_verilog -force -mode synth_stub
//               /home/mounir/HardBlare/hw_vivado/tmc_project/tmc_project.srcs/sources_1/bd/design_1/ip/design_1_tmc_0_0/design_1_tmc_0_0_stub.v
// Design      : design_1_tmc_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "tmc,Vivado 2018.2" *)
module design_1_tmc_0_0(clock, reset_in, axim_done, axim_error_in, 
  axim_address, axim_data_out, axim_data_in, axim_read_en, axim_write_en, axim_start, 
  enable_type_out, data_from_TMC_out, virtual_mem_address_out, t_opcode, pause_out, 
  b_bus_p_out, reg_source_1_gp_out, reset_out1, i_mem_clk, i_mem_reset, i_mem_sel, i_mem_en, 
  i_mem_address, i_mem_data_w, i_mem_data_r, config_mem_clk, config_mem_reset, 
  config_mem_address, config_mem_data_w, config_mem_data_r, config_mem_sel, config_mem_en, 
  instrumentation_empty, instrumentation_data_r, instrumentation_enable)
/* synthesis syn_black_box black_box_pad_pin="clock,reset_in,axim_done,axim_error_in,axim_address[31:0],axim_data_out[31:0],axim_data_in[31:0],axim_read_en,axim_write_en,axim_start,enable_type_out[1:0],data_from_TMC_out[31:0],virtual_mem_address_out[31:0],t_opcode[31:0],pause_out,b_bus_p_out[31:0],reg_source_1_gp_out[31:0],reset_out1,i_mem_clk,i_mem_reset,i_mem_sel[3:0],i_mem_en,i_mem_address[31:0],i_mem_data_w[31:0],i_mem_data_r[31:0],config_mem_clk,config_mem_reset,config_mem_address[31:0],config_mem_data_w[31:0],config_mem_data_r[31:0],config_mem_sel[3:0],config_mem_en,instrumentation_empty,instrumentation_data_r[31:0],instrumentation_enable" */;
  input clock;
  input reset_in;
  input axim_done;
  input axim_error_in;
  output [31:0]axim_address;
  output [31:0]axim_data_out;
  input [31:0]axim_data_in;
  output axim_read_en;
  output axim_write_en;
  output axim_start;
  output [1:0]enable_type_out;
  output [31:0]data_from_TMC_out;
  output [31:0]virtual_mem_address_out;
  output [31:0]t_opcode;
  output pause_out;
  output [31:0]b_bus_p_out;
  output [31:0]reg_source_1_gp_out;
  output reset_out1;
  output i_mem_clk;
  output i_mem_reset;
  output [3:0]i_mem_sel;
  output i_mem_en;
  output [31:0]i_mem_address;
  output [31:0]i_mem_data_w;
  input [31:0]i_mem_data_r;
  output config_mem_clk;
  output config_mem_reset;
  output [31:0]config_mem_address;
  output [31:0]config_mem_data_w;
  input [31:0]config_mem_data_r;
  output [3:0]config_mem_sel;
  output config_mem_en;
  input instrumentation_empty;
  input [31:0]instrumentation_data_r;
  output instrumentation_enable;
endmodule
